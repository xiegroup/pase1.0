#BSUB -J job_name
#BSUB -n 1440
#BSUB -o %J.submit.out
#BSUB -e %J.submit.err
#BSUB -W 1800
#BSUB -q big
#BSUB -R span[ptile=36]
#BSUB -x

cd $LS_SUBCWD
mpiexec ./test_gmg.exe -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
                   -mat_superlu_dist_replacetinypivot -eps_view_pre