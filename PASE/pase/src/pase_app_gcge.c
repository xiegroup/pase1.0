#include "pase_app_gcge.h"
#define SHIFT 0

void PASE_GCGE_MatView(void *mat, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatView(mat, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MatAxpby(double alpha, void *matX, double beta, void *matY, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatAxpby(alpha, matX, beta, matY, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MatDotMultiVec(void *mat, void **x, void **y,
                              int *start, int *end, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatDotMultiVec(mat, x, y, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MatTransDotMultiVec(void *mat, void **x, void **y,
                                   int *start, int *end, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatTransDotMultiVec(mat, x, y, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MatDotVec(void *mat, void *x, void *y, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatDotVec(mat, x, y, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MatTransDotVec(void *mat, void *x, void *y, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMatTransDotVec(mat, x, y, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecCreateByMat(void ***multi_vec, int num_vec, void *src_mat, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecCreateByMat(multi_vec, num_vec, src_mat, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecCreateByMat(void **des_vec, void *src_mat, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecCreateByMat(des_vec, src_mat, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecCreateByVec(void **des_vec, void *src_vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecCreateByVec(des_vec, src_vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecDestroy(void **des_vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecDestroy(des_vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecView(void *x, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecView(x, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecAxpby(double alpha, void *x, double beta, void *y, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecAxpby(alpha, x, beta, y, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecInnerProd(void *x, void *y, double *inner_prod, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecInnerProd(x, y, inner_prod, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecLocalInnerProd(void *x, void *y, double *inner_prod, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecLocalInnerProd(x, y, inner_prod, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_VecSetRandomValue(void *x, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultVecSetRandomValue(x, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecCreateByVec(void ***multi_vec, int num_vec, void *src_vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecCreateByVec(multi_vec, num_vec, src_vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecCreateByMultiVec(void ***multi_vec, int num_vec, void **src_mv, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecCreateByMultiVec(multi_vec, num_vec, src_mv, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecDestroy(void ***multi_vec, int num_vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecDestroy(multi_vec, num_vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_GetVecFromMultiVec(void **multi_vec, int col, void **vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultGetVecFromMultiVec(multi_vec, col, vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_RestoreVecForMultiVec(void **multi_vec, int col, void **vec, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultRestoreVecForMultiVec(multi_vec, col, vec, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecView(void **x, int start, int end, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecView(x, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecLocalInnerProd(char nsdIP, void **x, void **y, int is_vec, int *start, int *end,
                                      double *inner_prod, int ldIP, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecLocalInnerProd(nsdIP, x, y, is_vec, start, end,
                                       inner_prod, ldIP, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecInnerProd(char nsdIP, void **x, void **y, int is_vec, int *start, int *end,
                                 double *inner_prod, int ldIP, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecInnerProd_new(nsdIP, x, y, is_vec, start, end,
                                      inner_prod, ldIP, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecSetRandomValue(void **multi_vec, int start, int end, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecSetRandomValue(multi_vec, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecAxpby(double alpha, void **x, double beta, void **y,
                             int *start, int *end, struct OPS_ *pase_ops_gcge)
{
    PASE_BLASMultiVecAxpby(alpha, x, beta, y, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
    // PASE_DefaultMultiVecAxpby(alpha, x, beta, y, start, end, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void PASE_GCGE_MultiVecLinearComb(void **x, void **y, int is_vec, int *start, int *end,
                                  double *coef, int ldc, double *beta, int incb, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecLinearComb(x, y, is_vec, start, end,
                                   coef, ldc, beta, incb, (PASE_OPS *)(pase_ops_gcge->pase_ops));
}
/**
 * @brief QtAP : 计算 Q^{top} * A * P, 顺序是先算A*P, 一般是矩阵乘以多向量; 再算多向量内积  Q^{top} *(A * P)
 *
 * @param ntsA ntsA = 'S'表示矩阵A是对称矩阵, ntsA = 'N'表示矩阵A为非对称矩阵, ntsA = 'T'表示计算Q*A*P
 * @param ntsdQAP ntsdQAP = 'D'表示最后得到的结果是对角形式, 'S'表示最后得到的是对称形式(但不表示A此时对称),'N'表示最终是一般矩阵
 * @param mvQ 矩阵Q
 * @param matA 矩阵A
 * @param mvP 矩阵P
 * @param is_vec 0表示最终形式为多向量, 1表示为单向量
 * @param startQP startQP[0]--endQP[0], 标记前面一个变量参与计算的列数起点和终点
 * @param endQP startQP[1]--endQP[1], 标记前面一个变量参与计算的列数起点和终点
 * @param qAp 结果存在qAp
 * @param ldQAP qAp的
 * @param mv_ws 临时空间
 * @param pase_ops_gcge 标记调用的基本操作的结构体对象
 */
void PASE_GCGE_MultiVecQtAP(char ntsA, char ntsdQAP,
                            void **mvQ, void *matA, void **mvP, int is_vec,
                            int *startQP, int *endQP, double *qAp, int ldQAP,
                            void **mv_ws, struct OPS_ *pase_ops_gcge)
{
    PASE_DefaultMultiVecQtAP(ntsA, ntsdQAP, mvQ, matA, mvP,
                             is_vec, startQP, endQP, qAp, ldQAP, mv_ws,
                             (PASE_OPS *)(pase_ops_gcge->pase_ops));
}

void GCGE_PASE_SetOps(OPS *pase_ops_gcge, PASE_OPS *pase_ops)
{
    pase_ops_gcge->pase_ops = (void *)pase_ops;
    pase_ops_gcge->MatView = PASE_GCGE_MatView;
    pase_ops_gcge->MultiVecCreateByMat = PASE_GCGE_MultiVecCreateByMat;
    // pase_ops_gcge->MatAxpby = PASE_GCGE_MatAxpby;
    pase_ops_gcge->MatAxpby = NULL;
    pase_ops_gcge->MatDotMultiVec = PASE_GCGE_MatDotMultiVec;
    pase_ops_gcge->MatTransDotMultiVec = PASE_GCGE_MatTransDotMultiVec;
    pase_ops_gcge->MatDotVec = PASE_GCGE_MatDotVec;
    pase_ops_gcge->MatTransDotVec = PASE_GCGE_MatTransDotVec;
    pase_ops_gcge->VecCreateByMat = PASE_GCGE_VecCreateByMat;
    pase_ops_gcge->VecCreateByVec = PASE_GCGE_VecCreateByVec;
    pase_ops_gcge->VecDestroy = PASE_GCGE_VecDestroy;
    pase_ops_gcge->VecView = PASE_GCGE_VecView;
    pase_ops_gcge->VecAxpby = PASE_GCGE_VecAxpby;
    pase_ops_gcge->VecInnerProd = PASE_GCGE_VecInnerProd;
    pase_ops_gcge->VecLocalInnerProd = PASE_GCGE_VecLocalInnerProd;
    pase_ops_gcge->VecSetRandomValue = PASE_GCGE_VecSetRandomValue;
    pase_ops_gcge->MultiVecCreateByVec = PASE_GCGE_MultiVecCreateByVec;
    pase_ops_gcge->MultiVecCreateByMultiVec = PASE_GCGE_MultiVecCreateByMultiVec;
    pase_ops_gcge->MultiVecDestroy = PASE_GCGE_MultiVecDestroy;
    pase_ops_gcge->GetVecFromMultiVec = PASE_GCGE_GetVecFromMultiVec;
    pase_ops_gcge->RestoreVecForMultiVec = PASE_GCGE_RestoreVecForMultiVec;
    pase_ops_gcge->MultiVecView = PASE_GCGE_MultiVecView;
    pase_ops_gcge->MultiVecLocalInnerProd = PASE_GCGE_MultiVecLocalInnerProd;
    pase_ops_gcge->MultiVecInnerProd = PASE_GCGE_MultiVecInnerProd;
    pase_ops_gcge->MultiVecSetRandomValue = PASE_GCGE_MultiVecSetRandomValue;
    pase_ops_gcge->MultiVecAxpby = PASE_GCGE_MultiVecAxpby;
    pase_ops_gcge->MultiVecLinearComb = PASE_GCGE_MultiVecLinearComb;
    pase_ops_gcge->MultiVecQtAP = PASE_GCGE_MultiVecQtAP;

    return;
}

void PASE_Diag_BlockPCG(void *mat, void **mv_b, void **mv_x,
                        int *start_bx, int *end_bx, struct OPS_ *pase_ops_to_gcge)
{
    double p_one = 1.0, m_one = -1.0, zero = 0.0;
    int i_one = 1;
    PASE_OPS *pase_ops = pase_ops_to_gcge->pase_ops;
    OPS *gcge_ops = pase_ops->gcge_ops;
    PASE_BlockPCGSolver *pase_bpcg = (PASE_BlockPCGSolver *)(pase_ops_to_gcge->multi_linear_solver_workspace);
    MultiLinearSolverSetup_BlockPCG(pase_bpcg->max_iter, pase_bpcg->rate, pase_bpcg->tol, pase_bpcg->tol_type,
                                    pase_bpcg->mv_ws, pase_bpcg->dbl_ws, pase_bpcg->int_ws, pase_bpcg->pc,
                                    pase_bpcg->MatDotMultiVec, pase_ops_to_gcge);
#if 0
    gcge_ops->MultiLinearSolver(mat, mv_b, mv_x, start_bx, end_bx, pase_ops_to_gcge);
#else
    // 前处理
    int mv_s[2], mv_e[2];
    PASE_Matrix paseA = (PASE_Matrix)mat;
    void **AH_inv_a = paseA->AH_inv_aux_Hh;
    paseA->is_diag = 1;
    double *tmp = paseA->aux_hh_inv;
    paseA->aux_hh_inv = paseA->aux_hh;
    paseA->aux_hh = tmp;
    PASE_MultiVector paseb = (PASE_MultiVector)mv_b;
    void *b = paseb->b_H;
    double *beta = paseb->aux_h;
    double *beta_tmp = paseb->aux_h_tmp;
    int pase_nev = paseA->num_aux_vec;
    int length = end_bx[0] - start_bx[0];
    int num = pase_nev * length;
    mv_s[0] = 0;
    mv_e[0] = pase_nev;
    mv_s[1] = start_bx[0];
    mv_e[1] = end_bx[0];
    gcge_ops->MultiVecInnerProd('N', AH_inv_a, b, 0, mv_s, mv_e, beta_tmp + start_bx[0] * pase_nev, pase_nev, gcge_ops);
    dscal(&num, &m_one, beta_tmp + start_bx[0] * pase_nev, &i_one);
    daxpy(&num, &p_one, beta + start_bx[0] * pase_nev, &i_one, beta_tmp + start_bx[0] * pase_nev, &i_one);
    tmp = beta;
    paseb->aux_h = paseb->aux_h_tmp;
    paseb->aux_h_tmp = tmp;

    void **x = ((PASE_MultiVector)mv_x)->b_H;
    double *gamma = ((PASE_MultiVector)mv_x)->aux_h;
    tmp = ((PASE_MultiVector)mv_x)->aux_h_tmp;
    dcopy(&num, gamma + start_bx[1] * pase_nev, &i_one, tmp, &i_one);
    mv_s[0] = 0;
    mv_e[0] = pase_nev;
    mv_s[1] = start_bx[1];
    mv_e[1] = end_bx[1];
    gcge_ops->MultiVecLinearComb(AH_inv_a, x, 0, mv_s, mv_e,
                                 tmp, pase_nev, &p_one, 0, gcge_ops);

#if DIRECT == 0
    //(1) A_H * uH = fH wzj
    Mat solmat, rhsmat;
    KSP ksp = (KSP)(paseA->factorization);
    BVSetActiveColumns((BV)x, start_bx[1], end_bx[1]);
    BVSetActiveColumns((BV)b, start_bx[0], end_bx[0]);
    BVGetMat((BV)x, &solmat);
    BVGetMat((BV)b, &rhsmat);
    KSPSetMatSolveBatchSize(ksp, 1);
    KSPMatSolve(ksp, (Mat)rhsmat, (Mat)solmat);
    BVRestoreMat((BV)x, &solmat);
    BVRestoreMat((BV)b, &rhsmat);

    //(2) gamma = \alpha^{-1} * g
    double *alpha_inv = paseA->aux_hh; //aux_hh_inv : alpha^{-1}
    double *g = paseb->aux_h;
    char charN = 'N';
    dgemm(&charN, &charN, &pase_nev, &length, &pase_nev, &p_one, alpha_inv, &pase_nev,
        g + (start_bx[0] * pase_nev), &pase_nev, &zero, gamma + (start_bx[1] * pase_nev), &pase_nev);
#else
    gcge_ops->MultiLinearSolver(mat, mv_b, mv_x, start_bx, end_bx, pase_ops_to_gcge);
#endif
    // 后处理
    tmp = paseA->aux_hh_inv;
    paseA->aux_hh_inv = paseA->aux_hh;
    paseA->aux_hh = tmp;
    paseA->is_diag = 0;
    tmp = paseb->aux_h;
    paseb->aux_h = paseb->aux_h_tmp;
    paseb->aux_h_tmp = tmp;

    tmp = ((PASE_MultiVector)mv_x)->aux_h_tmp;
    dcopy(&num, gamma + start_bx[1] * pase_nev, &i_one, tmp, &i_one);
    dscal(&num, &m_one, tmp, &i_one);
    mv_s[0] = 0;
    mv_e[0] = pase_nev;
    mv_s[1] = start_bx[1];
    mv_e[1] = end_bx[1];
    gcge_ops->MultiVecLinearComb(AH_inv_a, x, 0, mv_s, mv_e,
                                 tmp, pase_nev, &p_one, 0, gcge_ops);
#endif
}

void PASE_Diag_BlockPCG_Setup(int max_iter, double rate, double tol,
                              const char *tol_type, void **mv_ws[3], double *dbl_ws, int *int_ws,
                              void *pc, void (*MatDotMultiVec)(void **x, void **y, int *, int *, void **z, int s, struct OPS_ *),
                              struct OPS_ *pase_ops_to_gcge)
{
    static PASE_BlockPCGSolver pase_bpcg_static = {
        .max_iter = 50, .rate = 1e-2, .tol = 1e-12, .tol_type = "abs", .mv_ws = {}, .pc = NULL, .MatDotMultiVec = NULL};
    pase_bpcg_static.max_iter = max_iter;
    pase_bpcg_static.rate = rate;
    pase_bpcg_static.tol = tol;
    strcpy(pase_bpcg_static.tol_type, tol_type);
    pase_bpcg_static.mv_ws[0] = mv_ws[0];
    pase_bpcg_static.mv_ws[1] = mv_ws[1];
    pase_bpcg_static.mv_ws[2] = mv_ws[2];
    pase_bpcg_static.dbl_ws = dbl_ws;
    pase_bpcg_static.int_ws = int_ws;
    pase_bpcg_static.MatDotMultiVec = MatDotMultiVec;

    pase_bpcg_static.niter = 0;
    pase_bpcg_static.residual = -1.0;

    pase_ops_to_gcge->multi_linear_solver_workspace = (void *)(&pase_bpcg_static);
    pase_ops_to_gcge->MultiLinearSolver = PASE_Diag_BlockPCG;
    return;
}
