#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pase_param.h"

void PASE_PARAMETER_Create(PASE_PARAMETER *param, int num_levels, int nev, double rtol, MULTIGRID_TYPE type)
{
    *param = (PASE_PARAMETER)malloc(sizeof(PASE_PARAMETER_PRIVATE));
    /* params about levels */
    (*param)->num_levels = num_levels;
    /* params about problem */
    (*param)->A = NULL;
    (*param)->B = NULL;
    (*param)->A_array = NULL;
    (*param)->B_array = NULL;
    (*param)->P_array = NULL;
    (*param)->nev = nev;
    (*param)->real_nev = nev;
    (*param)->num_given_eigs = 0;
    (*param)->multigrid_type = type;
    /* params about pase */
    (*param)->solver_type = PASE_DEFAULT;
    (*param)->aux_coarse_level = -1;
    (*param)->aux_fine_level = 0;
    (*param)->max_cycle_count = 20;
    (*param)->rtol = rtol;
    /* params about smoothing */
    (*param)->smoothing_type = PASE_BCG;
    (*param)->max_pre_count = 40;
    (*param)->max_post_count = 40;
    /* params about initial GCGE */
    (*param)->initial_level = -1;
    (*param)->max_initial_direct_count = 500;
    (*param)->initial_atol = 0.0;
    (*param)->initial_rtol = rtol;
    /* params about aux GCGE */
    (*param)->max_direct_count = 40;
    (*param)->aux_atol = 0.0;
    (*param)->aux_rtol = rtol;
    (*param)->max_aux_rtol = rtol;
    (*param)->aux_rtol_coef = 0.0;
    /* params about GMG */
    (*param)->boundary_type = PASE_DIRICHLET;
    (*param)->boundary_delete = true;
    (*param)->boundary_num = 0;
    (*param)->boundary_index = NULL;
    /* params about precondition */
    (*param)->pc_type = PRECOND_NONE;
    (*param)->pc_eigendcp = false;
    /* params about print */
    (*param)->print_level = 0;
    /* 分组分批 */
    (*param)->if_groups = false;
    (*param)->group_num = 1;
    (*param)->group_id = -1;
    (*param)->group = MPI_GROUP_NULL;
    (*param)->group_comm = MPI_COMM_NULL;
    (*param)->batch_size = 0;
    (*param)->if_batches = false;
    (*param)->more_batch_size = 0;
    (*param)->more_aux_nev = 0;
    /* 接口用 */
    (*param)->if_error_estimate = true;
    (*param)->initial_solution = NULL;
    (*param)->A_pre = NULL;
    (*param)->B_pre = NULL;
    (*param)->shift_pre = NULL;
}

void PASE_PARAMETER_Destroy(PASE_PARAMETER *param)
{
    free(*param);
    *param = NULL;
}

void PASE_PARAMETER_Get_from_command_line(PASE_PARAMETER param, int argc, char *argv[])
{
    int arg_index = 0;

    while (arg_index < argc)
    {
        if (strcmp(argv[arg_index], "-nev") == 0)
        {
            arg_index++;
            param->nev = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-num_levels") == 0)
        {
            arg_index++;
            param->num_levels = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-print_level") == 0)
        {
            arg_index++;
            param->print_level = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-initial_level") == 0)
        {
            arg_index++;
            param->initial_level = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-aux_coarse_level") == 0)
        {
            arg_index++;
            param->aux_coarse_level = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-aux_fine_level") == 0)
        {
            // 要求解的特征值个数
            arg_index++;
            param->aux_fine_level = atoi(argv[arg_index++]);
        }
        else if (strcmp(argv[arg_index], "-pc_type") == 0)
        {
            // 要求解的特征值个数
            arg_index++;
            param->pc_type = atoi(argv[arg_index++]);
        }
        else
        {
            arg_index++;
        }
    }
}
