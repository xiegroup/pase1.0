# MPI 
find_package(MPI REQUIRED)

# complier
SET(CMAKE_C_COMPILER "mpiicc")
SET(CMAKE_CXX_COMPILER "mpiicpc")

# complie flag
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -march=native -mtune=native -mkl=cluster -lifcore -std=c99")


