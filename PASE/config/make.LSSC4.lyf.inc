CPP      = mpiicpc
CC       = mpiicc

LIBBLASLAPACK = -lmkl_lapack95_lp64 -lmkl_blas95_lp64 -mkl=cluster -lifcore -lm -lstdc++

PETSCFLAGS = -fPIC -O2 -march=native -mtune=native -mkl=cluster -lifcore -std=c99  
PETSCINC   = -I${PETSC_DIR}/include -I${PETSC_DIR}/${PETSC_ARCH}/include -I${HYPRE_DIR}/include -I${PETSC_DIR}/include/petsc/private -I${PETSC_DIR}/src/mat/impls/aij/mpi
LIBPETSC   = -L${PETSC_DIR}/${PETSC_ARCH}/lib -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib -lpetsc \
            -Wl,-rpath,/soft/apps/intel/oneapi_base_2021/mkl/2021.1.1/lib/intel64 -L/soft/apps/intel/oneapi_base_2021/mkl/2021.1.1/lib/intel64 \
            -Wl,-rpath,/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/lib \
            -L/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/lib -Wl,-rpath,/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/lib/release \
            -L/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/lib/release -Wl,-rpath,/soft/apps/intel/oneapi_base_2021/tbb/2021.1.1/lib/intel64/gcc4.8 \
            -L/soft/apps/intel/oneapi_base_2021/tbb/2021.1.1/lib/intel64/gcc4.8 -Wl,-rpath,/soft/apps/intel/oneapi_base_2021/compiler/2021.1.1/linux/lib \
            -L/soft/apps/intel/oneapi_base_2021/compiler/2021.1.1/linux/lib -Wl,-rpath,/soft/apps/intel/oneapi_hpc_2021/compiler/2021.1.1/linux/lib \
            -L/soft/apps/intel/oneapi_hpc_2021/compiler/2021.1.1/linux/lib -Wl,-rpath,/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/libfabric/lib \
            -L/soft/apps/intel/oneapi_hpc_2021/mpi/2021.1.1/libfabric/lib -Wl,-rpath,/soft/apps/intel/oneapi_hpc_2021/compiler/2021.1.1/linux/compiler/lib/intel64_lin \
            -L/soft/apps/intel/oneapi_hpc_2021/compiler/2021.1.1/linux/compiler/lib/intel64_lin -Wl,-rpath,/soft/apps/intel/oneapi_base_2021/mkl/2021.1.1/lib/intel64_lin \
            -L/soft/apps/intel/oneapi_base_2021/mkl/2021.1.1/lib/intel64_lin -Wl,-rpath,/usr/lib/gcc/x86_64-redhat-linux/4.8.5 -L/usr/lib/gcc/x86_64-redhat-linux/4.8.5 \
            -lcmumps -ldmumps -lsmumps -lzmumps -lmumps_common -lpord -lmkl_scalapack_lp64 -lsuperlu -lsuperlu_dist -lmkl_lapack95_lp64 -lmkl_blas95_lp64 -lparmetis -lmetis -lmpi -lquadmath -lstdc++ -ldl

SLEPCFLAGS = -fPIC -O2 -mtune=native -mkl=cluster -lifcore -std=c99
SLEPCINC   = -I${SLEPC_DIR}/include -I${SLEPC_DIR}/${PETSC_ARCH}/include -I${SLEPC_DIR}/include/slepc/private
LIBSLEPC   = -L${SLEPC_DIR}/${PETSC_ARCH}/lib -Wl,-rpath,${SLEPC_DIR}/${PETSC_ARCH}/lib -lslepc

OPENPFEMINC = -I${OPENPFEM_DIR}/include 
LIBOPENPFEM = -L${OPENPFEM_DIR}/lib -Wl,-rpath,${OPENPFEM_DIR}/lib -lopenpfem


CFLAGS = ${SLEPCFLAGS} ${PETSCFLAGS}

.SUFFIXES: .c.o
vpath %.h ../include
vpath %.c ../test
SOURCE = $(wildcard ../src/*.c) $(wildcard ../src/mesh/*.c) $(wildcard ../src/fem/*.c) $(wildcard ../src/algebra/*.c) $(wildcard ../src/solver/eigensolver/*.c) $(wildcard ../src/solver/linearsolver/*.c) $(wildcard ../src/solver/*.c) 
OBJECTS = $(SOURCE:.c=.o)
