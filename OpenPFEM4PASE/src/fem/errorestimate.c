
#include "errorestimate.h"

#define PRINT_INFO 0

// 计算有限元函数的误差
DOUBLE ErrorEstimate(FEMFUNCTION *femfun, INT NumMultiIndex, MULTIINDEX *MultiIndex, FUNCTIONVEC *fun,
                     ERRORFORM *ErrorForm, QUADRATURE *Quadrature)
{
    INT ind, LocalNumElems;
    /** 目前只实现了左右网格是相同的情况 */
    FEMSPACE *FemSpace = femfun->FEMSpace;
    MESH *mesh = FemSpace->Mesh; // 获得有限元函数相对应的网格
    BASE *Base = FemSpace->Base;
    INT worlddim = mesh->worlddim;
    INT NumBase = Base->NumBase;
    INT ValueDim = Base->ValueDim;
    INT *LocalIndex = FemSpace->LocalIndex;
    INT *BeginIndex = FemSpace->BeginIndex;
    INT *localdof;
    DOUBLE *FEMValues = femfun->Values;
    DOUBLE *localvalues = malloc(NumBase * sizeof(DOUBLE));
    ELEMENT *Elem;
    INT NumPoints = Quadrature->NumPoints, i, j, k, tmpind, indindex, inddim;
    DOUBLE *BaseValues = malloc(NumBase * ValueDim * NumMultiIndex * sizeof(DOUBLE)); // 存储所有基函数的所有的导数的值
    DOUBLE *multiindexvalues = malloc(NumMultiIndex * ValueDim * sizeof(DOUBLE));     // 用来存储某一个基函数所有的导数值
    DOUBLE *funvalue = malloc(NumMultiIndex * ValueDim * sizeof(DOUBLE));             // 用来存储函数的计算值
    DOUBLE Error = 0.0, value, weight;
    DOUBLE Coord[worlddim], RefCoord[worlddim];
    MULTIINDEX NoGradIndex;
    // 进行与空间维数相关的运算和操作
    OpenPFEM_Print("NumBase * NumMultiIndex * ValueDim = %d\n", NumBase * NumMultiIndex * ValueDim);
    switch (worlddim)
    {
    case 2:
        NoGradIndex = D00;
        LocalNumElems = mesh->num_face;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    case 3:
        NoGradIndex = D000;
        // get the number of elements
        LocalNumElems = mesh->num_volu;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    }
    // 判断是否需要计算Jacobi逆矩阵
    tmpind = 0;
    for (i = 0; i < NumMultiIndex; i++)
    {
        // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
        if (MultiIndex[i] > NoGradIndex)
        {
            tmpind = 1;
            i = NumMultiIndex + 3;
        }
    }
    // 提前计算信息******
    // lhc 0219 add

    DISCRETEFORM *DiscreteForm = DiscreteFormBuild(FemSpace, NumMultiIndex, MultiIndex,
                                                   FemSpace, NumMultiIndex, MultiIndex,
                                                   NULL, NULL, NULL, Quadrature);
    ComputeBaseValuesInPointsOfRefElem(DiscreteForm);
    BOOL IsHaveLeftBaseValuesInPointsOfRefElem = DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem;
    BOOL IsHaveRightBaseValuesInPointsOfRefElem = DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem;
    DOUBLE *LeftBaseValuesInPointsOfRefElem = DiscreteForm->LeftBaseValuesInPointsOfRefElem;
    DOUBLE *RightBaseValuesInPointsOfRefElem = DiscreteForm->RightBaseValuesInPointsOfRefElem;
    INT *IndLeftMultiIndex = DiscreteForm->IndLeftMultiIndex;
    INT *IndRightMultiIndex = DiscreteForm->IndRightMultiIndex;

    // 增加利用变分形式判断单元类型
    SetElemType(Elem, FemSpace);
    // INT i,j;
    if (Elem->is_curl == 1 || Elem->is_div == 1)
        tmpind = 1;

    // 接下来进行单元循环
    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // 建立当前的有限单元
        ElementBuild(mesh, ind, Elem);
        // 计算单元的体积和Jacobian矩阵
        if (tmpind == 0)
        {
            ComputeElementJacobian(Elem);
        }
        else
        {
            ComputeElementInvJacobian(Elem);
        }
        // 获得当前单元上的有限元函数值
        localdof = LocalIndex + BeginIndex[ind];
        for (i = 0; i < NumBase; i++)
        {
            localvalues[i] = FEMValues[localdof[i]];
        }
        // 调换基函数对应自由度取值的顺序
        if (Elem->is_curl == 1)
        {
            ReorderFEMValuesForCurlElem(FemSpace, ind, Elem, localvalues);
        }
        if (Elem->is_div == 1)
        {
            ReorderFEMValuesForDivElem(FemSpace, ind, Elem, localvalues);
        }
        // OpenPFEM_Print("新编号下自由度值 %d\n", ind);
        // for (i = 0; i < 45; i++)
        // {
        //     OpenPFEM_Print("localvalues(%d) = %2.10f;\n", i+1, localvalues[i]);
        // }
        // OpenPFEM_Print("单元对应jacobian %d\n", ind);
        // for (i = 0; i < 9; i++)
        // {
        //     OpenPFEM_Print("Jacobi(%d,%d) = %2.10f;\n", i/3+1, i-(i/3)*3+1, Elem->Jacobian[i/3][i-(i/3)*3]);
        // }
        // OpenPFEM_Print("单元对应inv_jacobian %d\n", ind);
        // for (i = 0; i < 9; i++)
        // {
        //     OpenPFEM_Print("Inv(%d,%d) = %2.10f;\n", i/3+1, i-(i/3)*3+1, Elem->InvJacobian[i/3][i-(i/3)*3]);
        // }

        // 对积分点的循环, 求得本单元上误差
        for (k = 0; k < NumPoints; k++)
        {
            switch (worlddim)
            {
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                RefCoord[2] = Quadrature->QuadZ[k];
                break;
            } // end switch(worlddim)
            weight = Quadrature->QuadW[k];
            ElementRefCoord2Coord(Elem, RefCoord, Coord);
            // 计算基函数微分的值，为组装做准备,根据基函数求解相应的基函数值
            memset(BaseValues, 0.0, NumBase * ValueDim * NumMultiIndex * sizeof(DOUBLE));
            for (j = 0; j < NumMultiIndex; j++)
            {
                if (IsHaveLeftBaseValuesInPointsOfRefElem == 0)
                {
                    // 直接计算基函数的微分值
                    GetBaseValues(Base, MultiIndex[j], Elem, Coord, RefCoord, BaseValues + j * NumBase * ValueDim);
                }
                else
                {
                    // 利用已经计算好的参考单元上的微分值进行计算
                    GetBaseValuesWithInfo(Base, MultiIndex[j], Elem,
                                          LeftBaseValuesInPointsOfRefElem + k * IndLeftMultiIndex[3] * NumBase * ValueDim,
                                          IndLeftMultiIndex, BaseValues + j * NumBase * ValueDim);
                }
                // GetBaseValues(Base, MultiIndex[j], Elem, Coord, RefCoord, BaseValues + j * NumBase * ValueDim);
            }
            // 线性组装出在该积分点上所有微分的值， 每个微分的值
            memset(multiindexvalues, 0.0, NumMultiIndex * ValueDim * sizeof(DOUBLE));
            for (indindex = 0; indindex < NumMultiIndex; indindex++)
            {
                // 第inddex个微分
                for (i = 0; i < NumBase; i++)
                {
                    // OpenPFEM_Print("localvalues[%d]=%2.10f\n", i, localvalues[i]);
                    //   第i个基函数
                    for (inddim = 0; inddim < ValueDim; inddim++)
                    {
                        // 第inddim维的基函数值
                        // OpenPFEM_Print("BaseValues[%d]=%2.10f\n", indindex * NumBase * ValueDim
                        // + i * ValueDim + inddim, BaseValues[indindex * NumBase * ValueDim + i * ValueDim + inddim]);
                        // 第indindex个微分, 第i个基函数, 第inddim维度
                        multiindexvalues[indindex * ValueDim + inddim] += localvalues[i] * BaseValues[indindex * NumBase * ValueDim + i * ValueDim + inddim];
                    } // end for(inddim=0;inddim<ValueDim;inddim++)
                }     // end for(i=0;i<NumBase;i++)
            }         // end for(indindex=0;indindex<NumMultiIndex;indindex++)
            // 计算精确函数值
            if (fun != NULL)
                fun(Coord, NumMultiIndex * ValueDim, funvalue);
            ErrorForm(multiindexvalues, funvalue, &value);
            Error += value * weight * Elem->Volumn;
        } // end for(k=0;k<NumPoints;k++)//对积分点的循环
    }     // end for(ind=0;ind<LocalNumElems;ind++)
    OpenPFEM_Free(localvalues);
    OpenPFEM_Free(BaseValues);
    OpenPFEM_Free(multiindexvalues);
    OpenPFEM_Free(funvalue);
    ElementDestroy(&Elem);
    DOUBLE ERROR;
    MPI_Allreduce(&Error, &ERROR, 1, MPI_DOUBLE, MPI_SUM, mesh->comm); // 将所有进程的误差加起来得到总的误差
    return sqrt(ERROR);
} // end of error estimate

// 1.边值问题的后验误差估计
/*输入参数：
    FEMFUNCTION *femfun: 边值问题中方程的解
    INT NumMultiIndexOfElem: 计算后验误差时单元需要的微分信息的总个数
    INT NumMultiIndexOfEdge: 计算后验误差时边界需要的微分信息的总个数
    MULTIINDEX *MultiIndex: 计算后验误差需要的微分信息 前面部分是单元需要的 后面部分是边界需要的
                            从MultiIndex[0]到MultiIndex[NumMultiIndexOfElem-1]是单元需要的微分信息
                            从MultiIndex[NumMultiIndexOfElem]到MultiIndex[NumMultiIndexOfElem+NumMultiIndexOfEdge-1]是边界需要的微分信息
    FUNCTIONVEC *rhs: 函数接口 可以输入函数 在求解边值问题的后验误差时需要输入右端项
    ERRORFORM *ElemErrorForm: 单元上的误差计算格式 这里是内部函数的平方形式
    PARTIALERRORFORM *EdgeErrorForm: 边界上的误差计算格式 这里没有进行平方操作 涉及到计算跳量 这里包括了法向量的计算方式
    QUADRATURE *QuadratureElem: 单元上的积分格式
    QUADRATURE *QuadratureEdge: 边界上的积分格式
    DOUBLE *PosterioriError: 返回解的后验误差估计
    DOUBLE *TotalError: 返回全局的后验误差值\eta=sqrt(\sum \eta_K)
*/
void PosterioriErrorEstimate(FEMFUNCTION *femfun, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
                             FUNCTIONVEC *rhs, ERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
                             QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError)
{
    INT ind, LocalNumElems, LocalNumEdges;
    /** 目前只实现了左右网格是相同的情况 */
    FEMSPACE *FemSpace = femfun->FEMSpace; // 获得有限元空间的对象
    MESH *mesh = FemSpace->Mesh;           // 获得有限元函数相对应的网格
    INT worlddim = mesh->worlddim;
    BASE *Base = FemSpace->Base;
    INT NumBase = Base->NumBase;   // 基函数的个数
    INT ValueDim = Base->ValueDim; // 有限元函数的维数
    INT *LocalIndex = FemSpace->LocalIndex;
    INT *BeginIndex = FemSpace->BeginIndex;
    INT *localdof;                                          // 用来记录每个单元上的有限元函数系数
    DOUBLE *FEMValues = femfun->Values;                     // 有限元函数的系数值
    DOUBLE *localvalues = malloc(NumBase * sizeof(DOUBLE)); // 基函数个数长度的实数数组
    ELEMENT *Elem;                                          // 有限单元的对象
    INT NumPoints = QuadratureElem->NumPoints;              // 单元上的积分点个数
    INT NumPointsEdge = QuadratureEdge->NumPoints;          // 在边界上的积分点个数
    INT i, j, k, tmpind, indindex, inddim, EdgeNum, NumElemEdges, eind, edge_BDID, bdind;
    DOUBLE *BaseValuesOfElem = malloc(NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
    DOUBLE *BaseValuesOfEdge = malloc(NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
    DOUBLE *multiindexvaluesOfElem = malloc(NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
    DOUBLE *multiindexvaluesOfEdge = malloc(NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
    // 下面这个量用来存储右端项的值
    DOUBLE *rhsvalue = malloc(ValueDim * sizeof(DOUBLE)); // 针对某一种导数的函数值
    DOUBLE value, weight, weightEdge, xi, eta, zeta;
    DOUBLE *value_vec = malloc(ValueDim * sizeof(DOUBLE));
    DOUBLE Coord[worlddim], RefCoord[worlddim]; // 记录实际坐标和参考单元上的坐标
    // EdgeCoord: 用来存储每个单元上低一维面（线）上的坐标, EdgeRefCoord: 用来存储在标准单元某一个积分点的坐标
    DOUBLE EdgeCoord[worlddim * worlddim], EdgeRefCoord[worlddim]; // 记录边界上的坐标和参考坐标
    MULTIINDEX NoGradIndex;
    // 进行与空间维数相关的运算和操作
    switch (worlddim)
    {
    case 2:
        NoGradIndex = D00;
        LocalNumElems = mesh->num_face;
        LocalNumEdges = mesh->num_line;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    case 3:
        NoGradIndex = D000;
        // get the number of elements
        LocalNumElems = mesh->num_volu;
        LocalNumEdges = mesh->num_face;
        // Initialization an element
        Elem = ElementInitial(mesh);
        break;
    }
    // 判断是否需要计算Jacobi逆矩阵
    tmpind = 0;
    for (i = 0; i < NumMultiIndexOfElem; i++)
    {
        // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
        if (MultiIndex[i] > NoGradIndex)
        {
            tmpind = 1;
            break;
        } // end for if
    }     // end for i
    if (tmpind == 0)
    {
        for (i = NumMultiIndexOfElem; i < NumMultiIndexOfElem + NumMultiIndexOfEdge; i++)
        {
            // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
            if (MultiIndex[i] > NoGradIndex)
            {
                tmpind = 1;
                break;
                // i = NumMultiIndex + 3;
            } // end for if
        }     // end for i
    }         // end if (tmpind == 0)
    // 每个单元对应一个后验误差值
    memset(PosterioriError, 0.0, LocalNumElems * sizeof(DOUBLE));
    // 每个单元的每个Edge(每个面)对应一个法向量的值(目前只针对单纯形)
    DOUBLE NormalVec[(worlddim + 1) * worlddim]; // 3*2 或者 4*3
    // 存储单元边界的直径h(2d时是边的长度,3d时是面的外接圆直径),
    // 边界面积S(2d时是边的长度,3d时是面的面积),单元直径(2d时是外接圆直径,3d时是体的外接球直径)
    DOUBLE hEdge[worlddim + 1], SEdge[worlddim + 1], hElem; // 每个Edge的半径和面积
    // 存储边界上用于计算跳量的积分点的信息，每条边界存储在积分点上的外法向导数值, NumPointsEdge：每个单元边界上积分点个数
    DOUBLE *EdgePointValue = malloc(LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
    memset(EdgePointValue, 0.0, LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
    // 定义存储进程局部和全局的单元残差量和边的跳跃量
    DOUBLE LocalResidual = 0.0, TotalResidual = 0.0, LocalEdgeJump = 0.0, TotalEdgeJump = 0.0, TotalPostErr = 0.0;
    DOUBLE SqrthTimess; // 临时记录sqrt(hEdge[eind] * SEdge[eind])
    // 接下来进行单元循环, 计算每个单元上单元残差量和Edge上的外法向导数

    // 增加利用变分形式判断单元类型
    SetElemType(Elem, FemSpace);
    if (Elem->is_curl == 1)
        tmpind = 1;

    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // 建立当前的有限单元
        ElementBuild(mesh, ind, Elem);
        // 计算单元的体积和Jacobian矩阵
        if (tmpind == 0)
        {
            ComputeElementJacobian(Elem);
        }
        else
        {
            ComputeElementInvJacobian(Elem);
        }
        // 首先计算单元上的残差量, 计算单元的其他信息：Edge的直径和Elem的直径
        ComputeElementInfo(Elem, hEdge, SEdge, &hElem);
        // 获得当前单元上的有限元函数的系数，注意这里的自由度分布是按照单元的方式来排列的
        localdof = LocalIndex + BeginIndex[ind];
        // 得到当前单元有限元函数的系数
        for (i = 0; i < NumBase; i++)
        {
            localvalues[i] = FEMValues[localdof[i]];
        }
        // 调换基函数对应自由度取值的顺序
        if (Elem->is_curl == 1)
        {
            ReorderFEMValuesForCurlElem(FemSpace, ind, Elem, localvalues);
        }
        // 对单元上积分点循环, 求得本单元上残差量
        for (k = 0; k < NumPoints; k++)
        {
            // 第k个积分点
            switch (worlddim)
            {
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = QuadratureElem->QuadX[k];
                RefCoord[1] = QuadratureElem->QuadY[k];
                weight = QuadratureElem->QuadW[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = QuadratureElem->QuadX[k];
                RefCoord[1] = QuadratureElem->QuadY[k];
                RefCoord[2] = QuadratureElem->QuadZ[k];
                weight = QuadratureElem->QuadW[k];
                break;
            }
            ElementRefCoord2Coord(Elem, RefCoord, Coord); // 得到与积分点相应的实际坐标
            // 计算基函数微分的值,根据基函数求解相应的基函数值
            memset(BaseValuesOfElem, 0.0, NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
            // 按照导数的方式来排列, 每一个导数值的维数: NumBase * ValueDim
            for (j = 0; j < NumMultiIndexOfElem; j++)
            {
                GetBaseValues(Base, MultiIndex[j], Elem, Coord, RefCoord, BaseValuesOfElem + j * NumBase * ValueDim);
            }
            // 调整顺序得到每个基函数在当前积分点上所有微分的值: 维数: NumMultiIndex*ValueDim
            memset(multiindexvaluesOfElem, 0.0, NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE));
            for (indindex = 0; indindex < NumMultiIndexOfElem; indindex++)
            {
                for (i = 0; i < NumBase; i++)
                {
                    for (inddim = 0; inddim < ValueDim; inddim++)
                    {
                        multiindexvaluesOfElem[indindex * ValueDim + inddim] +=
                            localvalues[i] * BaseValuesOfElem[indindex * NumBase * ValueDim + i * ValueDim + inddim];
                    } // end for inddim
                }     // end for i
            }         // end for indindex
            // 计算右端项在积分点处的函数值 返回到funvalue
            rhs(Coord, ValueDim, rhsvalue);
            // 计算(f+\laplace u)^2在积分点处的值, 用户可以自由定义
            ElemErrorForm(multiindexvaluesOfElem, rhsvalue, &value);
            // 计算后验误差中的 单元部分的总和
            PosterioriError[ind] += value * weight;
        }                                                     // end for(k=0;k<NumPoints;k++)//对积分点的循环结束
        PosterioriError[ind] *= hElem * hElem * Elem->Volumn; // 乘上相应的h_K^2*|K|
        LocalResidual += PosterioriError[ind];                // 将本单元的残差加到总的残差上去
        // 下面来计算单元上每个Edge上的跳量, Edge上的每个积分点都需要进行存储值
        // 对单元的Edge进行循环, 计算Edge上积分点的导数值和外法向的值
        switch (worlddim)
        {
        case 2:
            NumElemEdges = mesh->Faces[ind].NumLines;
            break;
        case 3:
            NumElemEdges = mesh->Volus[ind].NumFaces;
            break;
        }                                         // 得到了每个单元上的边界条数
        ComputeElementNormalVec(Elem, NormalVec); // 计算目前单元每个边界的外法向量
        // 对本单元的Edge进行遍历
        for (eind = 0; eind < NumElemEdges; eind++)
        {
            SqrthTimess = sqrt(hEdge[eind] * SEdge[eind]);
            // 处理当前单元上的eind个edge
            switch (worlddim)
            {
            case 2:
                // EdgeNum表示这条边在本进程上的编号
                EdgeNum = mesh->Faces[ind].Line4Face[eind];
                edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            case 3:
                // EdgeNum表示这个面在本进程上的编号
                EdgeNum = mesh->Volus[ind].Face4Volu[eind];
                edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            }
            if (bdind == 1)
            {
                // 下面这个函数确定当前边上两个节点的坐标或面上三个节点在参考单元上的局部坐标
                FindEdgeCoord(mesh, ind, eind, EdgeCoord); // EdgeCoord存储了当前边界节点的参考坐标
                // 对当前的边或者面上积分点进行遍历
                for (k = 0; k < NumPointsEdge; k++)
                {
                    // 根据积分格式，找到当前积分点在标准单元上的坐标
                    switch (worlddim)
                    {
                    case 2:
                        // 获得积分点在参考单元上的坐标
                        xi = QuadratureEdge->QuadX[k];
                        for (i = 0; i < worlddim; i++)
                        {
                            EdgeRefCoord[i] = (1.0 - xi) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i];
                        }
                        break;
                    case 3:
                        // 获得积分点在参考单元上的坐标
                        xi = QuadratureEdge->QuadX[k];
                        eta = QuadratureEdge->QuadY[k];
                        for (i = 0; i < worlddim; i++)
                        {
                            EdgeRefCoord[i] = (1 - xi - eta) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i] + eta * EdgeCoord[2 * worlddim + i];
                        }
                        break;
                    }
                    // weightEdge = QuadratureEdge->QuadW[k]; //获得积分的权重
                    // 获得目前面上的积分点的实际坐标
                    ElementRefCoord2Coord(Elem, EdgeRefCoord, Coord);
                    // 计算基函数微分的值,为组装做准备,根据基函数求解相应的基函数值
                    memset(BaseValuesOfEdge, 0.0, NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
                    for (j = 0; j < NumMultiIndexOfEdge; j++)
                    {
                        GetBaseValues(Base, MultiIndex[NumMultiIndexOfElem + j], Elem, Coord, EdgeRefCoord, BaseValuesOfEdge + j * NumBase * ValueDim);
                    }
                    // 线性组装出在该积分点上所有微分的值
                    memset(multiindexvaluesOfEdge, 0.0, NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE));
                    for (indindex = 0; indindex < NumMultiIndexOfEdge; indindex++)
                    {
                        for (i = 0; i < NumBase; i++)
                        {
                            for (inddim = 0; inddim < ValueDim; inddim++)
                            {
                                multiindexvaluesOfEdge[indindex * ValueDim + inddim] +=
                                    localvalues[i] * BaseValuesOfEdge[indindex * NumBase * ValueDim + i * ValueDim + inddim];
                            } // end for inddim
                        }     // end for i
                    }         // end for indindex
                    // 调用边界误差的定义
                    EdgeErrorForm(multiindexvaluesOfEdge, rhsvalue, NormalVec + eind * worlddim, value_vec);
                    for (inddim = 0; inddim < ValueDim; inddim++)
                    {
                        value_vec[inddim] = value_vec[inddim] * SqrthTimess;
                        EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim] += value_vec[inddim];
                    }
                    // EdgePointValue[EdgeNum * NumPointsEdge + k] += value;
                } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
            }     // end for if(bdind==1)
        }         // end for eind: 对单元的每个边界进行循环
    }             // end for(ind=0;ind<LocalNumElems;ind++)//对单元进行循环
    // 计算单元部分的后验误差和
    MPI_Allreduce(&LocalResidual, &TotalResidual, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
    OpenPFEM_Print("TotalResidual = %2.10f\n", TotalResidual);
    INT nprocs;
    MPI_Comm_size(mesh->comm, &nprocs);
    if (nprocs > 1)
    {
        // 对跳量所需要的信息进行传输: 现在要传输：边上的NumPointsEdge个DOUBLE
        INT edgeind, pointind, recdataNum, *edgeindex;
        //(1) paradata creation
        PARADATA *paradata = NULL;
        ParaDataCreate(&paradata, mesh);
        //(2) paradata attachment (注意这里add的顺序将是返回数据里的顺序)switch (worlddim)
        switch (worlddim)
        {
        case 2:
            ParaDataAdd(paradata, LINEDATA, NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
            break;
        case 3:
            ParaDataAdd(paradata, FACEDATA, NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
            break;
        }
        //(3) communication
        RECDATA *recdata = NULL;
        ParaDataCommunicate(paradata, &recdata);
        //(4) treat the recdata
        double *pointvalue;
        switch (worlddim)
        {
        case 2:
            pointvalue = (double *)(recdata->LineDatas[0]);
            recdataNum = recdata->LineNum;
            edgeindex = recdata->LineIndex;
            break;
        case 3:
            pointvalue = (double *)(recdata->FaceDatas[0]);
            recdataNum = recdata->FaceNum;
            edgeindex = recdata->FaceIndex;
            break;
        }
        for (edgeind = 0; edgeind < recdataNum; edgeind++)
        {
            // 目前边上的NumPointsEdge的积分点个数
            for (pointind = 0; pointind < NumPointsEdge * ValueDim; pointind++)
            {
                EdgePointValue[edgeindex[edgeind] * NumPointsEdge * ValueDim + pointind] += pointvalue[edgeind * NumPointsEdge * ValueDim + pointind];
            }
        }
        //(5) free the data
        RecDataDestroy(&recdata);
        ParaDataDestroy(&paradata);
    }
    // 再对单元进行循环, 计算跳量的积分
    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // printf("ind=%d\n",ind);
        // 对单元的每个边界进行循环
        switch (worlddim)
        {
        case 2:
            NumElemEdges = mesh->Faces[ind].NumLines;
            break;
        case 3:
            NumElemEdges = mesh->Volus[ind].NumFaces;
            break;
        } // 得到了每个单元上的边界条数
        // 对该单元上的Edge进行遍历
        for (eind = 0; eind < NumElemEdges; eind++)
        {
            switch (worlddim)
            { // 找到这个边界在本进程上的编号
            case 2:
                EdgeNum = mesh->Faces[ind].Line4Face[eind];
                edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            case 3:
                EdgeNum = mesh->Volus[ind].Face4Volu[eind];
                edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            }
            if (bdind == 1)
            {
                // 对积分点循环
                for (k = 0; k < NumPointsEdge; k++)
                {
                    weightEdge = QuadratureEdge->QuadW[k]; // 获得积分的权重
                    value = 0.0;
                    for (inddim = 0; inddim < ValueDim; inddim++)
                    {
                        value += EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim] * EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim];
                    }
                    value = 0.5 * value * weightEdge;
                    // value = EdgePointValue[EdgeNum * NumPointsEdge + k];
                    // value = 0.5 * value * value * weightEdge; //
                    PosterioriError[ind] += value;
                    LocalEdgeJump += value;
                } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
            }     // end if(mesh->Lines[EdgeNum].BD_ID==0)
        }         // end for eind对单元的每个边界进行循环
    }             // end for(ind=0;ind<LocalNumElems;ind++)
    MPI_Allreduce(&LocalEdgeJump, &TotalEdgeJump, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
    // OpenPFEM_Print("TotalEdgeJump = %2.10f\n", TotalEdgeJump);
    TotalError[0] = sqrt(TotalResidual + TotalEdgeJump);
    // OpenPFEM_Print("整体的后验误差为 TotalError = %2.10f\n", TotalError[0]);
    // OpenPFEM_Print("Finish Computing PosterioriError!\n");
    OpenPFEM_Free(EdgePointValue);
    OpenPFEM_Free(rhsvalue);
    ElementDestroy(&Elem);
    OpenPFEM_Free(multiindexvaluesOfElem);
    OpenPFEM_Free(multiindexvaluesOfEdge);
    OpenPFEM_Free(BaseValuesOfElem);
    OpenPFEM_Free(BaseValuesOfEdge);
    OpenPFEM_Free(localvalues);
    return;
}

// 2.特征值问题中针对某对特征值和特征向量的后验误差估计
/*输入参数：
    FEMFUNCTION *femfun: 特征值问题中特征值函数
    DOUBLE *eigenvalue: 特征值
    INT NumMultiIndexOfElem: 计算后验误差时单元需要的微分信息的总个数
    INT NumMultiIndexOfEdge: 计算后验误差时边界需要的微分信息的总个数
    MULTIINDEX *MultiIndex: 计算后验误差需要的微分信息 前面部分是单元需要的 后面部分是边界需要的
                            从MultiIndex[0]到MultiIndex[NumMultiIndexOfElem-1]是单元需要的微分信息
                            从MultiIndex[NumMultiIndexOfElem]到MultiIndex[NumMultiIndexOfElem+NumMultiIndexOfEdge-1]是边界需要的微分信息
    ERRORFORM *ElemErrorForm: 单元上的误差计算格式 这里是内部函数的平方形式
    PARTIALERRORFORM *EdgeErrorForm: 边界上的误差计算格式 这里没有进行平方操作 涉及到计算跳量 这里包括了法向量的计算方式
    QUADRATURE *QuadratureElem: 单元上的积分格式
    QUADRATURE *QuadratureEdge: 边界上的积分格式
    DOUBLE *PosterioriErrorOfValue: 特征值的后验误差估计
    DOUBLE *TotalError: 全局的后验误差值\eta=sqrt(\sum \eta_K)
*/
// ****0220修改未检查
// void EigenPosterioriErrorEstimate(FEMFUNCTION *femfun, DOUBLE *eigenvalue, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
//                                   EIGENERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
//                                   QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError)
// {
//     INT ind, LocalNumElems, LocalNumEdges;
//     /** 目前只实现了左右网格是相同的情况 */
//     FEMSPACE *FemSpace = femfun->FEMSpace; // 获得有限元空间的对象
//     MESH *mesh = FemSpace->Mesh;           // 获得有限元函数相对应的网格
//     BASE *Base = FemSpace->Base;
//     INT NumBase = Base->NumBase;   // 基函数的个数
//     INT ValueDim = Base->ValueDim; // 有限元函数的维数
//     INT worlddim = mesh->worlddim;
//     INT *LocalIndex = FemSpace->LocalIndex;
//     INT *BeginIndex = FemSpace->BeginIndex;
//     INT *localdof;                                          // 用来记录每个单元上的有限元函数系数
//     DOUBLE *FEMValues = femfun->Values;                     // 有限元函数的系数值
//     DOUBLE *localvalues = malloc(NumBase * sizeof(DOUBLE)); // 基函数个数长度的实数数组
//     ELEMENT *Elem;                                          // 有限单元的对象
//     INT NumPoints = QuadratureElem->NumPoints;              // 单元上的积分点个数
//     INT NumPointsEdge = QuadratureEdge->NumPoints;          // 在边界上的积分点个数
//     INT i, j, k, tmpind, indindex, inddim, EdgeNum, NumElemEdges, eind, edge_BDID, bdind;
//     DOUBLE *BaseValuesOfElem = malloc(NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
//     DOUBLE *BaseValuesOfEdge = malloc(NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
//     DOUBLE *multiindexvaluesOfElem = malloc(NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
//     DOUBLE *multiindexvaluesOfEdge = malloc(NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
//     DOUBLE value, weight, weightEdge, xi, eta, zeta;
//     DOUBLE *value_vec = malloc(ValueDim * sizeof(DOUBLE));
//     DOUBLE Coord[worlddim], RefCoord[worlddim]; // 记录实际坐标和参考单元上的坐标
//     // EdgeCoord: 用来存储每个单元上低一维面（线）上的坐标, EdgeRefCoord: 用来存储在标准单元某一个积分点的坐标
//     DOUBLE EdgeCoord[worlddim * worlddim], EdgeRefCoord[worlddim];
//     VERT vert;
//     MULTIINDEX NoGradIndex;

//     // 进行与空间维数相关的运算和操作
//     switch (worlddim)
//     {
//     case 2:
//         NoGradIndex = D00;
//         LocalNumElems = mesh->num_face;
//         LocalNumEdges = mesh->num_line;
//         // 初始化相应的有限元单元
//         Elem = ElementInitial(mesh);
//         break;
//     case 3:
//         NoGradIndex = D000;
//         // get the number of elements
//         LocalNumElems = mesh->num_volu;
//         LocalNumEdges = mesh->num_face;
//         // Initialization an element
//         Elem = ElementInitial(mesh);
//         break;
//     }
//     // 判断是否需要计算Jacobi逆矩阵
//     tmpind = 0;
//     for (i = 0; i < NumMultiIndexOfElem; i++)
//     { // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
//         if (MultiIndex[i] > NoGradIndex)
//         {
//             tmpind = 1;
//             break;
//             // i = NumMultiIndex + 3;
//         } // end for if
//     }     // end for i
//     if (tmpind == 0)
//     {
//         for (i = NumMultiIndexOfElem; i < NumMultiIndexOfElem + NumMultiIndexOfEdge; i++)
//         { // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
//             if (MultiIndex[i] > NoGradIndex)
//             {
//                 tmpind = 1;
//                 break;
//                 // i = NumMultiIndex + 3;
//             } // end for if
//         }     // end for i
//     }
//     // 每个单元对应一个后验误差值
//     memset(PosterioriError, 0.0, LocalNumElems * sizeof(DOUBLE));
//     // 每个单元的每个Edge(每个面)对应一个法向量的值(目前只针对单纯形)
//     DOUBLE NormalVec[(worlddim + 1) * worlddim]; // 3*2 或者 4*3
//     // 存储单元边界的直径h(2d时是边的长度,3d时是面的外接圆直径), 边界面积S(2d时是边的长度,3d时是面的面积),单元直径(2d时是外接圆直径,3d时是体的外接球直径)
//     DOUBLE hEdge[worlddim + 1], SEdge[worlddim + 1], hElem; // 每个Edge的半径和面积
//     // 存储边界上用于计算跳量的积分点的信息，每条边界存储在积分点上的外法向导数值, NumPointsEdge：每个单元边界上积分点个数
//     DOUBLE *EdgePointValue = malloc(LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
//     memset(EdgePointValue, 0.0, LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
//     // 定义存储进程局部和全局的单元残差量和边的跳跃量
//     DOUBLE LocalResidual = 0.0, TotalResidual = 0.0, LocalEdgeJump = 0.0, TotalEdgeJump = 0.0, TotalPostErr = 0.0;
//     DOUBLE SqrthTimess; // 临时记录sqrt(hEdge[eind] * SEdge[eind])
//     // 接下来进行单元循环, 计算每个单元上单元残差量和Edge上的外法向导数

//     // 增加利用变分形式判断单元类型
//     SetElemType(Elem, FemSpace);
//     if (Elem->is_curl == 1)
//         tmpind = 1;

//     for (ind = 0; ind < LocalNumElems; ind++)
//     {
//         // 建立当前的有限单元
//         ElementBuild(mesh, ind, Elem);
//         // 计算单元的体积和Jacobian矩阵
//         if (tmpind == 0)
//         {
//             ComputeElementJacobian(Elem);
//         }
//         else
//         {
//             ComputeElementInvJacobian(Elem);
//         }
//         // 首先计算单元上的残差量, 计算单元的其他信息：Edge的直径和Elem的直径
//         ComputeElementInfo(Elem, hEdge, SEdge, &hElem);

//         // 获得当前单元上的有限元函数值
//         localdof = LocalIndex + BeginIndex[ind];
//         // 得到当前单元有限元函数的系数
//         for (i = 0; i < NumBase; i++)
//         {
//             localvalues[i] = FEMValues[localdof[i]];
//         }
//         // 调换基函数对应自由度取值的顺序
//         if (Elem->is_curl == 1)
//         {
//             ReorderFEMValuesForCurlElem(FemSpace, ind, Elem, localvalues);
//         }

//         // 对单元上积分点循环, 求得本单元上残差量
//         for (k = 0; k < NumPoints; k++)
//         {
//             switch (worlddim)
//             {
//             case 2:
//                 // 获得积分点坐标,参考单元上的坐标
//                 RefCoord[0] = QuadratureElem->QuadX[k];
//                 RefCoord[1] = QuadratureElem->QuadY[k];
//                 weight = QuadratureElem->QuadW[k];
//                 break;
//             case 3:
//                 // 获得积分点坐标,参考单元上的坐标
//                 RefCoord[0] = QuadratureElem->QuadX[k];
//                 RefCoord[1] = QuadratureElem->QuadY[k];
//                 RefCoord[2] = QuadratureElem->QuadZ[k];
//                 weight = QuadratureElem->QuadW[k];
//                 break;
//             }
//             ElementRefCoord2Coord(Elem, RefCoord, Coord); // 得到与积分点相应的实际坐标
//             // 计算基函数微分的值，根据基函数求解相应的基函数值
//             memset(BaseValuesOfElem, 0.0, NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
//             // 按照导数的方式来排列，每一个导数值的维数: NumBase * ValueDim
//             for (j = 0; j < NumMultiIndexOfElem; j++)
//             {
//                 GetBaseValues(Base, MultiIndex[j], Elem, Coord, RefCoord, BaseValuesOfElem + j * NumBase * ValueDim);
//             }
//             // 调整顺序得到每个基函数在当前积分点上所有微分的值: 维数: NumMultiIndex*ValueDim
//             memset(multiindexvaluesOfElem, 0.0, NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE));
//             for (indindex = 0; indindex < NumMultiIndexOfElem; indindex++)
//             {
//                 for (i = 0; i < NumBase; i++)
//                 {
//                     for (inddim = 0; inddim < ValueDim; inddim++)
//                     {
//                         multiindexvaluesOfElem[indindex * ValueDim + inddim] +=
//                             localvalues[i] * BaseValuesOfElem[indindex * NumBase * ValueDim + i * ValueDim + inddim];
//                     } // end for inddim
//                 }     // end for i
//             }         // end for indindex
//             // 计算(\lambda u-\laplace u)^2在积分点处的值, 用户可以自由定义
//             ElemErrorForm(eigenvalue[0], multiindexvaluesOfElem, Coord, &value);
//             // 计算后验误差中的 单元部分的总和
//             // OpenPFEM_Print("hElem: %f, value: %f, weight: %2.14f,  Volumn: %2.14f\n", hElem, value, weight, Elem->Volumn);
//             PosterioriError[ind] += value * weight;
//             // PosterioriError[ind] += hElem * hElem * value * weight * Elem->Volumn;
//         }                                                     // end for(k=0;k<NumPoints;k++)//对积分点的循环结束
//         PosterioriError[ind] *= hElem * hElem * Elem->Volumn; // 乘上相应的h_K^2*|K|
//         LocalResidual += PosterioriError[ind];                // 将本单元的残差加到总的残差上去
//         // 下面来计算单元上每个Edge上的跳量, Edge上的每个积分点都需要进行存储值
//         // 对单元的Edge进行循环, 计算Edge上积分点的导数值和外法向的值
//         switch (worlddim)
//         {
//         case 2:
//             NumElemEdges = mesh->Faces[ind].NumLines;
//             break;
//         case 3:
//             NumElemEdges = mesh->Volus[ind].NumFaces;
//             break;
//         }                                         // 得到了每个单元上的边界条数
//         ComputeElementNormalVec(Elem, NormalVec); // 计算目前单元每个边界的外法向量
//         // 对本单元的Edge进行遍历
//         for (eind = 0; eind < NumElemEdges; eind++)
//         {
//             SqrthTimess = sqrt(hEdge[eind] * SEdge[eind]);
//             switch (worlddim)
//             {
//             case 2:
//                 // EdgeNum表示这条边在本进程上的编号
//                 EdgeNum = mesh->Faces[ind].Line4Face[eind];
//                 edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
//                 // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
//                 if (FemSpace->BoundType(edge_BDID) != INNER)
//                 {
//                     bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
//                 }
//                 else
//                 {
//                     bdind = 1; // 表示需要计算边界跳跃项
//                 }
//                 break;
//             case 3:
//                 // EdgeNum表示这个面在本进程上的编号
//                 EdgeNum = mesh->Volus[ind].Face4Volu[eind];
//                 edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
//                 // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
//                 if (FemSpace->BoundType(edge_BDID) != INNER)
//                 {
//                     bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
//                 }
//                 else
//                 {
//                     bdind = 1; // 表示需要计算边界跳跃项
//                 }
//                 break;
//             }
//             if (bdind == 1)
//             {
//                 FindEdgeCoord(mesh, ind, eind, EdgeCoord); // EdgeCoord存储了当前边界节点的参考坐标
//                 for (k = 0; k < NumPointsEdge; k++)
//                 { // 根据积分格式，找到积分点在标准单元上的坐标
//                     switch (worlddim)
//                     {
//                     case 2:
//                         // 获得积分点在参考单元上的坐标
//                         xi = QuadratureEdge->QuadX[k];
//                         for (i = 0; i < worlddim; i++)
//                         {
//                             EdgeRefCoord[i] = (1.0 - xi) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i];
//                         }
//                         break;
//                     case 3:
//                         // 获得积分点在参考单元上的坐标
//                         xi = QuadratureEdge->QuadX[k];
//                         eta = QuadratureEdge->QuadY[k];
//                         for (i = 0; i < worlddim; i++)
//                         {
//                             EdgeRefCoord[i] = (1 - xi - eta) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i] + eta * EdgeCoord[2 * worlddim + i];
//                         }
//                         break;
//                     }
//                     // weightEdge = QuadratureEdge->QuadW[k]; //获得积分的权重
//                     ElementRefCoord2Coord(Elem, EdgeRefCoord, Coord);
//                     // 计算基函数微分的值，为组装做准备,根据基函数求解相应的基函数值
//                     memset(BaseValuesOfEdge, 0.0, NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
//                     for (j = 0; j < NumMultiIndexOfEdge; j++)
//                     {
//                         GetBaseValues(Base, MultiIndex[NumMultiIndexOfElem + j], Elem, Coord, EdgeRefCoord, BaseValuesOfEdge + j * NumBase * ValueDim);
//                     }
//                     // 线性组装出在该积分点上所有微分的值
//                     memset(multiindexvaluesOfEdge, 0.0, NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE));
//                     for (indindex = 0; indindex < NumMultiIndexOfEdge; indindex++)
//                     {
//                         for (i = 0; i < NumBase; i++)
//                         {
//                             for (inddim = 0; inddim < ValueDim; inddim++)
//                             {
//                                 multiindexvaluesOfEdge[indindex * ValueDim + inddim] +=
//                                     localvalues[i] * BaseValuesOfEdge[indindex * NumBase * ValueDim + i * ValueDim + inddim];
//                             } // end for inddim
//                         }     // end for i
//                     }         // end for indindex
//                     EdgeErrorForm(multiindexvaluesOfEdge, NULL, NormalVec + eind * worlddim, value_vec);
//                     for (inddim = 0; inddim < ValueDim; inddim++)
//                     {
//                         value_vec[inddim] = value_vec[inddim] * SqrthTimess;
//                         EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim] += value_vec[inddim];
//                     }

//                     // value = value * sqrt(hEdge[eind] * SEdge[eind]);

//                     // EdgePointValue[EdgeNum * NumPointsEdge + k] += value;
//                 } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
//                 // FaceAddTime[EdgeNum]++;
//             }
//         } // end for eind: 对单元的每个边界进行循环
//     }     // end for(ind=0;ind<LocalNumElems;ind++)//对单元进行循环
//     // 计算单元部分的后验误差和
//     MPI_Allreduce(&LocalResidual, &TotalResidual, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
//     OpenPFEM_Print("TotalResidual = %2.10f\n", TotalResidual);
//     // 对跳量所需要的信息进行传输: 现在要传输：边上的NumPointsEdge个DOUBLE
//     INT edgeind, pointind, recdataNum, *edgeindex;
//     INT nprocs;
//     MPI_Comm_size(mesh->comm, &nprocs);
//     if (nprocs > 1)
//     {
//         //(1) paradata creation
//         PARADATA *paradata = NULL;
//         ParaDataCreate(&paradata, mesh);
//         //(2) paradata attachment (注意这里add的顺序将是返回数据里的顺序)switch (worlddim)
//         switch (worlddim)
//         {
//         case 2:
//             ParaDataAdd(paradata, LINEDATA, NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
//             break;
//         case 3:
//             ParaDataAdd(paradata, FACEDATA, NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
//             break;
//         }
//         //(3) communication
//         RECDATA *recdata = NULL;
//         ParaDataCommunicate(paradata, &recdata);
//         //(4) treat the recdata
//         double *pointvalue;
//         switch (worlddim)
//         {
//         case 2:
//             pointvalue = (double *)(recdata->LineDatas[0]);
//             recdataNum = recdata->LineNum;
//             edgeindex = recdata->LineIndex;
//             break;
//         case 3:
//             pointvalue = (double *)(recdata->FaceDatas[0]);
//             recdataNum = recdata->FaceNum;
//             edgeindex = recdata->FaceIndex;
//             break;
//         }
//         for (edgeind = 0; edgeind < recdataNum; edgeind++)
//         {
//             for (pointind = 0; pointind < NumPointsEdge * ValueDim; pointind++)
//             {
//                 EdgePointValue[edgeindex[edgeind] * NumPointsEdge * ValueDim + pointind] += pointvalue[edgeind * NumPointsEdge * ValueDim + pointind];
//             }
//         }
//         //(5) free the data
//         RecDataDestroy(&recdata);
//         ParaDataDestroy(&paradata);
//     }
//     // 再对单元进行循环, 计算跳量的积分
//     for (ind = 0; ind < LocalNumElems; ind++)
//     {
//         // printf("ind=%d\n",ind);
//         // 对单元的每个边界进行循环
//         switch (worlddim)
//         {
//         case 2:
//             NumElemEdges = mesh->Faces[ind].NumLines;
//             break;
//         case 3:
//             NumElemEdges = mesh->Volus[ind].NumFaces;
//             break;
//         } // 得到了每个单元上的边界条数
//         // 对该单元上的Edge进行遍历
//         for (eind = 0; eind < NumElemEdges; eind++)
//         {
//             switch (worlddim)
//             { // 找到这个边界在本进程上的编号
//             case 2:
//                 EdgeNum = mesh->Faces[ind].Line4Face[eind];
//                 edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
//                 // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
//                 if (FemSpace->BoundType(edge_BDID) != INNER)
//                 {
//                     bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
//                 }
//                 else
//                 {
//                     bdind = 1; // 表示需要计算边界跳跃项
//                 }
//                 break;
//             case 3:
//                 EdgeNum = mesh->Volus[ind].Face4Volu[eind];
//                 edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
//                 // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
//                 if (FemSpace->BoundType(edge_BDID) != INNER)
//                 {
//                     bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
//                 }
//                 else
//                 {
//                     bdind = 1; // 表示需要计算边界跳跃项
//                 }
//                 break;
//             }
//             if (bdind == 1)
//             { // 对积分点循环
//                 for (k = 0; k < NumPointsEdge; k++)
//                 {
//                     weightEdge = QuadratureEdge->QuadW[k]; // 获得积分的权重
//                     value = 0.0;
//                     for (inddim = 0; inddim < ValueDim; inddim++)
//                     {
//                         value += EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim] * EdgePointValue[(EdgeNum * NumPointsEdge + k) * ValueDim + inddim];
//                     }
//                     value = 0.5 * value * weightEdge;
//                     // value = EdgePointValue[EdgeNum * NumPointsEdge + k];
//                     // if (FaceAddTime[EdgeNum] == 2)
//                     // {
//                     //     if (fabs(value) > 10e-12)
//                     //         OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d 面%d 积分点%d 值为%2.10f\n", ind, eind, k, value);
//                     // }
//                     // value = value * value * weightEdge;
//                     PosterioriError[ind] += value;
//                     LocalEdgeJump += value;
//                 } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
//             }     // end if(mesh->Lines[EdgeNum].BD_ID==0)
//         }         // end for eind对单元的每个边界进行循环
//     }             // end for(ind=0;ind<LocalNumElems;ind++)
//     MPI_Allreduce(&LocalEdgeJump, &TotalEdgeJump, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
//     // OpenPFEM_Print("TotalEdgeJump = %2.10f\n", TotalEdgeJump);
//     TotalError[0] = sqrt(TotalResidual + TotalEdgeJump);
//     // OpenPFEM_Print("整体的后验误差为 TotalError = %2.10f\n", TotalError[0]);
//     // OpenPFEM_Print("Finish Computing PosterioriError!\n");
//     OpenPFEM_Free(EdgePointValue);
//     ElementDestroy(&Elem);
//     OpenPFEM_Free(multiindexvaluesOfElem);
//     OpenPFEM_Free(multiindexvaluesOfEdge);
//     OpenPFEM_Free(BaseValuesOfElem);
//     OpenPFEM_Free(BaseValuesOfEdge);
//     OpenPFEM_Free(localvalues);
//     return;
// }
// 这里是对femfun中的所有特征对进行后验误差估计
void EigenPosterioriErrorEstimate(FEMFUNCTION *femfun, DOUBLE *eigenvalue, INT NumMultiIndexOfElem, INT NumMultiIndexOfEdge, MULTIINDEX *MultiIndex,
                                  EIGENERRORFORM *ElemErrorForm, PARTIALERRORFORM *EdgeErrorForm, QUADRATURE *QuadratureElem,
                                  QUADRATURE *QuadratureEdge, DOUBLE *PosterioriError, DOUBLE *TotalError)
{
    INT num_fun = femfun->num, LocalNumDOF = femfun->LocalNumDOF, ind_fun;
#if PRINT_INFO
    OpenPFEM_Print("计算 %d -> %d %d个特征对的后验误差估计\n", femfun->start, femfun->end - 1, num_fun);
#endif
    INT ind, LocalNumElems, LocalNumEdges;
    /** 目前只实现了左右网格是相同的情况 */
    FEMSPACE *FemSpace = femfun->FEMSpace; // 获得有限元空间的对象
    MESH *mesh = FemSpace->Mesh;           // 获得有限元函数相对应的网格
    BASE *Base = FemSpace->Base;
    INT NumBase = Base->NumBase;   // 基函数的个数
    INT ValueDim = Base->ValueDim; // 有限元函数的维数
    INT worlddim = mesh->worlddim;
    INT *LocalIndex = FemSpace->LocalIndex;
    INT *BeginIndex = FemSpace->BeginIndex;
    INT *localdof;                                                    // 用来记录每个单元上的有限元函数系数
    DOUBLE *FEMValues = femfun->Values + femfun->start * LocalNumDOF; // 有限元函数的系数值
    DOUBLE *localvalues = malloc(num_fun * NumBase * sizeof(DOUBLE)); // 基函数个数长度的实数数组
    ELEMENT *Elem;                                                    // 有限单元的对象
    INT NumPoints = QuadratureElem->NumPoints;                        // 单元上的积分点个数
    INT NumPointsEdge = QuadratureEdge->NumPoints;                    // 在边界上的积分点个数
    INT i, j, k, tmpind, indindex, inddim, EdgeNum, NumElemEdges, eind, edge_BDID, bdind;
    DOUBLE *BaseValuesOfElem = malloc(NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
    DOUBLE *BaseValuesOfEdge = malloc(NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
    DOUBLE *multiindexvaluesOfElem = malloc(NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
    DOUBLE *multiindexvaluesOfEdge = malloc(NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE)); // 某一种导数的值
    DOUBLE value, weight, weightEdge, xi, eta, zeta;
    DOUBLE *value_vec = malloc(ValueDim * sizeof(DOUBLE));
    DOUBLE Coord[worlddim], RefCoord[worlddim]; // 记录实际坐标和参考单元上的坐标
    // EdgeCoord: 用来存储每个单元上低一维面（线）上的坐标, EdgeRefCoord: 用来存储在标准单元某一个积分点的坐标
    DOUBLE EdgeCoord[worlddim * worlddim], EdgeRefCoord[worlddim];
    VERT vert;
    MULTIINDEX NoGradIndex;

    // 进行与空间维数相关的运算和操作
    switch (worlddim)
    {
    case 2:
        NoGradIndex = D00;
        LocalNumElems = mesh->num_face;
        LocalNumEdges = mesh->num_line;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    case 3:
        NoGradIndex = D000;
        // get the number of elements
        LocalNumElems = mesh->num_volu;
        LocalNumEdges = mesh->num_face;
        // Initialization an element
        Elem = ElementInitial(mesh);
        break;
    }
    // 判断是否需要计算Jacobi逆矩阵
    tmpind = 0;
    for (i = 0; i < NumMultiIndexOfElem; i++)
    { // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
        if (MultiIndex[i] > NoGradIndex)
        {
            tmpind = 1;
            break;
            // i = NumMultiIndex + 3;
        } // end for if
    }     // end for i
    if (tmpind == 0)
    {
        for (i = NumMultiIndexOfElem; i < NumMultiIndexOfElem + NumMultiIndexOfEdge; i++)
        { // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
            if (MultiIndex[i] > NoGradIndex)
            {
                tmpind = 1;
                break;
                // i = NumMultiIndex + 3;
            } // end for if
        }     // end for i
    }
    // 每个单元对应一个后验误差值
    memset(PosterioriError, 0.0, LocalNumElems * sizeof(DOUBLE));
    // 每个单元的每个Edge(每个面)对应一个法向量的值(目前只针对单纯形)
    DOUBLE NormalVec[(worlddim + 1) * worlddim]; // 3*2 或者 4*3
    // 存储单元边界的直径h(2d时是边的长度,3d时是面的外接圆直径), 边界面积S(2d时是边的长度,3d时是面的面积),单元直径(2d时是外接圆直径,3d时是体的外接球直径)
    DOUBLE hEdge[worlddim + 1], SEdge[worlddim + 1], hElem; // 每个Edge的半径和面积
    // 存储边界上用于计算跳量的积分点的信息，每条边界存储在积分点上的外法向导数值, NumPointsEdge：每个单元边界上积分点个数
    DOUBLE *EdgePointValue = malloc(num_fun * LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
    memset(EdgePointValue, 0.0, num_fun * LocalNumEdges * NumPointsEdge * ValueDim * sizeof(DOUBLE));
    // 定义存储进程局部和全局的单元残差量和边的跳跃量
    DOUBLE LocalResidual = 0.0, TotalResidual = 0.0, LocalEdgeJump = 0.0, TotalEdgeJump = 0.0, TotalPostErr = 0.0;
    DOUBLE SqrthTimess; // 临时记录sqrt(hEdge[eind] * SEdge[eind])
    // 接下来进行单元循环, 计算每个单元上单元残差量和Edge上的外法向导数

    // 增加利用变分形式判断单元类型
    SetElemType(Elem, FemSpace);
    if (Elem->is_curl == 1)
        tmpind = 1;

    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // 建立当前的有限单元
        ElementBuild(mesh, ind, Elem);
        // 计算单元的体积和Jacobian矩阵
        if (tmpind == 0)
        {
            ComputeElementJacobian(Elem);
        }
        else
        {
            ComputeElementInvJacobian(Elem);
        }
        // 首先计算单元上的残差量, 计算单元的其他信息：Edge的直径和Elem的直径
        ComputeElementInfo(Elem, hEdge, SEdge, &hElem);

        // 获得当前单元上的有限元函数值
        localdof = LocalIndex + BeginIndex[ind];
        // 得到当前单元有限元函数的系数
        for (ind_fun = 0; ind_fun < num_fun; ind_fun++)
        {
            for (i = 0; i < NumBase; i++)
            {
                localvalues[ind_fun * NumBase + i] = FEMValues[ind_fun * LocalNumDOF + localdof[i]];
            }
            // 调换基函数对应自由度取值的顺序
            if (Elem->is_curl == 1)
            {
                ReorderFEMValuesForCurlElem(FemSpace, ind, Elem, localvalues + ind_fun * NumBase);
            }
        }

        // 对单元上积分点循环, 求得本单元上残差量
        for (k = 0; k < NumPoints; k++)
        {
            switch (worlddim)
            {
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = QuadratureElem->QuadX[k];
                RefCoord[1] = QuadratureElem->QuadY[k];
                weight = QuadratureElem->QuadW[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = QuadratureElem->QuadX[k];
                RefCoord[1] = QuadratureElem->QuadY[k];
                RefCoord[2] = QuadratureElem->QuadZ[k];
                weight = QuadratureElem->QuadW[k];
                break;
            }
            ElementRefCoord2Coord(Elem, RefCoord, Coord); // 得到与积分点相应的实际坐标
            // 计算基函数微分的值，根据基函数求解相应的基函数值
            memset(BaseValuesOfElem, 0.0, NumBase * ValueDim * NumMultiIndexOfElem * sizeof(DOUBLE));
            // 按照导数的方式来排列，每一个导数值的维数: NumBase * ValueDim
            for (j = 0; j < NumMultiIndexOfElem; j++)
            {
                GetBaseValues(Base, MultiIndex[j], Elem, Coord, RefCoord, BaseValuesOfElem + j * NumBase * ValueDim);
            }
            // 调整顺序得到每个基函数在当前积分点上所有微分的值: 维数: NumMultiIndex*ValueDim
            for (ind_fun = 0; ind_fun < num_fun; ind_fun++)
            {
                memset(multiindexvaluesOfElem, 0.0, NumMultiIndexOfElem * ValueDim * sizeof(DOUBLE));
                for (indindex = 0; indindex < NumMultiIndexOfElem; indindex++)
                {
                    for (i = 0; i < NumBase; i++)
                    {
                        for (inddim = 0; inddim < ValueDim; inddim++)
                        {
                            multiindexvaluesOfElem[indindex * ValueDim + inddim] +=
                                localvalues[ind_fun * NumBase + i] * BaseValuesOfElem[indindex * NumBase * ValueDim + i * ValueDim + inddim];
                        } // end for inddim
                    }     // end for i
                }         // end for indindex
                // 计算(\lambda u-\laplace u)^2在积分点处的值, 用户可以自由定义
                ElemErrorForm(eigenvalue[ind_fun], multiindexvaluesOfElem, Coord, &value);
                // 计算后验误差中的 单元部分的总和
                // OpenPFEM_Print("hElem: %f, value: %f, weight: %2.14f,  Volumn: %2.14f\n", hElem, value, weight, Elem->Volumn);
                PosterioriError[ind] += value * weight;
                // PosterioriError[ind] += hElem * hElem * value * weight * Elem->Volumn;
            }
        }                                                     // end for(k=0;k<NumPoints;k++)//对积分点的循环结束
        PosterioriError[ind] *= hElem * hElem * Elem->Volumn; // 乘上相应的h_K^2*|K|
        LocalResidual += PosterioriError[ind];                // 将本单元的残差加到总的残差上去
        // 下面来计算单元上每个Edge上的跳量, Edge上的每个积分点都需要进行存储值
        // 对单元的Edge进行循环, 计算Edge上积分点的导数值和外法向的值
        switch (worlddim)
        {
        case 2:
            NumElemEdges = mesh->Faces[ind].NumLines;
            break;
        case 3:
            NumElemEdges = mesh->Volus[ind].NumFaces;
            break;
        }                                         // 得到了每个单元上的边界条数
        ComputeElementNormalVec(Elem, NormalVec); // 计算目前单元每个边界的外法向量
        // 对本单元的Edge进行遍历
        for (eind = 0; eind < NumElemEdges; eind++)
        {
            SqrthTimess = sqrt(hEdge[eind] * SEdge[eind]);
            switch (worlddim)
            {
            case 2:
                // EdgeNum表示这条边在本进程上的编号
                EdgeNum = mesh->Faces[ind].Line4Face[eind];
                edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            case 3:
                // EdgeNum表示这个面在本进程上的编号
                EdgeNum = mesh->Volus[ind].Face4Volu[eind];
                edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            }
            if (bdind == 1)
            {
                FindEdgeCoord(mesh, ind, eind, EdgeCoord); // EdgeCoord存储了当前边界节点的参考坐标
                for (k = 0; k < NumPointsEdge; k++)
                { // 根据积分格式，找到积分点在标准单元上的坐标
                    switch (worlddim)
                    {
                    case 2:
                        // 获得积分点在参考单元上的坐标
                        xi = QuadratureEdge->QuadX[k];
                        for (i = 0; i < worlddim; i++)
                        {
                            EdgeRefCoord[i] = (1.0 - xi) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i];
                        }
                        break;
                    case 3:
                        // 获得积分点在参考单元上的坐标
                        xi = QuadratureEdge->QuadX[k];
                        eta = QuadratureEdge->QuadY[k];
                        for (i = 0; i < worlddim; i++)
                        {
                            EdgeRefCoord[i] = (1 - xi - eta) * EdgeCoord[0 * worlddim + i] + xi * EdgeCoord[1 * worlddim + i] + eta * EdgeCoord[2 * worlddim + i];
                        }
                        break;
                    }
                    // weightEdge = QuadratureEdge->QuadW[k]; //获得积分的权重
                    ElementRefCoord2Coord(Elem, EdgeRefCoord, Coord);
                    // 计算基函数微分的值，为组装做准备,根据基函数求解相应的基函数值
                    memset(BaseValuesOfEdge, 0.0, NumBase * ValueDim * NumMultiIndexOfEdge * sizeof(DOUBLE));
                    for (j = 0; j < NumMultiIndexOfEdge; j++)
                    {
                        GetBaseValues(Base, MultiIndex[NumMultiIndexOfElem + j], Elem, Coord, EdgeRefCoord, BaseValuesOfEdge + j * NumBase * ValueDim);
                    }
                    // 线性组装出在该积分点上所有微分的值
                    for (ind_fun = 0; ind_fun < num_fun; ind_fun++)
                    {
                        memset(multiindexvaluesOfEdge, 0.0, NumMultiIndexOfEdge * ValueDim * sizeof(DOUBLE));
                        for (indindex = 0; indindex < NumMultiIndexOfEdge; indindex++)
                        {
                            for (i = 0; i < NumBase; i++)
                            {
                                for (inddim = 0; inddim < ValueDim; inddim++)
                                {
                                    multiindexvaluesOfEdge[indindex * ValueDim + inddim] +=
                                        localvalues[ind_fun * NumBase + i] * BaseValuesOfEdge[indindex * NumBase * ValueDim + i * ValueDim + inddim];
                                } // end for inddim
                            }     // end for i
                        }         // end for indindex
                        EdgeErrorForm(multiindexvaluesOfEdge, NULL, NormalVec + eind * worlddim, value_vec);
                        for (inddim = 0; inddim < ValueDim; inddim++)
                        {
                            value_vec[inddim] = value_vec[inddim] * SqrthTimess;
                            EdgePointValue[((EdgeNum * num_fun + ind_fun) * NumPointsEdge + k) * ValueDim + inddim] += value_vec[inddim];
                            // EdgePointValue[ind_fun * LocalNumEdges * NumPointsEdge * ValueDim + (EdgeNum * NumPointsEdge + k) * ValueDim + inddim] += value_vec[inddim];
                        }
                    }

                    // value = value * sqrt(hEdge[eind] * SEdge[eind]);

                    // EdgePointValue[EdgeNum * NumPointsEdge + k] += value;
                } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
                // FaceAddTime[EdgeNum]++;
            }
        } // end for eind: 对单元的每个边界进行循环
    }     // end for(ind=0;ind<LocalNumElems;ind++)//对单元进行循环
    // 计算单元部分的后验误差和
    MPI_Allreduce(&LocalResidual, &TotalResidual, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
#if PRINT_INFO
    OpenPFEM_Print("TotalResidual = %2.10f\n", TotalResidual);
#endif
    // 对跳量所需要的信息进行传输: 现在要传输：边上的NumPointsEdge个DOUBLE
    INT edgeind, pointind, recdataNum, *edgeindex;
    INT nprocs;
    MPI_Comm_size(mesh->comm, &nprocs);
    if (nprocs > 1)
    {
        //(1) paradata creation
        PARADATA *paradata = NULL;
        ParaDataCreate(&paradata, mesh);
#if PRINT_INFO
        MPI_Barrier(MPI_COMM_WORLD);
        OpenPFEM_Print("end1\n");
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        //(2) paradata attachment (注意这里add的顺序将是返回数据里的顺序)switch (worlddim)
        switch (worlddim)
        {
        case 2:
            ParaDataAdd(paradata, LINEDATA, num_fun * NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
            break;
        case 3:
            ParaDataAdd(paradata, FACEDATA, num_fun * NumPointsEdge * ValueDim, MPI_DOUBLE, (void *)EdgePointValue);
            break;
        }
#if PRINT_INFO
        MPI_Barrier(MPI_COMM_WORLD);
        OpenPFEM_Print("end2\n");
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        //(3) communication
        RECDATA *recdata = NULL;
        ParaDataCommunicate(paradata, &recdata);
#if PRINT_INFO
        MPI_Barrier(MPI_COMM_WORLD);
        OpenPFEM_Print("end3\n");
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        //(4) treat the recdata
        double *pointvalue;
        switch (worlddim)
        {
        case 2:
            pointvalue = (double *)(recdata->LineDatas[0]);
            recdataNum = recdata->LineNum;
            edgeindex = recdata->LineIndex;
            break;
        case 3:
            pointvalue = (double *)(recdata->FaceDatas[0]);
            recdataNum = recdata->FaceNum;
            edgeindex = recdata->FaceIndex;
            break;
        }
#if PRINT_INFO
        MPI_Barrier(MPI_COMM_WORLD);
        OpenPFEM_Print("end4 -- middle\n");
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        for (edgeind = 0; edgeind < recdataNum; edgeind++)
        {
            for (pointind = 0; pointind < num_fun * NumPointsEdge * ValueDim; pointind++)
            {
                EdgePointValue[edgeindex[edgeind] * num_fun * NumPointsEdge * ValueDim + pointind] += pointvalue[edgeind * num_fun * NumPointsEdge * ValueDim + pointind];
            }
        }
#if PRINT_INFO
        MPI_Barrier(MPI_COMM_WORLD);
        OpenPFEM_Print("end4\n");
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        //(5) free the data
        RecDataDestroy(&recdata);
        ParaDataDestroy(&paradata);
    }
#if PRINT_INFO
    printf("结束信息传递\n");
#endif
    // 再对单元进行循环, 计算跳量的积分
    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // printf("ind=%d\n",ind);
        // 对单元的每个边界进行循环
        switch (worlddim)
        {
        case 2:
            NumElemEdges = mesh->Faces[ind].NumLines;
            break;
        case 3:
            NumElemEdges = mesh->Volus[ind].NumFaces;
            break;
        } // 得到了每个单元上的边界条数
        // 对该单元上的Edge进行遍历
        for (eind = 0; eind < NumElemEdges; eind++)
        {
            switch (worlddim)
            { // 找到这个边界在本进程上的编号
            case 2:
                EdgeNum = mesh->Faces[ind].Line4Face[eind];
                edge_BDID = mesh->Lines[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            case 3:
                EdgeNum = mesh->Volus[ind].Face4Volu[eind];
                edge_BDID = mesh->Faces[EdgeNum].BD_ID; // 获得目前边的边界条件
                // if (FemSpace->BoundType(edge_BDID) == DIRICHLET)
                if (FemSpace->BoundType(edge_BDID) != INNER)
                {
                    bdind = 0; // DIRICHLET边界条件就不需要处理计算边界跳跃项
                }
                else
                {
                    bdind = 1; // 表示需要计算边界跳跃项
                }
                break;
            }
            if (bdind == 1)
            { // 对积分点循环
                for (k = 0; k < NumPointsEdge; k++)
                {
                    weightEdge = QuadratureEdge->QuadW[k]; // 获得积分的权重
                    for (ind_fun = 0; ind_fun < num_fun; ind_fun++)
                    {
                        value = 0.0;
                        for (inddim = 0; inddim < ValueDim; inddim++)
                        {
                            value += EdgePointValue[((EdgeNum * num_fun + ind_fun) * NumPointsEdge + k) * ValueDim + inddim] * EdgePointValue[((EdgeNum * num_fun + ind_fun) * NumPointsEdge + k) * ValueDim + inddim];
                        }
                        value = 0.5 * value * weightEdge;
                        // value = EdgePointValue[EdgeNum * NumPointsEdge + k];
                        // if (FaceAddTime[EdgeNum] == 2)
                        // {
                        //     if (fabs(value) > 10e-12)
                        //         OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d 面%d 积分点%d 值为%2.10f\n", ind, eind, k, value);
                        // }
                        // value = value * value * weightEdge;
                        PosterioriError[ind] += value;
                        LocalEdgeJump += value;
                    }
                } // end for(k=0;k<NumPointsEdge;k++)//对积分点循环
            }     // end if(mesh->Lines[EdgeNum].BD_ID==0)
        }         // end for eind对单元的每个边界进行循环
    }             // end for(ind=0;ind<LocalNumElems;ind++)
    MPI_Allreduce(&LocalEdgeJump, &TotalEdgeJump, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
    // OpenPFEM_Print("TotalEdgeJump = %2.10f\n", TotalEdgeJump);
    TotalError[0] = sqrt(TotalResidual + TotalEdgeJump);
    // OpenPFEM_Print("整体的后验误差为 TotalError = %2.10f\n", TotalError[0]);
    // OpenPFEM_Print("Finish Computing PosterioriError!\n");
    OpenPFEM_Free(EdgePointValue);
    ElementDestroy(&Elem);
    OpenPFEM_Free(multiindexvaluesOfElem);
    OpenPFEM_Free(multiindexvaluesOfEdge);
    OpenPFEM_Free(BaseValuesOfElem);
    OpenPFEM_Free(BaseValuesOfEdge);
    OpenPFEM_Free(localvalues);
    return;
}

//===================================================================================================================
// 找到单元ind的第i条边的起点start和终点end在标准单元上的坐标, 也就是判断边界的顶点是边准单元上的那个顶点 返回那个顶点在参考单元上的坐标
// 返回值储存在EdgeCoord=[x_start,y_start,x_end,y_end], 找到单元ind的第eind条边在标准单元上的坐标,也就是判断边界顶点是标准单元上
// 的哪个顶点 返回单元边界顶点在参考单元上的坐标, 返回值储存在EdgeCoord=[x_start,y_start,x_end,y_end](二维)
//---------------------------------------------------------------------------------------------------------------------------------
void FindEdgeCoord(MESH *mesh, INT ind, INT eind, DOUBLE *EdgeCoord)
{
    INT worlddim = mesh->worlddim;
    // ind: 单元编号, eind: 边界的局部编号, EdgeCoord: 存储相应面的局部坐标
    INT EdgeNum, m, n, i, j;
    INT *NumByEdge;
    INT NumByElem[worlddim];
    INT EdgeNumInElem[worlddim];
    INT RefElemCoords2D[6] = {0.0, 0.0, 1.0, 0.0, 0.0, 1.0}; // 参考三角形的三个节点坐标
    INT ElemEdgeVerts2D[6] = {1, 2, 2, 0, 0, 1};             // 三角形上三条边的节点
    // 标准四面体上4个节点的坐标
    INT RefElemCoords3D[12] = {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
    // 标准四面体上4个面的节点编号
    INT ElemEdgeVerts3D[12] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1};
    switch (worlddim)
    {
    case 2:
        // EdgeNum表示这条边在本进程上的编号, i表示在这个单元上的局部编号
        EdgeNum = mesh->Faces[ind].Line4Face[eind]; // 得到边的编号
        // NumByEdge记录由边界得到的顶点编号,2D按照边的Vert4Line的顺序, 这个数组有两个元素
        NumByEdge = mesh->Lines[EdgeNum].Vert4Line; // 得到目前边上的两个节点编号
        // 下面通过Elem找到这条边上的两个顶点编号, 这里的编号是按照面得到的
        // 单元的第eind条边的顶点在单元中的局部编号为m,n
        m = ElemEdgeVerts2D[2 * eind];
        n = ElemEdgeVerts2D[2 * eind + 1];
        NumByElem[0] = mesh->Faces[ind].Vert4Face[m]; // 获得相应的全局编号
        NumByElem[1] = mesh->Faces[ind].Vert4Face[n]; // 获得相应的全局编号
        // LineNumInElem储存这条线的起点和终点对应在单元中的局部编号
        if (NumByElem[0] == NumByEdge[0])
        {
            EdgeNumInElem[0] = m; // 获得当前边的起点在参考三角形上的节点编号
            EdgeNumInElem[1] = n; // 获得当前边的终点在参考三角形上的节点编号
        }
        else
        {
            EdgeNumInElem[0] = n;
            EdgeNumInElem[1] = m;
        }
        // 根据在单元中对应的局部编号,确定起点和终点的坐标
        for (i = 0; i < worlddim; i++)
        {
            EdgeCoord[i * 2] = RefElemCoords2D[2 * EdgeNumInElem[i]];
            EdgeCoord[i * 2 + 1] = RefElemCoords2D[2 * EdgeNumInElem[i] + 1];
        }
        break;
    case 3:
        // EdgeNum表示这个面在本进程上的编号,eind表示在该单元上的局部编号
        EdgeNum = mesh->Volus[ind].Face4Volu[eind]; // 得到单元的第eind个边界编号
        // NumByEdge记录由边界得到的顶点编号: 3D按照面的Vert4Face的顺序(这个数组有三个元素,通过节点编号来具体确定顺序)
        NumByEdge = &(mesh->Faces[EdgeNum].Vert4Face[0]); // 该面上的三个节点的全局编号
        // 下面从四面体得到的边界三个节点编号
        for (i = 0; i < 3; i++)
            NumByElem[i] = mesh->Volus[ind].Vert4Volu[ElemEdgeVerts3D[3 * eind + i]];
        // LineNumInElem储存这个边界面的顶点对应在单元中的局部编号
        for (i = 0; i < worlddim; i++)
        {
            // Edge上的第i个节点
            for (j = 0; j < worlddim; j++)
            { // Elem上的ElemEdgeVerts3D[3*eind+j]节点
                if (NumByEdge[i] == NumByElem[j])
                {
                    EdgeNumInElem[i] = ElemEdgeVerts3D[3 * eind + j]; // 得到参考单元上的节点编号
                }
            }
        }
        // 根据在单元中对应的局部编号 确定顶点的坐标
        for (j = 0; j < worlddim; j++)
        { // 节点编号
            for (i = 0; i < worlddim; i++)
            { // 坐标编号
                EdgeCoord[j * 3 + i] = RefElemCoords3D[3 * EdgeNumInElem[j] + i];
            } // end for i
        }     // end for j
        break;
    } // end for switch
    return;
} // end of this program
// 检查法向量是否计算正确
//  NormalVec=[n0x,n0y,n1x,n1y,n2x,n2y]
void CheckNormalVec(MESH *mesh, DOUBLE *NormalVec)
{
    INT i, j;
    DOUBLE length, innerproduct, deltaX, deltaY;
    // 对单元进行循环
    for (i = 0; i < mesh->num_face; i++)
    {
        // 对单元的每条边进行循环
        //  0号点对应的0号边
        //  1.检查法向量是否为单位向量
        length = NormalVec[i * 6] * NormalVec[i * 6] + NormalVec[i * 6 + 1] * NormalVec[i * 6 + 1];
        if (fabs(length - 1) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第0号边法向量不是单位向量\n", i);
        }
        //  2.检查法向量是否和边垂直
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[1];
        innerproduct = NormalVec[i * 6] * deltaX + NormalVec[i * 6 + 1] * deltaY;
        if (fabs(innerproduct) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第0号边法向量不垂直\n", i);
        }
        //  3.检查法向量是否向外
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[1];
        innerproduct = NormalVec[i * 6] * deltaX + NormalVec[i * 6 + 1] * deltaY;
        if (innerproduct < 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第0号边法向量不是向外的 其中 %f-%f=%f %f %f\n", i,
                               mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0], mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[0],
                               deltaX, deltaY, innerproduct);
        }
        // 1号点对应的1号边
        // 1.检查法向量是否为单位向量
        length = NormalVec[i * 6 + 2] * NormalVec[i * 6 + 2] + NormalVec[i * 6 + 3] * NormalVec[i * 6 + 3];
        if (fabs(length - 1.0) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第1号边法向量不是单位向量\n", i);
        }
        //  2.检查法向量是否和边垂直
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[1];
        innerproduct = NormalVec[i * 6 + 2] * deltaX + NormalVec[i * 6 + 3] * deltaY;
        if (fabs(innerproduct) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第1号边法向量不垂直\n", i);
        }
        // 3.检查法向量是否向外
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[1];
        innerproduct = NormalVec[i * 6 + 2] * deltaX + NormalVec[i * 6 + 3] * deltaY;
        if (innerproduct < 10e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第1号边法向量不是向外的 其中 %f %f %f\n", i, deltaX, deltaY, innerproduct);
        }
        // 3号点对应的3号边
        // 1.检查法向量是否为单位向量
        length = NormalVec[i * 6 + 4] * NormalVec[i * 6 + 4] + NormalVec[i * 6 + 5] * NormalVec[i * 6 + 5];
        if (fabs(length - 1) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第2号边法向量不是单位向量\n", i);
        }
        // 2.检查法向量是否和边垂直
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[0]].Coord[1];
        innerproduct = NormalVec[i * 6 + 4] * deltaX + NormalVec[i * 6 + 5] * deltaY;
        if (fabs(innerproduct) > 1.0e-12)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第2号边法向量不垂直\n", i);
        }
        // 3.检查法向量是否向外
        deltaX = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[0] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[0];
        deltaY = mesh->Verts[mesh->Faces[i].Vert4Face[1]].Coord[1] - mesh->Verts[mesh->Faces[i].Vert4Face[2]].Coord[1];
        innerproduct = NormalVec[i * 6 + 4] * deltaX + NormalVec[i * 6 + 5] * deltaY;
        if (innerproduct < 0)
        {
            OpenPFEM_RankPrint(DEFAULT_COMM, "单元%d中的第2号边法向量不是向外的 其中 %f %f %f\n", i, deltaX, deltaY, innerproduct);
        }
    }
    return;
}

// 对于调整了局部编号顺序的curl单元 在计算误差时需要对自由度取值调换顺序
void ReorderFEMValuesForCurlElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *FEMValues)
{
    MESH *mesh = femspace->Mesh;
    BASE *base = femspace->Base;
    INT *dof = base->DOF;
    INT NumBase = base->NumBase, Inter_Dim = base->InterpolationValueDim;
    INT worlddim = mesh->worlddim;
    DOUBLE *temp_FEMValues;
    temp_FEMValues = malloc(NumBase * sizeof(DOUBLE)); // 复制FEMValues
    memcpy(temp_FEMValues, FEMValues, NumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderFEMValuesForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new;

    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * dof[1];
        num_row_new = rowline * dof[1];
        for (rowdof = 0; rowdof < dof[1]; rowdof++)
        {
            FEMValues[num_row_new + rowdof] = temp_FEMValues[num_row_old + rowdof];
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * dof[1] + FaceIdInRefElem[rowline] * dof[2];
        num_row_new = 6 * dof[1] + rowline * dof[2];
        for (rowdof = 0; rowdof < dof[2]; rowdof++)
        {
            FEMValues[num_row_new + rowdof] = temp_FEMValues[num_row_old + rowdof];
        }
    }
    free(temp_FEMValues);
}

// 对于调整了局部编号顺序的div单元 在计算误差时需要对自由度取值调换顺序
void ReorderFEMValuesForDivElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *FEMValues)
{
    MESH *mesh = femspace->Mesh;
    BASE *base = femspace->Base;
    INT *dof = base->DOF;
    INT NumBase = base->NumBase, Inter_Dim = base->InterpolationValueDim;
    INT worlddim = mesh->worlddim;
    DOUBLE *temp_FEMValues;
    temp_FEMValues = malloc(NumBase * sizeof(DOUBLE)); // 复制FEMValues
    memcpy(temp_FEMValues, FEMValues, NumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderFEMValuesForDivElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new;

    INT ii0[4] = {1, 0, 0, 0};
    INT ii1[4] = {2, 2, 1, 1};
    INT ii2[4] = {3, 3, 3, 2};
    INT ii3[4] = {0, 1, 2, 3};
    DOUBLE vec_line0[3], vec_line1[3], vec_norm[3];
    INT i = 3;
    vec_line0[0] = Elem->Vert_X[ii1[i]] - Elem->Vert_X[ii0[i]];
    vec_line0[1] = Elem->Vert_Y[ii1[i]] - Elem->Vert_Y[ii0[i]];
    vec_line0[2] = Elem->Vert_Z[ii1[i]] - Elem->Vert_Z[ii0[i]];
    vec_line1[0] = Elem->Vert_X[ii2[i]] - Elem->Vert_X[ii0[i]];
    vec_line1[1] = Elem->Vert_Y[ii2[i]] - Elem->Vert_Y[ii0[i]];
    vec_line1[2] = Elem->Vert_Z[ii2[i]] - Elem->Vert_Z[ii0[i]];
    vec_norm[0] = vec_line0[1] * vec_line1[2] - vec_line0[2] * vec_line1[1];
    vec_norm[1] = vec_line0[2] * vec_line1[0] - vec_line0[0] * vec_line1[2];
    vec_norm[2] = vec_line0[0] * vec_line1[1] - vec_line0[1] * vec_line1[0];
    if ((vec_norm[0] * (Elem->Vert_X[ii3[i]] - Elem->Vert_X[ii0[i]]) + vec_norm[1] * (Elem->Vert_Y[ii3[i]] - Elem->Vert_Y[ii0[i]]) + vec_norm[2] * (Elem->Vert_Z[ii3[i]] - Elem->Vert_Z[ii0[i]])) > 0)
    {
        Elem->is_det_positive = 1;
    }
    else
    {
        Elem->is_det_positive = 0;
    }

    // 行对应 面上的自由度 // 同时将相应的法向量转换方向
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = FaceIdInRefElem[rowline] * dof[2];
        num_row_new = rowline * dof[2];
        for (rowdof = 0; rowdof < dof[2]; rowdof++)
        {
            FEMValues[num_row_new + rowdof] = pow(-1, (INT)Elem->is_det_positive + rowline + 1) * temp_FEMValues[num_row_old + rowdof];
        }
    }

    free(temp_FEMValues);
}