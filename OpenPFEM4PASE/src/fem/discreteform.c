#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "discreteform.h"

AUXFEMFUNCTION *AuxFEMFunctionBuild(INT NFEMFuns, FEMFUNCTION **femfunctions,
                                    INT *NAuxFEMFunMultiIndex, MULTIINDEX *AuxFEMFunMultiIndex)
{
    AUXFEMFUNCTION *AuxFEMFunction = malloc(sizeof(AUXFEMFUNCTION));
    AuxFEMFunction->NAuxFEMFun = NFEMFuns;
    AuxFEMFunction->AuxFEMFun = femfunctions;
    AuxFEMFunction->NAuxFEMFunMultiIndex = NAuxFEMFunMultiIndex;
    AuxFEMFunction->AuxFEMFunMultiIndex = AuxFEMFunMultiIndex;
    // 下面来计算NAuxFEMFunValues，同时申请相应的内存空间
    AuxFEMFunction->NAuxFEMFunValues = 0;
    INT k, maxlength = 0, tmp;
    BASE *Base;
    // 建立相应的内存空间为计算有限元基函数的值使用
    for (k = 0; k < NFEMFuns; k++)
    {
        Base = femfunctions[k]->FEMSpace->Base;
        AuxFEMFunction->NAuxFEMFunValues += NAuxFEMFunMultiIndex[k] * Base->ValueDim;
        tmp = Base->NumBase * Base->ValueDim;
        if (tmp > maxlength)
            maxlength = tmp;
    }
    // 2023.1.9 lyf 因为free会报错修改此处内容
    if (maxlength)
    {
        AuxFEMFunction->AuxBaseValues = (DOUBLE *)malloc(maxlength * sizeof(DOUBLE));
        AuxFEMFunction->AuxFEMFunValues = (DOUBLE *)malloc(AuxFEMFunction->NAuxFEMFunValues * sizeof(DOUBLE));
    }
    else
    {
        AuxFEMFunction->AuxBaseValues = NULL;
        AuxFEMFunction->AuxFEMFunValues = NULL;
    }
    // lhc 添加
    AuxFEMFunction->IsHaveAuxBaseValuesInPointsOfRefElem = 0;
    AuxFEMFunction->AuxBaseValuesInPointsOfRefElem = NULL;
    return AuxFEMFunction;
}
void AuxFEMFunctionDestroy(AUXFEMFUNCTION **AuxFEMFunction)
{
    OpenPFEM_Free((*AuxFEMFunction)->AuxBaseValues);
    OpenPFEM_Free((*AuxFEMFunction)->AuxFEMFunValues);
    OpenPFEM_Free((*AuxFEMFunction));
}

// 计算辅助函数在积分点的函数值
void ComputeAuxFEMFunValue(AUXFEMFUNCTION *AuxFEMFunction, INT ElemInd, ELEMENT *Elem,
                           DOUBLE Coord[], DOUBLE RefCoord[])
{
    INT k, i, j, m, pos, NumBase;
    INT NFEMFuns = AuxFEMFunction->NAuxFEMFun;
    FEMFUNCTION **femfunctions = AuxFEMFunction->AuxFEMFun, *femfun;
    FEMSPACE *femspace;
    BASE *Base;
    INT *NMultiIndex = AuxFEMFunction->NAuxFEMFunMultiIndex;
    MULTIINDEX *MultiIndex = AuxFEMFunction->AuxFEMFunMultiIndex;
    DOUBLE *AuxFEMFunValues = AuxFEMFunction->AuxFEMFunValues;
    DOUBLE *Values, *AuxBaseValues = AuxFEMFunction->AuxBaseValues, *AuxFEMValues = AuxFEMFunction->AuxFEMFunValues;
    INT *BeginIndex, *LocalIndex;
    INT ValueDim, base; // 有限元函数的维数
    // 根据基函数求解相应的基函数值
    //  void GetBaseValues(BASE *Base, MULTIINDEX MultiIndex,
    //                 ELEMENT *EElem, double Coord[worlddim],
    //                 double RefCoord[worlddim], double *Values);
    pos = 0;
    base = 0;
    DOUBLE *AuxBaseValuesInPointsOfRefElem = AuxFEMFunction->AuxBaseValuesInPointsOfRefElem;
    INT *IndAuxMultiIndex = AuxFEMFunction->IndAuxMultiIndex;
    BOOL IsHaveAuxBaseValuesInPointsOfRefElem = AuxFEMFunction->IsHaveAuxBaseValuesInPointsOfRefElem;

    for (k = 0; k < NFEMFuns; k++)
    {
        // 获得有限元函数的对象
        femfun = femfunctions[k];
        Values = femfun->Values; // 获得有限元的系数数组
        femspace = femfun->FEMSpace;
        // 获得当前单元的自由度编号数量
        LocalIndex = femspace->LocalIndex + femspace->BeginIndex[ElemInd];
        Base = femspace->Base; // 获得当前的有限元基函数
        NumBase = Base->NumBase;
        ValueDim = Base->ValueDim;
        for (i = 0; i < NMultiIndex[k]; i++)
        {
            // 计算第k个有限元函数的第i个微分的值
            if (IsHaveAuxBaseValuesInPointsOfRefElem == 0)
            {
                GetBaseValues(Base, MultiIndex[pos], Elem, Coord, RefCoord, AuxBaseValues);
            }
            else
            {
                GetBaseValuesWithInfo(Base, MultiIndex[pos], Elem, AuxBaseValuesInPointsOfRefElem + (AuxFEMFunction->Points_ind) * IndAuxMultiIndex[3] * NumBase * ValueDim, IndAuxMultiIndex,
                                      AuxBaseValues);
            }
            for (m = 0; m < ValueDim; m++) // 对函数值不同维数进行循环
            {
                AuxFEMValues[base + m] = 0.0;
                for (j = 0; j < NumBase; j++)
                {
                    // 计算有限元的函数值
                    AuxFEMFunValues[base + m] +=
                        Values[LocalIndex[j]] * AuxBaseValues[j * ValueDim + m];
                } // end for j
            }     // end for m
            pos++;
            base += ValueDim;
        } // end for i
    }     // end for k femfunction
} // end for computing the auxfemfunction values
// AuxFEMFunValues的排列顺序是: 不同的有限元函数,不同的微分算符,不同的维数值(基函数是向量函数)

// 计算多个有限元函数之间的运算的值: 比如$\|u_{1,h}-u_{2,h}\|_{1,\Omega}$
// 我们建设所有的有限元空间是定义在同一个网格上
DOUBLE ComputeFEMFunctionValue(AUXFEMFUNCTION *AuxFEMFun, ERRORFORM *ErrorForm, QUADRATURE *Quadrature)
{
    // FEMFUNCTION *femfunctions = AuxFEMFun->AuxFEMFun, *femfun;
    INT k, i, j, ElemInd, NAuxFEMFuns = AuxFEMFun->NAuxFEMFun;
    INT ind, LocalNumElems, NAuxFEMFun = AuxFEMFun->NAuxFEMFun;
    INT *NAuxFEMFunMultiIndex = AuxFEMFun->NAuxFEMFunMultiIndex;
    MULTIINDEX *AuxFEMFunMultiIndex = AuxFEMFun->AuxFEMFunMultiIndex;
    FEMSPACE *femspace;
    MESH *mesh;
    BASE *Base;
    ELEMENT *Elem;
    INT worlddim = mesh->worlddim;
    // INT *LocalIndex = FemSpace->LocalIndex;
    // INT *BeginIndex = FemSpace->BeginIndex;
    // INT *localdof;
    // DOUBLE *FEMValues = femfun->Values,
    DOUBLE localvalue;
    INT NumPoints = Quadrature->NumPoints;

    DOUBLE Coord[worlddim], RefCoord[worlddim], weight;
    MULTIINDEX NoGradIndex;
    // 进行与空间维数相关的运算和操作
    switch (worlddim)
    {
    case 1:
        NoGradIndex = D0;
        LocalNumElems = mesh->num_line;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    case 2:
        NoGradIndex = D00;
        LocalNumElems = mesh->num_face;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    case 3:
        NoGradIndex = D000;
        // get the number of elements
        LocalNumElems = mesh->num_volu;
        // 初始化相应的有限元单元
        Elem = ElementInitial(mesh);
        break;
    }
    // 判断是否需要计算Jacobi逆矩阵
    INT tmpind = 0, pos = 0;
    for (k = 0; k < NAuxFEMFuns; k++)
    {
        for (i = 0; i < NAuxFEMFunMultiIndex[k]; i++)
        {
            // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
            if (AuxFEMFunMultiIndex[pos] > NoGradIndex)
            {
                tmpind = 1;
                i = NAuxFEMFunMultiIndex[k] + 3;
                k = NAuxFEMFuns + 3;
            }
            pos++;
        } // end for i
    }     // end for k

    // 接下来进行单元循环
    DOUBLE Value = 0.0;
    for (ind = 0; ind < LocalNumElems; ind++)
    {
        // 建立当前的有限单元
        ElementBuild(mesh, ind, Elem);
        // 计算单元的体积和Jacobian矩阵
        if (tmpind == 0)
        {
            ComputeElementJacobian(Elem); // 不需要计算Jacboian逆矩阵
        }
        else
        {
            ComputeElementInvJacobian(Elem); // 需要计算Jacobian逆矩阵
        }
        // 对积分点的循环, 求得本单元上误差
        for (k = 0; k < NumPoints; k++)
        {
            switch (worlddim)
            {
            case 1:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                break;
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                RefCoord[2] = Quadrature->QuadZ[k];
                break;
            } // end switch(worlddim)
            weight = Quadrature->QuadW[k];
            ElementRefCoord2Coord(Elem, RefCoord, Coord);
            // 计算有限元函数在积分点的函数值
            AuxFEMFun->Points_ind = k;
            ComputeAuxFEMFunValue(AuxFEMFun, ind, Elem, Coord, RefCoord);
            ErrorForm(Coord, AuxFEMFun->AuxFEMFunValues, &localvalue);
            Value += localvalue * weight * Elem->Volumn;
        } // end for(k=0;k<NumPoints;k++)//对积分点的循环
    }     // end for(ind=0;ind<LocalNumElems;ind++)
    ElementDestroy(&Elem);
    DOUBLE VALUE = 0.0;
    MPI_Allreduce(&Value, &VALUE, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return sqrt(VALUE);
}

// 下面是一些操作函数：最重要的是建立离散变分形式: 本质上这里什么也没有做，只是做了一些登记
/** 建立离散变分形式 （不需要解析函数和有限元函数）*/
DISCRETEFORM *DiscreteFormBuild(FEMSPACE *LeftSpace, INT NumLeftMultiIndex, MULTIINDEX *LeftMultiIndex,
                                FEMSPACE *RightSpace, INT NumRightMultiIndex, MULTIINDEX *RightMultiIndex,
                                DISCRETEFORMMATRIX *DiscreteFormMatrix, DISCRETERHSFORMVEC *DiscreteFormVec,
                                FUNCTIONVEC *BoundaryFunction,
                                QUADRATURE *Quadrature)
{
    DISCRETEFORM *discreteform;
    discreteform = malloc(sizeof(DISCRETEFORM));
    discreteform->LeftSpace = LeftSpace;
    discreteform->NumLeftMultiIndex = NumLeftMultiIndex;
    discreteform->LeftMultiIndex = LeftMultiIndex;
    discreteform->LeftBaseValues = malloc(LeftSpace->Base->NumBase * LeftSpace->Base->ValueDim * NumLeftMultiIndex * sizeof(DOUBLE));
    discreteform->RightSpace = RightSpace;
    discreteform->NumRightMultiIndex = NumRightMultiIndex;
    discreteform->RightMultiIndex = RightMultiIndex;
    discreteform->RightBaseValues = malloc((RightSpace->Base->NumBase) * (RightSpace->Base->ValueDim) * NumRightMultiIndex * sizeof(DOUBLE));
    discreteform->AuxFEMFunction = NULL;
    discreteform->IsUseAuxFEMSpace = 0;
    discreteform->IsHaveLeftBaseValuesInPointsOfRefElem = 0;
    discreteform->IsHaveRightBaseValuesInPointsOfRefElem = 0;
    discreteform->LeftSpaceInAssemble = NULL;
    discreteform->Relates = NULL;
    discreteform->LeftBaseValuesInPointsOfRefElem = NULL;
    discreteform->RightBaseValuesInPointsOfRefElem = NULL;
    discreteform->DiscreteFormMatrix = DiscreteFormMatrix;
    discreteform->DiscreteFormVec = DiscreteFormVec;
    if (BoundaryFunction)
    {
        discreteform->BoundaryFunction = BoundaryFunction;
    }
    else
    {
        discreteform->BoundaryFunction = NULL;
    }
    discreteform->NeumannBoundaryFunction = NULL;
    discreteform->RobinBoundaryFunction = NULL;
    discreteform->Quadrature = Quadrature;
    INT RNum, LNum;
    RNum = (RightSpace->Base->NumBase) * (RightSpace->Base->ValueDim) * NumRightMultiIndex;
    LNum = (LeftSpace->Base->NumBase) * (LeftSpace->Base->ValueDim) * NumLeftMultiIndex;
    if (LNum > RNum)
        RNum = LNum;
    discreteform->AuxValues = malloc(RNum * sizeof(DOUBLE)); // 用来计算基函数微分值的辅助存储空间
    return discreteform;
}
/** 建立离散变分形式对象 （需要辅助的有限元函数）*/
DISCRETEFORM *DiscreteFormAuxFeFunBuild(FEMSPACE *LeftSpace, INT NumLeftMultiIndex,
                                        MULTIINDEX *LeftMultiIndex, FEMSPACE *RightSpace,
                                        INT NumRightMultiIndex, MULTIINDEX *RightMultiIndex,
                                        AUXFEMFUNCTION *AuxFEMFunction, DISCRETEFORMMATRIX *DiscreteFormMatrix,
                                        DISCRETERHSFORMVEC *DiscreteFormVec, FUNCTIONVEC *BoundaryFunction,
                                        QUADRATURE *Quadrature)
{
    DISCRETEFORM *discreteform;
    discreteform = malloc(sizeof(DISCRETEFORM));
    discreteform->LeftSpace = LeftSpace;
    discreteform->NumLeftMultiIndex = NumLeftMultiIndex;
    discreteform->LeftMultiIndex = LeftMultiIndex;
    discreteform->LeftBaseValues = malloc(LeftSpace->Base->NumBase * LeftSpace->Base->ValueDim * NumLeftMultiIndex * sizeof(DOUBLE));
    discreteform->RightSpace = RightSpace;
    discreteform->NumRightMultiIndex = NumRightMultiIndex;
    discreteform->RightMultiIndex = RightMultiIndex;
    discreteform->RightBaseValues = malloc((RightSpace->Base->NumBase) * (RightSpace->Base->ValueDim) * NumRightMultiIndex * sizeof(DOUBLE));
    discreteform->AuxFEMFunction = AuxFEMFunction;
    // lhc 23.01.28
    INT idx_auxfun;
    discreteform->IsUseAuxFEMSpace = 0;
    discreteform->IsHaveLeftBaseValuesInPointsOfRefElem = 0;
    discreteform->IsHaveRightBaseValuesInPointsOfRefElem = 0;
    discreteform->LeftSpaceInAssemble = NULL; // 如果非线性项对应的有限元空间和左空间完全相同 该值为NULL
    discreteform->LeftBaseValuesInPointsOfRefElem = NULL;
    discreteform->RightBaseValuesInPointsOfRefElem = NULL;
    for (idx_auxfun = 0; idx_auxfun < AuxFEMFunction->NAuxFEMFun; idx_auxfun++)
    {
        // 如果存在不相同的情况
        if ((LeftSpace) != (AuxFEMFunction->AuxFEMFun[idx_auxfun]->FEMSpace))
        {
            discreteform->IsUseAuxFEMSpace = 1;
            // 组装刚度矩阵用更细的有限元空间当作左空间进行组装
            discreteform->LeftSpaceInAssemble = AuxFEMFunction->AuxFEMFun[idx_auxfun]->FEMSpace;
            // 加入两个mesh层级的判断
            INT levelid_coarse = LeftSpace->Mesh->level_id;
            INT levelid_finer = discreteform->LeftSpaceInAssemble->Mesh->level_id;
            if ((levelid_finer - levelid_coarse) == 0)
            {
                discreteform->Relates = NULL;
            }
            else if ((levelid_finer - levelid_coarse) == 1)
            {
                discreteform->Relates = discreteform->LeftSpaceInAssemble->Mesh->Fathers;
            }
            else if (levelid_coarse == 0)
            {
                discreteform->Relates = discreteform->LeftSpaceInAssemble->Mesh->Ancestors;
            }
            else
            {
                RaiseError("DiscreteFormAuxFeFunBuild", "two femspaces have no correct relation");
            }
            idx_auxfun = AuxFEMFunction->NAuxFEMFun; // 退出循环
        }
    }

    discreteform->DiscreteFormMatrix = DiscreteFormMatrix;
    discreteform->DiscreteFormVec = DiscreteFormVec;
    if (BoundaryFunction)
    {
        discreteform->BoundaryFunction = BoundaryFunction;
    }
    else
    {
        discreteform->BoundaryFunction = NULL;
    }
    discreteform->NeumannBoundaryFunction = NULL;
    discreteform->RobinBoundaryFunction = NULL;
    discreteform->Quadrature = Quadrature;
    INT RNum, LNum;
    RNum = (RightSpace->Base->NumBase) * (RightSpace->Base->ValueDim) * NumRightMultiIndex;
    LNum = (LeftSpace->Base->NumBase) * (LeftSpace->Base->ValueDim) * NumLeftMultiIndex;
    if (LNum > RNum)
        RNum = LNum;
    discreteform->AuxValues = malloc(RNum * sizeof(DOUBLE));
    return discreteform;
}
// 释放离散变分形式的内存空间
void DiscreteFormDestroy(DISCRETEFORM **discreteform)
{
    DestroyBaseValuesInPointsOfRefElem(*discreteform);
    OpenPFEM_Free((*discreteform)->LeftBaseValues);
    OpenPFEM_Free((*discreteform)->RightBaseValues);
    OpenPFEM_Free((*discreteform)->AuxValues);
    if ((*discreteform)->AuxFEMFunction != NULL)
        AuxFEMFunctionDestroy(&((*discreteform)->AuxFEMFunction));
    OpenPFEM_Free((*discreteform));
}

// 计算RefCoord中各个积分点对应的函数值信息(只计算能用于简化计算的部分)
void ComputeBaseValuesInPointsOfRefElem(DISCRETEFORM *DiscreteForm)
{
    INT worlddim = DiscreteForm->LeftSpace->Mesh->worlddim;
    QUADRATURE *Quadrature = DiscreteForm->Quadrature; // 取出积分信息
    INT NumPoints = Quadrature->NumPoints;             // 积分点的个数
    INT ii, jj;
    DOUBLE RefCoord[worlddim]; // 用来存储标准单元上的积分点坐标
    MULTIINDEX MultiIndex;     // 相应的导数
    // 判断左空间信息是否可以提前生成,
    //(DiscreteForm->LeftSpace->Base->IsOnlyDependOnRefCoord == 1)表示基函数只依赖与RefCoord
    //(DiscreteForm->IsUseAuxFEMSpace == 0)表示使用RSpace进行组装
    // 上述都符合可以提前生成该信息, 列所对应的有限元空间是Affine类型，且辅助有限元函数的网格不是细网格，
    // 并且Discreteform中已经有了相应的存储内存
    if ((DiscreteForm->LeftSpace->Base->IsOnlyDependOnRefCoord == 1) && (DiscreteForm->IsUseAuxFEMSpace == 0) && (DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem == 0))
    {
        MULTIINDEX *RefLeftmultiindex; // 相应的导数
        ComputeMultiindexInRefElem(DiscreteForm->LeftMultiIndex, DiscreteForm->NumLeftMultiIndex,
                                   &RefLeftmultiindex, DiscreteForm->IndLeftMultiIndex, worlddim);
        FEMSPACE *Cspace = DiscreteForm->LeftSpace;
        BASE *Cbase = Cspace->Base;
        INT CNumBase = Cspace->Base->NumBase;
        INT CValueDim = Cspace->Base->ValueDim;
        INT *IndLeftMultiIndex = DiscreteForm->IndLeftMultiIndex;
        // 为存储标准单元上的基函数的导数值申请相应的内存空间，IndLeftMultiIndex[3]表示所有需要计算微分类型的个数
        DiscreteForm->LeftBaseValuesInPointsOfRefElem = malloc(NumPoints * CValueDim * CNumBase * (IndLeftMultiIndex[3]) * sizeof(DOUBLE));
        DOUBLE *CPointInfInRefElem = DiscreteForm->LeftBaseValuesInPointsOfRefElem; // 定义一个相应的指针，减少书写量

        // 对积分点进行循环 针对输入的积分格式来确定积分点的坐标
        for (ii = 0; ii < NumPoints; ii++)
        {
            // 获取积分点的坐标
            switch (worlddim)
            {
            case 1:
                RefCoord[0] = Quadrature->QuadX[ii];
                break;
            case 2:
                RefCoord[0] = Quadrature->QuadX[ii];
                RefCoord[1] = Quadrature->QuadY[ii];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[ii];
                RefCoord[1] = Quadrature->QuadY[ii];
                RefCoord[2] = Quadrature->QuadZ[ii];
                break;
            }
            // 对微分信息进行循环
            for (jj = 0; jj < IndLeftMultiIndex[3]; jj++)
            {
                MultiIndex = RefLeftmultiindex[jj];
                // ii表示积分点的指标，jj表示微分类型的编号，每个点都计算出所需要的微分信息
                // 下面的微分信息的排列需要慎重考虑一下任意不同的情况
                switch (worlddim)
                {
                case 1: // switch (worlddim)
                    Cbase->BaseFun[MultiIndex](NULL, NULL, RefCoord,
                                               CPointInfInRefElem + (ii * IndLeftMultiIndex[3] + jj) * CValueDim * CNumBase);
                    break; // switch (worlddim)

                case 2: // switch (worlddim)
                    Cbase->BaseFun[MultiIndex - 3](NULL, NULL, RefCoord,
                                                   CPointInfInRefElem + (ii * IndLeftMultiIndex[3] + jj) * CValueDim * CNumBase);
                    break; // switch (worlddim)
                case 3:    // switch (worlddim)
                    Cbase->BaseFun[MultiIndex - 9](NULL, NULL, RefCoord,
                                                   CPointInfInRefElem + (ii * IndLeftMultiIndex[3] + jj) * CValueDim * CNumBase);
                    break; // switch (worlddim)
                }          // end switch (worlddim)
            }              // end 微分信息循环
        }                  // end 积分点循环
        // 最后告诉DiscreteForm, 参考单元上的基函数的微分信息已经准备好了
        DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem = 1;
    }
    // 同样的方式处理与行相对应的有限元基函数微分信息的计算
    //  判断右空间信息是否可以提前生成
    //(DiscreteForm->RightSpace->Base->IsOnlyDependOnRefCoord == 1)表示基函数只依赖与RefCoord
    //(DiscreteForm->IsUseAuxFEMSpace == 0)表示使用Cspace进行组装
    //  上述都符合可以提前生成该信息
    if ((DiscreteForm->RightSpace->Base->IsOnlyDependOnRefCoord == 1) && (DiscreteForm->IsUseAuxFEMSpace == 0) && (DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem == 0))
    {
        MULTIINDEX *RefRightmultiindex;
        // 计算所需要计算的微分信息
        ComputeMultiindexInRefElem(DiscreteForm->RightMultiIndex, DiscreteForm->NumRightMultiIndex,
                                   &RefRightmultiindex, DiscreteForm->IndRightMultiIndex, worlddim);
        FEMSPACE *Rspace = DiscreteForm->RightSpace;                // 取出相应的有限元空间
        BASE *Rbase = Rspace->Base;                                 // 取出相应的基函数对象
        INT RNumBase = Rbase->NumBase;                              // 基函数个数
        INT RValueDim = Rbase->ValueDim;                            // 基函数函数值的维数
        INT *IndRightMultiIndex = DiscreteForm->IndRightMultiIndex; // 所需要计算的微分信息
        // 申请内存空间
        DiscreteForm->RightBaseValuesInPointsOfRefElem = malloc(NumPoints * RValueDim * RNumBase * IndRightMultiIndex[3] * sizeof(DOUBLE));
        DOUBLE *RPointInfInRefElem = DiscreteForm->RightBaseValuesInPointsOfRefElem; // 取一个别名，减少书写量
        // 对积分点进行循环 针对输入的积分格式来确定积分点的坐标
        for (ii = 0; ii < NumPoints; ii++)
        {
            // 获取积分点的坐标
            switch (worlddim)
            {
            case 1:
                RefCoord[0] = Quadrature->QuadX[ii];
                break;
            case 2:
                RefCoord[0] = Quadrature->QuadX[ii];
                RefCoord[1] = Quadrature->QuadY[ii];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[ii];
                RefCoord[1] = Quadrature->QuadY[ii];
                RefCoord[2] = Quadrature->QuadZ[ii];
                break;
            }
            // 对微分信息进行循环
            for (jj = 0; jj < IndRightMultiIndex[3]; jj++)
            {
                // ii表示积分点编号，jj表示微分类型的编号
                MultiIndex = RefRightmultiindex[jj];
                switch (worlddim)
                {
                case 1: // switch (worlddim)
                    Rbase->BaseFun[MultiIndex](NULL, NULL, RefCoord, RPointInfInRefElem + (ii * IndRightMultiIndex[3] + jj) * RValueDim * RNumBase);
                    break; // switch (worlddim)

                case 2: // switch (worlddim)
                    Rbase->BaseFun[MultiIndex - 3](NULL, NULL, RefCoord, RPointInfInRefElem + (ii * IndRightMultiIndex[3] + jj) * RValueDim * RNumBase);
                    break; // switch (worlddim)
                case 3:    // switch (worlddim)
                    Rbase->BaseFun[MultiIndex - 9](NULL, NULL, RefCoord, RPointInfInRefElem + (ii * IndRightMultiIndex[3] + jj) * RValueDim * RNumBase);
                    break;                                        // switch (worlddim)
                }                                                 // end switch (worlddim)
            }                                                     // end 微分信息循环
        }                                                         // end 积分点循环
        DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem = 1; // 最后通知Discreteform标准单元上的微分信息已准备好
    }
    // 判断非线性部分是否需要提前生成
    // 这里只考虑了所有非线性函数都建立在同一个有限元空间上的情况 如果不同需要增加 每个非线性函数单独计算存储！
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        FEMSPACE *Auxspace = DiscreteForm->AuxFEMFunction->AuxFEMFun[0]->FEMSpace; // 取出相应的有限元空间
        if (Auxspace->Base->IsOnlyDependOnRefCoord == 1)
        {
            // 如果辅助有限元函数只依赖标准单元上的微分值
            //  INT IndAuxMultiIndex[4];
            AUXFEMFUNCTION *AuxFEMFunction = DiscreteForm->AuxFEMFunction;
            INT NumAuxMultiIndex = 0;
            INT index_ind;
            // 计算需要计算的微分个数
            for (index_ind = 0; index_ind < AuxFEMFunction->NAuxFEMFun; index_ind++)
            {
                NumAuxMultiIndex += AuxFEMFunction->NAuxFEMFunMultiIndex[index_ind];
            }
            MULTIINDEX *RefAuxmultiindex;
            ComputeMultiindexInRefElem(AuxFEMFunction->AuxFEMFunMultiIndex, NumAuxMultiIndex, &RefAuxmultiindex,
                                       AuxFEMFunction->IndAuxMultiIndex, worlddim);
            INT *IndAuxMultiIndex = AuxFEMFunction->IndAuxMultiIndex;
            BASE *Auxbase = Auxspace->Base;
            INT AuxNumBase = Auxbase->NumBase;
            INT AuxValueDim = Auxbase->ValueDim;
            AuxFEMFunction->AuxBaseValuesInPointsOfRefElem =
                malloc(NumPoints * AuxValueDim * AuxNumBase * IndAuxMultiIndex[3] * sizeof(DOUBLE));
            DOUBLE *AuxPointInfInRefElem = AuxFEMFunction->AuxBaseValuesInPointsOfRefElem;

            // 对积分点进行循环 针对输入的积分格式来确定积分点的坐标
            for (ii = 0; ii < NumPoints; ii++)
            {
                // 获取积分点的坐标
                switch (worlddim)
                {
                case 1:
                    RefCoord[0] = Quadrature->QuadX[ii];
                    break;
                case 2:
                    RefCoord[0] = Quadrature->QuadX[ii];
                    RefCoord[1] = Quadrature->QuadY[ii];
                    break;
                case 3:
                    // 获得积分点坐标,参考单元上的坐标
                    RefCoord[0] = Quadrature->QuadX[ii];
                    RefCoord[1] = Quadrature->QuadY[ii];
                    RefCoord[2] = Quadrature->QuadZ[ii];
                    break;
                }
                // 对微分信息进行循环
                for (jj = 0; jj < IndAuxMultiIndex[3]; jj++)
                {
                    MultiIndex = RefAuxmultiindex[jj];
                    switch (worlddim)
                    {
                    case 1: // switch (worlddim)
                        Auxbase->BaseFun[MultiIndex](NULL, NULL, RefCoord, AuxPointInfInRefElem + (ii * IndAuxMultiIndex[3] + jj) * AuxValueDim * AuxNumBase);
                        break; // switch (worlddim)

                    case 2: // switch (worlddim)
                        Auxbase->BaseFun[MultiIndex - 3](NULL, NULL, RefCoord, AuxPointInfInRefElem + (ii * IndAuxMultiIndex[3] + jj) * AuxValueDim * AuxNumBase);
                        break; // switch (worlddim)
                    case 3:    // switch (worlddim)
                        Auxbase->BaseFun[MultiIndex - 9](NULL, NULL, RefCoord, AuxPointInfInRefElem + (ii * IndAuxMultiIndex[3] + jj) * AuxValueDim * AuxNumBase);
                        break; // switch (worlddim)
                    }          // end switch (worlddim)
                }              // end 微分信息循环
            }                  // end 积分点循环
            AuxFEMFunction->IsHaveAuxBaseValuesInPointsOfRefElem = 1;
        } // end if (Auxspace->Base->IsOnlyDependOnRefCoord == 1)
    }     // end if (DiscreteForm->AuxFEMFunction != NULL)
}

// 释放计算出的RefCoord中各个积分点对应的函数值信息
void DestroyBaseValuesInPointsOfRefElem(DISCRETEFORM *DiscreteForm)
{
    if (DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem == 1)
    {
        OpenPFEM_Free(DiscreteForm->LeftBaseValuesInPointsOfRefElem);
        DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem = 0;
    }
    if (DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem == 1)
    {
        OpenPFEM_Free(DiscreteForm->RightBaseValuesInPointsOfRefElem);
        DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem = 0;
    }
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        if (DiscreteForm->AuxFEMFunction->IsHaveAuxBaseValuesInPointsOfRefElem == 1)
        {
            OpenPFEM_Free(DiscreteForm->AuxFEMFunction->AuxBaseValuesInPointsOfRefElem);
            DiscreteForm->AuxFEMFunction->IsHaveAuxBaseValuesInPointsOfRefElem = 0;
        }
    }
}

// 根据组装过程中需要的微分信息得出需要在标准单元上计算基函数微分类型的信息
void ComputeMultiindexInRefElem(MULTIINDEX *MultiIndexInElem, INT NMultiIndexInElem, MULTIINDEX **MultiIndexInRefElem,
                                INT *IndMultiIndexInRefElem, INT worlddim)
{
    MULTIINDEX *MultiIndex;
    INT index_ind;
    IndMultiIndexInRefElem[0] = 0;
    IndMultiIndexInRefElem[1] = 0;
    IndMultiIndexInRefElem[2] = 0;
    IndMultiIndexInRefElem[3] = 0;
    // 确定是否需要各阶导数
    switch (worlddim)
    {
    case 1:
        // 判断是否包含0阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if (MultiIndexInElem[index_ind] == D0)
            {
                // 需要计算基函数的D0的微分值(即函数值本身)
                IndMultiIndexInRefElem[0] = 1;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[0];
                index_ind = NMultiIndexInElem; // 判断需要计算D0的值就结束本次循环，进入下一个微分符号的判断
            }
        }
        // 判断是否包含1阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if (MultiIndexInElem[index_ind] == D1)
            {
                IndMultiIndexInRefElem[1] = 1;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[1];
                index_ind = NMultiIndexInElem;
            }
        }
        // 判断是否包含2阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if (MultiIndexInElem[index_ind] == D2)
            {
                IndMultiIndexInRefElem[2] = 1;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[2];
                index_ind = NMultiIndexInElem;
            }
        }
        // 申请空间 并 赋值
        MultiIndex = malloc(IndMultiIndexInRefElem[3] * sizeof(MULTIINDEX));
        index_ind = 0;
        if (IndMultiIndexInRefElem[0] > 0)
        {
            MultiIndex[index_ind] = D0;
            index_ind += 1;
        }
        if (IndMultiIndexInRefElem[1] > 0)
        {
            MultiIndex[index_ind] = D1;
            index_ind += 1;
        }
        if (IndMultiIndexInRefElem[2] > 0)
        {
            MultiIndex[index_ind] = D2;
        }
        break;
    case 2:
        // 判断是否包含0阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if (MultiIndexInElem[index_ind] == D00)
            {
                IndMultiIndexInRefElem[0] = 1;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[0];
                index_ind = NMultiIndexInElem + 3;
            }
        }
        // 判断是否包含1阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if ((MultiIndexInElem[index_ind] == D10) || (MultiIndexInElem[index_ind] == D01))
            {
                IndMultiIndexInRefElem[1] = 2;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[1];
                index_ind = NMultiIndexInElem + 3;
            } // end if
        }     // end for index_ind
        // 判断是否包含2阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if ((MultiIndexInElem[index_ind] == D20) || (MultiIndexInElem[index_ind] == D11) || (MultiIndexInElem[index_ind] == D02))
            {
                IndMultiIndexInRefElem[2] = 3;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[2];
                index_ind = NMultiIndexInElem + 3;
            } // end if
        }     // end for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        // 申请空间 并 赋值
        // IndMultiIndexInRefElem[3]存储了需要计算标准单元上基函数微分算子的个数
        MultiIndex = malloc(IndMultiIndexInRefElem[3] * sizeof(MULTIINDEX));
        index_ind = 0;
        // 先排D00, 如果有的话
        if (IndMultiIndexInRefElem[0] > 0)
        {
            MultiIndex[index_ind] = D00;
            index_ind += 1;
        }
        // 然后排D10和D01, 如果有的话
        if (IndMultiIndexInRefElem[1] > 0)
        {
            MultiIndex[index_ind] = D10;
            MultiIndex[index_ind + 1] = D01;
            index_ind += 2;
        }
        // 然后排列D20, D11, D02
        if (IndMultiIndexInRefElem[2] > 0)
        {
            MultiIndex[index_ind] = D20;
            MultiIndex[index_ind + 1] = D11;
            MultiIndex[index_ind + 2] = D02;
        }
        break;

    case 3:
        // 判断是否包含0阶导数信息
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if (MultiIndexInElem[index_ind] == D000)
            {
                IndMultiIndexInRefElem[0] = 1;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[0];
                index_ind = NMultiIndexInElem;
            }
        }
        // 判断是否包含1阶导数信息: D100, D010, D001
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if ((MultiIndexInElem[index_ind] == D100) || (MultiIndexInElem[index_ind] == D010) || (MultiIndexInElem[index_ind] == D001))
            {
                IndMultiIndexInRefElem[1] = 3;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[1];
                index_ind = NMultiIndexInElem;
            }
        }
        // 判断是否包含2阶导数信息, D200,D020,D002, D110, D101, D011 (6个)
        for (index_ind = 0; index_ind < NMultiIndexInElem; index_ind++)
        {
            if ((MultiIndexInElem[index_ind] == D200) || (MultiIndexInElem[index_ind] == D020) || (MultiIndexInElem[index_ind] == D002) || (MultiIndexInElem[index_ind] == D110) || (MultiIndexInElem[index_ind] == D101) || (MultiIndexInElem[index_ind] == D011))
            {
                IndMultiIndexInRefElem[2] = 6;
                IndMultiIndexInRefElem[3] += IndMultiIndexInRefElem[2];
                index_ind = NMultiIndexInElem;
            }
        }
        // 申请空间 并 赋值
        MultiIndex = malloc(IndMultiIndexInRefElem[3] * sizeof(MULTIINDEX));
        index_ind = 0;
        // 线排D000 （如果有的话）
        if (IndMultiIndexInRefElem[0] > 0)
        {
            MultiIndex[index_ind] = D000;
            index_ind += 1;
        }
        // 然后排D100, D010, D001 （如果有的话）
        if (IndMultiIndexInRefElem[1] > 0)
        {
            MultiIndex[index_ind] = D100;
            MultiIndex[index_ind + 1] = D010;
            MultiIndex[index_ind + 2] = D001;
            index_ind += 3;
        }
        // 最后排D200,2020,D002, D110,D101,D011 (如果有的话)
        if (IndMultiIndexInRefElem[2] > 0)
        {
            MultiIndex[index_ind] = D200;
            MultiIndex[index_ind + 1] = D020;
            MultiIndex[index_ind + 2] = D002;
            MultiIndex[index_ind + 3] = D110;
            MultiIndex[index_ind + 4] = D101;
            MultiIndex[index_ind + 5] = D011;
        }
        break;
    }
    *MultiIndexInRefElem = MultiIndex;
} // 最后得到了需要在标准单元上计算基函数微分算子的信息

// 根据在标准单元上已经出来的基函数及其导数的信息来得到基函数MultiIndex的值
// BaseValuesInPointsOfRefElem记录了所有需要用到的参考单元上的基函数微分的信息
// MultiIndex表示我们需要计算的微分符号
// BaseValuesInPointsOfRefElem表示已经计算好的标准单元上的微分信息
// value用来存储实际单元上的基函数的微分信息
void GetBaseValuesWithInfo(BASE *Base, MULTIINDEX MultiIndex, ELEMENT *Elem, DOUBLE *BaseValuesInPointsOfRefElem,
                           INT *IndMultiIndexInRefElem, DOUBLE *Values)
{
    INT worlddim = Base->worlddim; // 获得基函数所在的维数
    INT i, j;
    INT ind[6];
    switch (Base->MapType)
    {
    case Affine:
        switch (worlddim)
        {
        case 1: // switch (worlddim)
            if (MultiIndex == D0)
            {
                // 函数值直接复制即可
                memcpy(Values, BaseValuesInPointsOfRefElem, (Base->NumBase * Base->ValueDim) * sizeof(DOUBLE));
                // 这里是不是均忽略了ValueDim？？？？
            } // end if(MultiIndex==D0)
            else if (MultiIndex == D1)
            {
                // 获得在标准单元微分值的起始位置
                ind[0] = IndMultiIndexInRefElem[0] * (Base->NumBase * Base->ValueDim);
                for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                {
                    Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0];
                }
            } // end if(MultiIndex==D1)
            else if (MultiIndex == D2)
            {
                // 获得在标准单元微分值中的起始位置
                ind[0] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1]) * (Base->NumBase * Base->ValueDim);
                for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                {
                    Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0];
                }
            } // end if(MultiIndex==D2)
            else
            {
                printf("The MultiIndex of Base is not supported!\n");
            }
            break; // switch (worlddim)

        case 2: // switch (worlddim) 二维情形
            if (MultiIndex == D00)
            {
                // D00只有一种情况，所以可以直接复制
                memcpy(Values, BaseValuesInPointsOfRefElem, (Base->NumBase * Base->ValueDim) * sizeof(DOUBLE));
                // Base->BaseFun[0](NULL, NULL, RefCoord, Values);
            }
            else if ((MultiIndex == D10) || (MultiIndex == D01))
            {
                // 求解基函数的一阶微分的值
                ind[0] = IndMultiIndexInRefElem[0] * (Base->NumBase * Base->ValueDim);       // D10的起始位置
                ind[1] = (IndMultiIndexInRefElem[0] + 1) * (Base->NumBase * Base->ValueDim); // D01的起始位置
                switch (MultiIndex)
                {
                case D10:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D01:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1];
                    }
                    break;
                } // end switch (MultiIndex)
            }     // end for D10 and D01
            else if ((MultiIndex == D20) || (MultiIndex == D11) || (MultiIndex == D02))
            {
                // 需要计算基函数的二阶微分, 首先获得D20,D11,D02的起始位置
                ind[0] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1]) * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 1) * (Base->NumBase * Base->ValueDim);
                ind[2] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 2) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D20:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0];
                        // printf("D20 value: %f\n",Values[i]);
                    }
                    break;
                case D11:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D02:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else
            {
                printf("The MultiIndex of Base is not supported!\n");
            }
            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            if (MultiIndex == D000)
            {
                // 函数值可以直接复制
                memcpy(Values, BaseValuesInPointsOfRefElem, (Base->NumBase * Base->ValueDim) * sizeof(DOUBLE));
            }
            else if ((MultiIndex == D100) || (MultiIndex == D010) || (MultiIndex == D001))
            {
                // 获得D100, D010, D001的起始位置
                ind[0] = IndMultiIndexInRefElem[0] * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + 1) * (Base->NumBase * Base->ValueDim);
                ind[2] = (IndMultiIndexInRefElem[0] + 2) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D100:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][0];
                    }
                    break;
                case D010:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][1];
                        // printf("Values[%d]=%f, ", i, Values[i]);
                    }
                    // printf("\n");
                    break;
                case D001:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][2];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D200) || (MultiIndex == D020) || (MultiIndex == D002) ||
                     (MultiIndex == D110) || (MultiIndex == D101) || (MultiIndex == D011))
            {
                // 首先获得基函数二阶微分D200,D020,D002,D110,D101,D011的起始位置
                ind[0] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1]) * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 1) * (Base->NumBase * Base->ValueDim);
                ind[2] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 2) * (Base->NumBase * Base->ValueDim);
                ind[3] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 3) * (Base->NumBase * Base->ValueDim);
                ind[4] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 4) * (Base->NumBase * Base->ValueDim);
                ind[5] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 5) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D200:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][0] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D020:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D002:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][2] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][2] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][2] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D110:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D101:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][0] * Elem->InvJacobian[1][2];
                    }
                    break;
                case D011:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[2] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[2][2] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[4] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[0][2] + BaseValuesInPointsOfRefElem[ind[5] + i] * Elem->InvJacobian[2][1] * Elem->InvJacobian[1][2];
                    }
                    break;
                } // end for D011
            }     // end for 二阶微分的情况
            else
            {
                printf("The MultiIndex of Base is not supported!\n");
            }
            break; // switch (worlddim)
        }          // end switch (worlddim)
        break;     // break for Affine!

    case CR:
        switch (worlddim)
        {
        case 1: // switch (worlddim)

            printf("There is not 1D CR element!\n");

            break; // switch (worlddim)

        case 2: // switch (worlddim)
            if (MultiIndex == D00)
            {
                // 基函数值直接复制即可
                memcpy(Values, BaseValuesInPointsOfRefElem, (Base->NumBase * Base->ValueDim) * sizeof(DOUBLE));
                // Base->BaseFun[0](NULL, NULL, RefCoord, Values);
            }
            else if ((MultiIndex == D10) || (MultiIndex == D01))
            {
                // 获得标准单元上基函数D10和D01的起始位置
                ind[0] = IndMultiIndexInRefElem[0] * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + 1) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D10:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D01:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D20) || (MultiIndex == D11) || (MultiIndex == D02))
            {
                ind[0] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1]) * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 1) * (Base->NumBase * Base->ValueDim);
                ind[2] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 2) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D20:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D11:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D02:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else
            {
                printf("The MultiIndex of Base is not supported!\n");
            }
            DOUBLE scale[3];
            // CR元：\phi_i(x)=(d\hat{s}/ds)_i \hat{\phi}_i(\hat{x})
            // 计算三个边的长度
            scale[0] = sqrt((Elem->Vert_X[1] - Elem->Vert_X[2]) * (Elem->Vert_X[1] - Elem->Vert_X[2]) + (Elem->Vert_Y[1] - Elem->Vert_Y[2]) * (Elem->Vert_Y[1] - Elem->Vert_Y[2]));
            scale[1] = sqrt((Elem->Vert_X[0] - Elem->Vert_X[2]) * (Elem->Vert_X[0] - Elem->Vert_X[2]) + (Elem->Vert_Y[0] - Elem->Vert_Y[2]) * (Elem->Vert_Y[0] - Elem->Vert_Y[2]));
            scale[2] = sqrt((Elem->Vert_X[0] - Elem->Vert_X[1]) * (Elem->Vert_X[0] - Elem->Vert_X[1]) + (Elem->Vert_Y[0] - Elem->Vert_Y[1]) * (Elem->Vert_Y[0] - Elem->Vert_Y[1]));
            // 计算ds/d\hat{s}
            scale[0] = scale[0] / sqrt(2);
            // 对函数值进行scale
            for (i = 0; i < Base->NumBase; i++)
            {
                Values[i] = Values[i] / scale[i];
            }
            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            printf("The 3D CR element will be provided in future!\n");
            break; // switch (worlddim)
        }          // end switch (worlddim)
        break;

    case Piola:
        switch (worlddim)
        {
        case 1: // switch (worlddim)

            printf("There is not Piola type of element!\n");

            break; // switch (worlddim)

        case 2: // switch (worlddim)
            printf("come to here !!!  with info");
            if (MultiIndex == D00)
            {
                memcpy(Values, BaseValuesInPointsOfRefElem, (Base->NumBase * Base->ValueDim) * sizeof(DOUBLE));
            }
            else if ((MultiIndex == D10) || (MultiIndex == D01))
            {
                ind[0] = IndMultiIndexInRefElem[0] * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + 1) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D10:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D01:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else if ((MultiIndex == D20) || (MultiIndex == D11) || (MultiIndex == D02))
            {
                ind[0] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1]) * (Base->NumBase * Base->ValueDim);
                ind[1] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 1) * (Base->NumBase * Base->ValueDim);
                ind[2] = (IndMultiIndexInRefElem[0] + IndMultiIndexInRefElem[1] + 2) * (Base->NumBase * Base->ValueDim);
                switch (MultiIndex)
                {
                case D20:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][0] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][0] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][0];
                    }
                    break;
                case D11:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][0] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][0] * Elem->InvJacobian[1][1];
                    }
                    break;
                case D02:
                    for (i = 0; i < Base->NumBase * Base->ValueDim; i++)
                    {
                        Values[i] = BaseValuesInPointsOfRefElem[ind[0] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[0][1] + BaseValuesInPointsOfRefElem[ind[3] + i] * Elem->InvJacobian[0][1] * Elem->InvJacobian[1][1] + BaseValuesInPointsOfRefElem[ind[1] + i] * Elem->InvJacobian[1][1] * Elem->InvJacobian[1][1];
                    }
                    break;
                }
            }
            else
            {
                printf("The MultiIndex of Base is not supported!\n");
            }
            DOUBLE scale[3];
            // CR元：\phi_i(x)=(d\hat{s}/ds)_i \hat{\phi}_i(\hat{x})
            // 计算三个边的长度
            scale[0] = sqrt((Elem->Vert_X[1] - Elem->Vert_X[2]) * (Elem->Vert_X[1] - Elem->Vert_X[2]) + (Elem->Vert_Y[1] - Elem->Vert_Y[2]) * (Elem->Vert_Y[1] - Elem->Vert_Y[2]));
            scale[1] = sqrt((Elem->Vert_X[0] - Elem->Vert_X[2]) * (Elem->Vert_X[0] - Elem->Vert_X[2]) + (Elem->Vert_Y[0] - Elem->Vert_Y[2]) * (Elem->Vert_Y[0] - Elem->Vert_Y[2]));
            scale[2] = sqrt((Elem->Vert_X[0] - Elem->Vert_X[1]) * (Elem->Vert_X[0] - Elem->Vert_X[1]) + (Elem->Vert_Y[0] - Elem->Vert_Y[1]) * (Elem->Vert_Y[0] - Elem->Vert_Y[1]));
            // 计算ds/d\hat{s}
            scale[0] = scale[0] / sqrt(2);
            INT k;
            // 对函数值进行scale
            // for (i = 0; i < Base->NumBase; i++)
            // {
            //     Values[i] = Values[i] / scale[i];
            // }
            for (i = 0; i < 3; i++)
            {
                // 目前在第i条边
                for (j = 0; j < Base->DOF[1]; j++)
                {
                    // printf("DOF[1]: %d\n", Base->DOF[1]);
                    // 第i条边上的第j个基函数
                    for (k = 0; k < Base->ValueDim; k++)
                    {
                        // 第i条边上的第j个基函数的第k维
                        Values[i * Base->DOF[1] * Base->ValueDim + j * Base->ValueDim + k] /= scale[i];
                    } // end for k
                }     // end for j
            }         // end for i

            break; // switch (worlddim)
        case 3:    // switch (worlddim)
            printf("3D CR元未补充\n");
            break; // switch (worlddim)
        }          // end switch (worlddim)
        break;     // end for Piloa case
    case Div:
        break; // end for Div case.
    default:
        printf("The transformation for reference element to the real element is not supported!\n");
        break;
    } // end for switch (Base->MapType)所有可能支持的变换类型
} // end for GetBaseValueWithInfo

void AddBoundFun(DISCRETEFORM *DiscreteForm, FUNCTIONVEC *BoundaryFunction, BOUNDARYTYPE bdtype, QUADRATURE *PartialQuadrature)
{
    switch (bdtype)
    {
    case DIRICHLET:
        DiscreteForm->BoundaryFunction = BoundaryFunction;
        if (PartialQuadrature != NULL)
            DiscreteForm->PartialQuadrature = PartialQuadrature;
        break;

    case NEUMANN:
        DiscreteForm->NeumannBoundaryFunction = BoundaryFunction;
        if (PartialQuadrature != NULL)
            DiscreteForm->PartialQuadrature = PartialQuadrature;
        break;

    case ROBIN:
        DiscreteForm->RobinBoundaryFunction = BoundaryFunction;
        if (PartialQuadrature != NULL)
            DiscreteForm->PartialQuadrature = PartialQuadrature;
        break;

    default:
        RaiseError("AddBoundFun", "wrong boundarytype!");
        break;
    }
}