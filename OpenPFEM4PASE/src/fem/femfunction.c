#include "femfunction.h"

// 根据有限元空间来建立相应的有限元函数, 同时为有限元函数设置存储空间
FEMFUNCTION *FEMFunctionBuild(FEMSPACE *FEMSpace, INT num)
{
    FEMFUNCTION *femfunction = malloc(sizeof(FEMFUNCTION));
    femfunction->LocalNumDOF = FEMSpace->NumDOF;
    femfunction->GlobalNumDOF = FEMSpace->GlobalNumDOF;
    femfunction->num = num;
    femfunction->start = 0;
    femfunction->end = num;
    femfunction->Values = (DOUBLE *)calloc((femfunction->LocalNumDOF) * num, sizeof(DOUBLE));
    femfunction->FEMSpace = FEMSpace;
    return femfunction;
}
// 根据有限元空间和相应的数组建立有限元函数
FEMFUNCTION *FEMFunctionBuildWithValues(FEMSPACE *FEMSpace, DOUBLE *Values, INT num)
{
    FEMFUNCTION *femfunction = malloc(sizeof(FEMFUNCTION));
    femfunction->LocalNumDOF = FEMSpace->NumDOF;
    femfunction->GlobalNumDOF = -1;
    femfunction->num = num;
    femfunction->start = 0;
    femfunction->end = num;
    femfunction->Values = Values;
    femfunction->FEMSpace = FEMSpace;
    return femfunction;
}
// 有限元插值
void FEMInterpolation(FEMFUNCTION *femfunction, FUNCTIONVEC *fun, INT num)
{
    // printf("Do the finite element interpolation!\n");
    FEMSPACE *femspace = femfunction->FEMSpace;
    MESH *mesh = femspace->Mesh;
    BASE *base = femspace->Base;
    NODALFUNCTION *NodalFun = base->NodalFun;
    INT NumBase = base->NumBase;
    INT ValueDim = base->ValueDim;
    INT worlddim = mesh->worlddim;
    DOUBLE *elemvalues = malloc(ValueDim * NumBase * sizeof(DOUBLE));
    INT NumElements, ind, idx_fun, i, start, numelemdof;
    INT *BeginIndex = femspace->BeginIndex;
    INT *LocalIndex = femspace->LocalIndex;
    DOUBLE *Values = femfunction->Values;
    FACE face;
    VOLU volu;
    ELEMENT *elem = ElementInitial(mesh);
    switch (worlddim)
    {
    case 1:
        NumElements = mesh->num_line;
        break;
    case 2:
        NumElements = mesh->num_face;
        break;
    case 3:
        NumElements = mesh->num_volu;
        break;
    }
    INT s = femfunction->start, e = femfunction->end, localnum = femfunction->LocalNumDOF;
    if (e - s != num)
        RaiseError("FEMInterpolation", "Wrong number or start/end of femfunction");
    SetElemType(elem, femspace);
    for (idx_fun = 0; idx_fun < num; idx_fun++)
    {
        for (ind = 0; ind < NumElements; ind++)
        {
            ElementBuild(mesh, ind, elem);
            // 获得在本单元上的函数值
            NodalFun(elem, fun, ValueDim, elemvalues);
            if (elem->is_curl == 1)
                ReorderElemValuesForCurlElem(femspace, ind, elem, elemvalues);
            if (elem->is_div == 1)
                ReorderElemValuesForDivElem(femspace, ind, elem, elemvalues);
            start = BeginIndex[ind];
            numelemdof = BeginIndex[ind + 1] - start;
            for (i = 0; i < numelemdof; i++)
            {
                Values[s * localnum + LocalIndex[start + i]] = elemvalues[i];
            }
        }
        s++;
    }
    OpenPFEM_Free(elemvalues);
    ElementDestroy(&elem);
}
// 输出有限元函数
void FEMFunctionPrint(FEMFUNCTION *femfunction)
{
    printf("Output the information of the finite element function!\n");
    INT LocalNumDOF = femfunction->LocalNumDOF;
    INT i, idx_fun;
    for (idx_fun = 0; idx_fun < femfunction->num; idx_fun++)
    {
        printf("====    function %d    ====\n", idx_fun);
        for (i = 0; i < LocalNumDOF; i++)
            printf("Values(%d) = %2.8f\n", i + 1, femfunction->Values[i]);
        printf("\n");
    }
}

// 按MATLAB格式输出网格信息
void FEMFunction2DMatlabWrite(FEMFUNCTION *fefunction, char *file)
{
    FILE *fp = fopen(file, "w");
    int i, j;
    MESH *mesh = fefunction->FEMSpace->Mesh;
    /* 输出体的信息 */
    fprintf(fp, "#DataFile of 2D Finite element function to matlab!\n");
    fprintf(fp, "POINTS %d double\n", mesh->num_vert);
    // 输出节点信息
    VERT vert;

    for (i = 0; i < mesh->num_vert; i++)
    {
        vert = mesh->Verts[i];
        fprintf(fp, "%f %f  \n", vert.Coord[0], vert.Coord[1]);
    }
    /* 输出面的信息 */
    fprintf(fp, "CELLS %d %d\n", mesh->num_face, mesh->num_face * 4);
    FACE face;
    int numverts;
    for (i = 0; i < mesh->num_face; i++)
    {
        face = mesh->Faces[i];
        numverts = face.NumVerts;
        fprintf(fp, "%d %d %d %d\n", numverts,
                face.Vert4Face[0] + 1, face.Vert4Face[1] + 1, face.Vert4Face[2] + 1);
    }
    fprintf(fp, "POINT_DATA %d\n", mesh->num_vert);
    for (i = 0; i < mesh->num_vert; i++)
    {
        fprintf(fp, "%2.14f\n", fefunction->Values[i]);
    }
    fclose(fp);
}

void FEMFunctionSetRange(FEMFUNCTION *femfun, INT start, INT end)
{
    if (start < 0)
        RaiseError("FEMFunctionSetRange", "start should be greater than 0!\n");
    if (end >= femfun->num)
        RaiseError("FEMFunctionSetRange", "end couldn't be greater than or equal to the number of femfunction(s)!\n");
    if (start >= end)
        RaiseError("FEMFunctionSetRange", "start should be less than end!\n");
    femfun->start = start;
    femfun->end = end;
}

void FEMFunctionCopy(FEMFUNCTION *femfunX, FEMFUNCTION *femfunY)
{
    if (femfunX->LocalNumDOF != femfunY->LocalNumDOF)
        RaiseError("FEMFunctionCopy", "Different femspaces!\n");
    INT x_s = femfunX->start, x_e = femfunX->end;
    INT y_s = femfunY->start, y_e = femfunY->end;
    if (x_e - x_s != y_e - y_s)
        RaiseError("FEMFunctionCopy", "Different num!\n");
    memcpy(femfunY->Values + y_s * femfunX->LocalNumDOF, femfunX->Values + x_s * femfunX->LocalNumDOF, femfunX->LocalNumDOF * (x_e - x_s) * sizeof(DOUBLE));
}

void FEMFunctionAxpby(DOUBLE alpha, FEMFUNCTION *femfunX, DOUBLE beta, FEMFUNCTION *femfunY)
{
    if (femfunX->LocalNumDOF != femfunY->LocalNumDOF)
        RaiseError("FEMFunctionAxpby", "Different femspaces!\n");
    INT x_s = femfunX->start, x_e = femfunX->end;
    INT y_s = femfunY->start, y_e = femfunY->end;
    INT func_num = x_e - x_s, length = femfunX->LocalNumDOF, num = func_num * length;
    if (x_e - x_s != y_e - y_s)
        RaiseError("FEMFunctionAxpby", "Different num!\n");
#if defined(BLAS_USE)
    DOUBLE *data_x = femfunX->Values, *data_y = femfunY->Values;
    if (beta == 0.0)
    {
        memset(data_y + y_s * length, 0.0, num * sizeof(DOUBLE));
    }
    else if (beta != 1.0)
    {
        INT one = 1;
        dscal(&(num), &beta, data_y + y_s * length, &one);
    }
    if (alpha == 0.0)
    {
        return;
    }
    INT one = 1;
    daxpy(&(num), &alpha, data_x + x_s * length, &one, data_y + y_s * length, &one);
#else
    INT i;
    DOUBLE temp;
    for (i = 0; i < num; i++)
    {
        temp = alpha * femfunX->Values[i + x_s * length] + beta * femfunY->Values[i + y_s * length];
        femfunY->Values[i + y_s * length] = temp;
    }
#endif
}

// 释放有限元函数的内存空间
void FEMFunctionDestroy(FEMFUNCTION **femfunction)
{
    // 释放有限元函数中的函数值数组
    if ((*femfunction)->Values)
        OpenPFEM_Free((*femfunction)->Values);
    OpenPFEM_Free(*femfunction);
}

// FEMFUNCTIONS *FEMFunctionsBuild(FEMSPACE *FEMSpace, INT num)
// {
//     FEMFUNCTIONS *funcs = malloc(sizeof(FEMFUNCTIONS));
//     funcs->Num = num;
//     funcs->LocalNumDOF = FEMSpace->NumDOF;
//     funcs->GlobalNumDOF = FEMSpace->GlobalNumDOF;
//     funcs->Start = 0;
//     funcs->End = num;
//     funcs->Values = (DOUBLE *)calloc((funcs->LocalNumDOF * num), sizeof(DOUBLE));
//     funcs->FEMSpace = FEMSpace;
//     return funcs;
// }

// void FEMFuntionsDestroy(FEMFUNCTIONS **femfunctions)
// {
//     if ((*femfunctions)->Values)
//         OpenPFEM_Free((*femfunctions)->Values);
//     OpenPFEM_Free((*femfunctions));
// }

// 对于调整了局部编号顺序的curl单元 需要调整Nodal的顺序 将新编号调整为原编号
void ReorderElemValuesForCurlElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *ElemValues)
{
    MESH *mesh = femspace->Mesh;
    BASE *base = femspace->Base;
    INT *dof = base->DOF;
    INT NumBase = base->NumBase, Inter_Dim = base->InterpolationValueDim;
    INT worlddim = mesh->worlddim;
    DOUBLE *temp_ElemValues;
    temp_ElemValues = malloc(NumBase * sizeof(DOUBLE)); // 复制单元自由度值
    memcpy(temp_ElemValues, ElemValues, NumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderElemValuesForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new;
    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * dof[1];
        num_row_new = rowline * dof[1];
        for (rowdof = 0; rowdof < dof[1]; rowdof++)
        {
            ElemValues[num_row_old + rowdof] = temp_ElemValues[num_row_new + rowdof];
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * dof[1] + FaceIdInRefElem[rowline] * dof[2];
        num_row_new = 6 * dof[1] + rowline * dof[2];
        for (rowdof = 0; rowdof < dof[2]; rowdof++)
        {
            ElemValues[num_row_old + rowdof] = temp_ElemValues[num_row_new + rowdof];
        }
    }
    free(temp_ElemValues);
}

// 对于调整了局部编号顺序的Div单元 需要调整Nodal的顺序 将新编号调整为原编号
void ReorderElemValuesForDivElem(FEMSPACE *femspace, INT idx_elem, ELEMENT *Elem, DOUBLE *ElemValues)
{
    MESH *mesh = femspace->Mesh;
    BASE *base = femspace->Base;
    INT *dof = base->DOF;
    INT NumBase = base->NumBase, Inter_Dim = base->InterpolationValueDim;
    INT worlddim = mesh->worlddim;
    DOUBLE *temp_ElemValues;
    temp_ElemValues = malloc(NumBase * sizeof(DOUBLE)); // 复制单元自由度值
    memcpy(temp_ElemValues, ElemValues, NumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderElemValuesForDivElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT num_row_old, num_row_new;
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = FaceIdInRefElem[rowline] * dof[2];
        num_row_new = rowline * dof[2];
        for (rowdof = 0; rowdof < dof[2]; rowdof++)
        {
            ElemValues[num_row_old + rowdof] = temp_ElemValues[num_row_new + rowdof];
        }
    }
    free(temp_ElemValues);
}
