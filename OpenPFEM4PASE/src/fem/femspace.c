#include "femspace.h"
#include "allbases.h"

#define PRINT_INFO 0

INT size, rank;

FEMSPACE *FEMSpaceBuild(MESH *mesh, FEMTYPE FemType, BOUNDARYTYPEFUNCTION *BoundType)
{
    MeshAdjustForDirichletBound(mesh, BoundType);
    FEMSPACE *femspace = malloc(sizeof(FEMSPACE));
    femspace->FemType = FemType;
    femspace->Mesh = mesh;
    INT worlddim = mesh->worlddim;
    if (!BoundType)
    {
        femspace->BoundType = NULL;
    }
    else
    {
        femspace->BoundType = BoundType;
    }
    BASE *Base = BaseBuild(FemType);
    femspace->Base = Base;
    // Build the information about the degre of freedom
    switch (worlddim)
    {
    case 1:
        BuildDOF1D(femspace);
        break;
    case 2:
        BuildDOF2D(femspace);
        break;
    case 3:
        BuildDOF3D(femspace);
        break;
    }
#if MPI_USE
    BuildSharedDOF(femspace);
    BuildDOFGlobalIndex(femspace);
#endif
    femspace->delete_dirichletbd = false;
    femspace->dirichletbdnum = 0;
    femspace->dirichletbdindex = NULL;
    return femspace;
}

FEMSPACE *FEMSpaceBuildbyBase(MESH *mesh, BASE *Base, BOUNDARYTYPEFUNCTION *BoundType)
{
    FEMSPACE *femspace = malloc(sizeof(FEMSPACE));
    femspace->FemType = 0; // 直接已经有了有限元基函数, 可以不需要表示有限元名称的信息了
    femspace->Mesh = mesh;
    INT worlddim = mesh->worlddim;
    if (!BoundType)
    {
        femspace->BoundType = NULL;
    }
    else
    {
        femspace->BoundType = BoundType;
    }
    femspace->Base = Base;
    // Build the information about the degre of freedom
    switch (worlddim)
    {
    case 1:
        BuildDOF1D(femspace);
        break;
    case 2:
        BuildDOF2D(femspace);
        break;
    case 3:
        BuildDOF3D(femspace);
        break;
    }
#if MPI_USE
    BuildSharedDOF(femspace);
    BuildDOFGlobalIndex(femspace);
#endif
    femspace->delete_dirichletbd = false;
    femspace->dirichletbdnum = 0;
    femspace->dirichletbdindex = NULL;
    return femspace;
}

void BuildSharedDOF(FEMSPACE *femspace)
{
    MESH *mesh = femspace->Mesh;
    INT worlddim = mesh->worlddim;
    if (mesh->SharedInfo == NULL)
    {
        return;
    }
    BASE *Base = femspace->Base;   // 获得有限元空间的基函数，自由度分布中的DOF就来源于此
    INT num_vert = mesh->num_vert; // 点的个数
    INT num_line = mesh->num_line; // 边的个数
    INT num_face = mesh->num_face; // 面的个数
    INT *dof = Base->DOF;          // 获得Base->DOF中的起始位置
    INT i, j, k, ind, curindex;
    // 计算本进程中的总自由度个数
    INT dof_num = dof[0] * num_vert + dof[1] * num_line + dof[2] * num_face;
    INT numbaseline = dof[0] * num_vert;
    INT numbaseface = 0;
    if (worlddim == 3)
        numbaseface = numbaseline + dof[1] * num_line;

    // 下面开始处理shared信息
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    INT *size = (INT *)malloc(neignum * sizeof(INT));
    SHAREDGEO *sharedverts = mesh->SharedInfo->SharedVerts, *sharedvert;
    SHAREDGEO *sharedlines = mesh->SharedInfo->SharedLines, *sharedline;
    SHAREDGEO *sharedfaces = NULL, *sharedface = NULL;
    if (worlddim == 3)
        sharedfaces = mesh->SharedInfo->SharedFaces;

    for (i = 0; i < neignum; i++)
    {
        if (worlddim == 2)
            size[i] = dof[0] * sharedverts[i].SharedNum + dof[1] * sharedlines[i].SharedNum;
        else if (worlddim == 3)
            size[i] = dof[0] * sharedverts[i].SharedNum + dof[1] * sharedlines[i].SharedNum + dof[2] * sharedfaces[i].SharedNum;
    }
    SharedGeoCreate(&(femspace->SharedDOF), neignum, size);
    OpenPFEM_Free(size);
    SHAREDDOF *sharedDOF = NULL;
    INT *DOFOwner, *DOFIndex, *DOFSharedIndex;
    INT *Owner, *Index, *SharedIndex;
    INT basenum, sharedbasenum;

    PARADATA *paradata = NULL;
    ParaDataCreate(&paradata, mesh);
    if (worlddim == 2)
    {
        ParaDataAdd(paradata, DOMAINDATA, 1, MPI_INT, (void *)(&numbaseline));
    }
    else if (worlddim == 3)
    {
        INT numbase[2] = {numbaseline, numbaseface};
        ParaDataAdd(paradata, DOMAINDATA, 2, MPI_INT, (void *)(&numbase));
    }
    RECDATA *recdata = NULL;
    ParaDataCommunicate(paradata, &recdata);
    INT *neigbasenum = NULL;
    if (worlddim == 2)
        neigbasenum = (INT *)(recdata->SubDomianData[0]);
    else if (worlddim == 3)
        neigbasenum = (INT *)(recdata->SubDomianData[0]);

    for (i = 0; i < neignum; i++)
    {
        sharedDOF = &(femspace->SharedDOF[i]);
        DOFOwner = sharedDOF->Owner;
        DOFIndex = sharedDOF->Index;
        DOFSharedIndex = sharedDOF->SharedIndex;

        sharedvert = &(sharedverts[i]);
        Owner = sharedvert->Owner;
        Index = sharedvert->Index;
        SharedIndex = sharedvert->SharedIndex;
        // 先看点
        for (j = 0; j < sharedvert->SharedNum; j++)
        {
            for (ind = 0; ind < dof[0]; ind++)
            {
                curindex = j * dof[0] + ind;
                DOFOwner[curindex] = Owner[j];
                DOFIndex[curindex] = Index[j] * dof[0] + ind;
                DOFSharedIndex[curindex] = SharedIndex[j] * dof[0] + ind;
            }
        }
        // 再看线
        basenum = sharedvert->SharedNum * dof[0];
        sharedline = &(sharedlines[i]);
        Owner = sharedline->Owner;
        Index = sharedline->Index;
        SharedIndex = sharedline->SharedIndex;
        if (worlddim == 2)
            sharedbasenum = neigbasenum[i];
        else if (worlddim == 3)
            sharedbasenum = neigbasenum[2 * i];

        for (j = 0; j < sharedline->SharedNum; j++)
        {
            for (ind = 0; ind < dof[1]; ind++)
            {
                curindex = basenum + j * dof[1] + ind;
                DOFOwner[curindex] = Owner[j];
                DOFIndex[curindex] = numbaseline + Index[j] * dof[1] + ind;
                DOFSharedIndex[curindex] = sharedbasenum + SharedIndex[j] * dof[1] + ind;
            }
        }
        if (worlddim == 3)
        {
            basenum = sharedvert->SharedNum * dof[0] + sharedline->SharedNum * dof[1];
            sharedface = &(sharedfaces[i]);
            Owner = sharedface->Owner;
            Index = sharedface->Index;
            SharedIndex = sharedface->SharedIndex;
            sharedbasenum = neigbasenum[2 * i + 1];
            for (j = 0; j < sharedface->SharedNum; j++)
            {
                for (ind = 0; ind < dof[2]; ind++)
                {
                    curindex = basenum + j * dof[2] + ind;
                    DOFOwner[curindex] = Owner[j];
                    DOFIndex[curindex] = numbaseface + Index[j] * dof[2] + ind;
                    DOFSharedIndex[curindex] = sharedbasenum + SharedIndex[j] * dof[2] + ind;
                }
            }
        }
    }
    ParaDataDestroy(&paradata);
    RecDataDestroy(&recdata);
}

// Build the distribution of the degree of freedom for 1D finite element space
void BuildDOF1D(FEMSPACE *femspace)
{
    MESH *mesh = femspace->Mesh;
    BASE *Base = femspace->Base;   // 获得有限元空间的基函数，自由度分布中的DOF就来源于此
    INT num_vert = mesh->num_vert; // 点的个数
    INT num_line = mesh->num_line; // 边的个数
    INT *dof = Base->DOF;          // 获得Base->DOF中的起始位置
    // 计算本进程中的总自由度个数
    INT dof_num = dof[0] * num_vert + dof[1] * num_line;
    femspace->NumDOF = dof_num;
    femspace->GlobalNumDOF = -1;
#if PRINT_INFO
    OpenPFEM_RankPrint(mesh->comm, "dof_num=%d\n", dof_num);
#endif
    femspace->BeginIndex = (INT *)malloc((num_line + 1) * sizeof(INT));
    femspace->GlobalIndex = NULL;
    // 下面来计算出Localindex的长度
    LINE *Lines = mesh->Lines;
    LINE *line; // 定义一个face的对象，用来进行遍历
    // 下面来具体生成自由度的分布
    femspace->BeginIndex[0] = 0;
    INT ind, localnum;
    INT NumVerts = 2;
    for (ind = 0; ind < num_line; ind++)
    {
        line = &(Lines[ind]); // 获得目前的line的对象
        // 生成当前line的自由度个数
        localnum = dof[0] * NumVerts + dof[1];
        // 然后就得到下一个line中自由度的起始位置
        femspace->BeginIndex[ind + 1] = femspace->BeginIndex[ind] + localnum;
    }
    // 从而获得了LocalIndex的长度
    femspace->LocalIndex = (INT *)malloc(femspace->BeginIndex[num_line] * sizeof(INT));

    INT *LocalIndex = femspace->LocalIndex;
    INT *BeginIndex = femspace->BeginIndex;
    // 定义一下Vert4Face
    INT *Vert4Line;
    // 计算线上的局部自由度的起始值
    INT numbaseline = dof[0] * num_vert;

    // 下面再次对所有的face进行遍历
    INT i, j;
    INT pos, vertind, lineind, curindex;
    INT linenumvert;
    for (ind = 0; ind < num_line; ind++)
    {
        line = &(Lines[ind]);        // 获得当前的face对象
        Vert4Line = line->Vert4Line; // 获得line上的节点编号
        pos = BeginIndex[ind];       // 当前line上的自由度的起始位置
        // treat the dof on the vertices处理face上节点上的自由度分布
        if (dof[0] > 0) // 当节点上有自由度就进行如下的操作
        {
            linenumvert = NumVerts; // face上的节点个数
            for (i = 0; i < linenumvert; i++)
            {
                vertind = Vert4Line[i]; // 获得当前的节点局部编号
                // 处理节点上的自由度编号 dof[0]
                for (j = 0; j < dof[0]; j++)
                {
                    // 获得节点上目前的自由度
                    curindex = dof[0] * vertind + j;
                    // 对DOFIndex上的pos位置赋予自由度编号
                    LocalIndex[pos] = curindex;
                    pos++; // 更新在有限元自由度存储中的位置
                }          // end for(j=0;j<dof[0];j++)
            }              // end for(i=0; i<facenumvert; i++)
        }                  // if(dof[0]>0)
        // treat the dof on the line，处理当前line上的自由度
        // 获得当前面所在的宿主进程
        if (dof[1] > 0) // 需要处理面上自由度的时候才处理
        {
            for (i = 0; i < dof[1]; i++)
            {
                // 计算相应的局部和全局自由度编号
                curindex = numbaseline + dof[1] * ind + i;
                LocalIndex[pos] = curindex;
                pos++;
            } // end for(i=0;i<dof[1];i++)
        }     // end for(ind=0;ind<num_line;ind++)
    }         // end if(dof[1]>0)
} // end for BuildDOF2D

// Build the distribution of the degree of freedom
void BuildDOF2D(FEMSPACE *femspace)
{
    MESH *mesh = femspace->Mesh;
    BASE *Base = femspace->Base;   // 获得有限元空间的基函数，自由度分布中的DOF就来源于此
    INT num_vert = mesh->num_vert; // 点的个数
    INT num_line = mesh->num_line; // 边的个数
    INT num_face = mesh->num_face; // 面的个数
    INT *dof = Base->DOF;          // 获得Base->DOF中的起始位置
    // 计算本进程中的总自由度个数
    INT dof_num = dof[0] * num_vert + dof[1] * num_line + dof[2] * num_face;
    femspace->NumDOF = dof_num;
    femspace->GlobalNumDOF = -1;
#if PRINT_INFO
    OpenPFEM_RankPrint(mesh->comm, "dof_num=%d\n", dof_num);
#endif
    femspace->BeginIndex = (INT *)malloc((num_face + 1) * sizeof(INT));
    femspace->GlobalIndex = NULL;
    // 下面来计算出Localindex的长度
    FACE *Faces = mesh->Faces;
    FACE *face; // 定义一个face的对象，用来进行遍历
    LINE *Lines = mesh->Lines, *line;
    // 下面来具体生成自由度的分布
    femspace->BeginIndex[0] = 0;
    INT ind, localnum;
    for (ind = 0; ind < num_face; ind++)
    {
        face = &(Faces[ind]); // 获得目前的face的对象
        // 生成当前face的自由度个数
        localnum = dof[0] * face->NumVerts + dof[1] * face->NumLines + dof[2];
        // 然后就得到下一个face中自由度的起始位置
        femspace->BeginIndex[ind + 1] = femspace->BeginIndex[ind] + localnum;
    }
    // 从而获得了LocalIndex的长度
    femspace->LocalIndex = (INT *)malloc(femspace->BeginIndex[num_face] * sizeof(INT));

    INT *LocalIndex = femspace->LocalIndex;
    INT *BeginIndex = femspace->BeginIndex;
    // 记录一下每条边的Line的Indicator（指示子）
    INT *LineIndicator = (INT *)calloc(num_line, sizeof(INT));
    // 定义一下Vert4Face和Line4Face
    INT *Vert4Face, *Line4Face;
    // 计算线上的局部自由度的起始值
    INT numbaseline = dof[0] * num_vert;
    // 计算面上的局部自由度的起始值
    INT numbaseface = numbaseline + dof[1] * num_line;
    // 下面再次对所有的face进行遍历
    INT i, j, linevert, facevert, direct, base;
    INT pos, vertind, lineind, curindex;
    INT facenumvert, facenumline;
    INT numDOF1byDim, inddim, Inter_Dim = Base->InterpolationValueDim; // 获得基函数插值值的维数
    INT FaceLineVert[6] = {1, 2, 2, 0, 0, 1};
    for (ind = 0; ind < num_face; ind++)
    {
        face = &(Faces[ind]);        // 获得当前的face对象
        Vert4Face = face->Vert4Face; // 获得face上的节点编号
        Line4Face = face->Line4Face; // 获得face上的线的编号
        pos = BeginIndex[ind];       // 当前face上的自由度的起始位置
        // treat the dof on the vertices处理face上节点上的自由度分布
        if (dof[0] > 0) // 当节点上有自由度就进行如下的操作
        {
            facenumvert = face->NumVerts; // face上的节点个数
            for (i = 0; i < facenumvert; i++)
            {
                vertind = Vert4Face[i]; // 获得当前的节点局部编号
                // 处理节点上的自由度编号 dof[0]
                for (j = 0; j < dof[0]; j++)
                {
                    // 获得节点上目前的自由度
                    curindex = dof[0] * vertind + j;
                    // 对DOFIndex上的pos位置赋予自由度编号
                    LocalIndex[pos] = curindex;
                    pos++; // 更新在有限元自由度存储中的位置
                }          // end for(j=0;j<dof[0];j++)
            }              // end for(i=0; i<facenumvert; i++)
        }                  // if(dof[0]>0)
        // treat the dof on the lines, 处理面上的线
        if (dof[1] > 0) // 需要处理线上的自由度的时候才处理
        {
            facenumline = face->NumLines;      // face上的线的个数
            numDOF1byDim = dof[1] / Inter_Dim; // 比如三次元, inter_DIM=2的时候, 每个边上有4个自由度, numDOF1byDim=2
            for (i = 0; i < facenumline; i++)
            {
                // 第i条边
                lineind = Line4Face[i]; // 获得当前线的编号
                // 根据线的目前的LineIndicator和dof[1]的情况来按照不同的方式生成自由度编号
                // 安排自由度的时候，我们以line自己的节点来排列自由度
                //  if (LineIndicator[lineind] == 0){
                line = &(Lines[lineind]);      // 获得当前线的对象
                linevert = line->Vert4Line[0]; // 线上的起点
                facevert = face->Vert4Face[FaceLineVert[i * 2]];
                if (linevert == facevert)
                {
                    direct = 1;
                    base = 0;
                }
                else
                {
                    direct = -1;
                    base = numDOF1byDim - 1; // dof[1] - Inter_Dim;  //1
                }
                // 第一次处理该条线上的自由度
                // for (j = 0; j < dof[1]; j++)
                for (j = 0; j < numDOF1byDim; j++)
                {
                    // 判断面上的点编号与线上的编号的关系
                    // 获得目前线上的本地自由度
                    for (inddim = 0; inddim < Inter_Dim; inddim++)
                    {
                        curindex = numbaseline + dof[1] * lineind + (base + direct * j) * Inter_Dim + inddim;
                        LocalIndex[pos] = curindex;
                        pos++;
                    }
                }                           // end for(j=0;j<dof[1];j++)
                LineIndicator[lineind] = 1; // 将线标记为已经处理过了
            }                               // end for(i=0; i<facenumline;i++), 线上的处理晚辈
        }                                   // end if(dof[1]>0)
        // treat the dof on the face，处理当前face上的自由度
        // 获得当前面所在的宿主进程
        if (dof[2] > 0) // 需要处理面上自由度的时候才处理
        {
            for (i = 0; i < dof[2]; i++)
            {
                // 计算相应的局部和全局自由度编号
                curindex = numbaseface + dof[2] * ind + i;
                LocalIndex[pos] = curindex;
                pos++;
            } // end for(i=0;i<dof[2];i++)
        }     // end for(ind=0;ind<num_face;ind++)
    }         // end if(dof[2]>0)
    OpenPFEM_Free(LineIndicator);
} // end for BuildDOF2D

// Build the distribution of the degree of freedom
void BuildDOF3D(FEMSPACE *femspace)
{
    MESH *mesh = femspace->Mesh;
    int num_vert = mesh->num_vert; // 点的个数
    int num_line = mesh->num_line; // 边的个数
    int num_face = mesh->num_face; // 面的个数
    int num_volu = mesh->num_volu; // 体的个数
    BASE *Base = femspace->Base;   // 获得有限元空间的基函数，自由度分布中的DOF就来源于此
    INT *dof = &(Base->DOF[0]);    // 获得Base->DOF中的起始位置
    // 计算本进程中的总自由度个数
    INT LocalNumDOF = dof[0] * num_vert + dof[1] * num_line + dof[2] * num_face + dof[3] * num_volu;
    femspace->NumDOF = LocalNumDOF;
    // 计算全局的自由度个数
    femspace->GlobalNumDOF = -1;
#if PRINT_INFO
    OpenPFEM_RankPrint(mesh->comm, "dof_num=%d\n", LocalNumDOF);
#endif
    femspace->GlobalIndex = NULL;
    // BeginIndex中记录每个单元上自由度分布的起始位置,
    // 最后一个位置的数据也就记录了LocalIndex和GlobalIndex数据的个数
    femspace->BeginIndex = malloc((num_volu + 1) * sizeof(INT));
    // 下面来计算出Localindex的长度
    INT NumIndex = 0, ind, localnum, i, j, k, tmpind, volunumline, volunumvert, volunumface, numfacevert, pos, curindex, startindex;
    // 三维的时候，我们需要基函数的插值维数来处理在面上的自由度分布
    INT numDOF1byDim, inddim, Inter_Dim = Base->InterpolationValueDim; // 获得基函数插值值的维数
    VOLU volu;                                                         // 定义一个volu的对象，用来进行遍历
    FACE face;                                                         // 定义一个face的对象，用来进行遍历
    LINE line;                                                         // 定义一个line的对象，用来进行遍历
    VERT vert;                                                         // 定义一个vert的对象，用来进行遍历
    // 下面来具体生成自由度的分布
    femspace->BeginIndex[0] = 0;
    for (ind = 0; ind < num_volu; ind++)
    {
        // 获得目前的volu的对象
        volu = mesh->Volus[ind];
        // 生成当前四面体上的自由度个数
        localnum = dof[0] * volu.NumVerts + dof[1] * volu.NumLines + dof[2] * volu.NumFaces + dof[3];
        // 然后就得到下一个volu中自由度的起始位置
        femspace->BeginIndex[ind + 1] = femspace->BeginIndex[ind] + localnum;
    }
    // 从而获得了LocalIndex的长度
    femspace->LocalIndex = malloc(femspace->BeginIndex[num_volu] * sizeof(INT));
    // 同样也获得了GlobalIndex的长度
    femspace->GlobalIndex = NULL;
    // 获得LocalIndex的指针
    INT *LocalIndex = femspace->LocalIndex;
    // 定义一下Vert4Volu和Line4Volu
    INT *Vert4Volu, *Line4Volu, *Face4Volu;
    // 计算线上的局部自由度的起始值
    INT numbaseline = dof[0] * num_vert;
    // 计算面上的局部自由度的起始值
    INT numbaseface = numbaseline + dof[1] * num_line;
    // 计算体上的局部自由度的起始值
    INT numbasevolu = numbaseface + dof[2] * num_face;
    // 定义vert和line的局部和全局的指标
    INT vertind, lineind, faceind;
    INT LineVert[6] = {0, 0, 0, 1, 1, 2};                    // 用来记录一个四面体的六条线的起始点
    INT facevert[12] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1}; // 用来记录一个四面体的四个面的局部节点编号
    INT facelinedofnum, facecenterdofnum;                    // facelinedofnum和facecenterdofnum表示面上的自由度分布
    // 下面再次对所有的volu进行遍历
    for (ind = 0; ind < num_volu; ind++)
    {
        volu = mesh->Volus[ind];          // 获得当前的volu对象
        Vert4Volu = &(volu.Vert4Volu[0]); // 获得volu上的节点编号
        Line4Volu = &(volu.Line4Volu[0]); // 获得volu上的线的编号
        Face4Volu = &(volu.Face4Volu[0]); // 获得volu上的面的编号
        pos = femspace->BeginIndex[ind];  // 当前volu上的自由度的起始位置
        // treat the dof on the vertices,处理volu上节点上的自由度分布
        if (dof[0] > 0)
        {
            volunumvert = volu.NumVerts; // volu上的节点个数
            for (i = 0; i < volunumvert; i++)
            {
                vertind = Vert4Volu[i]; // 获得当前的节点局部编号
                vert = mesh->Verts[vertind];
                // 处理节点上的自由度编号 dof[0]
                curindex = dof[0] * vertind; // 获得当前节点上自由度编号的起始位置
                for (j = 0; j < dof[0]; j++)
                {
                    // 对LocalIndex上的pos位置赋予自由度编号
                    LocalIndex[pos] = curindex;
                    curindex++; // 更新当前节点上自由度的编号
                    pos++;      // 更新在有限元自由度存储中的位置
                }               // end for(j=0;j<dof[0];j++)
            }                   // end for(i=0; i<facenumvert; i++)
        }                       // end if(dof[0]>0)
        // treat the dof on the lines, 处理面上的线
        // 线的局部编号:[01,02,03,12,13,23], 所以这里的volunumline=6
        if (dof[1] > 0)
        {
            volunumline = volu.NumLines; // volu上的线的个数
            for (i = 0; i < volunumline; i++)
            {
                lineind = Line4Volu[i]; // 获得当前线的编号
                // printf("elem: %d, lineind: %d\n",ind,lineind);
                line = mesh->Lines[lineind]; // 获得当前线的对象
                // 首先判断该线的局部编号与全局编号是否一致
                // if (Vert4Volu[LineVert[i]] == line.Vert4Line[0])
                if (Vert4Volu[LineVert[i]] == line.Vert4Line[0] || Base->MapType == Piola || Base->MapType == Div)
                {
                    // 表示四面体上的线的起始点与线上的起始点一致
                    // 正常处理该条线上的自由度,利用与当前线同样的顺序
                    for (j = 0; j < dof[1]; j++)
                    {
                        // 获得目前线上的本地自由度
                        curindex = numbaseline + dof[1] * lineind + j;
                        LocalIndex[pos] = curindex;
                        pos++;
                    } // end for(j=0;j<dof[1];j++)
                }     // end if(Vert4Volu[LineVert[i]] == line.Vert4Line[0])
                else
                {
                    numDOF1byDim = dof[1] / Inter_Dim;
                    // 当前线使用反向顺序排列自由度
                    for (j = 0; j < numDOF1byDim; j++)
                    {
                        // 计算目前线上的局部和全局自由度编号
                        for (inddim = 0; inddim < Inter_Dim; inddim++)
                        {
                            curindex = numbaseline + dof[1] * lineind + (numDOF1byDim - 1 - j) * Inter_Dim + inddim;
                            LocalIndex[pos] = curindex;
                            pos++;
                        }

                    } // end for(j=0;j<dof[1];j++)
                }     // end else
            }         // end for(i=0; i<facenumline;i++), 线上的处理完毕
        }             // end if(dof[1]>0)
        // treat the dof on the face，处理当前face上的自由度
        // 获得当前面所在的宿主进程
        if (dof[2] > 0)
        {
            // 需要处理面上的自由度的放置位置
            volunumface = volu.NumFaces;
            if (Base->MapType == Piola || Base->MapType == Div)
            {
                // 处理该单元上的四个面, 面上的自由度以面本身的自由度安排为准
                for (i = 0; i < volunumface; i++)
                {
                    // 处理第i个面上的自由度
                    faceind = Face4Volu[i];
                    for (j = 0; j < dof[2]; j++)
                    {
                        // 计算相应的局部和全局自由度编号
                        curindex = numbaseface + dof[2] * faceind + j;
                        LocalIndex[pos] = curindex;
                        pos++;
                    }
                }
            }
            else
            {
                // 处理该单元上的四个面, 面上的自由度以面本身的自由度安排为准
                for (i = 0; i < volunumface; i++)
                {
                    // 处理第i个面上的自由度
                    faceind = Face4Volu[i];                                              // 获得第i个面的编号
                    face = mesh->Faces[faceind];                                         // 获得当前面的编号
                    numfacevert = mesh->Faces[faceind].NumVerts;                         // 获得当前面上的节点个数
                    facecenterdofnum = ((dof[2] / Inter_Dim) % numfacevert) * Inter_Dim; // 在面中心上的自由度个数
                    // OpenPFEM_Print("facecenterdofnum: %d\n", facecenterdofnum);
                    facelinedofnum = (dof[2] - facecenterdofnum) / numfacevert; // 在每条面心与节点连线上的自由度个数
                    curindex = numbaseface + dof[2] * faceind;
                    if (facecenterdofnum > 0)
                    {
                        // 处理面心的自由度
                        for (j = 0; j < facecenterdofnum; j++)
                        {
                            // 计算相应的局部和全局自由度编号
                            // curindex = startindex + j;
                            // 计算相应的局部和全局自由度编号
                            LocalIndex[pos] = curindex;
                            pos++;      // 存储位置的更新
                            curindex++; // 面上编号的更新
                        }
                    }
                    // 对当前face的三个节点中心连线进行遍历
                    if (facelinedofnum > 0)
                    {
                        startindex = numbaseface + dof[2] * faceind + facecenterdofnum;
                        for (j = 0; j < numfacevert; j++) // 对面上中心与节点连线的遍历
                        {
                            // 获得该面上的第j个节点的编号(从体上的角度来看的编号)
                            // 这意味着我们给基函数的定义时是按照体上每个面的编号来进行的
                            vertind = volu.Vert4Volu[facevert[i * 3 + j]];
                            // 得到该面上的点在参考单元上的局部编号(从面上的角度来看的编号)
                            for (k = 0; k < numfacevert; k++)
                            {
                                if (vertind == face.Vert4Face[k])
                                {
                                    tmpind = k; // 那么目前的节点在面上的局部编号是k
                                }
                            }
                            // facelinedofnum: 每个节点与中心点连线上包含的自由度个数
                            for (k = 0; k < facelinedofnum; k++)
                            {
                                // 计算相应的局部和全局自由度编号, 用face上的编号来做顺序, 这样可以保证相邻单元的统一
                                curindex = startindex + tmpind * facelinedofnum + k;
                                LocalIndex[pos] = curindex;
                                pos++;
                            }
                        } // end for(j=0;j<3;j++)
                    }     // end if(facelinedofnum > 0)
                }         // end for(i=0;i<volunumface;i++)
            }
        } // end if(dof[2]>=1)
        // 处理目前体上的自由度
        for (i = 0; i < dof[3]; i++)
        {
            // 计算相应的局部和全局自由度编号
            curindex = numbasevolu + dof[3] * ind + i;
            LocalIndex[pos] = curindex;
            pos++;
        } // end for(i=0;i<dof[3];i++)
    }     // end for(ind=0;ind<num_volu;ind++)
} // end for BuildDOF3D

BASE *BaseBuild(FEMTYPE FemType)
{
    // BASE *Base = malloc(sizeof(BASE));
    switch (FemType)
    {
    // case D_L_P0_1D:
    //     return BuildBase1D(D_L_P0_1D_Num_Bas, D_L_P0_1D_Value_Dim, D_L_P0_1D_Polydeg,
    //                        D_L_P0_1D_IsOnlyDependOnRefCoord, D_L_P0_1D_Accuracy, D_L_P0_1D_Maptype, &D_L_P0_1D_dof[0],
    //                        D_L_P0_1D_D0, D_L_P0_1D_D1, D_L_P0_1D_D2, D_L_P0_1D_Nodal, D_L_P0_1D_InterFun);
    // case C_L_P1_1D:
    //     return BuildBase1D(C_L_P1_1D_Num_Bas, C_L_P1_1D_Value_Dim, C_L_P1_1D_Polydeg,
    //                        C_L_P1_1D_IsOnlyDependOnRefCoord, C_L_P1_1D_Accuracy, C_L_P1_1D_Maptype, &C_L_P1_1D_dof[0],
    //                        C_L_P1_1D_D0, C_L_P1_1D_D1, C_L_P1_1D_D2, C_L_P1_1D_Nodal, C_L_P1_1D_InterFun);
    case C_T_P1_2D:
        return BaseBuild2D(C_T_P1_2D_Num_Bas, C_T_P1_2D_Value_Dim, C_T_P1_2D_Inter_Dim, C_T_P1_2D_Polydeg,
                           C_T_P1_2D_IsOnlyDependOnRefCoord, C_T_P1_2D_Accuracy, C_T_P1_2D_Maptype, &C_T_P1_2D_dof[0],
                           C_T_P1_2D_D00, C_T_P1_2D_D10, C_T_P1_2D_D01, C_T_P1_2D_D20, C_T_P1_2D_D11, C_T_P1_2D_D02,
                           C_T_P1_2D_Nodal, C_T_P1_2D_InterFun);
        break;
    case C_T_P2_2D:
        return BaseBuild2D(C_T_P2_2D_Num_Bas, C_T_P2_2D_Value_Dim, C_T_P2_2D_Inter_Dim, C_T_P2_2D_Polydeg,
                           C_T_P2_2D_IsOnlyDependOnRefCoord, C_T_P2_2D_Accuracy, C_T_P2_2D_Maptype, &C_T_P2_2D_dof[0],
                           C_T_P2_2D_D00, C_T_P2_2D_D10, C_T_P2_2D_D01, C_T_P2_2D_D20, C_T_P2_2D_D11, C_T_P2_2D_D02,
                           C_T_P2_2D_Nodal, C_T_P2_2D_InterFun);
        break;
    case D_T_P0_2D:
        return BaseBuild2D(D_T_P0_2D_Num_Bas, D_T_P0_2D_Value_Dim, D_T_P0_2D_Inter_Dim, D_T_P0_2D_Polydeg,
                           D_T_P0_2D_IsOnlyDependOnRefCoord, D_T_P0_2D_Accuracy, D_T_P0_2D_Maptype, &D_T_P0_2D_dof[0],
                           D_T_P0_2D_D00, D_T_P0_2D_D10, D_T_P0_2D_D01, D_T_P0_2D_D20, D_T_P0_2D_D11, D_T_P0_2D_D02,
                           D_T_P0_2D_Nodal, D_T_P0_2D_InterFun);
        break;
    case D_T_P1_2D:
        return BaseBuild2D(D_T_P1_2D_Num_Bas, D_T_P1_2D_Value_Dim, D_T_P1_2D_Inter_Dim, D_T_P1_2D_Polydeg,
                           D_T_P1_2D_IsOnlyDependOnRefCoord, D_T_P1_2D_Accuracy, D_T_P1_2D_Maptype, &D_T_P1_2D_dof[0],
                           D_T_P1_2D_D00, D_T_P1_2D_D10, D_T_P1_2D_D01, D_T_P1_2D_D20, D_T_P1_2D_D11, D_T_P1_2D_D02,
                           D_T_P1_2D_Nodal, D_T_P1_2D_InterFun);
        break;
    case C_T_P3_2D:
        return BaseBuild2D(C_T_P3_2D_Num_Bas, C_T_P3_2D_Value_Dim, C_T_P3_2D_Inter_Dim, C_T_P3_2D_Polydeg,
                           C_T_P3_2D_IsOnlyDependOnRefCoord, C_T_P3_2D_Accuracy, C_T_P3_2D_Maptype, &C_T_P3_2D_dof[0],
                           C_T_P3_2D_D00, C_T_P3_2D_D10, C_T_P3_2D_D01, C_T_P3_2D_D20, C_T_P3_2D_D11, C_T_P3_2D_D02,
                           C_T_P3_2D_Nodal, C_T_P3_2D_InterFun);
        break;
    case C_T_CR_2D:
        return BaseBuild2D(C_T_CR_2D_Num_Bas, C_T_CR_2D_Value_Dim, C_T_CR_2D_Inter_Dim, C_T_CR_2D_Polydeg,
                           C_T_CR_2D_IsOnlyDependOnRefCoord, C_T_CR_2D_Accuracy, C_T_CR_2D_Maptype, &C_T_CR_2D_dof[0],
                           C_T_CR_2D_D00, C_T_CR_2D_D10, C_T_CR_2D_D01, C_T_CR_2D_D20, C_T_CR_2D_D11, C_T_CR_2D_D02,
                           C_T_CR_2D_Nodal, C_T_CR_2D_InterFun);
        break;
    case C_T_P1_3D:
        return BaseBuild3D(C_T_P1_3D_Num_Bas, C_T_P1_3D_Value_Dim, C_T_P1_3D_Inter_Dim, C_T_P1_3D_Polydeg,
                           C_T_P1_3D_IsOnlyDependOnRefCoord, C_T_P1_3D_Accuracy, C_T_P1_3D_Maptype, &C_T_P1_3D_dof[0],
                           C_T_P1_3D_D000, C_T_P1_3D_D100, C_T_P1_3D_D010, C_T_P1_3D_D001, C_T_P1_3D_D200, C_T_P1_3D_D020, C_T_P1_3D_D002,
                           C_T_P1_3D_D110, C_T_P1_3D_D101, C_T_P1_3D_D011, C_T_P1_3D_Nodal, C_T_P1_3D_InterFun);
        break;
    case C_T_P2_3D:
        return BaseBuild3D(C_T_P2_3D_Num_Bas, C_T_P2_3D_Value_Dim, C_T_P2_3D_Inter_Dim, C_T_P2_3D_Polydeg,
                           C_T_P2_3D_IsOnlyDependOnRefCoord, C_T_P2_3D_Accuracy, C_T_P2_3D_Maptype, &C_T_P2_3D_dof[0],
                           C_T_P2_3D_D000, C_T_P2_3D_D100, C_T_P2_3D_D010, C_T_P2_3D_D001, C_T_P2_3D_D200, C_T_P2_3D_D020, C_T_P2_3D_D002,
                           C_T_P2_3D_D110, C_T_P2_3D_D101, C_T_P2_3D_D011, C_T_P2_3D_Nodal, C_T_P2_3D_InterFun);
        break;

    case C_T_P1_2D_2D:
        return BaseBuild2D(C_T_P1_2D_2D_Num_Bas, C_T_P1_2D_2D_Value_Dim, C_T_P1_2D_2D_Inter_Dim, C_T_P1_2D_2D_Polydeg,
                           C_T_P1_2D_2D_IsOnlyDependOnRefCoord, C_T_P1_2D_2D_Accuracy, C_T_P1_2D_2D_Maptype, &C_T_P1_2D_2D_dof[0],
                           C_T_P1_2D_2D_D00, C_T_P1_2D_2D_D10, C_T_P1_2D_2D_D01, C_T_P1_2D_2D_D20, C_T_P1_2D_2D_D11, C_T_P1_2D_2D_D02,
                           C_T_P1_2D_2D_Nodal, C_T_P1_2D_2D_InterFun);
        break;
    case C_T_P2_2D_2D:
        return BaseBuild2D(C_T_P2_2D_2D_Num_Bas, C_T_P2_2D_2D_Value_Dim, C_T_P2_2D_2D_Inter_Dim, C_T_P2_2D_2D_Polydeg,
                           C_T_P2_2D_2D_IsOnlyDependOnRefCoord, C_T_P2_2D_2D_Accuracy, C_T_P2_2D_2D_Maptype, &C_T_P2_2D_2D_dof[0],
                           C_T_P2_2D_2D_D00, C_T_P2_2D_2D_D10, C_T_P2_2D_2D_D01, C_T_P2_2D_2D_D20, C_T_P2_2D_2D_D11, C_T_P2_2D_2D_D02,
                           C_T_P2_2D_2D_Nodal, C_T_P2_2D_2D_InterFun);
        break;
    case C_T_P3_2D_2D:
        return BaseBuild2D(C_T_P3_2D_2D_Num_Bas, C_T_P3_2D_2D_Value_Dim, C_T_P3_2D_2D_Inter_Dim, C_T_P3_2D_2D_Polydeg,
                           C_T_P3_2D_2D_IsOnlyDependOnRefCoord, C_T_P3_2D_2D_Accuracy, C_T_P3_2D_2D_Maptype, &C_T_P3_2D_2D_dof[0],
                           C_T_P3_2D_2D_D00, C_T_P3_2D_2D_D10, C_T_P3_2D_2D_D01, C_T_P3_2D_2D_D20, C_T_P3_2D_2D_D11, C_T_P3_2D_2D_D02,
                           C_T_P3_2D_2D_Nodal, C_T_P3_2D_2D_InterFun);
        break;

    case C_T_CR_2D_2D:
        return BaseBuild2D(C_T_CR_2D_2D_Num_Bas, C_T_CR_2D_2D_Value_Dim, C_T_CR_2D_2D_Inter_Dim, C_T_CR_2D_2D_Polydeg,
                           C_T_CR_2D_2D_IsOnlyDependOnRefCoord, C_T_CR_2D_2D_Accuracy, C_T_CR_2D_2D_Maptype, &C_T_CR_2D_2D_dof[0],
                           C_T_CR_2D_2D_D00, C_T_CR_2D_2D_D10, C_T_CR_2D_2D_D01, C_T_CR_2D_2D_D20, C_T_CR_2D_2D_D11, C_T_CR_2D_2D_D02,
                           C_T_CR_2D_2D_Nodal, C_T_CR_2D_2D_InterFun);
        break;
    case C_T_P3_3D:
        return BaseBuild3D(C_T_P3_3D_Num_Bas, C_T_P3_3D_Value_Dim, C_T_P3_3D_Inter_Dim, C_T_P3_3D_Polydeg,
                           C_T_P3_3D_IsOnlyDependOnRefCoord, C_T_P3_3D_Accuracy, C_T_P3_3D_Maptype, &C_T_P3_3D_dof[0],
                           C_T_P3_3D_D000, C_T_P3_3D_D100, C_T_P3_3D_D010, C_T_P3_3D_D001, C_T_P3_3D_D200, C_T_P3_3D_D020, C_T_P3_3D_D002,
                           C_T_P3_3D_D110, C_T_P3_3D_D101, C_T_P3_3D_D011, C_T_P3_3D_Nodal, C_T_P3_3D_InterFun);
        break;
    case C_T_P4_3D:
        return BaseBuild3D(C_T_P4_3D_Num_Bas, C_T_P4_3D_Value_Dim, C_T_P4_3D_Inter_Dim, C_T_P4_3D_Polydeg,
                           C_T_P4_3D_IsOnlyDependOnRefCoord, C_T_P4_3D_Accuracy, C_T_P4_3D_Maptype, &C_T_P4_3D_dof[0],
                           C_T_P4_3D_D000, C_T_P4_3D_D100, C_T_P4_3D_D010, C_T_P4_3D_D001, C_T_P4_3D_D200, C_T_P4_3D_D020, C_T_P4_3D_D002,
                           C_T_P4_3D_D110, C_T_P4_3D_D101, C_T_P4_3D_D011, C_T_P4_3D_Nodal, C_T_P4_3D_InterFun);
        break;

    case C_T_P5_3D:
        return BaseBuild3D(C_T_P5_3D_Num_Bas, C_T_P5_3D_Value_Dim, C_T_P5_3D_Inter_Dim, C_T_P5_3D_Polydeg,
                           C_T_P5_3D_IsOnlyDependOnRefCoord, C_T_P5_3D_Accuracy, C_T_P5_3D_Maptype, &C_T_P5_3D_dof[0],
                           C_T_P5_3D_D000, C_T_P5_3D_D100, C_T_P5_3D_D010, C_T_P5_3D_D001, C_T_P5_3D_D200, C_T_P5_3D_D020, C_T_P5_3D_D002,
                           C_T_P5_3D_D110, C_T_P5_3D_D101, C_T_P5_3D_D011, C_T_P5_3D_Nodal, C_T_P5_3D_InterFun);
        break;

    case C_T_P1_3D_3D:
        return BaseBuild3D(C_T_P1_3D_3D_Num_Bas, C_T_P1_3D_3D_Value_Dim, C_T_P1_3D_3D_Inter_Dim, C_T_P1_3D_3D_Polydeg,
                           C_T_P1_3D_3D_IsOnlyDependOnRefCoord, C_T_P1_3D_3D_Accuracy, C_T_P1_3D_3D_Maptype, &C_T_P1_3D_3D_dof[0],
                           C_T_P1_3D_3D_D000, C_T_P1_3D_3D_D100, C_T_P1_3D_3D_D010, C_T_P1_3D_3D_D001, C_T_P1_3D_3D_D200, C_T_P1_3D_3D_D020, C_T_P1_3D_3D_D002,
                           C_T_P1_3D_3D_D110, C_T_P1_3D_3D_D101, C_T_P1_3D_3D_D011, C_T_P1_3D_3D_Nodal, C_T_P1_3D_3D_InterFun);
        break;

    case C_T_P2_3D_3D:
        return BaseBuild3D(C_T_P2_3D_3D_Num_Bas, C_T_P2_3D_3D_Value_Dim, C_T_P2_3D_3D_Inter_Dim, C_T_P2_3D_3D_Polydeg,
                           C_T_P2_3D_3D_IsOnlyDependOnRefCoord, C_T_P2_3D_3D_Accuracy, C_T_P2_3D_3D_Maptype, &C_T_P2_3D_3D_dof[0],
                           C_T_P2_3D_3D_D000, C_T_P2_3D_3D_D100, C_T_P2_3D_3D_D010, C_T_P2_3D_3D_D001, C_T_P2_3D_3D_D200, C_T_P2_3D_3D_D020, C_T_P2_3D_3D_D002,
                           C_T_P2_3D_3D_D110, C_T_P2_3D_3D_D101, C_T_P2_3D_3D_D011, C_T_P2_3D_3D_Nodal, C_T_P2_3D_3D_InterFun);
        break;

    case C_T_P3_3D_3D:
        return BaseBuild3D(C_T_P3_3D_3D_Num_Bas, C_T_P3_3D_3D_Value_Dim, C_T_P3_3D_3D_Inter_Dim, C_T_P3_3D_3D_Polydeg,
                           C_T_P3_3D_3D_IsOnlyDependOnRefCoord, C_T_P3_3D_3D_Accuracy, C_T_P3_3D_3D_Maptype, &C_T_P3_3D_3D_dof[0],
                           C_T_P3_3D_3D_D000, C_T_P3_3D_3D_D100, C_T_P3_3D_3D_D010, C_T_P3_3D_3D_D001, C_T_P3_3D_3D_D200, C_T_P3_3D_3D_D020, C_T_P3_3D_3D_D002,
                           C_T_P3_3D_3D_D110, C_T_P3_3D_3D_D101, C_T_P3_3D_3D_D011, C_T_P3_3D_3D_Nodal, C_T_P3_3D_3D_InterFun);
        break;

    case C_T_P4_3D_3D:
        return BaseBuild3D(C_T_P4_3D_3D_Num_Bas, C_T_P4_3D_3D_Value_Dim, C_T_P4_3D_3D_Inter_Dim, C_T_P4_3D_3D_Polydeg,
                           C_T_P4_3D_3D_IsOnlyDependOnRefCoord, C_T_P4_3D_3D_Accuracy, C_T_P4_3D_3D_Maptype, &C_T_P4_3D_3D_dof[0],
                           C_T_P4_3D_3D_D000, C_T_P4_3D_3D_D100, C_T_P4_3D_3D_D010, C_T_P4_3D_3D_D001, C_T_P4_3D_3D_D200, C_T_P4_3D_3D_D020, C_T_P4_3D_3D_D002,
                           C_T_P4_3D_3D_D110, C_T_P4_3D_3D_D101, C_T_P4_3D_3D_D011, C_T_P4_3D_3D_Nodal, C_T_P4_3D_3D_InterFun);
        break;

    case C_T_P5_3D_3D:
        return BaseBuild3D(C_T_P5_3D_3D_Num_Bas, C_T_P5_3D_3D_Value_Dim, C_T_P5_3D_3D_Inter_Dim, C_T_P5_3D_3D_Polydeg,
                           C_T_P5_3D_3D_IsOnlyDependOnRefCoord, C_T_P5_3D_3D_Accuracy, C_T_P5_3D_3D_Maptype, &C_T_P5_3D_3D_dof[0],
                           C_T_P5_3D_3D_D000, C_T_P5_3D_3D_D100, C_T_P5_3D_3D_D010, C_T_P5_3D_3D_D001, C_T_P5_3D_3D_D200, C_T_P5_3D_3D_D020, C_T_P5_3D_3D_D002,
                           C_T_P5_3D_3D_D110, C_T_P5_3D_3D_D101, C_T_P5_3D_3D_D011, C_T_P5_3D_3D_Nodal, C_T_P5_3D_3D_InterFun);
        break;

    case Curl_T_P1_3D_3D:
        return BaseBuild3D(Curl_T_P1_3D_3D_Num_Bas, Curl_T_P1_3D_3D_Value_Dim, Curl_T_P1_3D_3D_Inter_Dim, Curl_T_P1_3D_3D_Polydeg,
                           Curl_T_P1_3D_3D_IsOnlyDependOnRefCoord, Curl_T_P1_3D_3D_Accuracy, Curl_T_P1_3D_3D_Maptype, &Curl_T_P1_3D_3D_dof[0],
                           Curl_T_P1_3D_3D_D000, Curl_T_P1_3D_3D_D100, Curl_T_P1_3D_3D_D010, Curl_T_P1_3D_3D_D001, Curl_T_P1_3D_3D_D200, Curl_T_P1_3D_3D_D020, Curl_T_P1_3D_3D_D002,
                           Curl_T_P1_3D_3D_D110, Curl_T_P1_3D_3D_D101, Curl_T_P1_3D_3D_D011, Curl_T_P1_3D_3D_Nodal, Curl_T_P1_3D_3D_InterFun);
        break;

    case Curl_T_P2_3D_3D:
        return BaseBuild3D(Curl_T_P2_3D_3D_Num_Bas, Curl_T_P2_3D_3D_Value_Dim, Curl_T_P2_3D_3D_Inter_Dim, Curl_T_P2_3D_3D_Polydeg,
                           Curl_T_P2_3D_3D_IsOnlyDependOnRefCoord, Curl_T_P2_3D_3D_Accuracy, Curl_T_P2_3D_3D_Maptype, &Curl_T_P2_3D_3D_dof[0],
                           Curl_T_P2_3D_3D_D000, Curl_T_P2_3D_3D_D100, Curl_T_P2_3D_3D_D010, Curl_T_P2_3D_3D_D001, Curl_T_P2_3D_3D_D200, Curl_T_P2_3D_3D_D020, Curl_T_P2_3D_3D_D002,
                           Curl_T_P2_3D_3D_D110, Curl_T_P2_3D_3D_D101, Curl_T_P2_3D_3D_D011, Curl_T_P2_3D_3D_Nodal, Curl_T_P2_3D_3D_InterFun);
        break;

    case Curl_T_P3_3D_3D:
        return BaseBuild3D(Curl_T_P3_3D_3D_Num_Bas, Curl_T_P3_3D_3D_Value_Dim, Curl_T_P3_3D_3D_Inter_Dim, Curl_T_P3_3D_3D_Polydeg,
                           Curl_T_P3_3D_3D_IsOnlyDependOnRefCoord, Curl_T_P3_3D_3D_Accuracy, Curl_T_P3_3D_3D_Maptype, &Curl_T_P3_3D_3D_dof[0],
                           Curl_T_P3_3D_3D_D000, Curl_T_P3_3D_3D_D100, Curl_T_P3_3D_3D_D010, Curl_T_P3_3D_3D_D001, Curl_T_P3_3D_3D_D200, Curl_T_P3_3D_3D_D020, Curl_T_P3_3D_3D_D002,
                           Curl_T_P3_3D_3D_D110, Curl_T_P3_3D_3D_D101, Curl_T_P3_3D_3D_D011, Curl_T_P3_3D_3D_Nodal, Curl_T_P3_3D_3D_InterFun);
        break;

    case Curl_T_P4_3D_3D:
        return BaseBuild3D(Curl_T_P4_3D_3D_Num_Bas, Curl_T_P4_3D_3D_Value_Dim, Curl_T_P4_3D_3D_Inter_Dim, Curl_T_P4_3D_3D_Polydeg,
                           Curl_T_P4_3D_3D_IsOnlyDependOnRefCoord, Curl_T_P4_3D_3D_Accuracy, Curl_T_P4_3D_3D_Maptype, &Curl_T_P4_3D_3D_dof[0],
                           Curl_T_P4_3D_3D_D000, Curl_T_P4_3D_3D_D100, Curl_T_P4_3D_3D_D010, Curl_T_P4_3D_3D_D001, Curl_T_P4_3D_3D_D200, Curl_T_P4_3D_3D_D020, Curl_T_P4_3D_3D_D002,
                           Curl_T_P4_3D_3D_D110, Curl_T_P4_3D_3D_D101, Curl_T_P4_3D_3D_D011, Curl_T_P4_3D_3D_Nodal, Curl_T_P4_3D_3D_InterFun);
        break;

    case Curl_T_P5_3D_3D:
        return BaseBuild3D(Curl_T_P5_3D_3D_Num_Bas, Curl_T_P5_3D_3D_Value_Dim, Curl_T_P5_3D_3D_Inter_Dim, Curl_T_P5_3D_3D_Polydeg,
                           Curl_T_P5_3D_3D_IsOnlyDependOnRefCoord, Curl_T_P5_3D_3D_Accuracy, Curl_T_P5_3D_3D_Maptype, &Curl_T_P5_3D_3D_dof[0],
                           Curl_T_P5_3D_3D_D000, Curl_T_P5_3D_3D_D100, Curl_T_P5_3D_3D_D010, Curl_T_P5_3D_3D_D001, Curl_T_P5_3D_3D_D200, Curl_T_P5_3D_3D_D020, Curl_T_P5_3D_3D_D002,
                           Curl_T_P5_3D_3D_D110, Curl_T_P5_3D_3D_D101, Curl_T_P5_3D_3D_D011, Curl_T_P5_3D_3D_Nodal, Curl_T_P5_3D_3D_InterFun);
        break;

    case Div_T_P1_3D_3D:
        return BaseBuild3D(Div_T_P1_3D_3D_Num_Bas, Div_T_P1_3D_3D_Value_Dim, Div_T_P1_3D_3D_Inter_Dim, Div_T_P1_3D_3D_Polydeg,
                           Div_T_P1_3D_3D_IsOnlyDependOnRefCoord, Div_T_P1_3D_3D_Accuracy, Div_T_P1_3D_3D_Maptype, &Div_T_P1_3D_3D_dof[0],
                           Div_T_P1_3D_3D_D000, Div_T_P1_3D_3D_D100, Div_T_P1_3D_3D_D010, Div_T_P1_3D_3D_D001, Div_T_P1_3D_3D_D200, Div_T_P1_3D_3D_D020, Div_T_P1_3D_3D_D002,
                           Div_T_P1_3D_3D_D110, Div_T_P1_3D_3D_D101, Div_T_P1_3D_3D_D011, Div_T_P1_3D_3D_Nodal, Div_T_P1_3D_3D_InterFun);
        break;

    case Div_T_P2_3D_3D:
        return BaseBuild3D(Div_T_P2_3D_3D_Num_Bas, Div_T_P2_3D_3D_Value_Dim, Div_T_P2_3D_3D_Inter_Dim, Div_T_P2_3D_3D_Polydeg,
                           Div_T_P2_3D_3D_IsOnlyDependOnRefCoord, Div_T_P2_3D_3D_Accuracy, Div_T_P2_3D_3D_Maptype, &Div_T_P2_3D_3D_dof[0],
                           Div_T_P2_3D_3D_D000, Div_T_P2_3D_3D_D100, Div_T_P2_3D_3D_D010, Div_T_P2_3D_3D_D001, Div_T_P2_3D_3D_D200, Div_T_P2_3D_3D_D020, Div_T_P2_3D_3D_D002,
                           Div_T_P2_3D_3D_D110, Div_T_P2_3D_3D_D101, Div_T_P2_3D_3D_D011, Div_T_P2_3D_3D_Nodal, Div_T_P2_3D_3D_InterFun);
        break;
    }
}

void BuildDOFGlobalIndex(FEMSPACE *femspace)
{
    if (femspace->GlobalIndex != NULL)
    {
        return;
    }
    MPI_Comm comm = femspace->Mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    if (size == 1)
    {
        INT tmp_idx;
        femspace->GlobalIndex = (INT *)calloc(femspace->NumDOF, sizeof(INT));
        for (tmp_idx = 0; tmp_idx < femspace->NumDOF; tmp_idx++)
        {
            femspace->GlobalIndex[tmp_idx] = tmp_idx;
        }
        femspace->GlobalNumDOF = femspace->NumDOF;
        femspace->MyNumDOF = femspace->NumDOF;
        femspace->GDOFstart = 0;
        femspace->GDOFend = femspace->GlobalNumDOF;
        return;
    }

    // 先计算属于本地的自由度个数
    INT MyDOFNUM = femspace->NumDOF;
    INT neignum = femspace->Mesh->SharedInfo->NumNeighborRanks;
    SHAREDINFO *SharedInfo = femspace->Mesh->SharedInfo;
    SHAREDDOF *shareddof = femspace->SharedDOF;
    INT idx_neig, i, neigrank;
    INT *Owner;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = SharedInfo->NeighborRanks[idx_neig];
        Owner = shareddof[idx_neig].Owner;
        for (i = 0; i < shareddof[idx_neig].SharedNum; i++)
        {
            if (Owner[i] == neigrank)
            {
                MyDOFNUM--;
            }
        }
    }
    femspace->MyNumDOF = MyDOFNUM;

    // 计算
    INT start_g, end_g;
    MPI_Scan(&MyDOFNUM, &end_g, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&MyDOFNUM, &(femspace->GlobalNumDOF), 1, MPI_INT, MPI_SUM, comm);
    start_g = end_g - MyDOFNUM;
    femspace->GDOFstart = start_g;
    femspace->GDOFend = end_g;
#if 0
    OpenPFEM_RankPrint(comm, "global index %d - %d\n", start_g, end_g);
#endif

    femspace->GlobalIndex = (INT *)calloc(femspace->NumDOF, sizeof(INT));
    INT *gindex = femspace->GlobalIndex;

    // 标出来不属于本进程的
    INT *Index;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = SharedInfo->NeighborRanks[idx_neig];
        Owner = shareddof[idx_neig].Owner;
        Index = shareddof[idx_neig].Index;
        for (i = 0; i < shareddof[idx_neig].SharedNum; i++)
        {
            if (Owner[i] != rank)
            {
                gindex[Index[i]] = -1;
            }
        }
    }

    // 先给属于本地的编号
    INT pos = start_g;
    for (i = 0; i < femspace->NumDOF; i++)
    {
        if (!(gindex[i]))
        {
            gindex[i] = pos;
            pos++;
        }
    }

    INT *dof = femspace->Base->DOF;
    MESH *mesh = femspace->Mesh;
    PARADATA *paradata = NULL;
    ParaDataCreate(&paradata, mesh);
    ParaDataAddCustomData(paradata, femspace->SharedDOF, 1, MPI_INT, (void *)gindex);
    RECDATA *recdata = NULL;
    ParaDataSynchronize(paradata, &recdata);
    INT *gshareddof = (INT *)(recdata->CustomData[0]);

    INT j, currindex;
    INT *dataindex = recdata->CustomDataIndex[0];
    for (i = 0; i < recdata->CustomDataNum[0]; i++)
    {
        currindex = dataindex[i];
        gindex[currindex] = gshareddof[i];
    }
    ParaDataDestroy(&paradata);
    RecDataDestroy(&recdata);
}

void SharedDOFPrint(FEMSPACE *femspace, INT printrank)
{
    SHAREDINFO *sharedinfo = femspace->Mesh->SharedInfo;
    MPI_Comm_rank(femspace->Mesh->comm, &rank);
    MPI_Barrier(femspace->Mesh->comm);
    if (rank != printrank)
    {
        return;
    }
    INT i, j, k;
    SHAREDDOF *sharedDOF;
    printf("\n================================================\n");
    printf("[RANK %d] 有限元空间有%d个邻居进程: \n\t\t\t", rank, sharedinfo->NumNeighborRanks);
    for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
    {
        printf("%d\t", sharedinfo->NeighborRanks[i]);
    }
    printf("\n================================================\n");

    printf("共享自由度: \n");
    if (femspace->SharedDOF == NULL)
    {
        printf("共享自由度信息尚未生成！\n");
    }
    else
    {
        for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
        {
            sharedDOF = &(femspace->SharedDOF[i]);
            if (sharedDOF->SharedNum == 0)
            {
                printf("和%d进程不存在共享自由度\n", sharedinfo->NeighborRanks[i]);
            }
            else
            {
                printf("和%d进程共享%d个自由度,分别为\n", sharedinfo->NeighborRanks[i], sharedDOF->SharedNum);
                for (j = 0; j < sharedDOF->SharedNum; j++)
                {
                    printf("___Index=%d  SharedIndex=%d  Owner=%d  Gindex=%d\n",
                           sharedDOF->Index[j], sharedDOF->SharedIndex[j], sharedDOF->Owner[j],
                           femspace->GlobalIndex[sharedDOF->Index[j]]);
                }
            }
        }
    }
    printf("\n================================================\n");
}

// 得到边界条件信息
void GetElementBoundaryInformation(FEMSPACE *femspace, INT ind, BOUNDARYTYPE *ElemBoundaryType, INT *DirichletBDInd)
{
    INT worlddim = femspace->Mesh->worlddim;
    INT *dof = &femspace->Base->DOF[0];
    memset(ElemBoundaryType, INNER, femspace->Base->NumBase * sizeof(BOUNDARYTYPE));
    LINE line, *Lines = femspace->Mesh->Lines; // 获得线的对象
    VERT vert, *Verts = femspace->Mesh->Verts; // 获得点的对象
    DirichletBDInd[0] = 0;                     // 存储Dirichlet边条件的编号
    INT i, j, k, bdid;
    BOUNDARYTYPEFUNCTION *BdType = femspace->BoundType; // 获得有限元空间的边界信息
    BOUNDARYTYPE bdtype;                                // 记录边界类型
    FACE face, *Faces = femspace->Mesh->Faces;          // 获得面的对象

    switch (worlddim)
    {
    case 1:
    {
        line = Lines[ind];
        INT numlinevert = 2;
        if (dof[0] > 0)
        {
            for (k = 0; k < numlinevert; k++)
            {
                vert = Verts[line.Vert4Line[k]]; // 获得当前节点的对象
                bdid = vert.BD_ID;               // 获得当前节点的边界信息
                if (bdid > 0)
                {
                    bdtype = BdType(bdid);
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 表明需要进行Dirichlet边界处理
                    }                          // end if(BdType[vert.BD_ID] == DIRICHLET)
                    // 处理DIRICHLET边界节点情况
                    for (i = 0; i < dof[0]; i++)
                    {
                        ElemBoundaryType[k * dof[0] + i] = bdtype; // 记录当前节点上所有自由度的边界信息
                    }                                              // end for(i=0;i<dof[0];i++)
                }                                                  // end if(bdid>0)
            }                                                      // end for(k=0;k<numvoluvert;k++)
        }                                                          // end if(dof[0]>0)
        break;
    }
    case 2:
    {
        face = Faces[ind];               // 获得当前的面对象
        INT numfacevert = face.NumVerts; // 当前面上的节点个数
        INT numfaceline = face.NumLines; // 当前面上的线的个数
        if (dof[0] > 0)
        {
            // 处理当前面的节点上的边界信息
            for (k = 0; k < numfacevert; k++)
            {
                // 取出当前的节点
                vert = Verts[face.Vert4Face[k]]; // 获得节点对象
                bdid = vert.BD_ID;               // 获得当前节点的边界编号
                if (bdid > 0)
                {
                    // 如果是边界就进行如下的处理
                    bdtype = BdType(bdid);
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 表明需要进行Dirichlet边界处理
                    }                          // end if(BdType[vert.BD_ID] == DIRICHLET)
                    // 处理DIRICHLET边界节点情况
                    for (i = 0; i < dof[0]; i++)
                    {
                        ElemBoundaryType[k * dof[0] + i] = bdtype;
                    } // end for(i=0;i<dof[0];i++)
                }     // end if(bdid>0)
            }         // end for(k=0;k<numvoluvert;k++)
        }             // end if(dof[0]>0)
        if (dof[1] > 0)
        {
            for (k = 0; k < numfaceline; k++)
            {
                line = Lines[face.Line4Face[k]]; // 获得当前第k个边的对象
                bdid = line.BD_ID;               // 获得当前线的边界编号
                if (bdid > 0)
                {
                    // 如果是边界的话就进行处理
                    bdtype = BdType(bdid);
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 处理DIRICHLET边界边的情况，表明当前的面含有DIRIHCHLET边条件
                        // // 如果线的边界条件是DIRICHLET 强制让线的两个顶点的边界条件为DIRICHLET
                        // for (i = 0; i < dof[0]; i++)
                        // {
                        //     ElemBoundaryType[((k + 1) % numfacevert) * dof[0] + i] = DIRICHLET;
                        //     ElemBoundaryType[((k + 2) % numfacevert) * dof[0] + i] = DIRICHLET;
                        // } // end for(i=0;i<dof[0];i++)
                    } // end if(BdType[line.BD_ID]==DIRICHLET)
                    for (i = 0; i < dof[1]; i++)
                    {
                        ElemBoundaryType[dof[0] * numfacevert + k * dof[1] + i] = bdtype; // 记录当前边上自由度的边界信息
                    }                                                                     // end for(i=0;i<dof[1];i++)
                }                                                                         // end if(bdid>0)
            }                                                                             // end for(k=0;k<numvoluline;k++)
        }                                                                                 // end if(dof[1]>0)
        break;
    } // end for case 2
    case 3:
    {
        VOLU volu = femspace->Mesh->Volus[ind]; // 获得当前体的对象
        INT numvoluvert = volu.NumVerts;        // 当前体的节点个数
        INT numvoluline = volu.NumLines;        // 当前体的线的个数
        INT numvoluface = volu.NumFaces;        // 当前体的点的个数
        // INT vert4line[12] = {0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3};
        // INT line4face[12] = {4, 5, 6, 2, 3, 6, 1, 3, 5, 1, 2, 4};
        // printf("numvoluvert: %d, numvoluline: %d, numvoluface: %d\n",numvoluvert,numvoluline,numvoluface);
        if (dof[0] > 0)
        {
            for (k = 0; k < numvoluvert; k++)
            {
                vert = Verts[volu.Vert4Volu[k]];
                bdid = vert.BD_ID; // 得动节点的边界编号
                if (bdid > 0)
                {
                    bdtype = BdType(bdid);
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 表明需要进行Dirichlet边界处理
                    }                          // end if(BdType[vert.BD_ID] == DIRICHLET)
                    // 处理DIRICHLET边界节点情况
                    for (i = 0; i < dof[0]; i++)
                    {
                        ElemBoundaryType[k * dof[0] + i] = bdtype;
                    } // end for(i=0;i<dof[0];i++)
                }     // end if(bdid>0)
            }         // end for(k=0;k<numvoluvert;k++)
        }             // end if(dof[0]>0)
        if (dof[1] > 0)
        {
            for (k = 0; k < numvoluline; k++)
            {
                line = Lines[volu.Line4Volu[k]];
                bdid = line.BD_ID;
                if (bdid > 0)
                {
                    bdtype = BdType(bdid);
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 处理DIRICHLET边界边的情况
                        // // 如果边的边界条件是DIRICHLET 强制让边对应的顶点边界条件设置为DIRICHLET
                        // for (i = 0; i < dof[0]; i++)
                        // {
                        //     ElemBoundaryType[vert4line[k * 2] * dof[0] + i] = DIRICHLET;
                        //     ElemBoundaryType[vert4line[k * 2 + 1] * dof[0] + i] = DIRICHLET;
                        // } // end for(i=0;i<dof[0];i++)
                    } // end if(BdType[line.BD_ID]==DIRICHLET)
                    for (i = 0; i < dof[1]; i++)
                    {
                        ElemBoundaryType[dof[0] * numvoluvert + k * dof[1] + i] = bdtype;
                        // printf("bdtype=%d\n", bdtype);
                    } // end for(i=0;i<dof[1];i++)
                }     // end if(bdid>0)
            }         // end for(k=0;k<numvoluline;k++)
        }             // end if(dof[1]>0)
        // 然后根据localDirichletInd中的指数来进行右端项的计算
        if (dof[2] > 0)
        {
            for (k = 0; k < numvoluface; k++)
            {
                face = Faces[volu.Face4Volu[k]]; // 面对项
                bdid = face.BD_ID;               // 面的边界编号
                if (bdid > 0)
                {
                    bdtype = BdType(bdid); // 获得当前面的边界类型
                    if (bdtype == DIRICHLET || bdtype == STIFFDIRICHLET)
                    {
                        DirichletBDInd[0] = 1; // 处理DIRICHLET边界面的情况
                        // // 如果面的边界条件为DIRICHLET 将面中所有线的边界条件设为DIRICHLET
                        // for (i = 0; i < dof[1]; i++)
                        // {
                        //     ElemBoundaryType[dof[0] * numvoluvert + line4face[k * 3] * dof[1] + i] = DIRICHLET;
                        //     ElemBoundaryType[dof[0] * numvoluvert + line4face[k * 3 + 1] * dof[1] + i] = DIRICHLET;
                        //     ElemBoundaryType[dof[0] * numvoluvert + line4face[k * 3 + 2] * dof[1] + i] = DIRICHLET;
                        // } // end for(i=0;i<dof[1];i++)
                        // // 将面中所有点的边界条件设为DIRICHLET
                        // for (i = 0; i < dof[0]; i++)
                        // {
                        //     ElemBoundaryType[((k + 1) % numvoluvert) * dof[0] + i] = DIRICHLET;
                        //     ElemBoundaryType[((k + 2) % numvoluvert) * dof[0] + i] = DIRICHLET;
                        //     ElemBoundaryType[((k + 3) % numvoluvert) * dof[0] + i] = DIRICHLET;
                        // } // end for(i=0;i<dof[0];i++)
                    } // end if(BdType[face.BD_ID]==DIRICHLET)
                    for (i = 0; i < dof[2]; i++)
                    {
                        ElemBoundaryType[dof[0] * numvoluvert + dof[1] * numvoluline + k * dof[2] + i] = bdtype;
                        // printf("bdtype=%d\n", bdtype);
                    } // end for(i=0;i<dof[2];i++)
                }     // end if(bdid>0)
            }         // end for(k=0;k<numvoluface;k++)
        }             // end if(dof[2]>0)
        break;
    } // end for case 3
    } // end switch (worlddim)
} // end for this program
// 释放有限元空间所占的内存空间
void FEMSpaceDestroy(FEMSPACE **fespace)
{
    BaseDestroy(&((*fespace)->Base));
    OpenPFEM_Free((*fespace)->BeginIndex);
    OpenPFEM_Free((*fespace)->LocalIndex);
    OpenPFEM_Free((*fespace)->GlobalIndex);
    SharedGeoDestroy((*fespace)->SharedDOF);
    OpenPFEM_Free((*fespace)->SharedDOF);
    if ((*fespace)->dirichletbdnum)
        OpenPFEM_Free((*fespace)->dirichletbdindex);
    OpenPFEM_Free((*fespace));
}

// 目前只有将Elem设置为curl单元的功能
void SetElemType(ELEMENT *Elem, FEMSPACE *FemSpace)
{
    // 如果基函数为curl类型 将单元类型设置为curl
    // 利用这个条件判断是否足够?
    if (FemSpace->Base->MapType == Piola)
    {
        OpenPFEM_Print("Assemble in curl element’s way!\n");
        Elem->is_curl = 1;
    }
    else
    {
        Elem->is_curl = 0;
    }
    if (FemSpace->Base->MapType == Div)
    {
        OpenPFEM_Print("Assemble in div element’s way!\n");
        Elem->is_div = 1;
    }
    else
    {
        Elem->is_div = 0;
    }
}