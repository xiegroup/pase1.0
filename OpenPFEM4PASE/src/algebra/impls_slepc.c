#include "matvec.h"

#if defined(SLEPC_USE)

void VectorsCreateByMatrix_SLEPc(VECTORS **vectors, MATRIX *matrix, INT num)
{
    DATATYPE type = matrix->type;
    VectorsCreate(vectors, type);
    (*vectors)->nvecs = num;
    (*vectors)->local_length = matrix->local_nrows;
    (*vectors)->global_length = matrix->global_nrows;
    (*vectors)->start = matrix->rows_start;
    (*vectors)->end = matrix->rows_end;
    (*vectors)->start_cols = 0;
    (*vectors)->end_cols = num;
    MPI_Comm_dup(matrix->comm, &((*vectors)->comm));
    MPI_Comm comm;
    Mat A = (Mat)matrix->data_petsc;
    PetscObjectGetComm((PetscObject)A, &comm);
    BV bv;
    BVCreate(comm, &bv);
    BVSetType(bv, BVMAT);
    BVSetSizes(bv, matrix->local_nrows, matrix->global_nrows, num);
    BVSetActiveColumns(bv, 0, num);
    BVSetRandom(bv);
    (*vectors)->data_slepc = (void *)bv;
}

void VectorsCreateByMatrixT_SLEPc(VECTORS **vectors, MATRIX *matrix, INT num)
{
    DATATYPE type = matrix->type;
    VectorsCreate(vectors, type);
    (*vectors)->nvecs = num;
    (*vectors)->local_length = matrix->local_ncols;
    (*vectors)->global_length = matrix->global_ncols;
    (*vectors)->start = matrix->cols_start;
    (*vectors)->end = matrix->cols_end;
    (*vectors)->start_cols = 0;
    (*vectors)->end_cols = num;
    MPI_Comm_dup(matrix->comm, &((*vectors)->comm));
    MPI_Comm comm;
    Mat A = (Mat)matrix->data_petsc;
    PetscObjectGetComm((PetscObject)A, &comm);
    BV bv;
    BVCreate(comm, &bv);
    BVSetType(bv, BVMAT);
    BVSetSizes(bv, matrix->local_ncols, matrix->global_ncols, num);
    BVSetActiveColumns(bv, 0, num);
    BVSetRandom(bv);
    (*vectors)->data_slepc = (void *)bv;
}

void MatrixVectorsMult_SLEPc(MATRIX *A, VECTORS *x, VECTORS *y)
{
    if ((x->end_cols - x->start_cols) != (y->end_cols - y->start_cols))
        RaiseError("MatrixVectorsMult_SLEPc", "the number of columns of x and y are different!");
    BV bvx = (BV)(x->data_slepc), bvy = (BV)(y->data_slepc);
    Mat matA = (Mat)(A->data_petsc);
    BVSetActiveColumns(bvx, x->start_cols, x->end_cols);
    BVSetActiveColumns(bvy, y->start_cols, y->end_cols);
    BVMatMult(bvx, matA, bvy);
}

void MatrixTransposeVectorsMult_SLEPc(MATRIX *A, VECTORS *x, VECTORS *y)
{
    if ((x->end_cols - x->start_cols) != (y->end_cols - y->start_cols))
        RaiseError("MatrixVectorsMult_SLEPc", "the number of columns of x and y are different!");
    BV bvx = (BV)(x->data_slepc), bvy = (BV)(y->data_slepc);
    Mat matA = (Mat)(A->data_petsc);
    BVSetActiveColumns(bvx, x->start_cols, x->end_cols);
    BVSetActiveColumns(bvy, y->start_cols, y->end_cols);
    BVMatMultTranspose(bvx, matA, bvy);
}

void VectorsCreate_SLEPc(VECTORS *vectors)
{
    vectors->if_slepc = true;
    vectors->data_slepc = NULL;
}

void VectorsDestroy_SLEPc(VECTORS *vectors)
{
    BVDestroy((BV *)(&(vectors->data_slepc)));
    vectors->if_slepc = false;
}

void VectorsGetArray_SLEPc(VECTORS *vectors, DOUBLE **data)
{
    BV bv = (BV)vectors->data_slepc;
    BVSetActiveColumns(bv, 0, vectors->nvecs);
    BVGetArray(bv, data);
}

void VectorsRestoreArray_SLEPc(VECTORS *vectors, DOUBLE **data)
{
    BV bv = (BV)vectors->data_slepc;
    BVRestoreArray(bv, data);
}

#endif