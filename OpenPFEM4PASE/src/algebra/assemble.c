#include "assemble.h"
#include "femspace.h"
#include "meshcomm.h"

#define MATRIX_COMPRESSION 1

// 因为PETSc的add_values和insert_values不能混用
// 在组装刚度矩阵(add)和处理Dirichlet边界条件(insert)时会出现冲突
// 下述代码获取PETSc向量矩阵格式内部数据的信息来直接进行insert
// 参考版本3.17.3, 矩阵格式MPIAIJ, 其他版本会不会冲突目前未知
#if defined(PETSC_USE)
#define PetscVecInsertValue(vec, localindex, value) \
    do                                              \
    {                                               \
        DOUBLE *array = NULL;                       \
        VecGetArray((vec), &array);                 \
        array[(localindex)] = (value);              \
    } while (0)
// 下述实现需要修改的对角元位于矩阵的对角部分，非对角部分未实现
#define PetscMatInsertDiagValue(mat, local_rowindex, global_colindex, value)                                    \
    do                                                                                                          \
    {                                                                                                           \
        PetscInt cstart = (mat)->cmap->rstart, cend = (mat)->cmap->rend, col, row;                              \
        if ((global_colindex) >= cstart && (global_colindex) < cend)                                            \
        {                                                                                                       \
            col = (global_colindex)-cstart;                                                                     \
            row = (local_rowindex);                                                                             \
            Mat_MPIAIJ *aij = (Mat_MPIAIJ *)(mat)->data;                                                        \
            Mat A = aij->A;                                                                                     \
            Mat_SeqAIJ *a = (Mat_SeqAIJ *)A->data;                                                              \
            PetscInt *aimax = a->imax, *ailen = a->ilen, *ai = a->i, *aj = a->j, am = A->rmap->n;               \
            PetscInt rmax1 = aimax[row];                                                                        \
            PetscInt nrow1 = ailen[row];                                                                        \
            PetscInt high1 = nrow1, low1 = 0, t, _i;                                                            \
            PetscInt lastcol1 = col;                                                                            \
            PetscInt *rp1 = aj + ai[row];                                                                       \
            MatScalar *aa = a->a;                                                                               \
            MatScalar *ap1 = aa + ai[row];                                                                      \
            while (high1 - low1 > 5)                                                                            \
            {                                                                                                   \
                t = (low1 + high1) / 2;                                                                         \
                if (rp1[t] > col)                                                                               \
                    high1 = t;                                                                                  \
                else                                                                                            \
                    low1 = t;                                                                                   \
            }                                                                                                   \
            for (_i = low1; _i < high1; _i++)                                                                   \
            {                                                                                                   \
                if (rp1[_i] > col)                                                                              \
                    break;                                                                                      \
                if (rp1[_i] == col)                                                                             \
                {                                                                                               \
                    ap1[_i] = (value);                                                                          \
                    goto a_noinsert;                                                                            \
                }                                                                                               \
            }                                                                                                   \
            if (nrow1 >= rmax1)                                                                                 \
            {                                                                                                   \
                PetscInt CHUNKSIZE = 15, new_nz = ai[am] + CHUNKSIZE, len;                                      \
                PetscInt *new_i = (PetscInt *)malloc((am + 1) * sizeof(PetscInt));                              \
                PetscInt *new_j = (PetscInt *)malloc(new_nz * sizeof(PetscInt));                                \
                MatScalar *new_a = (MatScalar *)malloc(new_nz * sizeof(MatScalar));                             \
                PetscInt _ii;                                                                                   \
                for (_ii = 0; _ii < row + 1; _ii++)                                                             \
                {                                                                                               \
                    new_i[_ii] = ai[_ii];                                                                       \
                }                                                                                               \
                for (_ii = row + 1; _ii < am + 1; _ii++)                                                        \
                {                                                                                               \
                    new_i[_ii] = ai[_ii] + CHUNKSIZE;                                                           \
                }                                                                                               \
                memcpy(new_j, aj, (ai[row] + nrow1) * sizeof(PetscInt));                                        \
                len = (new_nz - CHUNKSIZE - ai[row] - nrow1);                                                   \
                memcpy(new_j + ai[row] + nrow1 + CHUNKSIZE, aj + ai[row] + nrow1, len * sizeof(PetscInt));      \
                memcpy(new_a, aa, (ai[row] + nrow1) * sizeof(MatScalar));                                       \
                memset(new_a + (ai[row] + nrow1), 0.0, CHUNKSIZE * sizeof(MatScalar));                          \
                memcpy(new_a + (ai[row] + nrow1 + CHUNKSIZE), aa + (ai[row] + nrow1), len * sizeof(MatScalar)); \
                if (a->free_a)                                                                                  \
                    OpenPFEM_Free(a->a);                                                                        \
                if (a->free_ij)                                                                                 \
                    OpenPFEM_Free(a->j);                                                                        \
                if (a->free_ij)                                                                                 \
                    OpenPFEM_Free(a->i);                                                                        \
                aa = new_a;                                                                                     \
                a->a = (MatScalar *)new_a;                                                                      \
                ai = a->i = new_i;                                                                              \
                aj = a->j = new_j;                                                                              \
                a->singlemalloc = PETSC_TRUE;                                                                   \
                rp1 = aj + ai[row];                                                                             \
                ap1 = aa + ai[row];                                                                             \
                rmax1 = aimax[row] = aimax[row] + CHUNKSIZE;                                                    \
                a->maxnz += CHUNKSIZE;                                                                          \
                a->reallocs++;                                                                                  \
            }                                                                                                   \
            PetscInt N = nrow1++ - 1;                                                                           \
            a->nz++;                                                                                            \
            high1++;                                                                                            \
            OpenPFEM_Move(rp1 + _i + 1, rp1 + _i, N - _i + 1);                                                  \
            OpenPFEM_Move(ap1 + _i + 1, ap1 + _i, N - _i + 1);                                                  \
            rp1[_i] = col;                                                                                      \
            ap1[_i] = value;                                                                                    \
            A->nonzerostate++;                                                                                  \
        a_noinsert:;                                                                                            \
            ailen[(local_rowindex)] = nrow1;                                                                    \
        }                                                                                                       \
        else                                                                                                    \
        {                                                                                                       \
            RaiseError("PetscMatInsertDiagValue",                                                               \
                       "Inserting a non-diagonal element into matrix.");                                        \
        }                                                                                                       \
    } while (0)
#endif
// 创建进程内部的CSR稀疏矩阵对象 **Matrix
static void CSRMatCreation(CSRMAT **Matrix)
{
    CSRMAT *matrix = (CSRMAT *)malloc(sizeof(CSRMAT));
    matrix->NumRows = 0;
    matrix->NumColumns = 0;
    matrix->NumEntries = 0;
    matrix->RowPtr = NULL;
    matrix->KCol = NULL;
    matrix->Entries = NULL;
    *Matrix = matrix;
}

// 组装刚度矩阵: type表示组装生成的数据类型，目前有两种选择：一种是我们自己的定义的矩阵格式和向量，另一种是Petsc格式的矩阵和向量
void MatrixAssemble(MATRIX **Matrix, VECTOR **Rhs, DISCRETEFORM *DiscreteForm, DATATYPE type)
{
    // 根据离散变分形式来创建并行矩阵
    MATRIX *matrix = MatrixInitial(DiscreteForm, type);
    // 初始化右端项
    VECTOR *rhs = NULL;
    if (Rhs != NULL)
    {
        rhs = RHSInitial(DiscreteForm, type);
    }
    // 定义assembleinfo的对象
    ASSEMBLE_INFO *assembleinfo = NULL;
    // 生成刚度矩阵的结构
    MatrixStructureBuild(matrix, &assembleinfo, DiscreteForm);
    // (0) 准备
    /** 目前只实现了左右网格是相同的情况 */
    // R 表示与行对应，C表示与列对应
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase, Inter_Dim = Rbase->InterpolationValueDim;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *SubEntries = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 用来存储单刚矩阵的数组
    DOUBLE *SubVec = NULL;
    // 对assembleinfo进行赋值
    assembleinfo->submat_col_diag = (bool *)malloc(CNumBase * sizeof(bool));
    assembleinfo->submat_col_index = (INT *)malloc(CNumBase * sizeof(INT));
    if (Rhs != NULL)
    {
        // 如果需要组装成向量，就申请相应的空间
        SubVec = malloc(RNumBase * sizeof(DOUBLE));
    }
    // 获得行和列的有限元空间的自由度信息
    INT *Rlocalindex = Rspace->LocalIndex;
    INT *Rglobalindex = Rspace->GlobalIndex;
    INT *Rbeginindex = Rspace->BeginIndex;
    INT *Clocalindex = Cspace->LocalIndex;
    INT *Cglobalindex = Cspace->GlobalIndex;
    INT *Cbeginindex = Cspace->BeginIndex;
    ELEMENT *Elem = NULL; // 定义一个有限元单元对象
    INT HasBoundary = 0;  // 表示是否含有边界条件
    if (Rspace->BoundType != NULL)
        HasBoundary = 1; // 如果边边界条件不是INNER就表示有边界条件了
    // (0.0) 看看是否含有辅助有限元函数AuxFEMFun, 并且建立相应的存储基函数和辅助有限元函数值的内存
    DOUBLE *AuxFEMValues = NULL;
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        // 如果使用了辅助有限元函数，那么我们就使用AuxFEMValues来存储辅助有限元函数值
        AuxFEMValues = DiscreteForm->AuxFEMFunction->AuxFEMFunValues;
    }
    // 下面的流程应该可以写成一个单独的函数
    //  (0.1)  计算本进程中要发送出去和收到的entry个数并开辟空间
    INT neignum = Rmesh->SharedInfo->NumNeighborRanks;
    INT *neigranks = Rmesh->SharedInfo->NeighborRanks;
    INT *send_ent_num = assembleinfo->send_col_num;
    INT *send_ent_num_displs = assembleinfo->send_col_num_displs;
    assembleinfo->send_total_ent_num = send_ent_num_displs[neignum - 1] + send_ent_num[neignum - 1];
    INT *recv_ent_num = assembleinfo->recv_col_num;
    INT *recv_ent_num_displs = assembleinfo->recv_col_num_displs;
    assembleinfo->recv_total_ent_num = recv_ent_num_displs[neignum - 1] + recv_ent_num[neignum - 1];
    INT idx_neig;
    INT *send_row_num = assembleinfo->send_row_num;
    INT *send_row_num_displs = assembleinfo->send_row_num_displs;
    INT *recv_row_num = assembleinfo->recv_row_num;
    INT *recv_row_num_displs = assembleinfo->recv_row_num_displs;
    if (Rhs != NULL)
    {
        // 表示含有右端项的组装
        assembleinfo->if_rhs = true;
        // 如果有rhs需要增加和行数相同个数的数据
        for (idx_neig = 0; idx_neig < neignum; idx_neig++)
        {
            send_ent_num[idx_neig] += send_row_num[idx_neig];
            send_ent_num_displs[idx_neig] += send_row_num_displs[idx_neig];
            recv_ent_num[idx_neig] += recv_row_num[idx_neig];
            recv_ent_num_displs[idx_neig] += recv_row_num_displs[idx_neig];
        }
    }
    else
    {
        assembleinfo->if_rhs = false;
    }
    INT send_total_ent_num = send_ent_num_displs[neignum - 1] + send_ent_num[neignum - 1];
    assembleinfo->send_ent_each_row = (DOUBLE *)calloc(send_total_ent_num, sizeof(DOUBLE));
    INT recv_total_ent_num = recv_ent_num_displs[neignum - 1] + recv_ent_num[neignum - 1];
    assembleinfo->recv_ent_each_row = (DOUBLE *)malloc(recv_total_ent_num * sizeof(DOUBLE));

    // (1) 边界条件处理
    LINE line;
    FACE face;
    VOLU volu;
    INT LocalNumElems; // 用来记录本进程中的单元个数
    INT numlinevert, numfacevert, numvoluvert, numfaceline, numvoluline, numvoluface;
    INT numlocalbd, DirichletBDInd;                                           // 与边界相关的数值
    INT *dof = &Rspace->Base->DOF[0];                                         // 获得自由度的分布方式
    BOUNDARYTYPE *ElemBoundaryType = malloc(RNumBase * sizeof(BOUNDARYTYPE)); // 每一个自由度所属的边界类型
    memset(ElemBoundaryType, INNER, RNumBase * sizeof(BOUNDARYTYPE));         // 赋值
    DOUBLE *BoundaryVec = malloc(RNumBase * Inter_Dim * sizeof(DOUBLE));      // 用来记录边界值的向量
    // lhc 提前计算重复信息
    ComputeBaseValuesInPointsOfRefElem(DiscreteForm);

    // int test_ind = 0;
    // lhc 23.01.28
    // 判断是否存在更细的网格 如果有按照不同方式进行组装
    if (DiscreteForm->IsUseAuxFEMSpace == 0)
    {
        // 使用Rspace和CSpace的网格进行矩阵组装
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;
        case 2:
            LocalNumElems = Rmesh->num_face; // 二维的时候，单元个数就是面的个数
            Elem = ElementInitial(Rmesh);    // 初始化有限单元
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts; // 每个面上的节点个数
                numfaceline = face.NumLines; // 每个面上的线的条数
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;
        case 3:
            LocalNumElems = Rmesh->num_volu; // 三维的时候，单元个数就是体的个数
            Elem = ElementInitial(Rmesh);    // 初始化有限单元
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts; // 每个体上的节点个数
                numvoluline = volu.NumLines; // 每个体上的线的条数
                numvoluface = volu.NumFaces; // 每个体上的面的个数
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // 230409 add
        // 增加利用变分形式判断单元类型
        SetElemType(Elem, Rspace);
        INT i, j;
        BOUNDARYTYPE *ElemBoundaryType_temp;

        if (DiscreteForm->NeumannBoundaryFunction != NULL)
            OpenPFEM_Print("组装矩阵时处理非零Neumann边界\n");
        if (DiscreteForm->RobinBoundaryFunction != NULL)
            OpenPFEM_Print("组装矩阵时处理Robin边界\n");
        // (2) 单元遍历
        INT idx_elem; // 单元编号指标
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.1) 检查本单元上的边界信息
            DirichletBDInd = 0; // 用来记录本单元是否有Dirichlet边条件
            if (HasBoundary)
            {
                // 得到本单元上测试空间自由度的边界类型
                GetElementBoundaryInformation(Rspace, idx_elem, ElemBoundaryType, &DirichletBDInd);
                // curl单元要调整ElemBoundaryType到新编号对应顺序
                ElemBoundaryType_temp = malloc(RNumBase * sizeof(BOUNDARYTYPE));
                memcpy(ElemBoundaryType_temp, ElemBoundaryType, RNumBase * sizeof(BOUNDARYTYPE));
                if (Elem->is_curl == 1)
                    ReorderBoundaryTypeForCurlElem(DiscreteForm, idx_elem, Elem, ElemBoundaryType_temp);
                // if (Elem->is_curl == 1)
                //     ReorderBoundaryTypeForDivElem(DiscreteForm, idx_elem, Elem, ElemBoundaryType_temp);
            }
            // (2.2) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem, ElemBoundaryType_temp, SubEntries, SubVec);
            free(ElemBoundaryType_temp);
            // 这里替换为 如果Elem是curl的 调整SubEntries和SubVec的顺序和符号
            if (Elem->is_curl == 1)
                ReorderSubMatrixForCurlElem(DiscreteForm, idx_elem, Elem, SubEntries, SubVec);
            // 这里替换为 如果Elem是div的 调整SubEntries和SubVec的顺序和符号
            if (Elem->is_div == 1)
                ReorderSubMatrixForDivElem(DiscreteForm, idx_elem, Elem, SubEntries, SubVec);
            // // 输出单刚矩阵

            // if (test_ind == 0 && DirichletBDInd == 0)
            // {
            //     OpenPFEM_Print("elem[%d]\n", idx_elem);
            //     for (int ii = 0; ii < RNumBase; ii++)
            //     {
            //         for (int jj = 0; jj < CNumBase; jj++)
            //         {
            //             if (fabs(SubEntries[ii * CNumBase + jj]) > 1e-14)
            //                 OpenPFEM_Print("SubEntries[%d][%d] = %2.10f\n", ii, jj, SubEntries[ii * CNumBase + jj]);
            //         }
            //         if (fabs(SubVec[ii]) > 1e-14)
            //             OpenPFEM_Print("SubVec[%d] = %2.10f\n", ii, SubVec[ii]);
            //     }
            //     test_ind = 1;
            // }
            // (2.3) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_elem],
                               CNumBase, Clocalindex + Cbeginindex[idx_elem]);
            // (2.4) 处理右端项
            if (Rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_elem]);
            }
            // (2.5) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDInd == 1)
            {
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem,
                                          numlocalbd, BoundaryVec, ElemBoundaryType);
            }
        }
    }
    else
    {
        // 如果辅助有限元函数使用的是更细的网格的话，就以该细网格进行刚度矩阵的组装
        OpenPFEM_Print("MatrixAssemble with finer mesh!\n");
        // // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        // ComputeInvJacobiOfAllElem(Rmesh);
        // 细网格
        MESH *MeshInAssemble = DiscreteForm->LeftSpaceInAssemble->Mesh; // 获得辅助空间中的细网格
        // 细网格对应的信息
        INT LocalNumElems_finer;    // 存储细网格中的单元个数
        ELEMENT *Elem_finer = NULL; // 定义细网格上的单元
        // 获取网格的单元信息和自由度边界信息
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            LocalNumElems_finer = MeshInAssemble->num_line;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;

        case 2:
            LocalNumElems = Rmesh->num_face;
            LocalNumElems_finer = MeshInAssemble->num_face;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts;
                numfaceline = face.NumLines;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;

        case 3:
            LocalNumElems = Rmesh->num_volu;
            LocalNumElems_finer = MeshInAssemble->num_volu;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts;
                numvoluline = volu.NumLines;
                numvoluface = volu.NumFaces;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // 增加利用变分形式判断单元类型
        SetElemType(Elem, Rspace);
        // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        if (Elem->is_curl == 1 || Elem->is_div == 1)
        {
            ComputeInvJacobiOfAllElem(Rmesh, 1);
        }
        else
        {
            ComputeInvJacobiOfAllElem(Rmesh, 0);
        }

        // (2.0)首先提前计算粗网格Rmesh中每个单元的边界信息ElemBoundaryType和DirichletBDInd,
        // 并统一存储在BoundaryTypeOfAllElems和DirichletBDIndOfAllElems中
        BOUNDARYTYPE *BoundaryTypeOfAllElems = malloc(LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        memset(BoundaryTypeOfAllElems, INNER, LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        INT *DirichletBDIndOfAllElems = malloc(LocalNumElems * sizeof(INT));
        memset(DirichletBDIndOfAllElems, 0, LocalNumElems * sizeof(INT));
        // 对粗网格Rmesh进行单元循环 填充上述信息
        INT idx_elem, idx_related;
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.0.1) 检查本单元上的边界信息
            if (HasBoundary)
            {
                GetElementBoundaryInformation(Rspace, idx_elem, BoundaryTypeOfAllElems + idx_elem * RNumBase,
                                              &(DirichletBDIndOfAllElems[idx_elem]));
            }
        }
        // (2.1)对细网格进行单元循环 完成刚度矩阵的组装
        for (idx_elem = 0; idx_elem < LocalNumElems_finer; idx_elem++)
        {
            // (2.1.0) 建立当前的有限单元
            ElementBuild(MeshInAssemble, idx_elem, Elem_finer);
            // 找到对应粗网格中的大单元的编号
            // idx_related = MeshInAssemble->Ancestors[idx_elem];
            if (DiscreteForm->Relates == NULL)
            {
                idx_related = idx_elem;
            }
            else
            {
                idx_related = DiscreteForm->Relates[idx_elem];
            }
            // (2.1.1) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem_finer, BoundaryTypeOfAllElems + idx_related * RNumBase,
                              SubEntries, SubVec);
            // (2.1.2) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_related],
                               CNumBase, Clocalindex + Cbeginindex[idx_related]);
            // (2.1.3) 处理右端项
            if (Rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_related]);
            }
        }
        // (2.2)对粗网格单元进行单元循环 进行边界自由度对角元素的处理
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.2.1) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDIndOfAllElems[idx_elem] == 1)
            {
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem, numlocalbd, BoundaryVec,
                                          BoundaryTypeOfAllElems + idx_elem * RNumBase);
            }
        }
    } // 组装局部的CSR矩阵完成，接下来组装成全局刚度矩阵
    // 下面这个通讯过程最好是能写成一个功能函数的形式，这样可以在其他的地方使用
    //  (3.1) 开始通信
    MPI_Comm comm = Rmesh->SharedInfo->neighborcomm;
    MPI_Request entreq;
    DOUBLE *send_ent_each_row = assembleinfo->send_ent_each_row;
    DOUBLE *recv_ent_each_row = assembleinfo->recv_ent_each_row;
    MPI_Ineighbor_alltoallv(send_ent_each_row, send_ent_num, send_ent_num_displs, MPI_DOUBLE,
                            recv_ent_each_row, recv_ent_num, recv_ent_num_displs, MPI_DOUBLE, comm, &entreq);
    MPI_Wait(&entreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&entreq);

    // (3.2) 把收到的信息组装上去
    INT rows_start = matrix->rows_start, rows_end = matrix->rows_end;
    INT *row_globalindex = Rspace->GlobalIndex;
    INT *recv_row_index = assembleinfo->recv_row_index;
    INT *recv_entnum_each_row = assembleinfo->recv_colnum_each_row;
    INT *recv_colindex_each_row = assembleinfo->recv_colindex_each_row;
    INT start1 = 0, start2 = 0, position, entnum, idx_row, rowindex_local;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        position = recv_row_num_displs[idx_neig];
        start2 = recv_ent_num_displs[idx_neig];
        // 矩阵部分
        for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
        {
            rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
            entnum = recv_entnum_each_row[position];
            MatrixAddRowMatrix(matrix, entnum, rowindex_local, &(recv_colindex_each_row[start1]),
                               &(recv_ent_each_row[start2]));
            position++;
            start1 += entnum;
            start2 += entnum;
        }
        // 向量部分
        if (Rhs != NULL)
        {
            if (DataTypeOpenPFEMEqual(type))
            {
                position = recv_row_num_displs[idx_neig];
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
                    rhs->data[rowindex_local] += recv_ent_each_row[start2];
                    position++;
                    start2++;
                }
            }
            else if (DataTypePetscEqual(type))
            {
#if defined(PETSC_USE)
                position = recv_row_num_displs[idx_neig];
                Vec vec = (Vec)(rhs->data_petsc);
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]];
                    VecSetValue(vec, rowindex_local, recv_ent_each_row[start2], ADD_VALUES);
                    position++;
                    start2++;
                }
#else
                RaisePetscError("AssembleMatrix");
#endif
            }
        }
    }
    if (DataTypeOpenPFEMEqual(type))
    {
        // 最后压缩+整理非对角部分
        MatrixCompress(matrix);
        MatrixComplete(matrix);
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        MatAssemblyBegin((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        if (Rhs != NULL)
        {
            VecAssemblyBegin((Vec)(rhs->data_petsc));
            VecAssemblyEnd((Vec)(rhs->data_petsc));
        }
#else
        RaisePetscError("AssembleMatrix");
#endif
    }
    ElementDestroy(&Elem);
    OpenPFEM_Free(SubEntries);
    OpenPFEM_Free(SubVec);
    OpenPFEM_Free(ElemBoundaryType);
    OpenPFEM_Free(BoundaryVec);
    OpenPFEM_Free(assembleinfo->row_owner);
    OpenPFEM_Free(assembleinfo->row_owner_index);
    OpenPFEM_Free(assembleinfo->submat_col_diag);
    OpenPFEM_Free(assembleinfo->submat_col_index);
    OpenPFEM_Free(assembleinfo->send_row_num_displs);
    OpenPFEM_Free(assembleinfo->send_colnum_each_row_displs);
    OpenPFEM_Free(assembleinfo->send_colindex_each_row);
    OpenPFEM_Free(assembleinfo->send_ent_each_row);
    OpenPFEM_Free(assembleinfo->send_row_num);
    OpenPFEM_Free(assembleinfo->send_col_num);
    OpenPFEM_Free(assembleinfo->send_col_num_displs);
    OpenPFEM_Free(assembleinfo->recv_row_num);
    OpenPFEM_Free(assembleinfo->recv_row_num_displs);
    OpenPFEM_Free(assembleinfo->recv_col_num);
    OpenPFEM_Free(assembleinfo->recv_col_num_displs);
    OpenPFEM_Free(assembleinfo->recv_ent_each_row);
    OpenPFEM_Free(assembleinfo->recv_row_index);
    OpenPFEM_Free(assembleinfo->recv_colnum_each_row);
    OpenPFEM_Free(assembleinfo->recv_colindex_each_row);
    OpenPFEM_Free(assembleinfo);
    *Matrix = matrix;
    if (Rhs != NULL)
    {
        *Rhs = rhs;
    }
} // end MatrixAssemble

void MatrixComplete(MATRIX *matrix)
{
    // (1) 统计一下有多少不同的列
    INT entries_totalnum = matrix->ndiag->NumEntries;
    INT *ndiag_kcol = matrix->ndiag->KCol;
    INT *kcolcopy = (INT *)malloc(entries_totalnum * sizeof(INT));
    memcpy(kcolcopy, ndiag_kcol, entries_totalnum * sizeof(INT));
    INT *position = (INT *)malloc(entries_totalnum * sizeof(INT));
    INT idx_col;
    for (idx_col = 0; idx_col < entries_totalnum; idx_col++)
    {
        position[idx_col] = idx_col;
    }
    QuickSortIntValueVector(kcolcopy, position, 0, entries_totalnum - 1);
    INT current = -1, final_num = 0;
    for (idx_col = 0; idx_col < entries_totalnum; idx_col++)
    {
        if (current < kcolcopy[idx_col])
        {
            current = kcolcopy[idx_col];
            final_num++;
        }
    }
    INT *ndiag_globalcols = (INT *)malloc(final_num * sizeof(INT));
    current = -1, final_num = 0;
    for (idx_col = 0; idx_col < entries_totalnum; idx_col++)
    {
        if (current < kcolcopy[idx_col])
        {
            current = kcolcopy[idx_col];
            ndiag_globalcols[final_num] = current;
            final_num++;
        }
        ndiag_kcol[position[idx_col]] = final_num - 1;
    }
    matrix->ndiag_globalcols = ndiag_globalcols;
    matrix->ndiag->NumColumns = final_num;
    matrix->mult_recvtotalnum = final_num;
    matrix->mult_recvtmp = (DOUBLE *)malloc(final_num * sizeof(DOUBLE));
    OpenPFEM_Free(position);
    OpenPFEM_Free(kcolcopy);
    // (2) 统计两两进程之间的通信情况
    INT globalsize, myrank;
    MPI_Comm comm = matrix->comm;
    MPI_Comm_size(comm, &globalsize);
    MPI_Comm_rank(comm, &myrank);
    INT *col_start_global = (INT *)malloc((globalsize + 1) * sizeof(INT));
    INT col_start = matrix->cols_start;
    MPI_Allgather(&col_start, 1, MPI_INT, col_start_global, 1, MPI_INT, comm);
    col_start_global[globalsize] = matrix->global_ncols;
    bool *entries_from = (bool *)calloc(globalsize, sizeof(bool));
    bool *entries_to = (bool *)malloc(globalsize * sizeof(bool));
    INT idx_rank;
    current = 0;
    if (final_num == 0)
        goto finish_communication_check;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        while (ndiag_globalcols[current] >= col_start_global[idx_rank] &&
               ndiag_globalcols[current] < col_start_global[idx_rank + 1])
        {
            entries_from[idx_rank] = true;
            current++;
            if (current == final_num)
                goto finish_communication_check;
        }
    }
finish_communication_check:
    MPI_Alltoall(entries_from, 1, MPI_C_BOOL, entries_to, 1, MPI_C_BOOL, comm);
    MPI_Comm mult_comm = MPI_COMM_NULL;
    MPI_Comm inverse_mult_comm = MPI_COMM_NULL;
    INT incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
            incount++;
        if (entries_to[idx_rank])
            outcount++;
    }
    INT *inrank = (INT *)malloc(incount * sizeof(INT));
    INT *outrank = (INT *)malloc(outcount * sizeof(INT));
    incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            inrank[incount] = idx_rank;
            incount++;
        }
        if (entries_to[idx_rank])
        {
            outrank[outcount] = idx_rank;
            outcount++;
        }
    }
    MPI_Dist_graph_create_adjacent(comm, incount, inrank, MPI_UNWEIGHTED, outcount, outrank,
                                   MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &mult_comm);
    MPI_Dist_graph_create_adjacent(comm, outcount, outrank, MPI_UNWEIGHTED, incount, inrank,
                                   MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &inverse_mult_comm);
    matrix->mult_comm = mult_comm;
    matrix->inverse_mult_comm = inverse_mult_comm;
    OpenPFEM_Free(inrank);
    OpenPFEM_Free(outrank);
    // 确认两两之间要发送哪些行
    INT *num_entries_from = (INT *)calloc(incount, sizeof(INT));
    INT *num_entries_to = (INT *)malloc(outcount * sizeof(INT));
    current = 0;
    INT current_rank = 0;
    MPI_Request numreq;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            while (ndiag_globalcols[current] >= col_start_global[idx_rank] &&
                   ndiag_globalcols[current] < col_start_global[idx_rank + 1])
            {
                num_entries_from[current_rank]++;
                current++;
                if (current == final_num)
                    goto finish_num_check;
            }
            current_rank++;
        }
    }
finish_num_check:
    MPI_Ineighbor_alltoall(num_entries_from, 1, MPI_INT, num_entries_to, 1, MPI_INT, inverse_mult_comm, &numreq);
    INT *which_entries_from = ndiag_globalcols;
    INT *num_entries_from_displs = (INT *)malloc(incount * sizeof(INT));
    num_entries_from_displs[0] = 0;
    INT i;
    for (i = 1; i < incount; i++)
    {
        num_entries_from_displs[i] = num_entries_from_displs[i - 1] + num_entries_from[i - 1];
    }
    MPI_Wait(&numreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&numreq);
    INT *num_entries_to_displs = (INT *)malloc(outcount * sizeof(INT));
    num_entries_to_displs[0] = 0;
    for (i = 1; i < outcount; i++)
    {
        num_entries_to_displs[i] = num_entries_to_displs[i - 1] + num_entries_to[i - 1];
    }
    final_num = num_entries_to_displs[outcount - 1] + num_entries_to[outcount - 1];
    INT *which_entries_to = (INT *)malloc(final_num * sizeof(INT));
    MPI_Neighbor_alltoallv(which_entries_from, num_entries_from, num_entries_from_displs, MPI_INT,
                           which_entries_to, num_entries_to, num_entries_to_displs, MPI_INT, inverse_mult_comm);
    for (i = 0; i < final_num; i++)
    {
        which_entries_to[i] -= col_start;
    }
    matrix->mult_sendindex = which_entries_to;
    matrix->mult_sendnum = num_entries_to;
    matrix->mult_recvnum = num_entries_from;
    matrix->mult_sendnum_displs = num_entries_to_displs;
    matrix->mult_recvnum_displs = num_entries_from_displs;

    OpenPFEM_Free(entries_from);
    OpenPFEM_Free(entries_to);
    OpenPFEM_Free(col_start_global);
    matrix->mult_sendtotalnum = final_num;
    matrix->mult_sendtmp = (DOUBLE *)malloc(final_num * sizeof(DOUBLE));
}

// 删除矩阵中与DIRICHLET边条件对应的行和列
void MatrixDeleteDirichletBoundary(MATRIX *matrix, DISCRETEFORM *DiscreteForm)
{
    if (!(DataTypeOpenPFEMEqual(matrix->type)))
        RaiseError("MatrixDeleteDirichletBoundary", "the type of matrix must be OpenPFEM.");
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    if (Rspace != Cspace)
        RaiseError("MatrixDeleteDirichletBoundary", "different col & row spaces not implemented.");

    bool *if_delete = (bool *)calloc(matrix->local_nrows, sizeof(bool));
    INT *newindex_diag = (INT *)malloc(matrix->local_nrows * sizeof(INT));
    if (!(Rspace->delete_dirichletbd))
    {
        // find the boundary dofs
        INT worlddim = Rmesh->worlddim;
        if (!(Rspace->BoundType))
            return;
        INT elem_num = 0, bd4elem_num = 0;
        INT *dof = &Rspace->Base->DOF[0];
        switch (worlddim)
        {
        case 1:
            elem_num = Rmesh->num_line;
            bd4elem_num = dof[0] * 2;
            break;

        case 2:
            elem_num = Rmesh->num_face;
            bd4elem_num = dof[0] * Rmesh->Faces[0].NumVerts + dof[1] * Rmesh->Faces[0].NumLines;
            break;

        case 3:
            elem_num = Rmesh->num_volu;
            bd4elem_num = dof[0] * Rmesh->Volus[0].NumVerts + dof[1] * Rmesh->Volus[0].NumLines +
                          dof[2] * Rmesh->Volus[0].NumFaces;
            break;
        }

        INT idx_elem, index1, index2, indexfinal, i, j, DirichletBDInd, RNumBase = Rbase->NumBase;
        VERT *vert, *verts = Rmesh->Verts;
        LINE *line, *lines = Rmesh->Lines;
        FACE *face, *faces = Rmesh->Faces;
        VOLU *volu, *volus = Rmesh->Volus;
        BOUNDARYTYPEFUNCTION *BdType = Rspace->BoundType;
        INT *localindex = Rspace->LocalIndex, *beginindex = Rspace->BeginIndex,
            *globalindex = Rspace->GlobalIndex, *index4elem;
        INT start = matrix->rows_start, end = matrix->rows_end;
        INT numfacevert, numfaceline, numvoluvert, numvoluline, numvoluface;
        switch (worlddim)
        {
        case 1:
            if (dof[0])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    line = lines + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < 2; i++)
                    {
                        vert = verts + line->Vert4Line[i];
                        if (vert->BD_ID)
                        {
                            if (BdType(vert->BD_ID) == DIRICHLET || BdType(vert->BD_ID) == STIFFDIRICHLET || BdType(vert->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[0]; j++)
                                {
                                    index1 = i * dof[0] + j;     // 在本单元内的自由度编号
                                    index2 = index4elem[index1]; // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                        if_delete[indexfinal - start] = true;
                                }
                            }
                        }
                    }
                }
            }
            break;

        case 2:
            numfacevert = faces[0].NumVerts;
            numfaceline = faces[0].NumLines;
            if (dof[0])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    face = faces + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < numfacevert; i++)
                    {
                        vert = verts + face->Vert4Face[i];
                        if (vert->BD_ID)
                        {
                            if (BdType(vert->BD_ID) == DIRICHLET || BdType(vert->BD_ID) == STIFFDIRICHLET || BdType(vert->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[0]; j++)
                                {
                                    index1 = i * dof[0] + j;     // 在本单元内的自由度编号
                                    index2 = index4elem[index1]; // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                        if_delete[indexfinal - start] = true;
                                }
                            }
                        }
                    }
                }
            }
            if (dof[1])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    face = faces + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < numfaceline; i++)
                    {
                        line = lines + face->Line4Face[i];
                        if (line->BD_ID)
                        {
                            if (BdType(line->BD_ID) == DIRICHLET || BdType(line->BD_ID) == STIFFDIRICHLET || BdType(line->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[1]; j++)
                                {
                                    index1 = dof[0] * numfacevert + i * dof[0] + j; // 在本单元内的自由度编号
                                    index2 = index4elem[index1];                    // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                        if_delete[indexfinal - start] = true;
                                }
                            }
                        }
                    }
                }
            }
            break;

        case 3:
            numvoluvert = volus[0].NumVerts;
            numvoluline = volus[0].NumLines;
            numvoluface = volus[0].NumFaces;
            if (dof[0])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    volu = volus + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < numvoluvert; i++)
                    {
                        vert = verts + volu->Vert4Volu[i];
                        if (vert->BD_ID)
                        {
                            if (BdType(vert->BD_ID) == DIRICHLET || BdType(vert->BD_ID) == STIFFDIRICHLET || BdType(vert->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[0]; j++)
                                {
                                    index1 = i * dof[0] + j;     // 在本单元内的自由度编号
                                    index2 = index4elem[index1]; // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                        if_delete[indexfinal - start] = true;
                                }
                            }
                        }
                    }
                }
            }
            if (dof[1])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    volu = volus + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < numvoluline; i++)
                    {
                        line = lines + volu->Line4Volu[i];
                        if (line->BD_ID)
                        {
                            if (BdType(line->BD_ID) == DIRICHLET || BdType(line->BD_ID) == STIFFDIRICHLET || BdType(line->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[1]; j++)
                                {
                                    index1 = dof[0] * numvoluvert + i * dof[1] + j; // 在本单元内的自由度编号
                                    index2 = index4elem[index1];                    // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                        if_delete[indexfinal - start] = true;
                                }
                            }
                        }
                    }
                }
            }
            if (dof[2])
            {
                for (idx_elem = 0; idx_elem < elem_num; idx_elem++)
                {
                    volu = volus + idx_elem;
                    index4elem = localindex + beginindex[idx_elem];
                    for (i = 0; i < numvoluface; i++)
                    {
                        face = faces + volu->Face4Volu[i];
                        if (face->BD_ID)
                        {
                            if (BdType(face->BD_ID) == DIRICHLET || BdType(face->BD_ID) == STIFFDIRICHLET || BdType(face->BD_ID) == MASSDIRICHLET)
                            {
                                for (j = 0; j < dof[2]; j++)
                                {
                                    index1 = dof[0] * numvoluvert + dof[1] * numvoluline + i * dof[2] + j; // 在本单元内的自由度编号
                                    index2 = index4elem[index1];                                           // 在本进程的自由度编号
                                    indexfinal = globalindex[index2];
                                    if (indexfinal >= start && indexfinal < end)
                                    {
                                        if_delete[indexfinal - start] = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            break;
        }

        INT bdnum = 0;
        for (i = 0; i < matrix->local_nrows; i++)
        {
            if (if_delete[i])
                bdnum++;
        }
        INT *bdindex = (INT *)malloc(bdnum * sizeof(INT));
        bdnum = 0, j = 0;
        for (i = 0; i < matrix->local_nrows; i++)
        {
            if (if_delete[i])
            {
                bdindex[bdnum] = i;
                bdnum++;
            }
            else
            {
                newindex_diag[i] = j;
                j++;
            }
        }
        Rspace->delete_dirichletbd = true;
        Rspace->dirichletbdnum = bdnum;
        Rspace->dirichletbdindex = bdindex;
    }
    else
    {
        INT i, j = 0;
        for (i = 0; i < Rspace->dirichletbdnum; i++)
        {
            if_delete[Rspace->dirichletbdindex[i]] = true;
        }
        for (i = 0; i < matrix->local_nrows; i++)
        {
            if (if_delete[i])
                newindex_diag[i] = -1;
            else
            {
                newindex_diag[i] = j;
                j++;
            }
        }
    }

    matrix->delete_dirichletbd = true;
    INT bdnum = Rspace->dirichletbdnum, i, j, start, end;

    // (1) matrix property
    matrix->local_ncols -= bdnum;
    matrix->local_nrows -= bdnum;
    MPI_Allreduce(&(matrix->local_nrows), &(matrix->global_nrows), 1, MPI_INT, MPI_SUM, matrix->comm);
    matrix->global_ncols = matrix->global_nrows;
    MPI_Scan(&(matrix->local_nrows), &(matrix->rows_end), 1, MPI_INT, MPI_SUM, matrix->comm);
    matrix->rows_start = matrix->rows_end - matrix->local_nrows;
    matrix->cols_start = matrix->rows_start;
    matrix->cols_end = matrix->rows_end;

    // dofs about ndiag
    INT sendnum = matrix->mult_sendtotalnum, recvnum = matrix->mult_recvtotalnum;
    INT *sendindex = matrix->mult_sendindex;
    INT *sendtmp = (INT *)malloc(sendnum * sizeof(INT));
    INT *recvtmp = (INT *)malloc(recvnum * sizeof(INT));
    for (i = 0; i < sendnum; i++)
    {
        if (if_delete[sendindex[i]])
            sendtmp[i] = -1;
        else
            sendtmp[i] = matrix->cols_start + newindex_diag[sendindex[i]];
    }
    MPI_Neighbor_alltoallv(sendtmp, matrix->mult_sendnum, matrix->mult_sendnum_displs, MPI_INT,
                           recvtmp, matrix->mult_recvnum, matrix->mult_recvnum_displs, MPI_INT, matrix->mult_comm);
    OpenPFEM_Free(sendtmp);

    // (2) diag
    CSRMAT *csrmat = matrix->diag;
    INT new_rownum = csrmat->NumRows - bdnum;
    INT *rowptr = csrmat->RowPtr, *kcol = csrmat->KCol;
    DOUBLE *entries = csrmat->Entries;
    INT *newrowptr = (INT *)malloc((new_rownum + 1) * sizeof(INT));
    newrowptr[0] = 0;
    INT rowind = 0, num, new_entnum = 0;
    for (i = 0; i < csrmat->NumRows; i++)
    {
        if (!(if_delete[i]))
        {
            start = rowptr[i];
            end = rowptr[i + 1];
            num = 0;
            for (j = start; j < end; j++)
            {
                if (!(if_delete[kcol[j]]))
                {
                    kcol[new_entnum] = newindex_diag[kcol[j]];
                    entries[new_entnum] = entries[j];
                    num++;
                    new_entnum++;
                }
            }
            newrowptr[rowind + 1] = newrowptr[rowind] + num;
            rowind++;
        }
    }
    OpenPFEM_Free(csrmat->RowPtr);
    OpenPFEM_Free(newindex_diag);
    csrmat->RowPtr = newrowptr;
    csrmat->KCol = realloc(csrmat->KCol, new_entnum * sizeof(INT));
    csrmat->Entries = realloc(csrmat->Entries, new_entnum * sizeof(DOUBLE));
    csrmat->NumRows = new_rownum;
    csrmat->NumColumns = new_rownum;
    csrmat->NumEntries = new_entnum;

    // (3) ndiag
    csrmat = matrix->ndiag;
    rowptr = csrmat->RowPtr, kcol = csrmat->KCol, entries = csrmat->Entries;
    INT new_colnum = 0;
    INT *new_colindex_ndiag = (INT *)malloc(recvnum * sizeof(INT));
    for (i = 0; i < recvnum; i++)
    {
        if (recvtmp[i] != -1)
        {
            new_colindex_ndiag[i] = new_colnum;
            new_colnum++;
        }
        else
            new_colindex_ndiag[i] = -1;
    }
    INT *newrowptr2 = (INT *)malloc((new_rownum + 1) * sizeof(INT));
    newrowptr2[0] = 0;
    rowind = 0, new_entnum = 0;
    for (i = 0; i < csrmat->NumRows; i++)
    {
        if (!(if_delete[i]))
        {
            start = rowptr[i];
            end = rowptr[i + 1];
            num = 0;
            for (j = start; j < end; j++)
            {
                if (recvtmp[kcol[j]] != -1)
                {
                    kcol[new_entnum] = new_colindex_ndiag[kcol[j]];
                    entries[new_entnum] = entries[j];
                    num++;
                    new_entnum++;
                }
            }
            newrowptr2[rowind + 1] = newrowptr2[rowind] + num;
            rowind++;
        }
    }
    csrmat->RowPtr = newrowptr2;
    csrmat->KCol = realloc(csrmat->KCol, new_entnum * sizeof(INT));
    csrmat->Entries = realloc(csrmat->Entries, new_entnum * sizeof(DOUBLE));
    csrmat->NumRows = new_rownum;
    csrmat->NumEntries = new_entnum;
    INT *new_ndiag_globalcols = (INT *)malloc(new_colnum * sizeof(INT));
    j = 0;
    for (i = 0; i < recvnum; i++)
    {
        if (recvtmp[i] != -1)
        {
            new_ndiag_globalcols[j] = recvtmp[i];
            j++;
        }
    }
    OpenPFEM_Free(matrix->ndiag_globalcols);
    OpenPFEM_Free(recvtmp);
    matrix->ndiag_globalcols = new_ndiag_globalcols;

    // (4) multiplication info (if needed)
    bool multinfobuild = 1;
    if (new_colnum == csrmat->NumColumns)
    {
        multinfobuild = 0;
    }
    MPI_Allreduce(MPI_IN_PLACE, &multinfobuild, 1, MPI_C_BOOL, MPI_LOR, matrix->comm);
    if (!multinfobuild)
        goto end_of_MatrixDeleteDirichletBoundary;
    MPI_Comm_free(&((matrix)->mult_comm));
    MPI_Comm_free(&((matrix)->inverse_mult_comm));
    OpenPFEM_Free(matrix->mult_sendnum);
    OpenPFEM_Free(matrix->mult_recvnum);
    OpenPFEM_Free(matrix->mult_sendnum_displs);
    OpenPFEM_Free(matrix->mult_recvnum_displs);
    OpenPFEM_Free(matrix->mult_sendindex);
    OpenPFEM_Free(matrix->mult_sendtmp);
    OpenPFEM_Free(matrix->mult_recvtmp);

    // (4.1) 统计两两进程之间的通信情况
    INT globalsize, myrank;
    MPI_Comm comm = matrix->comm;
    MPI_Comm_size(comm, &globalsize);
    MPI_Comm_rank(comm, &myrank);
    INT *col_start_global = (INT *)malloc((globalsize + 1) * sizeof(INT));
    INT col_start = matrix->cols_start;
    MPI_Allgather(&col_start, 1, MPI_INT, col_start_global, 1, MPI_INT, comm);
    col_start_global[globalsize] = matrix->global_ncols;
    bool *entries_from = (bool *)calloc(globalsize, sizeof(bool));
    bool *entries_to = (bool *)malloc(globalsize * sizeof(bool));
    INT idx_rank, current = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        while (new_ndiag_globalcols[current] >= col_start_global[idx_rank] &&
               new_ndiag_globalcols[current] < col_start_global[idx_rank + 1])
        {
            entries_from[idx_rank] = true;
            current++;
            if (current == new_colnum)
                goto finish_communication_check2;
        }
    }
finish_communication_check2:
    MPI_Alltoall(entries_from, 1, MPI_C_BOOL, entries_to, 1, MPI_C_BOOL, comm);
    MPI_Comm mult_comm = MPI_COMM_NULL;
    MPI_Comm inverse_mult_comm = MPI_COMM_NULL;
    INT incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
            incount++;
        if (entries_to[idx_rank])
            outcount++;
    }
    INT *inrank = (INT *)malloc(incount * sizeof(INT));
    INT *outrank = (INT *)malloc(outcount * sizeof(INT));
    incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            inrank[incount] = idx_rank;
            incount++;
        }
        if (entries_to[idx_rank])
        {
            outrank[outcount] = idx_rank;
            outcount++;
        }
    }
    MPI_Dist_graph_create_adjacent(comm, incount, inrank, MPI_UNWEIGHTED, outcount, outrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &mult_comm);
    MPI_Dist_graph_create_adjacent(comm, outcount, outrank, MPI_UNWEIGHTED, incount, inrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &inverse_mult_comm);
    matrix->mult_comm = mult_comm;
    matrix->inverse_mult_comm = inverse_mult_comm;
    OpenPFEM_Free(inrank);
    OpenPFEM_Free(outrank);

    // (4.2) 确认两两之间要发送哪些行
    INT *num_entries_from = (INT *)calloc(incount, sizeof(INT));
    INT *num_entries_to = (INT *)malloc(outcount * sizeof(INT));
    current = 0;
    INT current_rank = 0;
    MPI_Request numreq;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            while (new_ndiag_globalcols[current] >= col_start_global[idx_rank] &&
                   new_ndiag_globalcols[current] < col_start_global[idx_rank + 1])
            {
                num_entries_from[current_rank]++;
                current++;
                if (current == new_colnum)
                    goto finish_num_check2;
            }
            current_rank++;
        }
    }
finish_num_check2:
    MPI_Ineighbor_alltoall(num_entries_from, 1, MPI_INT, num_entries_to, 1, MPI_INT, inverse_mult_comm, &numreq);
    INT *which_entries_from = new_ndiag_globalcols;
    INT *num_entries_from_displs = (INT *)malloc(incount * sizeof(INT));
    num_entries_from_displs[0] = 0;
    for (i = 1; i < incount; i++)
    {
        num_entries_from_displs[i] = num_entries_from_displs[i - 1] + num_entries_from[i - 1];
    }
    MPI_Wait(&numreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&numreq);
    INT *num_entries_to_displs = (INT *)malloc(outcount * sizeof(INT));
    num_entries_to_displs[0] = 0;
    for (i = 1; i < outcount; i++)
    {
        num_entries_to_displs[i] = num_entries_to_displs[i - 1] + num_entries_to[i - 1];
    }
    INT final_num = num_entries_to_displs[outcount - 1] + num_entries_to[outcount - 1];
    INT *which_entries_to = (INT *)malloc(final_num * sizeof(INT));
    MPI_Neighbor_alltoallv(which_entries_from, num_entries_from, num_entries_from_displs, MPI_INT,
                           which_entries_to, num_entries_to, num_entries_to_displs, MPI_INT, inverse_mult_comm);
    for (i = 0; i < final_num; i++)
    {
        which_entries_to[i] -= col_start;
    }
    matrix->mult_sendindex = which_entries_to;
    matrix->mult_sendnum = num_entries_to;
    matrix->mult_recvnum = num_entries_from;
    matrix->mult_sendnum_displs = num_entries_to_displs;
    matrix->mult_recvnum_displs = num_entries_from_displs;
    OpenPFEM_Free(entries_from);
    OpenPFEM_Free(entries_to);
    OpenPFEM_Free(col_start_global);
    matrix->mult_recvtotalnum = new_colnum;
    matrix->mult_sendtotalnum = final_num;
    matrix->mult_sendtmp = (DOUBLE *)malloc(final_num * sizeof(DOUBLE));
    matrix->mult_recvtmp = (DOUBLE *)malloc(new_colnum * sizeof(DOUBLE));

end_of_MatrixDeleteDirichletBoundary:
    csrmat->NumColumns = new_colnum;
    OpenPFEM_Free(if_delete);
    OpenPFEM_Free(new_colindex_ndiag);
}

void VectorsAddZeroDirichletBoundary(VECTORS *oldvecs, VECTORS **newvecs, FEMSPACE *femspace)
{
    if (oldvecs->delete_dirichletbd == false)
    {
        RaiseError("VectorsAddZeroDirichletBoundary", "Vectors are already complete");
    }
    if (newvecs == NULL)
    {
        if (oldvecs->start_cols != 0 || oldvecs->end_cols != oldvecs->nvecs)
            RaiseError("VectorsAddZeroDirichletBoundary", "Addition in place needs range (0 - num)");
    }
    INT new_rows = oldvecs->local_length + femspace->dirichletbdnum;
    INT old_rows = oldvecs->local_length;
    INT new_grows = 0, new_start = 0, new_end = 0;
    MPI_Allreduce(&new_rows, &new_grows, 1, MPI_INT, MPI_SUM, oldvecs->comm);
    MPI_Scan(&new_rows, &new_end, 1, MPI_INT, MPI_SUM, oldvecs->comm);
    new_start = new_end - new_rows;

    DATATYPE type = oldvecs->type;
    DOUBLE *newdata = NULL;
    void *bv_data = NULL;
    if (DataTypeOpenPFEMEqual(type))
    {
        newdata = (DOUBLE *)calloc(new_rows * oldvecs->nvecs, sizeof(DOUBLE));
    }
    if (DataTypeSlepcEqual(type))
    {
#if defined(SLEPC_USE)
        MPI_Comm bv_comm;
        BV oldbv = (BV)oldvecs->data_slepc;
        PetscObjectGetComm((PetscObject)oldbv, &bv_comm);
        BV bv;
        BVCreate(bv_comm, &bv);
        BVSetType(bv, BVMAT);
        BVSetSizes(bv, new_rows, new_grows, oldvecs->nvecs);
        BVSetActiveColumns(bv, 0, oldvecs->nvecs);
        BVSetRandom(bv);
        BVGetArray(bv, &newdata);
        memset(newdata, 0.0, new_rows * oldvecs->nvecs * sizeof(DOUBLE));
        bv_data = (void *)bv;
#else
        RaiseSlepcError("VectorsAddZeroDirichletBoundary");
#endif
    }

    DOUBLE *olddata = NULL;
    VectorsGetArray(oldvecs, &olddata);
    bool *if_d = (bool *)calloc(new_rows, sizeof(bool));
    INT *index = femspace->dirichletbdindex;
    INT i, j, ind = 0;
    for (i = 0; i < femspace->dirichletbdnum; i++)
    {
        if_d[index[i]] = true;
    }
    for (i = 0; i < new_rows; i++)
    {
        if (if_d[i] == false)
        {
            for (j = 0; j < oldvecs->nvecs; j++)
            {
                newdata[i + j * new_rows] = olddata[ind + j * old_rows];
            }
            ind++;
        }
    }
    VectorsRestoreArray(oldvecs, &olddata);
    OpenPFEM_Free(if_d);

    if (newvecs == NULL)
    {
        if (DataTypeOpenPFEMEqual(type))
        {
            oldvecs->local_length = new_rows;
            oldvecs->global_length = new_grows;
            oldvecs->start = new_start;
            oldvecs->end = new_end;
            oldvecs->data = newdata;
            free(olddata);
        }
        if (DataTypeSlepcEqual(type))
        {
#if defined(SLEPC_USE)
            BVRestoreArray((BV)bv_data, &newdata);
            oldvecs->local_length = new_rows;
            oldvecs->global_length = new_grows;
            oldvecs->start = new_start;
            oldvecs->end = new_end;
            BVDestroy((BV *)(&(oldvecs->data_slepc)));
            oldvecs->data_slepc = bv_data;
#else
            RaiseSlepcError("VectorsAddZeroDirichletBoundary");
#endif
        }
    }
    else
    {
        VectorsCreate(newvecs, type);
        (*newvecs)->nvecs = oldvecs->nvecs;
        (*newvecs)->local_length = new_rows;
        (*newvecs)->global_length = new_grows;
        (*newvecs)->start = new_start;
        (*newvecs)->end = new_end;
        (*newvecs)->start_cols = 0;
        (*newvecs)->end_cols = oldvecs->nvecs;
        MPI_Comm_dup(oldvecs->comm, &((*newvecs)->comm));
        (*newvecs)->data_slepc = (void *)bv_data;
    }
}

void MatrixAddRowMatrix(MATRIX *matrix, INT num, INT rowindex, INT *colindex_global, DOUBLE *entries)
{
    DATATYPE type = matrix->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        INT colstart = matrix->cols_start, colend = matrix->cols_end;
        INT i, colindex, position;
        INT diag_start = matrix->diag->RowPtr[rowindex];
        INT diag_end = matrix->diag->RowPtr[rowindex + 1];
        INT ndiag_start = matrix->ndiag->RowPtr[rowindex];
        INT ndiag_end = matrix->ndiag->RowPtr[rowindex + 1];
        DOUBLE alpha;
        INT *diag_kcol = matrix->diag->KCol;
        INT *ndiag_kcol = matrix->ndiag->KCol;
        DOUBLE *diag_entries = matrix->diag->Entries;
        DOUBLE *ndiag_entries = matrix->ndiag->Entries;
        for (i = 0; i < num; i++)
        {
            alpha = entries[i];
            if (alpha != 0.0)
            {
                colindex = colindex_global[i];
                if (colindex >= colstart && colindex < colend)
                {
                    colindex = colindex - colstart;
                    position = FindPosition(diag_kcol, diag_start, diag_end, colindex);
                    diag_entries[position] += alpha;
                    // printf(" row: %d col: %d entry: %2.10f\n", rowindex, colindex,alpha);
                }
                else
                {
                    position = FindPosition(ndiag_kcol, ndiag_start, ndiag_end, colindex);
                    ndiag_entries[position] += alpha;
                }
            }
        }
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        INT i, colindex;
        INT rowstart = matrix->cols_start;
        DOUBLE alpha;
        Mat mat = (Mat)(matrix->data_petsc);
        for (i = 0; i < num; i++)
        {
            alpha = entries[i];
#if MATRIX_COMPRESSION
            if (alpha != 0.0)
#endif
            {
                colindex = colindex_global[i];
                MatSetValue(mat, rowstart + rowindex, colindex, alpha, ADD_VALUES);
            }
        }
#else
        RaisePetscError("AddRowMatrix");
#endif
    }
}

// 进行DIRICHLET边条件的处理
//  ind: 当前单元的编号, numlocalbd: 当前单元上边界的条数
//  BoundaryVec: 用来记录当前边界值的向量
//  ElemBoundaryType: 记录每个自由度的边界类型
void DirichletBoundaryTreatmen(DISCRETEFORM *DiscreteForm, MATRIX *matrix, VECTOR *rhs, ELEMENT *Elem,
                               INT ind, INT numlocalbd, DOUBLE *BoundaryVec, BOUNDARYTYPE *ElemBoundaryType)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase;
    // (0) 如果需要，就对右端项进行计算
    FUNCTIONVEC *BoundaryFunction = DiscreteForm->BoundaryFunction; // 获得边界条件函数
    NODALFUNCTION *NodalFun = Rbase->NodalFun;                      // 获得有限元基函数的插值操作函数
    INT i;
    INT Inter_Dim = Rbase->InterpolationValueDim;
    if (rhs != NULL)
    {
        // Hcurl空间基函数需要计算$\vec{\tau}$
        // if (Rbase->MapType == Piola)
        //     ComputeElementLineDirection(Rspace->Mesh, ind, Elem);
        memset(BoundaryVec, 0.0, RNumBase * Inter_Dim * sizeof(DOUBLE)); // 为相应的边界值申请存储空间
        NodalFun(Elem, BoundaryFunction, Rbase->ValueDim, BoundaryVec);  // 调用边界函数得到相应的右端项
        // for(i=0;i<6;i++)
        //    OpenPFEM_Print("%2.10f, ", BoundaryVec[i]);
        // OpenPFEM_Print("\n");
        if (Rbase->MapType == Piola)
            ReorderBoundaryVecForCurlElem(DiscreteForm, ind, Elem, BoundaryVec);
    }
    // (1.0) 与本单元相关的行列号起始位置
    INT *Rbeginindex = Rspace->BeginIndex, *Cbeginindex = Cspace->BeginIndex;
    INT Rstart = Rbeginindex[ind], Cstart = Cbeginindex[ind];
    // (1.1) 对边界上自由度循环
    INT *Rlocalindex = Rspace->LocalIndex, *Clocalindex = Cspace->LocalIndex;
    INT *Rglobalindex = Rspace->GlobalIndex, *Cglobalindex = Cspace->GlobalIndex;
    INT RGDOFstart = Rspace->GDOFstart, RGDOFend = Rspace->GDOFend;
    INT rowind, rowind_g, rowowner;
    INT j, colind, colind_g, start, end;
    DATATYPE type = matrix->type;
    INT *diag_rowptr = NULL, *diag_kcol = NULL;
    DOUBLE *diag_entries = NULL;
    if (DataTypeOpenPFEMEqual(type))
    {
        diag_rowptr = matrix->diag->RowPtr;
        diag_kcol = matrix->diag->KCol;
        diag_entries = matrix->diag->Entries;
    }
    // 对当前单元的边界自由度进行遍历
    for (i = 0; i < numlocalbd; i++)
    {
        if (ElemBoundaryType[i] == DIRICHLET || ElemBoundaryType[i] == STIFFDIRICHLET)
        {
            // OpenPFEM_Print("numlocalbd: %d    ", numlocalbd);
            // OpenPFEM_Print("I: %d th bd\n", i);
            //   相应的行的局部编号, 那这里就需要我们依照点、线、面、体的方式来排列自由度
            //  如果当前自由度的边界条件为Dirichlet的话，需要进行相应的处理
            rowind = Rlocalindex[Rstart + i];
            // 相应的行的全局编号
            rowind_g = Rglobalindex[rowind];
            // 获得本行是否属于本进程, 在本进程的才能把相应的矩阵的对角线设置为1
            // 同样本进程的右端项才能被设置成相应的元素，否则设置为零
            if ((rowind_g >= RGDOFstart) && (rowind_g < RGDOFend))
            {
                rowowner = 1;
            }
            else
            {
                rowowner = 0;
            }
            // 然后找到与全局行号相等的全局列号的局部编号
            for (j = 0; j < CNumBase; j++)
            {
                // 目前列的全局编号
                colind_g = Cglobalindex[Clocalindex[Cstart + j]];
                if (colind_g == rowind_g)
                {
                    // 获得相应的局部列号, 如何能保证这种情况一定发生，当左右有限元空间不一样的时候，怎么才能保证呢??????
                    colind = colind_g; // Cnum列的全局编号与Rnum的全局编号一样
                    break;
                } // end if(Cglobalindex[Cstart+j]==Rgnum)
            }     // end for(j=0;j<CNumBase;j++)
            // 局部第i行是Dirichlet行, 直接对单刚矩阵相应的行进行赋值
            if (rhs != NULL && rowowner)
            {
                if (DataTypeOpenPFEMEqual(type))
                {
                    rhs->data[rowind_g - RGDOFstart] = BoundaryVec[i];
                }
                else if (DataTypePetscEqual(type))
                {
#if defined(PETSC_USE)
                    Vec vec = (Vec)(rhs->data_petsc);
                    PetscVecInsertValue(vec, rowind_g - RGDOFstart, BoundaryVec[i]);
#else
                    RaisePetscError("DirichletBoundaryTreatmen");
#endif
                }
            }
            // 处理矩阵的对角线元素,先获得矩阵的行号
            // 矩阵的起始位置
            if (rowowner)
            {
                if (DataTypeOpenPFEMEqual(type))
                {
                    start = diag_rowptr[rowind_g - RGDOFstart];
                    end = diag_rowptr[rowind_g - RGDOFstart + 1];
                    for (j = start; j < end; j++)
                    {
                        if ((diag_kcol[j] == colind_g - RGDOFstart))
                        {
                            diag_entries[j] = 1.0;
                            break;
                        }
                    }
                }
                else if (DataTypePetscEqual(type))
                {
#if defined(PETSC_USE)
                    Mat mat = (Mat)(matrix->data_petsc);
                    PetscMatInsertDiagValue(mat, rowind_g - RGDOFstart, colind_g, 1.0);
#else
                    RaisePetscError("DirichletBoundaryTreatmen");
#endif
                }
            }
        }
    }
    return;
}

void VectorAddSubVector(VECTOR *vector, ASSEMBLE_INFO *assembleinfo, DOUBLE *SubVec, INT n_row, INT *row)
{
    DATATYPE type = vector->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        INT idx_row, rowind;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        DOUBLE *vec_data = vector->data;
        INT neigrank, send_index, position;
        INT *send_row_num = assembleinfo->send_row_num;
        INT *send_ent_num = assembleinfo->send_col_num;
        INT *send_ent_num_displs = assembleinfo->send_col_num_displs;
        DOUBLE *send_vecdata = assembleinfo->send_ent_each_row;
        DOUBLE alpha;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            alpha = SubVec[idx_row];
            if (alpha != 0.0)
            {
                // (1) 检查当前行是否属于本进程
                if (row_owner[row[idx_row]] == -1) // √
                {
                    rowind = row_owner_index[row[idx_row]];
                    vec_data[rowind] += alpha;
                }
                else // ×
                {
                    neigrank = row_owner[row[idx_row]];         // 找出本行应该属于哪个邻居
                    send_index = row_owner_index[row[idx_row]]; // 找出本行是发送给这个邻居的第几行
                    position = send_ent_num_displs[neigrank] + send_ent_num[neigrank] - send_row_num[neigrank] + send_index;
                    send_vecdata[position] += alpha;
                }
            }
        }
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        INT idx_row, rowind, neigrank, send_index, position;
        DOUBLE alpha;
        DOUBLE *send_vecdata = assembleinfo->send_ent_each_row;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        INT *send_row_num = assembleinfo->send_row_num;
        INT *send_ent_num = assembleinfo->send_col_num;
        INT *send_ent_num_displs = assembleinfo->send_col_num_displs;
        Vec vec = (Vec)(vector->data_petsc);
        INT start = vector->start;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            alpha = SubVec[idx_row];
            if (alpha != 0.0)
            {
                // (1) 检查当前行是否属于本进程
                if (row_owner[row[idx_row]] == -1) // √
                {
                    rowind = row_owner_index[row[idx_row]] + start;
                    VecSetValue(vec, rowind, alpha, ADD_VALUES);
                }
                else // ×
                {
                    neigrank = row_owner[row[idx_row]];         // 找出本行应该属于哪个邻居
                    send_index = row_owner_index[row[idx_row]]; // 找出本行是发送给这个邻居的第几行
                    position = send_ent_num_displs[neigrank] + send_ent_num[neigrank] - send_row_num[neigrank] + send_index;
                    send_vecdata[position] += alpha;
                }
            }
        }
#else
        RaisePetscError("AddSubVec");
#endif
    }
}

void MatrixAddSubMatrix(MATRIX *A, ASSEMBLE_INFO *assembleinfo, DOUBLE *AA, INT n_row, INT *row, INT n_col, INT *col)
{
    DATATYPE type = A->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        INT idx_row, idx_col, rowind, colind;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        INT *diag_rowptr = A->diag->RowPtr, *diag_kcol = A->diag->KCol;
        INT *ndiag_rowptr = A->ndiag->RowPtr, *ndiag_kcol = A->ndiag->KCol;
        DOUBLE *diag_entries = A->diag->Entries, *ndiag_entries = A->ndiag->Entries;
        bool if_rhs = assembleinfo->if_rhs;
        // (0) 检查每一列属于对角元还是非对角元 及其编号
        INT *col_global_index = assembleinfo->col_global_index;
        bool *submat_col_diag = assembleinfo->submat_col_diag;
        INT *submat_col_index = assembleinfo->submat_col_index;
        INT col_start = A->cols_start, col_end = A->cols_end;
        for (idx_col = 0; idx_col < n_col; idx_col++)
        {
            colind = col_global_index[col[idx_col]];
            if (colind >= col_start && colind < col_end)
            {
                submat_col_diag[idx_col] = true;
                submat_col_index[idx_col] = colind;
            }
            else
            {
                submat_col_diag[idx_col] = false;
                submat_col_index[idx_col] = colind;
            }
        }
        // 开始组装
        INT *send_row_num_displs = assembleinfo->send_row_num_displs;
        INT *send_colnum_each_row_displs = assembleinfo->send_colnum_each_row_displs;
        INT *send_colindex_each_row = assembleinfo->send_colindex_each_row;
        DOUBLE *send_ent_each_row = assembleinfo->send_ent_each_row;
        INT diag_start, diag_end, ndiag_start, ndiag_end, position, globalind;
        INT neigrank, send_index, start, end;
        DOUBLE alpha;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            // (1) 检查当前行是否属于本进程
            if (row_owner[row[idx_row]] == -1) // √
            {
                rowind = row_owner_index[row[idx_row]];
                diag_start = diag_rowptr[rowind];
                diag_end = diag_rowptr[rowind + 1];
                ndiag_start = ndiag_rowptr[rowind];
                ndiag_end = ndiag_rowptr[rowind + 1];
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    if (alpha != 0.0)
                    {
                        // (2a) 检查当前列是对角还是非对角部分
                        if (submat_col_diag[idx_col])
                        {
                            colind = submat_col_index[idx_col] - col_start;
                            position = FindPosition(diag_kcol, diag_start, diag_end, colind);
                            diag_entries[position] += alpha;
                            // if(rowind==2&&colind==2){
                            // printf("sub  %d %d + %2.10f = %2.10f\n", idx_row, idx_col, alpha, diag_entries[position]);
                            // }
                        }
                        else
                        {
                            colind = submat_col_index[idx_col];
                            position = FindPosition(ndiag_kcol, ndiag_start, ndiag_end, colind);
                            ndiag_entries[position] += alpha;
                        }
                    }
                }
            }
            else // ×
            {
                // (2.1b) 找出本行应该属于哪个邻居
                neigrank = row_owner[row[idx_row]];
                // (2.2b) 找出本行是发送给这个邻居的第几行
                send_index = row_owner_index[row[idx_row]];
                // (2.3b) 找到这一行列号在 send_colindex_each_row 的起始
                globalind = send_row_num_displs[neigrank] + send_index; // 是总的第几行
                start = send_colnum_each_row_displs[globalind];
                end = send_colnum_each_row_displs[globalind + 1];
                // (2.4b) 往对应位置上加
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    if (alpha != 0.0)
                    {
                        colind = submat_col_index[idx_col];
                        position = FindPosition(send_colindex_each_row, start, end, colind);
                        if (if_rhs)
                            send_ent_each_row[position + send_row_num_displs[neigrank]] += alpha;
                        else
                            send_ent_each_row[position] += alpha;
                    }
                }
            }
        }
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        INT idx_row, idx_col, rowind, colind;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        INT *submat_col_index = assembleinfo->submat_col_index;
        INT *col_global_index = assembleinfo->col_global_index;
        for (idx_col = 0; idx_col < n_col; idx_col++)
        {
            colind = col_global_index[col[idx_col]];
            submat_col_index[idx_col] = colind;
        }

        INT *send_colindex_each_row = assembleinfo->send_colindex_each_row;
        INT *send_row_num_displs = assembleinfo->send_row_num_displs;
        INT *send_colnum_each_row_displs = assembleinfo->send_colnum_each_row_displs;
        DOUBLE *send_ent_each_row = assembleinfo->send_ent_each_row;
        Mat mat = (Mat)(A->data_petsc);
        INT row_start = A->rows_start;
        DOUBLE alpha;
        bool if_rhs = assembleinfo->if_rhs;
        INT neigrank, send_index, globalind, start, end, position;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            // (1) 检查当前行是否属于本进程
            if (row_owner[row[idx_row]] == -1) // √
            {
                rowind = row_owner_index[row[idx_row]] + row_start;
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    colind = submat_col_index[idx_col];
#if MATRIX_COMPRESSION
                    if (alpha != 0.0 || colind == rowind)
#endif
                    {
                        MatSetValue(mat, rowind, colind, alpha, ADD_VALUES);
                    }
                }
            }
            else
            {
                // (2.1b) 找出本行应该属于哪个邻居
                neigrank = row_owner[row[idx_row]];
                // (2.2b) 找出本行是发送给这个邻居的第几行
                send_index = row_owner_index[row[idx_row]];
                // (2.3b) 找到这一行列号在 send_colindex_each_row 的起始
                globalind = send_row_num_displs[neigrank] + send_index; // 是总的第几行
                start = send_colnum_each_row_displs[globalind];
                end = send_colnum_each_row_displs[globalind + 1];
                // (2.4b) 往对应位置上加
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    if (alpha != 0.0)
                    {
                        colind = submat_col_index[idx_col];
                        position = FindPosition(send_colindex_each_row, start, end, colind);
                        if (if_rhs)
                            send_ent_each_row[position + send_row_num_displs[neigrank]] += alpha;
                        else
                            send_ent_each_row[position] += alpha;
                    }
                }
            }
        }
#else
        RaisePetscError("AddSubMatrix");
#endif
    }
}

INT FindPosition(INT *Vec, INT start, INT end, INT value)
{
    INT pos = (INT)((start + end) / 2);
    if (Vec[pos] > value)
    {
        pos = FindPosition(Vec, start, pos, value);
    }
    else if (Vec[pos] < value)
    {
        pos = FindPosition(Vec, pos + 1, end, value);
    }
    return pos;
}
// 组装单刚矩阵: DiscreteForm: 离散变分形式, ElemInd: 目前单元的编号，Elem：单元对象, ElemBoundaryType: 记录目前单元上自由度的边界类型
//  SubEntries： 存储单刚矩阵，SubVec：存储单元上的右端项
void SubMatrixAssemble(DISCRETEFORM *DiscreteForm, INT ElemInd, ELEMENT *Elem,
                       BOUNDARYTYPE *ElemBoundaryType, DOUBLE *SubEntries, DOUBLE *SubVec)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Lmesh = Cspace->Mesh;            // 获得左右两边的网格
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;            // 获得左右两边的基函数
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase;     // 获得左右基函数的个数
    INT RValueDim = Rbase->ValueDim, CValueDim = Cbase->ValueDim; // 获得左右基函数值的维数
    INT worlddim = Rmesh->worlddim;                               // 获得目前计算问题所在的维数
    QUADRATURE *Quadrature = DiscreteForm->Quadrature;            // 获得积分信息
    INT NumPoints = Quadrature->NumPoints, i, j, k;
    DOUBLE Coord[worlddim], RefCoord[worlddim], weight;
    INT NumCMultiIndex = DiscreteForm->NumLeftMultiIndex, NumRMultiIndex = DiscreteForm->NumRightMultiIndex;
    DOUBLE *CBaseValues = DiscreteForm->LeftBaseValues, *RBaseValues = DiscreteForm->RightBaseValues;
    DOUBLE *AuxValues = DiscreteForm->AuxValues, value;
    DOUBLE *AuxFEMValues = NULL; // 辅助有限元函数的微分值
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        AuxFEMValues = DiscreteForm->AuxFEMFunction->AuxFEMFunValues;
    }
    MULTIINDEX *Rmultiindex = DiscreteForm->RightMultiIndex; // 与行相对应的求导信息
    MULTIINDEX *Cmultiindex = DiscreteForm->LeftMultiIndex;  // 与列相对应的求导信息
    INT indRindex, indCindex, inddim;
    DISCRETEFORMMATRIX *DiscreteFormMatrix = DiscreteForm->DiscreteFormMatrix; // 获得刚度矩阵的组装信息
    MULTIINDEX NoGradIndex;                                                    // 表示不同空间维数下不需要计算导数的判断指标

    // 看是否在参考单元上已经计算了相应的基函数的导数值
    BOOL IsHaveLeftBaseValuesInPointsOfRefElem = DiscreteForm->IsHaveLeftBaseValuesInPointsOfRefElem;
    BOOL IsHaveRightBaseValuesInPointsOfRefElem = DiscreteForm->IsHaveRightBaseValuesInPointsOfRefElem;
    DOUBLE *LeftBaseValuesInPointsOfRefElem = DiscreteForm->LeftBaseValuesInPointsOfRefElem;
    DOUBLE *RightBaseValuesInPointsOfRefElem = DiscreteForm->RightBaseValuesInPointsOfRefElem;
    INT *IndLeftMultiIndex = DiscreteForm->IndLeftMultiIndex;
    INT *IndRightMultiIndex = DiscreteForm->IndRightMultiIndex;

    // OpenPFEM_Print("RValueDim * NumRMultiIndex=%d\n",RValueDim * NumRMultiIndex);
    // lhc 23.01.28
    // 对是否用不同的有限元空间进行组装判断
    // 如果存在不同的有限元空间 本函数中传入的Elem和Ind是细空间的小单元的信息
    if (DiscreteForm->IsUseAuxFEMSpace == 0)
    {
        // 表示没有使用辅助有限元函数的网格作为组装的网格
        switch (worlddim)
        {
        case 1:
            NoGradIndex = D0;
            break;
        case 2:
            NoGradIndex = D00;
            break;
        case 3:
            NoGradIndex = D000;
            break;
        }
        // (0) 判断是否需要计算Jacobi逆矩阵
        INT tmpind = 0;
        for (i = 0; i < NumCMultiIndex; i++)
        {
            // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
            if (Cmultiindex[i] > NoGradIndex)
            {
                tmpind = 1;
                break;
            }
        }
        if (tmpind == 0)
        {
            for (i = 0; i < NumRMultiIndex; i++)
            {
                if (Rmultiindex[i] > NoGradIndex)
                {
                    tmpind = 1;
                    break;
                }
            }
        }
        if (Rbase->MapType == Piola)
            tmpind = 1;
        // tmpind=0表示不需要计算导数，也就是不需要计算Jacobian矩阵的逆，等于1表示需要计算Jacobian矩阵的逆
        //  lhc 23.01.28
        //  加入是否已知单元的Inv_jacobian的判断 如果有省略计算
        INT ii, jj;
        if (tmpind == 0)
        {
            if (Rmesh->IsJacobiInfoAvailable == 0)
            {
                // 表示网格中还没有计算单元的变换信息和Jacobian矩阵的信息
                //  计算单元的体积和Jacobian矩阵
                ComputeElementJacobian(Elem);
            }
            else
            {
                // 表示单元的体积和Jacobian矩阵的信息已经计算好了，只需要提取就行
                Elem->Volumn = Rmesh->VolumnOfAllElem[ElemInd];
                // 下面对单纯形是合适的
                for (ii = 0; ii < worlddim; ii++)
                {
                    for (jj = 0; jj < worlddim; jj++)
                    {
                        Elem->Jacobian[ii][jj] = Rmesh->JacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj];
                    } // end for jj
                }     // end for ii
            }         // end if (Rmesh->IsJacobiInfoAvailable == 0)
        }
        else
        {
            if (Rmesh->IsJacobiInfoAvailable == 0)
            {
                // 计算单元的体积和Jacobian矩阵的逆矩阵
                ComputeElementInvJacobian(Elem);
            }
            else
            {
                Elem->Volumn = Rmesh->VolumnOfAllElem[ElemInd];
                for (ii = 0; ii < worlddim; ii++)
                {
                    for (jj = 0; jj < worlddim; jj++)
                    {
                        Elem->Jacobian[ii][jj] = Rmesh->JacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj];       // 得到Jacobian矩阵
                        Elem->InvJacobian[ii][jj] = Rmesh->InvJacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj]; // 得到Jacobian逆矩阵
                    }
                }
            }
        }
        // 先将单刚矩阵设置为0，以免后面出现错误,memset,memcpy
        memset(SubEntries, 0.0, RNumBase * CNumBase * sizeof(DOUBLE));
        if (SubVec != NULL)
            memset(SubVec, 0.0, RNumBase * sizeof(DOUBLE));
        // (1) 对积分点进行遍历
        for (k = 0; k < NumPoints; k++)
        {
            // (1.1) 取出当前积分点的坐标和权重
            switch (worlddim)
            {
            case 1:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                weight = Quadrature->QuadW[k];
                break;
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                weight = Quadrature->QuadW[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                RefCoord[2] = Quadrature->QuadZ[k];
                weight = Quadrature->QuadW[k];
                break;
            }
            // (1.2) 计算得到积分点的实际坐标
            ElementRefCoord2Coord(Elem, RefCoord, Coord);
            // (1.3) 计算基函数微分的值，为组装做准备, 根据基函数求解相应的基函数值
            memset(RBaseValues, 0.0, RNumBase * RValueDim * NumRMultiIndex * sizeof(DOUBLE));
            for (j = 0; j < NumRMultiIndex; j++)
            {
                // GetBaseValues(Rbase, Rmultiindex[j], Elem, Coord, RefCoord, RBaseValues + j * RNumBase * RValueDim);
                if (IsHaveRightBaseValuesInPointsOfRefElem == 0)
                {
                    // 直接计算基函数的导数值
                    GetBaseValues(Rbase, Rmultiindex[j], Elem, Coord, RefCoord,
                                  RBaseValues + j * RNumBase * RValueDim);
                }
                else
                {
                    // 根据已经计算好的在标准单元上的基函数微分值来得到实际基函数的导数值
                    // j表示目前是第几个微分
                    // k是表示第几个积分点, 最外层的顺序是按照积分点的顺序来确定的
                    GetBaseValuesWithInfo(Rbase, Rmultiindex[j], Elem,
                                          RightBaseValuesInPointsOfRefElem + k * IndRightMultiIndex[3] * RNumBase * RValueDim,
                                          IndRightMultiIndex, RBaseValues + j * RNumBase * RValueDim);
                } // end if (IsHaveRightBaseValuesInPointsOfRefElem == 0)
            }     // end for (j = 0; j < NumRMultiIndex; j++)
            memset(CBaseValues, 0.0, CNumBase * CValueDim * NumCMultiIndex * sizeof(DOUBLE));
            for (j = 0; j < NumCMultiIndex; j++)
            {
                // GetBaseValues(Cbase, Cmultiindex[j], Elem, Coord, RefCoord, CBaseValues + j * CNumBase * CValueDim);
                if (IsHaveLeftBaseValuesInPointsOfRefElem == 0)
                {
                    GetBaseValues(Cbase, Cmultiindex[j], Elem, Coord, RefCoord, CBaseValues + j * CNumBase * CValueDim);
                }
                else
                {
                    GetBaseValuesWithInfo(Cbase, Cmultiindex[j], Elem,
                                          LeftBaseValuesInPointsOfRefElem + k * IndLeftMultiIndex[3] * CNumBase * CValueDim,
                                          IndLeftMultiIndex, CBaseValues + j * CNumBase * CValueDim);
                }
            }
            // (1.4) 计算辅助有限元函数在积分点处的函数值:AuxFEMValues
            if (DiscreteForm->AuxFEMFunction != NULL)
            {
                DiscreteForm->AuxFEMFunction->Points_ind = k;
                ComputeAuxFEMFunValue(DiscreteForm->AuxFEMFunction, ElemInd, Elem, Coord, RefCoord);
            }
            // 进行组装单刚矩阵的计算,单刚矩阵以行优先排列
            // (1.5) 为了后面组装刚度矩阵，我们需要将RBaseValues和CBaseValues进行重新排序成可以直接调用DiscreteFormMatrix的形式
            for (i = 0; i < RNumBase; i++)
            {
                // 准备当前行所对应的基函数各微分算子的值
                // 第i个基函数
                for (indRindex = 0; indRindex < NumRMultiIndex; indRindex++)
                {
                    // 第indRindex个微分
                    for (inddim = 0; inddim < RValueDim; inddim++)
                    {
                        // 第inddim维的函数值
                        AuxValues[i * NumRMultiIndex * RValueDim + indRindex * RValueDim + inddim] =
                            RBaseValues[indRindex * RNumBase * RValueDim + i * RValueDim + inddim];
                    }
                }
            }
            // copy back to RBaseValus
            memcpy(RBaseValues, AuxValues, RNumBase * RValueDim * NumRMultiIndex * sizeof(DOUBLE));
            // 处理列的
            for (i = 0; i < CNumBase; i++)
            {
                // 准备当前行所对应的基函数各微分算子的值
                // 第i个基函数
                for (indCindex = 0; indCindex < NumCMultiIndex; indCindex++)
                {
                    // 第 indCindex个微分
                    for (inddim = 0; inddim < CValueDim; inddim++)
                    {
                        // 第inddim维的基函数值
                        AuxValues[i * NumCMultiIndex * CValueDim + indCindex * CValueDim + inddim] =
                            CBaseValues[indCindex * CNumBase * CValueDim + i * CValueDim + inddim];
                    }
                }
            }
            // copy back to CBaseValus
            memcpy(CBaseValues, AuxValues, CNumBase * CValueDim * NumCMultiIndex * sizeof(DOUBLE));
            // (1.6) 组装刚度矩阵
            for (i = 0; i < RNumBase; i++)
            {
                // 右端项的计算
                if (SubVec != NULL)
                {
                    if (ElemBoundaryType[i] == DIRICHLET || ElemBoundaryType[i] == STIFFDIRICHLET)
                    {
                        // 应该需要改成专门的右端项计算过程
                        SubVec[i] = 0.0;
                    }
                    else
                    {
                        DiscreteForm->DiscreteFormVec(RBaseValues + i * RValueDim * NumRMultiIndex, Coord, AuxFEMValues, &value);
                        SubVec[i] += value * weight * Elem->Volumn;
                    }
                }
                // 准备刚度计算的各个部件的位置: 行的位置为i*RValueDim*NumRMultiIndex
                if ((ElemBoundaryType[i] == DIRICHLET) || (ElemBoundaryType[i] == STIFFDIRICHLET) || (ElemBoundaryType[i] == MASSDIRICHLET))
                {
                    memset(SubEntries + i * CNumBase, 0.0, CNumBase * sizeof(DOUBLE)); // 相应的行设置为零
                }
                else
                {
                    // 如果不是DIRICHLET边条件
                    for (j = 0; j < CNumBase; j++)
                    {
                        if ((ElemBoundaryType[j] == STIFFDIRICHLET) || (ElemBoundaryType[j] == MASSDIRICHLET))
                        // if (ElemBoundaryType[j] == MASSDIRICHLET)
                        {
                            SubEntries[i * CNumBase + j] = 0.0;
                        }
                        else
                        {
                            // 列的起始位置为: j*CValueDim*NumCMultiIndex
                            // 计算单刚矩阵的(i,j)元素
                            DiscreteFormMatrix(CBaseValues + j * CValueDim * NumCMultiIndex, RBaseValues + i * RValueDim * NumRMultiIndex, Coord,
                                               AuxFEMValues, &value);
                            SubEntries[i * CNumBase + j] += value * weight * Elem->Volumn;
                        }
                    } // end for(j=0;j<CNumBase;j++)
                }     // end if(ElemBoundaryType[i]==DIRICHLET)
            }         // end for(i=0;i<RNumBase;i++)
        }             // end for(k=0;k<NumPoints;k++)

        // // 230404 add
        QUADRATURE *PartialQuadrature = DiscreteForm->PartialQuadrature;              // 获得积分信息
        FUNCTIONVEC *NeumannBoundaryFunction = DiscreteForm->NeumannBoundaryFunction; // Neumann边界函数
        FUNCTIONVEC *RobinBoundaryFunction = DiscreteForm->RobinBoundaryFunction;     // Robin边界函数
        BOUNDARYTYPEFUNCTION *BdType = Rspace->BoundType;                             // 自由度的边界类型
        INT ind_partial, ind_base, ind_dim;
        DOUBLE *point_coord[3], value_temp, weight_coord[2], scale;
        DOUBLE PointRefCoord2D[3][2] = {0.0, 0.0, 1.0, 0.0, 0.0, 1.0};                               // 2D 标准单元中三个顶点的坐标
        DOUBLE PointRefCoord3D[4][3] = {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0}; // 3D 标准单元中三个顶点的坐标
        // 增加对于该单元Neumann边界的处理
        // 对单元的边界类型进行判断 如果有边界是Neumann类型的话对右端项进行更改
        if (DiscreteForm->NeumannBoundaryFunction != NULL)
        {
            NumPoints = PartialQuadrature->NumPoints;
            switch (worlddim)
            {
            case 1:
                RaiseError("SubMatrixAssembleMultiBound", "1D have not deal with Neumann boundary!");
                break;
            case 2:
                // 对单元的线(边界 与点对应)进行循环 判断线的边界类型
                for (i = 0; i < Elem->NumVerts; i++)
                {
                    ind_partial = Rmesh->Faces[ElemInd].Line4Face[i]; // 线在本进程中的全局编号
                    if (BdType(Rmesh->Lines[ind_partial].BD_ID) == NEUMANN)
                    {
                        // 这条线的信息
                        point_coord[0] = Rmesh->Verts[Rmesh->Lines[ind_partial].Vert4Line[0]].Coord;                                                                                                         // Coord Start
                        point_coord[1] = Rmesh->Verts[Rmesh->Lines[ind_partial].Vert4Line[1]].Coord;                                                                                                         // Coord End
                        scale = sqrt((point_coord[0][0] - point_coord[1][0]) * (point_coord[0][0] - point_coord[1][0]) + (point_coord[0][1] - point_coord[1][1]) * (point_coord[0][1] - point_coord[1][1])); // 长度
                        point_coord[0] = PointRefCoord2D[((i + 1) % 3)];                                                                                                                                     // RefCoord Start
                        point_coord[1] = PointRefCoord2D[((i + 2) % 3)];                                                                                                                                     // RefCoord End
                        // printf("%d line point1 %2.10f %2.10f point2 %2.10f %2.10f \n", i, point_coord[0][0], point_coord[0][1], point_coord[1][0], point_coord[1][1]);
                        // 找到这条边界上对应的积分点
                        for (k = 0; k < NumPoints; k++)
                        {
                            weight_coord[0] = PartialQuadrature->QuadX[k];
                            // 获得积分点坐标,参考单元上的坐标
                            RefCoord[0] = weight_coord[0] * point_coord[0][0] + (1 - weight_coord[0]) * point_coord[1][0];
                            RefCoord[1] = weight_coord[0] * point_coord[0][1] + (1 - weight_coord[0]) * point_coord[1][1];
                            weight = PartialQuadrature->QuadW[k];
                            // 计算得到积分点的实际坐标
                            ElementRefCoord2Coord(Elem, RefCoord, Coord);
                            // 直接计算基函数值v
                            memset(RBaseValues, 0.0, RNumBase * RValueDim * sizeof(DOUBLE));
                            GetBaseValues(Rbase, D00, Elem, Coord, RefCoord, RBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            // 计算边界函数值g(借用CBaseValues的地址)
                            memset(CBaseValues, 0.0, RValueDim * sizeof(DOUBLE));   // 边界函数的维数
                            NeumannBoundaryFunction(Coord, RValueDim, CBaseValues); // 取出积分点处Neumann边界的值
                            for (ind_base = 0; ind_base < RNumBase; ind_base++)
                            {
                                if (ElemBoundaryType[ind_base] != DIRICHLET && ElemBoundaryType[ind_base] != STIFFDIRICHLET)
                                {
                                    // (g,v)_{\Gamma_i}
                                    value_temp = 0.0;
                                    for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                    {
                                        value_temp += RBaseValues[ind_base * RValueDim + ind_dim] * CBaseValues[ind_dim];
                                    }
                                    // if (fabs(value_temp) > 1e-12)
                                    //     printf("value_temp %2.10f\n", value_temp);
                                    // 累加到对应的右端项
                                    SubVec[ind_base] += value_temp * weight * scale;
                                }
                            }
                        }
                    }
                }
                break;
            case 3:
                // 对单元的面进行循环 判断面的边界类型
                for (i = 0; i < Elem->NumVerts; i++)
                {
                    ind_partial = Rmesh->Volus[ElemInd].Face4Volu[i];
                    if (BdType(Rmesh->Faces[ind_partial].BD_ID) == NEUMANN)
                    {
                        // 这个面的信息
                        point_coord[0] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[0]].Coord; // Coord_0
                        point_coord[1] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[1]].Coord; // Coord_1
                        point_coord[2] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[2]].Coord; // Coord_2
                        scale = ComputeAreaByCoord(point_coord, worlddim);                           // 面积
                        point_coord[0] = PointRefCoord3D[((i + 1) % 4)];                             // RefCoord_0
                        point_coord[1] = PointRefCoord3D[((i + 2) % 4)];                             // RefCoord_1
                        point_coord[2] = PointRefCoord3D[((i + 3) % 4)];                             // RefCoord_2
                        // 找到这条边界上对应的积分点
                        for (k = 0; k < NumPoints; k++)
                        {
                            weight_coord[0] = PartialQuadrature->QuadX[k];
                            weight_coord[1] = PartialQuadrature->QuadY[k];
                            // 获得积分点坐标,参考单元上的坐标
                            RefCoord[0] = weight_coord[0] * point_coord[0][0] + weight_coord[1] * point_coord[1][0] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][0];
                            RefCoord[1] = weight_coord[0] * point_coord[0][1] + weight_coord[1] * point_coord[1][1] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][1];
                            RefCoord[2] = weight_coord[0] * point_coord[0][2] + weight_coord[1] * point_coord[1][2] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][2];
                            weight = PartialQuadrature->QuadW[k];
                            // 计算得到积分点的实际坐标
                            ElementRefCoord2Coord(Elem, RefCoord, Coord);
                            // 直接计算基函数值v
                            memset(RBaseValues, 0.0, RNumBase * RValueDim * sizeof(DOUBLE));
                            GetBaseValues(Rbase, D00, Elem, Coord, RefCoord, RBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            // 计算边界函数值g(借用CBaseValues的地址)
                            memset(CBaseValues, 0.0, RValueDim * sizeof(DOUBLE));   // 边界函数的维数
                            NeumannBoundaryFunction(Coord, RValueDim, CBaseValues); // 取出积分点处Neumann边界的值
                            for (ind_base = 0; ind_base < RNumBase; ind_base++)
                            {
                                if (ElemBoundaryType[ind_base] != DIRICHLET && ElemBoundaryType[ind_base] != STIFFDIRICHLET)
                                {
                                    // (g,v)_{\Gamma_i}
                                    value_temp = 0.0;
                                    for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                    {
                                        value_temp += RBaseValues[ind_base * RValueDim + ind_dim] * CBaseValues[ind_dim];
                                    }
                                    // 累加到对应的右端项
                                    SubVec[ind_base] += value_temp * weight * scale;
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        DOUBLE C; // Robin边条件系数
        // 增加对于该单元Robin边界的处理
        // 对单元的边界类型进行判断 如果有边界是Robin类型的话对刚度矩阵和右端项进行更改
        if (DiscreteForm->RobinBoundaryFunction != NULL)
        {
            NumPoints = PartialQuadrature->NumPoints;
            switch (worlddim)
            {
            case 1:
                RaiseError("SubMatrixAssembleMultiBound", "1D have not deal with Robin boundary!");
                break;
            case 2:
                // 对单元的线进行循环 判断线的边界类型
                for (i = 0; i < Elem->NumVerts; i++)
                {
                    ind_partial = Rmesh->Faces[ElemInd].Line4Face[i];
                    if (BdType(Rmesh->Lines[ind_partial].BD_ID) == ROBIN)
                    {
                        // 这条线的信息
                        point_coord[0] = Rmesh->Verts[Rmesh->Lines[ind_partial].Vert4Line[0]].Coord;                                                                                                         // Coord Start
                        point_coord[1] = Rmesh->Verts[Rmesh->Lines[ind_partial].Vert4Line[1]].Coord;                                                                                                         // Coord End
                        scale = sqrt((point_coord[0][0] - point_coord[1][0]) * (point_coord[0][0] - point_coord[1][0]) + (point_coord[0][1] - point_coord[1][1]) * (point_coord[0][1] - point_coord[1][1])); // 长度
                        point_coord[0] = PointRefCoord2D[((i + 1) % 3)];                                                                                                                                     // RefCoord Start
                        point_coord[1] = PointRefCoord2D[((i + 2) % 3)];                                                                                                                                     // RefCoord End
                        // 找到这条边界上对应的积分点
                        for (k = 0; k < NumPoints; k++)
                        {
                            weight_coord[0] = PartialQuadrature->QuadX[k];
                            // 获得积分点坐标,参考单元上的坐标
                            RefCoord[0] = weight_coord[0] * point_coord[0][0] + (1 - weight_coord[0]) * point_coord[1][0];
                            RefCoord[1] = weight_coord[0] * point_coord[0][1] + (1 - weight_coord[0]) * point_coord[1][1];
                            weight = PartialQuadrature->QuadW[k];
                            // 计算得到积分点的实际坐标
                            ElementRefCoord2Coord(Elem, RefCoord, Coord);
                            // 直接计算基函数值v
                            memset(RBaseValues, 0.0, RNumBase * RValueDim * sizeof(DOUBLE));
                            GetBaseValues(Rbase, D00, Elem, Coord, RefCoord, RBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            // 计算边界函数值g(借用CBaseValues的地址)
                            // 这里假设CBase至少有两个基函数！！否则空间不够
                            memset(CBaseValues, 0.0, (RValueDim + 1) * sizeof(DOUBLE)); // 边界函数的维数
                            RobinBoundaryFunction(Coord, RValueDim + 1, CBaseValues);   // 取出积分点处Robin边界的值
                            // 这里$Cu+\frac{\partial u}{\partial n} = g$
                            // CBaseValues[4] = {C g1 g2 ...}
                            C = CBaseValues[0];
                            for (ind_base = 0; ind_base < RNumBase; ind_base++)
                            {
                                if (ElemBoundaryType[ind_base] != DIRICHLET && ElemBoundaryType[ind_base] != STIFFDIRICHLET)
                                {
                                    // (g,v)_{\Gamma_i}
                                    value_temp = 0.0;
                                    for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                    {
                                        value_temp += RBaseValues[ind_base * RValueDim + ind_dim] * CBaseValues[ind_dim + 1];
                                    }
                                    // 累加到对应的右端项
                                    SubVec[ind_base] += value_temp * weight * scale;
                                }
                            }
                            // 直接计算基函数值u
                            memset(CBaseValues, 0.0, CNumBase * CValueDim * sizeof(DOUBLE));
                            GetBaseValues(Cbase, D00, Elem, Coord, RefCoord, CBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            for (i = 0; i < RNumBase; i++)
                            {
                                if ((ElemBoundaryType[i] != DIRICHLET) && (ElemBoundaryType[i] != STIFFDIRICHLET) && (ElemBoundaryType[i] != MASSDIRICHLET))
                                {
                                    for (j = 0; j < CNumBase; j++)
                                    {
                                        // (u,v)_{\Gamma_i}
                                        value_temp = 0.0;
                                        for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                        {
                                            value_temp += RBaseValues[i * RValueDim + ind_dim] * CBaseValues[j * CValueDim + ind_dim];
                                        }
                                        // 累加到对应的刚度矩阵
                                        SubEntries[i * CNumBase + j] += value_temp * weight * scale * C;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case 3:
                // 对单元的面进行循环 判断面的边界类型
                for (i = 0; i < Elem->NumVerts; i++)
                {
                    ind_partial = Rmesh->Volus[ElemInd].Face4Volu[i];
                    if (BdType(Rmesh->Faces[ind_partial].BD_ID) == ROBIN)
                    {
                        // 这个面的信息
                        point_coord[0] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[0]].Coord; // Coord_0
                        point_coord[1] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[1]].Coord; // Coord_1
                        point_coord[2] = Rmesh->Verts[Rmesh->Faces[ind_partial].Vert4Face[2]].Coord; // Coord_2
                        scale = ComputeAreaByCoord(point_coord, worlddim);                           // 面积
                        point_coord[0] = PointRefCoord3D[((i + 1) % 4)];                             // RefCoord_0
                        point_coord[1] = PointRefCoord3D[((i + 2) % 4)];                             // RefCoord_1
                        point_coord[2] = PointRefCoord3D[((i + 3) % 4)];                             // RefCoord_2
                        // 找到这条边界上对应的积分点
                        for (k = 0; k < NumPoints; k++)
                        {
                            weight_coord[0] = PartialQuadrature->QuadX[k];
                            weight_coord[1] = PartialQuadrature->QuadY[k];
                            // 获得积分点坐标,参考单元上的坐标
                            RefCoord[0] = weight_coord[0] * point_coord[0][0] + weight_coord[1] * point_coord[1][0] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][0];
                            RefCoord[1] = weight_coord[0] * point_coord[0][1] + weight_coord[1] * point_coord[1][1] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][1];
                            RefCoord[2] = weight_coord[0] * point_coord[0][2] + weight_coord[1] * point_coord[1][2] + (1 - weight_coord[0] - weight_coord[1]) * point_coord[2][2];
                            weight = PartialQuadrature->QuadW[k];
                            // 计算得到积分点的实际坐标
                            ElementRefCoord2Coord(Elem, RefCoord, Coord);
                            // 直接计算基函数值v
                            memset(RBaseValues, 0.0, RNumBase * RValueDim * sizeof(DOUBLE));
                            GetBaseValues(Rbase, D00, Elem, Coord, RefCoord, RBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            // 计算边界函数值g(借用CBaseValues的地址)
                            // 这里假设CBase至少有两个基函数！！否则空间不够
                            memset(CBaseValues, 0.0, (RValueDim + 1) * sizeof(DOUBLE)); // 边界函数的维数
                            RobinBoundaryFunction(Coord, RValueDim + 1, CBaseValues);   // 取出积分点处Robin边界的值
                            // 这里$Cu+\frac{\partial u}{\partial n} = g$
                            // CBaseValues[4] = {C g1 g2 ...}
                            C = CBaseValues[0];
                            for (ind_base = 0; ind_base < RNumBase; ind_base++)
                            {
                                if (ElemBoundaryType[i] != DIRICHLET && ElemBoundaryType[i] != STIFFDIRICHLET)
                                {
                                    // (g,v)_{\Gamma_i}
                                    value_temp = 0.0;
                                    for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                    {
                                        value_temp += RBaseValues[ind_base * RValueDim + ind_dim] * CBaseValues[ind_dim + 1];
                                    }
                                    // 累加到对应的右端项
                                    SubVec[ind_base] += value_temp * weight * scale;
                                }
                            }
                            // 直接计算基函数值u
                            memset(CBaseValues, 0.0, CNumBase * CValueDim * sizeof(DOUBLE));
                            GetBaseValues(Cbase, D00, Elem, Coord, RefCoord, CBaseValues); // 只有一个导数信息 因此不需要另外调换顺序
                            for (i = 0; i < RNumBase; i++)
                            {
                                if ((ElemBoundaryType[i] != DIRICHLET) && (ElemBoundaryType[i] != STIFFDIRICHLET) && (ElemBoundaryType[i] != MASSDIRICHLET))
                                {
                                    for (j = 0; j < CNumBase; j++)
                                    {
                                        // (u,v)_{\Gamma_i}
                                        value_temp = 0.0;
                                        for (ind_dim = 0; ind_dim < RValueDim; ind_dim++)
                                        {
                                            value_temp += RBaseValues[i * RValueDim + ind_dim] * CBaseValues[j * CValueDim + ind_dim];
                                        }
                                        // 累加到对应的刚度矩阵
                                        SubEntries[i * CNumBase + j] += value_temp * weight * scale * C;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
    }
    else
    {
        // DOUBLE Coord_Check[worlddim];
        INT NAuxFEMFun = DiscreteForm->AuxFEMFunction->NAuxFEMFun;
        INT idx_auxfun, NAuxMultiIndexAll, i, j;
        INT *NAuxFEMFunMultiIndex = DiscreteForm->AuxFEMFunction->NAuxFEMFunMultiIndex;
        NAuxMultiIndexAll = 0;
        for (idx_auxfun = 0; idx_auxfun < NAuxFEMFun; idx_auxfun++)
        {
            NAuxMultiIndexAll += NAuxFEMFunMultiIndex[idx_auxfun];
        }
        MULTIINDEX *AuxFEMFunMultiIndex = DiscreteForm->AuxFEMFunction->AuxFEMFunMultiIndex;
        INT NumVerts = Elem->NumVerts;
        // 用于记录细网格小单元的各个顶点在粗网格大单元中RefCoord的X坐标 Y坐标 Z坐标
        DOUBLE *RefCoord_X = malloc(NumVerts * sizeof(DOUBLE));
        DOUBLE *RefCoord_Y = malloc(NumVerts * sizeof(DOUBLE));
        DOUBLE *RefCoord_Z = malloc(NumVerts * sizeof(DOUBLE));
        DOUBLE RefCoordInElemCoarser[worlddim]; // 记录某个积分点在大单元中的RefCoord
        INT idx_vert, idx_related;
        MESH *mesh_finer = DiscreteForm->LeftSpaceInAssemble->Mesh;
        // idx_related = mesh_finer->Ancestors[ElemInd];
        if (DiscreteForm->Relates == NULL)
        {
            idx_related = ElemInd;
        }
        else
        {
            idx_related = DiscreteForm->Relates[ElemInd];
        }
        if ((Rmesh->IsJacobiInfoAvailable) == 0)
        {
            RaiseError("SubAssembleMatrix", "No Inv_Jacobi!");
        }
        DOUBLE *Jacobi = Rmesh->JacobiOfAllElem;
        DOUBLE *Inv_Jacobi = Rmesh->InvJacobiOfAllElem;
        DOUBLE *Volumn = Rmesh->VolumnOfAllElem;
        // 如果存在不同的有限元空间 本函数中传入的Elem和Ind是细空间的小单元的信息
        // 这里生成对应大单元在粗空间中的信息
        ELEMENT *Elem_coarser = ElementInitial(Rmesh);
        ElementBuild(Rmesh, idx_related, Elem_coarser);
        // 对应粗空间单元的Jacobi已经生成过了 下面通过非线性MultiIndex判断细空间单元是否需要生成Jacobi及Inv_Jacobi
        switch (worlddim)
        {
        case 1:
            NoGradIndex = D0;
            break;
        case 2:
            NoGradIndex = D00;
            break;
        case 3:
            NoGradIndex = D000;
            break;
        }
        // (0) 判断是否需要计算Jacobi逆矩阵
        INT tmpind = 0;
        for (i = 0; i < NAuxMultiIndexAll; i++)
        {
            // 这个判断法则意味着我们要在enumeations.h文件中将D10和DO1放在D00的后面
            if (AuxFEMFunMultiIndex[i] > NoGradIndex)
            {
                tmpind = 1;
                break;
            }
        }
        // 加入是否已知单元的Inv_jacobian的判断 如果有省略计算
        INT ii, jj;
        if (tmpind == 0)
        {
            if (mesh_finer->IsJacobiInfoAvailable == 0)
            {
                // 计算单元的体积和Jacobian矩阵
                ComputeElementJacobian(Elem);
            }
            else
            {
                Elem->Volumn = mesh_finer->VolumnOfAllElem[ElemInd];
                for (ii = 0; ii < worlddim; ii++)
                {
                    for (jj = 0; jj < worlddim; jj++)
                    {
                        Elem->Jacobian[ii][jj] = mesh_finer->JacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj];
                    }
                }
            }
        }
        else
        {
            if (mesh_finer->IsJacobiInfoAvailable == 0)
            {
                // 计算单元的体积和Jacobian矩阵的逆矩阵
                ComputeElementInvJacobian(Elem);
            }
            else
            {
                Elem->Volumn = mesh_finer->VolumnOfAllElem[ElemInd];
                for (ii = 0; ii < worlddim; ii++)
                {
                    for (jj = 0; jj < worlddim; jj++)
                    {
                        Elem->Jacobian[ii][jj] = mesh_finer->JacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj];
                        Elem->InvJacobian[ii][jj] = mesh_finer->InvJacobiOfAllElem[(ElemInd * worlddim + ii) * worlddim + jj];
                    }
                }
            }
        } // end if (tmpind == 0)
        // 对粗空间中大单元的Jacobi进行赋值
        for (i = 0; i < worlddim; i++)
        {
            for (j = 0; j < worlddim; j++)
            {
                Elem_coarser->Jacobian[i][j] = Jacobi[worlddim * worlddim * idx_related + i * worlddim + j];
                Elem_coarser->InvJacobian[i][j] = Inv_Jacobi[worlddim * worlddim * idx_related + i * worlddim + j];
            }
        }
        Elem_coarser->Volumn = Volumn[idx_related];

        DOUBLE length_X, length_Y, length_Z;
        // 对小单元中的顶点进行循环 寻找每个顶点的RefCoord
        for (idx_vert = 0; idx_vert < NumVerts; idx_vert++)
        {
            switch (worlddim)
            {
            case 1:
                length_X = Elem->Vert_X[idx_vert] - Elem_coarser->Vert_X[0];
                RefCoord_X[idx_vert] = Inv_Jacobi[idx_related] * length_X;
                break;
            case 2:
                length_X = Elem->Vert_X[idx_vert] - Elem_coarser->Vert_X[0];
                length_Y = Elem->Vert_Y[idx_vert] - Elem_coarser->Vert_Y[0];
                RefCoord_X[idx_vert] = Inv_Jacobi[4 * idx_related] * length_X + Inv_Jacobi[4 * idx_related + 1] * length_Y;
                RefCoord_Y[idx_vert] = Inv_Jacobi[4 * idx_related + 2] * length_X + Inv_Jacobi[4 * idx_related + 3] * length_Y;
                break;
            case 3:
                length_X = Elem->Vert_X[idx_vert] - Elem_coarser->Vert_X[0];
                length_Y = Elem->Vert_Y[idx_vert] - Elem_coarser->Vert_Y[0];
                length_Z = Elem->Vert_Z[idx_vert] - Elem_coarser->Vert_Z[0];
                RefCoord_X[idx_vert] = Inv_Jacobi[9 * idx_related] * length_X + Inv_Jacobi[9 * idx_related + 1] * length_Y + Inv_Jacobi[9 * idx_related + 2] * length_Z;
                RefCoord_Y[idx_vert] = Inv_Jacobi[9 * idx_related + 3] * length_X + Inv_Jacobi[9 * idx_related + 4] * length_Y + Inv_Jacobi[9 * idx_related + 5] * length_Z;
                RefCoord_Z[idx_vert] = Inv_Jacobi[9 * idx_related + 6] * length_X + Inv_Jacobi[9 * idx_related + 7] * length_Y + Inv_Jacobi[9 * idx_related + 8] * length_Z;
                break;
            }
        } // end for (idx_vert = 0; idx_vert < NumVerts; idx_vert++)
        // 先将单刚矩阵设置为0，以免后面出现错误,memset,memcpy
        memset(SubEntries, 0.0, RNumBase * CNumBase * sizeof(DOUBLE));
        if (SubVec != NULL)
            memset(SubVec, 0.0, RNumBase * sizeof(DOUBLE));
        // (1) 对积分点进行遍历
        for (k = 0; k < NumPoints; k++)
        {
            // (1.1) 取出当前积分点的坐标和权重 即是在细单元(Aux)上的RefCoord
            switch (worlddim)
            {
            case 1:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                // 计算得到该积分点在粗网格对应单元中的RefCoord
                RefCoordInElemCoarser[0] = (1 - RefCoord[0]) * RefCoord_X[0] + RefCoord[0] * RefCoord_X[1];
                weight = Quadrature->QuadW[k];
                break;
            case 2:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                // 计算得到该积分点在粗网格对应单元中的RefCoord
                RefCoordInElemCoarser[0] = (1 - RefCoord[0] - RefCoord[1]) * RefCoord_X[0] + RefCoord[0] * RefCoord_X[1] + RefCoord[1] * RefCoord_X[2];
                RefCoordInElemCoarser[1] = (1 - RefCoord[0] - RefCoord[1]) * RefCoord_Y[0] + RefCoord[0] * RefCoord_Y[1] + RefCoord[1] * RefCoord_Y[2];
                weight = Quadrature->QuadW[k];
                break;
            case 3:
                // 获得积分点坐标,参考单元上的坐标
                RefCoord[0] = Quadrature->QuadX[k];
                RefCoord[1] = Quadrature->QuadY[k];
                RefCoord[2] = Quadrature->QuadZ[k];
                // 计算得到该积分点在粗网格对应单元中的RefCoord
                RefCoordInElemCoarser[0] = (1 - RefCoord[0] - RefCoord[1] - RefCoord[2]) * RefCoord_X[0] + RefCoord[0] * RefCoord_X[1] + RefCoord[1] * RefCoord_X[2] + RefCoord[2] * RefCoord_X[3];
                RefCoordInElemCoarser[1] = (1 - RefCoord[0] - RefCoord[1] - RefCoord[2]) * RefCoord_Y[0] + RefCoord[0] * RefCoord_Y[1] + RefCoord[1] * RefCoord_Y[2] + RefCoord[2] * RefCoord_Y[3];
                RefCoordInElemCoarser[2] = (1 - RefCoord[0] - RefCoord[1] - RefCoord[2]) * RefCoord_Z[0] + RefCoord[0] * RefCoord_Z[1] + RefCoord[1] * RefCoord_Z[2] + RefCoord[2] * RefCoord_Z[3];
                weight = Quadrature->QuadW[k];
                break;
            }
            // (1.2) 计算得到积分点的实际坐标
            ElementRefCoord2Coord(Elem_coarser, RefCoordInElemCoarser, Coord);
            // (1.3) 计算基函数微分的值，为组装做准备, 根据基函数求解相应的基函数值
            memset(RBaseValues, 0.0, RNumBase * RValueDim * NumRMultiIndex * sizeof(DOUBLE));
            for (j = 0; j < NumRMultiIndex; j++)
            {

                // GetBaseValues(Rbase, Rmultiindex[j], Elem_coarser, Coord, RefCoordInElemCoarser, RBaseValues + j * RNumBase * RValueDim);
                if (IsHaveRightBaseValuesInPointsOfRefElem == 0)
                {
                    GetBaseValues(Rbase, Rmultiindex[j], Elem_coarser, Coord, RefCoordInElemCoarser, RBaseValues + j * RNumBase * RValueDim);
                }
                else
                {
                    GetBaseValuesWithInfo(Rbase, Rmultiindex[j], Elem_coarser, RightBaseValuesInPointsOfRefElem + k * IndRightMultiIndex[3] * RNumBase * RValueDim, IndRightMultiIndex, RBaseValues + j * RNumBase * RValueDim);
                }
            }
            memset(CBaseValues, 0.0, CNumBase * CValueDim * NumCMultiIndex * sizeof(DOUBLE));
            for (j = 0; j < NumCMultiIndex; j++)
            {
                // GetBaseValues(Cbase, Cmultiindex[j], Elem_coarser, Coord, RefCoordInElemCoarser, CBaseValues + j * CNumBase * CValueDim);
                if (IsHaveLeftBaseValuesInPointsOfRefElem == 0)
                {
                    GetBaseValues(Cbase, Cmultiindex[j], Elem_coarser, Coord, RefCoordInElemCoarser, CBaseValues + j * CNumBase * CValueDim);
                }
                else
                {
                    GetBaseValuesWithInfo(Cbase, Cmultiindex[j], Elem, LeftBaseValuesInPointsOfRefElem + k * IndLeftMultiIndex[3] * CNumBase * CValueDim, IndLeftMultiIndex, CBaseValues + j * CNumBase * CValueDim);
                }
            }
            // 这里add一个检查两种方式计算出的积分点的实际坐标Coord是否相同
            // Coord_Check[0] = Coord[0];
            // Coord_Check[1] = Coord[1];
            // Coord_Check[2] = Coord[2];
            // ElementRefCoord2Coord(Elem, RefCoord, Coord);
            // if (fabs(Coord_Check[0] - Coord[0]) + fabs(Coord_Check[1] - Coord[1]) + fabs(Coord_Check[2] - Coord[2]) > 1.0e-10)
            // {
            //     printf("两种方式计算出的Coord不相同\n");
            //     printf("elem_coarser [%2.10f %2.10f %2.10f]\n", Coord_Check[0], Coord_Check[1], Coord_Check[2]);
            //     printf("elem [%2.10f %2.10f %2.10f]\n", Coord[0], Coord[1], Coord[2]);
            // }
            // (1.4) 计算辅助有限元函数在积分点处的函数值:AuxFEMValues
            if (DiscreteForm->AuxFEMFunction != NULL)
            {
                DiscreteForm->AuxFEMFunction->Points_ind = k;
                ComputeAuxFEMFunValue(DiscreteForm->AuxFEMFunction, ElemInd, Elem, Coord, RefCoord);
            }
            // 进行组装单刚矩阵的计算,单刚矩阵以行优先排列
            // (1.5) 为了后面组装刚度矩阵，我们需要将RBaseValues和CBaseValues进行重新排序成可以直接调用DiscreteFormMatrix的形式
            for (i = 0; i < RNumBase; i++)
            {
                // 准备当前行所对应的基函数各微分算子的值
                for (indRindex = 0; indRindex < NumRMultiIndex; indRindex++)
                {
                    for (inddim = 0; inddim < RValueDim; inddim++)
                    {
                        AuxValues[i * NumRMultiIndex * RValueDim + indRindex * RValueDim + inddim] =
                            RBaseValues[indRindex * RNumBase * RValueDim + i * RValueDim + inddim];
                    }
                }
            }
            // copy back to RBaseValus
            memcpy(RBaseValues, AuxValues, RNumBase * RValueDim * NumRMultiIndex * sizeof(DOUBLE));
            // 处理列的
            for (i = 0; i < CNumBase; i++)
            {
                // 准备当前行所对应的基函数各微分算子的值
                for (indCindex = 0; indCindex < NumCMultiIndex; indCindex++)
                {
                    for (inddim = 0; inddim < CValueDim; inddim++)
                    {
                        AuxValues[i * NumCMultiIndex * CValueDim + indCindex * CValueDim + inddim] =
                            CBaseValues[indCindex * CNumBase * CValueDim + i * CValueDim + inddim];
                    }
                }
            }
            // copy back to CBaseValus
            memcpy(CBaseValues, AuxValues, CNumBase * CValueDim * NumCMultiIndex * sizeof(DOUBLE));
            // (1.6) 组装刚度矩阵
            for (i = 0; i < RNumBase; i++)
            {
                // 右端项的计算
                if (SubVec != NULL)
                {
                    if (ElemBoundaryType[i] == DIRICHLET || ElemBoundaryType[i] == STIFFDIRICHLET)
                    {
                        SubVec[i] = 0.0;
                    }
                    else
                    {
                        DiscreteForm->DiscreteFormVec(RBaseValues + i * RValueDim * NumRMultiIndex, Coord, AuxFEMValues, &value);
                        SubVec[i] += value * weight * Elem->Volumn;
                    }
                }
                // 准备刚度计算的各个部件的位置: 行的位置为i*RValueDim*NumRMultiIndex
                if ((ElemBoundaryType[i] == DIRICHLET) || (ElemBoundaryType[i] == STIFFDIRICHLET) || (ElemBoundaryType[i] == MASSDIRICHLET))
                {
                    memset(SubEntries + i * CNumBase, 0.0, CNumBase * sizeof(DOUBLE));
                }
                else
                {
                    for (j = 0; j < CNumBase; j++)
                    {
                        if ((ElemBoundaryType[j] == STIFFDIRICHLET) || (ElemBoundaryType[j] == MASSDIRICHLET))
                        // if (ElemBoundaryType[j] == MASSDIRICHLET)
                        {
                            SubEntries[i * CNumBase + j] = 0.0;
                        }
                        else
                        {
                            // 列的起始位置为: j*CValueDim*NumCMultiIndex
                            // 计算单刚矩阵的(i,j)元素
                            DiscreteFormMatrix(CBaseValues + j * CValueDim * NumCMultiIndex, RBaseValues + i * RValueDim * NumRMultiIndex, Coord,
                                               AuxFEMValues, &value);
                            SubEntries[i * CNumBase + j] += value * weight * Elem->Volumn;
                        }
                    } // end for(j=0;j<CNumBase;j++)
                }     // end if(ElemBoundaryType[i]==DIRICHLET)
            }         // end for(i=0;i<RNumBase;i++)
        }             // end for (k = 0; k < NumPoints; k++)
    }                 // end if (DiscreteForm->IsUseAuxFEMSpace == 0)
}

void MatrixStructureBuild(MATRIX *matrix, ASSEMBLE_INFO **Assembleinfo, DISCRETEFORM *DiscreteForm)
{
    CSRMAT *csrmat = NULL;
    CSRMatCreation(&csrmat);
    ASSEMBLE_INFO *assemble_info = (ASSEMBLE_INFO *)malloc(sizeof(ASSEMBLE_INFO));
    assemble_info->row_owner = NULL;
    assemble_info->row_owner_index = NULL;
    assemble_info->submat_col_diag = NULL;
    assemble_info->submat_col_index = NULL;
    assemble_info->col_global_index = NULL;
    assemble_info->send_row_num_displs = NULL;
    assemble_info->send_colnum_each_row_displs = NULL;
    assemble_info->send_colindex_each_row = NULL;
    assemble_info->send_ent_each_row = NULL;
    assemble_info->send_row_num = NULL;
    assemble_info->send_col_num = NULL;
    assemble_info->send_col_num_displs = NULL;
    assemble_info->recv_row_num = NULL;
    assemble_info->recv_row_num_displs = NULL;
    assemble_info->recv_col_num = NULL;
    assemble_info->recv_col_num_displs = NULL;
    assemble_info->recv_ent_each_row = NULL;
    assemble_info->recv_row_index = NULL;
    assemble_info->recv_colnum_each_row = NULL;
    assemble_info->recv_colindex_each_row = NULL;

    /** 目前只实现了左右网格是相同的情况 */
    INT ind, i, start, end, LocalNumElems, NumVerts, NumLines, NumFaces, j;
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT *Rdof = &(Rbase->DOF[0]), *Cdof = &(Cbase->DOF[0]);
    INT worlddim = Rmesh->worlddim;
    FACE face;
    LINE line;
    VERT vert;
    VOLU volu;
    INT *VertIndicator = NULL;
    INT *LineIndicator = NULL;
    INT *FaceIndicator = NULL;
    INT NumRows = Rspace->NumDOF;
    csrmat->NumRows = NumRows;
    INT NumColumns = Cspace->NumDOF;
    csrmat->NumColumns = NumColumns;
    INT *RowPtr = malloc((NumRows + 1) * sizeof(INT));
    memset(RowPtr, 0, (NumRows + 1) * sizeof(INT));
    INT *Rlocalindex = Rspace->LocalIndex; // 记录本进程中每个单元上的自由度编号
    INT *Rbeginindex = Rspace->BeginIndex; // 记录本进程中每个单元上自由度编号开始的位置
    INT *Clocalindex = Cspace->LocalIndex;
    INT *Cbeginindex = Cspace->BeginIndex;
    INT tmp, NumEntries, Rstart, Rend, Cstart, Cend, currow, curcol, k, curpos;
    INT *KCol, *Pos;
    if (worlddim == 2)
    {
        LocalNumElems = Rmesh->num_face;
        INT LineVert0[3] = {1, 2, 0};
        INT LineVert1[3] = {2, 0, 1};

        if (Cdof[0] > 0)
        {
            VertIndicator = malloc(Cmesh->num_vert * sizeof(INT));
            memset(VertIndicator, 0, (Cmesh->num_vert) * sizeof(INT));
        }
        if (2 * Cdof[0] + Cdof[1] > 0)
        {
            LineIndicator = malloc(Cmesh->num_line * sizeof(INT));
            memset(LineIndicator, 0, (Cmesh->num_line) * sizeof(INT));
        }
        // 进行面遍历
        for (ind = 0; ind < LocalNumElems; ind++)
        {
            face = Rmesh->Faces[ind];
            start = Rbeginindex[ind];
            NumVerts = face.NumVerts;
            NumLines = face.NumLines;
            // 先处理点和点上的自由度
            if (Rdof[0] > 0)
            {
                // 表示点上有自由度

                for (i = 0; i < NumVerts; i++)
                {
                    if (VertIndicator[face.Vert4Face[i]] == 0)
                    {
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            // 将该节点的非零元素加上相应的值
                            RowPtr[Rlocalindex[start + Rdof[0] * i + j]] += Cdof[0];
                        }
                        VertIndicator[face.Vert4Face[i]] = 1;
                    }
                }
            } // end if(Rdof[0]>0)
            // 接下来处理由线向量的自由度直接的关系
            if ((Rdof[0] + Rdof[1] > 0) && (Cdof[0] + Cdof[1] > 0))
            {
                // 线上的自由度
                for (i = 0; i < NumLines; i++)
                {
                    if (LineIndicator[face.Line4Face[i]] == 0)
                    {
                        // 处理线上两个点的自由度的相对行
                        // 处理线上的第一个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * LineVert0[i] + j]] += Cdof[0] + Cdof[1];
                        }
                        // 处理线上的第二个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * LineVert1[i] + j]] += Cdof[0] + Cdof[1];
                        }
                        // 处理线上的自由度
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * i + j]] += 2 * Cdof[0] + Cdof[1];
                        }                                     // end 线上的自由度处理完毕
                        LineIndicator[face.Line4Face[i]] = 1; // 标记处理完的这个线为1
                    }
                }
            } // end if((Rdof[0]+Rdof[1]>0)&&(Cdof[0]+Cdof[1]>0))
            // 接下来处理面, 只要面上有自由度就需要进行处理
            if ((Rdof[0] + Rdof[1] + Rdof[2] > 0) && (Cdof[0] + Cdof[1] + Cdof[2]) > 0)
            {
                // 面上的每个自由度都需要进行处理
                // 下处理面上点的自由度
                if (Rdof[0] > 0)
                {
                    // 处理点上由于面而产生的非零元个数
                    for (i = 0; i < NumVerts; i++)
                    {
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * i + j]] += Cdof[1] * (NumLines - 2) + Cdof[2];
                        }
                    } // end i
                }     // end if(Rdof[0]>0)
                // 接下来处理线上的自由度（由于面而带来的非零元个数）
                if (Rdof[1] > 0)
                {
                    for (i = 0; i < NumLines; i++)
                    {
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * i + j]] += (NumVerts - 2) * Cdof[0] + (NumLines - 1) * Cdof[1] + Cdof[2];
                        }
                    }
                }
                // 最后来处理面上的自由度(由于面而带来的非零元个数)
                if (Rdof[2] > 0)
                {
                    for (i = 0; i < Rdof[2]; i++)
                    {
                        // 与面上所有其它自由度都有关系
                        RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * NumLines + i]] += Cdof[0] * NumVerts + Cdof[1] * NumLines + Cdof[2];
                    }
                } // end if(Rdof[2]>0)
            }     // end if((Rdof[0]+Rdof[1]+Rdof[2]>0)&&(Cdof[0]+Cdof[1]+Cdof[2])>0)
        }         // end for(ind=0;ind<LocalNumElems;ind++)
        // 接下来具体生成矩阵的结构，即生成最后的RowPtr和Kcol的数据
        // 同样进行单元遍历
        // 首先产生需要的RowPtr(目前存储的是每行的非零元个数)
        NumEntries = RowPtr[0];
        RowPtr[0] = 0;
        for (i = 1; i <= NumRows; i++)
        {
            tmp = RowPtr[i]; // 记录当前位置的数值
            RowPtr[i] = NumEntries;
            NumEntries += tmp; // 更新cum的数值
        }
        KCol = malloc(NumEntries * sizeof(INT));
        memset(KCol, -1, (NumEntries) * sizeof(INT));
        Pos = malloc(NumRows * sizeof(INT));
        memset(Pos, 0, NumRows * sizeof(INT));
        for (ind = 0; ind < LocalNumElems; ind++)
        {
            face = Rmesh->Faces[ind];    // 得到当前的面
            Rstart = Rbeginindex[ind];   // 当前单元所在的自由度的起始位置
            Rend = Rbeginindex[ind + 1]; // 当前单元所在的自由度的终点位置
            Cstart = Cbeginindex[ind];
            Cend = Cbeginindex[ind + 1];
            NumVerts = face.NumVerts;
            NumLines = face.NumLines;
            // 处理该单元上的每个自由度
            for (i = Rstart; i < Rend; i++)
            {
                // 该单元上的第i个自由度
                currow = Rlocalindex[i];
                start = RowPtr[currow];
                curpos = Pos[currow];
                for (j = Cstart; j < Cend; j++)
                {
                    // 该单元的第j个自由度
                    curcol = Clocalindex[j];
                    // 把该列号放入KCol中, 先检查是否已经放入
                    tmp = 0;
                    for (k = 0; k < curpos; k++)
                    {
                        if (curcol == KCol[start + k])
                        {
                            // 表示该列号已经加入了KCol，不需要进行任何处理
                            k = curpos + 2;
                            tmp = 1;
                        }
                    } // end for k
                    if (tmp == 0)
                    {
                        KCol[start + curpos] = curcol;
                        curpos++;
                    } // end if(k!=curpos+2)
                }     // end for j
                Pos[currow] = curpos;
            } // end for i
        }     // end for(ind=0;ind<LocalNumElems;ind++)
        free(Pos);
        free(VertIndicator);
        free(LineIndicator);
        csrmat->NumEntries = NumEntries;
        csrmat->RowPtr = RowPtr;
        csrmat->KCol = KCol;
    }
    // 三维四面体网格的情况
    // 左右网格不一样的时候，我们假设行所对应的网格是细网格？
    if (worlddim == 3)
    {
        LocalNumElems = Rmesh->num_volu;
        INT LineVert0[6] = {0, 0, 0, 1, 1, 2};
        INT LineVert1[6] = {1, 2, 3, 2, 3, 3};
        INT FaceVert0[4] = {1, 0, 0, 0};
        INT FaceVert1[4] = {2, 3, 1, 2};
        INT FaceVert2[4] = {3, 2, 3, 1};
        INT FaceLine0[4] = {5, 5, 4, 3};
        INT FaceLine1[4] = {4, 1, 2, 0};
        INT FaceLine2[4] = {3, 2, 0, 1};

        if (Cdof[0] > 0)
        {
            // 为所有的节点定义一个Indicator
            VertIndicator = malloc(Cmesh->num_vert * sizeof(INT));
            memset(VertIndicator, 0, Cmesh->num_vert * sizeof(INT)); // 赋0
        }
        if (2 * Cdof[0] + Cdof[1] > 0)
        {
            // 每个线上的自由度加上两个端点的自由度个数大于0就进行定义相应的Indicator
            LineIndicator = malloc(Cmesh->num_line * sizeof(INT));
            memset(LineIndicator, 0, Cmesh->num_line * sizeof(INT));
        }
        if (3 * Cdof[0] + 3 * Cdof[1] + Cdof[2] > 0)
        {
            // 面上的情况
            FaceIndicator = malloc(Cmesh->num_face * sizeof(INT));
            memset(FaceIndicator, 0, Cmesh->num_face * sizeof(INT));
        }
        // 进行体遍历
        for (ind = 0; ind < LocalNumElems; ind++)
        {
            volu = Rmesh->Volus[ind]; // 获得当前的体对象
            // printf("ind=%d\n", ind);
            start = Rbeginindex[ind]; // 获得当前体上的自由度开始的位置
            // printf("Start!\n");
            NumVerts = volu.NumVerts; // 获得当前体上点的个数
            NumLines = volu.NumLines; // 获得当前体上线的个数
            NumFaces = volu.NumFaces; // 获得当前体上面的个数
            // 先处理点和点上的自由度
            if (Rdof[0] > 0 && Cdof[0] > 0)
            {
                // 表示点上有自由度
                for (i = 0; i < NumVerts; i++)
                {
                    // 目前在当前体上的第i个节点
                    if (VertIndicator[volu.Vert4Volu[i]] == 0)
                    {
                        // 表示当前节点没有被处理过
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            // 在当前节点所对应的行上加上相应的非零元个数
                            RowPtr[Rlocalindex[start + Rdof[0] * i + j]] += Cdof[0];
                        }
                        VertIndicator[volu.Vert4Volu[i]] = 1; // 表示当前节点与自己节点上的非零元个数已经处理过了
                    }
                }
            } // end if(Rdof[0]>0)
            // printf("After vert!\n");
            // 接下来处理由线向量的自由度直接的关系
            if ((Rdof[0] + Rdof[1] > 0) && (Cdof[0] + Cdof[1] > 0))
            {
                // 线上的自由度, 处理与当前线上的自由度相关的行
                for (i = 0; i < NumLines; i++)
                {
                    // 第i条边
                    if (LineIndicator[volu.Line4Volu[i]] == 0)
                    {
                        // 处理线上两个点的自由度的相对行
                        // 处理线上的第一个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            // 加上另一个点上的自由度和线上的自由度
                            RowPtr[Rlocalindex[start + Rdof[0] * LineVert0[i] + j]] += Cdof[0] + Cdof[1];
                        }
                        // 处理线上的第二个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * LineVert1[i] + j]] += Cdof[0] + Cdof[1];
                        }
                        // 处理线上的自由度
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            // 当前线上的自由度的非零元个数是两个端点的自由度加上线上本身的自由度
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * i + j]] += 2 * Cdof[0] + Cdof[1];
                        } // end 线上的自由度处理完毕
                        LineIndicator[volu.Line4Volu[i]] = 1;
                    } // end if(LineIndicator[volu.Line4Volu[i]]==0)
                }     // end for(i=0;i<NumLines;i++)
            }         // end if((Rdof[0]+Rdof[1]>0)&&(Cdof[0]+Cdof[1]>0))
            // printf("After line!\n");
            // 接下来处理面, 只要面上有自由度就需要进行处理
            if ((Rdof[0] + Rdof[1] + Rdof[2] > 0) && (Cdof[0] + Cdof[1] + Cdof[2]) > 0)
            {
                // 面上的每个自由度都需要进行处理
                for (i = 0; i < NumFaces; i++)
                {
                    if (FaceIndicator[volu.Face4Volu[i]] == 0)
                    {
                        // 下面的过程应该可以写的更加紧凑一些，或者更加广泛一些
                        // 目前处理的是三角形
                        // 处理面上第一个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            // 加上由于面所带来的新的非零元个数（对边的非零元个数加上面上的个数）
                            RowPtr[Rlocalindex[start + Rdof[0] * FaceVert0[i] + j]] += Cdof[1] + Cdof[2];
                        }
                        // 处理面上第二个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * FaceVert1[i] + j]] += Cdof[1] + Cdof[2];
                        }
                        // 处理面上第三个点
                        for (j = 0; j < Rdof[0]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * FaceVert2[i] + j]] += Cdof[1] + Cdof[2];
                        }
                        // 处理面上第一个线
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            // 面带来了: 对面点上的自由度，另两个边上的自由度和面上的自由度
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * FaceLine0[i] + j]] +=
                                Cdof[0] + 2 * Cdof[1] + Cdof[2];
                        }
                        // 处理面上第二个线
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * FaceLine1[i] + j]] +=
                                Cdof[0] + 2 * Cdof[1] + Cdof[2];
                        }
                        // 处理面上第三个线
                        for (j = 0; j < Rdof[1]; j++)
                        {
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * FaceLine2[i] + j]] +=
                                Cdof[0] + 2 * Cdof[1] + Cdof[2];
                        }
                        // 处理面上自由度
                        for (j = 0; j < Rdof[2]; j++)
                        {
                            // 非零元个数为: 三个点、三条边和自己面上的自由度个数
                            RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * NumLines + Rdof[2] * i + j]] += 3 * Cdof[0] + 3 * Cdof[1] + Rdof[2];
                        } // end 面上的自由度处理完毕
                        FaceIndicator[volu.Face4Volu[i]] = 1;
                    } // end if(FaceIndicator[volu.Face4Volu[i]]==0)
                }     // end for(i=0;i<NumFaces;i++)
            }         // end if((Rdof[0]+Rdof[1]+Rdof[2]>0)&&(Cdof[0]+Cdof[1]+Cdof[2])>0)
            // printf("After face!\n");
            // 接下来处理体的自由度所对应的行
            if ((Rdof[0] + Rdof[1] + Rdof[2] + Rdof[3] > 0) && (Cdof[0] + Cdof[1] + Cdof[2] + Cdof[3]) > 0)
            {
                // 处理点上由于体而产生的非零元个数
                for (i = 0; i < NumVerts; i++)
                {
                    for (j = 0; j < Rdof[0]; j++)
                    {
                        // 加上相对面上的自由度和体上的自由度个数
                        RowPtr[Rlocalindex[start + Rdof[0] * i + j]] += Cdof[2] * (NumFaces - 3) + Cdof[3];
                    }
                }
                // 处理线上由于体而产生的非零元个数
                for (i = 0; i < NumLines; i++)
                {
                    for (j = 0; j < Rdof[1]; j++)
                    {
                        // 加上不相连的另一条线上的自由、另两个面上的自由度、体上的自由度
                        RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * i + j]] +=
                            Cdof[1] + Cdof[2] * (NumFaces - 2) + Cdof[3];
                    }
                }
                // 处理面上由于体而产生的非零元个数
                for (i = 0; i < NumFaces; i++)
                {
                    for (j = 0; j < Rdof[2]; j++)
                    {
                        // 加上对面的点的自由度、另外三条线上的自由度、其它面上的自由度、体上的自由度
                        RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * NumLines + Rdof[2] * i + j]] +=
                            Cdof[0] * (NumVerts - 3) + Cdof[1] * (NumLines - 3) + Cdof[2] * (NumFaces - 1) + Cdof[3];
                    }
                }
                // 处理体上由于体而产生的非零元个数
                for (j = 0; j < Rdof[3]; j++)
                {
                    // 体上所有的自由度: 点、线、面、体上的自由度
                    RowPtr[Rlocalindex[start + Rdof[0] * NumVerts + Rdof[1] * NumLines + Rdof[2] * NumFaces + j]] +=
                        Cdof[0] * NumVerts + Cdof[1] * NumLines + Cdof[2] * NumFaces + Cdof[3];
                }
            } // end if((Rdof[0]+Rdof[1]+Rdof[2]+Rdof[3]>0)&&(Cdof[0]+Cdof[1]+Cdof[2]+Cdof[3])>0)
        }     // end for(ind=0;ind<LocalNumElems;ind++)
        // 接下来具体生成矩阵的结构，即生成最后的RowPtr和Kcol的数据
        // 同样进行单元遍历
        // 首先产生需要的RowPtr(目前存储的是每行的非零元个数)
        NumEntries = RowPtr[0];
        RowPtr[0] = 0;
        for (i = 1; i <= NumRows; i++)
        {
            tmp = RowPtr[i]; // 记录当前位置的数值
            RowPtr[i] = NumEntries;
            NumEntries += tmp; // 更新cum的数值
        }
        // 如此获得了本进程内矩阵非零元的个数以及每一行上面的非零元个数
        KCol = malloc(NumEntries * sizeof(INT));    // 开辟空间
        memset(KCol, -1, NumEntries * sizeof(INT)); // 全都赋值为-1
        Pos = malloc(NumRows * sizeof(INT));        // 用来记录每一行的位置
        memset(Pos, 0, NumRows * sizeof(INT));      // 赋值为0
        for (ind = 0; ind < LocalNumElems; ind++)
        {
            // 再次进行单元循环
            volu = Rmesh->Volus[ind];
            Rstart = Rbeginindex[ind];   // 本单元上行所对应的自由度起始位置
            Rend = Rbeginindex[ind + 1]; // 本单元上行所对应的自由度终止位置
            Cstart = Cbeginindex[ind];   // 本单元上列所对应的自由度起始位置
            Cend = Cbeginindex[ind + 1]; // 本单元上列所对应的自由度终止位置
            NumVerts = volu.NumVerts;
            NumLines = volu.NumLines;
            NumFaces = volu.NumFaces;
            // 处理该单元上的每个自由度
            for (i = Rstart; i < Rend; i++)
            {
                // 该单元上的第i个自由度
                currow = Rlocalindex[i]; // 获得当前的行号
                start = RowPtr[currow];  // 矩阵当前行非零元的起始位置
                curpos = Pos[currow];    // 矩阵当前行非零元的当前位置
                for (j = Cstart; j < Cend; j++)
                {
                    // 该单元的第j个自由度
                    curcol = Clocalindex[j];
                    // 把该列号放入KCol中
                    for (k = 0; k < curpos; k++)
                    {
                        if (curcol == KCol[start + k])
                        {
                            // 表示该列号已经加入了KCol，不需要进行任何处理
                            k = curpos + 2;
                        }
                    }
                    if (k == curpos)
                    {
                        // 表示当前的列号还没有加入到KCol中，那么就将之加入
                        KCol[start + curpos] = curcol;
                        curpos++; // 然后当前非零元的位置加1
                    }
                }
                Pos[currow] = curpos; // 处理完了当前的矩阵行，最后记录当前行非零元的最新位置
            }                         // end for (i = Rstart; i < Rend; i++)
        }                             // end for(ind=0;ind<LocalNumElems;ind++)
        free(Pos);                    // 处理完所有的行就将Pos释放掉
        if (Cdof[0] > 0)
        {
            free(VertIndicator);
        }
        if (2 * Cdof[0] + Cdof[1] > 0)
        {
            free(LineIndicator);
        }
        if (3 * Cdof[0] + 3 * Cdof[1] + Cdof[2] > 0)
        {
            free(FaceIndicator);
        }
        csrmat->NumEntries = NumEntries;
        csrmat->RowPtr = RowPtr;
        csrmat->KCol = KCol;
    } // end if (worlddim == 3)
    // printf("11111111\n");
    // 至此, 本进程中的矩阵结构生成完毕, 接下来生成全局矩阵和进行相应的调整
    MPI_Comm testcomm = Rmesh->comm;
    INT myrank, mysize;
    MPI_Comm_rank(testcomm, &myrank);
    MPI_Comm_size(testcomm, &mysize);
    // 计算本进程中要发送出去的行
    INT neignum = Rmesh->SharedInfo->NumNeighborRanks;
    INT *neigranks = Rmesh->SharedInfo->NeighborRanks;
    SHAREDDOF *RsharedDOF = Rspace->SharedDOF, *CsharedDOF = Cspace->SharedDOF;
    SHAREDDOF *sharedDOF = NULL;
    INT idx_neig, neigrank;
    INT *owner = NULL;
    // 本进程要发送和收到的行个数
    INT *send_row_num = (INT *)calloc(neignum, sizeof(INT));
    INT *recv_row_num = (INT *)calloc(neignum, sizeof(INT));
    INT *index = NULL;
    INT *row_owner = (INT *)malloc(NumRows * sizeof(INT));
    INT *row_owner_index = (INT *)malloc(NumRows * sizeof(INT));
    memset(row_owner, -1, NumRows * sizeof(INT));
    memset(row_owner_index, -1, NumRows * sizeof(INT));
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        sharedDOF = &(RsharedDOF[idx_neig]);
        neigrank = neigranks[idx_neig];
        owner = sharedDOF->Owner;
        index = sharedDOF->Index;
        for (i = 0; i < sharedDOF->SharedNum; i++)
        {
            if (owner[i] == neigrank)
            {
                row_owner[index[i]] = idx_neig;
                row_owner_index[index[i]] = send_row_num[idx_neig];
                send_row_num[idx_neig]++;
            }
            else if (owner[i] == myrank)
            {
                recv_row_num[idx_neig]++;
            }
        }
    }
    INT *send_row_num_displs = (INT *)malloc(neignum * sizeof(INT));
    INT *recv_row_num_displs = (INT *)malloc(neignum * sizeof(INT));
    send_row_num_displs[0] = 0, recv_row_num_displs[0] = 0;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        send_row_num_displs[idx_neig] = send_row_num_displs[idx_neig - 1] + send_row_num[idx_neig - 1];
        recv_row_num_displs[idx_neig] = recv_row_num_displs[idx_neig - 1] + recv_row_num[idx_neig - 1];
    }
    INT total_send_row_num = send_row_num_displs[neignum - 1] + send_row_num[neignum - 1];
    INT total_recv_row_num = recv_row_num_displs[neignum - 1] + recv_row_num[neignum - 1];
    // 本进程要发送和收到的每行的元个数
    INT *send_entnum_each_row = (INT *)malloc(total_send_row_num * sizeof(INT));
    INT *recv_entnum_each_row = (INT *)malloc(total_recv_row_num * sizeof(INT));
    INT *send_colnum_each_row_displs = (INT *)malloc((total_send_row_num + 1) * sizeof(INT));
    INT *send_row_index = (INT *)malloc(total_send_row_num * sizeof(INT));
    INT *recv_row_index = (INT *)malloc(total_recv_row_num * sizeof(INT));
    total_send_row_num = 0;
    total_recv_row_num = 0;
    INT num_rows = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        sharedDOF = &(RsharedDOF[idx_neig]);
        neigrank = neigranks[idx_neig];
        owner = sharedDOF->Owner;
        index = sharedDOF->Index;
        for (i = 0; i < sharedDOF->SharedNum; i++)
        {
            if (owner[i] == neigrank)
            {
                send_row_index[total_send_row_num] = index[i];
                num_rows = RowPtr[index[i] + 1] - RowPtr[index[i]];
                send_entnum_each_row[total_send_row_num] = num_rows;
                total_send_row_num++;
            }
            else if (owner[i] == myrank)
            {
                recv_row_index[total_recv_row_num] = index[i];
                total_recv_row_num++;
            }
        }
    }

    MPI_Comm comm = Rmesh->SharedInfo->neighborcomm;
    MPI_Request colnumreq;
    MPI_Ineighbor_alltoallv(send_entnum_each_row, send_row_num, send_row_num_displs, MPI_INT,
                            recv_entnum_each_row, recv_row_num, recv_row_num_displs, MPI_INT, comm, &colnumreq);
    send_colnum_each_row_displs[0] = 0;
    for (i = 0; i < total_send_row_num; i++)
    {
        send_colnum_each_row_displs[i + 1] = send_colnum_each_row_displs[i] + send_entnum_each_row[i];
    }
    MPI_Wait(&colnumreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&colnumreq);

    // 每行具体的列号
    INT *send_col_num = (INT *)calloc(neignum, sizeof(INT));
    INT *recv_col_num = (INT *)calloc(neignum, sizeof(INT));
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        for (i = 0; i < send_row_num[idx_neig]; i++)
        {
            send_col_num[idx_neig] += send_entnum_each_row[send_row_num_displs[idx_neig] + i];
        }
        for (i = 0; i < recv_row_num[idx_neig]; i++)
        {
            recv_col_num[idx_neig] += recv_entnum_each_row[recv_row_num_displs[idx_neig] + i];
        }
    }
    INT *send_col_num_displs = (INT *)malloc(neignum * sizeof(INT));
    INT *recv_col_num_displs = (INT *)malloc(neignum * sizeof(INT));
    send_col_num_displs[0] = 0, recv_col_num_displs[0] = 0;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        send_col_num_displs[idx_neig] = send_col_num_displs[idx_neig - 1] + send_col_num[idx_neig - 1];
        recv_col_num_displs[idx_neig] = recv_col_num_displs[idx_neig - 1] + recv_col_num[idx_neig - 1];
    }
    INT total_send_col_num = send_col_num_displs[neignum - 1] + send_col_num[neignum - 1];
    INT total_recv_col_num = recv_col_num_displs[neignum - 1] + recv_col_num[neignum - 1];
    // 本进程要发送和收到的每行的列号
    INT *send_ent_each_row = (INT *)malloc(total_send_col_num * sizeof(INT));
    INT *recv_ent_each_row = (INT *)malloc(total_recv_col_num * sizeof(INT));
    total_send_col_num = 0, total_recv_col_num = 0;
    INT idx = 0, idy = 0, position;
    INT *kcol = csrmat->KCol, *rowptr = csrmat->RowPtr;
    INT *col_globalindex = Cspace->GlobalIndex;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        idx = send_row_num_displs[idx_neig];
        for (i = 0; i < send_row_num[idx_neig]; i++)
        {
            start = rowptr[send_row_index[idx]];
            position = total_send_col_num;
            for (idy = 0; idy < send_entnum_each_row[idx]; idy++)
            {
                send_ent_each_row[total_send_col_num] = col_globalindex[kcol[start + idy]];
                total_send_col_num++;
            }
            QuickSort_Int(send_ent_each_row, position, total_send_col_num - 1);
            idx++;
        }
    }
    MPI_Request colindexreq;
    MPI_Ineighbor_alltoallv(send_ent_each_row, send_col_num, send_col_num_displs, MPI_INT,
                            recv_ent_each_row, recv_col_num, recv_col_num_displs, MPI_INT, comm, &colindexreq);

    INT num = 0;
    assemble_info->send_row_num_displs = send_row_num_displs;
    assemble_info->send_colnum_each_row_displs = send_colnum_each_row_displs;
    assemble_info->send_colindex_each_row = send_ent_each_row;

    // 先做包含重复的统计
    INT rows_start = matrix->rows_start, rows_end = matrix->rows_end;
    INT rows_num = matrix->local_nrows;
    INT *repeat_entnum_each_row = (INT *)malloc(rows_num * sizeof(INT));
    // (1) 本地
    INT idx_row = 0, rowindex_local = 0;
    INT *row_globalindex = Rspace->GlobalIndex;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        if (row_globalindex[idx_row] >= rows_start && row_globalindex[idx_row] < rows_end)
        {
            rowindex_local = row_globalindex[idx_row] - rows_start;
            row_owner_index[idx_row] = rowindex_local;
            repeat_entnum_each_row[rowindex_local] = RowPtr[idx_row + 1] - RowPtr[idx_row];
        }
    }
    // (2) 邻居
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        idx = recv_row_num_displs[idx_neig];
        for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
        {
            rowindex_local = row_globalindex[recv_row_index[idx]] - rows_start;
            repeat_entnum_each_row[rowindex_local] += recv_entnum_each_row[idx];
            idx++;
        }
    }
    // (3) 把重复的复制过来
    INT *repeat_entnum_each_row_displs = (INT *)malloc((rows_num + 1) * sizeof(INT));
    repeat_entnum_each_row_displs[0] = 0;
    for (idx_row = 0; idx_row < rows_num; idx_row++)
    {
        repeat_entnum_each_row_displs[idx_row + 1] = repeat_entnum_each_row_displs[idx_row] + repeat_entnum_each_row[idx_row];
    }
    INT *repeat_kcol = (INT *)malloc(repeat_entnum_each_row_displs[rows_num] * sizeof(INT));
    // (3.1) 本地
    INT idx_col = 0, start2 = 0;
    for (idx_row = 0; idx_row < NumRows; idx_row++)
    {
        if (row_globalindex[idx_row] >= rows_start && row_globalindex[idx_row] < rows_end)
        {
            rowindex_local = row_globalindex[idx_row] - rows_start;
            start2 = RowPtr[idx_row];
            num = RowPtr[idx_row + 1] - start2;
            repeat_entnum_each_row[rowindex_local] = num;
            start = repeat_entnum_each_row_displs[rowindex_local];
            for (idx_col = 0; idx_col < num; idx_col++)
            {
                repeat_kcol[start + idx_col] = col_globalindex[KCol[start2 + idx_col]];
            }
        }
    }
    // (3.2) 邻居
    MPI_Wait(&colindexreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&colindexreq);
    assemble_info->send_row_num = send_row_num;
    assemble_info->send_col_num = send_col_num;
    assemble_info->send_col_num_displs = send_col_num_displs;
    OpenPFEM_Free(send_row_index);
    OpenPFEM_Free(send_entnum_each_row);

    total_recv_col_num = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        idx = recv_row_num_displs[idx_neig];
        for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
        {
            rowindex_local = row_globalindex[recv_row_index[idx]] - rows_start;
            start = repeat_entnum_each_row_displs[rowindex_local] + repeat_entnum_each_row[rowindex_local];
            num = recv_entnum_each_row[idx];
            repeat_entnum_each_row[rowindex_local] += num;
            memcpy(&(repeat_kcol[start]), &(recv_ent_each_row[total_recv_col_num]), num * sizeof(INT));
            idx++;
            total_recv_col_num += num;
        }
    }

    // (4) 对每一行按全局编号重新排序
    for (idx_row = 0; idx_row < rows_num; idx_row++)
    {
        start = repeat_entnum_each_row_displs[idx_row];
        end = repeat_entnum_each_row_displs[idx_row + 1];
        QuickSort_Int(repeat_kcol, start, end - 1);
    }

    assemble_info->recv_row_num = recv_row_num;
    assemble_info->recv_row_num_displs = recv_row_num_displs;
    assemble_info->recv_col_num = recv_col_num;
    assemble_info->recv_col_num_displs = recv_col_num_displs;
    assemble_info->recv_row_index = recv_row_index;
    assemble_info->recv_colnum_each_row = recv_entnum_each_row;
    assemble_info->recv_colindex_each_row = recv_ent_each_row;
    CSRMatDestory(csrmat);

    // 分开 diag 和 ndiag
    INT cols_start = matrix->cols_start, cols_end = matrix->cols_end;
    INT cols_num = matrix->local_ncols;
    INT current_col = -1, diag_entnum = 0, ndiag_entnum = 0;
    if (DataTypeOpenPFEMEqual(matrix->type))
    {
        CSRMAT *diag = NULL, *ndiag = NULL;
        CSRMatCreation(&diag);
        CSRMatCreation(&ndiag);
        diag->NumRows = rows_num;
        diag->NumColumns = cols_num;
        ndiag->NumRows = rows_num;
        INT *diag_rowptr = (INT *)malloc((rows_num + 1) * sizeof(INT));
        INT *ndiag_rowptr = (INT *)malloc((rows_num + 1) * sizeof(INT));
        diag_rowptr[0] = 0, ndiag_rowptr[0] = 0;
        for (idx_row = 0; idx_row < rows_num; idx_row++)
        {
            start = repeat_entnum_each_row_displs[idx_row];
            num = repeat_entnum_each_row[idx_row];
            current_col = -1, diag_entnum = 0, ndiag_entnum = 0;
            for (i = 0; i < num; i++)
            {
                if (current_col < repeat_kcol[start + i])
                {
                    current_col = repeat_kcol[start + i];
                    if (current_col < cols_start || current_col >= cols_end)
                    {
                        ndiag_entnum++;
                    }
                    else
                    {
                        diag_entnum++;
                    }
                }
            }
            diag_rowptr[idx_row + 1] = diag_rowptr[idx_row] + diag_entnum;
            ndiag_rowptr[idx_row + 1] = ndiag_rowptr[idx_row] + ndiag_entnum;
        }
        diag->RowPtr = diag_rowptr;
        ndiag->RowPtr = ndiag_rowptr;
        diag->NumEntries = diag_rowptr[rows_num];
        ndiag->NumEntries = ndiag_rowptr[rows_num];

        INT *diag_kcol = (INT *)malloc(diag_rowptr[rows_num] * sizeof(INT));
        INT *ndiag_kcol = (INT *)malloc(ndiag_rowptr[rows_num] * sizeof(INT));
        diag_entnum = 0, ndiag_entnum = 0;
        for (idx_row = 0; idx_row < rows_num; idx_row++)
        {
            start = repeat_entnum_each_row_displs[idx_row];
            num = repeat_entnum_each_row[idx_row];
            current_col = -1;
            for (i = 0; i < num; i++)
            {
                if (current_col < repeat_kcol[start + i])
                {
                    current_col = repeat_kcol[start + i];
                    if (current_col < cols_start || current_col >= cols_end)
                    {
                        ndiag_kcol[ndiag_entnum] = current_col;
                        ndiag_entnum++;
                    }
                    else
                    {
                        diag_kcol[diag_entnum] = current_col - cols_start;
                        diag_entnum++;
                    }
                }
            }
        }
        diag->KCol = diag_kcol;
        ndiag->KCol = ndiag_kcol;
        DOUBLE *diag_entries = (DOUBLE *)calloc(diag_rowptr[rows_num], sizeof(DOUBLE));
        DOUBLE *mdiag_entries = (DOUBLE *)calloc(ndiag_rowptr[rows_num], sizeof(DOUBLE));
        diag->Entries = diag_entries;
        ndiag->Entries = mdiag_entries;
        matrix->diag = diag;
        matrix->ndiag = ndiag;
    }
    else if (DataTypePetscEqual(matrix->type))
    {
#if defined(PETSC_USE)
        INT *dnnz = (INT *)calloc(rows_num, sizeof(INT));
        INT *onnz = (INT *)calloc(rows_num, sizeof(INT));
        for (idx_row = 0; idx_row < rows_num; idx_row++)
        {
            start = repeat_entnum_each_row_displs[idx_row];
            num = repeat_entnum_each_row[idx_row];
            current_col = -1, diag_entnum = 0, ndiag_entnum = 0;
            for (i = 0; i < num; i++)
            {
                if (current_col < repeat_kcol[start + i])
                {
                    current_col = repeat_kcol[start + i];
                    if (current_col < cols_start || current_col >= cols_end)
                    {
                        ndiag_entnum++;
                    }
                    else
                    {
                        diag_entnum++;
                    }
                }
            }
            dnnz[idx_row] = diag_entnum;
            onnz[idx_row] = ndiag_entnum;
        }
        MatMPIAIJSetPreallocation((Mat)(matrix->data_petsc), 0, dnnz, 0, onnz);
        MatSeqAIJSetPreallocation((Mat)(matrix->data_petsc), 0, dnnz);
        OpenPFEM_Free(dnnz);
        OpenPFEM_Free(onnz);
#else
        RaisePetscError("BuildMatrix");
#endif
    }

    OpenPFEM_Free(repeat_entnum_each_row);
    OpenPFEM_Free(repeat_entnum_each_row_displs);
    OpenPFEM_Free(repeat_kcol);

    assemble_info->row_owner = row_owner;
    assemble_info->row_owner_index = row_owner_index;
    assemble_info->col_global_index = col_globalindex;
    *Assembleinfo = assemble_info;
}

VECTOR *RHSInitial(DISCRETEFORM *DiscreteForm, DATATYPE type)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace;
    MESH *Rmesh = Rspace->Mesh;

    VECTOR *rhs = NULL;
    VectorCreate(&rhs, type);
    rhs->global_length = Rspace->GlobalNumDOF;
    rhs->local_length = Rspace->MyNumDOF;
    rhs->start = Rspace->GDOFstart;
    rhs->end = Rspace->GDOFend;
    MPI_Comm_dup(Rmesh->comm, &(rhs->comm));

    if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        Vec v;
        VecCreate(Rmesh->comm, &v);
        VecSetSizes(v, Rspace->MyNumDOF, PETSC_DECIDE);
        VecSetFromOptions(v);
        rhs->data_petsc = (void *)v;
#else
        RaisePetscError("SetRHS");
#endif
    }
    else if (DataTypeOpenPFEMEqual(type))
    {
        rhs->data = (DOUBLE *)calloc(Rspace->MyNumDOF, sizeof(DOUBLE));
    }
    else
    {
        RaiseError("SetRHS", "wrong datatype!");
    }

    return rhs;
}

MATRIX *MatrixInitial(DISCRETEFORM *DiscreteForm, DATATYPE type)
{
    /** 目前只实现了左右网格是相同的情况 */
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    assert(Rmesh->comm == Cmesh->comm);

    MATRIX *matrix = NULL;
    MatrixCreate(&matrix, type);

    if (DataTypePetscEqual(type))
    {
        matrix->global_nrows = Rspace->GlobalNumDOF;
        matrix->global_ncols = Cspace->GlobalNumDOF;
        matrix->local_nrows = Rspace->MyNumDOF;
        matrix->local_ncols = Cspace->MyNumDOF;
        matrix->rows_start = Rspace->GDOFstart;
        matrix->rows_end = Rspace->GDOFend;
        matrix->cols_start = Cspace->GDOFstart;
        matrix->cols_end = Cspace->GDOFend;
        MPI_Comm_dup(Rmesh->comm, &(matrix->comm));
#if defined(PETSC_USE)
        Mat A;
        MatCreate(Rmesh->comm, &A);
        MatSetSizes(A, Rspace->MyNumDOF, Cspace->MyNumDOF, Rspace->GlobalNumDOF, Cspace->GlobalNumDOF);
        MatSetFromOptions(A);
        matrix->data_petsc = (void *)A;
#else
        RaisePetscError("SetMatrix");
#endif
    }
    else if (DataTypeOpenPFEMEqual(type))
    {
        matrix->global_nrows = Rspace->GlobalNumDOF;
        matrix->global_ncols = Cspace->GlobalNumDOF;
        matrix->local_nrows = Rspace->MyNumDOF;
        matrix->local_ncols = Cspace->MyNumDOF;
        matrix->rows_start = Rspace->GDOFstart;
        matrix->rows_end = Rspace->GDOFend;
        matrix->cols_start = Cspace->GDOFstart;
        matrix->cols_end = Cspace->GDOFend;
        MPI_Comm_dup(Rmesh->comm, &(matrix->comm));
    }
    else
    {
        RaiseError("SetMatrix", "wrong datatype!");
    }

    return matrix;
}

static MATRIX *SetProlongMatrix(FEMSPACE *rowspace, FEMSPACE *colspace, DATATYPE type)
{
    MESH *rowmesh = rowspace->Mesh, *colmesh = colspace->Mesh;
    MATRIX *matrix = NULL;
    MatrixCreate(&matrix, type);

    matrix->global_nrows = rowspace->GlobalNumDOF;
    matrix->global_ncols = colspace->GlobalNumDOF;
    matrix->local_nrows = rowspace->MyNumDOF;
    matrix->local_ncols = colspace->MyNumDOF;
    matrix->rows_start = rowspace->GDOFstart;
    matrix->rows_end = rowspace->GDOFend;
    matrix->cols_start = colspace->GDOFstart;
    matrix->cols_end = colspace->GDOFend;
    MPI_Comm_dup(rowmesh->comm, &(matrix->comm));
    if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        Mat A;
        MatCreate(rowmesh->comm, &A);
        MatSetSizes(A, rowspace->MyNumDOF, colspace->MyNumDOF, rowspace->GlobalNumDOF, colspace->GlobalNumDOF);
        MatSetFromOptions(A);
        matrix->data_petsc = (void *)A;
#else
        RaisePetscError("SetProlongMatrix");
#endif
    }
    else if (DataTypeOpenPFEMEqual(type))
    {
    }
    else
    {
        RaiseError("SetProlongMatrix", "wrong datatype!");
    }

    return matrix;
}

static void BuildProlongMatrix(MATRIX *matrix, ASSEMBLE_INFO **Assembleinfo, FEMSPACE *rowspace, FEMSPACE *colspace)
{
    BASE *rowbase = rowspace->Base, *colbase = colspace->Base;
    INT num_row_base = rowbase->NumBase, num_col_base = colbase->NumBase;
    INT worlddim = rowspace->Mesh->worlddim;

    ASSEMBLE_INFO *assemble_info = (ASSEMBLE_INFO *)malloc(sizeof(ASSEMBLE_INFO));

    CSRMAT *diag = NULL, *ndiag = NULL;
    CSRMatCreation(&diag);
    CSRMatCreation(&ndiag);
    diag->NumRows = matrix->local_nrows;
    diag->NumColumns = matrix->local_ncols;
    ndiag->NumRows = matrix->local_nrows;
    ndiag->NumColumns = matrix->global_ncols;

    INT nrows = matrix->local_nrows;
    INT totalentnum = nrows * num_col_base;
    INT *kcol_tmp = (INT *)malloc(totalentnum * sizeof(INT));
    INT num_elems;
    switch (worlddim)
    {
    case 1:
        num_elems = rowspace->Mesh->num_line;
        break;
    case 2:
        num_elems = rowspace->Mesh->num_face;
        break;
    case 3:
        num_elems = rowspace->Mesh->num_volu;
        break;
    }

    INT *rowspace_localindex = rowspace->LocalIndex, *rowspace_beginindex = rowspace->BeginIndex;
    INT *colspace_localindex = colspace->LocalIndex, *colspace_beginindex = colspace->BeginIndex;
    INT idx_elem, i, j, rowind, rowindg, relateind, start, end;
    INT level_id_row = rowspace->Mesh->level_id;
    INT level_id_col = colspace->Mesh->level_id;
    // INT free_relates_ind = 0; // 表示relates是不是需要另外free
    INT *relates = NULL;
    // 两个网格是相同的
    if ((level_id_row - level_id_col) == 0)
    {
        relates = NULL;
    }
    // 两个网格之间相差一层
    else if ((level_id_row - level_id_col) == 1)
    {
        relates = rowspace->Mesh->Fathers;
    }
    // 粗网格是ancestor
    else if (level_id_col == 0)
    {
        relates = rowspace->Mesh->Ancestors;
    }
    else
    {
        RaiseError("BuildProlongMatrix", "wrong femspace!");
    }
    INT *row_globalindex = rowspace->GlobalIndex, *col_globalindex = colspace->GlobalIndex;
    INT rows_start = matrix->rows_start, rows_end = matrix->rows_end;
    assemble_info->col_global_index = colspace->GlobalIndex;

    INT *rows_for_elem = NULL, *cols_for_relate;
    INT *row_owner = (INT *)calloc(rowspace->NumDOF, sizeof(INT));
    INT *row_owner_index = (INT *)malloc(rowspace->NumDOF * sizeof(INT));
    for (idx_elem = 0; idx_elem < num_elems; idx_elem++)
    {
        rows_for_elem = rowspace_localindex + rowspace_beginindex[idx_elem];
        if (relates == NULL)
        {
            relateind = idx_elem;
        }
        else
        {
            relateind = relates[idx_elem];
        }
        cols_for_relate = colspace_localindex + colspace_beginindex[relateind];
        for (i = 0; i < num_row_base; i++)
        {
            rowindg = row_globalindex[rows_for_elem[i]];
            if (rowindg >= rows_start && rowindg < rows_end)
            {
                rowind = rowindg - rows_start;
                row_owner[rows_for_elem[i]] = -1;
                row_owner_index[rows_for_elem[i]] = rowind;
                start = rowind * num_col_base;
                for (j = 0; j < num_col_base; j++)
                {
                    kcol_tmp[start + j] = col_globalindex[cols_for_relate[j]];
                }
            }
        }
    }
    // if (relates != NULL)
    // {
    //     OpenPFEM_Free(relates);
    // }
    INT idx_row;
    for (idx_row = 0; idx_row < nrows; idx_row++)
    {
        start = idx_row * num_col_base;
        end = (idx_row + 1) * num_col_base;
        QuickSort_Int(kcol_tmp, start, end - 1);
    }

    INT cols_start = matrix->cols_start, cols_end = matrix->cols_end;
    INT ncols = matrix->local_ncols;
    INT current_col = 0, diag_entnum = 0, ndiag_entnum = 0;
    if (DataTypeOpenPFEMEqual(matrix->type))
    {
        INT *diag_rowptr = (INT *)malloc((nrows + 1) * sizeof(INT));
        INT *ndiag_rowptr = (INT *)malloc((nrows + 1) * sizeof(INT));
        diag_rowptr[0] = 0, ndiag_rowptr[0] = 0;
        for (idx_row = 0; idx_row < nrows; idx_row++)
        {
            start = idx_row * num_col_base;
            diag_entnum = 0, ndiag_entnum = 0;
            for (i = 0; i < num_col_base; i++)
            {
                current_col = kcol_tmp[start + i];
                if (current_col < cols_start || current_col >= cols_end)
                {
                    ndiag_entnum++;
                }
                else
                {
                    diag_entnum++;
                }
            }
            diag_rowptr[idx_row + 1] = diag_rowptr[idx_row] + diag_entnum;
            ndiag_rowptr[idx_row + 1] = ndiag_rowptr[idx_row] + ndiag_entnum;
        }
        diag->RowPtr = diag_rowptr;
        ndiag->RowPtr = ndiag_rowptr;
        diag->NumEntries = diag_rowptr[nrows];
        ndiag->NumEntries = ndiag_rowptr[nrows];

        INT *diag_kcol = (INT *)malloc(diag_rowptr[nrows] * sizeof(INT));
        INT *ndiag_kcol = (INT *)malloc(ndiag_rowptr[nrows] * sizeof(INT));
        diag_entnum = 0, ndiag_entnum = 0;
        for (idx_row = 0; idx_row < nrows; idx_row++)
        {
            start = idx_row * num_col_base;
            for (i = 0; i < num_col_base; i++)
            {
                current_col = kcol_tmp[start + i];
                if (current_col < cols_start || current_col >= cols_end)
                {
                    ndiag_kcol[ndiag_entnum] = current_col;
                    ndiag_entnum++;
                }
                else
                {
                    diag_kcol[diag_entnum] = current_col - cols_start;
                    diag_entnum++;
                }
            }
        }
        diag->KCol = diag_kcol;
        ndiag->KCol = ndiag_kcol;
        DOUBLE *diag_entries = (DOUBLE *)calloc(diag_rowptr[nrows], sizeof(DOUBLE));
        DOUBLE *mdiag_entries = (DOUBLE *)calloc(ndiag_rowptr[nrows], sizeof(DOUBLE));
        diag->Entries = diag_entries;
        ndiag->Entries = mdiag_entries;
        OpenPFEM_Free(kcol_tmp);
    }
    else if (DataTypePetscEqual(matrix->type))
    {
#if defined(PETSC_USE)
        INT *dnnz = (INT *)calloc(nrows, sizeof(INT));
        INT *onnz = (INT *)calloc(nrows, sizeof(INT));
        for (idx_row = 0; idx_row < nrows; idx_row++)
        {
            start = idx_row * num_col_base;
            current_col = -1, diag_entnum = 0, ndiag_entnum = 0;
            for (i = 0; i < num_col_base; i++)
            {
                current_col = kcol_tmp[start + i];
                if (current_col < cols_start || current_col >= cols_end)
                {
                    ndiag_entnum++;
                }
                else
                {
                    diag_entnum++;
                }
            }
            dnnz[idx_row] = diag_entnum;
            onnz[idx_row] = ndiag_entnum;
        }
        MatMPIAIJSetPreallocation((Mat)(matrix->data_petsc), 0, dnnz, 0, onnz);
        MatSeqAIJSetPreallocation((Mat)(matrix->data_petsc), 0, dnnz);
        OpenPFEM_Free(dnnz);
        OpenPFEM_Free(onnz);
        assemble_info->send_row_num = kcol_tmp;
#else
        RaisePetscError("BuildMatrix");
#endif
    }

    matrix->diag = diag;
    matrix->ndiag = ndiag;
    assemble_info->submat_col_diag = (bool *)malloc(num_col_base * sizeof(bool));
    assemble_info->submat_col_index = (INT *)malloc(num_col_base * sizeof(INT));
    assemble_info->row_owner = row_owner;
    assemble_info->row_owner_index = row_owner_index;
    assemble_info->send_total_ent_num = num_col_base;
    *Assembleinfo = assemble_info;
}

void ProlongMatrixAssemble(MATRIX **Matrix, FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, DATATYPE type)
{
    MATRIX *matrix = SetProlongMatrix(FinerSpace, CoarseSpace, type);
    ASSEMBLE_INFO *assembleinfo = NULL;
    BuildProlongMatrix(matrix, &assembleinfo, FinerSpace, CoarseSpace);

    MESH *CoarseMesh = CoarseSpace->Mesh, *FinerMesh = FinerSpace->Mesh;
    BASE *CoarseBase = CoarseSpace->Base, *FinerBase = FinerSpace->Base;
    INT NumCoarseBase = CoarseBase->NumBase, NumFinerBase = FinerBase->NumBase;
    INT *CoarseBeginIndex = CoarseSpace->BeginIndex, *FinerBeginIndex = FinerSpace->BeginIndex;
    INT *CoarseLocalIndex = CoarseSpace->LocalIndex, *FinerLocalIndex = FinerSpace->LocalIndex;
    INT *colgindex = CoarseSpace->GlobalIndex;
    ELEMENT *CoarseElem = ElementInitial(CoarseMesh);
    ELEMENT *FinerElem = ElementInitial(FinerMesh);
    ELEMENT *RelativeElem = ElementInitial(FinerMesh);
    DOUBLE *LocalProlong = (DOUBLE *)malloc(NumCoarseBase * NumFinerBase * sizeof(DOUBLE));
    INT worlddim = FinerMesh->worlddim;

    // 增加利用有限元空间判断单元类型
    SetElemType(CoarseElem, CoarseSpace);
    SetElemType(FinerElem, FinerSpace);

    INT ind, num_elems, relate;
    switch (worlddim)
    {
    case 1:
        num_elems = FinerMesh->num_line;
        break;
    case 2:
        num_elems = FinerMesh->num_face;
        break;
    case 3:
        num_elems = FinerMesh->num_volu;
        break;
    }

    INT level_id_finer = FinerMesh->level_id;
    INT level_id_coarse = CoarseMesh->level_id;
    // INT free_relates_ind = 0; // 表示relates是不是需要另外free
    INT *relates = NULL;
    // 两个网格是相同的
    if ((level_id_finer - level_id_coarse) == 0)
    {
        relates = NULL;
    }
    // 两个网格之间相差一层
    else if ((level_id_finer - level_id_coarse) == 1)
    {
        relates = FinerMesh->Fathers;
    }
    // 粗网格是ancestor
    else if (level_id_coarse == 0)
    {
        relates = FinerMesh->Ancestors;
    }
    else
    {
        RaiseError("ProlongMatrixAssemble", "wrong femspace!");
    }

    if (CoarseElem->is_curl == 1 || CoarseElem->is_div == 1)
    {
        ComputeInvJacobiOfAllElem(CoarseMesh, 1);
    }
    else
    {
        ComputeInvJacobiOfAllElem(CoarseMesh, 0);
    }
    INT ii, jj;
    for (ind = 0; ind < num_elems; ind++)
    {
        // 建立当前的有限单元
        ElementBuild(FinerMesh, ind, FinerElem);
        if (relates == NULL)
        {
            relate = ind;
        }
        else
        {
            relate = relates[ind];
        }
        ElementBuild(CoarseMesh, relate, CoarseElem);
        // 计算CoarseElem的Jacobian信息
        if (CoarseMesh->IsJacobiInfoAvailable == 0)
        {
            // 计算单元的体积和Jacobian矩阵的逆矩阵
            ComputeElementInvJacobian(CoarseElem);
        }
        else
        {
            CoarseElem->Volumn = CoarseMesh->VolumnOfAllElem[relate];
            for (ii = 0; ii < worlddim; ii++)
            {
                for (jj = 0; jj < worlddim; jj++)
                {
                    CoarseElem->Jacobian[ii][jj] = CoarseMesh->JacobiOfAllElem[(relate * worlddim + ii) * worlddim + jj];
                    CoarseElem->InvJacobian[ii][jj] = CoarseMesh->InvJacobiOfAllElem[(relate * worlddim + ii) * worlddim + jj];
                }
            }
        }
        // 产生细单元在粗单元上的相对单元
        ProduceRelativeElem(CoarseElem, FinerElem, RelativeElem);
        // 获得局部的扩展算子
        GetLocalProlong(CoarseElem, CoarseBase, FinerElem, FinerBase, RelativeElem, LocalProlong);

        // 如果是curl元 调整LocalProlong的排列顺序
        if (CoarseElem->is_curl == 1)
        {
            ReorderLocalProlongColForCurlElem(CoarseSpace, FinerSpace, relate, CoarseElem, LocalProlong);
            ReorderLocalProlongRowForCurlElem(CoarseSpace, FinerSpace, ind, FinerElem, LocalProlong);
        }
        if (CoarseElem->is_div == 1)
        {
            ReorderLocalProlongColForDivElem(CoarseSpace, FinerSpace, relate, CoarseElem, LocalProlong);
            ReorderLocalProlongRowForDivElem(CoarseSpace, FinerSpace, ind, FinerElem, LocalProlong);
        }

        // 把获得到局部的扩展算子设置到全局的扩张算子: 先行然后列
        ProlongMatrixSetSubMatrix(matrix, assembleinfo, LocalProlong, NumFinerBase, FinerLocalIndex + FinerBeginIndex[ind],
                                  NumCoarseBase, CoarseLocalIndex + CoarseBeginIndex[relate]);
    }
    if (DataTypeOpenPFEMEqual(type))
    {
        // 最后压缩+整理非对角部分
#if MATRIX_COMPRESSION
        MatrixCompress(matrix);
#endif
        MatrixComplete(matrix);
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        MatAssemblyBegin((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        OpenPFEM_Free(assembleinfo->send_row_num);
#else
        RaisePetscError("AssembleMatrix");
#endif
    }

    OpenPFEM_Free(LocalProlong);
    ElementDestroy(&CoarseElem);
    ElementDestroy(&FinerElem);
    ElementDestroy(&RelativeElem);
    OpenPFEM_Free(assembleinfo->submat_col_diag);
    OpenPFEM_Free(assembleinfo->submat_col_index);
    OpenPFEM_Free(assembleinfo->row_owner);
    OpenPFEM_Free(assembleinfo->row_owner_index);
    OpenPFEM_Free(assembleinfo);

    *Matrix = matrix;
}

void ProlongMatrixSetSubMatrix(MATRIX *A, ASSEMBLE_INFO *assembleinfo, DOUBLE *AA, INT n_row, INT *row, INT n_col, INT *col)
{
    DATATYPE type = A->type;
    if (DataTypeOpenPFEMEqual(type))
    {
        INT *col_global_index = assembleinfo->col_global_index;
        bool *submat_col_diag = assembleinfo->submat_col_diag;
        INT *submat_col_index = assembleinfo->submat_col_index;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        // (0) 检查每一列属于对角元还是非对角元 及其编号
        INT idx_row, idx_col, rowind, colind;
        INT col_start = A->cols_start, col_end = A->cols_end;
        for (idx_col = 0; idx_col < n_col; idx_col++)
        {
            colind = col_global_index[col[idx_col]];
            if (colind >= col_start && colind < col_end)
            {
                submat_col_diag[idx_col] = true;
                submat_col_index[idx_col] = colind;
            }
            else
            {
                submat_col_diag[idx_col] = false;
                submat_col_index[idx_col] = colind;
            }
        }
        // 开始组装
        INT *diag_rowptr = A->diag->RowPtr, *diag_kcol = A->diag->KCol;
        INT *ndiag_rowptr = A->ndiag->RowPtr, *ndiag_kcol = A->ndiag->KCol;
        DOUBLE *diag_entries = A->diag->Entries, *ndiag_entries = A->ndiag->Entries;
        INT diag_start, diag_end, ndiag_start, ndiag_end, position, i;
        DOUBLE alpha;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            // (1) 检查当前行是否属于本进程
            if (row_owner[row[idx_row]] == -1) // √
            {
                rowind = row_owner_index[row[idx_row]];
                diag_start = diag_rowptr[rowind];
                diag_end = diag_rowptr[rowind + 1];
                ndiag_start = ndiag_rowptr[rowind];
                ndiag_end = ndiag_rowptr[rowind + 1];
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    if (submat_col_diag[idx_col])
                    {
                        colind = submat_col_index[idx_col] - col_start;
                        for (i = diag_start; i < diag_end; i++)
                        {
                            if (diag_kcol[i] == colind)
                            {
                                diag_entries[i] = alpha;
                                break;
                            }
                        }
                    }
                    else
                    {
                        colind = submat_col_index[idx_col];
                        for (i = ndiag_start; i < ndiag_end; i++)
                        {
                            if (ndiag_kcol[i] == colind)
                            {
                                ndiag_entries[i] = alpha;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        INT *col_global_index = assembleinfo->col_global_index;
        INT *submat_col_index = assembleinfo->submat_col_index;
        INT *row_owner = assembleinfo->row_owner;
        INT *row_owner_index = assembleinfo->row_owner_index;
        INT *kcol = assembleinfo->send_row_num;
        INT num_col_base = assembleinfo->send_total_ent_num;
        // (0) 检查每一列属于对角元还是非对角元 及其编号
        INT idx_row, idx_col, rowind, colind;
        INT col_start = A->cols_start, col_end = A->cols_end;
        for (idx_col = 0; idx_col < n_col; idx_col++)
        {
            colind = col_global_index[col[idx_col]];
            submat_col_index[idx_col] = colind;
        }
        // 开始组装
        Mat mat = (Mat)(A->data_petsc);
        INT row_start = A->rows_start;
        INT start, end, position, i;
        DOUBLE alpha;
        for (idx_row = 0; idx_row < n_row; idx_row++)
        {
            // (1) 检查当前行是否属于本进程
            if (row_owner[row[idx_row]] == -1) // √
            {
                rowind = row_owner_index[row[idx_row]];
                for (idx_col = 0; idx_col < n_col; idx_col++)
                {
                    alpha = AA[idx_row * n_col + idx_col];
                    colind = submat_col_index[idx_col];
                    start = rowind * num_col_base;
                    end = start + num_col_base;
                    for (i = start; i < end; i++)
                    {
                        if (kcol[i] == colind)
                        {
                            MatSetValue(mat, rowind + row_start, colind, alpha, INSERT_VALUES);
                            break;
                        }
                    }
                }
            }
        }
#else
        RaisePetscError("SetSubProlongMatrix");
#endif
    }
}

void GetLocalProlong(ELEMENT *CoarseElem, BASE *CoarseBase, ELEMENT *FinerElem,
                     BASE *FinerBase, ELEMENT *RelativeElem, DOUBLE *LocalProlong)
{
    // Get Local Matrix
    INT i, CoarseNumBase = CoarseBase->NumBase, FinerNumBase = FinerBase->NumBase;
    NODALFUNCTION *NodalFun = FinerBase->NodalFun;
    FUNCTIONVEC *CoarseInterFun = CoarseBase->InterFun;
    INT CoarseValueDim = CoarseBase->ValueDim, FinerValueDim = FinerBase->ValueDim;
    // printf("relative coord 0[%2.10f %2.10f %2.10f]\n",RelativeElem->Vert_X[0],RelativeElem->Vert_Y[0],RelativeElem->Vert_Z[0]);
    // printf("relative coord 1[%2.10f %2.10f %2.10f]\n",RelativeElem->Vert_X[1],RelativeElem->Vert_Y[1],RelativeElem->Vert_Z[1]);
    // printf("relative coord 2[%2.10f %2.10f %2.10f]\n",RelativeElem->Vert_X[2],RelativeElem->Vert_Y[2],RelativeElem->Vert_Z[2]);
    // printf("relative coord 3[%2.10f %2.10f %2.10f]\n",RelativeElem->Vert_X[3],RelativeElem->Vert_Y[3],RelativeElem->Vert_Z[3]);
    if (((CoarseBase->MapType == Piola) && (FinerBase->MapType == Piola)) || ((CoarseBase->MapType == Div) && (FinerBase->MapType == Div)))
    {
        // printf("come to here %d!!!!\n", FinerBase->PolynomialDegree);
        // if (FinerBase->PolynomialDegree > 2){
        //     ComputeElementJacobian(RelativeElem);
        //     printf("come to here\n");}
        NodalFun(RelativeElem, CoarseInterFun, CoarseNumBase * CoarseValueDim, LocalProlong);
    }
    // coarse base 里面的每一个基函数在finer 单元上的插值作为Prolong的元素
    if ((CoarseBase->MapType == Affine) && (FinerBase->MapType == Affine))
    {
        // printf("come to here!!\n");
        // 产生细网格单元在粗网格单元上的相对单元数据
        // ProduceRelativeElem(CoarseElem, FinerElem, RelativeElem);
        // NodalFun(RelativeElem, CoarseInterFun, CoarseNumBase * CoarseValueDim, LocalProlong);
        // 如果问题的ValueDim>1，需要对取出的数组进行位置调整
        if (CoarseValueDim > 1)
        {
            // InterValues用来存储从CoarseInterFun中取出的值
            DOUBLE *InterValues = malloc(CoarseNumBase * FinerNumBase * sizeof(DOUBLE));
            INT FinernumBasebyDim = FinerNumBase / FinerValueDim;
            INT CoarsenumBasebyDim = CoarseNumBase / CoarseValueDim;
            INT ind_coarsebasebydim, ind_finerbasebydim, ind_finerdim, ind_coarsedim, start;
            NodalFun(RelativeElem, CoarseInterFun, CoarseNumBase * CoarseValueDim, InterValues); // 长度为函数CoarseInterFun的维数
            // 调整InterValues顺序赋值给LocalProlong矩阵
            // 该层位置和变量基函数的自由度排布相关 顺序不变动
            for (ind_finerbasebydim = 0; ind_finerbasebydim < FinernumBasebyDim; ind_finerbasebydim++)
            {
                start = ind_finerbasebydim * CoarseNumBase * CoarseValueDim;
                // 需要交换这两层的相互顺序 按照文档中图示进行交换
                for (ind_coarsebasebydim = 0; ind_coarsebasebydim < CoarsenumBasebyDim; ind_coarsebasebydim++)
                {
                    for (ind_finerdim = 0; ind_finerdim < FinerValueDim; ind_finerdim++)
                    {
                        // 该层位置和内层ValueDim相关 顺序不变动
                        for (ind_coarsedim = 0; ind_coarsedim < CoarseValueDim; ind_coarsedim++)
                        {
                            LocalProlong[start + (ind_finerdim * CoarsenumBasebyDim + ind_coarsebasebydim) * FinerValueDim + ind_coarsedim] = InterValues[start + (ind_coarsebasebydim * FinerValueDim + ind_finerdim) * CoarseValueDim + ind_coarsedim];
                        }
                    }
                }
            }
            free(InterValues);
        }
        else
        {
            NodalFun(RelativeElem, CoarseInterFun, CoarseNumBase * CoarseValueDim, LocalProlong);
        }
        // 输出LocalProlong
        // INT i, j;
        // for (i = 0; i < CoarseNumBase; i++)
        // {
        //     for (j = 0; j < CoarseNumBase; j++)
        //     {
        //         OpenPFEM_Print("LocalProlong[%d][%d] = %2.10f\n", i, j, LocalProlong[i * CoarseNumBase + j]);
        //     }
        // }
    } // end if
}

// 产生相对单元信息（细单元在大单元中的局部坐标, 大单元映射成一个标准单元, 然后考察此时细单元在大单元中的局部坐标）
// 对于是Lagrange型的有限元是没有问题的
void ProduceRelativeElem(ELEMENT *CoarseElem, ELEMENT *FinerElem, ELEMENT *RelativeElem)
{
    INT worlddim = CoarseElem->worlddim;
    int i, NumVerts = worlddim + 1;
    // 基于粗网格单元来计算仿射变换及其相应的Jacobian矩阵的逆
    // ComputeElementInvJacobian(CoarseElem);
    double dx, dy, dz;
    switch (worlddim)
    {
    case 1:
        for (i = 0; i < NumVerts; i++)
        {
            dx = FinerElem->Vert_X[i] - CoarseElem->Vert_X[0];
            RelativeElem->Vert_X[i] = CoarseElem->InvJacobian[0][0] * dx;
        }
        break;
    case 2:
        for (i = 0; i < NumVerts; i++)
        {
            dx = FinerElem->Vert_X[i] - CoarseElem->Vert_X[0];
            dy = FinerElem->Vert_Y[i] - CoarseElem->Vert_Y[0];
            RelativeElem->Vert_X[i] = CoarseElem->InvJacobian[0][0] * dx + CoarseElem->InvJacobian[0][1] * dy;
            RelativeElem->Vert_Y[i] = CoarseElem->InvJacobian[1][0] * dx + CoarseElem->InvJacobian[1][1] * dy;
        }
        break;
    case 3:
        for (i = 0; i < NumVerts; i++)
        {
            dx = FinerElem->Vert_X[i] - CoarseElem->Vert_X[0];
            dy = FinerElem->Vert_Y[i] - CoarseElem->Vert_Y[0];
            dz = FinerElem->Vert_Z[i] - CoarseElem->Vert_Z[0];
            RelativeElem->Vert_X[i] = CoarseElem->InvJacobian[0][0] * dx + CoarseElem->InvJacobian[0][1] * dy + CoarseElem->InvJacobian[0][2] * dz;
            RelativeElem->Vert_Y[i] = CoarseElem->InvJacobian[1][0] * dx + CoarseElem->InvJacobian[1][1] * dy + CoarseElem->InvJacobian[1][2] * dz;
            RelativeElem->Vert_Z[i] = CoarseElem->InvJacobian[2][0] * dx + CoarseElem->InvJacobian[2][1] * dy + CoarseElem->InvJacobian[2][2] * dz;
        }
        break;
    }
}

static MATRIX *SetRestrictMatrix(FEMSPACE *rowspace, FEMSPACE *colspace, DATATYPE type)
{
    MESH *rowmesh = rowspace->Mesh, *colmesh = colspace->Mesh;
    MATRIX *matrix = NULL;
    MatrixCreate(&matrix, type);

    matrix->global_nrows = rowspace->GlobalNumDOF;
    matrix->global_ncols = colspace->GlobalNumDOF;
    matrix->local_nrows = rowspace->MyNumDOF;
    matrix->local_ncols = colspace->MyNumDOF;
    matrix->rows_start = rowspace->GDOFstart;
    matrix->rows_end = rowspace->GDOFend;
    matrix->cols_start = colspace->GDOFstart;
    matrix->cols_end = colspace->GDOFend;
    MPI_Comm_dup(rowmesh->comm, &(matrix->comm));
    if (DataTypePetscEqual(type))
    {
        MPI_Comm_dup(rowmesh->comm, &(matrix->comm));
#if defined(PETSC_USE)
        Mat A;
        MatCreate(rowmesh->comm, &A);
        MatSetSizes(A, rowspace->MyNumDOF, colspace->MyNumDOF, rowspace->GlobalNumDOF, colspace->GlobalNumDOF);
        MatSetFromOptions(A);
        matrix->data_petsc = (void *)A;
#else
        RaisePetscError("SetProlongMatrix");
#endif
    }
    else if (DataTypeOpenPFEMEqual(type))
    {
    }
    else
    {
        RaiseError("SetRestrictMatrix", "wrong datatype!");
    }

    return matrix;
}

void RestrictMatrixAssemble(MATRIX **Matrix, FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, DATATYPE type)
{
    MATRIX *matrix = SetRestrictMatrix(CoarseSpace, FinerSpace, type);
    ASSEMBLE_INFO *assembleinfo = NULL;
    // BuildRestrictMatrix(matrix, &assembleinfo, CoarseSpace, FinerSpace);
    RaiseError("AssembleRestrictMatrix", "Not implemented yet!");
    *Matrix = matrix;
}

void ProlongDeleteDirichletBoundary(MATRIX *prolong, FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace)
{
    if (!(DataTypeOpenPFEMEqual(prolong->type)))
        RaiseError("ProlongDeleteDirichletBoundary", "the type of prolong must be OpenPFEM.");
    if (CoarseSpace->delete_dirichletbd == false || FinerSpace->delete_dirichletbd == false)
        RaiseError("ProlongDeleteDirichletBoundary", "femspace -> delete_dirichletbd should both be true!");
    INT rowbd_num = FinerSpace->dirichletbdnum, colbd_num = CoarseSpace->dirichletbdnum;
    INT *rowbd = FinerSpace->dirichletbdindex, *colbd = CoarseSpace->dirichletbdindex;
    bool *ifrow_d = (bool *)calloc(prolong->local_nrows, sizeof(bool));
    bool *ifcol_d = (bool *)calloc(prolong->local_ncols, sizeof(bool));
    prolong->delete_dirichletbd = true;

    INT i, ind = 0;
    INT new_rownum = prolong->local_nrows - rowbd_num;
    INT new_colnum = prolong->local_ncols - colbd_num;
    INT *new_colindex = (INT *)malloc(prolong->local_ncols * sizeof(INT));
    memset(new_colindex, -1, prolong->local_ncols * sizeof(INT));
    for (i = 0; i < rowbd_num; i++)
    {
        ifrow_d[rowbd[i]] = true;
    }
    for (i = 0; i < colbd_num; i++)
    {
        ifcol_d[colbd[i]] = true;
    }
    for (i = 0; i < prolong->local_ncols; i++)
    {
        if (ifcol_d[i])
            new_colindex[i] = -1;
        else
        {
            new_colindex[i] = ind;
            ind++;
        }
    }

    // (1) matrix property
    MPI_Comm comm = prolong->comm;
    prolong->local_nrows = new_rownum;
    prolong->local_ncols = new_colnum;
    MPI_Allreduce(&new_rownum, &(prolong->global_nrows), 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&new_colnum, &(prolong->global_ncols), 1, MPI_INT, MPI_SUM, comm);
    MPI_Scan(&new_rownum, &(prolong->rows_end), 1, MPI_INT, MPI_SUM, comm);
    MPI_Scan(&new_colnum, &(prolong->cols_end), 1, MPI_INT, MPI_SUM, comm);
    prolong->rows_start = prolong->rows_end - prolong->local_nrows;
    prolong->cols_start = prolong->cols_end - prolong->local_ncols;

    // dofs about ndiag
    INT sendnum = prolong->mult_sendtotalnum, recvnum = prolong->mult_recvtotalnum;
    INT *sendindex = prolong->mult_sendindex;
    INT *sendtmp = (INT *)malloc(sendnum * sizeof(INT));
    INT *recvtmp = (INT *)malloc(recvnum * sizeof(INT));
    for (i = 0; i < sendnum; i++)
    {
        if (ifcol_d[sendindex[i]])
            sendtmp[i] = -1;
        else
            sendtmp[i] = prolong->cols_start + new_colindex[sendindex[i]];
    }
    MPI_Neighbor_alltoallv(sendtmp, prolong->mult_sendnum, prolong->mult_sendnum_displs, MPI_INT,
                           recvtmp, prolong->mult_recvnum, prolong->mult_recvnum_displs, MPI_INT, prolong->mult_comm);
    OpenPFEM_Free(sendtmp);

    // (2) diag
    CSRMAT *csrmat = prolong->diag;
    INT *rowptr = csrmat->RowPtr, *kcol = csrmat->KCol;
    DOUBLE *entries = csrmat->Entries;
    INT *newrowptr = (INT *)malloc((new_rownum + 1) * sizeof(INT));
    newrowptr[0] = 0;
    INT rowind = 0, num, new_entnum = 0, j, start, end;
    for (i = 0; i < csrmat->NumRows; i++)
    {
        if (!(ifrow_d[i]))
        {
            start = rowptr[i];
            end = rowptr[i + 1];
            num = 0;
            for (j = start; j < end; j++)
            {
                if (!(ifcol_d[kcol[j]]))
                {
                    kcol[new_entnum] = new_colindex[kcol[j]];
                    entries[new_entnum] = entries[j];
                    num++;
                    new_entnum++;
                }
            }
            newrowptr[rowind + 1] = newrowptr[rowind] + num;
            rowind++;
        }
    }
    OpenPFEM_Free(csrmat->RowPtr);
    OpenPFEM_Free(new_colindex);
    csrmat->RowPtr = newrowptr;
    csrmat->KCol = realloc(csrmat->KCol, new_entnum * sizeof(INT));
    csrmat->Entries = realloc(csrmat->Entries, new_entnum * sizeof(DOUBLE));
    csrmat->NumRows = new_rownum;
    csrmat->NumEntries = new_entnum;

    // (3) ndiag
    csrmat = prolong->ndiag;
    rowptr = csrmat->RowPtr, kcol = csrmat->KCol, entries = csrmat->Entries;
    INT new_ndiagnum = 0;
    INT *new_ndiagindex = (INT *)malloc(recvnum * sizeof(INT));
    for (i = 0; i < recvnum; i++)
    {
        if (recvtmp[i] != -1)
        {
            new_ndiagindex[i] = new_ndiagnum;
            new_ndiagnum++;
        }
        else
            new_ndiagindex[i] = -1;
    }
    INT *newrowptr2 = (INT *)malloc((new_rownum + 1) * sizeof(INT));
    newrowptr2[0] = 0;
    rowind = 0, new_entnum = 0;
    for (i = 0; i < csrmat->NumRows; i++)
    {
        if (!(ifrow_d[i]))
        {
            start = rowptr[i];
            end = rowptr[i + 1];
            num = 0;
            for (j = start; j < end; j++)
            {
                if (recvtmp[kcol[j]] != -1)
                {
                    kcol[new_entnum] = new_ndiagindex[kcol[j]];
                    entries[new_entnum] = entries[j];
                    num++;
                    new_entnum++;
                }
            }
            newrowptr2[rowind + 1] = newrowptr2[rowind] + num;
            rowind++;
        }
    }
    csrmat->RowPtr = newrowptr2;
    csrmat->KCol = realloc(csrmat->KCol, new_entnum * sizeof(INT));
    csrmat->Entries = realloc(csrmat->Entries, new_entnum * sizeof(DOUBLE));
    csrmat->NumRows = new_rownum;
    csrmat->NumColumns = new_ndiagnum;
    csrmat->NumEntries = new_entnum;
    INT *new_ndiag_globalcols = (INT *)malloc(new_ndiagnum * sizeof(INT));
    j = 0;
    for (i = 0; i < recvnum; i++)
    {
        if (recvtmp[i] != -1)
        {
            new_ndiag_globalcols[j] = recvtmp[i];
            j++;
        }
    }
    OpenPFEM_Free(prolong->ndiag_globalcols);
    OpenPFEM_Free(recvtmp);
    prolong->ndiag_globalcols = new_ndiag_globalcols;

    // (4) multiplication info (if needed)
    bool multinfobuild = 1;
    if (new_colnum == csrmat->NumColumns)
    {
        multinfobuild = 0;
    }
    MPI_Allreduce(MPI_IN_PLACE, &multinfobuild, 1, MPI_C_BOOL, MPI_LOR, comm);
    if (!multinfobuild)
        goto end_of_ProlongDeleteDirichletBoundary;

    MPI_Comm_free(&((prolong)->mult_comm));
    MPI_Comm_free(&((prolong)->inverse_mult_comm));
    OpenPFEM_Free(prolong->mult_sendnum);
    OpenPFEM_Free(prolong->mult_recvnum);
    OpenPFEM_Free(prolong->mult_sendnum_displs);
    OpenPFEM_Free(prolong->mult_recvnum_displs);
    OpenPFEM_Free(prolong->mult_sendindex);
    OpenPFEM_Free(prolong->mult_sendtmp);
    OpenPFEM_Free(prolong->mult_recvtmp);

    // (4.1) 统计两两进程之间的通信情况
    INT globalsize, myrank;
    MPI_Comm_size(comm, &globalsize);
    MPI_Comm_rank(comm, &myrank);
    INT *col_start_global = (INT *)malloc((globalsize + 1) * sizeof(INT));
    INT col_start = prolong->cols_start;
    MPI_Allgather(&col_start, 1, MPI_INT, col_start_global, 1, MPI_INT, comm);
    col_start_global[globalsize] = prolong->global_ncols;
    bool *entries_from = (bool *)calloc(globalsize, sizeof(bool));
    bool *entries_to = (bool *)malloc(globalsize * sizeof(bool));
    INT idx_rank, current = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        while (new_ndiag_globalcols[current] >= col_start_global[idx_rank] &&
               new_ndiag_globalcols[current] < col_start_global[idx_rank + 1])
        {
            entries_from[idx_rank] = true;
            current++;
            if (current == new_colnum)
                goto finish_communication_check3;
        }
    }
finish_communication_check3:
    MPI_Alltoall(entries_from, 1, MPI_C_BOOL, entries_to, 1, MPI_C_BOOL, comm);
    MPI_Comm mult_comm = MPI_COMM_NULL;
    MPI_Comm inverse_mult_comm = MPI_COMM_NULL;
    INT incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
            incount++;
        if (entries_to[idx_rank])
            outcount++;
    }
    INT *inrank = (INT *)malloc(incount * sizeof(INT));
    INT *outrank = (INT *)malloc(outcount * sizeof(INT));
    incount = 0, outcount = 0;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            inrank[incount] = idx_rank;
            incount++;
        }
        if (entries_to[idx_rank])
        {
            outrank[outcount] = idx_rank;
            outcount++;
        }
    }
    MPI_Dist_graph_create_adjacent(comm, incount, inrank, MPI_UNWEIGHTED, outcount, outrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &mult_comm);
    MPI_Dist_graph_create_adjacent(comm, outcount, outrank, MPI_UNWEIGHTED, incount, inrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &inverse_mult_comm);
    prolong->mult_comm = mult_comm;
    prolong->inverse_mult_comm = inverse_mult_comm;
    OpenPFEM_Free(inrank);
    OpenPFEM_Free(outrank);

    // (4.2) 确认两两之间要发送哪些行
    INT *num_entries_from = (INT *)calloc(incount, sizeof(INT));
    INT *num_entries_to = (INT *)malloc(outcount * sizeof(INT));
    current = 0;
    INT current_rank = 0;
    MPI_Request numreq;
    for (idx_rank = 0; idx_rank < globalsize; idx_rank++)
    {
        if (entries_from[idx_rank])
        {
            while (new_ndiag_globalcols[current] >= col_start_global[idx_rank] &&
                   new_ndiag_globalcols[current] < col_start_global[idx_rank + 1])
            {
                num_entries_from[current_rank]++;
                current++;
                if (current == new_colnum)
                    goto finish_num_check3;
            }
            current_rank++;
        }
    }
finish_num_check3:
    MPI_Ineighbor_alltoall(num_entries_from, 1, MPI_INT, num_entries_to, 1, MPI_INT, inverse_mult_comm, &numreq);
    INT *which_entries_from = new_ndiag_globalcols;
    INT *num_entries_from_displs = (INT *)malloc(incount * sizeof(INT));
    num_entries_from_displs[0] = 0;
    for (i = 1; i < incount; i++)
    {
        num_entries_from_displs[i] = num_entries_from_displs[i - 1] + num_entries_from[i - 1];
    }
    MPI_Wait(&numreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&numreq);
    INT *num_entries_to_displs = (INT *)malloc(outcount * sizeof(INT));
    num_entries_to_displs[0] = 0;
    for (i = 1; i < outcount; i++)
    {
        num_entries_to_displs[i] = num_entries_to_displs[i - 1] + num_entries_to[i - 1];
    }
    INT final_num = num_entries_to_displs[outcount - 1] + num_entries_to[outcount - 1];
    INT *which_entries_to = (INT *)malloc(final_num * sizeof(INT));
    MPI_Neighbor_alltoallv(which_entries_from, num_entries_from, num_entries_from_displs, MPI_INT,
                           which_entries_to, num_entries_to, num_entries_to_displs, MPI_INT, inverse_mult_comm);
    for (i = 0; i < final_num; i++)
    {
        which_entries_to[i] -= col_start;
    }
    prolong->mult_sendindex = which_entries_to;
    prolong->mult_sendnum = num_entries_to;
    prolong->mult_recvnum = num_entries_from;
    prolong->mult_sendnum_displs = num_entries_to_displs;
    prolong->mult_recvnum_displs = num_entries_from_displs;
    OpenPFEM_Free(entries_from);
    OpenPFEM_Free(entries_to);
    OpenPFEM_Free(col_start_global);
    prolong->mult_recvtotalnum = new_colnum;
    prolong->mult_sendtotalnum = final_num;
    prolong->mult_sendtmp = (DOUBLE *)malloc(final_num * sizeof(DOUBLE));
    prolong->mult_recvtmp = (DOUBLE *)malloc(new_colnum * sizeof(DOUBLE));

end_of_ProlongDeleteDirichletBoundary:
    csrmat->NumColumns = new_colnum;
    OpenPFEM_Free(new_ndiagindex);
    OpenPFEM_Free(new_colindex);
    OpenPFEM_Free(ifrow_d);
    OpenPFEM_Free(ifcol_d);
}

void MatrixBeginAssemble(MATRIX **Matrix, VECTOR **Rhs, DISCRETEFORM *DiscreteForm, DATATYPE type)
{
    MATRIX *matrix = MatrixInitial(DiscreteForm, type);
    VECTOR *rhs = NULL;
    if (Rhs != NULL)
    {
        rhs = RHSInitial(DiscreteForm, type);
    }
    ASSEMBLE_INFO *assembleinfo = NULL;
    MatrixStructureBuild(matrix, &assembleinfo, DiscreteForm);
    matrix->assemble_info = (void *)assembleinfo;

    // (0) 准备
    /** 目前只实现了左右网格是相同的情况 */
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *SubEntries = malloc(RNumBase * CNumBase * sizeof(DOUBLE));
    DOUBLE *SubVec = NULL;
    assembleinfo->submat_col_diag = (bool *)malloc(CNumBase * sizeof(bool));
    assembleinfo->submat_col_index = (INT *)malloc(CNumBase * sizeof(INT));
    if (Rhs != NULL)
    {
        SubVec = malloc(RNumBase * sizeof(DOUBLE));
    }
    INT *Rlocalindex = Rspace->LocalIndex;
    INT *Rglobalindex = Rspace->GlobalIndex;
    INT *Rbeginindex = Rspace->BeginIndex;
    INT *Clocalindex = Cspace->LocalIndex;
    INT *Cglobalindex = Cspace->GlobalIndex;
    INT *Cbeginindex = Cspace->BeginIndex;
    ELEMENT *Elem = NULL;
    INT HasBoundary = 0;
    if (Rspace->BoundType)
        HasBoundary = 1;
    // (0.0) 看看是否含有辅助有限元函数AuxFEMFun, 并且建立相应的存储基函数和辅助有限元函数值的内存
    DOUBLE *AuxFEMValues = NULL;
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        AuxFEMValues = DiscreteForm->AuxFEMFunction->AuxFEMFunValues;
    }
    // (0.1)  计算本进程中要发送出去和收到的entry个数并开辟空间
    INT neignum = Rmesh->SharedInfo->NumNeighborRanks;
    INT *neigranks = Rmesh->SharedInfo->NeighborRanks;
    INT *send_ent_num = assembleinfo->send_col_num;
    INT *send_ent_num_displs = assembleinfo->send_col_num_displs;
    assembleinfo->send_total_ent_num = send_ent_num_displs[neignum - 1] + send_ent_num[neignum - 1];
    INT *recv_ent_num = assembleinfo->recv_col_num;
    INT *recv_ent_num_displs = assembleinfo->recv_col_num_displs;
    assembleinfo->recv_total_ent_num = recv_ent_num_displs[neignum - 1] + recv_ent_num[neignum - 1];
    INT idx_neig;
    INT *send_row_num = assembleinfo->send_row_num;
    INT *send_row_num_displs = assembleinfo->send_row_num_displs;
    INT *recv_row_num = assembleinfo->recv_row_num;
    INT *recv_row_num_displs = assembleinfo->recv_row_num_displs;
    if (Rhs != NULL)
    {
        assembleinfo->if_rhs = true;
        // 如果有rhs需要增加和行数相同个数的数据
        for (idx_neig = 0; idx_neig < neignum; idx_neig++)
        {
            send_ent_num[idx_neig] += send_row_num[idx_neig];
            send_ent_num_displs[idx_neig] += send_row_num_displs[idx_neig];
            recv_ent_num[idx_neig] += recv_row_num[idx_neig];
            recv_ent_num_displs[idx_neig] += recv_row_num_displs[idx_neig];
        }
    }
    else
    {
        assembleinfo->if_rhs = false;
    }
    INT send_total_ent_num = send_ent_num_displs[neignum - 1] + send_ent_num[neignum - 1];
    assembleinfo->send_ent_each_row = (DOUBLE *)calloc(send_total_ent_num, sizeof(DOUBLE));
    INT recv_total_ent_num = recv_ent_num_displs[neignum - 1] + recv_ent_num[neignum - 1];
    assembleinfo->recv_ent_each_row = (DOUBLE *)malloc(recv_total_ent_num * sizeof(DOUBLE));
    assembleinfo->matrix = matrix;
    assembleinfo->vector = rhs;
    assembleinfo->discreteform = DiscreteForm;

    // (1) 边界条件处理
    LINE line;
    FACE face;
    VOLU volu;
    INT LocalNumElems;
    INT numlinevert, numfacevert, numvoluvert, numfaceline, numvoluline, numvoluface;
    INT numlocalbd, DirichletBDInd;
    INT *dof = &Rspace->Base->DOF[0];
    BOUNDARYTYPE *ElemBoundaryType = malloc(RNumBase * sizeof(BOUNDARYTYPE));
    memset(ElemBoundaryType, INNER, RNumBase * sizeof(BOUNDARYTYPE));
    DOUBLE *BoundaryVec = malloc(RNumBase * sizeof(DOUBLE));

    // lhc 23.01.28
    // 判断是否存在更细的网格 如果有 按照不同方式进行组装
    if (DiscreteForm->IsUseAuxFEMSpace == 0)
    {
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;

        case 2:
            LocalNumElems = Rmesh->num_face;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts;
                numfaceline = face.NumLines;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;

        case 3:
            LocalNumElems = Rmesh->num_volu;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts;
                numvoluline = volu.NumLines;
                numvoluface = volu.NumFaces;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // (2) 单元遍历
        INT idx_elem;
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.1) 检查本单元上的边界信息
            DirichletBDInd = 0;
            if (HasBoundary)
            {
                // 得到本单元上测试空间自由度的边界类型
                //  void GetElementBoundaryInformation(FEMSPACE *femspace, INT ind, INT numlocalbd, BOUNDARYTYPE *ElemBoundaryType, INT *DirichletBDInd)
                //  ElemBoundaryType: 存储了该单元上每个自由度的边界条件
                GetElementBoundaryInformation(Rspace, idx_elem, ElemBoundaryType, &DirichletBDInd);
            }
            // (2.2) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem, ElemBoundaryType, SubEntries, SubVec);
            // (2.3) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_elem],
                               CNumBase, Clocalindex + Cbeginindex[idx_elem]);
            // (2.4) 处理右端项
            if (Rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_elem]);
            }
            // (2.5) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDInd == 1)
            {
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem, numlocalbd, BoundaryVec, ElemBoundaryType);
            }
        }
    }
    else
    {
        // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        // ComputeInvJacobiOfAllElem(Rmesh);
        // 细网格
        MESH *MeshInAssemble = DiscreteForm->LeftSpaceInAssemble->Mesh;
        // 细网格对应的信息
        INT LocalNumElems_finer;
        ELEMENT *Elem_finer = NULL;
        // 获取网格的单元信息和自由度边界信息
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            LocalNumElems_finer = MeshInAssemble->num_line;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;

        case 2:
            LocalNumElems = Rmesh->num_face;
            LocalNumElems_finer = MeshInAssemble->num_face;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts;
                numfaceline = face.NumLines;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;

        case 3:
            LocalNumElems = Rmesh->num_volu;
            LocalNumElems_finer = MeshInAssemble->num_volu;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts;
                numvoluline = volu.NumLines;
                numvoluface = volu.NumFaces;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // (2.0)首先提前计算粗网格Rmesh中每个单元的边界信息ElemBoundaryType和DirichletBDInd 并统一存储在BoundaryTypeOfAllElems和DirichletBDIndOfAllElems中
        BOUNDARYTYPE *BoundaryTypeOfAllElems = malloc(LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        memset(BoundaryTypeOfAllElems, INNER, LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        INT *DirichletBDIndOfAllElems = malloc(LocalNumElems * sizeof(INT));
        memset(DirichletBDIndOfAllElems, 0, LocalNumElems * sizeof(INT));
        // 增加利用变分形式判断单元类型
        SetElemType(Elem, Rspace);
        // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        if (Elem->is_curl == 1 || Elem->is_div == 1)
        {
            ComputeInvJacobiOfAllElem(Rmesh, 1);
        }
        else
        {
            ComputeInvJacobiOfAllElem(Rmesh, 0);
        }
        // 对粗网格Rmesh进行单元循环 填充上述信息
        INT idx_elem, idx_related;
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.0.1) 检查本单元上的边界信息
            if (HasBoundary)
            {
                GetElementBoundaryInformation(Rspace, idx_elem, BoundaryTypeOfAllElems + idx_elem * RNumBase, &(DirichletBDIndOfAllElems[idx_elem]));
            }
        }
        // (2.1)对细网格进行单元循环 完成刚度矩阵的组装
        for (idx_elem = 0; idx_elem < LocalNumElems_finer; idx_elem++)
        {
            // (2.1.0) 建立当前的有限单元
            ElementBuild(MeshInAssemble, idx_elem, Elem_finer);
            // 找到对应粗网格中的大单元的编号
            // idx_related = MeshInAssemble->Ancestors[idx_elem];
            if (DiscreteForm->Relates == NULL)
            {
                idx_related = idx_elem;
            }
            else
            {
                idx_related = DiscreteForm->Relates[idx_elem];
            }
            // (2.1.1) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem_finer, BoundaryTypeOfAllElems + idx_related * RNumBase, SubEntries, SubVec);
            // (2.1.2) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_related],
                               CNumBase, Clocalindex + Cbeginindex[idx_related]);
            // (2.1.3) 处理右端项
            if (Rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_related]);
            }
        }
        // (2.2)对粗网格单元进行单元循环 进行边界自由度对角元素的处理
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.2.1) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDIndOfAllElems[idx_elem] == 1)
            {
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem, numlocalbd, BoundaryVec, BoundaryTypeOfAllElems + idx_elem * RNumBase);
            }
        }
    }

    // (3.1) 开始通信
    MPI_Comm comm = Rmesh->SharedInfo->neighborcomm;
    MPI_Request entreq;
    DOUBLE *send_ent_each_row = assembleinfo->send_ent_each_row;
    DOUBLE *recv_ent_each_row = assembleinfo->recv_ent_each_row;
    MPI_Ineighbor_alltoallv(send_ent_each_row, send_ent_num, send_ent_num_displs, MPI_DOUBLE,
                            recv_ent_each_row, recv_ent_num, recv_ent_num_displs, MPI_DOUBLE, comm, &entreq);
    MPI_Wait(&entreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&entreq);

    // (3.2) 把收到的信息组装上去
    INT rows_start = matrix->rows_start, rows_end = matrix->rows_end;
    INT *row_globalindex = Rspace->GlobalIndex;
    INT *recv_row_index = assembleinfo->recv_row_index;
    INT *recv_entnum_each_row = assembleinfo->recv_colnum_each_row;
    INT *recv_colindex_each_row = assembleinfo->recv_colindex_each_row;
    INT start1 = 0, start2 = 0, position, entnum, idx_row, rowindex_local;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        position = recv_row_num_displs[idx_neig];
        start2 = recv_ent_num_displs[idx_neig];
        // 矩阵部分
        for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
        {
            rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
            entnum = recv_entnum_each_row[position];
            MatrixAddRowMatrix(matrix, entnum, rowindex_local, &(recv_colindex_each_row[start1]), &(recv_ent_each_row[start2]));
            position++;
            start1 += entnum;
            start2 += entnum;
        }
        // 向量部分
        if (Rhs != NULL)
        {
            if (DataTypeOpenPFEMEqual(type))
            {
                position = recv_row_num_displs[idx_neig];
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
                    rhs->data[rowindex_local] += recv_ent_each_row[start2];
                    position++;
                    start2++;
                }
            }
            else if (DataTypePetscEqual(type))
            {
#if defined(PETSC_USE)
                position = recv_row_num_displs[idx_neig];
                Vec vec = (Vec)(rhs->data_petsc);
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]];
                    VecSetValue(vec, rowindex_local, recv_ent_each_row[start2], ADD_VALUES);
                    position++;
                    start2++;
                }
#else
                RaisePetscError("AssembleMatrix");
#endif
            }
        }
    }

    if (DataTypeOpenPFEMEqual(type))
    {
        // 最后压缩+整理非对角部分
#if MATRIX_COMPRESSION
        MatrixCompress(matrix);
#endif
        MatrixComplete(matrix);
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        MatAssemblyBegin((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        if (Rhs != NULL)
        {
            VecAssemblyBegin((Vec)(rhs->data_petsc));
            VecAssemblyEnd((Vec)(rhs->data_petsc));
        }
#else
        RaisePetscError("AssembleMatrix");
#endif
    }

    ElementDestroy(&Elem);
    OpenPFEM_Free(SubEntries);
    OpenPFEM_Free(SubVec);
    OpenPFEM_Free(ElemBoundaryType);
    OpenPFEM_Free(BoundaryVec);

    *Matrix = matrix;
    if (Rhs != NULL)
    {
        *Rhs = rhs;
    }
}

void MatrixReAssemble(MATRIX *matrix, VECTOR *rhs)
{
    // (0.0) 前处理
    ASSEMBLE_INFO *assembleinfo = (ASSEMBLE_INFO *)matrix->assemble_info;
    if (assembleinfo == NULL)
        RaiseError("ReAssembleMatrix", "Only use after BeginAssembleMatrix!");
    DATATYPE type = matrix->type;
    MatrixScale(0.0, matrix);
    if (rhs != NULL)
        VectorScale(0.0, rhs);
    INT *old_kcol = NULL;
    if (DataTypeOpenPFEMEqual(type))
    {
        INT *ndiag_globalcols = matrix->ndiag_globalcols;
        INT *old_kcol = matrix->ndiag->KCol;
        INT *new_kcol = (INT *)malloc(matrix->ndiag->NumEntries * sizeof(INT));
        INT i;
        for (i = 0; i < matrix->ndiag->NumEntries; i++)
        {
            new_kcol[i] = ndiag_globalcols[old_kcol[i]];
        }
        matrix->ndiag->KCol = new_kcol;
    }
    DISCRETEFORM *DiscreteForm = assembleinfo->discreteform;
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    INT idx_neig, neignum = Rmesh->SharedInfo->NumNeighborRanks;
    DOUBLE *send_ent_each_row = assembleinfo->send_ent_each_row;
    DOUBLE *recv_ent_each_row = assembleinfo->recv_ent_each_row;
    INT *recv_ent_num = assembleinfo->recv_col_num;
    INT *recv_ent_num_displs = assembleinfo->recv_col_num_displs;
    INT *send_ent_num = assembleinfo->send_col_num;
    INT *send_ent_num_displs = assembleinfo->send_col_num_displs;
    memset(send_ent_each_row, 0.0, (send_ent_num_displs[neignum - 1] + send_ent_num[neignum - 1]) * sizeof(DOUBLE));

    // lhc 提前计算重复信息
    ComputeBaseValuesInPointsOfRefElem(DiscreteForm);

    // (0) 准备
    /** 目前只实现了左右网格是相同的情况 */
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *SubEntries = malloc(RNumBase * CNumBase * sizeof(DOUBLE));
    DOUBLE *SubVec = NULL;
    if (rhs != NULL)
    {
        SubVec = malloc(RNumBase * sizeof(DOUBLE));
    }
    INT *Rlocalindex = Rspace->LocalIndex;
    INT *Rglobalindex = Rspace->GlobalIndex;
    INT *Rbeginindex = Rspace->BeginIndex;
    INT *Clocalindex = Cspace->LocalIndex;
    INT *Cglobalindex = Cspace->GlobalIndex;
    INT *Cbeginindex = Cspace->BeginIndex;
    ELEMENT *Elem = NULL;
    INT HasBoundary = 0;
    if (Rspace->BoundType)
        HasBoundary = 1;
    // (0.0) 看看是否含有辅助有限元函数AuxFEMFun, 并且建立相应的存储基函数和辅助有限元函数值的内存
    DOUBLE *AuxFEMValues = NULL;
    if (DiscreteForm->AuxFEMFunction != NULL)
    {
        AuxFEMValues = DiscreteForm->AuxFEMFunction->AuxFEMFunValues;
    }

    // (1) 边界条件处理
    LINE line;
    FACE face;
    VOLU volu;
    INT LocalNumElems;
    INT numlinevert, numfacevert, numvoluvert, numfaceline, numvoluline, numvoluface;
    INT numlocalbd, DirichletBDInd;
    INT *dof = &Rspace->Base->DOF[0];
    BOUNDARYTYPE *ElemBoundaryType = malloc(RNumBase * sizeof(BOUNDARYTYPE));
    memset(ElemBoundaryType, INNER, RNumBase * sizeof(BOUNDARYTYPE));
    DOUBLE *BoundaryVec = malloc(RNumBase * sizeof(DOUBLE));

    // lhc 23.01.28
    // 判断是否存在更细的网格 如果有 按照不同方式进行组装
    if (DiscreteForm->IsUseAuxFEMSpace == 0)
    {
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;

        case 2:
            LocalNumElems = Rmesh->num_face;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts;
                numfaceline = face.NumLines;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;

        case 3:
            LocalNumElems = Rmesh->num_volu;
            Elem = ElementInitial(Rmesh);
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts;
                numvoluline = volu.NumLines;
                numvoluface = volu.NumFaces;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // (2) 单元遍历
        INT idx_elem;
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.1) 检查本单元上的边界信息
            DirichletBDInd = 0;
            if (HasBoundary)
            {
                // 得到本单元上测试空间自由度的边界类型
                //  void GetElementBoundaryInformation(FEMSPACE *femspace, INT ind, INT numlocalbd,
                //  BOUNDARYTYPE *ElemBoundaryType, INT *DirichletBDInd)
                //  ElemBoundaryType: 存储了该单元上每个自由度的边界条件
                // DirichletBDInd表示该单元是否需要进行Dirichlet边条件的处理
                GetElementBoundaryInformation(Rspace, idx_elem, ElemBoundaryType, &DirichletBDInd);
            }
            // (2.2) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem, ElemBoundaryType, SubEntries, SubVec);
            // (2.3) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_elem],
                               CNumBase, Clocalindex + Cbeginindex[idx_elem]);
            // (2.4) 处理右端项
            if (rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_elem]);
            }
            // (2.5) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDInd == 1)
            {
                // 表示要处理Dirichlet边条件
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem, numlocalbd, BoundaryVec,
                                          ElemBoundaryType);
            }
        }
    }
    else
    {
        // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        // ComputeInvJacobiOfAllElem(Rmesh);
        // 细网格
        MESH *MeshInAssemble = DiscreteForm->LeftSpaceInAssemble->Mesh;
        // 细网格对应的信息
        INT LocalNumElems_finer;
        ELEMENT *Elem_finer = NULL;
        // 获取网格的单元信息和自由度边界信息
        switch (worlddim)
        {
        case 1:
            LocalNumElems = Rmesh->num_line;
            LocalNumElems_finer = MeshInAssemble->num_line;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                line = Rmesh->Lines[0];
                numlinevert = 2;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numlinevert;
            }
            break;

        case 2:
            LocalNumElems = Rmesh->num_face;
            LocalNumElems_finer = MeshInAssemble->num_face;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                face = Rmesh->Faces[0];
                numfacevert = face.NumVerts;
                numfaceline = face.NumLines;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numfacevert + dof[1] * numfaceline;
            }
            break;

        case 3:
            LocalNumElems = Rmesh->num_volu;
            LocalNumElems_finer = MeshInAssemble->num_volu;
            Elem = ElementInitial(Rmesh);
            Elem_finer = ElementInitial(MeshInAssemble);
            if (HasBoundary)
            {
                volu = Rmesh->Volus[0];
                numvoluvert = volu.NumVerts;
                numvoluline = volu.NumLines;
                numvoluface = volu.NumFaces;
                // 边界条件的个数就是定义在边界边上的自由度个数
                numlocalbd = dof[0] * numvoluvert + dof[1] * numvoluline + dof[2] * numvoluface;
            }
            break;
        }
        // (2.0)首先提前计算粗网格Rmesh中每个单元的边界信息ElemBoundaryType和DirichletBDInd 并统一存储在BoundaryTypeOfAllElems和DirichletBDIndOfAllElems中
        BOUNDARYTYPE *BoundaryTypeOfAllElems = malloc(LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        memset(BoundaryTypeOfAllElems, INNER, LocalNumElems * RNumBase * sizeof(BOUNDARYTYPE));
        INT *DirichletBDIndOfAllElems = malloc(LocalNumElems * sizeof(INT));
        memset(DirichletBDIndOfAllElems, 0, LocalNumElems * sizeof(INT));
        // 增加利用变分形式判断单元类型
        SetElemType(Elem, Rspace);
        // 提前生成粗网格各个单元的Jacobian Volumn Inv_Jacobian并存储到粗网格结构体中
        if (Elem->is_curl == 1 || Elem->is_div == 1)
        {
            ComputeInvJacobiOfAllElem(Rmesh, 1);
        }
        else
        {
            ComputeInvJacobiOfAllElem(Rmesh, 0);
        }
        // 对粗网格Rmesh进行单元循环 填充上述信息
        INT idx_elem, idx_related;
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.0.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.0.1) 检查本单元上的边界信息
            if (HasBoundary)
            {
                GetElementBoundaryInformation(Rspace, idx_elem, BoundaryTypeOfAllElems + idx_elem * RNumBase, &(DirichletBDIndOfAllElems[idx_elem]));
            }
        }
        // (2.1)对细网格进行单元循环 完成刚度矩阵的组装
        for (idx_elem = 0; idx_elem < LocalNumElems_finer; idx_elem++)
        {
            // (2.1.0) 建立当前的有限单元
            ElementBuild(MeshInAssemble, idx_elem, Elem_finer);
            // 找到对应粗网格中的大单元的编号
            // idx_related = MeshInAssemble->Ancestors[idx_elem];
            if (DiscreteForm->Relates == NULL)
            {
                idx_related = idx_elem;
            }
            else
            {
                idx_related = DiscreteForm->Relates[idx_elem];
            }
            // (2.1.1) 在目前单元上组装单刚矩阵
            SubMatrixAssemble(DiscreteForm, idx_elem, Elem_finer, BoundaryTypeOfAllElems + idx_related * RNumBase, SubEntries, SubVec);
            // (2.1.2) 单刚矩阵合成总刚度矩阵
            MatrixAddSubMatrix(matrix, assembleinfo, SubEntries, RNumBase, Rlocalindex + Rbeginindex[idx_related],
                               CNumBase, Clocalindex + Cbeginindex[idx_related]);
            // (2.1.3) 处理右端项
            if (rhs != NULL)
            {
                VectorAddSubVector(rhs, assembleinfo, SubVec, RNumBase, Rlocalindex + Rbeginindex[idx_related]);
            }
        }
        // (2.2)对粗网格单元进行单元循环 进行边界自由度对角元素的处理
        for (idx_elem = 0; idx_elem < LocalNumElems; idx_elem++)
        {
            // (2.2.0) 建立当前的有限单元
            ElementBuild(Rmesh, idx_elem, Elem);
            // (2.2.1) 最后再来处理Dirichlet边条件的右端项和刚度矩阵的对角线
            if (DirichletBDIndOfAllElems[idx_elem] == 1)
            {
                DirichletBoundaryTreatmen(DiscreteForm, matrix, rhs, Elem, idx_elem, numlocalbd, BoundaryVec,
                                          BoundaryTypeOfAllElems + idx_elem * RNumBase);
            }
        }
    }

    // (3.1) 开始通信
    MPI_Comm comm = Rmesh->SharedInfo->neighborcomm;
    MPI_Request entreq;
    MPI_Ineighbor_alltoallv(send_ent_each_row, send_ent_num, send_ent_num_displs, MPI_DOUBLE,
                            recv_ent_each_row, recv_ent_num, recv_ent_num_displs, MPI_DOUBLE, comm, &entreq);
    MPI_Wait(&entreq, MPI_STATUS_IGNORE);
    MPI_Request_free(&entreq);

    // (3.2) 把收到的信息组装上去
    INT rows_start = matrix->rows_start, rows_end = matrix->rows_end;
    INT *row_globalindex = Rspace->GlobalIndex;
    INT *recv_row_index = assembleinfo->recv_row_index;
    INT *recv_entnum_each_row = assembleinfo->recv_colnum_each_row;
    INT *recv_colindex_each_row = assembleinfo->recv_colindex_each_row;
    INT *recv_row_num = assembleinfo->recv_row_num;
    INT *recv_row_num_displs = assembleinfo->recv_row_num_displs;
    INT start1 = 0, start2 = 0, position, entnum, idx_row, rowindex_local;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        position = recv_row_num_displs[idx_neig];
        start2 = recv_ent_num_displs[idx_neig];
        // 矩阵部分
        for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
        {
            rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
            entnum = recv_entnum_each_row[position];
            MatrixAddRowMatrix(matrix, entnum, rowindex_local, &(recv_colindex_each_row[start1]), &(recv_ent_each_row[start2]));
            position++;
            start1 += entnum;
            start2 += entnum;
        }
        // 向量部分
        if (rhs != NULL)
        {
            if (DataTypeOpenPFEMEqual(type))
            {
                position = recv_row_num_displs[idx_neig];
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]] - rows_start;
                    rhs->data[rowindex_local] += recv_ent_each_row[start2];
                    position++;
                    start2++;
                }
            }
            else if (DataTypePetscEqual(type))
            {
#if defined(PETSC_USE)
                position = recv_row_num_displs[idx_neig];
                Vec vec = (Vec)(rhs->data_petsc);
                for (idx_row = 0; idx_row < recv_row_num[idx_neig]; idx_row++)
                {
                    rowindex_local = row_globalindex[recv_row_index[position]];
                    VecSetValue(vec, rowindex_local, recv_ent_each_row[start2], ADD_VALUES);
                    position++;
                    start2++;
                }
#else
                RaisePetscError("AssembleMatrix");
#endif
            }
        }
    }

    if (DataTypeOpenPFEMEqual(type))
    {
        OpenPFEM_Free(matrix->ndiag->KCol);
        matrix->ndiag->KCol = old_kcol;
    }
    else if (DataTypePetscEqual(type))
    {
#if defined(PETSC_USE)
        MatAssemblyBegin((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd((Mat)(matrix->data_petsc), MAT_FINAL_ASSEMBLY);
        if (rhs != NULL)
        {
            VecAssemblyBegin((Vec)(rhs->data_petsc));
            VecAssemblyEnd((Vec)(rhs->data_petsc));
        }
#else
        RaisePetscError("AssembleMatrix");
#endif
    }
}

void MatrixEndAssemble(MATRIX *matrix, VECTOR *rhs, DISCRETEFORM *DiscreteForm)
{
    ASSEMBLE_INFO *assembleinfo = (ASSEMBLE_INFO *)matrix->assemble_info;
    OpenPFEM_Free(assembleinfo->row_owner);
    OpenPFEM_Free(assembleinfo->row_owner_index);
    OpenPFEM_Free(assembleinfo->submat_col_diag);
    OpenPFEM_Free(assembleinfo->submat_col_index);
    OpenPFEM_Free(assembleinfo->send_row_num_displs);
    OpenPFEM_Free(assembleinfo->send_colnum_each_row_displs);
    OpenPFEM_Free(assembleinfo->send_colindex_each_row);
    OpenPFEM_Free(assembleinfo->send_ent_each_row);
    OpenPFEM_Free(assembleinfo->send_row_num);
    OpenPFEM_Free(assembleinfo->send_col_num);
    OpenPFEM_Free(assembleinfo->send_col_num_displs);
    OpenPFEM_Free(assembleinfo->recv_row_num);
    OpenPFEM_Free(assembleinfo->recv_row_num_displs);
    OpenPFEM_Free(assembleinfo->recv_col_num);
    OpenPFEM_Free(assembleinfo->recv_col_num_displs);
    OpenPFEM_Free(assembleinfo->recv_ent_each_row);
    OpenPFEM_Free(assembleinfo->recv_row_index);
    OpenPFEM_Free(assembleinfo->recv_colnum_each_row);
    OpenPFEM_Free(assembleinfo->recv_colindex_each_row);
    OpenPFEM_Free(assembleinfo);
    matrix->assemble_info = NULL;
}

// 对于调整了局部编号顺序的curl单元 完成单刚组装之后 将单刚调整回原来的顺序
void ReorderSubMatrixForCurlElem(DISCRETEFORM *DiscreteForm, INT idx_elem, ELEMENT *Elem, DOUBLE *SubEntries, DOUBLE *SubVec)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT *Rdof = Rbase->DOF, *Cdof = Cbase->DOF;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase, Inter_Dim = Rbase->InterpolationValueDim;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *temp_SubEntries, *temp_SubVec;
    temp_SubEntries = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制单刚
    memcpy(temp_SubEntries, SubEntries, RNumBase * CNumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof, colline, colface, coldof;
    if (worlddim < 3)
        RaiseError("ReorderSubMatrixForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new, num_col_old, num_col_new;
    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * Rdof[1];
        num_row_new = rowline * Rdof[1];
        for (rowdof = 0; rowdof < Rdof[1]; rowdof++)
        {
            // 列对应 线上自由度
            for (colline = 0; colline < 6; colline++)
            {
                num_col_old = LineIdInRefElem[colline] * Cdof[1];
                num_col_new = colline * Cdof[1];
                for (coldof = 0; coldof < Cdof[1]; coldof++)
                {
                    SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
                }
            }
            // 列对应 面上自由度
            for (colline = 0; colline < 4; colline++)
            {
                num_col_old = 6 * Cdof[1] + FaceIdInRefElem[colline] * Cdof[2];
                num_col_new = 6 * Cdof[1] + colline * Cdof[2];
                for (coldof = 0; coldof < Cdof[2]; coldof++)
                {
                    SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
                }
            }
            // 列对应 体上自由度
            num_col_old = 6 * Cdof[1] + 4 * Cdof[2];
            num_col_new = num_col_old;
            for (coldof = 0; coldof < Cdof[3]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * Rdof[1] + FaceIdInRefElem[rowline] * Rdof[2];
        num_row_new = 6 * Rdof[1] + rowline * Rdof[2];
        for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
        {
            // 列对应 线上自由度
            for (colline = 0; colline < 6; colline++)
            {
                num_col_old = LineIdInRefElem[colline] * Cdof[1];
                num_col_new = colline * Cdof[1];
                for (coldof = 0; coldof < Cdof[1]; coldof++)
                {
                    SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
                }
            }
            // 列对应 面上自由度
            for (colline = 0; colline < 4; colline++)
            {
                num_col_old = 6 * Cdof[1] + FaceIdInRefElem[colline] * Cdof[2];
                num_col_new = 6 * Cdof[1] + colline * Cdof[2];
                for (coldof = 0; coldof < Cdof[2]; coldof++)
                {
                    SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
                }
            }
            // 列对应 体上自由度
            num_col_old = 6 * Cdof[1] + 4 * Cdof[2];
            num_col_new = num_col_old;
            for (coldof = 0; coldof < Cdof[3]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
    }
    // 行对应 体上自由度
    num_row_old = 6 * Rdof[1] + 4 * Rdof[2];
    num_row_new = num_row_old;
    for (rowdof = 0; rowdof < Rdof[3]; rowdof++)
    {
        // 列对应 线上自由度
        for (colline = 0; colline < 6; colline++)
        {
            num_col_old = LineIdInRefElem[colline] * Cdof[1];
            num_col_new = colline * Cdof[1];
            for (coldof = 0; coldof < Cdof[1]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
        // 列对应 面上自由度
        for (colline = 0; colline < 4; colline++)
        {
            num_col_old = 6 * Cdof[1] + FaceIdInRefElem[colline] * Cdof[2];
            num_col_new = 6 * Cdof[1] + colline * Cdof[2];
            for (coldof = 0; coldof < Cdof[2]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
    }
    free(temp_SubEntries);
    if (SubVec != NULL)
    {
        temp_SubVec = malloc(RNumBase * sizeof(DOUBLE)); // 复制右端项
        memcpy(temp_SubVec, SubVec, RNumBase * sizeof(DOUBLE));
        // 行对应 线上的自由度
        for (rowline = 0; rowline < 6; rowline++)
        {
            num_row_old = LineIdInRefElem[rowline] * Rdof[1];
            num_row_new = rowline * Rdof[1];
            for (rowdof = 0; rowdof < Rdof[1]; rowdof++)
            {
                SubVec[num_row_old + rowdof] = temp_SubVec[num_row_new + rowdof];
            }
        }
        // 行对应 面上的自由度
        for (rowline = 0; rowline < 4; rowline++)
        {
            num_row_old = 6 * Rdof[1] + FaceIdInRefElem[rowline] * Rdof[2];
            num_row_new = 6 * Rdof[1] + rowline * Rdof[2];
            for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
            {
                SubVec[num_row_old + rowdof] = temp_SubVec[num_row_new + rowdof];
            }
        }
        free(temp_SubVec);
    }
}

// 对于调整了局部编号顺序的div单元 完成单刚组装之后 将单刚调整回原来的顺序
void ReorderSubMatrixForDivElem(DISCRETEFORM *DiscreteForm, INT idx_elem, ELEMENT *Elem, DOUBLE *SubEntries, DOUBLE *SubVec)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT *Rdof = Rbase->DOF, *Cdof = Cbase->DOF;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase, Inter_Dim = Rbase->InterpolationValueDim;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *temp_SubEntries, *temp_SubVec;
    temp_SubEntries = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制单刚
    memcpy(temp_SubEntries, SubEntries, RNumBase * CNumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof, colline, colface, coldof;
    if (worlddim < 3)
        RaiseError("ReorderSubMatrixForDivElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new, num_col_old, num_col_new;
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = FaceIdInRefElem[rowline] * Rdof[2];
        num_row_new = rowline * Rdof[2];
        for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
        {
            // 列对应 面上自由度
            for (colline = 0; colline < 4; colline++)
            {
                num_col_old = FaceIdInRefElem[colline] * Cdof[2];
                num_col_new = colline * Cdof[2];
                for (coldof = 0; coldof < Cdof[2]; coldof++)
                {
                    SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = pow(-1, colline + rowline) * temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
                }
            }
            // 列对应 体上自由度
            num_col_old = 4 * Cdof[2];
            num_col_new = num_col_old;
            for (coldof = 0; coldof < Cdof[3]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = pow(-1, (int)Elem->is_det_positive + rowline + 1) * temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
    }
    // 行对应 体上自由度
    num_row_old = 4 * Rdof[2];
    num_row_new = num_row_old;
    for (rowdof = 0; rowdof < Rdof[3]; rowdof++)
    {
        // 列对应 面上自由度
        for (colline = 0; colline < 4; colline++)
        {
            num_col_old = 6 * Cdof[1] + FaceIdInRefElem[colline] * Cdof[2];
            num_col_new = 6 * Cdof[1] + colline * Cdof[2];
            for (coldof = 0; coldof < Cdof[2]; coldof++)
            {
                SubEntries[(num_row_old + rowdof) * CNumBase + num_col_old + coldof] = pow(-1, (int)Elem->is_det_positive + colline + 1) * temp_SubEntries[(num_row_new + rowdof) * CNumBase + num_col_new + coldof];
            }
        }
    }
    free(temp_SubEntries);
    if (SubVec != NULL)
    {
        temp_SubVec = malloc(RNumBase * sizeof(DOUBLE)); // 复制右端项
        memcpy(temp_SubVec, SubVec, RNumBase * sizeof(DOUBLE));
        // 行对应 面上的自由度
        for (rowline = 0; rowline < 4; rowline++)
        {
            num_row_old = FaceIdInRefElem[rowline] * Rdof[2];
            num_row_new = rowline * Rdof[2];
            for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
            {
                SubVec[num_row_old + rowdof] = pow(-1, (int)Elem->is_det_positive + rowline + 1) * temp_SubVec[num_row_new + rowdof];
            }
        }
        free(temp_SubVec);
    }
}

// 对于调整了局部编号顺序的curl单元 处理dirichlet边界时对右端项进行赋值 需要调整Nodal的顺序
void ReorderBoundaryVecForCurlElem(DISCRETEFORM *DiscreteForm, INT idx_elem, ELEMENT *Elem, DOUBLE *BoundaryVec)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT *Rdof = Rbase->DOF, *Cdof = Cbase->DOF;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase, Inter_Dim = Rbase->InterpolationValueDim;
    INT worlddim = Rmesh->worlddim;
    DOUBLE *temp_BoundaryVec;
    temp_BoundaryVec = malloc(RNumBase * sizeof(DOUBLE)); // 复制右端项
    memcpy(temp_BoundaryVec, BoundaryVec, RNumBase * sizeof(DOUBLE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderBoundaryVecForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new;
    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * Rdof[1];
        num_row_new = rowline * Rdof[1];
        for (rowdof = 0; rowdof < Rdof[1]; rowdof++)
        {
            BoundaryVec[num_row_old + rowdof] = temp_BoundaryVec[num_row_new + rowdof];
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * Rdof[1] + FaceIdInRefElem[rowline] * Rdof[2];
        num_row_new = 6 * Rdof[1] + rowline * Rdof[2];
        for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
        {
            BoundaryVec[num_row_old + rowdof] = temp_BoundaryVec[num_row_new + rowdof];
        }
    }
    free(temp_BoundaryVec);
}

// 对于调整了局部编号顺序的curl单元 需要将BoundaryType转化为新编号对应的顺序
void ReorderBoundaryTypeForCurlElem(DISCRETEFORM *DiscreteForm, INT idx_elem, ELEMENT *Elem, BOUNDARYTYPE *BoundaryType)
{
    FEMSPACE *Rspace = DiscreteForm->RightSpace, *Cspace = DiscreteForm->LeftSpace;
    MESH *Rmesh = Rspace->Mesh, *Cmesh = Cspace->Mesh;
    BASE *Rbase = Rspace->Base, *Cbase = Cspace->Base;
    INT *Rdof = Rbase->DOF, *Cdof = Cbase->DOF;
    INT RNumBase = Rbase->NumBase, CNumBase = Cbase->NumBase, Inter_Dim = Rbase->InterpolationValueDim;
    INT worlddim = Rmesh->worlddim;
    BOUNDARYTYPE *temp_BoundaryType;
    temp_BoundaryType = malloc(RNumBase * sizeof(BOUNDARYTYPE)); // 复制右端项
    memcpy(temp_BoundaryType, BoundaryType, RNumBase * sizeof(BOUNDARYTYPE));
    INT rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderBoundaryTypeForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = Elem->VertIdInRefElem;
    INT *LineIdInRefElem = Elem->LineIdInRefElem;
    // BOOL *if_line_positive = Elem->if_line_positive;
    // BOOL *if_norm_positive = Elem->if_norm_positive;
    INT num_row_old, num_row_new;
    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * Rdof[1];
        num_row_new = rowline * Rdof[1];
        for (rowdof = 0; rowdof < Rdof[1]; rowdof++)
        {
            BoundaryType[num_row_new + rowdof] = temp_BoundaryType[num_row_old + rowdof];
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * Rdof[1] + FaceIdInRefElem[rowline] * Rdof[2];
        num_row_new = 6 * Rdof[1] + rowline * Rdof[2];
        for (rowdof = 0; rowdof < Rdof[2]; rowdof++)
        {
            BoundaryType[num_row_new + rowdof] = temp_BoundaryType[num_row_old + rowdof];
        }
    }
    free(temp_BoundaryType);
}

void ReorderLocalProlongColForCurlElem(FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, INT relate, ELEMENT *CoarseElem, DOUBLE *LocalProlong)
{
    MESH *FinerMesh = FinerSpace->Mesh, *CoarseMesh = CoarseSpace->Mesh;
    BASE *FinerBase = FinerSpace->Base, *CoarseBase = CoarseSpace->Base;
    INT *FinerDof = FinerBase->DOF, *CoarseDof = CoarseBase->DOF;
    INT RNumBase = FinerBase->NumBase, CNumBase = CoarseBase->NumBase;
    INT worlddim = FinerMesh->worlddim;
    DOUBLE *temp_LocalProlong;
    temp_LocalProlong = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制LocalProlong
    memcpy(temp_LocalProlong, LocalProlong, RNumBase * CNumBase * sizeof(DOUBLE));
    INT rowind, colline, colface, coldof;
    if (worlddim < 3)
        RaiseError("ReorderLocalProlongColForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = CoarseElem->VertIdInRefElem;
    INT *LineIdInRefElem = CoarseElem->LineIdInRefElem;
    INT num_col_old, num_col_new;
    // 列对应 线上自由度
    for (colline = 0; colline < 6; colline++)
    {
        num_col_old = LineIdInRefElem[colline] * CoarseDof[1];
        num_col_new = colline * CoarseDof[1];
        for (coldof = 0; coldof < CoarseDof[1]; coldof++)
        {
            for (rowind = 0; rowind < RNumBase; rowind++)
                LocalProlong[rowind * CNumBase + num_col_old + coldof] = temp_LocalProlong[rowind * CNumBase + num_col_new + coldof];
        }
    }
    // 列对应 面上自由度
    for (colline = 0; colline < 4; colline++)
    {
        num_col_old = 6 * CoarseDof[1] + FaceIdInRefElem[colline] * CoarseDof[2];
        num_col_new = 6 * CoarseDof[1] + colline * CoarseDof[2];
        for (coldof = 0; coldof < CoarseDof[2]; coldof++)
        {
            for (rowind = 0; rowind < RNumBase; rowind++)
                LocalProlong[rowind * CNumBase + num_col_old + coldof] = temp_LocalProlong[rowind * CNumBase + num_col_new + coldof];
        }
    }
    free(temp_LocalProlong);
}

void ReorderLocalProlongRowForCurlElem(FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, INT ind, ELEMENT *FinerElem, DOUBLE *LocalProlong)
{
    MESH *FinerMesh = FinerSpace->Mesh, *CoarseMesh = CoarseSpace->Mesh;
    BASE *FinerBase = FinerSpace->Base, *CoarseBase = CoarseSpace->Base;
    INT *FinerDof = FinerBase->DOF, *CoarseDof = CoarseBase->DOF;
    INT RNumBase = FinerBase->NumBase, CNumBase = CoarseBase->NumBase;
    INT worlddim = FinerMesh->worlddim;
    DOUBLE *temp_LocalProlong;
    temp_LocalProlong = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制LocalProlong
    memcpy(temp_LocalProlong, LocalProlong, RNumBase * CNumBase * sizeof(DOUBLE));
    INT colind, rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderLocalProlongRowForCurlElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = FinerElem->VertIdInRefElem;
    INT *LineIdInRefElem = FinerElem->LineIdInRefElem;
    INT num_row_old, num_row_new;
    // 行对应 线上的自由度
    for (rowline = 0; rowline < 6; rowline++)
    {
        num_row_old = LineIdInRefElem[rowline] * FinerDof[1];
        num_row_new = rowline * FinerDof[1];
        for (rowdof = 0; rowdof < FinerDof[1]; rowdof++)
        {
            for (colind = 0; colind < CNumBase; colind++)
                LocalProlong[(num_row_old + rowdof) * CNumBase + colind] = temp_LocalProlong[(num_row_new + rowdof) * CNumBase + colind];
        }
    }
    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = 6 * FinerDof[1] + FaceIdInRefElem[rowline] * FinerDof[2];
        num_row_new = 6 * FinerDof[1] + rowline * FinerDof[2];
        for (rowdof = 0; rowdof < FinerDof[2]; rowdof++)
        {
            for (colind = 0; colind < CNumBase; colind++)
                LocalProlong[(num_row_old + rowdof) * CNumBase + colind] = temp_LocalProlong[(num_row_new + rowdof) * CNumBase + colind];
        }
    }
    free(temp_LocalProlong);
}

void ReorderLocalProlongColForDivElem(FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, INT relate, ELEMENT *CoarseElem, DOUBLE *LocalProlong)
{
    MESH *FinerMesh = FinerSpace->Mesh, *CoarseMesh = CoarseSpace->Mesh;
    BASE *FinerBase = FinerSpace->Base, *CoarseBase = CoarseSpace->Base;
    INT *FinerDof = FinerBase->DOF, *CoarseDof = CoarseBase->DOF;
    INT RNumBase = FinerBase->NumBase, CNumBase = CoarseBase->NumBase;
    INT worlddim = FinerMesh->worlddim;
    DOUBLE *temp_LocalProlong;
    temp_LocalProlong = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制LocalProlong
    memcpy(temp_LocalProlong, LocalProlong, RNumBase * CNumBase * sizeof(DOUBLE));
    INT rowind, colline, colface, coldof;
    if (worlddim < 3)
        RaiseError("ReorderLocalProlongColForDivElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = CoarseElem->VertIdInRefElem;
    INT *LineIdInRefElem = CoarseElem->LineIdInRefElem;
    INT num_col_old, num_col_new;

    INT ii0[4] = {1, 0, 0, 0};
    INT ii1[4] = {2, 2, 1, 1};
    INT ii2[4] = {3, 3, 3, 2};
    INT ii3[4] = {0, 1, 2, 3};
    DOUBLE vec_line0[3], vec_line1[3], vec_norm[3];
    INT i = 3;
    vec_line0[0] = CoarseElem->Vert_X[ii1[i]] - CoarseElem->Vert_X[ii0[i]];
    vec_line0[1] = CoarseElem->Vert_Y[ii1[i]] - CoarseElem->Vert_Y[ii0[i]];
    vec_line0[2] = CoarseElem->Vert_Z[ii1[i]] - CoarseElem->Vert_Z[ii0[i]];
    vec_line1[0] = CoarseElem->Vert_X[ii2[i]] - CoarseElem->Vert_X[ii0[i]];
    vec_line1[1] = CoarseElem->Vert_Y[ii2[i]] - CoarseElem->Vert_Y[ii0[i]];
    vec_line1[2] = CoarseElem->Vert_Z[ii2[i]] - CoarseElem->Vert_Z[ii0[i]];
    vec_norm[0] = vec_line0[1] * vec_line1[2] - vec_line0[2] * vec_line1[1];
    vec_norm[1] = vec_line0[2] * vec_line1[0] - vec_line0[0] * vec_line1[2];
    vec_norm[2] = vec_line0[0] * vec_line1[1] - vec_line0[1] * vec_line1[0];
    if ((vec_norm[0] * (CoarseElem->Vert_X[ii3[i]] - CoarseElem->Vert_X[ii0[i]]) + vec_norm[1] * (CoarseElem->Vert_Y[ii3[i]] - CoarseElem->Vert_Y[ii0[i]]) + vec_norm[2] * (CoarseElem->Vert_Z[ii3[i]] - CoarseElem->Vert_Z[ii0[i]])) > 0)
    {
        CoarseElem->is_det_positive = 1;
    }
    else
    {
        CoarseElem->is_det_positive = 0;
    }
    OpenPFEM_Print("coarse %d\n", CoarseElem->is_det_positive);

    // 列对应 面上自由度
    for (colline = 0; colline < 4; colline++)
    {
        num_col_old = FaceIdInRefElem[colline] * CoarseDof[2];
        num_col_new = colline * CoarseDof[2];
        for (coldof = 0; coldof < CoarseDof[2]; coldof++)
        {
            for (rowind = 0; rowind < RNumBase; rowind++)
                // LocalProlong[rowind * CNumBase + num_col_old + coldof] = pow(-1, (int)CoarseElem->is_det_positive) * temp_LocalProlong[rowind * CNumBase + num_col_new + coldof];
                LocalProlong[rowind * CNumBase + num_col_old + coldof] = pow(-1, (int)CoarseElem->is_det_positive + 1) * temp_LocalProlong[rowind * CNumBase + num_col_new + coldof];
        }
    }
    free(temp_LocalProlong);
}

void ReorderLocalProlongRowForDivElem(FEMSPACE *CoarseSpace, FEMSPACE *FinerSpace, INT ind, ELEMENT *FinerElem, DOUBLE *LocalProlong)
{
    MESH *FinerMesh = FinerSpace->Mesh, *CoarseMesh = CoarseSpace->Mesh;
    BASE *FinerBase = FinerSpace->Base, *CoarseBase = CoarseSpace->Base;
    INT *FinerDof = FinerBase->DOF, *CoarseDof = CoarseBase->DOF;
    INT RNumBase = FinerBase->NumBase, CNumBase = CoarseBase->NumBase;
    INT worlddim = FinerMesh->worlddim;
    DOUBLE *temp_LocalProlong;
    temp_LocalProlong = malloc(RNumBase * CNumBase * sizeof(DOUBLE)); // 复制LocalProlong
    memcpy(temp_LocalProlong, LocalProlong, RNumBase * CNumBase * sizeof(DOUBLE));
    INT colind, rowline, rowface, rowdof;
    if (worlddim < 3)
        RaiseError("ReorderLocalProlongRowForDivElem", "1D 2D 未完成！");
    INT *FaceIdInRefElem = FinerElem->VertIdInRefElem;
    INT *LineIdInRefElem = FinerElem->LineIdInRefElem;
    INT num_row_old, num_row_new;

    INT ii0[4] = {1, 0, 0, 0};
    INT ii1[4] = {2, 2, 1, 1};
    INT ii2[4] = {3, 3, 3, 2};
    INT ii3[4] = {0, 1, 2, 3};
    DOUBLE vec_line0[3], vec_line1[3], vec_norm[3];
    INT i = 3;
    vec_line0[0] = FinerElem->Vert_X[ii1[i]] - FinerElem->Vert_X[ii0[i]];
    vec_line0[1] = FinerElem->Vert_Y[ii1[i]] - FinerElem->Vert_Y[ii0[i]];
    vec_line0[2] = FinerElem->Vert_Z[ii1[i]] - FinerElem->Vert_Z[ii0[i]];
    vec_line1[0] = FinerElem->Vert_X[ii2[i]] - FinerElem->Vert_X[ii0[i]];
    vec_line1[1] = FinerElem->Vert_Y[ii2[i]] - FinerElem->Vert_Y[ii0[i]];
    vec_line1[2] = FinerElem->Vert_Z[ii2[i]] - FinerElem->Vert_Z[ii0[i]];
    vec_norm[0] = vec_line0[1] * vec_line1[2] - vec_line0[2] * vec_line1[1];
    vec_norm[1] = vec_line0[2] * vec_line1[0] - vec_line0[0] * vec_line1[2];
    vec_norm[2] = vec_line0[0] * vec_line1[1] - vec_line0[1] * vec_line1[0];
    if ((vec_norm[0] * (FinerElem->Vert_X[ii3[i]] - FinerElem->Vert_X[ii0[i]]) + vec_norm[1] * (FinerElem->Vert_Y[ii3[i]] - FinerElem->Vert_Y[ii0[i]]) + vec_norm[2] * (FinerElem->Vert_Z[ii3[i]] - FinerElem->Vert_Z[ii0[i]])) > 0)
    {
        FinerElem->is_det_positive = 1;
    }
    else
    {
        FinerElem->is_det_positive = 0;
    }
    OpenPFEM_Print("finer %d\n", FinerElem->is_det_positive);

    // 行对应 面上的自由度
    for (rowline = 0; rowline < 4; rowline++)
    {
        num_row_old = FaceIdInRefElem[rowline] * FinerDof[2];
        num_row_new = rowline * FinerDof[2];
        for (rowdof = 0; rowdof < FinerDof[2]; rowdof++)
        {
            for (colind = 0; colind < CNumBase; colind++)
                LocalProlong[(num_row_old + rowdof) * CNumBase + colind] = pow(-1, (int)FinerElem->is_det_positive + 1) * pow(-1, rowline) * temp_LocalProlong[(num_row_new + rowdof) * CNumBase + colind];
            // LocalProlong[(num_row_old + rowdof) * CNumBase + colind] = pow(-1, (int)FinerElem->is_det_positive + rowline) * temp_LocalProlong[(num_row_new + rowdof) * CNumBase + colind];
            // LocalProlong[(num_row_old + rowdof) * CNumBase + colind] = temp_LocalProlong[(num_row_new + rowdof) * CNumBase + colind];
        }
    }
    free(temp_LocalProlong);
}
void ProlongMatrixReorder(MATRIX **prolong, FEMSPACE **oldspace, MESH *newmesh, BRIDGE *bridge)
{
    // (1) 在新网格上建立新的有限元空间
    FEMTYPE FemType = (*oldspace)->FemType;
    FEMSPACE *newspace = FEMSpaceBuild(newmesh, FemType, (*oldspace)->BoundType);
    // (2) 找到新的自由度对应的旧自由度的全局编号
    INT *gindex_old = (*oldspace)->GlobalIndex;
    INT *DOF = (*oldspace)->Base->DOF;
    INT old_num_vert = bridge->oldgeonum[0];
    INT old_num_line = bridge->oldgeonum[1];
    INT old_num_face = bridge->oldgeonum[2];
    INT numbaseline_old = DOF[0] * old_num_vert;
    INT numbaseface_old = numbaseline_old + DOF[1] * old_num_line;
    PARADATA *paradata = NULL;
    ParaDataCreateWithBridge(&paradata, bridge);
    ParaDataAdd(paradata, VERTDATA, DOF[0], MPI_INT, (void *)gindex_old);
    ParaDataAdd(paradata, LINEDATA, DOF[1], MPI_INT, (void *)(&(gindex_old[numbaseline_old])));
    if (newmesh->worlddim == 3)
        ParaDataAdd(paradata, FACEDATA, DOF[2], MPI_INT, (void *)(&(gindex_old[numbaseface_old])));
    PARADATA *recvdata = NULL;
    ParaDataCreateByParaData(&recvdata, paradata);
    BridgeCommunicate(paradata, recvdata);
    // 找出来对应矩阵每行的部分 (属于本进程的)
    INT *old_gindex_4new = (INT *)malloc(newspace->MyNumDOF * sizeof(INT));
    INT new_gstart = newspace->GDOFstart, new_gend = newspace->GDOFend;
    INT *gindex_new = newspace->GlobalIndex;
    INT new_num_vert = bridge->newgeonum[0];
    INT new_num_line = bridge->newgeonum[1];
    INT new_num_face = bridge->newgeonum[2];
    INT numbaseline_new = DOF[0] * new_num_vert;
    INT numbaseface_new = numbaseline_new + DOF[1] * new_num_line;
    INT position = 0, i;
    INT *vertdata = (INT *)recvdata->VertData[0]->Data;
    for (i = 0; i < numbaseline_new; i++)
    {
        // 如果属于本进程, 即这个自由度有对应行
        if (gindex_new[i] < new_gend && gindex_new[i] >= new_gstart)
        {
            old_gindex_4new[position] = vertdata[i];
            position++;
        }
    }
    INT *linedata = (INT *)recvdata->LineData[0]->Data;
    for (i = numbaseline_new; i < numbaseface_new; i++)
    {
        // 如果属于本进程, 即这个自由度有对应行
        if (gindex_new[i] < new_gend && gindex_new[i] >= new_gstart)
        {
            old_gindex_4new[position] = linedata[i - numbaseline_new];
            position++;
        }
    }
    if (newmesh->worlddim == 3)
    {
        INT *facedata = (INT *)recvdata->FaceData[0]->Data;
        for (i = numbaseface_new; i < newspace->NumDOF; i++)
        {
            // 如果属于本进程, 即这个自由度有对应行
            if (gindex_new[i] < new_gend && gindex_new[i] >= new_gstart)
            {
                old_gindex_4new[position] = facedata[i - numbaseface_new];
                position++;
            }
        }
    }
    ParaDataDestroy(&paradata);
    ParaDataDestroy(&recvdata);
    // (3) 重排矩阵
    MatrixRowsReOrder(prolong, old_gindex_4new, newspace->MyNumDOF, bridge);

    OpenPFEM_Free(old_gindex_4new);
    FEMSpaceDestroy(oldspace);
    *oldspace = newspace;
}
