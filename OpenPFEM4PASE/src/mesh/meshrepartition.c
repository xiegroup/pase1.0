#include "meshrepartition.h"

#define PRINT_INFO 0
#define DEBUG 1

int rank, size;

void MeshRepartition(MESH *mesh, BRIDGE **bri)
{
    /*-----------------------------------------------*/
    /*                重分布信息的准备               */
    /*-----------------------------------------------*/
    if (!MPI_USE)
        return;
    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    INT worlddim = mesh->worlddim;
    if (size == 1)
    {
        return;
    }
    MESH *new_mesh = NULL;
    MeshCreate(&new_mesh, mesh->worlddim, comm);
    BRIDGE *bridge = NULL;
    if (bri != NULL)
    {
        BridgeCreate(&bridge, mesh->worlddim);
    }
    /*-----------------------------------------------*/
    PTW *Vert4Elems = NULL;
    PTWCreate(&Vert4Elems);
    GetGEO4Elems(mesh, worlddim, 0, Vert4Elems);
    INT elemnum = Vert4Elems->NumRows;
    /*-----------------------------------------------*/
    VertGlobalIndexGenerate(mesh);
    /*-----------------------------------------------*/
    INT *elem_rank = (INT *)malloc(elemnum * sizeof(INT));
    ParMetisPartition(mesh, Vert4Elems, elemnum, elem_rank);
    PTWDestroy(&Vert4Elems);

    /*-----------------------------------------------*/
    /*              计算点的信息并打包               */
    /*-----------------------------------------------*/
    PTW *Vert2Ranks = NULL, *Vert4Ranks = NULL;
    PTWCreate(&Vert2Ranks);
    GetGEO2Ranks(mesh, size, elem_rank, worlddim, 0, Vert2Ranks);
    /*-----------------------------------------------*/
    SharedGeoReRank(mesh, Vert2Ranks, VERTDATA);
    PTWCreate(&Vert4Ranks);
    PTWTranspose(Vert2Ranks, Vert4Ranks);
    /*-----------------------------------------------*/
    // 计算vert包裹的大小
    INT *vertcount, *vertdisplc;
    INT VertPackageTotalSize =
        Vert_PackageSize(mesh, Vert4Ranks, Vert2Ranks, &vertcount, &vertdisplc);
    MPI_Request reqvertsize;
    INT *Vert_Boxsize = (INT *)malloc(size * sizeof(INT));
    MPI_Ialltoall(vertcount, 1, MPI_INT, Vert_Boxsize, 1, MPI_INT, comm, &reqvertsize);
    /*-----------------------------------------------*/
    // 计算vert包裹
    char *vertpackage = NULL;
    if (bri != NULL)
    {
        Vert_Package(mesh, bridge, Vert4Ranks, Vert2Ranks, &vertpackage, VertPackageTotalSize);
    }
    else
    {
        Vert_Package(mesh, NULL, Vert4Ranks, Vert2Ranks, &vertpackage, VertPackageTotalSize);
    }
    PTWDestroy(&Vert2Ranks);
    MPI_Wait(&reqvertsize, MPI_STATUS_IGNORE);
    MPI_Request_free(&reqvertsize);
    INT VertBoxTotalSize = Vert_Boxsize[0];
    INT *Vert_BoxDisplc = (INT *)malloc(size * sizeof(INT));
    Vert_BoxDisplc[0] = 0;
    INT idx;
    for (idx = 1; idx < size; idx++)
    {
        Vert_BoxDisplc[idx] = Vert_BoxDisplc[idx - 1] + Vert_Boxsize[idx - 1];
        VertBoxTotalSize += Vert_Boxsize[idx];
    }
    char *VertBox = (char *)malloc(VertBoxTotalSize * sizeof(char));
    /*-----------------------------------------------*/
    // 通信vert包裹
    MPI_Request reqvert;
    MPI_Ialltoallv(vertpackage, vertcount, vertdisplc, MPI_PACKED,
                   VertBox, Vert_Boxsize, Vert_BoxDisplc, MPI_PACKED, comm, &reqvert);
    LineGlobalIndexGenerate(mesh);

    /*-----------------------------------------------*/
    /*              计算线的信息并打包               */
    /*-----------------------------------------------*/
    // 计算线的分配信息
    PTW *Line2Ranks = NULL, *Line4Ranks = NULL;
    PTWCreate(&Line2Ranks);
    GetGEO2Ranks(mesh, size, elem_rank, worlddim, 1, Line2Ranks);
    SharedGeoReRank(mesh, Line2Ranks, LINEDATA);
    PTWCreate(&Line4Ranks);
    PTWTranspose(Line2Ranks, Line4Ranks);
    /*-----------------------------------------------*/
    // 计算line包裹的大小
    INT *linecount, *linedisplc;
    INT LinePackageTotalSize =
        Line_PackageSize(mesh, Line4Ranks, Line2Ranks, &linecount, &linedisplc);
    MPI_Request reqlinesize;
    INT *Line_Boxsize = (INT *)malloc(size * sizeof(INT));
    MPI_Ialltoall(linecount, 1, MPI_INT, Line_Boxsize, 1, MPI_INT, comm, &reqlinesize);
    /*-----------------------------------------------*/
    // 计算line包裹
    char *linepackage = NULL;
    if (bri != NULL)
    {
        Line_Package(mesh, bridge, Line4Ranks, Line2Ranks, &linepackage, LinePackageTotalSize);
    }
    else
    {
        Line_Package(mesh, NULL, Line4Ranks, Line2Ranks, &linepackage, LinePackageTotalSize);
    }
    PTWDestroy(&Line2Ranks);
    MPI_Wait(&reqlinesize, MPI_STATUS_IGNORE);
    MPI_Request_free(&reqlinesize);
    INT LineBoxTotalSize = Line_Boxsize[0];
    INT *Line_BoxDisplc = (INT *)malloc(size * sizeof(INT));
    Line_BoxDisplc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        Line_BoxDisplc[idx] = Line_BoxDisplc[idx - 1] + Line_Boxsize[idx - 1];
        LineBoxTotalSize += Line_Boxsize[idx];
    }
    char *LineBox = (char *)malloc(LineBoxTotalSize * sizeof(char));
    /*-----------------------------------------------*/
    // 通信line包裹
    MPI_Request reqline;
    MPI_Ialltoallv(linepackage, linecount, linedisplc, MPI_PACKED,
                   LineBox, Line_Boxsize, Line_BoxDisplc, MPI_PACKED, comm, &reqline);
    FaceGlobalIndexGenerate(mesh);

    /*-----------------------------------------------*/
    /*              计算面的信息并打包               */
    /*-----------------------------------------------*/
    // 计算面的分配信息
    PTW *Face2Ranks = NULL, *Face4Ranks = NULL;
    PTWCreate(&Face2Ranks);
    GetGEO2Ranks(mesh, size, elem_rank, worlddim, 2, Face2Ranks);
    SharedGeoReRank(mesh, Face2Ranks, FACEDATA);
    PTWCreate(&Face4Ranks);
    PTWTranspose(Face2Ranks, Face4Ranks);
    /*-----------------------------------------------*/
    // 计算face包裹的大小
    INT *facecount, *facedisplc;
    INT FacePackageTotalSize =
        Face_PackageSize(mesh, Face4Ranks, Face2Ranks, &facecount, &facedisplc);
    MPI_Request reqfacesize;
    INT *Face_Boxsize = (INT *)malloc(size * sizeof(INT));
    MPI_Ialltoall(facecount, 1, MPI_INT, Face_Boxsize, 1, MPI_INT, comm, &reqfacesize);
    /*-----------------------------------------------*/
    // 计算face包裹
    char *facepackage = NULL;
    if (bridge != NULL)
    {
        Face_Package(mesh, bridge, Face4Ranks, Face2Ranks, &facepackage, FacePackageTotalSize);
    }
    else
    {
        Face_Package(mesh, NULL, Face4Ranks, Face2Ranks, &facepackage, FacePackageTotalSize);
    }
    PTWDestroy(&Face2Ranks);
    MPI_Wait(&reqfacesize, MPI_STATUS_IGNORE);
    MPI_Request_free(&reqfacesize);
    INT FaceBoxTotalSize = Face_Boxsize[0];
    INT *Face_BoxDisplc = (INT *)malloc(size * sizeof(INT));
    Face_BoxDisplc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        Face_BoxDisplc[idx] = Face_BoxDisplc[idx - 1] + Face_Boxsize[idx - 1];
        FaceBoxTotalSize += Face_Boxsize[idx];
    }
    char *FaceBox = (char *)malloc(FaceBoxTotalSize * sizeof(char));
    /*-----------------------------------------------*/
    // 通信face包裹
    MPI_Request reqface;
    MPI_Ialltoallv(facepackage, facecount, facedisplc, MPI_PACKED,
                   FaceBox, Face_Boxsize, Face_BoxDisplc, MPI_PACKED, comm, &reqface);

    MPI_Request reqvolu;
    INT *volucount, *voludisplc, *Volu_Boxsize, *Volu_BoxDisplc;
    INT VoluPackageTotalSize = 0, VoluBoxTotalSize = 0;
    PTW *Volu4Ranks = NULL;
    char *VoluBox = NULL;
    if (worlddim == 3)
    {
        /*-----------------------------------------------*/
        /*              计算体的信息并打包               */
        /*-----------------------------------------------*/
        // 计算体的分配信息
        PTW *Volu2Ranks = NULL;
        PTWCreate(&Volu2Ranks);
        PTWCreate(&Volu4Ranks);
        GetGEO2Ranks(mesh, size, elem_rank, worlddim, 3, Volu2Ranks);
        PTWTranspose(Volu2Ranks, Volu4Ranks);
        /*-----------------------------------------------*/
        // 计算volu包裹的大小
        VoluPackageTotalSize =
            Volu_PackageSize(mesh, Volu4Ranks, Volu2Ranks, &volucount, &voludisplc);
        MPI_Request reqvolusize;
        Volu_Boxsize = (INT *)malloc(size * sizeof(INT));
        MPI_Ialltoall(volucount, 1, MPI_INT, Volu_Boxsize, 1, MPI_INT, comm, &reqvolusize);
        /*-----------------------------------------------*/
        // 计算volu包裹
        char *volupackage = NULL;
        Volu_Package(mesh, Volu4Ranks, Volu2Ranks, &volupackage, VoluPackageTotalSize);
        PTWDestroy(&Volu2Ranks);
        MPI_Wait(&reqvolusize, MPI_STATUS_IGNORE);
        MPI_Request_free(&reqvolusize);
        VoluBoxTotalSize = Volu_Boxsize[0];
        Volu_BoxDisplc = (INT *)malloc(size * sizeof(INT));
        Volu_BoxDisplc[0] = 0;
        for (idx = 1; idx < size; idx++)
        {
            Volu_BoxDisplc[idx] = Volu_BoxDisplc[idx - 1] + Volu_Boxsize[idx - 1];
            VoluBoxTotalSize += Volu_Boxsize[idx];
        }
        VoluBox = (char *)malloc(VoluBoxTotalSize * sizeof(char));
        /*-----------------------------------------------*/
        // 通信volu包裹
        MPI_Ialltoallv(volupackage, volucount, voludisplc, MPI_PACKED,
                       VoluBox, Volu_Boxsize, Volu_BoxDisplc, MPI_PACKED, comm, &reqvolu);
    }

    // 等收到点的包裹
    MPI_Wait(&reqvert, MPI_STATUSES_IGNORE);
    OpenPFEM_Free(vertcount);
    OpenPFEM_Free(vertdisplc);
    OpenPFEM_Free(vertpackage);
    /*-----------------------------------------------*/
    // 拆包vert包裹
    if (bri != NULL)
    {
        VertBox2Mesh(new_mesh, mesh, bridge, Vert4Ranks, VertBox, Vert_Boxsize, Vert_BoxDisplc, VertBoxTotalSize);
    }
    else
    {
        VertBox2Mesh(new_mesh, mesh, NULL, Vert4Ranks, VertBox, Vert_Boxsize, Vert_BoxDisplc, VertBoxTotalSize);
    }
    OpenPFEM_Free(Vert_Boxsize);
    OpenPFEM_Free(Vert_BoxDisplc);
    /*-----------------------------------------------*/
    // 建立节点的全局编号和局部编号对应的法则
    INT k, num_vert = new_mesh->num_vert;
    INT *GVertIndex = malloc(num_vert * sizeof(INT)); // 存储全局编号
    INT *VertIndex = malloc(num_vert * sizeof(INT));  // 存储局部编号
    for (k = 0; k < num_vert; k++)
    {
        GVertIndex[k] = new_mesh->Verts[k].Index;
        VertIndex[k] = k;
    }
    // 得到以全局节点编号顺序排列的节点局部编号信息
    MergeSortRecursion(GVertIndex, VertIndex, 0, num_vert - 1);
    if (bri != NULL)
    {
        RebuildSharedGeo(new_mesh, bridge, GVertIndex, VertIndex, VERTDATA);
    }
    else
    {
        RebuildSharedGeo(new_mesh, NULL, GVertIndex, VertIndex, VERTDATA);
    }
    // 有上面的信息才可以计算线的信息
    MPI_Wait(&reqline, MPI_STATUSES_IGNORE);
    OpenPFEM_Free(linecount);
    OpenPFEM_Free(linedisplc);
    OpenPFEM_Free(linepackage);
    /*-----------------------------------------------*/
    // 拆包line包裹，同时生成线的信息
    if (bri != NULL)
    {
        LineBox2Mesh(new_mesh, mesh, bridge, Line4Ranks, LineBox, Line_Boxsize, Line_BoxDisplc, LineBoxTotalSize, GVertIndex, VertIndex);
    }
    else
    {
        LineBox2Mesh(new_mesh, mesh, NULL, Line4Ranks, LineBox, Line_Boxsize, Line_BoxDisplc, LineBoxTotalSize, GVertIndex, VertIndex);
    }
    OpenPFEM_Free(Line_Boxsize);
    OpenPFEM_Free(Line_BoxDisplc);
    /*-----------------------------------------------*/
    // 建立线的全局编号与局部编号的对应关系
    INT num_line = new_mesh->num_line;
    INT *GLineIndex = malloc(num_line * sizeof(INT));
    INT *LineIndex = malloc(num_line * sizeof(INT));
    for (k = 0; k < num_line; k++)
    {
        GLineIndex[k] = new_mesh->Lines[k].Index;
        LineIndex[k] = k;
    }
    // 得到以全局线编号顺序排列的线的局部编号信息
    MergeSortRecursion(GLineIndex, LineIndex, 0, num_line - 1);
    if (bri != NULL)
    {
        RebuildSharedGeo(new_mesh, bridge, GLineIndex, LineIndex, LINEDATA);
    }
    else
    {
        RebuildSharedGeo(new_mesh, NULL, GLineIndex, LineIndex, LINEDATA);
    }
    // 有上面的信息才可以计算面的信息
    MPI_Wait(&reqface, MPI_STATUSES_IGNORE);
    OpenPFEM_Free(facecount);
    OpenPFEM_Free(facedisplc);
    /*-----------------------------------------------*/
    // 拆包face包裹
    if (bri != NULL)
    {
        FaceBox2Mesh(new_mesh, mesh, bridge, Face4Ranks, FaceBox, Face_Boxsize, Face_BoxDisplc, FaceBoxTotalSize,
                     GVertIndex, VertIndex, GLineIndex, LineIndex);
    }
    else
    {
        FaceBox2Mesh(new_mesh, mesh, NULL, Face4Ranks, FaceBox, Face_Boxsize, Face_BoxDisplc, FaceBoxTotalSize,
                     GVertIndex, VertIndex, GLineIndex, LineIndex);
    }
    OpenPFEM_Free(Face_Boxsize);
    OpenPFEM_Free(Face_BoxDisplc);
    /*-----------------------------------------------*/
    INT *GFaceIndex = NULL, *FaceIndex = NULL;
    if (worlddim == 3)
    {
        // 建立面的全局编号与局部编号的对应关系
        INT num_face = new_mesh->num_face;
        GFaceIndex = malloc(num_face * sizeof(INT));
        FaceIndex = malloc(num_face * sizeof(INT));
        for (k = 0; k < num_face; k++)
        {
            GFaceIndex[k] = new_mesh->Faces[k].Index;
            FaceIndex[k] = k;
        }
        // 得到以全局面编号顺序排列的面的局部编号信息
        MergeSortRecursion(GFaceIndex, FaceIndex, 0, num_face - 1);
        if (bri != NULL)
        {
            RebuildSharedGeo(new_mesh, bridge, GFaceIndex, FaceIndex, FACEDATA);
        }
        else
        {
            RebuildSharedGeo(new_mesh, NULL, GFaceIndex, FaceIndex, FACEDATA);
        }
        MPI_Wait(&reqvolu, MPI_STATUSES_IGNORE);
        OpenPFEM_Free(volucount);
        OpenPFEM_Free(voludisplc);
        /*-----------------------------------------------*/
        // 拆包volu包裹
        VoluBox2Mesh(new_mesh, mesh, Volu4Ranks, VoluBox, Volu_Boxsize, Volu_BoxDisplc, VoluBoxTotalSize, GVertIndex, VertIndex,
                     GLineIndex, LineIndex, GFaceIndex, FaceIndex);
        MPI_Request_free(&reqvolu);
        PTWDestroy(&Volu4Ranks);
        OpenPFEM_Free(Volu_Boxsize);
        OpenPFEM_Free(Volu_BoxDisplc);
        /*-----------------------------------------------*/
    }
    // 释放全局编号与局部编号对应向量的内存
    OpenPFEM_Free(GVertIndex);
    OpenPFEM_Free(VertIndex);
    OpenPFEM_Free(GLineIndex);
    OpenPFEM_Free(LineIndex);
    if (worlddim == 3)
    {
        OpenPFEM_Free(GFaceIndex);
        OpenPFEM_Free(FaceIndex);
    }
    OpenPFEM_Free(elem_rank);
    MPI_Barrier(comm);
    if (bri != NULL)
    {
        BridgeCommCreate(new_mesh, mesh, bridge);
    }
    /* 销毁旧网格并用新网格覆盖 */
    MeshReplace(mesh, new_mesh);
    /* 搭建bridge的通信桥梁 */
    if (bri != NULL)
    {
        *bri = bridge;
    }
    // 将网格对象的Index改成局部Index????
    if (mesh->Ancestors != NULL)
        free(mesh->Ancestors);
    mesh->Ancestors = NULL;
    if (mesh->Fathers != NULL)
        free(mesh->Fathers);
    mesh->Fathers = NULL;
    return;
}

void MeshReplace(MESH *oldmesh, MESH *newmesh)
{
    assert(oldmesh->comm == newmesh->comm);
    OpenPFEM_Free(oldmesh->Verts);
    oldmesh->Verts = newmesh->Verts;
    OpenPFEM_Free(oldmesh->Lines);
    oldmesh->Lines = newmesh->Lines;
    OpenPFEM_Free(oldmesh->Faces);
    oldmesh->Faces = newmesh->Faces;
    OpenPFEM_Free(oldmesh->Volus);
    oldmesh->Volus = newmesh->Volus;
    SharedInfoDestroy(&(oldmesh->SharedInfo));
    oldmesh->SharedInfo = newmesh->SharedInfo;
    oldmesh->num_vert = newmesh->num_vert;
    oldmesh->num_line = newmesh->num_line;
    oldmesh->num_face = newmesh->num_face;
    oldmesh->num_volu = newmesh->num_volu;
    if (oldmesh->Fathers != NULL)
        OpenPFEM_Free(oldmesh->Fathers);
    if (oldmesh->Ancestors != NULL)
        OpenPFEM_Free(oldmesh->Ancestors);
    oldmesh->Fathers = NULL;
    oldmesh->Ancestors = NULL;
    NeighborCommCreate(oldmesh);

    INT i;
    for (i = 0; i < oldmesh->num_vert; i++)
    {
        oldmesh->Verts[i].Index = i;
    }
    for (i = 0; i < oldmesh->num_line; i++)
    {
        oldmesh->Lines[i].Index = i;
    }
    for (i = 0; i < oldmesh->num_face; i++)
    {
        oldmesh->Faces[i].Index = i;
    }
    for (i = 0; i < oldmesh->num_volu; i++)
    {
        oldmesh->Volus[i].Index = i;
    }
}

void RebuildSharedGeo(MESH *mesh, BRIDGE *bridge, INT *GIndex, INT *Index, INT dim)
{
    NeighborCommCreate(mesh);
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    INT neignum = sharedinfo->NumNeighborRanks;
    INT *neigrank = sharedinfo->NeighborRanks;
    MPI_Comm comm = sharedinfo->neighborcomm;
    INT orignum = 0;
    INT *origrank = NULL;
    GEOBRIDGE *geobridge = NULL;
    if (bridge != NULL)
    {
        orignum = bridge->OriginRankNum;
        origrank = bridge->OriginRank;
        geobridge = bridge->GeoRecv;
    }

    SHAREDGEO *sharedgeo = NULL;
    INT geonum = 0;
    switch (dim)
    {
    case VERTDATA:
        sharedgeo = sharedinfo->SharedVerts;
        geonum = mesh->num_vert;
        break;
    case LINEDATA:
        sharedgeo = sharedinfo->SharedLines;
        geonum = mesh->num_line;
        break;
    case FACEDATA:
        sharedgeo = sharedinfo->SharedFaces;
        geonum = mesh->num_face;
        break;
    case VOLUDATA:
        sharedgeo = sharedinfo->SharedVerts;
        geonum = mesh->num_volu;
        break;
    }

    /* 把全局编号换成局部的 */
    INT idx_neig, idx, idy = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        idy = 0;
        INT *sharedindex = (INT *)malloc(sharedgeo[idx_neig].SharedNum * sizeof(INT));
        for (idx = 0; idx < sharedgeo[idx_neig].SharedNum; idx++)
        {
            while (sharedgeo[idx_neig].Index[idx] != GIndex[idy]) // 找这个全局编号
            {
                idy++;
            }
            sharedindex[idx] = Index[idy]; // 暂时存在sharedindex里，因为全局编号还有用
        }

        sharedgeo[idx_neig].SharedIndex = sharedindex;
    }

    /* 互通局部编号 */
    /* (1)准备信息 */
    INT packagetotalsize = 0;
    INT *packagesize = (INT *)malloc(neignum * sizeof(INT));
    INT *packagedispls = (INT *)malloc(neignum * sizeof(INT));
    packagedispls[0] = 0;
    for (idx_neig = 0; idx_neig < neignum - 1; idx_neig++)
    {
        packagesize[idx_neig] = sharedgeo[idx_neig].SharedNum;
        packagedispls[idx_neig + 1] = packagedispls[idx_neig] + packagesize[idx_neig];
    }
    packagesize[neignum - 1] = sharedgeo[neignum - 1].SharedNum;
    packagetotalsize = packagedispls[neignum - 1] + packagesize[neignum - 1];
    INT *package = (INT *)malloc(packagetotalsize * sizeof(INT));
    INT *box = (INT *)malloc(packagetotalsize * sizeof(INT));
    packagetotalsize = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        for (idx = 0; idx < sharedgeo[idx_neig].SharedNum; idx++)
        {
            package[packagetotalsize] = sharedgeo[idx_neig].SharedIndex[idx];
            packagetotalsize++;
        }
    }
    MPI_Request request;
    MPI_Ineighbor_alltoallv(package, packagesize, packagedispls, MPI_INT, box, packagesize, packagedispls, MPI_INT, comm, &request);

    /* 全局分配Owner */
    /* 规则：全局编号奇数归进程号小的，偶数归进程号大的 */
    INT neigindex, smallrank, largerank;
    /* 对所有owner做一个统计 */
    INT *geoowner = (INT *)malloc(geonum * sizeof(INT));
    for (idx = 0; idx < geonum; idx++)
    {
        geoowner[idx] = rank;
    }
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigindex = neigrank[idx_neig];
        for (idx = 0; idx < sharedgeo[idx_neig].SharedNum; idx++)
        {
            if (sharedgeo[idx_neig].Index[idx] % 2 == 0)
            {
                if (neigindex > geoowner[sharedgeo[idx_neig].SharedIndex[idx]])
                {
                    geoowner[sharedgeo[idx_neig].SharedIndex[idx]] = neigindex;
                }
            }
            else
            {
                if (neigindex < geoowner[sharedgeo[idx_neig].SharedIndex[idx]])
                {
                    geoowner[sharedgeo[idx_neig].SharedIndex[idx]] = neigindex;
                }
            }
        }
    }
    /* 更新共享的owner */
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        idy = 0;
        INT *owner = (INT *)malloc(sharedgeo[idx_neig].SharedNum * sizeof(INT));
        for (idx = 0; idx < sharedgeo[idx_neig].SharedNum; idx++)
        {
            owner[idx] = geoowner[sharedgeo[idx_neig].SharedIndex[idx]];
        }
        sharedgeo[idx_neig].Owner = owner;
    }
    OpenPFEM_Free(geoowner);

    if (bridge != NULL)
    {
        /* 进行bridge信息的更新 */
        INT idx_orig, idx_geo, origeonum = 0;
        INT *origeoindex = NULL;
        for (idx_orig = 0; idx_orig < orignum; idx_orig++) // 对每个曾经收到信息的进程
        {
            switch (dim)
            {
            case VERTDATA:
                origeonum = geobridge[idx_orig].VertNum;
                origeoindex = geobridge[idx_orig].VertIndex;
                break;
            case LINEDATA:
                origeonum = geobridge[idx_orig].LineNum;
                origeoindex = geobridge[idx_orig].LineIndex;
                break;
            case FACEDATA:
                origeonum = geobridge[idx_orig].FaceNum;
                origeoindex = geobridge[idx_orig].FaceIndex;
                break;
            case VOLUDATA:
                origeonum = geobridge[idx_orig].VoluNum;
                origeoindex = geobridge[idx_orig].VoluIndex;
                break;
            }
            INT *posind = (INT *)malloc(origeonum * sizeof(INT)); // 作为一个位置的辅助
            for (idx_geo = 0; idx_geo < origeonum; idx_geo++)
            {
                posind[idx_geo] = idx_geo;
            }
            QuickSortIntValueVector(origeoindex, posind, 0, origeonum - 1);
            // 把全局编号更新成局部的编号
            idy = 0;
            for (idx = 0; idx < origeonum; idx++)
            {
                while (origeoindex[idx] != GIndex[idy]) // 找这个全局编号
                {
                    idy++;
                }
                origeoindex[idx] = Index[idy];
            }
            QuickSortIntValueVector(posind, origeoindex, 0, origeonum - 1);
            OpenPFEM_Free(posind);
        }
    }

    /* 更新邻居局部编号 */
    MPI_Wait(&request, MPI_STATUS_IGNORE);
    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(package);
    packagetotalsize = 0;
    INT *tmp;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        for (idx = 0; idx < sharedgeo[idx_neig].SharedNum; idx++)
        {
            sharedgeo[idx_neig].Index[idx] = box[packagetotalsize];
            packagetotalsize++;
        }
        tmp = sharedgeo[idx_neig].Index;
        sharedgeo[idx_neig].Index = sharedgeo[idx_neig].SharedIndex;
        sharedgeo[idx_neig].SharedIndex = tmp;
    }
    OpenPFEM_Free(box);
}

void VoluBox2Mesh(MESH *newmesh, MESH *oldmesh, PTW *Volu4Rank, char *VoluBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex, INT *GFaceIndex, INT *FaceIndex)
{
    INT *volunum = (INT *)malloc(size * sizeof(INT));
    INT **globalindex = (INT **)malloc(size * sizeof(INT *));
    INT **vert4volu = (INT **)malloc(size * sizeof(INT *));
    INT **line4volu = (INT **)malloc(size * sizeof(INT *));
    INT **face4volu = (INT **)malloc(size * sizeof(INT *));

    VERT *oldverts = oldmesh->Verts;
    LINE *oldlines = oldmesh->Lines;
    FACE *oldfaces = oldmesh->Faces;
    VOLU *oldvolus = oldmesh->Volus;

    MPI_Comm comm = newmesh->comm;
    INT position = 0, num, numrank;
    INT idx_rank, idx, begin, end;
    INT *ptwu4r = Volu4Rank->Ptw;
    INT *u4r = Volu4Rank->Entries;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            MPI_Unpack(VoluBox, BoxTotalSize, &position, &num, 1, MPI_INT, comm);
            volunum[idx_rank] = num;
            globalindex[idx_rank] = (INT *)malloc(num * sizeof(INT));
            vert4volu[idx_rank] = (INT *)malloc(MAXVERT4VOLU * num * sizeof(INT *));
            line4volu[idx_rank] = (INT *)malloc(MAXLINE4VOLU * num * sizeof(INT *));
            face4volu[idx_rank] = (INT *)malloc(MAXFACE4VOLU * num * sizeof(INT *));
            for (idx = 0; idx < num; idx++)
            {
                MPI_Unpack(VoluBox, BoxTotalSize, &position, &(globalindex[idx_rank][idx]), 1, MPI_INT, comm);
                MPI_Unpack(VoluBox, BoxTotalSize, &position, &(vert4volu[idx_rank][MAXVERT4VOLU * idx]), MAXVERT4VOLU, MPI_INT, comm);
                MPI_Unpack(VoluBox, BoxTotalSize, &position, &(line4volu[idx_rank][MAXLINE4VOLU * idx]), MAXLINE4VOLU, MPI_INT, comm);
                MPI_Unpack(VoluBox, BoxTotalSize, &position, &(face4volu[idx_rank][MAXFACE4VOLU * idx]), MAXFACE4VOLU, MPI_INT, comm);
            }
        }
        else
        {
            begin = ptwu4r[rank];
            end = ptwu4r[rank + 1];
            num = end - begin;
            volunum[idx_rank] = num;
        }
    }
    OpenPFEM_Free(VoluBox);
    num = 0;
    for (idx = 0; idx < size; idx++)
    {
        num += volunum[idx];
    }
    VOLU *volus, *volu;
    VolusCreate(&volus, num);
    INT pos = 0, localindex, i;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            for (idx = 0; idx < volunum[idx_rank]; idx++)
            {
                volu = &(volus[pos]);
                volu->Index = globalindex[idx_rank][idx];
                memcpy(volu->Vert4Volu, &(vert4volu[idx_rank][MAXVERT4VOLU * idx]), MAXVERT4VOLU * sizeof(INT));
                memcpy(volu->Line4Volu, &(line4volu[idx_rank][MAXLINE4VOLU * idx]), MAXLINE4VOLU * sizeof(INT));
                memcpy(volu->Face4Volu, &(face4volu[idx_rank][MAXFACE4VOLU * idx]), MAXFACE4VOLU * sizeof(INT));
                pos++;
            }
            OpenPFEM_Free(vert4volu[idx_rank]);
            OpenPFEM_Free(line4volu[idx_rank]);
            OpenPFEM_Free(face4volu[idx_rank]);
        }
        else
        {
            begin = ptwu4r[rank];
            end = ptwu4r[rank + 1];
            for (idx = begin; idx < end; idx++)
            {
                localindex = u4r[idx];
                volu = &(volus[pos]);
                volu->Index = oldvolus[localindex].Index;
                for (i = 0; i < MAXVERT4VOLU; i++)
                {
                    volu->Vert4Volu[i] = oldverts[oldvolus[localindex].Vert4Volu[i]].Index;
                }
                for (i = 0; i < MAXLINE4VOLU; i++)
                {
                    volu->Line4Volu[i] = oldlines[oldvolus[localindex].Line4Volu[i]].Index;
                }
                for (i = 0; i < MAXFACE4VOLU; i++)
                {
                    volu->Face4Volu[i] = oldfaces[oldvolus[localindex].Face4Volu[i]].Index;
                }
                pos++;
            }
        }
    }
    OpenPFEM_Free(vert4volu);
    OpenPFEM_Free(line4volu);
    OpenPFEM_Free(face4volu);

    // 建立网格的体的信息
    newmesh->Volus = volus;
    newmesh->num_volu = num;
    RebuildVolus(newmesh, GVertIndex, VertIndex, GLineIndex, LineIndex, GFaceIndex, FaceIndex);
    // printf("[rank %d] 新网格单元个数: %d\n", rank, num);
}

// 重建四面体的信息, 由于没有重复的四面体, 这里的主要任务是建立相应的点、线、面的局部信息
void RebuildVolus(MESH *mesh, INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex, INT *GFaceIndex, INT *FaceIndex)
{
    INT k, i, num_volu = mesh->num_volu, pos;
    // 建立体上的点、线、面的局部编号
    INT *GVerts4Volus = malloc(6 * num_volu * sizeof(INT));
    INT *VoluPos = malloc(6 * num_volu * sizeof(INT)); // 为了后面进行线信息的建立
    for (k = 0; k < num_volu; k++)
    {
        pos = 4 * k;
        for (i = 0; i < 4; i++)
        {
            GVerts4Volus[pos + i] = mesh->Volus[k].Vert4Volu[i]; // 记录几点的全局编号
            VoluPos[pos + i] = pos + i;                          // 记录位置
        }
    }
    MergeSortRecursion(GVerts4Volus, VoluPos, 0, 4 * num_volu - 1);
    // 利用节点全局与局部编号之间的对应关系建立四面体的局部节点编号信息
    pos = 0;
    INT vertind = VertIndex[pos], Gvertind = GVertIndex[pos];
    INT end = 4 * num_volu;
    for (k = 0; k < end; k++)
    {
        if (GVerts4Volus[k] == Gvertind)
        {
            // 得到目前全局编号与节点的编号相等的情况, 可以得到局部的节点编号
            GVerts4Volus[k] = vertind; // 直接赋节点的局部编号
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行节点编号的更新
            pos++;
            vertind = VertIndex[pos];   // 得到新的节点编号
            Gvertind = GVertIndex[pos]; // 得到新的节点的全局编号
            GVerts4Volus[k] = vertind;  // 依然赋一个局部编号
        }                               // end if
    }                                   // end for k=0:end
    // 重新排序volupos获得与四面体顺序相对应的编号
    MergeSortRecursion(VoluPos, GVerts4Volus, 0, end - 1);
    for (k = 0; k < num_volu; k++)
    {
        pos = 4 * k;
        for (i = 0; i < 4; i++)
        {
            mesh->Volus[k].Vert4Volu[i] = GVerts4Volus[pos + i];
        }
    } // end for k
    // 下面来生成面上的线的信息
    INT *GFaces4Volus = GVerts4Volus; // 只是改了一下名字
    for (k = 0; k < num_volu; k++)
    {
        pos = 4 * k;
        for (i = 0; i < 4; i++)
        {
            GFaces4Volus[pos + i] = mesh->Volus[k].Face4Volu[i];
            VoluPos[pos + i] = pos + i;
        } // end for i
    }     // end for k
    MergeSortRecursion(GFaces4Volus, VoluPos, 0, end - 1);
    // 建立四面体上面的信息
    pos = 0;
    INT faceind = FaceIndex[pos], Gfaceind = GFaceIndex[pos];
    for (k = 0; k < end; k++)
    {
        if (GFaces4Volus[k] == Gfaceind)
        {
            // 得到目前全局编号与面的编号相等的情况, 可以得到局部的面编号
            GFaces4Volus[k] = faceind;
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行面编号的更新
            pos++;
            faceind = FaceIndex[pos];   // 得到新的面编号
            Gfaceind = GFaceIndex[pos]; // 得到新的面的全局编号
            GFaces4Volus[k] = faceind;
        } // end if
    }     // end for k=0:end
    // 重新排序volupos获得与四面体顺序相对应的编号
    MergeSortRecursion(VoluPos, GFaces4Volus, 0, end - 1);
    for (k = 0; k < num_volu; k++)
    {
        pos = 4 * k;
        for (i = 0; i < 4; i++)
        {
            mesh->Volus[k].Face4Volu[i] = GFaces4Volus[pos + i];
        }
    } // end for k
    // 现在来建立四面体的线编号信息
    INT *GLines4Volus = GVerts4Volus;
    for (k = 0; k < num_volu; k++)
    {
        pos = 6 * k;
        for (i = 0; i < 6; i++)
        {
            GLines4Volus[pos + i] = mesh->Volus[k].Line4Volu[i]; // 获得线的全局编号
            VoluPos[pos + i] = pos + i;                          // 记录位置
        }
    }
    end = 6 * num_volu;
    MergeSortRecursion(GLines4Volus, VoluPos, 0, end - 1);
    pos = 0;
    INT lineind = LineIndex[pos], Glineind = GLineIndex[pos];
    for (k = 0; k < end; k++)
    {
        if (GLines4Volus[k] == Glineind)
        {
            // 得到目前全局编号与节点的编号相等的情况, 可以得到局部的节点编号
            GLines4Volus[k] = lineind;
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行节点编号的更新
            pos++;
            lineind = LineIndex[pos];   // 得到新的节点编号
            Glineind = GLineIndex[pos]; // 得到新的节点的全局编号
            GLines4Volus[k] = lineind;
        } // end if
    }     // end for k=0:end
    // 重新排序volupos获得与四面体顺序相对应的编号
    MergeSortRecursion(VoluPos, GLines4Volus, 0, end - 1);
    for (k = 0; k < num_volu; k++)
    {
        pos = 6 * k;
        for (i = 0; i < 6; i++)
        {
            mesh->Volus[k].Line4Volu[i] = GLines4Volus[pos + i];
        } // end for i
    }     // end for k
    free(GVerts4Volus);
    free(VoluPos);
} // end for rebuildvolus

void Volu_Package(MESH *mesh, PTW *Volu4Rank, PTW *Volu2Rank, char **VoluPackage, INT totalsize)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0;
    INT num, begin, end, begin2, end2;
    INT idx_rank, idx, idy, voluindex, rankindex;
    INT neignum, volunum, totalnum, tmp1, tmp2;

    INT *ptwu4r = Volu4Rank->Ptw;
    INT *u4r = Volu4Rank->Entries;
    INT *ptwu2r = Volu2Rank->Ptw;
    INT *u2r = Volu2Rank->Entries;
    VERT *verts = mesh->Verts, *vert;
    LINE *lines = mesh->Lines, *line;
    FACE *faces = mesh->Faces, *face;
    VOLU *volus = mesh->Volus, *volu;

    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        neignum = 0;
        volunum = 0;
        totalnum = 0;
        begin = ptwu4r[idx_rank];
        end = ptwu4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        }
        for (idx = begin; idx < end; idx++)
        {
            voluindex = u4r[idx];
            volu = &(volus[voluindex]);
            if (idx_rank != rank)
            {
                MPI_Pack(&(volu->Index), 1, MPI_INT, package, totalsize, &position, comm);
                for (idy = 0; idy < MAXVERT4VOLU; idy++)
                {
                    MPI_Pack(&(verts[volu->Vert4Volu[idy]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                }
                for (idy = 0; idy < MAXLINE4VOLU; idy++)
                {
                    MPI_Pack(&(lines[volu->Line4Volu[idy]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                }
                for (idy = 0; idy < MAXFACE4VOLU; idy++)
                {
                    MPI_Pack(&(faces[volu->Face4Volu[idy]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                }
            }
        }
    }
    *VoluPackage = package;
}

INT Volu_PackageSize(MESH *mesh, PTW *Volu4Rank, PTW *Volu2Rank, INT **volucount, INT **voludispls)
{
    INT *ptwu4r = Volu4Rank->Ptw;
    INT *u4r = Volu4Rank->Entries;
    INT *ptwu2r = Volu2Rank->Ptw;
    INT *u2r = Volu2Rank->Entries;
    MPI_Comm comm = mesh->SharedInfo->neighborcomm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    INT *pack_size = (INT *)calloc(size, sizeof(INT));

    INT idx_rank, idx, idy, num, end, begin, flag, totalnum;
    INT voluindex, rankindex, begin2, end2;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        flag = 0;
        totalnum = 0;
        begin = ptwu4r[idx_rank];
        end = ptwu4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            pack_size[idx_rank] += sizeof(INT);                      // 体的个数
            pack_size[idx_rank] += num * sizeof(INT);                // 全局编号
            pack_size[idx_rank] += num * MAXVERT4VOLU * sizeof(INT); // 顶点的全局编号
            pack_size[idx_rank] += num * MAXLINE4VOLU * sizeof(INT); // 线的全局编号
            pack_size[idx_rank] += num * MAXFACE4VOLU * sizeof(INT); // 面的全局编号
        }
    }

    INT *pack_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = pack_size[0];
    pack_displc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        pack_displc[idx] = pack_displc[idx - 1] + pack_size[idx - 1];
        totalsize += pack_size[idx];
    }

    *volucount = pack_size;
    *voludispls = pack_displc;
    return totalsize;
}

void FaceBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Face4Rank, char *FaceBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex)
{
    INT worlddim = oldmesh->worlddim;
    INT *facenum = (INT *)calloc(size, sizeof(INT));
    INT **globalindex = (INT **)malloc(size * sizeof(INT *));
    INT **vert4face = (INT **)malloc(size * sizeof(INT *));
    INT **line4face = (INT **)malloc(size * sizeof(INT *));
    INT **BoundaryID = NULL, **neigrank = NULL, **neigsharednum = NULL;
    INT *neignum = NULL;
    INT ***neigsharedindex = NULL;
    if (worlddim == 3)
    {
        BoundaryID = (INT **)malloc(size * sizeof(INT *));
        neignum = (INT *)malloc(size * sizeof(INT));
        neigrank = (INT **)malloc(size * sizeof(INT *));
        neigsharednum = (INT **)malloc(size * sizeof(INT *));
        neigsharedindex = (INT ***)malloc(size * sizeof(INT **));
    }

    VERT *oldverts = oldmesh->Verts;
    LINE *oldlines = oldmesh->Lines;
    FACE *oldfaces = oldmesh->Faces;

    MPI_Comm comm = newmesh->comm;
    INT position = 0, num, numrank;
    INT idx_rank, idx, begin, end;
    INT *ptwf4r = Face4Rank->Ptw;
    INT *f4r = Face4Rank->Entries;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            MPI_Unpack(FaceBox, BoxTotalSize, &position, &num, 1, MPI_INT, comm);
            facenum[idx_rank] = num;
            globalindex[idx_rank] = (INT *)malloc(num * sizeof(INT));
            vert4face[idx_rank] = (INT *)malloc(MAXVERT4FACE * num * sizeof(INT *));
            line4face[idx_rank] = (INT *)malloc(MAXLINE4FACE * num * sizeof(INT *));
            if (worlddim == 3)
                BoundaryID[idx_rank] = (INT *)malloc(num * sizeof(INT));
            for (idx = 0; idx < num; idx++)
            {
                MPI_Unpack(FaceBox, BoxTotalSize, &position, &(globalindex[idx_rank][idx]), 1, MPI_INT, comm);
                MPI_Unpack(FaceBox, BoxTotalSize, &position, &(vert4face[idx_rank][MAXVERT4FACE * idx]), MAXVERT4FACE, MPI_INT, comm);
                MPI_Unpack(FaceBox, BoxTotalSize, &position, &(line4face[idx_rank][MAXLINE4FACE * idx]), MAXLINE4FACE, MPI_INT, comm);
                if (worlddim == 3)
                    MPI_Unpack(FaceBox, BoxTotalSize, &position, &(BoundaryID[idx_rank][idx]), 1, MPI_INT, comm);
            }
        }
        else
        {
            begin = ptwf4r[rank];
            end = ptwf4r[rank + 1];
            num = end - begin;
            facenum[idx_rank] = num;
        }
        if (worlddim == 3)
        {
            MPI_Unpack(FaceBox, BoxTotalSize, &position, &numrank, 1, MPI_INT, comm);
            neignum[idx_rank] = numrank;
            neigrank[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
            MPI_Unpack(FaceBox, BoxTotalSize, &position, neigrank[idx_rank], numrank, MPI_INT, comm);
            neigsharednum[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
            MPI_Unpack(FaceBox, BoxTotalSize, &position, neigsharednum[idx_rank], numrank, MPI_INT, comm);
            neigsharedindex[idx_rank] = (INT **)malloc(numrank * sizeof(INT *));
            for (idx = 0; idx < numrank; idx++)
            {
                neigsharedindex[idx_rank][idx] = (INT *)malloc(neigsharednum[idx_rank][idx] * sizeof(INT));
                MPI_Unpack(FaceBox, BoxTotalSize, &position, neigsharedindex[idx_rank][idx], neigsharednum[idx_rank][idx], MPI_INT, comm);
            }
        }
    }
    OpenPFEM_Free(FaceBox);

    if (worlddim == 3)
    {
        if (bridge != NULL)
        {
            /* 把bridge中的recv先填上全局编号 */
            INT orignum = bridge->OriginRankNum;
            INT *origrank = bridge->OriginRank;
            GEOBRIDGE *geobridge = bridge->GeoRecv;
            for (idx_rank = 0; idx_rank < orignum; idx_rank++)
            {
                if (facenum[origrank[idx_rank]])
                {
                    geobridge[idx_rank].FaceNum = facenum[origrank[idx_rank]];
                    geobridge[idx_rank].FaceIndex = (INT *)malloc(facenum[origrank[idx_rank]] * sizeof(INT));
                    if (origrank[idx_rank] != rank)
                    {
                        for (idx = 0; idx < facenum[origrank[idx_rank]]; idx++)
                        {
                            geobridge[idx_rank].FaceIndex[idx] = globalindex[origrank[idx_rank]][idx];
                        }
                    }
                    else
                    {
                        begin = ptwf4r[rank];
                        end = ptwf4r[rank + 1];
                        for (idx = begin; idx < end; idx++)
                        {
                            geobridge[idx_rank].FaceIndex[idx - begin] = oldfaces[f4r[idx]].Index;
                        }
                    }
                }
            }
        }
    }
    num = 0;
    for (idx = 0; idx < size; idx++)
    {
        num += facenum[idx];
    }
    FACE *faces, *face;
    FacesCreate(&faces, num);
    INT pos = 0, localindex, i;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            for (idx = 0; idx < facenum[idx_rank]; idx++)
            {
                face = &(faces[pos]);
                face->Index = globalindex[idx_rank][idx];
                memcpy(face->Vert4Face, &(vert4face[idx_rank][MAXVERT4FACE * idx]), MAXVERT4FACE * sizeof(INT));
                memcpy(face->Line4Face, &(line4face[idx_rank][MAXLINE4FACE * idx]), MAXLINE4FACE * sizeof(INT));
                if (worlddim == 3)
                    face->BD_ID = BoundaryID[idx_rank][idx];
                pos++;
            }
            OpenPFEM_Free(vert4face[idx_rank]);
            OpenPFEM_Free(line4face[idx_rank]);
            if (worlddim == 3)
                OpenPFEM_Free(BoundaryID[idx_rank]);
        }
        else
        {
            begin = ptwf4r[rank];
            end = ptwf4r[rank + 1];
            for (idx = begin; idx < end; idx++)
            {
                localindex = f4r[idx];
                face = &(faces[pos]);
                face->Index = oldfaces[localindex].Index;
                for (i = 0; i < MAXVERT4FACE; i++)
                {
                    face->Vert4Face[i] = oldverts[oldfaces[localindex].Vert4Face[i]].Index;
                }
                for (i = 0; i < MAXLINE4FACE; i++)
                {
                    face->Line4Face[i] = oldlines[oldfaces[localindex].Line4Face[i]].Index;
                }
                if (worlddim == 3)
                    face->BD_ID = oldfaces[localindex].BD_ID;
                pos++;
            }
        }
    }
    OpenPFEM_Free(vert4face);
    OpenPFEM_Free(line4face);
    if (worlddim == 3)
        OpenPFEM_Free(BoundaryID);

    newmesh->Faces = faces;
    newmesh->num_face = num;
    RebuildFaces(newmesh, GVertIndex, VertIndex, GLineIndex, LineIndex);

    if (worlddim == 3)
    {
        // 开始处理共享信息
        SHAREDINFO *sharedinfo = newmesh->SharedInfo;
        INT idy_rank, rankindex;
        INT *new_neigrank = sharedinfo->NeighborRanks;
        INT new_neignum = sharedinfo->NumNeighborRanks;
        /* 统计和每个邻居共享几个点 */
        SHAREDGEO *sharedface = (SHAREDGEO *)malloc(new_neignum * sizeof(SHAREDGEO));
        INT new_sharednum, neigindex, idz_rank, tmpindex, idy, tmpnum;
        for (idx_rank = 0; idx_rank < new_neignum; idx_rank++) // 对于每个邻居
        {
            new_sharednum = 0;                  // 从零个开始
            neigindex = new_neigrank[idx_rank]; // 邻居进程号
            for (idy_rank = 0; idy_rank < size; idy_rank++)
            {
                for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
                {
                    if (neigrank[idy_rank][idz_rank] == neigindex)
                    {
                        new_sharednum += neigsharednum[idy_rank][idz_rank]; // 先进行有重复统计
                    }
                }
            }
            if (new_sharednum == 0)
            {
                sharedface[idx_rank].Index = NULL;
                sharedface[idx_rank].Owner = NULL;
                sharedface[idx_rank].SharedNum = 0;
                sharedface[idx_rank].SharedIndex = NULL;
            }
            else
            {
                INT *new_sharedindex = (INT *)malloc(new_sharednum * sizeof(INT));
                memset(new_sharedindex, -1, new_sharednum * sizeof(INT));
                new_sharednum = 0;
                for (idy_rank = 0; idy_rank < size; idy_rank++)
                {
                    for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
                    {
                        if (neigrank[idy_rank][idz_rank] == neigindex)
                        {
                            for (idx = 0; idx < neigsharednum[idy_rank][idz_rank]; idx++)
                            {
                                new_sharedindex[new_sharednum] = neigsharedindex[idy_rank][idz_rank][idx]; // 先进行有重复统计
                                new_sharednum++;
                            }
                        }
                    }
                }
                QuickSort_Int(new_sharedindex, 0, new_sharednum - 1); // 进行一个排序
                tmpindex = -1;
                tmpnum = new_sharednum;
                for (idx = 0; idx < new_sharednum; idx++)
                {
                    if (new_sharedindex[idx] == tmpindex)
                    {
                        new_sharedindex[idx] = -1;
                        tmpnum--;
                    }
                    else
                    {
                        tmpindex = new_sharedindex[idx];
                    }
                } // 给重复的赋值-1，同时统计里减去重复的
                INT *actual_sharedindex = (INT *)malloc(tmpnum * sizeof(INT));
                tmpnum = 0;
                for (idx = 0; idx < new_sharednum; idx++)
                {
                    if (new_sharedindex[idx] != -1)
                    {
                        actual_sharedindex[tmpnum] = new_sharedindex[idx];
                        tmpnum++;
                    }
                } // 统计不重复的
                OpenPFEM_Free(new_sharedindex);
                sharedface[idx_rank].Index = actual_sharedindex;
                sharedface[idx_rank].Owner = NULL;
                sharedface[idx_rank].SharedNum = tmpnum;
                sharedface[idx_rank].SharedIndex = NULL;
            }
        }
        newmesh->SharedInfo->SharedFaces = sharedface;
        for (idx_rank = 0; idx_rank < size; idx_rank++)
        {
            for (idy_rank = 0; idy_rank < neignum[idx_rank]; idy_rank++)
            {
                OpenPFEM_Free(neigsharedindex[idx_rank][idy_rank]);
            }
            OpenPFEM_Free(neigrank[idx_rank]);
            OpenPFEM_Free(neigsharednum[idx_rank]);
            OpenPFEM_Free(neigsharedindex[idx_rank]);
        }
        OpenPFEM_Free(neignum);
        OpenPFEM_Free(neigrank);
        OpenPFEM_Free(neigsharednum);
        OpenPFEM_Free(neigsharedindex);
    }
}

// 建立面的信息, 处理重复面的对象，同时建立点、线的局部信息
void RebuildFaces(MESH *mesh, INT *GVertIndex, INT *VertIndex, INT *GLineIndex, INT *LineIndex)
{
    INT worlddim = mesh->worlddim;
    INT k, num_face = mesh->num_face;
    INT *FacePos = malloc(3 * num_face * sizeof(INT)); // 为了后面建立面上点、线的信息做准备
    INT pos = 0;
    if (worlddim == 3)
    { // 三维的时候才有重复的情况
        INT *GFaceIndex = malloc(num_face * sizeof(INT));
        for (k = 0; k < num_face; k++)
        {
            GFaceIndex[k] = mesh->Faces[k].Index;
            FacePos[k] = k;
        }
        // 对全局编号进行排序, 这样相同的面就会排在一起了，同时位置数列进行相应的移动
        MergeSortRecursion(GFaceIndex, FacePos, 0, num_face - 1);
        INT tmp = -1; // 记录在遍历的过程中目前的全局编号
        for (k = 0; k < num_face; k++)
        {
            if (GFaceIndex[k] != tmp)
            {
                // 找到了一个新的节点
                tmp = GFaceIndex[k]; // 记录目前新的节点编号
            }
            else
            {
                // 目前位置的节点跟前一个是一样的,那么就标记为需要删除的对象
                GFaceIndex[k] = -1; // 表示之后需要删除
            }                       // end if
        }                           // end for k: 至此已经消除了重复的节点对象
        // 然后将FacePos进行排序, 这样就可以知道每一个面是否是重复的了
        MergeSortRecursion(FacePos, GFaceIndex, 0, num_face - 1);
        for (k = 0; k < num_face; k++)
        {
            if (GFaceIndex[k] != -1)
            {
                // 表示第k个节点不是重复的，需要保存到pos的位置, 如果k>pos的时候需要进行复制
                if (k > pos)
                    mesh->Faces[pos] = mesh->Faces[k];
                pos++;
            }                 // end if
        }                     // end for k
        mesh->num_face = pos; // 获得没有重复的节点个数
        mesh->Faces = realloc(mesh->Faces, mesh->num_face * sizeof(FACE));
        // 释放内存空间
        free(GFaceIndex);
    }
    // 生成面上的点和线的局部编号
    INT i;
    num_face = mesh->num_face;
    INT *GVerts4Faces = malloc(3 * num_face * sizeof(INT));
    for (k = 0; k < num_face; k++)
    {
        pos = 3 * k;
        for (i = 0; i < 3; i++)
        {
            GVerts4Faces[pos + i] = mesh->Faces[k].Vert4Face[i];
            FacePos[pos + i] = pos + i;
        }
    }
    MergeSortRecursion(GVerts4Faces, FacePos, 0, 3 * num_face - 1);
    pos = 0;
    INT vertind = VertIndex[pos], Gvertind = GVertIndex[pos];
    INT end = 3 * num_face;
    for (k = 0; k < end; k++)
    {
        if (GVerts4Faces[k] == Gvertind)
        {
            // 得到目前全局编号与节点的编号相等的情况, 可以得到局部的节点编号
            GVerts4Faces[k] = vertind;
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行节点编号的更新
            pos++;
            vertind = VertIndex[pos];   // 得到新的节点编号
            Gvertind = GVertIndex[pos]; // 得到新的节点的全局编号
            GVerts4Faces[k] = vertind;
        } // end if
    }     // end for k=0:end
    MergeSortRecursion(FacePos, GVerts4Faces, 0, 3 * num_face - 1);
    for (k = 0; k < num_face; k++)
    {
        pos = 3 * k;
        for (i = 0; i < 3; i++)
        {
            mesh->Faces[k].Vert4Face[i] = GVerts4Faces[pos + i];
        }
    } // end for k
    // 下面来生成面上的线的信息
    INT *GLines4Faces = GVerts4Faces; // 只是改了一下名字而已
    for (k = 0; k < num_face; k++)
    {
        pos = 3 * k;
        for (i = 0; i < 3; i++)
        {
            GLines4Faces[pos + i] = mesh->Faces[k].Line4Face[i]; // 得到线的全局编号
            FacePos[pos + i] = pos + i;                          // 记录位置
        }                                                        // end for i
    }                                                            // end for k
    MergeSortRecursion(GLines4Faces, FacePos, 0, 3 * num_face - 1);
    pos = 0;
    INT lineind = LineIndex[pos], Glineind = GLineIndex[pos];
    for (k = 0; k < end; k++)
    {
        if (GLines4Faces[k] == Glineind)
        {
            // 得到目前全局编号与节点的编号相等的情况, 可以得到局部的节点编号
            GLines4Faces[k] = lineind;
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行节点编号的更新
            pos++;
            lineind = LineIndex[pos];   // 得到新的节点编号
            Glineind = GLineIndex[pos]; // 得到新的节点的全局编号
            GLines4Faces[k] = lineind;
        } // end if
    }     // end for k=0:end
    MergeSortRecursion(FacePos, GLines4Faces, 0, end - 1);
    for (k = 0; k < num_face; k++)
    {
        pos = 3 * k;
        for (i = 0; i < 3; i++)
        {
            mesh->Faces[k].Line4Face[i] = GLines4Faces[pos + i];
        }
    } // end for k
    free(GLines4Faces);
    free(FacePos);
} // end for rebuildfaces

void Face_Package(MESH *mesh, BRIDGE *bridge, PTW *Face4Rank, PTW *Face2Rank, char **FacePackage, INT totalsize)
{
    INT worlddim = mesh->worlddim;
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0;
    INT num, begin, end, begin2, end2;
    INT idx_rank, idx, idy, faceindex, rankindex;
    INT neignum, facenum, totalnum, tmp1, tmp2;

    INT *ptwf4r = Face4Rank->Ptw;
    INT *f4r = Face4Rank->Entries;
    INT *ptwf2r = Face2Rank->Ptw;
    INT *f2r = Face2Rank->Entries;
    VERT *verts = mesh->Verts, *vert;
    LINE *lines = mesh->Lines, *line;
    FACE *faces = mesh->Faces, *face;
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator2 = (INT *)malloc(size * sizeof(INT));

    if (bridge != NULL)
    {
        /* 计算一下bridge */
        INT destnum = bridge->DestinationRankNum;
        INT *destindex = bridge->DestinationRank;
        GEOBRIDGE *geobridge = bridge->GeoSend;
        num = 0;
        for (idx_rank = 0; idx_rank < destnum; idx_rank++)
        {
            begin = ptwf4r[destindex[idx_rank]];
            end = ptwf4r[destindex[idx_rank] + 1];
            num = end - begin;
            if (num != 0)
            {
                geobridge[idx_rank].FaceNum = num;
                geobridge[idx_rank].FaceIndex = (INT *)malloc(num * sizeof(INT));
                for (idx = begin; idx < end; idx++)
                {
                    geobridge[idx_rank].FaceIndex[idx - begin] = f4r[idx];
                }
            }
        }
    }

    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        neignum = 0;
        facenum = 0;
        totalnum = 0;
        begin = ptwf4r[idx_rank];
        end = ptwf4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        }
        for (idx = begin; idx < end; idx++)
        {
            faceindex = f4r[idx];
            face = &(faces[faceindex]);
            if (idx_rank != rank)
            {
                MPI_Pack(&(face->Index), 1, MPI_INT, package, totalsize, &position, comm);
                for (idy = 0; idy < MAXVERT4FACE; idy++)
                {
                    MPI_Pack(&(verts[face->Vert4Face[idy]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                }
                for (idy = 0; idy < MAXLINE4FACE; idy++)
                {
                    MPI_Pack(&(lines[face->Line4Face[idy]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                }
                if (worlddim == 3)
                    MPI_Pack(&(face->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
            }
            if (worlddim == 3)
            {
                begin2 = ptwf2r[faceindex];
                end2 = ptwf2r[faceindex + 1];
                for (idy = begin2; idy < end2; idy++)
                {
                    rankindex = f2r[idy];
                    if (rankindex != idx_rank)
                    {
                        if (rank_indicator[rankindex] == 0)
                        {
                            rank_indicator2[neignum] = rankindex;
                            neignum++;
                        }
                        rank_indicator[rankindex]++;
                        totalnum++;
                    }
                }
            }
        }
        if (worlddim == 3)
        {
            MPI_Pack(&neignum, 1, MPI_INT, package, totalsize, &position, comm);
            if (neignum != 0)
            {
                MPI_Pack(rank_indicator2, neignum, MPI_INT, package, totalsize, &position, comm);
                facenum = 0;
                for (idx = 0; idx < neignum; idx++)
                {
                    MPI_Pack(&(rank_indicator[rank_indicator2[idx]]), 1, MPI_INT, package, totalsize, &position, comm);
                    facenum += rank_indicator[rank_indicator2[idx]];
                }
                INT *faceinfo = (INT *)malloc(facenum * sizeof(INT));
                tmp1 = rank_indicator[rank_indicator2[0]];
                rank_indicator[rank_indicator2[0]] = 0;
                for (idx = 1; idx < neignum; idx++)
                {
                    tmp2 = rank_indicator[rank_indicator2[idx]];
                    rank_indicator[rank_indicator2[idx]] = rank_indicator[rank_indicator2[idx - 1]] + tmp1;
                    tmp1 = tmp2;
                }
                for (idx = begin; idx < end; idx++)
                {
                    faceindex = f4r[idx];
                    begin2 = ptwf2r[faceindex];
                    end2 = ptwf2r[faceindex + 1];
                    for (idy = begin2; idy < end2; idy++)
                    {
                        rankindex = f2r[idy];
                        if (rankindex != idx_rank)
                        {
                            faceinfo[rank_indicator[rankindex]] = faces[faceindex].Index;
                            rank_indicator[rankindex]++;
                        }
                    }
                }
                MPI_Pack(faceinfo, facenum, MPI_INT, package, totalsize, &position, comm);
                OpenPFEM_Free(faceinfo);
                memset(rank_indicator, 0, size * sizeof(INT));
            }
        }
    }
    OpenPFEM_Free(rank_indicator);
    OpenPFEM_Free(rank_indicator2);
    *FacePackage = package;
}

INT Face_PackageSize(MESH *mesh, PTW *Face4Rank, PTW *Face2Rank, INT **facecount, INT **facedispls)
{
    INT worlddim = mesh->worlddim;
    INT *ptwf4r = Face4Rank->Ptw;
    INT *f4r = Face4Rank->Entries;
    INT *ptwf2r = Face2Rank->Ptw;
    INT *f2r = Face2Rank->Entries;
    MPI_Comm comm = mesh->SharedInfo->neighborcomm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    INT *pack_size = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));

    INT idx_rank, idx, idy, num, end, begin, flag, totalnum;
    INT faceindex, rankindex, begin2, end2;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        flag = 0;
        totalnum = 0;
        begin = ptwf4r[idx_rank];
        end = ptwf4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            pack_size[idx_rank] += sizeof(INT);       // 面的个数
            pack_size[idx_rank] += num * sizeof(INT); // 全局编号
            if (worlddim == 3)
                pack_size[idx_rank] += num * sizeof(INT);            // BD_ID
            pack_size[idx_rank] += num * MAXVERT4FACE * sizeof(INT); // 顶点的全局编号
            pack_size[idx_rank] += num * MAXLINE4FACE * sizeof(INT); // 线的全局编号
        }
        if (worlddim == 3)
        {
            pack_size[idx_rank] += sizeof(INT); // 还在几个进程
            for (idx = begin; idx < end; idx++)
            {
                faceindex = f4r[idx];
                begin2 = ptwf2r[faceindex];
                end2 = ptwf2r[faceindex + 1];
                for (idy = begin2; idy < end2; idy++)
                {
                    rankindex = f2r[idy];
                    if (rankindex != idx_rank)
                    {
                        totalnum++;
                        if (rank_indicator[rankindex] == 0)
                        {
                            rank_indicator[rankindex] = 1;
                            flag++;
                        }
                    }
                }
            }
            pack_size[idx_rank] += (2 * flag + totalnum) * sizeof(INT);
            memset(rank_indicator, 0, size * sizeof(INT));
        }
    }

    INT *pack_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = pack_size[0];
    pack_displc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        pack_displc[idx] = pack_displc[idx - 1] + pack_size[idx - 1];
        totalsize += pack_size[idx];
    }

    OpenPFEM_Free(rank_indicator);
    *facecount = pack_size;
    *facedispls = pack_displc;
    return totalsize;
}

void LineBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Line4Rank, char *LineBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize,
                  INT *GVertIndex, INT *VertIndex)
{
    INT *linenum = (INT *)calloc(size, sizeof(INT));
    INT **globalindex = (INT **)malloc(size * sizeof(INT *));
    INT **vert4line = (INT **)malloc(size * sizeof(INT *));
    INT **BoundaryID = (INT **)malloc(size * sizeof(INT *));
    INT *neignum = (INT *)malloc(size * sizeof(INT));
    INT **neigrank = (INT **)malloc(size * sizeof(INT *));
    INT **neigsharednum = (INT **)malloc(size * sizeof(INT *));
    INT ***neigsharedindex = (INT ***)malloc(size * sizeof(INT **));

    VERT *oldverts = oldmesh->Verts;
    LINE *oldlines = oldmesh->Lines;

    MPI_Comm comm = newmesh->comm;
    INT position = 0, num, numrank;
    INT idx_rank, idx, begin, end;
    INT *ptwl4r = Line4Rank->Ptw;
    INT *l4r = Line4Rank->Entries;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            MPI_Unpack(LineBox, BoxTotalSize, &position, &num, 1, MPI_INT, comm);
            linenum[idx_rank] = num;
            globalindex[idx_rank] = (INT *)malloc(num * sizeof(INT));
            vert4line[idx_rank] = (INT *)malloc(2 * num * sizeof(INT *));
            BoundaryID[idx_rank] = (INT *)malloc(num * sizeof(INT));
            for (idx = 0; idx < num; idx++)
            {
                MPI_Unpack(LineBox, BoxTotalSize, &position, &(globalindex[idx_rank][idx]), 1, MPI_INT, comm);
                MPI_Unpack(LineBox, BoxTotalSize, &position, &(vert4line[idx_rank][2 * idx]), 2, MPI_INT, comm);
                MPI_Unpack(LineBox, BoxTotalSize, &position, &(BoundaryID[idx_rank][idx]), 1, MPI_INT, comm);
            }
        }
        else
        {
            begin = ptwl4r[rank];
            end = ptwl4r[rank + 1];
            num = end - begin;
            linenum[idx_rank] = num;
        }
        MPI_Unpack(LineBox, BoxTotalSize, &position, &numrank, 1, MPI_INT, comm);
        neignum[idx_rank] = numrank;
        neigrank[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
        MPI_Unpack(LineBox, BoxTotalSize, &position, neigrank[idx_rank], numrank, MPI_INT, comm);
        neigsharednum[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
        MPI_Unpack(LineBox, BoxTotalSize, &position, neigsharednum[idx_rank], numrank, MPI_INT, comm);
        neigsharedindex[idx_rank] = (INT **)malloc(numrank * sizeof(INT *));
        for (idx = 0; idx < numrank; idx++)
        {
            neigsharedindex[idx_rank][idx] = (INT *)malloc(neigsharednum[idx_rank][idx] * sizeof(INT));
            MPI_Unpack(LineBox, BoxTotalSize, &position, neigsharedindex[idx_rank][idx], neigsharednum[idx_rank][idx], MPI_INT, comm);
        }
    }
    OpenPFEM_Free(LineBox);

    if (bridge != NULL)
    {
        /* 把bridge中的recv先填上全局编号 */
        INT orignum = bridge->OriginRankNum;
        INT *origrank = bridge->OriginRank;
        GEOBRIDGE *geobridge = bridge->GeoRecv;
        for (idx_rank = 0; idx_rank < orignum; idx_rank++)
        {
            if (linenum[origrank[idx_rank]])
            {
                geobridge[idx_rank].LineNum = linenum[origrank[idx_rank]];
                geobridge[idx_rank].LineIndex = (INT *)malloc(linenum[origrank[idx_rank]] * sizeof(INT));
                if (origrank[idx_rank] != rank)
                {
                    for (idx = 0; idx < linenum[origrank[idx_rank]]; idx++)
                    {
                        geobridge[idx_rank].LineIndex[idx] = globalindex[origrank[idx_rank]][idx];
                    }
                }
                else
                {
                    begin = ptwl4r[rank];
                    end = ptwl4r[rank + 1];
                    for (idx = begin; idx < end; idx++)
                    {
                        geobridge[idx_rank].LineIndex[idx - begin] = oldlines[l4r[idx]].Index;
                    }
                }
            }
        }
    }

    num = 0;
    for (idx = 0; idx < size; idx++)
    {
        num += linenum[idx];
    }
    LINE *lines, *line;
    LinesCreate(&lines, num);
    INT pos = 0, localindex, i;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            for (idx = 0; idx < linenum[idx_rank]; idx++)
            {
                line = &(lines[pos]);
                line->Index = globalindex[idx_rank][idx];
                memcpy(line->Vert4Line, &(vert4line[idx_rank][2 * idx]), 2 * sizeof(INT));
                line->BD_ID = BoundaryID[idx_rank][idx];
                pos++;
            }
            OpenPFEM_Free(vert4line[idx_rank]);
            OpenPFEM_Free(BoundaryID[idx_rank]);
        }
        else
        {
            begin = ptwl4r[rank];
            end = ptwl4r[rank + 1];
            for (idx = begin; idx < end; idx++)
            {
                localindex = l4r[idx];
                line = &(lines[pos]);
                line->Index = oldlines[localindex].Index;
                line->Vert4Line[0] = oldverts[oldlines[localindex].Vert4Line[0]].Index;
                line->Vert4Line[1] = oldverts[oldlines[localindex].Vert4Line[1]].Index;
                line->BD_ID = oldlines[localindex].BD_ID;
                pos++;
            }
        }
    }
    OpenPFEM_Free(vert4line);
    OpenPFEM_Free(BoundaryID);
    newmesh->Lines = lines;
    newmesh->num_line = num;
    RebuildLines(newmesh, GVertIndex, VertIndex);

    // 开始处理共享信息
    SHAREDINFO *sharedinfo = newmesh->SharedInfo;
    INT idy_rank, rankindex;
    INT *new_neigrank = sharedinfo->NeighborRanks;
    INT new_neignum = sharedinfo->NumNeighborRanks;
    /* 统计和每个邻居共享几个点 */
    SHAREDGEO *sharedline = (SHAREDGEO *)malloc(new_neignum * sizeof(SHAREDGEO));
    INT new_sharednum, neigindex, idz_rank, tmpindex, idy, tmpnum;
    for (idx_rank = 0; idx_rank < new_neignum; idx_rank++) // 对于每个邻居
    {
        new_sharednum = 0;                  // 从零个开始
        neigindex = new_neigrank[idx_rank]; // 邻居进程号
        for (idy_rank = 0; idy_rank < size; idy_rank++)
        {
            for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
            {
                if (neigrank[idy_rank][idz_rank] == neigindex)
                {
                    new_sharednum += neigsharednum[idy_rank][idz_rank]; // 先进行有重复统计
                }
            }
        }
        if (new_sharednum == 0)
        {
            sharedline[idx_rank].Index = NULL;
            sharedline[idx_rank].Owner = NULL;
            sharedline[idx_rank].SharedNum = 0;
            sharedline[idx_rank].SharedIndex = NULL;
        }
        else
        {
            INT *new_sharedindex = (INT *)malloc(new_sharednum * sizeof(INT));
            memset(new_sharedindex, -1, new_sharednum * sizeof(INT));
            new_sharednum = 0;
            for (idy_rank = 0; idy_rank < size; idy_rank++)
            {
                for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
                {
                    if (neigrank[idy_rank][idz_rank] == neigindex)
                    {
                        for (idx = 0; idx < neigsharednum[idy_rank][idz_rank]; idx++)
                        {
                            new_sharedindex[new_sharednum] = neigsharedindex[idy_rank][idz_rank][idx]; // 先进行有重复统计
                            new_sharednum++;
                        }
                    }
                }
            }
            QuickSort_Int(new_sharedindex, 0, new_sharednum - 1); // 进行一个排序
            tmpindex = -1;
            tmpnum = new_sharednum;
            for (idx = 0; idx < new_sharednum; idx++)
            {
                if (new_sharedindex[idx] == tmpindex)
                {
                    new_sharedindex[idx] = -1;
                    tmpnum--;
                }
                else
                {
                    tmpindex = new_sharedindex[idx];
                }
            } // 给重复的赋值-1，同时统计里减去重复的
            INT *actual_sharedindex = (INT *)malloc(tmpnum * sizeof(INT));
            tmpnum = 0;
            for (idx = 0; idx < new_sharednum; idx++)
            {
                if (new_sharedindex[idx] != -1)
                {
                    actual_sharedindex[tmpnum] = new_sharedindex[idx];
                    tmpnum++;
                }
            } // 统计不重复的
            OpenPFEM_Free(new_sharedindex);
            sharedline[idx_rank].Index = actual_sharedindex;
            sharedline[idx_rank].Owner = NULL;
            sharedline[idx_rank].SharedNum = tmpnum;
            sharedline[idx_rank].SharedIndex = NULL;
        }
    }
    newmesh->SharedInfo->SharedLines = sharedline;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        for (idy_rank = 0; idy_rank < neignum[idx_rank]; idy_rank++)
        {
            OpenPFEM_Free(neigsharedindex[idx_rank][idy_rank]);
        }
        OpenPFEM_Free(neigrank[idx_rank]);
        OpenPFEM_Free(neigsharednum[idx_rank]);
        OpenPFEM_Free(neigsharedindex[idx_rank]);
    }
    OpenPFEM_Free(neignum);
    OpenPFEM_Free(neigrank);
    OpenPFEM_Free(neigsharednum);
    OpenPFEM_Free(neigsharedindex);
}

// 建立线的信息，消除重复的线，同时建立线上的点的局部编号
void RebuildLines(MESH *mesh, INT *GVertIndex, INT *VertIndex)
{
    INT k, num_line = mesh->num_line;
    INT *GLineIndex = malloc(num_line * sizeof(INT));
    INT *LinePos = malloc(2 * num_line * sizeof(INT));
    for (k = 0; k < num_line; k++)
    {
        GLineIndex[k] = mesh->Lines[k].Index;
        LinePos[k] = k;
    }
    // 对全局编号进行排序, 这样相同的节点就会排在一起了，同时位置数列进行相应的移动
    MergeSortRecursion(GLineIndex, LinePos, 0, num_line - 1);
    INT tmp = -1; // 记录在遍历的过程中目前的全局编号
    for (k = 0; k < num_line; k++)
    {
        if (GLineIndex[k] != tmp)
        {
            // 找到了一个新的节点
            tmp = GLineIndex[k]; // 记录目前新的节点编号
                                 //  newnum_line++;        //非重复的节点个数加1
        }
        else
        {
            // 目前位置的节点跟前一个是一样的,那么就标记为需要删除的对象
            GLineIndex[k] = -1; // 表示之后需要删除
        }                       // end if
    }                           // end for k: 至此已经消除了重复的节点对象
    // 然后将LinePos进行排序, 这样就可以知道每一个线是否是重复的了
    // 重新排序得到每条线是否需要保存下来
    MergeSortRecursion(LinePos, GLineIndex, 0, num_line - 1);
    // if (rank == 0)
    // {
    //     for (k = 0; k < num_line; k++)
    //     {
    //         printf("after: rank: %d, GLineIndex[%d]: %d, LinePos[%d]: %d\n",
    //                rank, k, GLineIndex[k], k, LinePos[k]);
    //     }
    // }
    INT pos = 0;
    for (k = 0; k < num_line; k++)
    {
        if (GLineIndex[k] != -1)
        {
            // 表示第k个节点不是重复的，需要保存到pos的位置, 如果k>pos的时候需要进行复制
            if (k > pos)
                mesh->Lines[pos] = mesh->Lines[k];
            pos++;
        }                 // end if
    }                     // end for k
    mesh->num_line = pos; // 获得没有重复的节点个数
    mesh->Lines = realloc(mesh->Lines, mesh->num_line * sizeof(LINE));
    // 释放内存空间
    free(GLineIndex);
    // 生成每条线上节点的局部信息
    num_line = mesh->num_line;
    INT i, *GVerts4Lines = malloc(2 * num_line * sizeof(INT));
    for (k = 0; k < num_line; k++)
    {
        pos = 2 * k;
        for (i = 0; i < 2; i++)
        {
            GVerts4Lines[pos + i] = mesh->Lines[k].Vert4Line[i];
            LinePos[pos + i] = pos + i;
            // printf("rank %d 1st GVerts4Lines[%d]: %d\n", rank, pos + i, GVerts4Lines[pos + i]);
        }
    }
    // 对GVerts4Lines进行排序, 同时移动LinePos
    MergeSortRecursion(GVerts4Lines, LinePos, 0, 2 * num_line - 1);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // if (rank == 0)
    // {
    //     for (k = 0; k < num_line; k++)
    //     {
    //         pos = 2 * k;
    //         printf("the %d-th line: GVerts:[%d, %d]\n", k, GVerts4Lines[pos], GVerts4Lines[pos + 1]);
    //     }
    // }

    pos = 0;
    INT vertind = VertIndex[pos], Gvertind = GVertIndex[pos];
    INT end = 2 * num_line;
    for (k = 0; k < end; k++)
    {
        if (GVerts4Lines[k] == Gvertind)
        {
            // 得到目前全局编号与节点的编号相等的情况, 可以得到局部的节点编号
            GVerts4Lines[k] = vertind;
            // printf("rank %d 2nd GVerts4Lines[%d]: %d\n", rank, k, GVerts4Lines[k]);
        }
        else
        {
            // 得到了一个新的全局编号, 需要进行节点编号的更新
            do
            {
                pos++;
                vertind = VertIndex[pos];   // 得到新的节点编号
                Gvertind = GVertIndex[pos]; // 得到新的节点的全局编号
            } while (GVerts4Lines[k] != Gvertind);
            GVerts4Lines[k] = vertind;
            // printf("rank %d 3rd GVerts4Lines[%d]: %d\n", rank, k, GVerts4Lines[k]);
        } // end if
    }     // end for k=0:num_line
    // 这样GVerts4Lines存储了节点的局部编号
    MergeSortRecursion(LinePos, GVerts4Lines, 0, 2 * num_line - 1);
    for (k = 0; k < num_line; k++)
    {
        pos = 2 * k;
        mesh->Lines[k].Vert4Line[0] = GVerts4Lines[pos];
        mesh->Lines[k].Vert4Line[1] = GVerts4Lines[pos + 1];
    } // 至此得到了线的节点的局部编号
} // end for rebuildlines

void Line_Package(MESH *mesh, BRIDGE *bridge, PTW *Line4Rank, PTW *Line2Rank, char **LinePackage, INT totalsize)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0;
    INT num, begin, end, begin2, end2;
    INT idx_rank, idx, idy, lineindex, rankindex;
    INT neignum, linenum, totalnum, tmp1, tmp2;

    INT *ptwl4r = Line4Rank->Ptw;
    INT *l4r = Line4Rank->Entries;
    INT *ptwl2r = Line2Rank->Ptw;
    INT *l2r = Line2Rank->Entries;
    VERT *verts = mesh->Verts, *vert;
    LINE *lines = mesh->Lines, *line;
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator2 = (INT *)malloc(size * sizeof(INT));

    if (bridge != NULL)
    {
        /* 计算一下bridge */
        INT destnum = bridge->DestinationRankNum;
        INT *destindex = bridge->DestinationRank;
        GEOBRIDGE *geobridge = bridge->GeoSend;
        num = 0;
        for (idx_rank = 0; idx_rank < destnum; idx_rank++)
        {
            begin = ptwl4r[destindex[idx_rank]];
            end = ptwl4r[destindex[idx_rank] + 1];
            num = end - begin;
            if (num != 0)
            {
                geobridge[idx_rank].LineNum = num;
                geobridge[idx_rank].LineIndex = (INT *)malloc(num * sizeof(INT));
                for (idx = begin; idx < end; idx++)
                {
                    geobridge[idx_rank].LineIndex[idx - begin] = l4r[idx];
                }
            }
        }
    }

    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        neignum = 0;
        linenum = 0;
        totalnum = 0;
        begin = ptwl4r[idx_rank];
        end = ptwl4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        }
        for (idx = begin; idx < end; idx++)
        {
            lineindex = l4r[idx];
            line = &(lines[lineindex]);
            if (idx_rank != rank)
            {
                MPI_Pack(&(line->Index), 1, MPI_INT, package, totalsize, &position, comm);
                MPI_Pack(&(verts[line->Vert4Line[0]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                MPI_Pack(&(verts[line->Vert4Line[1]].Index), 1, MPI_INT, package, totalsize, &position, comm);
                MPI_Pack(&(line->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
            }
            begin2 = ptwl2r[lineindex];
            end2 = ptwl2r[lineindex + 1];
            for (idy = begin2; idy < end2; idy++)
            {
                rankindex = l2r[idy];
                if (rankindex != idx_rank)
                {
                    if (rank_indicator[rankindex] == 0)
                    {
                        rank_indicator2[neignum] = rankindex;
                        neignum++;
                    }
                    rank_indicator[rankindex]++;
                    totalnum++;
                }
            }
        }
        MPI_Pack(&neignum, 1, MPI_INT, package, totalsize, &position, comm);
        if (neignum != 0)
        {
            MPI_Pack(rank_indicator2, neignum, MPI_INT, package, totalsize, &position, comm);
            linenum = 0;
            for (idx = 0; idx < neignum; idx++)
            {
                MPI_Pack(&(rank_indicator[rank_indicator2[idx]]), 1, MPI_INT, package, totalsize, &position, comm);
                linenum += rank_indicator[rank_indicator2[idx]];
            }
            INT *lineinfo = (INT *)malloc(linenum * sizeof(INT));
            tmp1 = rank_indicator[rank_indicator2[0]];
            rank_indicator[rank_indicator2[0]] = 0;
            for (idx = 1; idx < neignum; idx++)
            {
                tmp2 = rank_indicator[rank_indicator2[idx]];
                rank_indicator[rank_indicator2[idx]] = rank_indicator[rank_indicator2[idx - 1]] + tmp1;
                tmp1 = tmp2;
            }
            for (idx = begin; idx < end; idx++)
            {
                lineindex = l4r[idx];
                begin2 = ptwl2r[lineindex];
                end2 = ptwl2r[lineindex + 1];
                for (idy = begin2; idy < end2; idy++)
                {
                    rankindex = l2r[idy];
                    if (rankindex != idx_rank)
                    {
                        lineinfo[rank_indicator[rankindex]] = lines[lineindex].Index;
                        rank_indicator[rankindex]++;
                    }
                }
            }
            MPI_Pack(lineinfo, linenum, MPI_INT, package, totalsize, &position, comm);
            OpenPFEM_Free(lineinfo);
            memset(rank_indicator, 0, size * sizeof(INT));
        }
    }
    OpenPFEM_Free(rank_indicator);
    OpenPFEM_Free(rank_indicator2);
    *LinePackage = package;
}

INT Line_PackageSize(MESH *mesh, PTW *Line4Rank, PTW *Line2Rank, INT **linecount, INT **linedispls)
{
    INT *ptwl4r = Line4Rank->Ptw;
    INT *l4r = Line4Rank->Entries;
    INT *ptwl2r = Line2Rank->Ptw;
    INT *l2r = Line2Rank->Entries;
    MPI_Comm comm = mesh->SharedInfo->neighborcomm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    INT *pack_size = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));

    INT idx_rank, idx, idy, num, end, begin, flag, totalnum;
    INT lineindex, rankindex, begin2, end2;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        flag = 0;
        totalnum = 0;
        begin = ptwl4r[idx_rank];
        end = ptwl4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            pack_size[idx_rank] += sizeof(INT);           // 线的个数
            pack_size[idx_rank] += 2 * num * sizeof(INT); // 全局编号和BD_ID
            pack_size[idx_rank] += num * 2 * sizeof(INT); // 顶点的全局编号
        }
        pack_size[idx_rank] += sizeof(INT); // 还在几个进程
        for (idx = begin; idx < end; idx++)
        {
            lineindex = l4r[idx];
            begin2 = ptwl2r[lineindex];
            end2 = ptwl2r[lineindex + 1];
            for (idy = begin2; idy < end2; idy++)
            {
                rankindex = l2r[idy];
                if (rankindex != idx_rank)
                {
                    totalnum++;
                    if (rank_indicator[rankindex] == 0)
                    {
                        rank_indicator[rankindex] = 1;
                        flag++;
                    }
                }
            }
        }
        pack_size[idx_rank] += (2 * flag + totalnum) * sizeof(INT);
        memset(rank_indicator, 0, size * sizeof(INT));
    }

    INT *pack_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = pack_size[0];
    pack_displc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        pack_displc[idx] = pack_displc[idx - 1] + pack_size[idx - 1];
        totalsize += pack_size[idx];
    }

    OpenPFEM_Free(rank_indicator);
    *linecount = pack_size;
    *linedispls = pack_displc;
    return totalsize;
}

void VertBox2Mesh(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge, PTW *Vert4Rank, char *VertBox, INT *BoxSize, INT *BoxDisplc, INT BoxTotalSize)
{
    INT worlddim = oldmesh->worlddim;
    INT *vertnum = (INT *)calloc(size, sizeof(INT));
    INT **globalindex = (INT **)malloc(size * sizeof(INT *));
    DOUBLE **coord = (DOUBLE **)malloc(size * sizeof(DOUBLE *));
    INT **BoundaryID = (INT **)malloc(size * sizeof(INT *));
    INT *neignum = (INT *)malloc(size * sizeof(INT));
    INT **neigrank = (INT **)malloc(size * sizeof(INT *));
    INT **neigsharednum = (INT **)malloc(size * sizeof(INT *));
    INT ***neigsharedindex = (INT ***)malloc(size * sizeof(INT **));

    VERT *oldverts = oldmesh->Verts;

    MPI_Comm comm = newmesh->comm;
    INT position = 0, num, numrank;
    INT idx_rank, idx, begin, end;
    INT *ptwv4r = Vert4Rank->Ptw;
    INT *v4r = Vert4Rank->Entries;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            MPI_Unpack(VertBox, BoxTotalSize, &position, &num, 1, MPI_INT, comm);
            vertnum[idx_rank] = num;
            globalindex[idx_rank] = (INT *)malloc(num * sizeof(INT));
            coord[idx_rank] = (DOUBLE *)malloc(worlddim * num * sizeof(DOUBLE *));
            BoundaryID[idx_rank] = (INT *)malloc(num * sizeof(INT));
            for (idx = 0; idx < num; idx++)
            {
                MPI_Unpack(VertBox, BoxTotalSize, &position, &(globalindex[idx_rank][idx]), 1, MPI_INT, comm);
                MPI_Unpack(VertBox, BoxTotalSize, &position, &(coord[idx_rank][worlddim * idx]), worlddim, MPI_DOUBLE, comm);
                MPI_Unpack(VertBox, BoxTotalSize, &position, &(BoundaryID[idx_rank][idx]), 1, MPI_INT, comm);
            }
        }
        else
        {
            begin = ptwv4r[rank];
            end = ptwv4r[rank + 1];
            num = end - begin;
            vertnum[idx_rank] = num;
        }
        MPI_Unpack(VertBox, BoxTotalSize, &position, &numrank, 1, MPI_INT, comm);
        neignum[idx_rank] = numrank;
        neigrank[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
        MPI_Unpack(VertBox, BoxTotalSize, &position, neigrank[idx_rank], numrank, MPI_INT, comm);
        neigsharednum[idx_rank] = (INT *)malloc(numrank * sizeof(INT));
        MPI_Unpack(VertBox, BoxTotalSize, &position, neigsharednum[idx_rank], numrank, MPI_INT, comm);
        neigsharedindex[idx_rank] = (INT **)malloc(numrank * sizeof(INT *));
        for (idx = 0; idx < numrank; idx++)
        {
            neigsharedindex[idx_rank][idx] = (INT *)malloc(neigsharednum[idx_rank][idx] * sizeof(INT));
            MPI_Unpack(VertBox, BoxTotalSize, &position, neigsharedindex[idx_rank][idx], neigsharednum[idx_rank][idx], MPI_INT, comm);
        }
    }
    OpenPFEM_Free(VertBox);

    if (bridge != NULL)
    {
        /* 把bridge中的recv先填上全局编号 */
        INT orignum = 0;
        for (idx_rank = 0; idx_rank < size; idx_rank++)
        {
            if (vertnum[idx_rank])
            {
                orignum++;
            }
        }
        bridge->OriginRankNum = orignum;
        INT *origrank = (INT *)malloc(orignum * sizeof(INT));
        RecvGeoBridgeCreate(bridge, orignum);
        GEOBRIDGE *geobridge = bridge->GeoRecv;
        orignum = 0;
        for (idx_rank = 0; idx_rank < size; idx_rank++)
        {
            if (vertnum[idx_rank])
            {
                origrank[orignum] = idx_rank;
                geobridge[orignum].VertNum = vertnum[idx_rank];
                geobridge[orignum].VertIndex = (INT *)malloc(vertnum[idx_rank] * sizeof(INT));
                if (idx_rank != rank)
                {
                    for (idx = 0; idx < vertnum[idx_rank]; idx++)
                    {
                        geobridge[orignum].VertIndex[idx] = globalindex[idx_rank][idx];
                    }
                }
                else
                {
                    begin = ptwv4r[rank];
                    end = ptwv4r[rank + 1];
                    for (idx = begin; idx < end; idx++)
                    {
                        geobridge[orignum].VertIndex[idx - begin] = oldverts[v4r[idx]].Index;
                    }
                }
                orignum++;
            }
        }
        bridge->OriginRank = origrank;
    }

    num = 0;
    for (idx = 0; idx < size; idx++)
    {
        num += vertnum[idx];
    }
    VERT *verts, *vert;
    VertsCreate(&verts, num, 3);
    INT pos = 0, localindex;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (idx_rank != rank)
        {
            for (idx = 0; idx < vertnum[idx_rank]; idx++)
            {
                vert = &(verts[pos]);
                vert->Index = globalindex[idx_rank][idx];
                memcpy(vert->Coord, &(coord[idx_rank][worlddim * idx]), worlddim * sizeof(DOUBLE));
                vert->BD_ID = BoundaryID[idx_rank][idx];
                pos++;
            }
            OpenPFEM_Free(globalindex[idx_rank]);
            OpenPFEM_Free(coord[idx_rank]);
            OpenPFEM_Free(BoundaryID[idx_rank]);
        }
        else
        {
            begin = ptwv4r[rank];
            end = ptwv4r[rank + 1];
            for (idx = begin; idx < end; idx++)
            {
                localindex = v4r[idx];
                vert = &(verts[pos]);
                vert->Index = oldverts[localindex].Index;
                memcpy(vert->Coord, oldverts[localindex].Coord, worlddim * sizeof(DOUBLE));
                vert->BD_ID = oldverts[localindex].BD_ID;
                pos++;
            }
        }
    }
    OpenPFEM_Free(globalindex);
    OpenPFEM_Free(coord);
    OpenPFEM_Free(BoundaryID);
    newmesh->Verts = verts;
    newmesh->num_vert = num;
    RebuildVerts(newmesh);

    // 开始处理共享信息
    if (newmesh->SharedInfo == NULL)
    {
        SharedInfoCreate(newmesh);
    }
    bool *rankindicator = (bool *)calloc(size, sizeof(bool));
    INT idy_rank, rankindex;
    /* 统计收到的信息里不重复的邻居进程号 */
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        for (idy_rank = 0; idy_rank < neignum[idx_rank]; idy_rank++)
        {
            rankindex = neigrank[idx_rank][idy_rank];
            rankindicator[rankindex] = 1;
        }
    }
    INT new_neignum = 0;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (rankindicator[idx_rank])
        {
            new_neignum++;
        }
    }
    INT *new_neigrank = (INT *)malloc(new_neignum * sizeof(INT));
    new_neignum = 0;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        if (rankindicator[idx_rank])
        {
            new_neigrank[new_neignum] = idx_rank;
            new_neignum++;
        }
    }
    OpenPFEM_Free(rankindicator);
    /* 创建sharedinfo并更新邻居个数and进程号 */
    SHAREDINFO *sharedinfo = newmesh->SharedInfo;
    sharedinfo->NumNeighborRanks = new_neignum;
    sharedinfo->NeighborRanks = new_neigrank;
    /* 统计和每个邻居共享几个点 */
    SHAREDGEO *sharedvert = (SHAREDGEO *)malloc(new_neignum * sizeof(SHAREDGEO));
    INT new_sharednum, neigindex, idz_rank, tmpindex, idy, tmpnum;
    for (idx_rank = 0; idx_rank < new_neignum; idx_rank++) // 对于每个邻居
    {
        new_sharednum = 0;                  // 从零个开始
        neigindex = new_neigrank[idx_rank]; // 邻居进程号
        for (idy_rank = 0; idy_rank < size; idy_rank++)
        {
            for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
            {
                if (neigrank[idy_rank][idz_rank] == neigindex)
                {
                    new_sharednum += neigsharednum[idy_rank][idz_rank]; // 先进行有重复统计
                }
            }
        }
        INT *new_sharedindex = (INT *)malloc(new_sharednum * sizeof(INT));
        memset(new_sharedindex, -1, new_sharednum * sizeof(INT));
        new_sharednum = 0;
        for (idy_rank = 0; idy_rank < size; idy_rank++)
        {
            for (idz_rank = 0; idz_rank < neignum[idy_rank]; idz_rank++)
            {
                if (neigrank[idy_rank][idz_rank] == neigindex)
                {
                    for (idx = 0; idx < neigsharednum[idy_rank][idz_rank]; idx++)
                    {
                        new_sharedindex[new_sharednum] = neigsharedindex[idy_rank][idz_rank][idx]; // 先进行有重复统计
                        new_sharednum++;
                    }
                }
            }
        }
        QuickSort_Int(new_sharedindex, 0, new_sharednum - 1); // 进行一个排序
        tmpindex = -1;
        tmpnum = new_sharednum;
        for (idx = 0; idx < new_sharednum; idx++)
        {
            if (new_sharedindex[idx] == tmpindex)
            {
                new_sharedindex[idx] = -1;
                tmpnum--;
            }
            else
            {
                tmpindex = new_sharedindex[idx];
            }
        } // 给重复的赋值-1，同时统计里减去重复的
        INT *actual_sharedindex = (INT *)malloc(tmpnum * sizeof(INT));
        tmpnum = 0;
        for (idx = 0; idx < new_sharednum; idx++)
        {
            if (new_sharedindex[idx] != -1)
            {
                actual_sharedindex[tmpnum] = new_sharedindex[idx];
                tmpnum++;
            }
        } // 统计不重复的
        OpenPFEM_Free(new_sharedindex);
        sharedvert[idx_rank].Index = actual_sharedindex;
        sharedvert[idx_rank].Owner = NULL;
        sharedvert[idx_rank].SharedNum = tmpnum;
        sharedvert[idx_rank].SharedIndex = NULL;
    }
    newmesh->SharedInfo->SharedVerts = sharedvert;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        for (idy_rank = 0; idy_rank < neignum[idx_rank]; idy_rank++)
        {
            OpenPFEM_Free(neigsharedindex[idx_rank][idy_rank]);
        }
        OpenPFEM_Free(neigrank[idx_rank]);
        OpenPFEM_Free(neigsharednum[idx_rank]);
        OpenPFEM_Free(neigsharedindex[idx_rank]);
    }
    OpenPFEM_Free(neignum);
    OpenPFEM_Free(neigrank);
    OpenPFEM_Free(neigsharednum);
    OpenPFEM_Free(neigsharedindex);
}

// 建立网格的节点的信息, 消除重复的节点
void RebuildVerts(MESH *mesh)
{
    INT k, num_vert = mesh->num_vert;
    INT *GVertIndex = malloc(num_vert * sizeof(INT)); // 记录每个节点的全局编号
    INT *VertPos = malloc(num_vert * sizeof(INT));    // 用来记录节点所在的位置
    for (k = 0; k < num_vert; k++)
    {
        GVertIndex[k] = mesh->Verts[k].Index; // 赋给全局编号
        VertPos[k] = k;                       // 记录节点的位置
    }
    // 对全局编号进行排序, 这样相同的节点就会排在一起了，同时位置数列进行相应的移动
    MergeSortRecursion(GVertIndex, VertPos, 0, num_vert - 1);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    INT tmp = -1; // 记录在遍历的过程中目前的全局编号
    for (k = 0; k < num_vert; k++)
    {
        if (GVertIndex[k] != tmp)
        {
            // 找到了一个新的节点
            tmp = GVertIndex[k]; // 记录目前新的节点编号
                                 //  newnum_vert++;        //非重复的节点个数加1
        }
        else
        {
            // 目前位置的节点跟前一个是一样的,那么就标记为需要删除的对象
            GVertIndex[k] = -1; // 表示之后需要删除
        }                       // end if
    }                           // end for k: 至此已经消除了重复的节点对象
    // 然后将VertPos进行排序, 这样就可以知道每一个节点是否是重复的节点了
    // 新的节点个数为newnum_vert
    // 对VertPos进行排序, 重新得到每个节点是否将会保存下来
    MergeSortRecursion(VertPos, GVertIndex, 0, num_vert - 1);
    INT pos = 0;
    for (k = 0; k < num_vert; k++)
    {
        if (GVertIndex[k] != -1)
        {
            // 表示第k个节点不是重复的，需要保存到pos的位置, 如果k>pos的时候需要进行复制
            if (k > pos)
                mesh->Verts[pos] = mesh->Verts[k];
            pos++;
        }                 // end if
    }                     // end for k
    mesh->num_vert = pos; // 获得没有重复的节点个数
    mesh->Verts = realloc(mesh->Verts, mesh->num_vert * sizeof(VERT));
    // 释放内存空间
    free(GVertIndex);
    free(VertPos);
}

void Vert_Package(MESH *mesh, BRIDGE *bridge, PTW *Vert4Rank, PTW *Vert2Rank, char **VertPackage, INT totalsize)
{
    INT worlddim = mesh->worlddim;
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0;
    INT num, begin, end, begin2, end2;
    INT idx_rank, idx, idy, vertindex, rankindex;
    INT neignum, vertnum, totalnum, tmp1, tmp2;

    INT *ptwv4r = Vert4Rank->Ptw;
    INT *v4r = Vert4Rank->Entries;
    INT *ptwv2r = Vert2Rank->Ptw;
    INT *v2r = Vert2Rank->Entries;
    VERT *verts = mesh->Verts, *vert;
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator2 = (INT *)malloc(size * sizeof(INT));

    if (bridge != NULL)
    {
        /* 计算一下bridge */
        INT *bridgeindicator = (INT *)calloc(size, sizeof(INT)); // 记录发往了哪几个进程，每个几个点
        INT destnum = 0;
        for (idx_rank = 0; idx_rank < size; idx_rank++)
        {
            begin = ptwv4r[idx_rank];
            end = ptwv4r[idx_rank + 1];
            num = end - begin;
            if (num != 0)
            {
                destnum++;
                bridgeindicator[idx_rank] = num;
            }
        }
        bridge->DestinationRankNum = destnum;
        INT *destrank = (INT *)malloc(destnum * sizeof(INT));
        SendGeoBridgeCreate(bridge, destnum);
        GEOBRIDGE *geobridge = bridge->GeoSend;
        num = 0;
        for (idx_rank = 0; idx_rank < size; idx_rank++)
        {
            if (bridgeindicator[idx_rank])
            {
                destrank[num] = idx_rank;
                geobridge[num].VertNum = bridgeindicator[idx_rank];
                geobridge[num].VertIndex = (INT *)malloc(bridgeindicator[idx_rank] * sizeof(INT));
                begin = ptwv4r[idx_rank];
                end = ptwv4r[idx_rank + 1];
                for (idx = begin; idx < end; idx++)
                {
                    geobridge[num].VertIndex[idx - begin] = v4r[idx];
                }
                num++;
            }
        }
        bridge->DestinationRank = destrank;
        OpenPFEM_Free(bridgeindicator);
    }

    /*计算要发出去的数据*/
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        neignum = 0;
        vertnum = 0;
        totalnum = 0;
        begin = ptwv4r[idx_rank];
        end = ptwv4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        }
        for (idx = begin; idx < end; idx++)
        {
            vertindex = v4r[idx];
            vert = &(verts[vertindex]);
            if (idx_rank != rank)
            {
                MPI_Pack(&(vert->Index), 1, MPI_INT, package, totalsize, &position, comm);
                MPI_Pack(vert->Coord, worlddim, MPI_DOUBLE, package, totalsize, &position, comm);
                MPI_Pack(&(vert->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
            }
            begin2 = ptwv2r[vertindex];
            end2 = ptwv2r[vertindex + 1];
            for (idy = begin2; idy < end2; idy++)
            {
                rankindex = v2r[idy];
                if (rankindex != idx_rank)
                {
                    if (rank_indicator[rankindex] == 0)
                    {
                        rank_indicator2[neignum] = rankindex;
                        neignum++;
                    }
                    rank_indicator[rankindex]++;
                    totalnum++;
                }
            }
        }
        MPI_Pack(&neignum, 1, MPI_INT, package, totalsize, &position, comm);
        if (neignum != 0)
        {
            MPI_Pack(rank_indicator2, neignum, MPI_INT, package, totalsize, &position, comm);
            vertnum = 0;
            for (idx = 0; idx < neignum; idx++)
            {
                MPI_Pack(&(rank_indicator[rank_indicator2[idx]]), 1, MPI_INT, package, totalsize, &position, comm);
                vertnum += rank_indicator[rank_indicator2[idx]];
            }
            INT *vertinfo = (INT *)malloc(vertnum * sizeof(INT));
            tmp1 = rank_indicator[rank_indicator2[0]];
            rank_indicator[rank_indicator2[0]] = 0;
            for (idx = 1; idx < neignum; idx++)
            {
                tmp2 = rank_indicator[rank_indicator2[idx]];
                rank_indicator[rank_indicator2[idx]] = rank_indicator[rank_indicator2[idx - 1]] + tmp1;
                tmp1 = tmp2;
            }
            for (idx = begin; idx < end; idx++)
            {
                vertindex = v4r[idx];
                begin2 = ptwv2r[vertindex];
                end2 = ptwv2r[vertindex + 1];
                for (idy = begin2; idy < end2; idy++)
                {
                    rankindex = v2r[idy];
                    if (rankindex != idx_rank)
                    {
                        vertinfo[rank_indicator[rankindex]] = verts[vertindex].Index;
                        rank_indicator[rankindex]++;
                    }
                }
            }
            MPI_Pack(vertinfo, vertnum, MPI_INT, package, totalsize, &position, comm);
            OpenPFEM_Free(vertinfo);
            memset(rank_indicator, 0, size * sizeof(INT));
        }
    }
    OpenPFEM_Free(rank_indicator);
    OpenPFEM_Free(rank_indicator2);
    *VertPackage = package;
}

INT Vert_PackageSize(MESH *mesh, PTW *Vert4Rank, PTW *Vert2Rank, INT **vertcount, INT **vertdispls)
{
    INT worlddim = mesh->worlddim;
    INT *ptwv4r = Vert4Rank->Ptw;
    INT *v4r = Vert4Rank->Entries;
    INT *ptwv2r = Vert2Rank->Ptw;
    INT *v2r = Vert2Rank->Entries;
    MPI_Comm comm = mesh->SharedInfo->neighborcomm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    INT *pack_size = (INT *)calloc(size, sizeof(INT));
    INT *rank_indicator = (INT *)calloc(size, sizeof(INT));

    INT idx_rank, idx, idy, num, end, begin, flag, totalnum;
    INT vertindex, rankindex, begin2, end2;
    for (idx_rank = 0; idx_rank < size; idx_rank++)
    {
        flag = 0;
        totalnum = 0;
        begin = ptwv4r[idx_rank];
        end = ptwv4r[idx_rank + 1];
        num = end - begin;
        if (idx_rank != rank)
        {
            pack_size[idx_rank] += sizeof(INT);                     // 点的个数
            pack_size[idx_rank] += 2 * num * sizeof(INT);           // 全局编号和BD_ID
            pack_size[idx_rank] += num * worlddim * sizeof(DOUBLE); // 点的坐标
        }
        pack_size[idx_rank] += sizeof(INT); // 还在几个进程
        for (idx = begin; idx < end; idx++)
        {
            vertindex = v4r[idx];
            begin2 = ptwv2r[vertindex];
            end2 = ptwv2r[vertindex + 1];
            for (idy = begin2; idy < end2; idy++)
            {
                rankindex = v2r[idy];
                if (rankindex != idx_rank)
                {
                    totalnum++;
                    if (rank_indicator[rankindex] == 0)
                    {
                        rank_indicator[rankindex] = 1;
                        flag++;
                    }
                }
            }
        }
        pack_size[idx_rank] += (2 * flag + totalnum) * sizeof(INT);
        memset(rank_indicator, 0, size * sizeof(INT));
    }

    INT *pack_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = pack_size[0];
    pack_displc[0] = 0;
    for (idx = 1; idx < size; idx++)
    {
        pack_displc[idx] = pack_displc[idx - 1] + pack_size[idx - 1];
        totalsize += pack_size[idx];
    }

    OpenPFEM_Free(rank_indicator);
    *vertcount = pack_size;
    *vertdispls = pack_displc;
    return totalsize;
}

void SharedGeoReRank(MESH *mesh, PTW *Geo2Ranks, INT dim)
{
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    SHAREDGEO *sharedgeo = NULL;
    INT neignum = sharedinfo->NumNeighborRanks;
    INT geonum = 0;
    switch (dim)
    {
    case VERTDATA:
        sharedgeo = sharedinfo->SharedVerts;
        geonum = mesh->num_vert;
        break;
    case LINEDATA:
        sharedgeo = sharedinfo->SharedLines;
        geonum = mesh->num_line;
        break;
    case FACEDATA:
        sharedgeo = sharedinfo->SharedFaces;
        geonum = mesh->num_face;
        break;
    case VOLUDATA:
        sharedgeo = sharedinfo->SharedVolus;
        geonum = mesh->num_volu;
        break;
    }
    /*-----------------------------------------------*/
    PTW *Geo2RanksPack = (PTW *)malloc(neignum * sizeof(PTW));
    PTW *PtwTmp = NULL;
    SHAREDGEO *SharedGeoTmp = NULL;
    INT idx_neig, idx_geo, idx_rank, index, geoindex;
    INT *packptw, *packentries;
    INT *ptw = Geo2Ranks->Ptw;
    INT *entries = Geo2Ranks->Entries;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        PtwTmp = &(Geo2RanksPack[idx_neig]);       // 装入第几个PTW
        SharedGeoTmp = &(sharedgeo[idx_neig]);     // 取出和这个邻居的共享几何体信息
        PtwTmp->NumRows = SharedGeoTmp->SharedNum; //
        PtwTmp->Ptw = (INT *)malloc((PtwTmp->NumRows + 1) * sizeof(INT));
        packptw = PtwTmp->Ptw;
        packptw[0] = 0;
        for (idx_geo = 0; idx_geo < PtwTmp->NumRows; idx_geo++)
        {
            geoindex = SharedGeoTmp->Index[idx_geo];
            packptw[idx_geo + 1] = packptw[idx_geo] + ptw[geoindex + 1] - ptw[geoindex];
        }
        // 再填入内容
        PtwTmp->Entries = (INT *)malloc((packptw[PtwTmp->NumRows]) * sizeof(INT));
        PtwTmp->AndEntries = NULL;
        packentries = PtwTmp->Entries;
        index = 0;
        for (idx_geo = 0; idx_geo < PtwTmp->NumRows; idx_geo++)
        {
            geoindex = SharedGeoTmp->Index[idx_geo];
            for (idx_rank = ptw[geoindex]; idx_rank < ptw[geoindex + 1]; idx_rank++)
            {
                packentries[index] = entries[idx_rank];
                index++;
            }
        }
    }
    /*-----------------------------------------------*/
    INT *packagesize = (INT *)calloc(neignum, sizeof(INT));
    INT *packagedispls = (INT *)malloc(neignum * sizeof(INT));
    INT *boxsize = (INT *)malloc(neignum * sizeof(INT));
    INT packtotalsize = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        PtwTmp = &(Geo2RanksPack[idx_neig]);
        packagesize[idx_neig] += 1;
        packagesize[idx_neig] += PtwTmp->NumRows + 1;
        packagesize[idx_neig] += PtwTmp->Ptw[PtwTmp->NumRows];
        packtotalsize += packagesize[idx_neig];
    }
    packagedispls[0] = 0;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        packagedispls[idx_neig] = packagedispls[idx_neig - 1] + packagesize[idx_neig - 1];
    }
    /*-----------------------------------------------*/
    // 发送数据的大小
    MPI_Comm comm = mesh->SharedInfo->neighborcomm;
    MPI_Request rqst;
    MPI_Ineighbor_alltoall(packagesize, 1, MPI_INT, boxsize, 1, MPI_INT, comm, &rqst);
    // 打包数据
    INT *package = (INT *)malloc(packtotalsize * sizeof(INT));
    INT pos;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        PtwTmp = &(Geo2RanksPack[idx_neig]);
        pos = packagedispls[idx_neig];
        package[pos] = PtwTmp->NumRows;
        pos += 1;
        memcpy(&(package[pos]), PtwTmp->Ptw, (PtwTmp->NumRows + 1) * sizeof(INT));
        pos += PtwTmp->NumRows + 1;
        memcpy(&(package[pos]), PtwTmp->Entries, PtwTmp->Ptw[PtwTmp->NumRows] * sizeof(INT));
        OpenPFEM_Free(PtwTmp->Ptw);
        OpenPFEM_Free(PtwTmp->Entries);
    }
    OpenPFEM_Free(Geo2RanksPack);
    MPI_Wait(&rqst, MPI_STATUS_IGNORE);
    // 接收数据
    INT *boxdispls = (INT *)malloc(neignum * sizeof(INT));
    boxdispls[0] = 0;
    INT boxtotalsize;
    for (idx_neig = 1; idx_neig < neignum; idx_neig++)
    {
        boxdispls[idx_neig] = boxdispls[idx_neig - 1] + boxsize[idx_neig - 1];
    }
    boxtotalsize = boxdispls[neignum - 1] + boxsize[neignum - 1];
    INT *box = (INT *)malloc(boxtotalsize * sizeof(INT));
    MPI_Neighbor_alltoallv(package, packagesize, packagedispls, MPI_INT,
                           box, boxsize, boxdispls, MPI_INT, comm);

    OpenPFEM_Free(package);
    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(boxsize);
    /*-----------------------------------------------*/
    INT newgeonum = 0;
    bool *SharedIndicator = (bool *)calloc(geonum, sizeof(bool));
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        SharedGeoTmp = &(sharedgeo[idx_neig]);
        for (idx_geo = 0; idx_geo < SharedGeoTmp->SharedNum; idx_geo++)
        {
            geoindex = SharedGeoTmp->Index[idx_geo];
            if (SharedIndicator[geoindex] == 0)
            {
                SharedIndicator[geoindex] = 1;
                newgeonum++;
            }
        }
    }
    OpenPFEM_Free(SharedIndicator);
    /*-----------------------------------------------*/
    INT *sharedindex = (INT *)calloc(newgeonum, sizeof(INT)); // 共享几何体编号
    INT *shared2ranknum = (INT *)calloc(newgeonum, sizeof(INT));
    INT ranknum;
    INT **geoindicator = (INT **)malloc(neignum * sizeof(INT *));
    INT tmp = 0;
    bool flag;
    INT idy_geo;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        SharedGeoTmp = &(sharedgeo[idx_neig]);
        pos = boxdispls[idx_neig] + 1;
        geoindicator[idx_neig] = (INT *)malloc(SharedGeoTmp->SharedNum * sizeof(INT));
        for (idx_geo = 0; idx_geo < SharedGeoTmp->SharedNum; idx_geo++)
        {
            ranknum = box[pos + idx_geo + 1] - box[pos + idx_geo];
            geoindex = SharedGeoTmp->Index[idx_geo];
            flag = 0;
            for (idy_geo = 0; idy_geo < tmp; idy_geo++)
            {
                if (sharedindex[idy_geo] == geoindex)
                {
                    flag = 1;
                    shared2ranknum[idy_geo] += ranknum;
                    geoindicator[idx_neig][idx_geo] = idy_geo;
                    break;
                }
            }
            if (!flag)
            {
                sharedindex[tmp] = geoindex;
                shared2ranknum[tmp] += ranknum;
                geoindicator[idx_neig][idx_geo] = tmp;
                tmp++;
            }
        }
    }
    /*-----------------------------------------------*/
    INT **shared2rank = (INT **)malloc(newgeonum * sizeof(INT *));
    for (idx_geo = 0; idx_geo < newgeonum; idx_geo++)
    {
        shared2rank[idx_geo] = (INT *)malloc(shared2ranknum[idx_geo] * sizeof(INT));
        memset(shared2rank[idx_geo], -1, shared2ranknum[idx_geo] * sizeof(INT));
    }
    INT begin, end, begin_pos, end_pos, idx, idy, geoindex2;
    tmp = 0;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        SharedGeoTmp = &(sharedgeo[idx_neig]);
        pos = boxdispls[idx_neig] + 1;
        for (idx_geo = 0; idx_geo < SharedGeoTmp->SharedNum; idx_geo++)
        {
            geoindex2 = SharedGeoTmp->Index[idx_geo];
            begin = ptw[geoindex2];
            end = ptw[geoindex2 + 1];
            geoindex = geoindicator[idx_neig][idx_geo];
            begin_pos = box[pos + idx_geo] + pos + SharedGeoTmp->SharedNum + 1;
            end_pos = box[pos + idx_geo + 1] + pos + SharedGeoTmp->SharedNum + 1;
            for (idx = begin_pos; idx < end_pos; idx++)
            {
                flag = 0;
                for (idy = begin; idy < end; idy++)
                {
                    if (entries[idy] == box[idx])
                    {
                        flag = 1;
                        break;
                    }
                }
                if (!flag)
                {
                    for (idy_geo = 0; idy_geo < shared2ranknum[geoindex]; idy_geo++)
                    {
                        if (shared2rank[geoindex][idy_geo] == -1)
                        {
                            shared2rank[geoindex][idy_geo] = box[idx];
                            tmp++;
                            break;
                        }
                        else if (shared2rank[geoindex][idy_geo] == box[idx])
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
    OpenPFEM_Free(boxdispls);
    OpenPFEM_Free(box);
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        OpenPFEM_Free(geoindicator[idx_neig]);
    }
    OpenPFEM_Free(geoindicator);
    /*-----------------------------------------------*/
    tmp += ptw[Geo2Ranks->NumRows];
    INT *newptw = (INT *)malloc((Geo2Ranks->NumRows + 1) * sizeof(INT));
    for (idx_geo = 1; idx_geo < (Geo2Ranks->NumRows + 1); idx_geo++)
    {
        newptw[idx_geo] = ptw[idx_geo] - ptw[idx_geo - 1];
    }
    INT *newentries = (INT *)malloc(tmp * sizeof(INT));
    for (idx_geo = 0; idx_geo < newgeonum; idx_geo++)
    {
        geoindex = sharedindex[idx_geo];
        for (idx = 0; idx < shared2ranknum[idx_geo]; idx++)
        {
            if (shared2rank[idx_geo][idx] == -1)
            {
                break;
            }
        }
        newptw[geoindex + 1] += idx;
    }
    OpenPFEM_Free(shared2ranknum);
    newptw[0] = 0;
    for (idx_geo = 1; idx_geo < (Geo2Ranks->NumRows + 1); idx_geo++)
    {
        newptw[idx_geo] += newptw[idx_geo - 1];
    }
    for (idx_geo = 0; idx_geo < Geo2Ranks->NumRows; idx_geo++)
    {
        begin = ptw[idx_geo];
        end = ptw[idx_geo + 1];
        pos = newptw[idx_geo];
        memcpy(&(newentries[pos]), &(entries[begin]), (end - begin) * sizeof(INT));
    }
    for (idx_geo = 0; idx_geo < newgeonum; idx_geo++)
    {
        geoindex = sharedindex[idx_geo];
        begin = ptw[geoindex];
        end = ptw[geoindex + 1];
        pos = newptw[geoindex] + end - begin;
        memcpy(&(newentries[pos]), shared2rank[idx_geo], (newptw[geoindex + 1] - pos) * sizeof(INT));
        OpenPFEM_Free(shared2rank[idx_geo]);
    }
    Geo2Ranks->Ptw = newptw;
    Geo2Ranks->Entries = newentries;
    OpenPFEM_Free(ptw);
    OpenPFEM_Free(entries);
    OpenPFEM_Free(sharedindex);
    OpenPFEM_Free(shared2rank);
}

void ParMetisPartition(MESH *mesh, PTW *Vert4Elems, INT NumElems, INT *ElemRanks)
{
    MPI_Comm comm = mesh->comm;
    INT idx_rank;
    INT *elem_dist = (INT *)calloc(size + 1, sizeof(INT));
    elem_dist[rank + 1] = NumElems;
    MPI_Allreduce(MPI_IN_PLACE, elem_dist + 1, size, MPI_INT, MPI_MAX, comm);
#if PRINT_INFO
    INT min_num = elem_dist[1], max_num = elem_dist[1];
    for (idx_rank = 2; idx_rank < size + 1; idx_rank++)
    {
        if (min_num > elem_dist[idx_rank])
        {
            min_num = elem_dist[idx_rank];
        }
        if (max_num < elem_dist[idx_rank])
        {
            max_num = elem_dist[idx_rank];
        }
    }
    OpenPFEM_Print("最大和最小单元个数比例为: %d / %d = %g\n", max_num, min_num, ((double)max_num) / min_num);
#endif
    for (idx_rank = 2; idx_rank <= size; idx_rank++)
    {
        elem_dist[idx_rank] += elem_dist[idx_rank - 1];
    }

    idx_t *vtxdist = (idx_t *)malloc((size + 1) * sizeof(idx_t));
    for (idx_rank = 0; idx_rank <= size; idx_rank++)
    {
        vtxdist[idx_rank] = (idx_t)elem_dist[idx_rank];
    }
    idx_t *eptr, *eind;
    eptr = (idx_t *)malloc((mesh->num_face + 1) * sizeof(idx_t));
    eind = (idx_t *)malloc(mesh->num_face * MAXVERT4FACE * sizeof(idx_t));
    INT *PtwV4E = Vert4Elems->Ptw;
    INT *V4E = Vert4Elems->Entries;
    INT idx;
    idx_t elem_num = (idx_t)NumElems;
    idx_t totalnum = (idx_t)(Vert4Elems->Ptw[NumElems]);
    for (idx = 0; idx <= elem_num; idx++)
    {
        eptr[idx] = (idx_t)(PtwV4E[idx]);
    }
    VERT *verts = mesh->Verts;
    for (idx = 0; idx < totalnum; idx++)
    {
        eind[idx] = (idx_t)(verts[V4E[idx]].Index);
    }

    idx_t zero = 0;
    idx_t two = 2;
    idx_t *xadj, *adjncy;
    int result = ParMETIS_V3_Mesh2Dual(vtxdist, eptr, eind, &zero, &two, &xadj, &adjncy, &comm);
    if (result == METIS_ERROR)
    {
        exit(0);
    }
    // 改全局
    real_t ubvec = 1.05;
    real_t itr = 1000;
    idx_t edgecut;
    idx_t nparts = (idx_t)size;
    idx_t *partition = (idx_t *)malloc(NumElems * sizeof(idx_t));
    real_t *tpwgts = (real_t *)malloc(nparts * sizeof(real_t));
    for (idx = 0; idx < nparts; idx++)
    {
        tpwgts[idx] = 1.0 / nparts;
    }
    tpwgts[rank] += 0.0;
    idx_t ncon = 1;
    idx_t options[4] = {0, 0, 0, 0};
    ParMETIS_V3_AdaptiveRepart(vtxdist, xadj, adjncy,
                               NULL, NULL, NULL, &zero,
                               &zero, &ncon, &nparts, tpwgts,
                               &ubvec, &itr, options,
                               &edgecut, partition, &comm);
#if PRINT_INFO
    INT presv = 0;
#endif
    for (idx = 0; idx < NumElems; idx++)
    {
        ElemRanks[idx] = (int)(partition[idx]);
#if PRINT_INFO
        if (ElemRanks[idx] == rank)
        {
            presv++;
        }
#endif
    }
#if PRINT_INFO
    printf("[rank %d] 分配后本进程还留存的比例为 %d / %d = %g\n", rank, presv, NumElems, ((double)presv) / NumElems);
#endif
    OpenPFEM_Free(tpwgts);
    OpenPFEM_Free(elem_dist);
    OpenPFEM_Free(eptr);
    OpenPFEM_Free(eind);
    OpenPFEM_Free(vtxdist);
    return;
}

void VertGlobalIndexGenerate(MESH *mesh)
{
    MPI_Comm comm = mesh->comm;
    INT myVertNum = mesh->num_vert;
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    SHAREDGEO *sharedvert = sharedinfo->SharedVerts;
    INT idx_neig, i, neigrank;
    INT *Owner = NULL;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = sharedinfo->NeighborRanks[idx_neig];
        Owner = sharedvert[idx_neig].Owner;
        for (i = 0; i < sharedvert[idx_neig].SharedNum; i++)
        {
            if (Owner[i] == neigrank)
            {
                myVertNum--;
            }
        }
    }

    INT start_g, end_g;
    MPI_Scan(&myVertNum, &end_g, 1, MPI_INT, MPI_SUM, comm);
    start_g = end_g - myVertNum;
    INT *Gindex = (INT *)calloc(mesh->num_vert, sizeof(INT));
    INT *Index = NULL;
    VERT *verts = mesh->Verts;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = sharedinfo->NeighborRanks[idx_neig];
        Owner = sharedvert[idx_neig].Owner;
        Index = sharedvert[idx_neig].Index;
        for (i = 0; i < sharedvert[idx_neig].SharedNum; i++)
        {
            if (Owner[i] != rank)
            {
                Gindex[Index[i]] = -1;
            }
        }
    }
    INT pos = start_g;
    for (i = 0; i < mesh->num_vert; i++)
    {
        if (Gindex[i] == 0)
        {
            Gindex[i] = pos;
            pos++;
        }
    }

    PARADATA *paradata = NULL;
    ParaDataCreate(&paradata, mesh);
    ParaDataAdd(paradata, VERTDATA, 1, MPI_INT, (void *)Gindex);
    RECDATA *recdata = NULL;
    ParaDataCommunicate(paradata, &recdata);
    INT *vertsharedGindex = (INT *)(recdata->VertDatas[0]);

    INT currindex;
    INT *vertindex = recdata->VertIndex;
    for (i = 0; i < recdata->VertNum; i++)
    {
        if (vertsharedGindex[i] != -1)
        {
            currindex = vertindex[i];
            Gindex[currindex] = vertsharedGindex[i];
        }
    }
    ParaDataDestroy(&paradata);
    RecDataDestroy(&recdata);

    for (i = 0; i < mesh->num_vert; i++)
    {
        verts[i].Index = Gindex[i];
    }
    OpenPFEM_Free(Gindex);
    return;
}

void FaceGlobalIndexGenerate(MESH *mesh)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    INT worlddim = mesh->worlddim;

    INT myFaceNum = mesh->num_face;
    INT i;
    INT idx_neig, neigrank, neignum;
    INT *Owner = NULL;
    SHAREDINFO *sharedinfo = NULL;
    SHAREDGEO *sharedface = NULL;
    if (worlddim == 3)
    {
        neignum = mesh->SharedInfo->NumNeighborRanks;
        sharedinfo = mesh->SharedInfo;
        sharedface = sharedinfo->SharedFaces;
        for (idx_neig = 0; idx_neig < neignum; idx_neig++)
        {
            neigrank = sharedinfo->NeighborRanks[idx_neig];
            Owner = sharedface[idx_neig].Owner;
            for (i = 0; i < sharedface[idx_neig].SharedNum; i++)
            {
                if (Owner[i] == neigrank)
                {
                    myFaceNum--;
                }
            }
        }
    }

    INT start_g, end_g;
    MPI_Scan(&myFaceNum, &end_g, 1, MPI_INT, MPI_SUM, comm);
    start_g = end_g - myFaceNum;

    INT *Gindex = NULL;
    FACE *faces = mesh->Faces;
    if (worlddim == 3)
    {
        Gindex = (INT *)calloc(mesh->num_face, sizeof(INT));
        INT *Index = NULL;
        for (idx_neig = 0; idx_neig < neignum; idx_neig++)
        {
            neigrank = sharedinfo->NeighborRanks[idx_neig];
            Owner = sharedface[idx_neig].Owner;
            Index = sharedface[idx_neig].Index;
            for (i = 0; i < sharedface[idx_neig].SharedNum; i++)
            {
                if (Owner[i] != rank)
                {
                    Gindex[Index[i]] = -1;
                }
            }
        }
    }

    INT pos = start_g;
    for (i = 0; i < mesh->num_face; i++)
    {
        if (worlddim == 2)
        {
            mesh->Faces[i].Index = pos;
            pos++;
        }
        else if (worlddim == 3)
        {
            if (!Gindex[i])
            {
                Gindex[i] = pos;
                pos++;
            }
        }
    }

    if (worlddim == 3)
    {
        PARADATA *paradata = NULL;
        ParaDataCreate(&paradata, mesh);
        ParaDataAdd(paradata, FACEDATA, 1, MPI_INT, (void *)Gindex);
        RECDATA *recdata = NULL;
        ParaDataCommunicate(paradata, &recdata);
        INT *facesharedGindex = (INT *)(recdata->FaceDatas[0]);

        INT currindex;
        INT *faceindex = recdata->FaceIndex;
        for (i = 0; i < recdata->FaceNum; i++)
        {
            if (facesharedGindex[i] != -1)
            {
                currindex = faceindex[i];
                Gindex[currindex] = facesharedGindex[i];
            }
        }
        ParaDataDestroy(&paradata);
        RecDataDestroy(&recdata);

        for (i = 0; i < mesh->num_face; i++)
        {
            faces[i].Index = Gindex[i];
        }
        OpenPFEM_Free(Gindex);
    }
}

void VoluGlobalIndexGenerate(MESH *mesh)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    INT myVoluNum = mesh->num_volu;
    INT i;

    INT start_g, end_g;
    MPI_Scan(&myVoluNum, &end_g, 1, MPI_INT, MPI_SUM, comm);
    start_g = end_g - myVoluNum;
    INT pos = start_g;
    for (i = 0; i < mesh->num_volu; i++)
    {
        mesh->Volus[i].Index = pos;
        pos++;
    }
}