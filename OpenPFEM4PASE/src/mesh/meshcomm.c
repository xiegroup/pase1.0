#include "meshcomm.h"

#define PRINT_INFO 0

int rank, size, typesize;

void ParaDataCreate(PARADATA **ParaData, MESH *mesh)
{
    PARADATA *paradata = (PARADATA *)malloc(sizeof(PARADATA));
    if (mesh != NULL)
    {
        paradata->mesh = mesh;
        paradata->worlddim = mesh->worlddim;
        if (mesh->SharedInfo->neighborcomm == MPI_COMM_NULL)
        {
            NeighborCommCreate(mesh);
        }
    }
    paradata->NumDatas[0] = 0;
    paradata->VertData = NULL;
    paradata->NumDatas[1] = 0;
    paradata->LineData = NULL;
    paradata->NumDatas[2] = 0;
    paradata->FaceData = NULL;
    paradata->NumDatas[3] = 0;
    paradata->VoluData = NULL;
    paradata->NumDatas[4] = 0;
    paradata->SubDomianData = NULL;
    paradata->bridge = NULL;
    paradata->customsharedinfo = NULL;
    paradata->CustomNum = 0;
    paradata->CustomData = NULL;
    *ParaData = paradata;
}

void BridgeReverse(BRIDGE *bridge)
{
    INT *tmppointer = NULL;
    tmppointer = bridge->DestinationRank;
    bridge->DestinationRank = bridge->OriginRank;
    bridge->OriginRank = tmppointer;

    INT tmpint = 0;
    tmpint = bridge->DestinationRankNum;
    bridge->DestinationRankNum = bridge->OriginRankNum;
    bridge->OriginRankNum = tmpint;

    GEOBRIDGE *tmpgeo = NULL;
    tmpgeo = bridge->GeoSend;
    bridge->GeoSend = bridge->GeoRecv;
    bridge->GeoRecv = tmpgeo;

    tmpint = bridge->newgeonum[0];
    bridge->newgeonum[0] = bridge->oldgeonum[0];
    bridge->oldgeonum[0] = tmpint;
    tmpint = bridge->newgeonum[1];
    bridge->newgeonum[1] = bridge->oldgeonum[1];
    bridge->oldgeonum[1] = tmpint;
    tmpint = bridge->newgeonum[2];
    bridge->newgeonum[2] = bridge->oldgeonum[2];
    bridge->oldgeonum[2] = tmpint;
    tmpint = bridge->newgeonum[3];
    bridge->newgeonum[3] = bridge->oldgeonum[3];
    bridge->oldgeonum[3] = tmpint;

    MPI_Comm_free(&(bridge->BridgeComm));
    BridgeCommCreate(NULL, NULL, bridge);
}

void AttachedDataCreate(ATTACHEDDATA *AttachedData, INT BlockSize, MPI_Datatype Dttp, void *Data)
{
    ATTACHEDDATA attacheddata = (ATTACHEDDATA)malloc(sizeof(ATTACHED_DATA_PRIVATE));
    attacheddata->BlockSize = BlockSize;
    attacheddata->Dttp = Dttp;
    attacheddata->Data = Data;
    *AttachedData = attacheddata;
}

static INT RecData_basic_element_build(ATTACHEDDATA *attahceddata, INT num, void ***Data, INT **Index, INT neignum, SHAREDGEO *sharedgeo)
{
    INT i, totalnum = 0;
    void **data = (void **)malloc(num * sizeof(void *));

    for (i = 0; i < neignum; i++)
    {
        totalnum += sharedgeo[i].SharedNum;
    }
    if (totalnum == 0)
    {
        *Data = NULL;
        *Index = NULL;
        return 0;
    }
    INT *index = (INT *)malloc(totalnum * sizeof(INT));
    totalnum = 0;
    for (i = 0; i < neignum; i++)
    {
        memcpy(&(index[totalnum]), sharedgeo[i].Index, sharedgeo[i].SharedNum * sizeof(INT));
        totalnum += sharedgeo[i].SharedNum;
    }
    for (i = 0; i < num; i++)
    {
        MPI_Type_size(attahceddata[i]->Dttp, &typesize);
        data[i] = malloc(typesize * attahceddata[i]->BlockSize * totalnum);
    }
    *Data = data;
    *Index = index;
    return totalnum;
}

static INT RecData_basic_element_build_syn(ATTACHEDDATA *attahceddata, INT num, void ***Data, INT **Index, INT neignum, INT *neigrank, SHAREDGEO *sharedgeo)
{
    INT i, j, totalnum = 0, neigindex = 0;
    void **data = (void **)malloc(num * sizeof(void *));

    for (i = 0; i < neignum; i++)
    {
        neigindex = neigrank[i];
        for (j = 0; j < sharedgeo[i].SharedNum; j++)
        {
            if (sharedgeo[i].Owner[j] == neigindex)
            {
                totalnum++;
            }
        }
    }
    if (totalnum == 0)
    {
        *Data = NULL;
        *Index = NULL;
        return 0;
    }
    INT *index = (INT *)malloc(totalnum * sizeof(INT));
    totalnum = 0;
    for (i = 0; i < neignum; i++)
    {
        neigindex = neigrank[i];
        for (j = 0; j < sharedgeo[i].SharedNum; j++)
        {
            if (sharedgeo[i].Owner[j] == neigindex)
            {
                index[totalnum] = sharedgeo[i].Index[j];
                totalnum++;
            }
        }
    }
    for (i = 0; i < num; i++)
    {
        MPI_Type_size(attahceddata[i]->Dttp, &typesize);
        data[i] = malloc(typesize * attahceddata[i]->BlockSize * totalnum);
    }
    *Data = data;
    *Index = index;
    return totalnum;
}

void RecDataCreate(PARADATA *ParaData, RECDATA **RecData)
{
    RECDATA *recdata = (RECDATA *)malloc(sizeof(RECDATA));
    MESH *mesh = ParaData->mesh;
    SHAREDINFO *sharedinfo = mesh->SharedInfo;

    INT i;
    for (i = 0; i < 5; i++)
    {
        recdata->NumTypes[i] = ParaData->NumDatas[i];
    }
    if (recdata->NumTypes[0])
    {
        recdata->VertNum = RecData_basic_element_build(ParaData->VertData, recdata->NumTypes[0], &(recdata->VertDatas), &(recdata->VertIndex),
                                                       sharedinfo->NumNeighborRanks, sharedinfo->SharedVerts);
    }
    if (recdata->NumTypes[1])
    {
        recdata->LineNum = RecData_basic_element_build(ParaData->LineData, recdata->NumTypes[1], &(recdata->LineDatas), &(recdata->LineIndex),
                                                       sharedinfo->NumNeighborRanks, sharedinfo->SharedLines);
    }
    if (recdata->NumTypes[2])
    {
        recdata->FaceNum = RecData_basic_element_build(ParaData->FaceData, recdata->NumTypes[2], &(recdata->FaceDatas), &(recdata->FaceIndex),
                                                       sharedinfo->NumNeighborRanks, sharedinfo->SharedFaces);
    }
    if (recdata->NumTypes[3])
    {
        recdata->VoluNum = RecData_basic_element_build(ParaData->VoluData, recdata->NumTypes[3], &(recdata->VoluDatas), &(recdata->VoluIndex),
                                                       sharedinfo->NumNeighborRanks, sharedinfo->SharedVolus);
    }
    if (recdata->NumTypes[4])
    {
        recdata->SubDomianData = (void **)malloc(recdata->NumTypes[4] * sizeof(void *));
        for (i = 0; i < recdata->NumTypes[4]; i++)
        {
            MPI_Type_size(ParaData->SubDomianData[i]->Dttp, &typesize);
            recdata->SubDomianData[i] = malloc(typesize * (ParaData->SubDomianData[i]->BlockSize) * (sharedinfo->NumNeighborRanks));
        }
    }
    recdata->CustomNum = ParaData->CustomNum;
    recdata->CustomDataNum = (INT *)malloc(recdata->CustomNum * sizeof(INT));
    recdata->CustomDataIndex = (INT **)malloc(recdata->CustomNum * sizeof(INT *));
    recdata->CustomData = (void **)malloc(recdata->CustomNum * sizeof(void *));
    for (i = 0; i < recdata->CustomNum; i++)
    {
        INT j, totalnum = 0;
        for (j = 0; j < sharedinfo->NumNeighborRanks; j++)
        {
            totalnum += ParaData->customsharedinfo[i][j].SharedNum;
        }
        INT *index = (INT *)malloc(totalnum * sizeof(INT));
        totalnum = 0;
        for (j = 0; j < sharedinfo->NumNeighborRanks; j++)
        {
            memcpy(&(index[totalnum]), (ParaData->customsharedinfo[i][j]).Index, ParaData->customsharedinfo[i][j].SharedNum * sizeof(INT));
            totalnum += ParaData->customsharedinfo[i][j].SharedNum;
        }
        MPI_Type_size(ParaData->CustomData[i]->Dttp, &typesize);
        recdata->CustomDataNum[i] = totalnum;
        recdata->CustomData[i] = malloc(typesize * ((ParaData->CustomData[i])->BlockSize) * totalnum);
        recdata->CustomDataIndex[i] = index;
    }
    *RecData = recdata;
}

void ParaDataAdd(PARADATA *ParaData, INT geotype, INT BlockSize, MPI_Datatype Dttp, void *Data)
{
    ATTACHEDDATA newdata = NULL;
    AttachedDataCreate(&newdata, BlockSize, Dttp, Data);

    INT datasize = ParaData->NumDatas[geotype];
    ParaData->NumDatas[geotype]++;
    switch (geotype)
    {
    case VERTDATA:
        if (datasize)
        {
            ParaData->VertData = (ATTACHEDDATA *)realloc(ParaData->VertData, (datasize + 1) * sizeof(ATTACHEDDATA));
        }
        else
        {
            ParaData->VertData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
        }
        ParaData->VertData[datasize] = newdata;
        break;
    case LINEDATA:
        if (datasize)
        {
            ParaData->LineData = (ATTACHEDDATA *)realloc(ParaData->LineData, (datasize + 1) * sizeof(ATTACHEDDATA));
        }
        else
        {
            ParaData->LineData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
        }
        ParaData->LineData[datasize] = newdata;
        break;
    case FACEDATA:
        if (datasize)
        {
            ParaData->FaceData = (ATTACHEDDATA *)realloc(ParaData->FaceData, (datasize + 1) * sizeof(ATTACHEDDATA));
        }
        else
        {
            ParaData->FaceData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
        }
        ParaData->FaceData[datasize] = newdata;
        break;
    case VOLUDATA:
        if (datasize)
        {
            ParaData->VoluData = (ATTACHEDDATA *)realloc(ParaData->VoluData, (datasize + 1) * sizeof(ATTACHEDDATA));
        }
        else
        {
            ParaData->VoluData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
        }
        ParaData->VoluData[datasize] = newdata;
        break;
    case DOMAINDATA:
        if (datasize)
        {
            ParaData->SubDomianData = (ATTACHEDDATA *)realloc(ParaData->SubDomianData, (datasize + 1) * sizeof(ATTACHEDDATA));
        }
        else
        {
            ParaData->SubDomianData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
        }
        ParaData->SubDomianData[datasize] = newdata;
        break;
    }
}

void ParaDataSub(PARADATA *ParaData, INT geotype)
{
    ATTACHEDDATA *data = NULL;
    INT datasize = ParaData->NumDatas[geotype];
    ParaData->NumDatas[geotype] = 0;
    switch (geotype)
    {
    case VERTDATA:
        data = ParaData->VertData;
        ParaData->VertData = NULL;
        break;
    case LINEDATA:
        data = ParaData->LineData;
        ParaData->LineData = NULL;
        break;
    case FACEDATA:
        data = ParaData->FaceData;
        ParaData->FaceData = NULL;
        break;
    case VOLUDATA:
        data = ParaData->VoluData;
        ParaData->VoluData = NULL;
        break;
    case DOMAINDATA:
        data = ParaData->SubDomianData;
        ParaData->VoluData = NULL;
        break;
    }
    if (data == NULL)
    {
        return;
    }
    INT i;
    for (i = 0; i < datasize; i++)
    {
        OpenPFEM_Free(data[i]);
    }
    OpenPFEM_Free(data);
}

void ParaDataClean(PARADATA *ParaData)
{
    ParaDataSub(ParaData, VERTDATA);
    ParaDataSub(ParaData, LINEDATA);
    ParaDataSub(ParaData, FACEDATA);
    ParaDataSub(ParaData, VOLUDATA);
    ParaDataSub(ParaData, DOMAINDATA);
    ATTACHEDDATA *data = NULL;
    INT datasize = ParaData->CustomNum;
    ParaData->CustomNum = 0;
    data = ParaData->CustomData;
    ParaData->CustomData = NULL;
    if (data != NULL)
    {
        INT i;
        for (i = 0; i < datasize; i++)
        {
            OpenPFEM_Free(data[i]);
        }
    }
    OpenPFEM_Free(data);
}

void ParaDataPrint(PARADATA *ParaData)
{
    INT i;
    OpenPFEM_Print("\n===================================================\n");
    OpenPFEM_Print("当前ParaData中含有:\n");
    OpenPFEM_Print("%d个点数据,%d个线数据,%d个面数据,%d个体数据,%d个网格数据\n",
                   ParaData->NumDatas[0], ParaData->NumDatas[1], ParaData->NumDatas[2], ParaData->NumDatas[3], ParaData->NumDatas[4]);
    OpenPFEM_Print("===================================================\n");
}

void ParaDataCommunicate(PARADATA *ParaData, RECDATA **RecData)
{
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = ParaData->mesh->SharedInfo->neighborcomm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    SHAREDINFO *sharedinfo = ParaData->mesh->SharedInfo;
    INT neignum = sharedinfo->NumNeighborRanks;
    int neigind, neigrank, packagesizetotal = 0;
    INT *packagesize = (INT *)malloc(neignum * sizeof(INT));
    INT *packagedispls = (INT *)malloc(neignum * sizeof(INT));
    packagedispls[0] = 0;
    for (neigind = 0; neigind < neignum; neigind++) // 对每个邻居
    {
        packagesize[neigind] = PackageSize(ParaData, sharedinfo, neigind);
        packagesizetotal += packagesize[neigind];
    }
    for (neigind = 1; neigind < neignum; neigind++) // 对每个邻居
    {
        packagedispls[neigind] = packagedispls[neigind - 1] + packagesize[neigind - 1];
    }
    char *send_package = (char *)malloc(packagesizetotal * sizeof(char));
    char *recv_package = (char *)malloc(packagesizetotal * sizeof(char));
    INT position = 0;
    for (neigind = 0; neigind < neignum; neigind++)
    {
        PackageData(ParaData, sharedinfo, neigind, send_package, packagesizetotal, &position);
    }
    MPI_Request request;
    MPI_Ineighbor_alltoallv(send_package, packagesize, packagedispls, MPI_PACKED, recv_package, packagesize, packagedispls, MPI_PACKED, neigcomm, &request);
    RecDataCreate(ParaData, RecData);
    MPI_Wait(&request, MPI_STATUS_IGNORE);
    Unpackage(ParaData, *RecData, recv_package, packagesizetotal, packagesize, packagedispls, neignum);

    MPI_Request_free(&request);
    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(send_package);
    OpenPFEM_Free(recv_package);
}

void Unpackage(PARADATA *ParaData, RECDATA *RecData, char *package, INT totalsize, INT *size, INT *displs, INT packagenum)
{
    INT worlddim = ParaData->mesh->worlddim;
    INT i, j, sharednum, count, packsize, position, num;
    ATTACHEDDATA data = NULL;
    SHAREDINFO *sharedinfo = ParaData->mesh->SharedInfo;
    SHAREDGEO *sharedgeo = NULL;
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = sharedinfo->neighborcomm;
    char **vpointer = NULL, **lpointer = NULL, **fpointer = NULL, **upointer = NULL, **spointer = NULL, **cpointer = NULL;

    if (ParaData->NumDatas[0])
    {
        num = ParaData->NumDatas[0];
        vpointer = (char **)malloc(ParaData->NumDatas[0] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            vpointer[i] = (char *)(RecData->VertDatas[i]);
            // vpointer[i] = NULL;
        }
    }
    if (ParaData->NumDatas[1])
    {
        num = ParaData->NumDatas[1];
        lpointer = (char **)malloc(ParaData->NumDatas[1] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            lpointer[i] = (char *)(RecData->LineDatas[i]);
        }
    }
    if (ParaData->NumDatas[2])
    {
        num = ParaData->NumDatas[2];
        fpointer = (char **)malloc(ParaData->NumDatas[2] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            fpointer[i] = (char *)(RecData->FaceDatas[i]);
        }
    }
    if (ParaData->NumDatas[3])
    {
        num = ParaData->NumDatas[3];
        upointer = (char **)malloc(ParaData->NumDatas[3] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            upointer[i] = (char *)(RecData->VoluDatas[i]);
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        spointer = (char **)malloc(ParaData->NumDatas[4] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            spointer[i] = (char *)(RecData->SubDomianData[i]);
        }
    }
    if (ParaData->CustomNum)
    {
        cpointer = (char **)malloc(ParaData->CustomNum * sizeof(char *));
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            cpointer[i] = (char *)(RecData->CustomData[i]);
        }
    }

    for (i = 0; i < packagenum; i++)
    {
        position = displs[i];
        // 如果有点的
        num = ParaData->NumDatas[0];
        if (num)
        {
            sharedgeo = &(sharedinfo->SharedVerts[i]); // 读取点的共享信息
            sharednum = sharedgeo->SharedNum;          // 共享信息大小
            for (j = 0; j < num; j++)                  // 对每个点的附加信息
            {
                data = ParaData->VertData[j];
                count = data->BlockSize * sharednum;
                MPI_Unpack(package, totalsize, &position, vpointer[j], count, data->Dttp, neigcomm);
                vpointer[j] = (char *)(vpointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                vpointer[j] += count * typesize;
            }
        }
        num = ParaData->NumDatas[1];
        if (num)
        {
            sharedgeo = &(sharedinfo->SharedLines[i]);
            sharednum = sharedgeo->SharedNum;
            for (j = 0; j < num; j++)
            {
                data = ParaData->LineData[j];
                count = data->BlockSize * sharednum;
                MPI_Unpack(package, totalsize, &position, lpointer[j], count, data->Dttp, neigcomm);
                lpointer[j] = (char *)(lpointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                lpointer[j] += count * typesize;
            }
        }
        if (worlddim == 3)
        {
            num = ParaData->NumDatas[2];
            if (num)
            {
                sharedgeo = &(sharedinfo->SharedFaces[i]); // 读取点的共享信息
                sharednum = sharedgeo->SharedNum;          // 共享信息大小
                for (j = 0; j < num; j++)                  // 对每个点的附加信息
                {
                    data = ParaData->FaceData[j];
                    count = data->BlockSize * sharednum;
                    MPI_Unpack(package, totalsize, &position, fpointer[j], count, data->Dttp, neigcomm);
                    fpointer[j] = (char *)(fpointer[j]);
                    MPI_Type_size(data->Dttp, &typesize);
                    fpointer[j] += count * typesize;
                }
            }
        }
        num = ParaData->NumDatas[4];
        if (num)
        {
            sharednum = 1;
            for (j = 0; j < num; j++)
            {
                data = ParaData->SubDomianData[j];
                count = data->BlockSize * sharednum;
                MPI_Unpack(package, totalsize, &position, spointer[j], count, data->Dttp, neigcomm);
                spointer[j] = (char *)(spointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                spointer[j] += count * typesize;
            }
        }
        if (ParaData->CustomNum)
        {
            for (j = 0; j < ParaData->CustomNum; j++) // 对每个点的附加信息
            {
                sharedgeo = &(ParaData->customsharedinfo[j][i]); // 读取点的共享信息
                sharednum = sharedgeo->SharedNum;                // 共享信息大小
                data = ParaData->CustomData[j];
                count = data->BlockSize * sharednum;
                MPI_Unpack(package, totalsize, &position, cpointer[j], count, data->Dttp, neigcomm);
                cpointer[j] = (char *)(cpointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                cpointer[j] += count * typesize;
            }
        }
    }

    return;
}

void PackageData(PARADATA *ParaData, SHAREDINFO *SharedInfo, INT neigind, char *package, INT size, INT *position)
{
    INT worlddim = ParaData->mesh->worlddim;
    INT num, count, sharednum;
    INT i, j, blocksize;
    INT *sharedindex;
    void *infodata;
    ATTACHEDDATA data;
    SHAREDGEO *sharedgeo;
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = SharedInfo->neighborcomm;

    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        sharedgeo = &(SharedInfo->SharedVerts[neigind]);
        sharednum = sharedgeo->SharedNum;
        sharedindex = sharedgeo->Index;
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
            }
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        sharedgeo = &(SharedInfo->SharedLines[neigind]);
        sharednum = sharedgeo->SharedNum;
        sharedindex = sharedgeo->Index;
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
            }
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            sharedgeo = &(SharedInfo->SharedFaces[neigind]);
            sharednum = sharedgeo->SharedNum;
            sharedindex = sharedgeo->Index;
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                blocksize = data->BlockSize;
                infodata = data->Data;
                MPI_Type_size(data->Dttp, &typesize);
                for (j = 0; j < sharednum; j++)
                {
                    MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
                }
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            MPI_Pack(infodata, blocksize, data->Dttp, package, size, position, neigcomm);
        }
    }
    if (ParaData->CustomNum)
    {
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            sharedgeo = &(ParaData->customsharedinfo[i][neigind]);
            sharednum = sharedgeo->SharedNum;
            sharedindex = sharedgeo->Index;
            data = ParaData->CustomData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
            }
        }
    }
    return;
}

INT PackageSize(PARADATA *ParaData, SHAREDINFO *SharedInfo, INT neigind)
{
    INT size = 0, num;
    INT i;
    INT worlddim = ParaData->mesh->worlddim;
    ATTACHEDDATA data;
    SHAREDGEO *sharedgeo;
    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        sharedgeo = &(SharedInfo->SharedVerts[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize * sharedgeo->SharedNum;
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        sharedgeo = &(SharedInfo->SharedLines[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize * sharedgeo->SharedNum;
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            sharedgeo = &(SharedInfo->SharedFaces[neigind]);
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                MPI_Type_size(data->Dttp, &typesize);
                size += data->BlockSize * typesize * sharedgeo->SharedNum;
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize;
        }
    }
    if (ParaData->CustomNum)
    {
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            sharedgeo = &(ParaData->customsharedinfo[i][neigind]);
            data = ParaData->CustomData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize * sharedgeo->SharedNum;
        }
    }
    return size;
}

void RecDataDestroy(RECDATA **RecData)
{
    INT i;
    if ((*RecData)->NumTypes[0])
    {
        OpenPFEM_Free((*RecData)->VertIndex);
        for (i = 0; i < (*RecData)->NumTypes[0]; i++)
        {
            OpenPFEM_Free((*RecData)->VertDatas[i]);
        }
        OpenPFEM_Free((*RecData)->VertDatas);
    }
    if ((*RecData)->NumTypes[1])
    {
        OpenPFEM_Free((*RecData)->LineIndex);
        for (i = 0; i < (*RecData)->NumTypes[1]; i++)
        {
            OpenPFEM_Free((*RecData)->LineDatas[i]);
        }
        OpenPFEM_Free((*RecData)->LineDatas);
    }
    if ((*RecData)->NumTypes[2])
    {
        OpenPFEM_Free((*RecData)->FaceIndex);
        for (i = 0; i < (*RecData)->NumTypes[2]; i++)
        {
            OpenPFEM_Free((*RecData)->FaceDatas[i]);
        }
        OpenPFEM_Free((*RecData)->FaceDatas);
    }
    if ((*RecData)->NumTypes[3])
    {
        OpenPFEM_Free((*RecData)->VoluIndex);
        for (i = 0; i < (*RecData)->NumTypes[3]; i++)
        {
            OpenPFEM_Free((*RecData)->VoluDatas[i]);
        }
        OpenPFEM_Free((*RecData)->VoluDatas);
    }
    if ((*RecData)->NumTypes[4])
    {
        for (i = 0; i < (*RecData)->NumTypes[4]; i++)
        {
            OpenPFEM_Free((*RecData)->SubDomianData[i]);
        }
        OpenPFEM_Free((*RecData)->SubDomianData);
    }
    if ((*RecData)->CustomNum)
    {
        OpenPFEM_Free((*RecData)->CustomDataNum);
        for (i = 0; i < (*RecData)->CustomNum; i++)
        {
            OpenPFEM_Free((*RecData)->CustomDataIndex[i]);
            OpenPFEM_Free((*RecData)->CustomData[i]);
        }
        OpenPFEM_Free((*RecData)->CustomData);
    }
    OpenPFEM_Free((*RecData));
}

void ParaDataDestroy(PARADATA **ParaData)
{
    ParaDataClean(*ParaData);
    OpenPFEM_Free(*ParaData);
}

void NeighborCommCreate(MESH *mesh)
{
    MPI_Comm comm_old = mesh->comm;
    INT incount = 0, outcount = 0; // 定义为从进程数大的到小的
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    INT *neigrank = mesh->SharedInfo->NeighborRanks;
    MPI_Comm comm_new;
    MPI_Dist_graph_create_adjacent(comm_old, neignum, neigrank, MPI_UNWEIGHTED, neignum, neigrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &comm_new);
    mesh->SharedInfo->neighborcomm = comm_new;
}

/*------------------------------------------------------------------------------------*/
void BridgeCreate(BRIDGE **bridge, INT worlddim)
{
    BRIDGE *bri = (BRIDGE *)malloc(sizeof(BRIDGE));
    bri->worlddim = worlddim;
    bri->DestinationRankNum = 0;
    bri->DestinationRank = NULL;
    bri->GeoSend = NULL;
    bri->OriginRankNum = 0;
    bri->OriginRank = NULL;
    bri->GeoRecv = NULL;
    bri->BridgeComm = MPI_COMM_NULL;
    *bridge = bri;
}

// simufact
void SendGeoBridgeCreate(BRIDGE *bridge, INT num)
{
    GEOBRIDGE *geobridge = (GEOBRIDGE *)malloc(num * sizeof(GEOBRIDGE));
    INT i;
    for (i = 0; i < num; i++)
    {
        geobridge[i].VertNum = 0;
        geobridge[i].VertIndex = NULL;
        geobridge[i].LineNum = 0;
        geobridge[i].LineIndex = NULL;
        geobridge[i].FaceNum = 0;
        geobridge[i].FaceIndex = NULL;
        geobridge[i].VoluNum = 0;
        geobridge[i].VoluIndex = NULL;
    }
    bridge->GeoSend = geobridge;
}

void RecvGeoBridgeCreate(BRIDGE *bridge, INT num)
{
    GEOBRIDGE *geobridge = (GEOBRIDGE *)malloc(num * sizeof(GEOBRIDGE));
    INT i;
    for (i = 0; i < num; i++)
    {
        geobridge[i].VertNum = 0;
        geobridge[i].VertIndex = NULL;
        geobridge[i].LineNum = 0;
        geobridge[i].LineIndex = NULL;
        geobridge[i].FaceNum = 0;
        geobridge[i].FaceIndex = NULL;
        geobridge[i].VoluNum = 0;
        geobridge[i].VoluIndex = NULL;
    }
    bridge->GeoRecv = geobridge;
}

void BridgeCommCreate(MESH *newmesh, MESH *oldmesh, BRIDGE *bridge)
{
    if (newmesh != NULL)
    {
        MPI_Comm comm = newmesh->comm;
        MPI_Comm bridge_comm = MPI_COMM_NULL;
        INT incount = bridge->OriginRankNum;
        INT outcount = bridge->DestinationRankNum;
        INT *inrank = bridge->OriginRank;
        INT *outrank = bridge->DestinationRank;
        MPI_Dist_graph_create_adjacent(comm, incount, inrank, MPI_UNWEIGHTED, outcount, outrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &bridge_comm);
        bridge->pure_comm = comm;
        bridge->BridgeComm = bridge_comm;
        bridge->oldgeonum[0] = oldmesh->num_vert;
        bridge->oldgeonum[1] = oldmesh->num_line;
        bridge->oldgeonum[2] = oldmesh->num_face;
        bridge->oldgeonum[3] = oldmesh->num_volu;
        bridge->newgeonum[0] = newmesh->num_vert;
        bridge->newgeonum[1] = newmesh->num_line;
        bridge->newgeonum[2] = newmesh->num_face;
        bridge->newgeonum[3] = newmesh->num_vert;
    }
    else
    {
        MPI_Comm comm = bridge->pure_comm;
        MPI_Comm bridge_comm = MPI_COMM_NULL;
        INT incount = bridge->OriginRankNum;
        INT outcount = bridge->DestinationRankNum;
        INT *inrank = bridge->OriginRank;
        INT *outrank = bridge->DestinationRank;
        MPI_Dist_graph_create_adjacent(comm, incount, inrank, MPI_UNWEIGHTED, outcount, outrank, MPI_UNWEIGHTED, MPI_INFO_NULL, 0, &bridge_comm);
        bridge->BridgeComm = bridge_comm;
    }
}

void BridgePrint(BRIDGE *bridge, INT printrank)
{
    MPI_Comm comm = bridge->BridgeComm;
    MPI_Comm_rank(comm, &rank);
    if (rank == printrank)
    {
        INT i, j;
        printf("\n===================================================\n");
        printf("进程 %d 的新网格从 %d 个进程得到:\n\t", rank, bridge->OriginRankNum);
        for (i = 0; i < bridge->OriginRankNum; i++)
        {
            printf("----第%d个是进程[%d](%d个)\n", i, bridge->OriginRank[i], bridge->GeoRecv[i].VertNum);
            for (j = 0; j < bridge->GeoRecv[i].LineNum; j++)
            {
                printf("--------(%d) 线%d\n", j, bridge->GeoRecv[i].LineIndex[j]);
            }
        }
        printf("===================================================\n");
        printf("\n进程 %d 的旧网格发给了 %d 个进程:\n\t", rank, bridge->DestinationRankNum);
        for (i = 0; i < bridge->DestinationRankNum; i++)
        {
            printf("----第%d个是进程[%d](%d个)\n", i, bridge->DestinationRank[i], bridge->GeoSend[i].VertNum);
            for (j = 0; j < bridge->GeoSend[i].LineNum; j++)
            {
                printf("--------(%d) 线%d\n", j, bridge->GeoSend[i].LineIndex[j]);
            }
        }
        printf("===================================================\n");
    }
    MPI_Barrier(comm);
}

void ParaDataAddCustomData(PARADATA *ParaData, SHAREDGEO *sharedinfo, INT BlockSize, MPI_Datatype Dttp, void *Data)
{
    ATTACHEDDATA newdata = NULL;
    AttachedDataCreate(&newdata, BlockSize, Dttp, Data);

    INT datasize = ParaData->CustomNum;
    ParaData->CustomNum++;
    if (datasize)
    {
        ParaData->customsharedinfo = (SHAREDGEO **)realloc(ParaData->customsharedinfo, ParaData->CustomNum * sizeof(SHAREDGEO *));
        ParaData->CustomData = (ATTACHEDDATA *)realloc(ParaData->VertData, (datasize + 1) * sizeof(ATTACHEDDATA));
    }
    else
    {
        ParaData->customsharedinfo = (SHAREDGEO **)malloc(sizeof(SHAREDGEO *));
        ParaData->CustomData = (ATTACHEDDATA *)malloc(sizeof(ATTACHEDDATA));
    }
    ParaData->customsharedinfo[datasize] = sharedinfo;
    ParaData->CustomData[datasize] = newdata;
}

void ParaDataCreateWithBridge(PARADATA **ParaData, BRIDGE *bridge)
{
    PARADATA *paradata = (PARADATA *)malloc(sizeof(PARADATA));
    if (bridge != NULL)
    {
        paradata->bridge = bridge;
        paradata->worlddim = bridge->worlddim;
    }
    paradata->NumDatas[0] = 0;
    paradata->VertData = NULL;
    paradata->NumDatas[1] = 0;
    paradata->LineData = NULL;
    paradata->NumDatas[2] = 0;
    paradata->FaceData = NULL;
    paradata->NumDatas[3] = 0;
    paradata->VoluData = NULL;
    paradata->NumDatas[4] = 0;
    paradata->SubDomianData = NULL;
    paradata->customsharedinfo = NULL;
    paradata->CustomNum = 0;
    paradata->CustomData = NULL;
    *ParaData = paradata;
}

void ParaDataCreateByParaData(PARADATA **ParaData, PARADATA *Sample)
{
    PARADATA *paradata = NULL;
    INT worlddim = Sample->bridge->worlddim;
    ParaDataCreateWithBridge(&paradata, Sample->bridge);
    INT i, block_size;
    INT newnum = Sample->bridge->newgeonum[0];
    for (i = 0; i < Sample->NumDatas[0]; i++)
    {
        MPI_Type_size(Sample->VertData[i]->Dttp, &typesize);
        block_size = Sample->VertData[i]->BlockSize;
        char *data = malloc(typesize * block_size * newnum);
        ParaDataAdd(paradata, VERTDATA, block_size, Sample->VertData[i]->Dttp, (void *)data);
    }
    newnum = Sample->bridge->newgeonum[1];
    for (i = 0; i < Sample->NumDatas[1]; i++)
    {
        MPI_Type_size(Sample->LineData[i]->Dttp, &typesize);
        block_size = Sample->LineData[i]->BlockSize;
        char *data = malloc(typesize * block_size * newnum);
        ParaDataAdd(paradata, LINEDATA, block_size, Sample->LineData[i]->Dttp, (void *)data);
    }
    if (worlddim == 3)
    {
        newnum = Sample->bridge->newgeonum[2];
        for (i = 0; i < Sample->NumDatas[2]; i++)
        {
            MPI_Type_size(Sample->FaceData[i]->Dttp, &typesize);
            block_size = Sample->FaceData[i]->BlockSize;
            void *data = malloc(typesize * block_size * newnum);
            ParaDataAdd(paradata, FACEDATA, block_size, Sample->FaceData[i]->Dttp, data);
        }
    }
    for (i = 0; i < Sample->NumDatas[4]; i++)
    {
        MPI_Type_size(Sample->SubDomianData[i]->Dttp, &typesize);
        block_size = Sample->SubDomianData[i]->BlockSize;
        void *data = malloc(typesize * block_size);
        ParaDataAdd(paradata, 4, block_size, Sample->SubDomianData[i]->Dttp, data);
    }
    *ParaData = paradata;
}

void BridgeCommunicate(PARADATA *SendParaData, PARADATA *RecvParaData)
{
    BRIDGE *bridge = SendParaData->bridge;
    MPI_Comm comm = bridge->BridgeComm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    INT outnum = bridge->DestinationRankNum;
    INT *packagesize = (INT *)malloc(outnum * sizeof(INT));
    INT *packagedispls = (INT *)malloc(outnum * sizeof(INT));
    packagedispls[0] = 0;
    INT idx_rank, packagesizetotal = 0;
    for (idx_rank = 0; idx_rank < outnum; idx_rank++) // 对每个邻居
    {
        packagesize[idx_rank] = BridgePackageSize(SendParaData, idx_rank);
        packagesizetotal += packagesize[idx_rank];
    }
    for (idx_rank = 1; idx_rank < outnum; idx_rank++) // 对每个邻居
    {
        packagedispls[idx_rank] = packagedispls[idx_rank - 1] + packagesize[idx_rank - 1];
    }
    char *send_package = (char *)malloc(packagesizetotal * sizeof(char));
    INT innum = bridge->OriginRankNum;
    INT *boxsize = (INT *)malloc(innum * sizeof(INT));
    INT *boxdispls = (INT *)malloc(innum * sizeof(INT));
    boxdispls[0] = 0;
    INT boxsizetotal = 0;
    for (idx_rank = 0; idx_rank < innum; idx_rank++) // 对每个邻居
    {
        boxsize[idx_rank] = BridgeBoxSize(SendParaData, idx_rank);
        boxsizetotal += boxsize[idx_rank];
    }
    for (idx_rank = 1; idx_rank < innum; idx_rank++) // 对每个邻居
    {
        boxdispls[idx_rank] = boxdispls[idx_rank - 1] + boxsize[idx_rank - 1];
    }
    char *recv_package = (char *)malloc(boxsizetotal * sizeof(char));
    INT position = 0;
    for (idx_rank = 0; idx_rank < outnum; idx_rank++)
    {
        BridgePackageData(SendParaData, idx_rank, send_package, packagesizetotal, &position);
    }
    MPI_Neighbor_alltoallv(send_package, packagesize, packagedispls, MPI_PACKED, recv_package, boxsize, boxdispls, MPI_PACKED, comm);
    BridgeUnpackage(SendParaData, RecvParaData, recv_package, boxsizetotal, boxsize, boxdispls, innum);

    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(boxsize);
    OpenPFEM_Free(boxdispls);
    OpenPFEM_Free(send_package);
    OpenPFEM_Free(recv_package);
}

void BridgeUnpackage(PARADATA *SendParaData, PARADATA *RecvParaData, char *package, INT totalsize, INT *size, INT *displs, INT packagenum)
{
    INT worlddim = SendParaData->bridge->worlddim;
    INT i, j, k, geonum, count, packsize, position, num;
    INT *geoindex = NULL;
    ATTACHEDDATA data = NULL;
    BRIDGE *bridge = SendParaData->bridge;
    GEOBRIDGE *georecv = bridge->GeoRecv;
    MPI_Comm comm = bridge->BridgeComm;
    char **vpointer = NULL, **lpointer = NULL, **fpointer = NULL, **upointer = NULL, **spointer = NULL;
    if (RecvParaData->NumDatas[0])
    {
        num = RecvParaData->NumDatas[0];
        vpointer = (char **)malloc(RecvParaData->NumDatas[0] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            vpointer[i] = (char *)(RecvParaData->VertData[i]->Data);
        }
    }
    if (RecvParaData->NumDatas[1])
    {
        num = RecvParaData->NumDatas[1];
        lpointer = (char **)malloc(RecvParaData->NumDatas[1] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            lpointer[i] = (char *)(RecvParaData->LineData[i]->Data);
        }
    }
    if (RecvParaData->NumDatas[2])
    {
        num = RecvParaData->NumDatas[2];
        fpointer = (char **)malloc(RecvParaData->NumDatas[2] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            fpointer[i] = (char *)(RecvParaData->FaceData[i]->Data);
        }
    }
    if (RecvParaData->NumDatas[3])
    {
        num = RecvParaData->NumDatas[3];
        upointer = (char **)malloc(RecvParaData->NumDatas[3] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            upointer[i] = (char *)(RecvParaData->VoluData[i]->Data);
        }
    }
    if (RecvParaData->NumDatas[4])
    {
        num = RecvParaData->NumDatas[4];
        spointer = (char **)malloc(RecvParaData->NumDatas[4] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            spointer[i] = (char *)(RecvParaData->SubDomianData[i]->Data);
        }
    }
    for (i = 0; i < packagenum; i++)
    {
        position = displs[i];
        // 如果有点的
        num = RecvParaData->NumDatas[0];
        if (num)
        {
            geonum = georecv[i].VertNum;     // 读取点的共享信息
            geoindex = georecv[i].VertIndex; // 共享信息大小
            for (j = 0; j < num; j++)        // 对每个点的附加信息
            {
                data = RecvParaData->VertData[j];
                MPI_Type_size(data->Dttp, &typesize);
                vpointer[j] = (char *)(vpointer[j]);
                for (k = 0; k < geonum; k++)
                {
                    count = geoindex[k] * typesize * (data->BlockSize);
                    MPI_Unpack(package, totalsize, &position, vpointer[j] + count, data->BlockSize, data->Dttp, comm);
                }
            }
        }
        num = RecvParaData->NumDatas[1];
        if (num)
        {
            geonum = georecv[i].LineNum;     // 读取点的共享信息
            geoindex = georecv[i].LineIndex; // 共享信息大小
            for (j = 0; j < num; j++)        // 对每个点的附加信息
            {
                data = RecvParaData->LineData[j];
                MPI_Type_size(data->Dttp, &typesize);
                lpointer[j] = (char *)(lpointer[j]);
                for (k = 0; k < geonum; k++)
                {
                    count = geoindex[k] * typesize * (data->BlockSize);
                    MPI_Unpack(package, totalsize, &position, lpointer[j] + count, data->BlockSize, data->Dttp, comm);
                }
            }
        }
        if (worlddim == 3)
        {
            num = RecvParaData->NumDatas[2];
            if (num)
            {
                geonum = georecv[i].FaceNum;     // 读取点的共享信息
                geoindex = georecv[i].FaceIndex; // 共享信息大小
                for (j = 0; j < num; j++)        // 对每个点的附加信息
                {
                    data = RecvParaData->FaceData[j];
                    MPI_Type_size(data->Dttp, &typesize);
                    fpointer[j] = (char *)(fpointer[j]);
                    for (k = 0; k < geonum; k++)
                    {
                        count = geoindex[k] * typesize * data->BlockSize;
                        MPI_Unpack(package, totalsize, &position, fpointer[j] + count, data->BlockSize, data->Dttp, comm);
                    }
                }
            }
        }
        num = RecvParaData->NumDatas[4];
        if (num)
        {
            geonum = 1;
            for (j = 0; j < num; j++)
            {
                data = RecvParaData->SubDomianData[j];
                count = data->BlockSize * geonum;
                MPI_Unpack(package, totalsize, &position, spointer[j], count, data->Dttp, comm);
                spointer[j] = (char *)(spointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                spointer[j] += count * typesize;
            }
        }
    }

    return;
}

void BridgePackageData(PARADATA *ParaData, INT neigind, char *package, INT size, INT *position)
{
    INT worlddim = ParaData->bridge->worlddim;
    INT num, count, geonum;
    INT i, j, blocksize;
    INT *geoindex = NULL;
    void *infodata = NULL;
    ATTACHEDDATA data;
    BRIDGE *bridge = ParaData->bridge;
    MPI_Comm comm = bridge->BridgeComm;
    GEOBRIDGE *geobridge = bridge->GeoSend;

    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        geonum = geobridge[neigind].VertNum;
        geoindex = geobridge[neigind].VertIndex;
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < geonum; j++)
            {
                MPI_Pack(infodata + typesize * blocksize * geoindex[j], blocksize, data->Dttp, package, size, position, comm);
            }
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        geonum = geobridge[neigind].LineNum;
        geoindex = geobridge[neigind].LineIndex;
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < geonum; j++)
            {
                MPI_Pack(infodata + typesize * blocksize * geoindex[j], blocksize, data->Dttp, package, size, position, comm);
            }
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            geonum = geobridge[neigind].FaceNum;
            geoindex = geobridge[neigind].FaceIndex;
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                blocksize = data->BlockSize;
                infodata = data->Data;
                MPI_Type_size(data->Dttp, &typesize);
                for (j = 0; j < geonum; j++)
                {
                    MPI_Pack(infodata + typesize * blocksize * geoindex[j], blocksize, data->Dttp, package, size, position, comm);
                }
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            MPI_Pack(infodata, blocksize, data->Dttp, package, size, position, comm);
        }
    }
    return;
}

INT BridgePackageSize(PARADATA *ParaData, INT neigind)
{
    INT size = 0, num;
    INT i;
    INT worlddim = ParaData->bridge->worlddim;
    ATTACHEDDATA data;
    BRIDGE *bridge = ParaData->bridge;
    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize * (bridge->GeoSend[neigind].VertNum);
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize * (bridge->GeoSend[neigind].LineNum);
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                MPI_Type_size(data->Dttp, &typesize);
                size += data->BlockSize * typesize * (bridge->GeoSend[neigind].FaceNum);
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize;
        }
    }
    return size;
}

INT BridgeBoxSize(PARADATA *ParaData, INT neigind)
{
    INT size = 0, num;
    INT i;
    INT worlddim = ParaData->bridge->worlddim;
    ATTACHEDDATA data;
    BRIDGE *bridge = ParaData->bridge;
    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += (data->BlockSize) * typesize * (bridge->GeoRecv[neigind].VertNum);
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += (data->BlockSize) * typesize * (bridge->GeoRecv[neigind].LineNum);
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                MPI_Type_size(data->Dttp, &typesize);
                size += (data->BlockSize) * typesize * (bridge->GeoRecv[neigind].FaceNum);
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += (data->BlockSize) * typesize;
        }
    }
    return size;
}

void BridgeDestroy(BRIDGE **bridge)
{
    OpenPFEM_Free((*bridge)->DestinationRank);
    OpenPFEM_Free((*bridge)->OriginRank);
    INT i;
    for (i = 0; i < (*bridge)->DestinationRankNum; i++)
    {
        OpenPFEM_Free((*bridge)->GeoSend[i].VertIndex);
        OpenPFEM_Free((*bridge)->GeoSend[i].LineIndex);
        OpenPFEM_Free((*bridge)->GeoSend[i].FaceIndex);
        OpenPFEM_Free((*bridge)->GeoSend[i].VoluIndex);
    }
    OpenPFEM_Free((*bridge)->GeoSend);
    for (i = 0; i < (*bridge)->OriginRankNum; i++)
    {
        OpenPFEM_Free((*bridge)->GeoRecv[i].VertIndex);
        OpenPFEM_Free((*bridge)->GeoRecv[i].LineIndex);
        OpenPFEM_Free((*bridge)->GeoRecv[i].FaceIndex);
        OpenPFEM_Free((*bridge)->GeoRecv[i].VoluIndex);
    }
    OpenPFEM_Free((*bridge)->GeoRecv);
    OpenPFEM_Free((*bridge));
}

void ParaDataSynchronize(PARADATA *ParaData, RECDATA **RecData)
{
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = ParaData->mesh->SharedInfo->neighborcomm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    SHAREDINFO *sharedinfo = ParaData->mesh->SharedInfo;
    INT neignum = sharedinfo->NumNeighborRanks;
    int neigind, neigrank, packagesizetotal = 0, recpackagesizetotal = 0;
    INT *packagesize = (INT *)malloc(neignum * sizeof(INT));
    INT *recpackagesize = (INT *)malloc(neignum * sizeof(INT));
    INT *packagedispls = (INT *)malloc(neignum * sizeof(INT));
    INT *recpackagedispls = (INT *)malloc(neignum * sizeof(INT));
    packagedispls[0] = 0;
    recpackagedispls[0] = 0;
    for (neigind = 0; neigind < neignum; neigind++) // 对每个邻居
    {
        packagesize[neigind] = SynPackageSize(ParaData, sharedinfo, neigind);
        recpackagesize[neigind] = SynRecPackageSize(ParaData, sharedinfo, neigind);
        packagesizetotal += packagesize[neigind];
        recpackagesizetotal += recpackagesize[neigind];
    }
    for (neigind = 1; neigind < neignum; neigind++) // 对每个邻居
    {
        packagedispls[neigind] = packagedispls[neigind - 1] + packagesize[neigind - 1];
        recpackagedispls[neigind] = recpackagedispls[neigind - 1] + recpackagesize[neigind - 1];
    }
    char *send_package = (char *)malloc(packagesizetotal * sizeof(char));
    char *recv_package = (char *)malloc(recpackagesizetotal * sizeof(char));
    INT position = 0;
    for (neigind = 0; neigind < neignum; neigind++)
    {
        SynPackageData(ParaData, sharedinfo, neigind, send_package, packagesizetotal, &position);
    }
    MPI_Request request;
    MPI_Ineighbor_alltoallv(send_package, packagesize, packagedispls, MPI_PACKED, recv_package, recpackagesize, recpackagedispls, MPI_PACKED, neigcomm, &request);
    SynRecDataCreate(ParaData, RecData);
    MPI_Wait(&request, MPI_STATUS_IGNORE);
    SynUnpackage(ParaData, *RecData, recv_package, recpackagesizetotal, recpackagesize, recpackagedispls, neignum);
    MPI_Request_free(&request);
    OpenPFEM_Free(packagesize);
    OpenPFEM_Free(recpackagesize);
    OpenPFEM_Free(packagedispls);
    OpenPFEM_Free(recpackagedispls);
    OpenPFEM_Free(send_package);
    OpenPFEM_Free(recv_package);
}

void SynUnpackage(PARADATA *ParaData, RECDATA *RecData, char *package, INT totalsize, INT *size, INT *displs, INT packagenum)
{
    INT i, j, sharednum, count, packsize, position = 0, num;
    INT worlddim = ParaData->worlddim;
    ATTACHEDDATA data = NULL;
    SHAREDINFO *sharedinfo = ParaData->mesh->SharedInfo;
    SHAREDGEO *sharedgeo = NULL;
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = sharedinfo->neighborcomm;
    char **vpointer = NULL, **lpointer = NULL, **fpointer = NULL, **upointer = NULL, **spointer = NULL, **cpointer = NULL;

    if (ParaData->NumDatas[0])
    {
        num = ParaData->NumDatas[0];
        vpointer = (char **)malloc(ParaData->NumDatas[0] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            vpointer[i] = (char *)(RecData->VertDatas[i]);
        }
    }
    if (ParaData->NumDatas[1])
    {
        num = ParaData->NumDatas[1];
        lpointer = (char **)malloc(ParaData->NumDatas[1] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            lpointer[i] = (char *)(RecData->LineDatas[i]);
        }
    }
    if (ParaData->NumDatas[2])
    {
        num = ParaData->NumDatas[2];
        fpointer = (char **)malloc(ParaData->NumDatas[2] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            fpointer[i] = (char *)(RecData->FaceDatas[i]);
        }
    }
    if (ParaData->NumDatas[3])
    {
        num = ParaData->NumDatas[3];
        upointer = (char **)malloc(ParaData->NumDatas[3] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            upointer[i] = (char *)(RecData->VoluDatas[i]);
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        spointer = (char **)malloc(ParaData->NumDatas[4] * sizeof(char *));
        for (i = 0; i < num; i++)
        {
            spointer[i] = (char *)(RecData->SubDomianData[i]);
        }
    }
    if (ParaData->CustomNum)
    {
        cpointer = (char **)malloc(ParaData->CustomNum * sizeof(char *));
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            cpointer[i] = (char *)(RecData->CustomData[i]);
        }
    }

    INT k, neigindex = 0, *sharedowner;
    for (i = 0; i < packagenum; i++)
    {
        neigindex = sharedinfo->NeighborRanks[i];
        position = displs[i];
        // 如果有点的
        num = ParaData->NumDatas[0];
        if (num)
        {
            sharedgeo = &(sharedinfo->SharedVerts[i]); // 读取点的共享信息
            sharednum = sharedgeo->SharedNum;          // 共享信息大小
            sharedowner = sharedgeo->SharedIndex;
            for (j = 0; j < num; j++) // 对每个点的附加信息
            {
                data = ParaData->VertData[j];
                count = data->BlockSize;
                for (k = 0; k < sharednum; k++)
                {
                    if (sharedowner[k] == neigindex)
                    {
                        MPI_Unpack(package, totalsize, &position, vpointer[j], count, data->Dttp, neigcomm);
                        vpointer[j] = (char *)(vpointer[j]);
                        MPI_Type_size(data->Dttp, &typesize);
                        vpointer[j] += count * typesize;
                    }
                }
            }
        }
        num = ParaData->NumDatas[1];
        if (num)
        {
            sharedgeo = &(sharedinfo->SharedLines[i]);
            sharednum = sharedgeo->SharedNum;
            sharedowner = sharedgeo->Owner;
            for (j = 0; j < num; j++) // 对每个点的附加信息
            {
                data = ParaData->LineData[j];
                count = data->BlockSize;
                for (k = 0; k < sharednum; k++)
                {
                    if (sharedowner[k] == neigindex)
                    {
                        MPI_Unpack(package, totalsize, &position, lpointer[j], count, data->Dttp, neigcomm);
                        lpointer[j] = (char *)(lpointer[j]);
                        MPI_Type_size(data->Dttp, &typesize);
                        lpointer[j] += count * typesize;
                    }
                }
            }
        }
        if (worlddim == 3)
        {
            num = ParaData->NumDatas[2];
            if (num)
            {
                sharedgeo = &(sharedinfo->SharedFaces[i]); // 读取点的共享信息
                sharednum = sharedgeo->SharedNum;          // 共享信息大小
                sharedowner = sharedgeo->Owner;
                for (j = 0; j < num; j++) // 对每个点的附加信息
                {
                    data = ParaData->FaceData[j];
                    count = data->BlockSize;
                    for (k = 0; k < sharednum; k++)
                    {
                        if (sharedowner[k] == neigindex)
                        {
                            MPI_Unpack(package, totalsize, &position, fpointer[j], count, data->Dttp, neigcomm);
                            fpointer[j] = (char *)(fpointer[j]);
                            MPI_Type_size(data->Dttp, &typesize);
                            fpointer[j] += count * typesize;
                        }
                    }
                }
            }
        }
        num = ParaData->NumDatas[4];
        if (num)
        {
            sharednum = 1;
            for (j = 0; j < num; j++)
            {
                data = ParaData->SubDomianData[j];
                count = data->BlockSize * sharednum;
                MPI_Unpack(package, totalsize, &position, spointer[j], count, data->Dttp, neigcomm);
                spointer[j] = (char *)(spointer[j]);
                MPI_Type_size(data->Dttp, &typesize);
                spointer[j] += count * typesize;
            }
        }
        if (ParaData->CustomNum)
        {
            for (j = 0; j < ParaData->CustomNum; j++) // 对每个点的附加信息
            {
                sharedgeo = &(ParaData->customsharedinfo[j][i]); // 读取点的共享信息
                sharednum = sharedgeo->SharedNum;                // 共享信息大小
                sharedowner = sharedgeo->Owner;
                data = ParaData->CustomData[j];
                count = data->BlockSize;
                MPI_Type_size(data->Dttp, &typesize);
                for (k = 0; k < sharednum; k++)
                {
                    if (sharedowner[k] == neigindex)
                    {
                        MPI_Unpack(package, totalsize, &position, cpointer[j], count, data->Dttp, neigcomm);
                        cpointer[j] = (char *)(cpointer[j]);
                        cpointer[j] += count * typesize;
                    }
                }
            }
        }
    }
    return;
}

void SynRecDataCreate(PARADATA *ParaData, RECDATA **RecData)
{
    RECDATA *recdata = (RECDATA *)malloc(sizeof(RECDATA));
    MESH *mesh = ParaData->mesh;
    SHAREDINFO *sharedinfo = mesh->SharedInfo;

    INT i;
    for (i = 0; i < 5; i++)
    {
        recdata->NumTypes[i] = ParaData->NumDatas[i];
    }
    if (recdata->NumTypes[0])
    {
        recdata->VertNum = RecData_basic_element_build_syn(ParaData->VertData, recdata->NumTypes[0], &(recdata->VertDatas), &(recdata->VertIndex),
                                                           sharedinfo->NumNeighborRanks, sharedinfo->NeighborRanks, sharedinfo->SharedVerts);
    }
    if (recdata->NumTypes[1])
    {
        recdata->LineNum = RecData_basic_element_build_syn(ParaData->LineData, recdata->NumTypes[1], &(recdata->LineDatas), &(recdata->LineIndex),
                                                           sharedinfo->NumNeighborRanks, sharedinfo->NeighborRanks, sharedinfo->SharedLines);
    }
    if (recdata->NumTypes[2])
    {
        recdata->FaceNum = RecData_basic_element_build_syn(ParaData->FaceData, recdata->NumTypes[2], &(recdata->FaceDatas), &(recdata->FaceIndex),
                                                           sharedinfo->NumNeighborRanks, sharedinfo->NeighborRanks, sharedinfo->SharedFaces);
    }
    if (recdata->NumTypes[3])
    {
        recdata->VoluNum = RecData_basic_element_build_syn(ParaData->VoluData, recdata->NumTypes[3], &(recdata->VoluDatas), &(recdata->VoluIndex),
                                                           sharedinfo->NumNeighborRanks, sharedinfo->NeighborRanks, sharedinfo->SharedVolus);
    }
    if (recdata->NumTypes[4])
    {
        recdata->SubDomianData = (void **)malloc(recdata->NumTypes[4] * sizeof(void *));
        for (i = 0; i < recdata->NumTypes[4]; i++)
        {
            MPI_Type_size(ParaData->SubDomianData[i]->Dttp, &typesize);
            recdata->SubDomianData[i] = malloc(typesize * (ParaData->SubDomianData[i]->BlockSize) * (sharedinfo->NumNeighborRanks));
        }
    }
    recdata->CustomNum = ParaData->CustomNum;
    recdata->CustomDataNum = (INT *)malloc(recdata->CustomNum * sizeof(INT));
    recdata->CustomDataIndex = (INT **)malloc(recdata->CustomNum * sizeof(INT *));
    recdata->CustomData = (void **)malloc(recdata->CustomNum * sizeof(void *));
    INT k, neigindex;
    for (i = 0; i < recdata->CustomNum; i++)
    {
        INT j, totalnum = 0;
        for (j = 0; j < sharedinfo->NumNeighborRanks; j++)
        {
            neigindex = sharedinfo->NeighborRanks[j];
            for (k = 0; k < ParaData->customsharedinfo[i][j].SharedNum; k++)
            {
                if (ParaData->customsharedinfo[i][j].Owner[k] == neigindex)
                {
                    totalnum++;
                }
            }
        }
        INT *index = (INT *)malloc(totalnum * sizeof(INT));
        totalnum = 0;
        for (j = 0; j < sharedinfo->NumNeighborRanks; j++)
        {
            neigindex = sharedinfo->NeighborRanks[j];
            for (k = 0; k < ParaData->customsharedinfo[i][j].SharedNum; k++)
            {
                if (ParaData->customsharedinfo[i][j].Owner[k] == neigindex)
                {
                    index[totalnum] = ParaData->customsharedinfo[i][j].Index[k];
                    totalnum++;
                }
            }
        }
        MPI_Type_size(ParaData->CustomData[i]->Dttp, &typesize);
        recdata->CustomDataNum[i] = totalnum;
        recdata->CustomData[i] = malloc(typesize * ((ParaData->CustomData[i])->BlockSize) * totalnum);
        recdata->CustomDataIndex[i] = index;
    }
    *RecData = recdata;
}

INT SynRecPackageSize(PARADATA *ParaData, SHAREDINFO *SharedInfo, INT neigind)
{
    INT worlddim = ParaData->worlddim;
    INT size = 0, num = 0, mynum = 0;
    INT i, j;
    INT neigindex = SharedInfo->NeighborRanks[neigind];
    ATTACHEDDATA data;
    SHAREDGEO *sharedgeo;
    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        sharedgeo = &(SharedInfo->SharedVerts[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == neigindex)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * mynum;
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        sharedgeo = &(SharedInfo->SharedLines[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == neigindex)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * num;
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            sharedgeo = &(SharedInfo->SharedFaces[neigind]);
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                MPI_Type_size(data->Dttp, &typesize);
                mynum = 0;
                for (j = 0; j < sharedgeo->SharedNum; j++)
                {
                    if (sharedgeo->Owner[j] == neigindex)
                    {
                        mynum++;
                    }
                }
                size += data->BlockSize * typesize * mynum;
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize;
        }
    }
    if (ParaData->CustomNum)
    {
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            sharedgeo = &(ParaData->customsharedinfo[i][neigind]);
            data = ParaData->CustomData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == neigindex)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * mynum;
        }
    }
    return size;
}

void SynPackageData(PARADATA *ParaData, SHAREDINFO *SharedInfo, INT neigind, char *package, INT size, INT *position)
{
    INT worlddim = ParaData->worlddim;
    INT num, count, sharednum;
    INT i, j, blocksize;
    INT *sharedindex, *sharedowner;
    void *infodata;
    ATTACHEDDATA data;
    SHAREDGEO *sharedgeo;
    MPI_Comm comm = ParaData->mesh->comm;
    MPI_Comm neigcomm = SharedInfo->neighborcomm;

    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        sharedgeo = &(SharedInfo->SharedVerts[neigind]);
        sharednum = sharedgeo->SharedNum;
        sharedindex = sharedgeo->Index;
        sharedowner = sharedgeo->Owner;
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                if (sharedowner[j] == rank)
                {
                    MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
                }
            }
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        sharedgeo = &(SharedInfo->SharedLines[neigind]);
        sharednum = sharedgeo->SharedNum;
        sharedindex = sharedgeo->Index;
        sharedowner = sharedgeo->Owner;
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                if (sharedowner[j] == rank)
                {
                    MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
                }
            }
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            sharedgeo = &(SharedInfo->SharedFaces[neigind]);
            sharednum = sharedgeo->SharedNum;
            sharedindex = sharedgeo->Index;
            sharedowner = sharedgeo->Owner;
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                blocksize = data->BlockSize;
                infodata = data->Data;
                MPI_Type_size(data->Dttp, &typesize);
                for (j = 0; j < sharednum; j++)
                {
                    if (sharedowner[j] == rank)
                    {
                        MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
                    }
                }
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            MPI_Pack(infodata, blocksize, data->Dttp, package, size, position, neigcomm);
        }
    }
    if (ParaData->CustomNum)
    {
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            sharedgeo = &(ParaData->customsharedinfo[i][neigind]);
            sharednum = sharedgeo->SharedNum;
            sharedindex = sharedgeo->Index;
            sharedowner = sharedgeo->Owner;
            data = ParaData->CustomData[i];
            blocksize = data->BlockSize;
            infodata = data->Data;
            MPI_Type_size(data->Dttp, &typesize);
            for (j = 0; j < sharednum; j++)
            {
                if (sharedowner[j] == rank)
                {
                    MPI_Pack(infodata + typesize * blocksize * sharedindex[j], blocksize, data->Dttp, package, size, position, neigcomm);
                }
            }
        }
    }
    return;
}

INT SynPackageSize(PARADATA *ParaData, SHAREDINFO *SharedInfo, INT neigind)
{
    INT worlddim = ParaData->worlddim;
    INT size = 0, num = 0, mynum = 0;
    INT i, j;
    ATTACHEDDATA data;
    SHAREDGEO *sharedgeo;
    if (ParaData->NumDatas[0]) // 有点的信息
    {
        num = ParaData->NumDatas[0];
        sharedgeo = &(SharedInfo->SharedVerts[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->VertData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == rank)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * mynum;
        }
    }
    if (ParaData->NumDatas[1]) // 有线的信息
    {
        num = ParaData->NumDatas[1];
        sharedgeo = &(SharedInfo->SharedLines[neigind]);
        for (i = 0; i < num; i++)
        {
            data = ParaData->LineData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == rank)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * num;
        }
    }
    if (worlddim == 3)
    {
        if (ParaData->NumDatas[2]) // 有面的信息
        {
            num = ParaData->NumDatas[2];
            sharedgeo = &(SharedInfo->SharedFaces[neigind]);
            for (i = 0; i < num; i++)
            {
                data = ParaData->FaceData[i];
                MPI_Type_size(data->Dttp, &typesize);
                mynum = 0;
                for (j = 0; j < sharedgeo->SharedNum; j++)
                {
                    if (sharedgeo->Owner[j] == rank)
                    {
                        mynum++;
                    }
                }
                size += data->BlockSize * typesize * mynum;
            }
        }
    }
    if (ParaData->NumDatas[4])
    {
        num = ParaData->NumDatas[4];
        for (i = 0; i < num; i++)
        {
            data = ParaData->SubDomianData[i];
            MPI_Type_size(data->Dttp, &typesize);
            size += data->BlockSize * typesize;
        }
    }
    if (ParaData->CustomNum)
    {
        for (i = 0; i < ParaData->CustomNum; i++)
        {
            sharedgeo = &(ParaData->customsharedinfo[i][neigind]);
            data = ParaData->CustomData[i];
            MPI_Type_size(data->Dttp, &typesize);
            mynum = 0;
            for (j = 0; j < sharedgeo->SharedNum; j++)
            {
                if (sharedgeo->Owner[j] == rank)
                {
                    mynum++;
                }
            }
            size += data->BlockSize * typesize * mynum;
        }
    }
    return size;
}