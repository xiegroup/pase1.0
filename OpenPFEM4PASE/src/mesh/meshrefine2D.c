#include "mesh.h"
#include "meshcomm.h"

#define PRINT_INFO 0

INT size, rank;

static bool check_if_line_shared(MESH *mesh, INT index)
{
    INT flag = 0;
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    SHAREDGEO *sharedline = mesh->SharedInfo->SharedLines;
    INT i, j, num;
    for (i = 0; i < neignum; i++)
    {
        num = sharedline[i].SharedNum;
        for (j = 0; j < num; j++)
        {
            if (index == sharedline[i].Index[j])
            {
                flag = 1;
                break;
            }
        }
        if (flag)
        {
            break;
        }
    }
    return flag;
}

static bool BisectionRefineCycle2D(MESH *mesh, bool *Face_Mark, PARADATA *para_edgemark,
                                   INT *Line_Owner, INT *Face_Neighbor, INT *real_refine_num, INT *Num_Refine_Edges)
{
    bool flag = 0;
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    INT i, j, k;
    INT num_verts = mesh->num_vert;
    INT num_lines = mesh->num_line;
    INT num_faces = mesh->num_face;
    VERT *Verts = mesh->Verts;
    LINE *Lines = mesh->Lines;
    FACE *Faces = mesh->Faces;

    RECDATA *recdata = NULL;
    ParaDataCommunicate(para_edgemark, &recdata);

    bool *edgemark = (bool *)(para_edgemark->LineData[0]->Data);
    INT recvnum = recdata->LineNum;
    INT *recvindex = recdata->LineIndex;
    bool *recvbool = (bool *)(recdata->LineDatas[0]);

    //更新
    INT newedgenum = 0, newfacenum = 0;
    INT face_ind;
    for (i = 0; i < recvnum; i++)
    {
        if (recvbool[i])
        {
            k = recvindex[i];
            if (!(edgemark[k]))
            {
                edgemark[k] = 1;
                newedgenum++;
                //有新的加密边时，就检查加密单元
                face_ind = Line_Owner[2 * k];
                while (Face_Mark[face_ind] == 0)
                {
                    Face_Mark[face_ind] = 1;
                    newfacenum++;
                    //需要加密就把他的主边标记位加密
                    j = Faces[face_ind].Line4Face[0];
                    if (edgemark[j] == 0)
                    {
                        edgemark[j] = 1;
                        //有连锁引起的新加密的边就检查一下是不是shared的边
                        flag = flag | check_if_line_shared(mesh, j);
                        newedgenum++;
                    }
                    if (Face_Neighbor[face_ind] > -1)
                    {

                        face_ind = Face_Neighbor[face_ind];
                    }
                }
            }
        }
    }

#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "新增shared边 = %d\n", flag);
#endif
    *real_refine_num += newfacenum;
    *Num_Refine_Edges += newedgenum;
    MPI_Allreduce(MPI_IN_PLACE, &flag, 1, MPI_C_BOOL, MPI_LOR, comm);

    RecDataDestroy(&recdata);

    return flag;
}

//以此为例实现一个通信过程
void MeshBisectionRefine2D(MESH *mesh, INT Num_Refine_Elems, INT *Refine_Elems)
{
    extern int cyclenum;
    MPI_Comm comm = mesh->comm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    INT i, j, k;
    INT num_verts = mesh->num_vert;
    INT num_lines = mesh->num_line;
    INT num_faces = mesh->num_face;
    VERT *Verts = mesh->Verts, *vert;
    LINE *Lines = mesh->Lines, *line, *newline;
    FACE *Faces = mesh->Faces, *face, *newface;

#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "bisection refinement num = %d\n", Num_Refine_Elems);
#endif

    //记录每条线的属于哪个单元，如果是边界就取-1
    INT *Line_Owner = (INT *)malloc(2 * num_lines * sizeof(INT));
    memset(Line_Owner, -1, 2 * num_lines * sizeof(int));
    for (i = 0; i < mesh->num_face; i++)
    {
        face = &(Faces[i]);
        for (j = 0; j < face->NumLines; j++)
        {
            k = face->Line4Face[j];
            if (Line_Owner[2 * k] == -1)
            {
                Line_Owner[2 * k] = i;
            }
            else
            {
                Line_Owner[2 * k + 1] = i;
            }
        }
    }
    //记录每个单元的主边的邻居,-1表示边界
    INT *Face_Neighbor = (INT *)malloc(num_faces * sizeof(INT));
    for (i = 0; i < num_faces; i++)
    {
        j = Faces[i].Line4Face[0];
        if (Line_Owner[2 * j + 1] == i)
        {
            Face_Neighbor[i] = Line_Owner[2 * j];
        }
        else
        {
            Face_Neighbor[i] = Line_Owner[2 * j + 1];
        }
    }

    //计算需要加密的单元的集合
    bool *Face_Mark = calloc((unsigned int)num_faces, sizeof(bool));
    INT refine_ind, face_ind;
    INT real_refine_num = 0;
    for (refine_ind = 0; refine_ind < Num_Refine_Elems; refine_ind++)
    {
        face_ind = Refine_Elems[refine_ind];
        while (Face_Mark[face_ind] == 0)
        {
            Face_Mark[face_ind] = 1;
            real_refine_num++;
            i = Face_Neighbor[face_ind];
            if (i > -1)
            {
                face_ind = i;
            }
        }
    }

#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "初始本地计算 real_refine_num = %d\n", real_refine_num);
#endif

    //计算需要加密的线的集合
    bool *Edge_Mark = (bool *)calloc(num_lines, sizeof(bool));
    INT Num_Refine_Edges = 0;
    for (i = 0; i < num_faces; i++)
    {
        face = &(Faces[i]);
        //检查本单元是否需要加密，如果需要加密就把他的主边标记位加密
        if (Face_Mark[i] == 1)
        {
            k = face->Line4Face[0];
            if (Edge_Mark[k] == 0)
            {
                Edge_Mark[k] = 1;
                Num_Refine_Edges++;
            }
        }
    }

#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "初始本地计算 Num_Refine_Edges = %d\n", Num_Refine_Edges);
#endif

    PARADATA *paradata = NULL;
    ParaDataCreate(&paradata, mesh);
    ParaDataAdd(paradata, 1, 1, MPI_C_BOOL, (void *)Edge_Mark);
    bool flag = 1;
    while (flag)
    {
        flag = BisectionRefineCycle2D(mesh, Face_Mark, paradata,
                                      Line_Owner, Face_Neighbor, &real_refine_num, &Num_Refine_Edges);
    }

#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "通信结束 refine_num=%d Num_Refine_Edges=%d\n", real_refine_num, Num_Refine_Edges);
#endif

    INT *Real_Refine_Elems = (INT *)malloc(2 * real_refine_num * sizeof(INT));
    INT *Next_Refine_Elems = (INT *)malloc(2 * real_refine_num * sizeof(INT));
    INT check_num = 2 * real_refine_num;

    INT position = 0;
    for (i = 0; i < num_faces; i++)
    {
        face = &(Faces[i]);
        //检查本单元是否需要加密，如果需要加密就把他的主边标记位加密
        if (Face_Mark[i] == 1)
        {
            Real_Refine_Elems[position] = i;
            position++;
        }
    }

    OpenPFEM_Free(Line_Owner);
    OpenPFEM_Free(Face_Neighbor);
    OpenPFEM_Free(Face_Mark);

    //开始加密
    INT New_verts = num_verts + Num_Refine_Edges; //每一个需要加密的边产生一个新的节点
    INT New_lines = num_lines + Num_Refine_Edges; //每一个需要加密的边需要增加一条新的边
    INT New_faces = num_faces;
    for (i = 0; i < real_refine_num; i++)
    {
        face = &(Faces[Real_Refine_Elems[i]]);
        for (j = 0; j < MAXLINE4FACE; j++)
        {
            k = face->Line4Face[j];
            New_lines += Edge_Mark[k];
            New_faces += Edge_Mark[k];
        }
    }
    VertsReCreate(&(mesh->Verts), num_verts, New_verts, mesh->worlddim);
    LinesReCreate(&(mesh->Lines), num_lines, New_lines);
    FacesReCreate(&(mesh->Faces), num_faces, New_faces);
    if (mesh->Fathers == NULL)
    {
        mesh->Fathers = (INT *)malloc(New_faces * sizeof(INT));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, New_faces * sizeof(INT));
    }

    INT *Edge_Child_Line = (INT *)malloc(num_lines * sizeof(INT));
    memset(Edge_Child_Line, -1, num_lines * sizeof(INT));
    INT *Edge_Child_Vert = (INT *)malloc(num_lines * sizeof(INT));
    memset(Edge_Child_Vert, -1, num_lines * sizeof(INT));

    //网格加密
    Verts = mesh->Verts;
    Lines = mesh->Lines;
    Faces = mesh->Faces;
    //第一遍加密
    position = 0;
    INT new_vert_index, new_line_index1, new_line_index2;
    INT vert_curr_index = num_verts;
    INT line_curr_index = num_lines;
    INT face_curr_index = num_faces;
    //遍历所有需要加密的面
    for (i = 0; i < real_refine_num; i++)
    {
        face = &(Faces[Real_Refine_Elems[i]]); //找到加密的面
        if (face->Line4Face[2] < num_lines)
        {                                           //如果第2条边是原来的边（或者继承了编号）
            if (Edge_Mark[face->Line4Face[2]] == 1) //该边要加密
            {
                Next_Refine_Elems[position] = Real_Refine_Elems[i]; //下一步加密这条边
                position++;
            }
        }
        line = &(Lines[face->Line4Face[0]]); //查看最长边
        //加密产生的新点
        if (Edge_Child_Vert[face->Line4Face[0]] == -1)
        { //这条边没有被加密过
            vert = &(Verts[vert_curr_index]);
            vert->Index = vert_curr_index;
            vert->Coord[0] = 0.5 * (Verts[line->Vert4Line[0]].Coord[0] + Verts[line->Vert4Line[1]].Coord[0]);
            vert->Coord[1] = 0.5 * (Verts[line->Vert4Line[0]].Coord[1] + Verts[line->Vert4Line[1]].Coord[1]);
            vert->BD_ID = line->BD_ID;
            Edge_Child_Vert[face->Line4Face[0]] = vert_curr_index;
            new_vert_index = vert_curr_index;
            vert_curr_index++;
        }
        else
        { //这条边已经被加密过
            new_vert_index = Edge_Child_Vert[face->Line4Face[0]];
        }
        //加密产生的新边
        if (Edge_Child_Line[face->Line4Face[0]] == -1)
        {
            newline = &(Lines[line_curr_index]);
            newline->Index = line_curr_index;
            newline->Vert4Line[0] = new_vert_index;
            newline->Vert4Line[1] = line->Vert4Line[1];
            newline->BD_ID = line->BD_ID;
            line->Vert4Line[1] = new_vert_index;
            Edge_Child_Line[face->Line4Face[0]] = line_curr_index;
            new_line_index1 = line_curr_index;
            line_curr_index++;
        }
        else
        {
            new_line_index1 = Edge_Child_Line[face->Line4Face[0]];
        }
        // //内部的新边
        newline = &(Lines[line_curr_index]);
        newline->Index = line_curr_index;
        newline->Vert4Line[0] = new_vert_index;
        newline->Vert4Line[1] = face->Vert4Face[0];
        newline->BD_ID = 0;
        new_line_index2 = line_curr_index;
        line_curr_index++;
        //处理新的单元信息
        newface = &(Faces[face_curr_index]);
        newface->NumVerts = 3;
        newface->NumLines = 3;
        newface->Index = face_curr_index;
        if (face->Index < num_faces)
        { //加密的是旧单元
            mesh->Fathers[Real_Refine_Elems[i]] = face->Index;
            mesh->Fathers[face_curr_index] = face->Index;
        }
        else
        { //加密的是新产生的单元，它的father已有，不用改
            mesh->Fathers[face_curr_index] = mesh->Fathers[Real_Refine_Elems[i]];
        }
        //检查新单元下一次是否需要加密
        if (face->Line4Face[1] < num_lines)
        {
            if (Edge_Mark[face->Line4Face[1]] == 1)
            {
                Next_Refine_Elems[position] = face_curr_index;
                position++;
            }
        }
        newface->Vert4Face[0] = new_vert_index;
        newface->Vert4Face[1] = face->Vert4Face[2];
        newface->Vert4Face[2] = face->Vert4Face[0];
        newface->Line4Face[0] = face->Line4Face[1];
        newface->Line4Face[1] = new_line_index2;
        face->Line4Face[0] = face->Line4Face[2];
        face->Line4Face[2] = new_line_index2;
        j = face->Vert4Face[0];
        k = face->Vert4Face[1];
        face->Vert4Face[0] = new_vert_index;
        face->Vert4Face[1] = j;
        face->Vert4Face[2] = k;
        if (k == line->Vert4Line[0])
        {
            newface->Line4Face[2] = new_line_index1;
            face->Line4Face[1] = line->Index;
        }
        else
        {
            newface->Line4Face[2] = line->Index;
            face->Line4Face[1] = new_line_index1;
        }
        face_curr_index++;
    }
    real_refine_num = position;
    if (real_refine_num > 0)
    {
        memcpy(Real_Refine_Elems, Next_Refine_Elems, real_refine_num * sizeof(int));
    }

    //第二遍网格加密
    for (i = 0; i < real_refine_num; i++)
    {
        face = &(Faces[Real_Refine_Elems[i]]); //找到加密的面
        line = &(Lines[face->Line4Face[0]]);   //查看最长边
        //加密产生的新点
        if (Edge_Child_Vert[face->Line4Face[0]] == -1)
        { //这条边没有被加密过
            vert = &(Verts[vert_curr_index]);
            vert->Index = vert_curr_index;
            vert->Coord[0] = 0.5 * (Verts[line->Vert4Line[0]].Coord[0] + Verts[line->Vert4Line[1]].Coord[0]);
            vert->Coord[1] = 0.5 * (Verts[line->Vert4Line[0]].Coord[1] + Verts[line->Vert4Line[1]].Coord[1]);
            vert->BD_ID = line->BD_ID;
            Edge_Child_Vert[face->Line4Face[0]] = vert_curr_index;
            new_vert_index = vert_curr_index;
            vert_curr_index++;
        }
        else
        { //这条边已经被加密过
            new_vert_index = Edge_Child_Vert[face->Line4Face[0]];
        }
        //加密产生的新边
        if (Edge_Child_Line[face->Line4Face[0]] == -1)
        {
            newline = &(Lines[line_curr_index]);
            newline->Index = line_curr_index;
            newline->Vert4Line[0] = new_vert_index;
            newline->Vert4Line[1] = line->Vert4Line[1];
            newline->BD_ID = line->BD_ID;
            line->Vert4Line[1] = new_vert_index;
            Edge_Child_Line[face->Line4Face[0]] = line_curr_index;
            new_line_index1 = line_curr_index;
            line_curr_index++;
        }
        else
        {
            new_line_index1 = Edge_Child_Line[face->Line4Face[0]];
        }
        // //内部的新边
        newline = &(Lines[line_curr_index]);
        newline->Index = line_curr_index;
        newline->Vert4Line[0] = new_vert_index;
        newline->Vert4Line[1] = face->Vert4Face[0];
        newline->BD_ID = 0;
        new_line_index2 = line_curr_index;
        line_curr_index++;
        //处理新的单元信息
        newface = &(Faces[face_curr_index]);
        newface->NumVerts = MAXVERT4FACE;
        newface->NumLines = MAXLINE4FACE;
        newface->Index = face_curr_index;
        if (face->Index < num_faces)
        { //加密的是旧单元
            mesh->Fathers[Real_Refine_Elems[i]] = face->Index;
            mesh->Fathers[face_curr_index] = face->Index;
        }
        else
        { //加密的是新产生的单元，它的father已有，不用改
            mesh->Fathers[face_curr_index] = mesh->Fathers[Real_Refine_Elems[i]];
        }
        newface->Vert4Face[0] = new_vert_index;
        newface->Vert4Face[1] = face->Vert4Face[2];
        newface->Vert4Face[2] = face->Vert4Face[0];
        newface->Line4Face[0] = face->Line4Face[1];
        newface->Line4Face[1] = new_line_index2;
        face->Line4Face[0] = face->Line4Face[2];
        face->Line4Face[2] = new_line_index2;
        j = face->Vert4Face[0];
        k = face->Vert4Face[1];
        face->Vert4Face[0] = new_vert_index;
        face->Vert4Face[1] = j;
        face->Vert4Face[2] = k;
        if (k == line->Vert4Line[0])
        {
            newface->Line4Face[2] = new_line_index1;
            face->Line4Face[1] = line->Index;
        }
        else
        {
            newface->Line4Face[2] = line->Index;
            face->Line4Face[1] = new_line_index1;
        }
        face_curr_index++;
    }
    OpenPFEM_Free(Edge_Mark);
    OpenPFEM_Free(Real_Refine_Elems);
    OpenPFEM_Free(Next_Refine_Elems);

    mesh->num_vert = New_verts;
    mesh->num_line = New_lines;
    mesh->num_face = New_faces;

    //更新sharedinfo
    ParaDataClean(paradata);
    ParaDataAdd(paradata, 1, 1, MPI_INT, (void *)Edge_Child_Vert);
    ParaDataAdd(paradata, 1, 1, MPI_INT, (void *)Edge_Child_Line);
    RECDATA *recdata = NULL;
    ParaDataCommunicate(paradata, &recdata);

    //更新点
    SHAREDGEO *sharedline, *sharedvert;
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    position = 0;
    INT *neig_child_vert = (INT *)(recdata->LineDatas[0]);
    INT *neig_child_line = (INT *)(recdata->LineDatas[1]);
    INT sharedlinenum, sharedvertnum, lineindex;
    for (i = 0; i < neignum; i++) //对每个邻居
    {
        sharedvert = &(mesh->SharedInfo->SharedVerts[i]);
        sharedline = &(mesh->SharedInfo->SharedLines[i]);
        sharedvertnum = sharedvert->SharedNum;
        sharedlinenum = sharedline->SharedNum;
        for (j = position; j < position + sharedline->SharedNum; j++)
        {
            if (neig_child_vert[j] != -1) //如果有加密点
            {
                sharedvertnum++;
            }
            if (neig_child_line[j] != -1) //如果有加密线
            {
                sharedlinenum++;
            }
        }
        sharedvert->Index = realloc(sharedvert->Index, sharedvertnum * sizeof(INT));
        sharedvert->SharedIndex = realloc(sharedvert->SharedIndex, sharedvertnum * sizeof(INT));
        sharedvert->Owner = realloc(sharedvert->Owner, sharedvertnum * sizeof(INT));
        sharedline->Index = realloc(sharedline->Index, sharedlinenum * sizeof(INT));
        sharedline->SharedIndex = realloc(sharedline->SharedIndex, sharedlinenum * sizeof(INT));
        sharedline->Owner = realloc(sharedline->Owner, sharedlinenum * sizeof(INT));
        sharedvertnum = sharedvert->SharedNum;
        sharedlinenum = sharedline->SharedNum;
        for (j = position; j < position + sharedline->SharedNum; j++)
        {
            if (neig_child_vert[j] != -1) //如果有加密点
            {
                lineindex = recdata->LineIndex[j];
                sharedvert->Index[sharedvertnum] = Edge_Child_Vert[lineindex];
                sharedvert->Owner[sharedvertnum] = sharedline->Owner[j - position];
                sharedvert->SharedIndex[sharedvertnum] = neig_child_vert[j];
                sharedvertnum++;
            }
            if (neig_child_line[j] != -1) //如果有加密线
            {
                lineindex = recdata->LineIndex[j];
                sharedline->Index[sharedlinenum] = Edge_Child_Line[lineindex];
                sharedline->Owner[sharedlinenum] = sharedline->Owner[j - position];
                sharedline->SharedIndex[sharedlinenum] = neig_child_line[j];
                sharedlinenum++;
            }
        }
        position += sharedline->SharedNum;
        sharedvert->SharedNum = sharedvertnum;
        sharedline->SharedNum = sharedlinenum;
    }

    // LabelMeshSharedInfo(mesh);

    RecDataDestroy(&recdata);
    ParaDataDestroy(&paradata);
    OpenPFEM_Free(Edge_Child_Line);
    OpenPFEM_Free(Edge_Child_Vert);
}

//下面是对二维网格的一致加密
void MeshUniformRefine2D(MESH *mesh)
{
    if (mesh->Verts == NULL || mesh->Lines == NULL || mesh->Faces == NULL)
    {
        return;
    }

    //开始加密
    INT i, j, idx_tmp;
    INT num_verts, num_lines, num_faces;
    INT New_verts, New_lines, New_faces;
    INT vert0, vert1, vert2;
    //获得局部的点、线、面的个数
    num_verts = mesh->num_vert;
    num_lines = mesh->num_line;
    num_faces = mesh->num_face;
    //计算出新的点、线、面的个数
    New_verts = num_verts + num_lines;
    New_lines = 2 * num_lines + 3 * num_faces;
    New_faces = 4 * num_faces;
    //为新的点、线、面建立相应的存储空间
    VertsReCreate(&(mesh->Verts), num_verts, New_verts, mesh->worlddim);
    LinesReCreate(&(mesh->Lines), num_lines, New_lines);
    FacesReCreate(&(mesh->Faces), num_faces, New_faces);
    //产生父亲单元的信息
    if (!mesh->Fathers)
    {
        mesh->Fathers = malloc(New_faces * sizeof(int));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, New_faces * sizeof(int));
    }
    INT *Fathers = mesh->Fathers;
    VERT *Verts = mesh->Verts, *vert, *vert_s, *vert_e;
    LINE *Lines = mesh->Lines, *line, *line_old;
    FACE *Faces = mesh->Faces, *face, *face_old;
    // renew the datas of the mesh
    mesh->num_vert = New_verts;
    mesh->num_line = New_lines;
    mesh->num_face = New_faces;

    //产生新的点的数据
    for (i = 0; i < num_lines; i++)
    {
        vert = &(Verts[num_verts + i]);
        line = &(Lines[i]);
        vert_s = &(Verts[line->Vert4Line[0]]);
        vert_e = &(Verts[line->Vert4Line[1]]);
        //计算新节点的坐标
        vert->Coord[0] = 0.5 * (vert_s->Coord[0] + vert_e->Coord[0]);
        vert->Coord[1] = 0.5 * (vert_s->Coord[1] + vert_e->Coord[1]);
        //确定新点的边界信息
        vert->BD_ID = line->BD_ID;
        //产生新节点的编号
        vert->Index = num_verts + i;
    }

    //计算新产生的内部线的信息
    for (i = 0; i < num_faces; i++)
    {
        face = &(Faces[i]);
        //目前单元中新的线的编号
        //第一条线(与节点0对应的线)
        idx_tmp = 2 * num_lines + 3 * i;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[1] + num_verts;
        line->Vert4Line[1] = face->Line4Face[2] + num_verts;
        line->BD_ID = 0; // 0表示是区域内部
        line->Index = 2 * num_lines + 3 * face->Index;
        //第二条线
        idx_tmp = 2 * num_lines + 3 * i + 1;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[2] + num_verts;
        line->Vert4Line[1] = face->Line4Face[0] + num_verts;
        line->BD_ID = 0;
        line->Index = 2 * num_lines + 3 * face->Index + 1;
        //第三条线
        idx_tmp = 2 * num_lines + 3 * i + 2;
        line = &(Lines[idx_tmp]);
        line->Vert4Line[0] = face->Line4Face[0] + num_verts;
        line->Vert4Line[1] = face->Line4Face[1] + num_verts;
        line->BD_ID = 0;
        line->Index = 2 * num_lines + 3 * face->Index + 2;
    }

    //处理产生新的面，每个面生成4个面
    for (i = 0; i < num_faces; i++)
    {
        face_old = &(Faces[i]);

        // second
        idx_tmp = num_faces + 3 * i;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[2];
        face->Vert4Face[1] = face_old->Vert4Face[1];
        face->Vert4Face[2] = num_verts + face_old->Line4Face[0];
        line = &(Lines[face_old->Line4Face[0]]);
        if (face_old->Vert4Face[1] == line->Vert4Line[0])
        {
            face->Line4Face[0] = face_old->Line4Face[0];
        }
        else
        {
            face->Line4Face[0] = num_lines + face_old->Line4Face[0];
        }
        face->Line4Face[1] = 2 * num_lines + 3 * i + 1;
        line = &(Lines[face_old->Line4Face[2]]);
        if (face_old->Vert4Face[0] == line->Vert4Line[0])
        {
            face->Line4Face[2] = num_lines + face_old->Line4Face[2];
        }
        else
        {
            face->Line4Face[2] = face_old->Line4Face[2];
        }
        face->Index = num_faces + 3 * face_old->Index;

        // third
        idx_tmp = num_faces + 3 * i + 1;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[1];
        face->Vert4Face[1] = num_verts + face_old->Line4Face[0];
        face->Vert4Face[2] = face_old->Vert4Face[2];
        line = &(Lines[face_old->Line4Face[0]]);
        if (face_old->Vert4Face[1] == line->Vert4Line[0])
        {
            face->Line4Face[0] = num_lines + face_old->Line4Face[0];
        }
        else
        {
            face->Line4Face[0] = face_old->Line4Face[0];
        }
        line = &(Lines[face_old->Line4Face[1]]);
        if (face_old->Vert4Face[2] == line->Vert4Line[0])
        {
            face->Line4Face[1] = face_old->Line4Face[1];
        }
        else
        {
            face->Line4Face[1] = num_lines + face_old->Line4Face[1];
        }
        face->Line4Face[2] = 2 * num_lines + 3 * i + 2;
        face->Index = num_faces + 3 * face_old->Index + 1;

        // fourth
        idx_tmp = num_faces + 3 * i + 2;
        face = &(Faces[idx_tmp]);
        //得到父亲单元的编号
        Fathers[idx_tmp] = i;
        face->Vert4Face[0] = num_verts + face_old->Line4Face[0];
        face->Vert4Face[1] = num_verts + face_old->Line4Face[1];
        face->Vert4Face[2] = num_verts + face_old->Line4Face[2];
        face->Line4Face[0] = 2 * num_lines + 3 * i;
        face->Line4Face[1] = 2 * num_lines + 3 * i + 1;
        face->Line4Face[2] = 2 * num_lines + 3 * i + 2;
        face->Index = num_faces + 3 * face_old->Index + 2;

        // first
        //得到父亲单元的编号
        Fathers[i] = i;
        vert0 = face_old->Vert4Face[0];
        vert1 = face_old->Vert4Face[1];
        vert2 = face_old->Vert4Face[2];
        face_old->Vert4Face[1] = num_verts + face_old->Line4Face[2];
        face_old->Vert4Face[2] = num_verts + face_old->Line4Face[1];
        face_old->Line4Face[0] = 2 * num_lines + 3 * i;
        line = &(Lines[face_old->Line4Face[1]]);
        if (vert2 == line->Vert4Line[0])
        {
            face_old->Line4Face[1] = num_lines + face_old->Line4Face[1];
        }
        else
        {
            face_old->Line4Face[1] = face_old->Line4Face[1];
        }
        line = &(Lines[face_old->Line4Face[2]]);
        if (vert0 == line->Vert4Line[0])
        {
            face_old->Line4Face[2] = face_old->Line4Face[2];
        }
        else
        {
            face_old->Line4Face[2] = num_lines + face_old->Line4Face[2];
        }
    }

    //最后处理原来的线(一分为二)
    for (i = 0; i < num_lines; i++)
    {
        line_old = &(Lines[i]);
        line = &(Lines[num_lines + i]);
        line->Vert4Line[0] = num_verts + i;
        line->Vert4Line[1] = line_old->Vert4Line[1];
        line->BD_ID = line_old->BD_ID;
        line->Index = num_lines + line_old->Index;
        line_old->Vert4Line[1] = num_verts + i;
    }

#if MPI_USE
    if (mesh->SharedInfo != NULL)
    {
        MPI_Comm comm = mesh->comm;
        MPI_Comm_rank(comm, &rank);
        MPI_Comm_size(comm, &size);
        INT *geonum = malloc(2 * sizeof(INT));
        geonum[0] = num_verts;
        geonum[1] = num_lines;
        PARADATA *paradata = NULL;
        ParaDataCreate(&paradata, mesh);
        ParaDataAdd(paradata, DOMAINDATA, 2, MPI_INT, (void *)geonum);
        RECDATA *recdata = NULL;
        ParaDataCommunicate(paradata, &recdata);
        INT *neiggeonum = (INT *)(recdata->SubDomianData[0]);
        INT neignum = mesh->SharedInfo->NumNeighborRanks;

        SHAREDGEO *sharedline, *sharedvert;
        INT vertnum, newvertnum, linenum;
        for (i = 0; i < neignum; i++) //对每个邻居进行处理
        {
            sharedline = &(mesh->SharedInfo->SharedLines[i]);
            //处理点
            sharedvert = &(mesh->SharedInfo->SharedVerts[i]);
            vertnum = sharedvert->SharedNum;
            newvertnum = sharedline->SharedNum;
            sharedvert->Index = realloc(sharedvert->Index, (vertnum + newvertnum) * sizeof(INT));
            sharedvert->Owner = realloc(sharedvert->Owner, (vertnum + newvertnum) * sizeof(INT));
            sharedvert->SharedIndex = realloc(sharedvert->SharedIndex, (vertnum + newvertnum) * sizeof(INT));
            for (j = 0; j < newvertnum; j++)
            {
                //线上的点新编号为线编号+点的个数
                sharedvert->Index[vertnum + j] = sharedline->Index[j] + num_verts;
                sharedvert->Owner[vertnum + j] = sharedline->Owner[j];
                sharedvert->SharedIndex[vertnum + j] = sharedline->SharedIndex[j] + neiggeonum[2 * i];
            }
            sharedvert->SharedNum = vertnum + newvertnum;
            //处理线
            linenum = newvertnum;
            sharedline->Index = realloc(sharedline->Index, 2 * linenum * sizeof(INT));
            sharedline->Owner = realloc(sharedline->Owner, 2 * linenum * sizeof(INT));
            sharedline->SharedIndex = realloc(sharedline->SharedIndex, 2 * linenum * sizeof(INT));
            for (j = 0; j < linenum; j++)
            {
                //线上的点新编号为线编号+点的个数
                sharedline->Index[linenum + j] = sharedline->Index[j] + num_lines;
                sharedline->Owner[linenum + j] = sharedline->Owner[j];
                sharedline->SharedIndex[linenum + j] = sharedline->SharedIndex[j] + neiggeonum[2 * i + 1];
            }
            sharedline->SharedNum = 2 * linenum;
        }
        RecDataDestroy(&recdata);
        ParaDataDestroy(&paradata);
    }
#endif
} // end of UniformRefineMesh2D

//下面是自适应加密的主程序
//参数说明：
//  mesh: 输入的网格
//  ElemErrors： 后验误差估计子，即每个单元上都有一个值来表示该单元上的误差
//  theta: 自适应加密系数，与自适应迭代算法速度相关的参数
void MeshAdaptiveRefine2D(MESH *mesh, DOUBLE *ElemErrors, DOUBLE theta)
{
    // REFINE_ELEMS为需要加密的单元编号
    INT certain_refine_num;
    INT *REFINE_ELEMS = malloc(mesh->num_face * sizeof(INT));
    //寻找加密单元
    FindRefineElem(mesh, ElemErrors, theta, &certain_refine_num, REFINE_ELEMS);
    MeshBisectionRefine2D(mesh, certain_refine_num, REFINE_ELEMS);
    OpenPFEM_Free(REFINE_ELEMS);
}

//下面是根据后验误差寻找加密单元的函数
//参数信息：
//  mesh: 输入的网格
//  PosterioriError： 计算出的每个单元的后验误差估计子
//  theta: 自适应加密系数，与自适应迭代算法速度相关的参数
//  certain_refine_num：返回需要加密的单元的个数
//  REFINE_ELEMS：返回需要加密的单元编号
// 2022.10.17修改版本
void FindRefineElem(MESH *mesh, DOUBLE *PosterioriError, DOUBLE theta, INT *certain_refine_num, INT *REFINE_ELEMS)
{
    //将区间划分为partition_num份后进行判断
    // PARDITION_NUM是设定好的固定的全局变量
    INT partition_num = PARDITION_NUM;
    //获取进程总数和本进程的进程号
    INT myrank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    INT i, j;
    INT num_elem;
    //记录循环次数
    INT roll_num = 0;
    INT levelind, elemind;
    INT position_left, position_right;
    //这个数组储存每个区间后验误差起始的位置编号 也就是编号小于start_pos[i]的单元的误差大于level[i]
    INT *start_pos = malloc((partition_num + 1) * sizeof(INT));
    start_pos[0] = 0; //初始化第一个区间对应的起始的位置编号
    // tol表示最后的误差精度
    // tol_length表示区间长度的阈值 区间长度低于阈值退出循环
    DOUBLE tol_length = 1.0e-11; //此为DOUBLE存储时最小的阈值 如果次值取得过小 在循环次数很大时length会存不下变为0
    // tol_equal为判断两个DOUBLE数是否相等的阈值 小于该阈值则认为两个数是相等的 (两个DOUBLE数没有办法直接用==判断是否相等)
    DOUBLE tol_equal = 1.0e-12;
    // criteration_exact记录\theta*(\sum_K \eta_K)
    //我们要选取出一个最小的集合使得这个集合中的误差总和大于criteration_exact
    DOUBLE criteration_exact;
    // length表示将区间划分成partition_num份之后每个区间的长度
    DOUBLE length;
    // sum_max_min这个数组的三个位置分别表示后验误差的在局部区间内的 求和 最大值 最小值
    DOUBLE sum_max_min[3];
    // level这个数组记录区间的端点值，长度比区间数多1
    DOUBLE *level = malloc((partition_num + 1) * sizeof(DOUBLE));
    //创建这个结构体 储存每个节点之前的后验误差和 以及 每个区间中后验误差的个数
    //这里用新结构体操作是方便之后同时做通讯
    LEVELSUMNUM *sumnum;
    sumnum = malloc(sizeof(LEVELSUMNUM));
    // level_sum记录本进程每个端点值对应的大于该端点值的后验误差和
    DOUBLE *level_sum = &(sumnum->level_sum[0]);
    // level_num记录每个区间中后验误差的个数
    INT *level_num = &(sumnum->level_num[0]);
    //全局部分 创建这个结构体 储存每个节点之前的后验误差和 以及 每个区间中后验误差的个数
    LEVELSUMNUM *global_sumnum;
    global_sumnum = malloc(sizeof(LEVELSUMNUM));
    // global_level_sum记录所有进程中每个端点值对应的大于该端点值的后验误差和
    DOUBLE *global_level_sum = &(global_sumnum->level_sum[0]);
    // global_level_num记录所有进程中每个区间中后验误差的个数
    INT *global_level_num = &(global_sumnum->level_num[0]);
    //初始化为0 再对sumnum中的两个值进行计算(累加)
    for (i = 0; i < PARDITION_NUM + 1; i++)
    {
        sumnum->level_sum[i] = 0.0;
    }
    for (i = 0; i < PARDITION_NUM; i++)
    {
        sumnum->level_num[i] = 0;
    }

    switch (mesh->worlddim)
    {
    case 2:
        num_elem = mesh->num_face;
        break;
    case 3:
        num_elem = mesh->num_volu;
        break;
    }
    //首先生成后验误差对应的单元编号，以后验误差从大到小的标准对二者同时进行排序 以保持后验误差和编号的对应关系
    //这里找出每个单元对应的编号
    for (i = 0; i < num_elem; i++)
    {
        REFINE_ELEMS[i] = i;
    }
    //下面分三步来对后验误差从大到小进行排列 1.变号 2.排序 3.变号
    //这里先对后验误差取负数，方便之后从小到大排序
    for (i = 0; i < num_elem; i++)
    {
        PosterioriError[i] = -PosterioriError[i];
    }
    //首先对后验误差进行从小到大排序，此时REFINE_ELEMS是根据后验误差的转换方式转换的
    QuickSortRealValueVector(PosterioriError, REFINE_ELEMS, 0, num_elem - 1);
    //这里先对后验误差取负数，恢复原来的值
    for (i = 0; i < num_elem; i++)
    {
        PosterioriError[i] = -PosterioriError[i];
    }
    //直接选取后验误差的最大值
    sum_max_min[1] = PosterioriError[0];
    //遍历对本进程后验误差求和
    sum_max_min[0] = 0.0;
    //计算后验误差的总和 \sum_K \eta_K
    for (i = 0; i < num_elem; i++)
    {
        sum_max_min[0] += PosterioriError[i];
    }
    //直接选取后验误差的最小值
    sum_max_min[2] = PosterioriError[num_elem - 1];
    //下面对信息进行同步 这里可以把三个allreduce进行合并 同时allreduce
    // MPI_Allreduce(MPI_IN_PLACE, &sum_max_min[0], 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
    // MPI_Allreduce(MPI_IN_PLACE, &sum_max_min[1], 1, MPI_DOUBLE, MPI_MAX, mesh->comm);
    // MPI_Allreduce(MPI_IN_PLACE, &sum_max_min[2], 1, MPI_DOUBLE, MPI_MIN, mesh->comm);
    //下面是合并的情况
    // 通讯 完成：1. 对各个进程中的和sum_max_min[0]取求和 结果存到各个进程中sum_max_min[0]原位置
    //          2.对各个进程中的和sum_max_min[1]取最大值 结果存到各个进程中sum_max_min[1]原位置
    //          3.对各个进程中的和sum_max_min[2]取最小值 结果存到各个进程中sum_max_min[2]原位置
    MPI_Op MPI_SUMMAXMIN;
    MPI_Op_create((MPI_User_function *)dsummaxminOP, 1, &MPI_SUMMAXMIN);
    MPI_Allreduce(MPI_IN_PLACE, &sum_max_min[0], 3, MPI_DOUBLE, MPI_SUMMAXMIN, mesh->comm);
    //为下面循环中的信息传输做准备
    //首先生成和结构体LEVELSUMNUM对应的mpi数据类型并提交
    //提交该数据类型是为了在level_sum数组和level_num数组的对应位置求和时 同时进行通讯
    MPI_Datatype mpi_sum_num;
    int blocklens_array[2];
    MPI_Aint displs_array[2];
    MPI_Datatype old_type_array[2];
    LEVELSUMNUM sumnum1;
    old_type_array[0] = MPI_DOUBLE;
    old_type_array[1] = MPI_INT;
    blocklens_array[0] = PARDITION_NUM + 1;
    blocklens_array[1] = PARDITION_NUM;
    MPI_Address(&sumnum1.level_num[0], &displs_array[1]); //第一个INT型相对于MPI_BOTTOM的偏移
    MPI_Address(&sumnum1.level_sum[0], &displs_array[0]);
    displs_array[1] = displs_array[1] - displs_array[0];
    displs_array[0] = 0;
    MPI_Type_struct(2, blocklens_array, displs_array, old_type_array, &mpi_sum_num);
    MPI_Type_commit(&mpi_sum_num);
    //生成结构体上的运算方式：两个数组对应位置全局求和
    MPI_Op MPI_ISUMDSUM;
    MPI_Op_create((MPI_User_function *)dsumisumOP, 1, &MPI_ISUMDSUM);

    // criteration_exact记录\theta*(\sum_K \eta_K)
    //我们要选取出一个最小的集合使得这个集合中的误差总和大于criteration_exact
    criteration_exact = sum_max_min[0] * theta;
    level_sum[0] = 0.0;
    while (1)
    {
        roll_num++;
        //对我们关注的区间等分为partition_num份 分析每个新得到的小区间内误差的情况 从而判断我们要选择那些误差
        length = (sum_max_min[1] - sum_max_min[2]) / partition_num;
        //新区间的第一个区间的最大值(右端点)为上一次确定下的大区间的最大值(右端点)，即上次关注区间的最大值，存储在sum_max_min[1]
        level[0] = sum_max_min[1];
        //根据小区间长度 依次得到每个区间端点的值
        for (levelind = 1; levelind < partition_num + 1; levelind++)
        {
            level[levelind] = level[levelind - 1] - length;
        }

        //对从大到小排列的单元误差进行遍历，找到每个区间端点 对应 后验误差小于等于该端点值对应的本进程中的后验误差的起始的位置，节点也是从大到小进行排列的
        //找到每个区间 大于该区间最小值 小于等于该区间最大值 的本进程中的后验误差的局部和
        //由于level[0]为大区间所有误差的最大值 所以这里只需要从level[1]开始进行循环
        for (levelind = 1; levelind < partition_num; levelind++)
        {
            for (elemind = start_pos[levelind - 1]; elemind < num_elem; elemind++)
            {
                //这里如果PosterioriError[elemind]>level[levelind] 就表示这个后验误差在当前区间里
                //否则 就表示后验误差进入了下一个区间里
                if (PosterioriError[elemind] > level[levelind])
                {
                    // level_sum 是按节点之前计算的 所以和节点编号是对应的
                    // level_sum 先统计大于本节点 小于等于上一个节点的部分和 之后再进行累加
                    level_sum[levelind] += PosterioriError[elemind];
                }
                else
                {
                    //如果后验误差进入了下一个区间 那么将这个后验误差的编号设置为新的start位置
                    //同时跳出本次elemind的循环进入下一次levelind的循环
                    start_pos[levelind] = elemind;
                    elemind = num_elem + 1;
                }
            }
            //如果对所有单元都循环完都大于区间下界，那么设对应的start_pos为num_elem
            if (elemind < num_elem + 1)
            {
                start_pos[levelind] = num_elem;
            }
        }
        //最后一个区间需要设置成>= 把和最小值相等的部分误差也包含进来
        for (elemind = start_pos[partition_num - 1]; elemind < num_elem; elemind++)
        {
            //这里如果PosterioriError[elemind]>=level[levelind] 就表示这个后验误差在当前区间里
            //这里不能直接用>= 直接用>=会导致相等的部分没有被包括进来
            //否则 就表示后验误差进入了下一个区间里
            if (PosterioriError[elemind] >= level[partition_num] - tol_equal)
            {
                // level_sum 是按节点之前计算的 所以和节点编号是对应的
                // level_sum 先统计大于本节点 小于上一个节点的部分和 之后再进行累加
                level_sum[partition_num] += PosterioriError[elemind];
            }
            else
            {
                //如果后验误差进入了下一个区间 那么将这个后验误差的编号设置为新的start位置
                //同时跳出本次elemind的循环进入下一次levelind的循环
                start_pos[partition_num] = elemind;
                elemind = num_elem + 1;
            }
        }
        //如果对所有单元都循环完都大于区间下界，那么设最后的start_pos为num_elem
        if (elemind < num_elem + 1)
        {
            start_pos[partition_num] = num_elem;
        }

        //下面计算每个区间端点 大于该端点的本进程中的后验误差的部分和
        //计算每个区间内部 大于区间最小值 小于等于区间最大值 本进程中后验误差的个数
        for (levelind = 1; levelind < partition_num + 1; levelind++)
        {
            //把区间的sum累加成节点之前的sum
            level_sum[levelind] += level_sum[levelind - 1];
            //计算每个区间的后验误差个数
            level_num[levelind - 1] = start_pos[levelind] - start_pos[levelind - 1];
        }

        //下面对level_sum level_num进行同步，可以将这两个allreduce合并
        // MPI_Allreduce(level_sum, global_level_sum, partition_num + 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
        // MPI_Allreduce(MPI_IN_PLACE, level_num, partition_num, MPI_INT, MPI_SUM, mesh->comm);
        //下面把这两个过程合并
        //通讯 实现：1.将数组sumnum->level_num(区间中后验误差个数)中各个位置进行进程间的全进程对应位置求和 储存到各个进程的global_sumnum->level_num
        //         2.将数组sumnum->level_sum(节点前后验误差总和)中各个位置进行进程间的全进程对应位置求和 储存到各个进程的global_sumnum->level_sum
        MPI_Allreduce(sumnum, global_sumnum, 1, mpi_sum_num, MPI_ISUMDSUM, mesh->comm);
        //下面寻找criteration_exact在global_level_sum的那个区间中
        // global_level_sum[position_left] <= criteration_exact < global_level_sum[position_right]
        findPosition(global_level_sum, 0, partition_num, criteration_exact, &position_left, &position_right, 1);
        
        //区间[level(position_right), level(position_left)]对应的后验误差个数是level_num[position_left]
        //如果所有进程中该区间中的后验误差个数为1 那么将这个后验误差包括进来 即加密的单元个数为start_pos[position_right]
        if (global_level_num[position_left] < 2)
        {
            certain_refine_num[0] = start_pos[position_right];
            break;
        }
        //当区间长度非常小时，由于DOUBLE类型存储的精度有限，我们在下一次计算length时，会无法存储导致length为0
        //上面的过程中已经确定了区间[level(position_right), level(position_left)]，
        //因此当这种情况出现时，我们对此时挑选出来的区间[level(position_right), level(position_left)]中的后验误差进行全局的排序
        //全局排序后挑选最大的我们需要的一部分误差 使得总和大于等于criteration_exact
        if (length < tol_length)
        {
            INT *num_rank;       // num_rank记录每个进程中该区间的单元个数
            DOUBLE *gather_post; // gather_post记录收集到的后验误差
            INT *end_rank;       // end_rank记录每个进程误差结束的位置(所有进程中该区间的误差合并到同一进程中后)
            INT *elem_num;       // elem_num记录本进程中单元的新的编号 = 进程号小于本进程进程号的进程中本区间中单元个数 + 本单元在本进程中区间内的局部编号
            INT *final_num;      // final_num记录每个进程最后选出来的后验误差个数
            INT my_num;          // my_num记录本进程在该区间中最后选出来的后验误差个数

            num_rank = malloc(nprocs * sizeof(INT));
            //收集各个进程 本区间内元素的个数 同步到各个区间中的num_rank   num_rank[i]=第i个进程中该区间内有多少个后验误差
            MPI_Allgather(&(level_num[position_left]), 1, MPI_INT, num_rank, 1, MPI_INT, mesh->comm);

            //将误差合并到根进程 排序挑选后再返回给各个进程最后挑选出的单元个数
            if (myrank == ROOT_RANK)
            {
                INT temp_end = 0;
                // 累加和从而计算出每个进程结束的位置 得到的end_rank用于确定单元属于哪个进程
                // 小于end_rank[i] 大于end_rank[i-1]的编号对应的后验误差来自i-1进程
                end_rank = malloc(nprocs * sizeof(INT));
                for (i = 0; i < nprocs; i++)
                {
                    temp_end += num_rank[i];
                    end_rank[i] = temp_end;
                }
                // 根进程收集本区间中不同进程的后验误差 按进程号从小到大的顺序存储到gather_post
                gather_post = malloc(global_level_num[position_left] * sizeof(DOUBLE));
                // 0进程
                for (i = 0; i < num_rank[0]; i++)
                {
                    gather_post[i] = PosterioriError[start_pos[position_left] + i];
                }
                // 其他进程
                for (i = 1; i < nprocs; i++)
                {
                    MPI_Recv(&(gather_post[end_rank[i - 1]]), num_rank[i], MPI_DOUBLE, i, 0, mesh->comm, MPI_STATUS_IGNORE);
                }
                //确定收集到的后验误差的新编号 在将后验误差由大到小排序时，将新编号同步进行排序，从而保证编号和误差的一一对应关系
                elem_num = malloc(global_level_num[position_left] * sizeof(INT));
                elem_num[0] = 0;
                for (i = 1; i < global_level_num[position_left]; i++)
                {
                    elem_num[i] = elem_num[i - 1] + 1;
                }
                //对该区间内的后验误差进行全进程由大到小排序 并将编号同步改变 1.变号 2.排序 3.变号
                for (i = 0; i < global_level_num[position_left]; i++)
                {
                    gather_post[i] = -gather_post[i];
                }
                QuickSortRealValueVector(gather_post, elem_num, 0, global_level_num[position_left] - 1);
                for (i = 0; i < global_level_num[position_left]; i++)
                {
                    gather_post[i] = -gather_post[i];
                }
                // 确定每个进程最后需要标记几个单元
                final_num = malloc(nprocs * sizeof(INT));      //记录每个进程最后需要标记几个单元
                memset(final_num, 0, nprocs * sizeof(INT));    //初始化为0
                DOUBLE temp = global_level_sum[position_left]; // temp取出大于该区间的最大值的后验误差的全局总和
                //在temp上不断累加剩下的部分中最大的误差 直至temp的值>=criteration_exact
                if (temp < criteration_exact)
                {
                    for (i = 0; i < global_level_num[position_left]; i++)
                    {
                        temp += gather_post[i];
                        //找到该单元属于哪个进程 累加到对应的final_num
                        for (j = 0; j < nprocs; j++)
                        {
                            if (elem_num[i] < end_rank[j])
                            {
                                final_num[j]++;
                                j = nprocs;
                            }
                        }
                        //满足误差要求退出循环
                        if (temp >= criteration_exact)
                        {
                            i = global_level_num[position_left];
                        }
                    }
                }
                // 下面将实际选择的单元个数发给各个进程
                // 进程0
                my_num = final_num[0];
                // 其他进程
                for (i = 1; i < nprocs; i++)
                {
                    MPI_Send(&(final_num[i]), 1, MPI_INT, i, 1, mesh->comm);
                }
                free(final_num);
                free(elem_num);
                free(gather_post);
                free(end_rank);
            }
            else
            {
                //每个进程发送给零进程 本进程的误差
                MPI_Send(&(PosterioriError[start_pos[position_left]]), num_rank[myrank], MPI_DOUBLE, ROOT_RANK, 0, mesh->comm);
                //每个进程接收零进程发来的 本进程要挑选多少个误差值
                MPI_Recv(&my_num, 1, MPI_INT, ROOT_RANK, 1, mesh->comm, MPI_STATUS_IGNORE);
            }
            free(num_rank);

            // printf("进程%d 在小区间中筛选后最终确定的小区间中的个数%d\n", myrank, my_num);
            certain_refine_num[0] = start_pos[position_left] + my_num;

            if (myrank == 0)
                printf("循环次数%d  因为区间长度小于阈值退出！\n", roll_num);
            break;
        }
        //如果criteration_exact和两个求和的端点值(对应的大于level_sum的后验误差的全局和)近似相等时 停止循环
        //如果fabs(level_sum[position_left]-criteration_exact)<tol_equal，即level_sum[position_left]刚好满足我们的要求
        //那么大于level[position_left]的后验误差对应的单元都需要加密 即加密的单元个数为start_pos[position_left]
        // if ((criteration_exact - global_level_sum[position_left]) == 0.0)
        // 下面考虑到两个DOUBLE数不能直接判断相等 所以当小于阈值就认为相等
        if ((criteration_exact - global_level_sum[position_left]) < tol_equal)
        {
            certain_refine_num[0] = start_pos[position_left];
            if (myrank == 0)
                printf("循环次数%d  因为挑选出相对精确的部分退出！\n", roll_num);
            break;
        }
        //如果fabs(level_sum[position_right]-criteration_exact)<tol_equal，即level_sum[position_right]刚好满足我们的要求
        //那么大于level[position_right]的后验误差对应的单元都需要加密 即加密的单元个数为start_pos[position_right]
        if ((global_level_sum[position_right] - criteration_exact) < tol_equal)
        {
            certain_refine_num[0] = start_pos[position_right];
            if (myrank == 0)
                printf("循环次数%d  因为挑选出相对精确的部分退出！\n", roll_num);
            break;
        }
        //如果以上的退出条件均不满足 就继续对单元进行细分 下面对信息进行更新
        //更新区间的两个端点 将选定的小区间的上下界作为下次循环中大区间的上下界
        sum_max_min[1] = level[position_left];
        sum_max_min[2] = level[position_right];
        //更新start_pos[0] 为小区间对应的后验误差的起始位置
        start_pos[0] = start_pos[position_left];
        //更新level_sum 为大于新的小区间的上界的后验误差总和
        sumnum->level_sum[0] = level_sum[position_left];
        //清空level_sum 进入下次循环
        for (i = 1; i < PARDITION_NUM + 1; i++)
        {
            sumnum->level_sum[i] = 0.0;
        }
    }
    free(global_sumnum);
    free(sumnum);
    free(level);
    free(start_pos);
    //检查最后是否计算正确
    //计算所有进程确定加密的单元的误差和
    DOUBLE sum_refine_post = 0.0;
    INT indsum;
    for (indsum = 0; indsum < certain_refine_num[0]; indsum++)
    {
        sum_refine_post += PosterioriError[indsum];
    }
    MPI_Allreduce(MPI_IN_PLACE, &sum_refine_post, 1, MPI_DOUBLE, MPI_SUM, mesh->comm);
    INT refine_num_all;
    MPI_Allreduce(&(certain_refine_num[0]), &refine_num_all, 1, MPI_INT, MPI_SUM, mesh->comm);

    // printf("进程%d中标记加密的单元个数为 %d\n", myrank, certain_refine_num[0]);
    //下面三个函数为后验误差挑选结果的检测函数：
    // 1.判断挑选出的误差值是不是全进程中的后验误差中最大的部分
    // 2.每个进程中的后验误差已经完成从大到小的排序
    // 3.每个进程最后确定的加密单元为排序后的第0个到第certain_refine_num[0]-1个 下面判断这些后验误差是否比未选择的部分都大
    DOUBLE MinInRefine;
    if (certain_refine_num[0] > 0)
    {
        MinInRefine = PosterioriError[certain_refine_num[0] - 1]; //本进程中 标记为加密的单元中最小的误差值
    }
    else
    {
        MinInRefine = 100.0; //本进程无加密单元 不参与取MIN
    }
    DOUBLE MaxInNoRefine = PosterioriError[certain_refine_num[0]];                   //本进程中 未标记为加密的单元中最大的误差值
    MPI_Allreduce(MPI_IN_PLACE, &MinInRefine, 1, MPI_DOUBLE, MPI_MIN, mesh->comm);   //所有进程中 标记为加密的单元中最小的误差值
    MPI_Allreduce(MPI_IN_PLACE, &MaxInNoRefine, 1, MPI_DOUBLE, MPI_MAX, mesh->comm); //所有进程中 未标记为加密的单元中最大的误差值
    if ((MaxInNoRefine - MinInRefine) > 0)
    {
        printf("标记加密单元的误差值不是最大的！\n");
        if ((MaxInNoRefine - MinInRefine) < 1.0e-13)
        {
            printf("但是标记为加密单元的最小误差值 和 未标记为加密单元的最大误差值 近似相等\n");
        }
    }
    // 2.判断如果去掉某一个误差是否低于标准值 如果不是就说明结果错误
    if (certain_refine_num[0] > 0)
    {
        if ((sum_refine_post - PosterioriError[certain_refine_num[0] - 1]) >= criteration_exact)
        {
            printf("这里多挑选了误差值!!!  myrank%d\n", myrank);
            printf("check: %2.20f %2.20f %2.20f %2.20f\n", PosterioriError[certain_refine_num[0] - 4], PosterioriError[certain_refine_num[0] - 3], PosterioriError[certain_refine_num[0] - 2], PosterioriError[certain_refine_num[0] - 1]);
        }
    }
    // 3.判断挑选出的误差值总和是否大于标准值(如果是情况3退出的也会出现这种情况！)
    if (sum_refine_post < criteration_exact)
    {
        printf("Warning!!!!!!!!!!!!!\n");
    }
    return;
}

//针对DOUBLE[3]的运算操作符 每个位置分别对应运算 求和 求最大值 求最小值
void dsummaxminOP(DOUBLE *in, DOUBLE *inout, int *len, MPI_Datatype *datatype)
{
    inout[0] = in[0] + inout[0];
    if (in[1] > inout[1])
    {
        inout[1] = in[1];
    }
    else
    {
        inout[1] = inout[1];
    }
    if (in[2] > inout[2])
    {
        inout[2] = inout[2];
    }
    else
    {
        inout[2] = in[2];
    }
}

//针对一个DOUBLE一个INT组合的操作符 对应的操作均为求和
void dsumisumOP(LEVELSUMNUM *in, LEVELSUMNUM *inout, int *len, MPI_Datatype *datatype)
{
    int i, j;
    for (i = 0; i < *len; i++)
    {
        for (j = 0; j < PARDITION_NUM + 1; j++)
        {
            inout->level_sum[j] = in->level_sum[j] + inout->level_sum[j];
        }
        for (j = 0; j < PARDITION_NUM; j++)
        {
            inout->level_num[j] = in->level_num[j] + inout->level_num[j];
        }
    }
}

//二分法寻找一个数value在数组a[start:end]中的位置, 数组长度 start-end+1,
//数组a是从大到小排列的(tag==0),数组a是从小到大排列的(tag==1)
void findPosition(DOUBLE a[], INT start, INT end, DOUBLE value, INT *left, INT *right, INT tag)
{
    if (tag == 0)
    {
        if (start > end)
            return;
        int i, j, temp;
        i = start;
        j = end;
        temp = (j + i) / 2;
        // printf("temp:%d\n",temp);
        if (value > a[temp])
        {
            if (value <= a[temp - 1])
            {
                // printf("temp:::::::\n");
                left[0] = temp - 1;
                right[0] = temp;
                return;
            }
            else
            {
                findPosition(a, start, temp - 1, value, left, right, 0);
            }
        }
        else
        {
            if (value > a[temp + 1])
            {
                left[0] = temp;
                right[0] = temp + 1;
                return;
            }
            else
            {
                findPosition(a, temp + 1, end, value, left, right, 0);
            }
        }
    }
    else
    {
        if (start > end)
            return;
        int i, j, temp;
        i = start;
        j = end;
        temp = (j + i) / 2;
        // printf("temp:%d\n",temp);
        if (value < a[temp])
        {
            if (value >= a[temp - 1])
            {
                // printf("temp:::::::\n");
                left[0] = temp - 1;
                right[0] = temp;
                return;
            }
            else
            {
                findPosition(a, start, temp - 1, value, left, right, 1);
            }
        }
        else
        {
            if (value < a[temp + 1])
            {
                left[0] = temp;
                right[0] = temp + 1;
                return;
            }
            else
            {
                findPosition(a, temp + 1, end, value, left, right, 1);
            }
        }
    }
}
