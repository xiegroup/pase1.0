#include "mesh.h"
#include "meshcomm.h"
#include "constants.h"

#define PRINT_INFO 0

int check_comm = 0;
int check_bis = 0;

INT size, rank;

void RenewAdaptiveShareInfo(REFINEMESH *refinemesh, PARADATA *paradata, INT num_lines, INT num_faces);
/*******************************************************************************************
    3维网格一致加密
        首先，需要有一个3D网格(可以通过InitialMesh3D()、InitialCube3D()等函数产生),
        格式(信息)与Mesh.h中的结构体一致
********************************************************************************************/
// 下面是对三维网格的一致加密
void MeshUniformRefine3D(MESH *mesh)
{
    // printf("1111\n");
    DOUBLE starttime, endtime;
    starttime = GetTime();
    int myrank, nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    if (mesh->Verts == NULL || mesh->Lines == NULL || mesh->Faces == NULL)
    {
        return;
    }
    // 对网格进行调整, 使得每个体中的最长边为第0条边
    // LabelMesh3D4Bisection(mesh);
    INT i, j, k, start, end, num, NP, NF, NE, NV;
    // 加密前, 点、线、面、体的个数
    NP = mesh->num_vert;
    NE = mesh->num_line;
    NF = mesh->num_face;
    NV = mesh->num_volu;
    // 加密一次, 点、线、面、体的个数
    INT tmp_nverts = NP + NE;
    INT tmp_nlines = NE * 2 + NF * 3 + NV;
    INT tmp_nfaces = NF * 4 + NV * 8;
    INT tmp_nvolus = NV * 8;
    // 为新的点、线、面、体开辟内存, 同时拷贝到新的内存
    // mesh->Verts = realloc(mesh->Verts, tmp_nverts * sizeof(VERT));
    VertsReCreate(&(mesh->Verts), NP, tmp_nverts, mesh->worlddim);
    mesh->Lines = realloc(mesh->Lines, tmp_nlines * sizeof(LINE));
    mesh->Faces = realloc(mesh->Faces, tmp_nfaces * sizeof(FACE));
    mesh->Volus = realloc(mesh->Volus, tmp_nvolus * sizeof(VOLU));
    // 定义保存父亲单元信息的对象
    // 那原来的mesh->Fathers的内存去哪了呢?
    if (mesh->Fathers == NULL)
    {
        mesh->Fathers = malloc(tmp_nvolus * sizeof(INT));
    }
    else
    {
        mesh->Fathers = realloc(mesh->Fathers, tmp_nvolus * sizeof(INT));
    }
    // 保存祖先单元的信息, 主要用于扩展子空间算法的实现
    if (mesh->Ancestors == NULL)
    {
        mesh->Ancestors = malloc(tmp_nvolus * sizeof(INT));
        for (i = 0; i < NV; i++)
            mesh->Ancestors[i] = i;
    }
    else
    {
        mesh->Ancestors = realloc(mesh->Ancestors, tmp_nvolus * sizeof(INT));
    }
    // 更新点的信息(坐标与全局编号以及边界信息), 旧点编号不变, 新点编号为 NP+边的编号. 新点坐标为所在边中点坐标.
    for (i = 0; i < NE; i++)
    {
        num = i + NP;
        for (j = 0; j < 3; j++)
        {
            // 计算新点的坐标
            mesh->Verts[num].Coord[j] = 0.5 * (mesh->Verts[mesh->Lines[i].Vert4Line[0]].Coord[j] + mesh->Verts[mesh->Lines[i].Vert4Line[1]].Coord[j]);
        } // end for(j=0; j<3; j++)
        // 新点的边界信息
        mesh->Verts[num].BD_ID = mesh->Lines[i].BD_ID;
        // 新点的全局编号
        mesh->Verts[num].Index = num;
    } // end for(i=0; i<NE; i++)
    // 更新线的信息
    // 边上的线
    for (i = 0; i < NE; i++)
    {
        num = i + NE; // 当前新的线的编号
        j = i + NP;   // 当前边的中点编号
        // mesh->Lines[i].Vert4Line[0] = mesh->Lines[i].Vert4Line[0];
        mesh->Lines[num].Vert4Line[0] = j;                           // 新的线的起点
        mesh->Lines[num].Vert4Line[1] = mesh->Lines[i].Vert4Line[1]; // 新的线的终点
        mesh->Lines[i].Vert4Line[1] = j;                             // 旧的线的终点
        // 新线的边界信息
        mesh->Lines[num].BD_ID = mesh->Lines[i].BD_ID;
        // 新线的全局编号
        mesh->Lines[num].Index = num;
    } // end for(i=0; i<NE; i++)
    FACE face;
    VOLU volu;
    INT pos, ind, ind0, ind1, bd_id, tmp, base, FaceLineVerts[6] = {4, 5, 5, 3, 3, 4};
    INT FaceChildVerts[6];
    // 面上的线
    base = 2 * NE;
    // 对所有的面循环
    for (i = 0; i < NF; i++)
    { // i是面的编号
        face = mesh->Faces[i];
        // 面上的新线的BD_ID都为面的BD_ID
        bd_id = face.BD_ID; // 该面上的新线继承面的边界
        // 得到加密的面上的6个节点的编号
        for (k = 0; k < 3; k++)
        {
            FaceChildVerts[k] = face.Vert4Face[k];
            FaceChildVerts[3 + k] = NP + face.Line4Face[k];
        }
        // 面上的新线0: 45,面上的新线1: 53, 面上的新线2: 34
        for (k = 0; k < 3; k++)
        {
            num = base + i * 3 + k;
            mesh->Lines[num].Vert4Line[0] = FaceChildVerts[FaceLineVerts[2 * k]];
            mesh->Lines[num].Vert4Line[1] = FaceChildVerts[FaceLineVerts[2 * k + 1]];
            mesh->Lines[num].Index = num;
            mesh->Lines[num].BD_ID = bd_id;
        } // end for k
    }     // end for(i=0; i<NF; i++)
    // 体内的线
    base = 2 * NE + NF * 3;
    for (i = 0; i < NV; i++)
    {
        // 将第0条边与第5条边连接起来, 所以需要保正01边是该四面体上的主边（最长边）
        num = base + i;
        mesh->Lines[num].Vert4Line[0] = mesh->Volus[i].Line4Volu[0] + NP; // 01边上线的中点编号
        mesh->Lines[num].Vert4Line[1] = mesh->Volus[i].Line4Volu[5] + NP; // 23边上线的中点编号
        // 体内的线必定为内部的线, 故取默认值0 (内部为0, 边界默认为1)
        mesh->Lines[num].BD_ID = 0;
        mesh->Lines[num].Index = num;
    } // end for(i=0; i<NV; i++)
    // 直接对加密之后的网格的点、线、面、体的个数进行赋值
    mesh->num_vert = tmp_nverts; // 网格中点的个数
    mesh->num_line = tmp_nlines; // 网格中边的个数
    mesh->num_face = tmp_nfaces; // 网格中面的个数
    mesh->num_volu = tmp_nvolus; // 网格中体的个数
    // 得到点到边的对应关系
    PTW *Vert2Lines;
    PTWCreate(&Vert2Lines);
    GetVert2Lines(mesh, Vert2Lines);
    INT *VertPos = malloc(tmp_nverts * sizeof(INT));
    memset(VertPos, 0, tmp_nverts * sizeof(INT));
    // 下面考虑产生新的四面体
    INT Vert4Volu[10];                                                   // 用来记录每个四面体上的10个局部节点编号
    INT VoluFaceVerts[12] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1};        // 记录四面体的4个面（三角形）的局部编号
    INT VoluChildFaceVerts03[12] = {4, 5, 6, 4, 8, 7, 5, 7, 9, 6, 9, 8}; // 记录四面体4个与节点对应的三角形节点的局部编号
    INT VoluChildFaceVerts49[12] = {5, 4, 9, 6, 4, 9, 7, 4, 9, 8, 4, 9}; // 记录与对角线49相联系的四面体的局部编号
    // 下面这个变量用来记录1个四面一致加密得到八个子四面体节点的局部编号，第一个四面体继承原来的编号
    INT Vert4ChildVolus[32] = {0, 4, 5, 6, 1, 4, 8, 7, 5, 7, 2, 9, 6, 8, 9, 3,
                               4, 9, 5, 6, 4, 9, 7, 5, 4, 9, 8, 7, 4, 9, 6, 8};
    INT FaceVerts[6] = {1, 2, 2, 0, 0, 1};
    // INT LocalFaceVerts[6] = {1, 2, 0, 2, 0, 1};
    INT LineVerts[12] = {0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3};
    // INT LocalVoluVerts[20] = {0, 2, 3, 1, 0, 3, 1, 2, 1, 2, 0, 3, 1, 3, 2, 0, 2, 3, 0, 1};
    // INT GVoluVerts[4], GFaceVerts[3];
    // VERT vert0, vert1;
    // DOUBLE Lens[6], maxvalue;
    // 更新体上点的信息和全局编号. 单元的编号顺序: 第一个点所在的体, 全局编号不变, 其它七个排在 NV 后面.
    for (i = 0; i < NV; i++)
    {
        // 取出当前的体
        volu = mesh->Volus[i];
        // 确定10个节点的编号
        for (k = 0; k < 4; k++)
        { // 4个四面体的端点
            Vert4Volu[k] = volu.Vert4Volu[k];
        } // end for k
        for (k = 0; k < 6; k++)
        { // 6条线的中点
            Vert4Volu[k + 4] = NP + volu.Line4Volu[k];
        } // end for k
        // 对4个内部面进行循环，给出相应的节点信息, 主边的选取按照相应的四面体上的面的主边来进行相应的选取
        for (k = 0; k < 4; k++)
        {                                          // 第k个节点对应的内部面
            face = mesh->Faces[volu.Face4Volu[k]]; // 目前的面的信息还是原来的面信息
            num = NF * 4 + i * 8 + k;
            for (j = 0; j < 3; j++)
            { // 给内部面的3个节点
                // 检查节点VoluFaceVerts[3*k+j]在face中的局部位置
                ind = 3 * k + j; // 体上的局部编号的位置
                for (pos = 0; pos < 3; pos++)
                { // pos：相应面上的局部位置
                    if (volu.Vert4Volu[VoluFaceVerts[ind]] == face.Vert4Face[pos])
                    {
                        mesh->Faces[num].Vert4Face[pos] = Vert4Volu[VoluChildFaceVerts03[ind]];
                    } // end if if(volu.Vert4Volu[VoluFaceVerts[ind]]==face.Vert4Face[pos])
                }     // end for pos
            }         // end for j
            mesh->Faces[num].BD_ID = 0;
            mesh->Faces[num].NumVerts = 3;
            mesh->Faces[num].NumLines = 3;
            mesh->Faces[num].Index = num;
        } // end for k
        // 接下来形成与对角线4-9相关的内部面
        for (k = 0; k < 4; k++)
        {
            num = NF * 4 + i * 8 + 4 + k;
            for (j = 0; j < 3; j++)
            {
                mesh->Faces[num].Vert4Face[j] = Vert4Volu[VoluChildFaceVerts49[3 * k + j]]; // 49边所有相关的面的主边
                // GFaceVerts[j] = mesh->Faces[num].Vert4Face[j];
            }
            // //检查面的点编号是否合理
            // maxvalue = 0.0;
            // pos = 0;
            // for (j = 0; j < 3; j++)
            // {
            //     vert0 = mesh->Verts[GFaceVerts[FaceVerts[2 * j]]];
            //     vert1 = mesh->Verts[GFaceVerts[FaceVerts[2 * j + 1]]];
            //     Lens[j] = Distance4Verts(vert0, vert1);
            //     if (Lens[j] > maxvalue)
            //     {
            //         maxvalue = Lens[j];
            //         pos = j;
            //     }
            // }
            // if (pos > 0)
            // {
            //     pos--;
            //     for (j = 0; j < 3; j++)
            //     {
            //         mesh->Faces[num].Vert4Face[j] = GFaceVerts[LocalFaceVerts[3 * pos + j]];
            //     }
            // }
            mesh->Faces[num].BD_ID = 0;
            mesh->Faces[num].NumVerts = 3;
            mesh->Faces[num].NumLines = 3;
            mesh->Faces[num].Index = num;
        } // end for k
        // 首先产生7个新的四面体
        for (k = 0; k < 7; k++)
        {
            num = NV + i * 7 + k;
            for (j = 0; j < 4; j++)
            {
                mesh->Volus[num].Vert4Volu[j] = Vert4Volu[Vert4ChildVolus[4 * (1 + k) + j]];
            } // end for j
            // //检查四面体的正确性
            // if (k >= 3)
            // {
            //     for (j = 0; j < 4; j++)
            //         GVoluVerts[j] = mesh->Volus[num].Vert4Volu[j];
            //     //这个时候需要检查该四面体是否符合主边原则
            //     maxvalue = 0.0;
            //     pos = 0;
            //     for (j = 0; j < 6; j++)
            //     {
            //         vert0 = mesh->Verts[mesh->Volus[num].Vert4Volu[LineVerts[2 * j]]];
            //         vert1 = mesh->Verts[mesh->Volus[num].Vert4Volu[LineVerts[2 * j + 1]]];
            //         Lens[j] = Distance4Verts(vert0, vert1);
            //         if (Lens[j] > maxvalue)
            //         {
            //             maxvalue = Lens[j];
            //             pos = j;
            //         }
            //     } // end for j
            //     if (pos > 0)
            //     {
            //         //需要调整节点的编号
            //         pos--;
            //         for (j = 0; j < 4; j++)
            //         {
            //             mesh->Volus[num].Vert4Volu[j] = GVoluVerts[LocalVoluVerts[4 * pos + j]];
            //         }
            //     }
            // }
            mesh->Volus[num].NumVerts = 4;
            mesh->Volus[num].NumLines = 6;
            mesh->Volus[num].NumFaces = 4;
            mesh->Volus[num].Index = num;
            mesh->Fathers[num] = i;
            mesh->Ancestors[num] = mesh->Ancestors[i];
        } // end for k
        // 继承旧的四面体的节点编号
        for (j = 0; j < 4; j++)
        {
            mesh->Volus[i].Vert4Volu[j] = Vert4Volu[Vert4ChildVolus[j]];
        }                     // end for j
        mesh->Fathers[i] = i; // Father是自身, Ancestor将不会改变;
    }                         // end for(i=0; i<NV; i++)
    // 每个旧面产生的4个面
    INT Vert4ChildFaces[12] = {5, 1, 3, 4, 3, 2, 3, 4, 5, 0, 5, 4}; // 加密之后得到的4个三角形节点的局部编号, 最后一个三角形继承原来的三角形编号
    for (i = 0; i < NF; i++)
    {
        // 目前的面
        face = mesh->Faces[i]; // 目前这个面的信息还未改变
        bd_id = face.BD_ID;
        // 获得加密之后的该面上的6个点的编号
        for (k = 0; k < 3; k++)
        {
            FaceVerts[k] = face.Vert4Face[k];          // 前3个点
            FaceVerts[k + 3] = face.Line4Face[k] + NP; // 产生新的3个边中点的编号
        }
        // 先产生3个新面
        for (k = 0; k < 3; k++)
        { // 3个新的面
            num = NF + i * 3 + k;
            for (j = 0; j < 3; j++) // 每个新的面上的3个节点
                mesh->Faces[num].Vert4Face[j] = FaceVerts[Vert4ChildFaces[k * 3 + j]];
            mesh->Faces[num].NumVerts = 3;
            mesh->Faces[num].NumLines = 3;
            mesh->Faces[num].Index = num;
            mesh->Faces[num].BD_ID = bd_id;
        } // end for k
        // 更新继承原来面编号的面的数据
        for (j = 0; j < 3; j++)
            mesh->Faces[i].Vert4Face[j] = FaceVerts[Vert4ChildFaces[9 + j]];
    } // 面上加密产生得到的新的面
    // 下面要组装面上线的信息、四面体上线和面的信息
    // 先生成每个面上的线得信息
    extern int pararefine;
    INT LocalLineVerts[6] = {1, 2, 2, 0, 0, 1};
    for (i = 0; i < tmp_nfaces; i++)
    {
        face = mesh->Faces[i];
        for (j = 0; j < 3; j++)
        { // 对3条线进行循环
            ind0 = face.Vert4Face[LocalLineVerts[2 * j]];
            ind1 = face.Vert4Face[LocalLineVerts[2 * j + 1]];
            if (ind0 > ind1)
            {
                tmp = ind1;
                ind1 = ind0;
                ind0 = tmp;
            } // 交换两个节点的编号
            start = Vert2Lines->Ptw[ind0];
            end = Vert2Lines->Ptw[ind0 + 1];
            for (k = start; k < end; k++)
            {
                if (Vert2Lines->Entries[k] == ind1)
                {
                    mesh->Faces[i].Line4Face[j] = Vert2Lines->AndEntries[k];
                } // end if
            }     // end for k
        }         // end for j
    }             // end for face
    // 下面来产生每个四面体上线的信息
    INT VoluLineVerts[12] = {0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3};
    for (i = 0; i < tmp_nvolus; i++)
    {
        volu = mesh->Volus[i];
        for (j = 0; j < 6; j++)
        {
            ind0 = volu.Vert4Volu[VoluLineVerts[2 * j]];
            ind1 = volu.Vert4Volu[VoluLineVerts[2 * j + 1]];
            if (ind0 > ind1)
            {
                tmp = ind1;
                ind1 = ind0, ind0 = tmp;
            } // 交换两个节点的编号
            start = Vert2Lines->Ptw[ind0];
            end = Vert2Lines->Ptw[ind0 + 1];
            for (k = start; k < end; k++)
            {
                if (Vert2Lines->Entries[k] == ind1)
                {
                    mesh->Volus[i].Line4Volu[j] = Vert2Lines->AndEntries[k];
                } // end if
            }     // end for k
        }         // end for j
    }             // end for i
    PTWDestroy(&Vert2Lines);
    // 下面来产生每个四面体的面的信息
    INT LocalVerts[3];
    PTW *Vert2Faces;
    PTWCreate(&Vert2Faces);
    Vert2Faces->NumRows = tmp_nverts;
    Vert2Faces->Ptw = malloc((tmp_nverts + 1) * sizeof(INT));
    memset(Vert2Faces->Ptw, 0, (tmp_nverts + 1) * sizeof(INT));
    // 产生面的信息
    for (i = 0; i < tmp_nfaces; i++)
    {
        face = mesh->Faces[i];
        for (j = 0; j < 3; j++)
        {
            LocalVerts[j] = face.Vert4Face[j];
        }
        QuickSort_Int(LocalVerts, 0, 2);
        // 对最小编号的节点进行登记
        Vert2Faces->Ptw[LocalVerts[0] + 1]++;
    } // end for i
    // 进行每个节点的统计
    Vert2Faces->Ptw[0] = 0;
    for (i = 0; i < tmp_nverts; i++)
    {
        Vert2Faces->Ptw[i + 1] += Vert2Faces->Ptw[i];
    }
    Vert2Faces->Entries = malloc(Vert2Faces->Ptw[tmp_nverts] * sizeof(INT));
    Vert2Faces->AndEntries = malloc(Vert2Faces->Ptw[tmp_nverts] * sizeof(INT));
    INT *FaceIndex = malloc(Vert2Faces->Ptw[tmp_nverts] * sizeof(INT));
    memset(VertPos, 0, tmp_nverts * sizeof(INT));
    // 下面开始生成点与面的关系
    for (i = 0; i < tmp_nfaces; i++)
    {
        face = mesh->Faces[i];
        for (j = 0; j < 3; j++)
        {
            LocalVerts[j] = face.Vert4Face[j];
        }
        QuickSort_Int(LocalVerts, 0, 2);
        // 对最小编号的节点进行登记
        pos = Vert2Faces->Ptw[LocalVerts[0]] + VertPos[LocalVerts[0]];
        Vert2Faces->Entries[pos] = LocalVerts[1];
        Vert2Faces->AndEntries[pos] = LocalVerts[2];
        FaceIndex[pos] = i; // 记录当前面的边号
        VertPos[LocalVerts[0]]++;
    }
    // 然后生成每个四面体上的面编号
    for (i = 0; i < tmp_nvolus; i++)
    {
        volu = mesh->Volus[i];
        for (j = 0; j < 4; j++)
        {                                                                 // 4个面
            for (k = 0; k < 3; k++)                                       // 面上的3个节点编号
                LocalVerts[k] = volu.Vert4Volu[VoluFaceVerts[3 * j + k]]; // 得到第j个面的节点编号
            QuickSort_Int(LocalVerts, 0, 2);
            // 然后开始寻找相应的位置
            start = Vert2Faces->Ptw[LocalVerts[0]];
            end = Vert2Faces->Ptw[LocalVerts[0] + 1];
            for (k = start; k < end; k++)
            {
                if ((Vert2Faces->Entries[k] == LocalVerts[1]) && (Vert2Faces->AndEntries[k] == LocalVerts[2]))
                {
                    mesh->Volus[i].Face4Volu[j] = FaceIndex[k];
                    k = end + 1;
                } // end if
            }     // end for k
        }         // end for j
    }             // end for i

    PTWDestroy(&Vert2Faces);
    OpenPFEM_Free(VertPos);
    OpenPFEM_Free(FaceIndex);
    endtime = GetTime();
    // OpenPFEM_Print("The time for the serial uniform refine: %2.10f!\n", endtime-starttime);
#if MPI_USE
    if (mesh->SharedInfo != NULL)
    {
        MPI_Comm comm = mesh->comm;
        MPI_Comm_rank(comm, &rank);
        MPI_Comm_size(comm, &size);
        INT *geonum = malloc(3 * sizeof(INT));
        geonum[0] = NP;
        geonum[1] = NE;
        geonum[2] = NF;
        PARADATA *paradata = NULL;
        // starttime = GetTime();
        ParaDataCreate(&paradata, mesh);
        ParaDataAdd(paradata, DOMAINDATA, 3, MPI_INT, (void *)geonum);
        RECDATA *recdata = NULL;
        ParaDataCommunicate(paradata, &recdata);

        INT *neiggeonum = (INT *)(recdata->SubDomianData[0]);
        INT neignum = mesh->SharedInfo->NumNeighborRanks;
        SHAREDGEO *sharedline, *sharedvert, *sharedface;
        INT vertnum, newvertnum, linenum, newlinenum, facenum;
        for (i = 0; i < neignum; i++) // 对每个邻居进行处理
        {
            sharedface = &(mesh->SharedInfo->SharedFaces[i]);
            sharedline = &(mesh->SharedInfo->SharedLines[i]);
            sharedvert = &(mesh->SharedInfo->SharedVerts[i]);
            // 处理点
            vertnum = sharedvert->SharedNum;
            newvertnum = sharedline->SharedNum; // 共享的新的点
            sharedvert->Index = realloc(sharedvert->Index, (vertnum + newvertnum) * sizeof(INT));
            sharedvert->Owner = realloc(sharedvert->Owner, (vertnum + newvertnum) * sizeof(INT));
            sharedvert->SharedIndex = realloc(sharedvert->SharedIndex, (vertnum + newvertnum) * sizeof(INT));
            for (j = 0; j < newvertnum; j++)
            {
                // 线上的点新编号为线编号+点的个数
                sharedvert->Index[vertnum + j] = sharedline->Index[j] + NP;
                sharedvert->Owner[vertnum + j] = sharedline->Owner[j];
                sharedvert->SharedIndex[vertnum + j] = sharedline->SharedIndex[j] + neiggeonum[3 * i]; // 在邻居进程上的编号
            }
            sharedvert->SharedNum = vertnum + newvertnum;
            // 处理线
            linenum = newvertnum; // 旧shared线个数
            facenum = sharedface->SharedNum;
            newlinenum = linenum * 2 + 3 * facenum; // shared边的总数(含原来的数量)
            sharedline->Index = realloc(sharedline->Index, newlinenum * sizeof(INT));
            sharedline->Owner = realloc(sharedline->Owner, newlinenum * sizeof(INT));
            sharedline->SharedIndex = realloc(sharedline->SharedIndex, newlinenum * sizeof(INT));
            for (j = 0; j < linenum; j++)
            {
                // 线上的新线编号为线编号+线的个数
                sharedline->Index[linenum + j] = sharedline->Index[j] + NE;
                sharedline->Owner[linenum + j] = sharedline->Owner[j];
                sharedline->SharedIndex[linenum + j] = sharedline->SharedIndex[j] + neiggeonum[3 * i + 1];
            }
            for (j = 0; j < facenum; j++)
            {
                // 面上的新线编号为 2*旧线个数+面的编号*3+ 0 or 1 or 2
                sharedline->Index[linenum * 2 + j * 3] = sharedface->Index[j] * 3 + NE * 2;
                sharedline->Owner[linenum * 2 + j * 3] = sharedface->Owner[j];
                sharedline->SharedIndex[linenum * 2 + j * 3] = sharedface->SharedIndex[j] * 3 + 2 * neiggeonum[3 * i + 1];

                sharedline->Index[linenum * 2 + j * 3 + 1] = sharedface->Index[j] * 3 + 2 * NE + 1; // 修改
                sharedline->Owner[linenum * 2 + j * 3 + 1] = sharedface->Owner[j];
                sharedline->SharedIndex[linenum * 2 + j * 3 + 1] = sharedface->SharedIndex[j] * 3 + 2 * neiggeonum[3 * i + 1] + 1;

                sharedline->Index[linenum * 2 + j * 3 + 2] = sharedface->Index[j] * 3 + 2 * NE + 2; // 修改
                sharedline->Owner[linenum * 2 + j * 3 + 2] = sharedface->Owner[j];
                sharedline->SharedIndex[linenum * 2 + j * 3 + 2] = sharedface->SharedIndex[j] * 3 + 2 * neiggeonum[3 * i + 1] + 2;
            }
            sharedline->SharedNum = newlinenum;
            // 处理面
            sharedface->Index = realloc(sharedface->Index, facenum * 4 * sizeof(INT));
            sharedface->Owner = realloc(sharedface->Owner, facenum * 4 * sizeof(INT));
            sharedface->SharedIndex = realloc(sharedface->SharedIndex, facenum * 4 * sizeof(INT));
            for (j = 0; j < facenum; j++)
            {
                sharedface->Index[facenum + j * 3] = sharedface->Index[j] * 3 + NF;
                sharedface->Owner[facenum + j * 3] = sharedface->Owner[j];
                sharedface->SharedIndex[facenum + j * 3] = sharedface->SharedIndex[j] * 3 + neiggeonum[3 * i + 2];

                sharedface->Index[facenum + j * 3 + 1] = sharedface->Index[j] * 3 + NF + 1;
                sharedface->Owner[facenum + j * 3 + 1] = sharedface->Owner[j];
                sharedface->SharedIndex[facenum + j * 3 + 1] = sharedface->SharedIndex[j] * 3 + neiggeonum[3 * i + 2] + 1;

                sharedface->Index[facenum + j * 3 + 2] = sharedface->Index[j] * 3 + NF + 2;
                sharedface->Owner[facenum + j * 3 + 2] = sharedface->Owner[j];
                sharedface->SharedIndex[facenum + j * 3 + 2] = sharedface->SharedIndex[j] * 3 + neiggeonum[3 * i + 2] + 2;
            }
            sharedface->SharedNum = facenum * 4;
        }
        RecDataDestroy(&recdata);
        ParaDataDestroy(&paradata);
    }
    // endtime = GetTime();
    // OpenPFEM_Print("The time for parallel communication: %2.10f seconds!\n",endtime - starttime);
#endif
    // starttime = GetTime();
    Mesh3D4BisectionLabel(mesh);
    // endtime=GetTime();
    //  OpenPFEM_Print("The time for the lamebl mesh 3D for bisection: %2.10f seconds!\n", endtime-starttime);
}
// 下面是自适应加密的主程序
// 参数说明: mesh: 输入的网格, ElemErrors： 后验误差估计子，即每个单元上都有一个值来表示该单元上的误差
// theta: 自适应加密系数，与自适应迭代算法速度相关的参数
void MeshAdaptiveRefine3D(MESH *mesh, DOUBLE *ElemErrors, DOUBLE theta)
{
    // LabelMesh3D4Bisection(mesh);
#if MPI_USE
    MPI_Comm_size(mesh->comm, &size);
    MPI_Comm_rank(mesh->comm, &rank);
#endif
    INT num_face = mesh->num_face, i, j, k, faceindex;
    // 建立加密网格的结构对象
    REFINEMESH *refinemesh = malloc(sizeof(REFINEMESH));
    refinemesh->Mesh = mesh; // 对网格加密结构体赋网格
    // 对加密之前的网格进行处理，产生线与面、线与体的联系
    PTWCreate(&(refinemesh->Line2Faces));
    PTWCreate(&(refinemesh->Line2Volus));
    GetGEO2Elems(mesh, 2, 1, refinemesh->Line2Faces);
    GetGEO2Elems(mesh, 3, 1, refinemesh->Line2Volus);
    // 对网格的局部编号进行重排，以保证编号的一致性
    // LabelMesh3D4Bisection(refinemesh->mesh);
    // 得到每个单元上每个面的主边局部编号: 每个面的主边是四面体边的编号
    GetFaceLine4Vlou(refinemesh);
    INT num_line = mesh->num_line;
    // NumRefines[0]: 需要加密的线的个数, NumRefines[1]: 需要加密的面的个数, NumRefines[2]: 需要加密的四面体的个数
    INT *NumRefines = malloc(3 * sizeof(INT));
    memset(NumRefines, 0, 3 * sizeof(INT));
    // 获得需要加密的线、面、体的集合, 同时也要保正并行的协调性
    //  OpenPFEM_Print("开始标记加密边!\n");
    // 由加密单元的集合得到加密的主边
    FindRefineLineByElems(refinemesh, ElemErrors, theta, NumRefines);
    // 由加密的主边的集合得到加密的单元集合(符合协调原则，没有悬点)
    FindBisectionSetByRefineLines(refinemesh, NumRefines);
#if MPI_USE
    // 创建通讯器

    PARADATA *paradata = NULL;
    if (size > 1)
        ParaDataCreate(&paradata, mesh);

    INT num_lines = mesh->num_line;
    INT num_faces = mesh->num_face;
#endif
    // NumRefines[0]: 表示需要加密边的个数
    // NumRefines[1]: 表示需要加密面的个数
    // NumRefines[2]: 表示需要加密四面体的个数
    INT endline = NumRefines[0]; // 表示需要加密线的个数
    INT endface = NumRefines[1]; // 表示需要加密面的个数
    INT endvolu = NumRefines[2]; // 表示需要加密单元的个数
    // printf("需要加密的线: %d 面: %d 体: %d 单位:个数\n", endline, endface, endvolu);
    // OpenPFEM_Print("The numbers to be refined: %d lines, %d faces, %d volus\n", endline, endface, endvolu);
    // 然后进行网格加密
    BisectionRefineMesh(refinemesh, endvolu, endface, endline);
    // 至此把需要加密的体、面、线都已经加密完成了, 接下来需要将mesh的数据完备化
    // 获得点与线的对应关系
    PTWCreate(&(refinemesh->Vert2Lines));
    GetVert2Lines(refinemesh->Mesh, refinemesh->Vert2Lines);
    CompleteAdaptiveMesh(refinemesh);
    // OpenPFEM_Print("End of adaptive refining the mesh! return to main program!\n");
#if MPI_USE
    if (size > 1)
        RenewAdaptiveShareInfo(refinemesh, paradata, num_lines, num_faces);
#endif
// 释放申请的内存空间
#if MPI_USE
    if (size > 1)
        ParaDataDestroy(&paradata);
#endif
    OpenPFEM_Free(refinemesh->RefineLineInformation);
    OpenPFEM_Free(refinemesh->FaceLineMid); // 数组的长度 3*num_face
    OpenPFEM_Free(refinemesh->FaceLineMidOld);
    OpenPFEM_Free(refinemesh->FaceChildren);
    OpenPFEM_Free(refinemesh->FaceFather);
    PTWDestroy(&(refinemesh->Vert2Lines));
    OpenPFEM_Free(NumRefines);
    mesh = refinemesh->Mesh;
    Mesh3D4BisectionLabel(mesh);
} // end 网格自适应加密完毕

// 找到四面体的4个面的主边的编号
void GetFaceLine4Vlou(REFINEMESH *refinemesh)
{
    MESH *mesh = refinemesh->Mesh;
    INT num_volu = mesh->num_volu;
    refinemesh->FaceLine4Volus = malloc(4 * num_volu * sizeof(INT));
    memset(refinemesh->FaceLine4Volus, -1, 4 * num_volu * sizeof(INT));
    INT *FaceLine4Volus = refinemesh->FaceLine4Volus;
    VOLU volu, *Volus = mesh->Volus;
    FACE face, *Faces = mesh->Faces;
    INT faceind, voluind, lineind, k, i, localstart, start, localpos, flag;
    INT NumFace4Volu = 4;
    INT LocalLine4Face[12] = {3, 5, 4, 1, 2, 5, 0, 4, 2, 0, 1, 3}; // 记录四面体每个面的局部线编号
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        start = 4 * voluind;
        // 考察局部第k个面
        for (k = 0; k < NumFace4Volu; k++)
        {
            face = Faces[volu.Face4Volu[k]]; // 取出相应的面对象
            lineind = face.Line4Face[0];     // 获得目前面上的主边,在加密之前我们需要把网格面的主边是第0条边
            localstart = 3 * k;
            flag = 0;
            for (i = 0; i < 3; i++)
            { // 确定当前主边在四面体中的局部编号
                localpos = localstart + i;
                if (volu.Line4Volu[LocalLine4Face[localpos]] == lineind)
                {
                    FaceLine4Volus[start + k] = LocalLine4Face[localpos]; // 找到了第k个面上的主边
                    flag = 1;
                }
            }
            if ((k >= 2) && (FaceLine4Volus[start + k] != 0))
            {
                // 表示该面的主边设置不合理, 对相应的面进行调整
            }
            if (flag == 0)
                printf("Can not find the base line for the %d-th volu, %d-th face\n", voluind, k);
        } // end for k-th face
    }     // end for voluind-th volu
    // 正常的话, face2和face3的主边的局部编号应该都是0
    for (k = 0; k < num_volu; k++)
    {
        start = 4 * k;
        if ((FaceLine4Volus[start + 2] != 0) || (FaceLine4Volus[start + 3] != 0) || (FaceLine4Volus[start + 1] == 4))
        {
            printf("XXXXXXXXXX There is wrong! the %d-th volu, [%d, %d, %d]!\n", k, FaceLine4Volus[start + 2], FaceLine4Volus[start + 3],
                   FaceLine4Volus[start + 1]);
        }
    } // end for(k=0;k<num_volu;k++)
} // end for GetFaceLine4Vlou(REFINEMESH *refinemesh)

// 下面的程序目的是为了得到每个四面上加密边之后得到的新的节点编号
// 如果改变没有被加密，相应边上中点的编号为-1
// 下面参数中的num_volu表示还没被加密之前的单元个数, 因为我们只需要对就网格的边进行分割就足够了
void GetVoluLineMid(REFINEMESH *refinemesh, INT Oldnum_volu)
{
    MESH *mesh = refinemesh->Mesh;
    INT *RefineLineInformation = refinemesh->RefineLineInformation;
    VOLU volu, *Volus = mesh->Volus;
    refinemesh->VoluLineMid = malloc(6 * (mesh->num_volu) * sizeof(INT));    // 表示每个单元上的6条边加密之后的中点的编号
    memset(refinemesh->VoluLineMid, -1, 6 * (mesh->num_volu) * sizeof(INT)); //-1表示该条边没有被加密
    INT *VoluLineMid = refinemesh->VoluLineMid;
    INT k, i, start;
    // 下面开始进行单元循环
    for (k = 0; k < Oldnum_volu; k++)
    {
        // 表示该单元在VoluLineMid中的起始位置
        start = 6 * k;
        volu = Volus[k];
        for (i = 0; i < 6; i++)
        {
            VoluLineMid[start + i] = RefineLineInformation[2 * volu.Line4Volu[i] + 1]; // 直接获得该边加密之后的中点编号
        }
    } // end for k (volu)
} // end GetVoluLineMid

//===============================================================================================
// 下面来对需要加密的单元进行加密
// 需要加密的单元个数为: endvolu，需要加密面的个数为endface，需要加密的边的个数为endline
void BisectionRefineMesh(REFINEMESH *refinemesh, INT endvolu, INT endface, INT endline)
{
    INT newnum_volu, newnum_face, newnum_line, newnum_vert; // 用来记录加密之后网格的点、线、面的个数
    MESH *mesh = refinemesh->Mesh;
    // 先赋给当前的点、线、面、体的个数
    newnum_volu = mesh->num_volu;
    newnum_face = mesh->num_face;
    newnum_line = mesh->num_line;
    newnum_vert = mesh->num_vert;
    INT num_face = mesh->num_face;
    // 获得加密网格对象中的相应的对象的指针
    INT *PtwLine2Faces = refinemesh->Line2Faces->Ptw;
    INT *PtwLine2Volus = refinemesh->Line2Volus->Ptw;
    // 在加密之前，我们需要计算出加密之后的点、线、面、体的个数
    INT lineind, numline2faces, numline2volus, k, elemind;
    // 先根据需要加密线得个数来得到加密之后得点、线、面、体的个数
    for (k = 0; k < endline; k++)
    {
        // 获得加密边的编号
        lineind = refinemesh->RefineLines[k];
        // 获得与该条边相关的面的个数
        numline2faces = PtwLine2Faces[lineind + 1] - PtwLine2Faces[lineind];
        // 获得与该条边相关的四面体的个数
        numline2volus = PtwLine2Volus[lineind + 1] - PtwLine2Volus[lineind];
        // 每个加密边会使得每个四面体上多出一个四面体
        newnum_volu += numline2volus;
        // 每个加密边在每个面上多出一个面，在每个四面体上多出一个面
        newnum_face += numline2faces + numline2volus;
        // 每个加密边在每个面上会多出来一条边，同时自身一分为二多出来1条边
        newnum_line += numline2faces + 1;
        // 每个加密边会多出来一个点
        newnum_vert += 1;
    } // end for for(k=0;k<endline;k++)
    // 检查一下加密单元上是否有23是加密边的时候，在每个四面体上多出1条线、3个面、2个体
    VOLU volu;
    // 下面对单元进行循环，主要是为了处理有23边加密的单元的情况，会带来额外的新的线、面和体的个数
    for (k = 0; k < endvolu; k++)
    {
        elemind = refinemesh->RefineElems[k];
        volu = mesh->Volus[elemind];
        // 检查该四面体的23是否需要加密
        if (refinemesh->LineIndicator[volu.Line4Volu[5]] == 1)
        {
            newnum_line += 1; // 会多出来1条新的边
            newnum_face += 2; // 会多出来2个面
            newnum_volu += 1; // 会多出来1个四面体, 在原有的基础上再增加一个四面体
        }                     // end if(LineIndicator[volu.Line4Volu[5]]==1)
    }                         // end for(k=0;k<endvolu;k++)
    PTWDestroy(&(refinemesh->Line2Faces));
    PTWDestroy(&(refinemesh->Line2Volus));
    free(refinemesh->LineIndicator); // 之后不再使用LineIndicator了，可以释放掉
    // 为新的网格申请内存空间
    mesh->Volus = realloc(mesh->Volus, newnum_volu * sizeof(VOLU));
    mesh->Faces = realloc(mesh->Faces, newnum_face * sizeof(FACE));
    mesh->Lines = realloc(mesh->Lines, newnum_line * sizeof(LINE));
    // mesh->Verts = realloc(mesh->Verts, newnum_vert * sizeof(VERT));
    VertsReCreate(&(mesh->Verts), mesh->num_vert, newnum_vert, mesh->worlddim);
    // 之后在加密的过程中可能会产生需要新加密的体，所以在这里扩张一下空间
    refinemesh->RefineElems = realloc(refinemesh->RefineElems, newnum_volu * sizeof(INT));
    // 之后在加密的过程中可能会产生需要新加密的面，所以在这里扩张一下空间
    refinemesh->RefineFaces = realloc(refinemesh->RefineFaces, newnum_face * sizeof(INT));
    // FaceLine4Volus: 记录一个四面体上的每个面上的主边编号
    refinemesh->FaceLine4Volus = realloc(refinemesh->FaceLine4Volus, 4 * newnum_volu * sizeof(INT));
    // FaceChildren: 记录每个面上的孩子面的编号, 一个面最多含有4个孩子单元
    refinemesh->FaceChildren = malloc(4 * newnum_face * sizeof(INT));
    memset(refinemesh->FaceChildren, -1, 4 * newnum_face * sizeof(INT)); // 初始化
    // 记录每个面的父亲单元编号
    refinemesh->FaceFather = malloc(newnum_face * sizeof(INT));
    memset(refinemesh->FaceFather, -1, newnum_face * sizeof(INT)); // 初始化
    // 定义保存父亲单元信息的对象
    INT num_volu = mesh->num_volu;
    if (mesh->Fathers == NULL)
    {
        // printf("rank: %d, This is the first case: num_volu: %d!\n",rank,num_volu);
        mesh->Fathers = malloc(newnum_volu * sizeof(INT));
    }
    else
    {
        // printf("rank: %d, This the second case: num_volu: %d!\n",rank,num_volu);
        mesh->Fathers = realloc(mesh->Fathers, newnum_volu * sizeof(INT));
    }
    memset(mesh->Fathers, -1, newnum_volu * sizeof(INT)); // 对每个单元的父亲单元编号初始化
    for (k = 0; k < num_volu; k++)
    {
        mesh->Fathers[k] = k; // 对加密之前的单元进行初始化
    }
    // 保存祖先单元的信息, 主要用于扩展子空间算法的实现
    if (mesh->Ancestors == NULL)
    {
        mesh->Ancestors = malloc(newnum_volu * sizeof(INT)); // 首次建立祖先单元的信息
        for (k = 0; k < num_volu; k++)
            mesh->Ancestors[k] = k;
    }
    else
    {
        mesh->Ancestors = realloc(mesh->Ancestors, newnum_volu * sizeof(INT));
    }
    INT faceind, base;
    // 将加密之前的所有面的父亲面设为自身
    for (faceind = 0; faceind < num_face; faceind++)
    {
        // 先对每个面的父亲面的编号, 直接设为自己的编号
        refinemesh->FaceFather[faceind] = faceind;
        // 同时将该面所对应的孩子面的第一个设为自身的编号，因为将会继承父亲面的编号
        base = 4 * faceind;
        refinemesh->FaceChildren[base] = faceind; // 第一个孩子单元设为自己的编号, 继承本单元编号的孩子单元
    }
    INT newvoluind, newfaceind, newlineind, newvertind;
    // 下面定义一些量来标记目前新的点、线、面、体的编号
    newvoluind = mesh->num_volu;
    newfaceind = mesh->num_face;
    newlineind = mesh->num_line;
    newvertind = mesh->num_vert;
    // 再更新网格的点、线、面、体的个数
    mesh->num_volu = newnum_volu;
    mesh->num_face = newnum_face;
    mesh->num_line = newnum_line;
    mesh->num_vert = newnum_vert;
    // 记录每个边的加密信息: 每个边加密得到新的边的编号, 新的点的编号
    refinemesh->RefineLineInformation = malloc(2 * newnum_line * sizeof(INT));
    memset(refinemesh->RefineLineInformation, -1, 2 * newnum_line * sizeof(INT));
    // 先处理所有需要加密的线，然后处理需要加密的体，最后处理需要加密的面
    // 进行线加密得到新的点，这个过程是确定的
    INT newindices[7];          // 用来记录加密过程中的指标
    newindices[0] = newlineind; // 记录目前最新线的编号
    newindices[1] = newvertind; // 记录目前最新点的编号
    INT refineind;
    // 先对现有网格上标记为需要加密的线进行加密，需要加密的线其实是已经都被加密了
    for (refineind = 0; refineind < endline; refineind++)
    {
        BisectionLine(refinemesh, refineind, newindices); // 加密当前边
    }                                                     // 处理完了加密边的加密
    // 需要加密的线已经全被加密了, 之后产生的新的线都是不需要再加密, 所以RefineLine可以释放掉
    free(refinemesh->RefineLines); // 记录需要加密的线的编号
    // 然后更新一下最新的线和点的编号
    newlineind = newindices[0]; // 得到最新边的编号，为未来进行加密做准备
    newvertind = newindices[1]; // 得到最新点的编号，为未来进行加密做准备
    // 为了进行面的加密，我们先建立每个面上三条边加密之后的中点编号(其实也是表示了该边的加密信息)
    // 如果面上的边的中点编号大于-1就表示该线是个加密边
    GetFaceLineMid(refinemesh, newfaceind);                             // 得到面上的三条边的中点编号
    refinemesh->FaceLineMidOld = malloc(3 * newnum_face * sizeof(INT)); // 用于后面的并行通讯
    // 记录一下目前的面的3条线的中点信息，为之后进行通讯准备信息
    memcpy(refinemesh->FaceLineMidOld, refinemesh->FaceLineMid, 3 * newnum_face * sizeof(INT));
    // 接下来开始进行单元的加密，加密之前我们先介绍每个单元的每条边的加密中点的节点编号
    GetVoluLineMid(refinemesh, newvoluind);
    newindices[0] = newvoluind; // 记录目前最新四面体的编号
    newindices[1] = newfaceind; // 记录目前最新面的编号
    newindices[2] = newlineind; // 记录目前最新线的编
    newindices[3] = endvolu;    // 记录目前需要加密的单元个数
    newindices[4] = endface;    // 记录目前需要加密的面的个数
    newindices[5] = 0;          // 记录目前在加密单元集合RefineElems中的位置，此时设为0
    newindices[6] = 0;          // 记录需要加密的face423的编号
    refineind = 0;
    // 开始进行单元的加密
    //  OpenPFEM_Print("start refine the tethedral!\n");
    // 对加密单元进行遍历
    while (refineind < endvolu)
    {
        BisectionTethedral(refinemesh, refineind, newindices); // 加密四面体
        // 加密完了之后, 需要决定下一次加密的四面体编号和总共需要加密的单元个数
        endvolu = newindices[3]; // 记录最终加密单元的个数
        refineind = newindices[5];
    }                                 // 至此对单元的加密完成
    free(refinemesh->FaceLine4Volus); // 记录每个单元中的每个面的主边编号
    free(refinemesh->RefineElems);    // 记录需要加密的单元的编号
    free(refinemesh->VoluLineMid);    // 数组的长度 6*num_volu
    newvoluind = newindices[0];       // 获得目前最新的四面体的编号
    newfaceind = newindices[1];       // 获得目前最新的面编号
    newlineind = newindices[2];       // 获得目前最新边的编号
    endface = newindices[4];          // 目前已经含有的需要加密的单元个数

    int myrank, nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 然后再进行面的加密
    newindices[0] = newfaceind; // 记录目前最新面的编号
    newindices[1] = newlineind; // 记录目前最新线的编
    newindices[2] = endface;    // 记录目前需要加密的面的个数
    newindices[3] = 0;          // 记录目前在加密单元集合RefineFaces中的位置, 此时设为0
    refineind = 0;
    while (refineind < endface)
    {
        // 加密一个面
        BisectionTriangle(refinemesh, refineind, newindices);
        endface = newindices[2]; // 记录最终加密的面的个数
        refineind = newindices[3];
    }
    newfaceind = newindices[0];    // 记录在加密面之后最新的面的编号
    newlineind = newindices[1];    // 记录在加密面之后最新的边的编号
    free(refinemesh->RefineFaces); // 记录需要加密的面的编号
} // 至此对网格的自适应加密完成

//========================================================================================================
void GetRefineElements(DOUBLE *ElemErrors, DOUBLE theta, INT num_volu, INT *NumRefines, INT *RefineElems)
{
    INT k, num_refine_elem;
    INT *Position = malloc(num_volu * sizeof(INT));
    DOUBLE TotalError = 0.0;
    for (k = 0; k < num_volu; k++)
    {
        Position[k] = k;
        TotalError += ElemErrors[k];
        ElemErrors[k] = -ElemErrors[k];
    }
    MergeSortRealRecursion(ElemErrors, Position, 0, num_volu - 1);
    DOUBLE val = 0.0, RefineError = theta * TotalError;
    num_refine_elem = 0;
    for (k = 0; k < num_volu; k++)
    {
        RefineElems[k] = k;
        num_refine_elem++;
    } // end for k
    NumRefines[0] = num_refine_elem;
} // end GetRefineElements
// 下面来定义一些相应的子函数
// 在初始网格上对加密边进行加密之后来获得每个面上的三个边的中点编号(没有被加密的边的中点设为-1)
void GetFaceLineMid(REFINEMESH *refinemesh, INT Oldnum_face)
{
    MESH *mesh = refinemesh->Mesh;
    FACE *Faces = mesh->Faces, face;
    LINE *Lines = mesh->Lines;
    INT *RefineLineInformation = refinemesh->RefineLineInformation;
    INT num_face = mesh->num_face, faceind, k, start, linemidind;
    refinemesh->FaceLineMid = malloc(3 * num_face * sizeof(INT));
    memset(refinemesh->FaceLineMid, -1, 3 * num_face * sizeof(INT));
    INT *FaceLineMid = refinemesh->FaceLineMid;
    // 根据每个面上的三条边的加密信息来确定FaceLineIndicator, 为1的时候表示该边需要加密，为0的话表示不需要加密
    for (faceind = 0; faceind < Oldnum_face; faceind++)
    {
        face = Faces[faceind];
        start = 3 * faceind;
        for (k = 0; k < 3; k++)
        {
            // 处理face的第k条边, 获得它的中点编号，如果该边没被加密，则为-1
            linemidind = RefineLineInformation[2 * face.Line4Face[k] + 1];
            if (linemidind != -1)
            {
                FaceLineMid[start + k] = linemidind;
            } // end for if
        }     // end for k
    }         // end for(faceind=0;faceind<Oldnum_face;faceind++)
} // end for GetFaceLineMid

//--------------------------------------------------------------------------------
// 加密一条边: 产生一个新的中点和一条新的边
// newindices[0]: 记录目前边的编号
// newindices[1]: 记录目前点的编号
// RefineLineInformation: 记录边的加密信息, 记录旧的边产生新的边的编号
void BisectionLine(REFINEMESH *refinemesh, INT refineind, INT *newindices)
{
    // 得到需要加密的线的编号
    MESH *mesh = refinemesh->Mesh;
    INT lineind = refinemesh->RefineLines[refineind]; // 取出需要加密的线的编号
    INT *RefineLineInformation = refinemesh->RefineLineInformation;
    LINE *Lines = mesh->Lines;
    VERT *Verts = mesh->Verts;
    // 取出线的对象
    LINE line = Lines[lineind];
    // 得到新的点
    VERT vert0 = Verts[line.Vert4Line[0]];
    VERT vert1 = Verts[line.Vert4Line[1]];
    // 得到新的点的坐标, 边的中点
    INT newlineind = newindices[0];    // 获得最新边的编号
    INT i, newvertind = newindices[1]; // 获得最新点的编号
    for (i = 0; i < mesh->worlddim; i++)
    {
        mesh->Verts[newvertind].Coord[i] = 0.5 * (vert0.Coord[i] + vert1.Coord[i]);
    }
    // 获得vert的其它信息
    mesh->Verts[newvertind].BD_ID = line.BD_ID;
    mesh->Verts[newvertind].Index = newvertind;
    // 产生新的边: 01边变成0，newlineind和newlineind,1
    mesh->Lines[newlineind].Vert4Line[0] = newvertind;
    mesh->Lines[newlineind].Vert4Line[1] = line.Vert4Line[1];
    mesh->Lines[lineind].Vert4Line[1] = newvertind;
    mesh->Lines[newlineind].BD_ID = line.BD_ID;
    mesh->Lines[newlineind].Index = newlineind;
    // 记录目前边加密之后得到新的边的编号
    RefineLineInformation[2 * lineind] = newlineind;
    RefineLineInformation[2 * lineind + 1] = newvertind;
    // 目前线的最新编号进行更新
    newindices[0]++;
    newindices[1]++;
} // 至此加密边结束
//-----------------------------------------------------------------------------------
// 将一个三角形加密成两个三角形的程序
// refineind: 需要加密在RefineFaces中的第refineind个三角形
// newindices[0]: 记录目前最新面的编号
// newindices[1]: 记录目前最新边的编号
// newindices[2]: 记录需要加密面的个数
// newindices[3]: 记录目前正在加密的面编号
void BisectionTriangle(REFINEMESH *refinemesh, INT refineind, INT *newindices)
{
    // 获得需要加密的单元编号
    MESH *mesh = refinemesh->Mesh;
    INT *RefineFaces = refinemesh->RefineFaces;
    INT *FaceLineMid = refinemesh->FaceLineMid; // 记录每个三角形上三边中点的编号
    INT faceind = RefineFaces[refineind];
    FACE face = mesh->Faces[faceind];
    // 找到这个面需要加密的边，然后再来进行加密剖分：首先会得到一个新的边
    INT newfaceind = newindices[0]; // 取出目前最新面的编号
    INT newlineind = newindices[1]; // 取出目前最新边的编号
    // 产生一条新的边[3,0]
    INT vert3ind = FaceLineMid[3 * faceind], vert0ind = face.Vert4Face[0],
        vert1ind = face.Vert4Face[1], vert2ind = face.Vert4Face[2];
    mesh->Lines[newlineind].Vert4Line[0] = vert3ind; // 在加密边的中点
    mesh->Lines[newlineind].Vert4Line[1] = vert0ind; // 终点是面的第0点
    mesh->Lines[newlineind].BD_ID = face.BD_ID;
    mesh->Lines[newlineind].Index = newlineind;
    // 先处理新的面的信息
    mesh->Faces[newfaceind].Vert4Face[0] = vert3ind;
    mesh->Faces[newfaceind].Vert4Face[1] = vert2ind;
    mesh->Faces[newfaceind].Vert4Face[2] = vert0ind;
    mesh->Faces[newfaceind].Line4Face[0] = face.Line4Face[1];
    mesh->Faces[newfaceind].Line4Face[1] = newlineind;
    mesh->Faces[newfaceind].Line4Face[2] = -1;
    // 下面两个量用来记录面的加密过程，FaceFather记录该面是从哪个面加密而来的
    INT *FaceFather = refinemesh->FaceFather, k, i;
    // FaceChildren是用来记录最开始的面加密之后的面的编号
    INT *FaceChildren = refinemesh->FaceChildren;
    mesh->Faces[newfaceind].BD_ID = face.BD_ID;
    mesh->Faces[newfaceind].Index = newfaceind;
    mesh->Faces[newfaceind].NumVerts = 3;
    mesh->Faces[newfaceind].NumLines = 3;
    INT father = FaceFather[faceind]; // 提取旧单元的father编号
    FaceFather[newfaceind] = father;  // 记录新加密的面的father编号，继承旧单元的father编号
    // 将newfaceind存入到father所在的子单元集合中
    INT start = 4 * father;
    // 将新产生的面的编号存储在father的children中
    for (k = 0; k < 4; k++)
    {
        if (FaceChildren[start + k] == -1)
        {
            FaceChildren[start + k] = newfaceind;
            k = 5;
        }
    } // end for k
    // 最后再来生成新的面的FaceLineIndicator和FaceLineMid的信息
    start = 3 * newfaceind;
    INT start0 = 3 * faceind;
    FaceLineMid[start] = FaceLineMid[start0 + 1];
    FaceLineMid[start + 1] = -1;
    FaceLineMid[start + 2] = -1;
    // 然后更新旧的面，用来存储加密之后得左边面，注意face存储得是原来的信息，这个时候还没变
    //  face：301
    mesh->Faces[faceind].Vert4Face[0] = vert3ind;
    mesh->Faces[faceind].Vert4Face[1] = vert0ind;
    mesh->Faces[faceind].Vert4Face[2] = vert1ind;
    mesh->Faces[faceind].Line4Face[0] = face.Line4Face[2];
    mesh->Faces[faceind].Line4Face[1] = -1;
    mesh->Faces[faceind].Line4Face[2] = newlineind;
    FaceLineMid[start0] = FaceLineMid[start0 + 2]; // 先赋值给第0条边的中点编号
    FaceLineMid[start0 + 1] = -1;
    FaceLineMid[start0 + 2] = -1; // 然后再更新第2条边上的中点编号
    // 检测新的面是否需要加密
    INT refinefaceind = newindices[2];
    if (FaceLineMid[start] != -1)
    {
        // 表示产生的新的面需要进行加密
        RefineFaces[refinefaceind] = newfaceind;
        newindices[2]++; // 需要加密的面的个数加1, endface将会加1
    }
    if (FaceLineMid[start0] == -1)
    {
        // 表示旧的面不需要进行下一次加密了，可以进入下一个加密单元了，否则目前这个单元需要继续加密
        newindices[3]++;
    }
    newindices[0]++; // 新的面的编号加1
    newindices[1]++; // 新的边的编号加1
} // 至此加密一个三角形完成
//--------------------------------------------------------------------------------------
// 将一个四面体加密成两个四面体的程序，加密过程中会产生新的体、面、线
// refineind: 需要加密在RefineElems中的第refineind个四面体
// newindices[0]: 记录目前最新体的编号
// newindices[1]: 记录目前最新面的编号
// newindices[2]: 记录目前最新线得编号
// newindices[3]: 记录需要加密四面体单9元的个数
// newindices[4]: 记录需要加密面的个数
// newindices[5]: 记录目前正在加密的单元编号
void BisectionTethedral(REFINEMESH *refinemesh, INT refineind, INT *newindices)
{
    // 本程序将一个单元通过01边分成两个字单元child0和child1
    // 获得当前网格及其相应的内部对象
    INT elemind = refinemesh->RefineElems[refineind]; // 获得当前处理的单元编号
    MESH *mesh = refinemesh->Mesh;
    VOLU *Volus = mesh->Volus, volu = mesh->Volus[elemind];
    FACE *Faces = mesh->Faces, face;
    INT *FaceLine4Volus = refinemesh->FaceLine4Volus; // 记录四面体上每个面的主边的编号
    INT *VoluLineMid = refinemesh->VoluLineMid;
    INT *Fathers = mesh->Fathers;     // 获得Fathers的指针
    INT *Ancestors = mesh->Ancestors; // 获得Ancestors的指针
    // 获得01条边的中点的编号(01边肯定已经被加密了)
    INT vertind4 = VoluLineMid[6 * elemind];
    // 下面处理加密面423，注意这里没有处理该面上的三个线的编号，留待进行面加密的时候补上
    INT newfaceind = newindices[1]; // 获得face423的编号
    INT newvoluind = newindices[0]; // 获得当前最新的单元编号，用来放加密得到的新的单元child1
    // 取出目前存储将要加密单元的最新位置, 用来存储可能需要加密的子单元child1的编号
    INT newrefineelemeind = newindices[3];
    mesh->Faces[newfaceind].Vert4Face[0] = vertind4;
    mesh->Faces[newfaceind].Vert4Face[1] = volu.Vert4Volu[2];
    mesh->Faces[newfaceind].Vert4Face[2] = volu.Vert4Volu[3];

    INT base0 = 6 * elemind;
    INT newvertind = VoluLineMid[base0 + 5];
    // 此处还不能确定得到的这个新面的边的信息
    mesh->Faces[newfaceind].Line4Face[0] = -1;
    mesh->Faces[newfaceind].Line4Face[1] = -1;
    mesh->Faces[newfaceind].Line4Face[2] = -1;
    mesh->Faces[newfaceind].BD_ID = 0;
    mesh->Faces[newfaceind].Index = newfaceind;
    mesh->Faces[newfaceind].NumVerts = 3;
    mesh->Faces[newfaceind].NumLines = 3;
    // 同时得到这个新的面的三个边的加密信息，即加密之后的中点编号，-1表示未加密
    INT *FaceLineMid = refinemesh->FaceLineMid;
    INT face423start = 3 * newfaceind;
    FaceLineMid[face423start] = -1;
    FaceLineMid[face423start + 1] = -1;
    FaceLineMid[face423start + 2] = -1;
    // 下面两个参数用来表示将要加密得到的两个子单元的主边类型(主边是哪一条边)
    INT tmp0, tmp1, k;
    // 根据face0和face1的主边来确定child1和child0的主边的局部编号
    INT start = 4 * elemind;
    tmp0 = FaceLine4Volus[start];     // 表示face0的主边局部编号
    tmp1 = FaceLine4Volus[start + 1]; // 表示face1的主边局部编号
    // start表示child1在FaceLine4Volus的起始位置
    start = 4 * newvoluind; // 得到child1在FaceLine4Volus中的起始位置
    // 下面两个参数表示当前的单元和child1在VoluLineMid的起始位置
    INT base = 6 * newvoluind;
    INT newrefinefaceind = newindices[4]; // 获得目前需要加密的面的个数
    INT father, faceind;
    INT *FaceChildren = refinemesh->FaceChildren;
    INT *FaceFather = refinemesh->FaceFather;
    FaceFather[newfaceind] = newfaceind;       // 给newface(face423)赋上father值
    FaceChildren[4 * newfaceind] = newfaceind; // 同时记录children的值
    newindices[1]++;                           // 将目前最新面的编号加1
    if (newvertind != -1)
    {
        // face423需要进行加密, 到以后再去加密
        refinemesh->RefineFaces[newrefinefaceind] = newfaceind;
        FaceLineMid[face423start] = newvertind; // 加密23边得到的新的节点
        newindices[4]++;                        // 这里登记Face423需要进行加密, 之后进行子四面体加密的时候再进行加密
    }
    // 下面开始形成child1和child0的数据
    INT VoluVerts[5]; // k;
    for (k = 0; k < 4; k++)
        VoluVerts[k] = volu.Vert4Volu[k]; // 先记录4个顶点的编号
    VoluVerts[4] = vertind4;              // 然后记录加密得到新的点的编号
    // 记录Child1可能的三种编号方式, 主要依据face0的主边是哪个, 然后确定Child1的编号规则, 使得第0条边是face0的主边
    INT Child1LocalVert4Volu[12] = {1, 3, 2, 4, 1, 2, 4, 3, 2, 3, 4, 1};
    // 记录Child1可能的三种面主边的编号方式, 跟Child1LocalVert4Volu的数据相对应
    INT Child1FaceLine4Volu[12] = {3, 1, 0, 0, 4, 2, 0, 0, 4, 2, 0, 0};
    // Child1需要更新边中点的边的局部编号
    INT Child1VoluLineMidTo[9] = {0, 1, 3, 0, 2, 4, 0, 2, 4};
    // Child1加密之后更新边中点的边在父亲单元中的边的局部编号, 与Child1VoluLineMidTo对应
    INT Child1VoluLineMidFrom[9] = {4, 3, 5, 3, 4, 5, 5, 3, 4};
    // 记录加密之前的面的编号和得到的一个新的面423
    INT ChildFace4Volu[5]; // 一共有5个面
    for (k = 0; k < 4; k++)
        ChildFace4Volu[k] = volu.Face4Volu[k];
    ChildFace4Volu[4] = newfaceind;
    // Child1四个面属于父亲单元上面的编号和加密得到的新的面的编号: 4表示得到的新的面423
    INT Child1LocalFace4Volu[12] = {4, 3, 2, 0, 4, 2, 0, 3, 2, 3, 0, 4};
    INT Child1Index, Child0Index;
    switch (tmp0)
    {
    case 4:
        // 13是child1的主边，节点编号为1324
        Child1Index = 0;
        break;
    case 3:
        // 12是child1的主边，节点编号为1243
        Child1Index = 1;
        break;
    case 5:
        // 23是child1的主边，节点编号为2341
        Child1Index = 2;
        break;
    } // end switch
    INT Child1LocalStart, Child0LocalStart;
    Child1LocalStart = 4 * Child1Index;
    for (k = 0; k < 4; k++)
    {
        mesh->Volus[newvoluind].Vert4Volu[k] = VoluVerts[Child1LocalVert4Volu[Child1LocalStart + k]]; // 得到Child1的节点坐标
        FaceLine4Volus[start + k] = Child1FaceLine4Volu[Child1LocalStart + k];                        // 的到Child1的每个面的主边的局部编号
        mesh->Volus[newvoluind].Face4Volu[k] =
            ChildFace4Volu[Child1LocalFace4Volu[Child1LocalStart + k]]; // 得到Child1的每个面的编号
    }
    for (k = 0; k < 6; k++)
        VoluLineMid[base + k] = -1;
    Child1LocalStart = 3 * Child1Index;
    for (k = 0; k < 3; k++)
    {
        VoluLineMid[base + Child1VoluLineMidTo[Child1LocalStart + k]] =
            VoluLineMid[base0 + Child1VoluLineMidFrom[Child1LocalStart + k]];
    }
    if (VoluLineMid[base] != -1)
    {
        // 表示child1主边需要加密，即child1需要加密
        refinemesh->RefineElems[newrefineelemeind] = newvoluind;
        // 需要加密的单元个数加1
        newindices[3]++;
    }
    newindices[0]++; // 当前的单元编号加1
    mesh->Volus[newvoluind].Index = newvoluind;
    mesh->Volus[newvoluind].NumVerts = 4;
    mesh->Volus[newvoluind].NumLines = 6;
    mesh->Volus[newvoluind].NumFaces = 4;
    if (Fathers[newvoluind] == -1)
        Fathers[newvoluind] = Fathers[elemind];
    Ancestors[newvoluind] = Ancestors[elemind];
    // 至此已经生成了child1单元的信息，同时获得目前正在处理加密的单元编号，现在来更新Child0的信息
    start = 4 * elemind; // child0继续使用当前单元的编号，则在FaceLine4Volus中的位置位start
    INT Child0LocalVert4Volu[12] = {0, 2, 3, 4, 0, 3, 4, 2, 2, 3, 0, 4};
    INT Child0FaceLine4Volu[12] = {3, 1, 0, 0, 4, 2, 0, 0, 3, 1, 0, 0};
    INT Child0VoluLineMidTo[9] = {0, 1, 3, 0, 2, 4, 0, 1, 3};
    INT Child0VoluLineMidFrom[9] = {1, 2, 5, 2, 1, 5, 5, 1, 2};
    // Child0VoluLineMid: 记录四面体上的每条边加密的中点编号
    INT Child0VoluLineMid[6] = {-1, -1, -1, -1, -1, -1};
    // 父亲单元的1,2,5在Child0中有可能被进一步加密
    Child0VoluLineMid[1] = VoluLineMid[base0 + 1];
    Child0VoluLineMid[2] = VoluLineMid[base0 + 2];
    Child0VoluLineMid[5] = VoluLineMid[base0 + 5];
    INT Child0LocalFace4Volu[12] = {4, 2, 3, 1, 4, 3, 1, 2, 2, 3, 4, 1};
    switch (tmp1)
    {
    case 1:
        // 02是child0的主边，节点编号为0234
        Child0Index = 0;
        break;
    case 2:
        // 03是face1的主边，child0的编号为0342
        Child0Index = 1;
        break;
    case 5:
        // 23是face1的主边，child0的编号为2304
        Child0Index = 2;
        break;
    } // end switch
    Child0LocalStart = 4 * Child0Index;
    for (k = 0; k < 4; k++)
    {
        mesh->Volus[elemind].Vert4Volu[k] = VoluVerts[Child0LocalVert4Volu[Child0LocalStart + k]];
        FaceLine4Volus[start + k] = Child0FaceLine4Volu[Child0LocalStart + k]; // 表示在第k个局部面上的主边在四面体上的边的局部编号
        mesh->Volus[elemind].Face4Volu[k] =
            ChildFace4Volu[Child0LocalFace4Volu[Child0LocalStart + k]];
    }
    Child0LocalStart = 3 * Child0Index;
    for (k = 0; k < 6; k++)
        VoluLineMid[base0 + k] = -1;
    for (k = 0; k < 3; k++)
    { // 更新四面体上每条线的中点编号
        VoluLineMid[base0 + Child0VoluLineMidTo[Child0LocalStart + k]] =
            Child0VoluLineMid[Child0VoluLineMidFrom[Child0LocalStart + k]];
    }
    if (Fathers[elemind] == -1)
        Fathers[elemind] = elemind;
    //===========================================================
    if (VoluLineMid[base0] == -1)
    {
        // child0主边不需要加密，即child0不需要继续加密
        newindices[5]++;
    }
} // 至此将单元的加密完成
//==============================================================
// 将网格的数据进行完备化
void CompleteAdaptiveMesh(REFINEMESH *refinemesh)
{
    MESH *mesh = refinemesh->Mesh;
    VOLU *Volus = mesh->Volus, volu;
    FACE *Faces = mesh->Faces, face;
    VERT *Verts = mesh->Verts;
    INT num_volu = mesh->num_volu, num_face = mesh->num_face;
    INT elemind, localfaceind, faceind, father, NumFaces = 4;
    INT *FaceFather = refinemesh->FaceFather;
    INT *FaceChildren = refinemesh->FaceChildren;
    INT i, j, k, vert0, vert1, start, end, flag, tmp;
    INT num_line = mesh->num_line;
    LINE line, *Lines = mesh->Lines;
    // 处理面上的线的信息
    INT LocalVert4Line[6] = {1, 2, 2, 0, 0, 1};
    PTW *Vert2Lines = refinemesh->Vert2Lines;
    INT *PtwVert2Lines = Vert2Lines->Ptw;           // 每条线的第一个点的编号
    INT *Vert12Lines = Vert2Lines->Entries;         // 每条线的第二个点的编号
    INT *Vert2LineIndices = Vert2Lines->AndEntries; // 记录线的编号
    for (faceind = 0; faceind < num_face; faceind++)
    {
        face = Faces[faceind];
        for (k = 0; k < 3; k++)
        {
            if (face.Line4Face[k] == -1) // 需要处理的判断条件
            {
                flag = 0;
                // 该面的第k条线没有被编号
                vert0 = face.Vert4Face[LocalVert4Line[2 * k]];
                vert1 = face.Vert4Face[LocalVert4Line[2 * k + 1]];
                if (vert0 > vert1)
                {
                    tmp = vert1;
                    vert1 = vert0;
                    vert0 = tmp;
                }
                start = PtwVert2Lines[vert0];
                end = PtwVert2Lines[vert0 + 1];
                for (j = start; j < end; j++)
                {
                    if (Vert12Lines[j] == vert1)
                    {
                        mesh->Faces[faceind].Line4Face[k] = Vert2LineIndices[j];
                        flag = 1;
                    } // if if(Vert2Lines[j]==vert1)
                }     // end for j
                if (flag == 0)
                {
                    printf("can not find the line for the face: %d, line: %d !\n", faceind, k);
                }
            } // end for if
        }     // end for k
    }         // end for faceind
    // 接下来处理四面体线的信息
    INT LineVert0[6] = {0, 0, 0, 1, 1, 2}; // 每个四面体上6条线的第一个点的局部编号
    INT LineVert1[6] = {1, 2, 3, 2, 3, 3}; // 每个四面体上6条线的第二个点的局部编号
    // 对所有的四面进行遍历
    for (elemind = 0; elemind < num_volu; elemind++)
    {
        volu = Volus[elemind];
        // 处理该四面体上的6条线
        for (k = 0; k < 6; k++)
        {
            vert0 = volu.Vert4Volu[LineVert0[k]];
            vert1 = volu.Vert4Volu[LineVert1[k]];
            if (vert0 > vert1)
            {
                tmp = vert1;
                vert1 = vert0;
                vert0 = tmp;
            }
            start = PtwVert2Lines[vert0];
            end = PtwVert2Lines[vert0 + 1];
            flag = 0;
            for (j = start; j < end; j++)
            {
                if (Vert12Lines[j] == vert1)
                {
                    Volus[elemind].Line4Volu[k] = Vert2LineIndices[j];
                    flag = 1;
                }
            } // end for j
            if (flag == 0)
            {
                printf("Can not find the %d-th line for the %d-th volu: [%d, %d] \n!", k, elemind, vert0, vert1);
            }
        } // end for k
    }     // end for elemind
    // 处理四面体的面信息
    INT FaceVerts[3], Positions[3];
    // 每个四面体上4个面上节点的局部编号，同时他们也保持方向向外
    INT LocalFaceIndices[12] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1};
    for (elemind = 0; elemind < num_volu; elemind++)
    {
        volu = Volus[elemind]; // 获得目前的四面体
        // 对该四面体的四个面进行循环
        for (localfaceind = 0; localfaceind < 4; localfaceind++)
        {
            faceind = volu.Face4Volu[localfaceind];
            father = FaceFather[faceind];
            // 取出第localfaceind面的三个节点的编号
            for (k = 0; k < 3; k++)
            {
                FaceVerts[k] = volu.Vert4Volu[LocalFaceIndices[3 * localfaceind + k]];
            }
            faceind = GetLocalPosition(Faces, FaceChildren, father, FaceVerts, Positions);
            Volus[elemind].Face4Volu[localfaceind] = faceind;
        } // end for localfaceind
    }     // end for elemind
} // end void CompleteAdaptiveMesh(REFINEMESH *refinemesh)
//==================================================================
INT FindFacePosition(FACE face, INT Verts[], INT Positions[], INT num)
{
    // 验证face中的节点编号是否与Verts中的一样，并且同时给出Verts编号在face中的位置
    INT i, k, flag = 0;
    for (k = 0; k < num; k++)
    {
        for (i = 0; i < num; i++)
        {
            if (face.Vert4Face[i] == Verts[k])
            {
                Positions[k] = i;
                flag++;
                i = num + 1;
            } // end if
        }     // end for i
        if (flag < k + 1)
            k = num + 1;
    } // end for k
    if (flag == num)
    {
        return 1;
    }
    else
    {
        return 0;
    }
} // end for FindPosition
// 获得一个面在FaceChildren中的位置
INT GetLocalPosition(FACE *Faces, INT *FaceChildren, INT father, INT Verts[], INT Positions[])
{
    INT k, start, faceind = -1, flag;
    start = 4 * father;
    FACE face;
    for (k = 0; k < 4; k++)
    {
        faceind = FaceChildren[start + k];
        if (faceind >= 0)
        {
            face = Faces[faceind];
        }
        else
        {
            k = 6;
        } // end if(faceind>=0)
        flag = FindFacePosition(face, Verts, Positions, 3);
        if (flag == 1)
        {
            // 找到了相应的面的编号和相应的三个节点在面上的位置
            k = 6;
        } // end for if flag==1
    }     // end for k
    return faceind;
}
void RefineMeshDestroy(REFINEMESH **refinemesh)
{
    OpenPFEM_Free((*refinemesh)->FaceChildren);     // 记录每个面加密之后得到的子面集合（4*num_face）
    OpenPFEM_Free((*refinemesh)->FaceFather);       // 记录每个面的父亲面编号
    PTWDestroy(&((*refinemesh)->Vert2Lines)); // 释放Vert2Lines的空间
    OpenPFEM_Free((*refinemesh));
}

// 由加密单元得到相应的加密主边的集合
void FindRefineLineByElems(REFINEMESH *refinemesh, DOUBLE *ElemErrors, DOUBLE theta, INT *NumRefines)
{
    MESH *mesh = refinemesh->Mesh;
    INT num_volu = mesh->num_volu, num_line = mesh->num_line;
    // ElemIndicator 表示每个单元是否需要加密: 0表示不需要加密, 1表示需要加密
    refinemesh->ElemIndicator = malloc(num_volu * sizeof(INT)); // 记录每个面是否被处理
    memset(refinemesh->ElemIndicator, 0, num_volu * sizeof(INT));
    INT *ElemIndicator = refinemesh->ElemIndicator;
    // RefineElems 存储需要加密的单元编号
    refinemesh->RefineElems = malloc(num_volu * sizeof(INT));
    //-1表示没有单元编号, 大于等于零表示需要加密的单元编号
    memset(refinemesh->RefineElems, -1, num_volu * sizeof(INT));
    INT *RefineElems = refinemesh->RefineElems;
    // LineIndicator:表示每条边是否需要加密
    refinemesh->LineIndicator = malloc(num_line * sizeof(INT));
    // 0表示该线不需要加密，1表示需要加密
    memset(refinemesh->LineIndicator, 0, num_line * sizeof(INT));
    INT *LineIndicator = refinemesh->LineIndicator;
    // RefineLines表示需要加密的边的编号
    refinemesh->RefineLines = malloc(num_line * sizeof(INT));
    // 大于等于0表示是加密边的编号
    memset(refinemesh->RefineLines, -1, num_line * sizeof(INT));
    INT *RefineLines = refinemesh->RefineLines;
    // GetRefineElements(ElemErrors, theta, num_volu, NumRefines + 2, RefineElems);
    // 下面调用刘昊宸写的确定加密单元的程序来确定RefineElems集合
    FindRefineElem(mesh, ElemErrors, theta, NumRefines + 2, RefineElems);
    // NumRefines[2]记录了需要加密单元的个数, RefineElems: 记录了需要加密的单元编号
    VOLU *Volus = mesh->Volus, volu;
    INT lineind, elemind, k;
    INT endvolu = NumRefines[2];
    // OpenPFEM_Print("The refine elements: %d\n", endvolu);
    INT endline = 0;
    // 预处理, 得到需要加密的边的集合
    for (k = 0; k < endvolu; k++)
    {
        // 获得当前需要加密的单元编号
        elemind = RefineElems[k];
        // 首先将所在的单元标记为1，即为要加密的单元
        ElemIndicator[elemind] = 1;
        // 得到目前四面体的对象
        volu = Volus[elemind];
        // 将本单元的01条边标记为加密
        lineind = volu.Line4Volu[0]; // 得到第0条边的编号
        // 如果本条边没有被标记为加密的话，就将本条边标记为需要加密
        if (LineIndicator[lineind] == 0)
        {
            LineIndicator[lineind] = 1;
            // 加需要加密的边的编号存储在RefineLines
            RefineLines[endline] = lineind;
            endline++; // 需要加密的边的个数加1
        }              // end if(LineIndicator[lineind]==0)
    }                  // end for(k = startvolu; k<endvolu; k++)得到需要加密边的集合
    // 如此得到了需要新加密边的起始位置startline和终点位置endline
    NumRefines[0] = endline;
} //

// 获得需要加密的线、面、体的集合，这里采用的是在每个面上、每个体上都满足Bisection原则
// 该原则就是: 由加密的线出发得到需要加密的面和体的集合
// 这样最后加密得到的网格就能保持正则性
//  NumRefines[0]: 表示需要加密边的个数
//  NumRefines[1]: 表示需要加密面的个数
//  NumRefines[2]: 表示需要加密四面体的个数
void FindBisectionSetByRefineLines(REFINEMESH *refinemesh, INT *NumRefines)
{
    MESH *mesh = refinemesh->Mesh;
    MPI_Comm_size(mesh->comm, &size);
    MPI_Comm_rank(mesh->comm, &rank);
    INT num_volu = mesh->num_volu;
    INT num_face = mesh->num_face, num_line = mesh->num_line;
    refinemesh->FaceIndicator = malloc(num_face * sizeof(INT)); // 记录每个单元是否已经被处理
    memset(refinemesh->FaceIndicator, 0, num_face * sizeof(INT));
    refinemesh->RefineFaces = malloc(num_face * sizeof(INT));
    memset(refinemesh->RefineFaces, -1, num_face * sizeof(INT));
    // 获得本进程上需要加密的单元个数和相应的加密单元编号
    INT j, k, ind, endvolu, startline, endline, lineind, elemlineind, facelineind,
        elemind, endpos, endface, faceind, start, end;
    INT *ElemIndicator = refinemesh->ElemIndicator;
    INT *RefineElems = refinemesh->RefineElems;
    INT *FaceIndicator = refinemesh->FaceIndicator;
    INT *LineIndicator = refinemesh->LineIndicator;
    INT *RefineFaces = refinemesh->RefineFaces;
    INT *RefineLines = refinemesh->RefineLines;
    INT *PtwLine2Faces = refinemesh->Line2Faces->Ptw;
    INT *Line2Faces = refinemesh->Line2Faces->Entries;
    INT *PtwLine2Volus = refinemesh->Line2Volus->Ptw;
    INT *Line2Volus = refinemesh->Line2Volus->Entries;
    // 获得网格的集合对象
    VOLU *Volus = mesh->Volus, volu;
    FACE *Faces = mesh->Faces, face;
    LINE *Lines = mesh->Lines;
    // 下面来记录需要加密的线和面的起点和终点
    startline = 0;
    endline = 0;
    endface = 0;
    endline = NumRefines[0]; // 目前得到的需要加密边的条数
    // 表示需要处理的新的加密单元编号在RefineElems中的终点位置(后一个位置)
    endvolu = NumRefines[2];
    INT NumNewRefineLines = endline - startline; // 得到本次搜索需要新加密的边的条数
    INT tmp, tmpp, NumRefineInterBoundaryLines;

#if MPI_USE
    PARADATA *paradata = NULL;
    if (size > 1)
        ParaDataCreate(&paradata, mesh);
#endif
    // 下面进行循环来确定最终需要加密的单元、加密的面和线
    do
    {
        tmp = startline;
        // 先处理好子区域内部需要加密线的协调问题
        while (NumNewRefineLines > 0)
        {
            // 对当前新的加密边进行处理
            endpos = endline; // 记录目前处理的新的加密边的终点位置
            // startline：表示目前处理的加密边在RefineLines中的位置
            for (k = startline; k < endpos; k++)
            {
                // 获得当前需要加密边的编号
                lineind = RefineLines[k];
                // 获得本条边所对应的单元
                start = PtwLine2Volus[lineind];
                end = PtwLine2Volus[lineind + 1];
                // 对当前边所对应的单元进行循环，所有的这些单元以及他们的主边都需要标记为加密
                for (j = start; j < end; j++)
                {
                    // 获得单元的编号和对象
                    elemind = Line2Volus[j];
                    // 处理当前的单元，如果没有被标记为加密，则标记为加密
                    if (ElemIndicator[elemind] == 0)
                    {
                        // 将目前的单元标记为加密
                        ElemIndicator[elemind] = 1;
                        // 然后将目前的单元放到需要加密的单元集合中
                        RefineElems[endvolu] = elemind;
                        // endvolu表示保存目前需要加密单元编号的位置
                        endvolu++;
                        // 同时这个单元的主边也需要标记为加密
                        volu = Volus[elemind];
                        elemlineind = volu.Line4Volu[0];
                        // 如果该条边没有被标记为加密，那么就标记为加密
                        if (LineIndicator[elemlineind] == 0)
                        {
                            // 将该条边标记为加密
                            LineIndicator[elemlineind] = 1;
                            // 将目前加密边存储到加密边的集合中
                            RefineLines[endline] = elemlineind;
                            // 将需要加密的边的个数加1
                            endline++;
                        } // end if(LineIndicator[lineind]==0)
                    }     // end if(ElemIndicator[elemind]==0)
                }         // 如此处理完了目前边对应的所有的单元 for(j=start;j<end;j++)
                // 下面来处理需要加密的面, 找到目前边所对应的面的集合
                start = PtwLine2Faces[lineind];
                end = PtwLine2Faces[lineind + 1];
                for (j = start; j < end; j++)
                {
                    // 获得面的编号
                    faceind = Line2Faces[j];
                    // 如果当前面没有被标记为加密，就标记为加密，同时将该面的主边标记为加密
                    if (FaceIndicator[faceind] == 0)
                    {
                        FaceIndicator[faceind] = 1;
                        RefineFaces[endface] = faceind;
                        endface++;
                        face = Faces[faceind];
                        facelineind = face.Line4Face[0];
                        // 如果该条边没有被标记为加密，那么就标记为加密
                        if (LineIndicator[facelineind] == 0)
                        {
                            // 将该条边标记为加密
                            LineIndicator[facelineind] = 1;
                            RefineLines[endline] = facelineind;
                            // 将需要加密的边的个数加1
                            endline++;
                        } // end if(LineIndicator[lineind]==0)
                    }     // end if(FaceIndicator[faceind]==0)
                }         // end for(j=start;j<end;j++)
            }             // 如此处理完了目前需要重新加密的边
            // 下面来获得下一次需要继续处理加密边的情况
            NumNewRefineLines = endline - endpos;
            // 下一次加密边的迭代的起点
            startline = endpos; // 下一次处理的起始线的位置
        }                       // end while(NumNewRefineLines>0)表示已经处理完备了所有需要加密的单元和边
// 接下来需要处理进程之间的边界边和边界面的加密协调性，使得全局网格也是协调的
// 统计本进程中需要新加密的内部边界线和面, 全局同步一下，然后进行通讯
#if MPI_USE
        RECDATA *recdata = NULL;
        if (size > 1)
        {
            ParaDataClean(paradata);
            ParaDataAdd(paradata, LINEDATA, 1, MPI_INT, (void *)LineIndicator);

            ParaDataCommunicate(paradata, &recdata);

            INT recvnum = recdata->LineNum;
            INT *recvindex = recdata->LineIndex;
            INT *recvIndicator = (INT *)(recdata->LineDatas[0]);
            for (j = 0; j < recvnum; j++)
            {
                if (recvIndicator[j])
                {                     // 如果这条边在邻居进程需要加密的时候
                    k = recvindex[j]; // 获得的共享线的编号
                    if (LineIndicator[k] == 0)
                    { // 有新加密边时
                        LineIndicator[k] = 1;
                        RefineLines[endline] = k;
                        endline++;
                    }
                }
            }
            NumNewRefineLines = endline - startline;
            RecDataDestroy(&recdata);
        }
        MPI_Allreduce(&NumNewRefineLines, &NumRefineInterBoundaryLines, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#elif
        NumRefineInterBoundaryLines = 0;
#endif
    } while (NumRefineInterBoundaryLines);
#if MPI_USE
    if (size > 1)
        ParaDataDestroy(&paradata);
#endif
    NumRefines[0] = endline;
    NumRefines[1] = endface;
    NumRefines[2] = endvolu;
    // 释放掉FaceIndicator和ElemIndicator的空间，LineIndicator之后还有用处
    free(refinemesh->FaceIndicator);
    free(refinemesh->ElemIndicator);
} // end 至此就把本进程中所有需要加密的边和单元都标记为加密了

void RenewAdaptiveShareInfo(REFINEMESH *refinemesh, PARADATA *paradata, INT num_lines, INT num_faces)
{
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // 用于旧线生成新点和新线(非继承)
    MESH *mesh = refinemesh->Mesh;
    INT i, j, k, faceindex;
    refinemesh->Edge_Child_Line = (INT *)malloc(num_lines * sizeof(INT));
    memset((refinemesh->Edge_Child_Line), -1, num_lines * sizeof(INT));
    INT *Edge_Child_Line = refinemesh->Edge_Child_Line;

    refinemesh->Edge_Child_Vert = (INT *)malloc(num_lines * sizeof(INT));
    memset((refinemesh->Edge_Child_Vert), -1, num_lines * sizeof(INT));
    INT *Edge_Child_Vert = refinemesh->Edge_Child_Vert;

    for (i = 0; i < num_lines; i++)
    {
        refinemesh->Edge_Child_Line[i] = refinemesh->RefineLineInformation[2 * i];     // 得到边加密得到的新的线编号
        refinemesh->Edge_Child_Vert[i] = refinemesh->RefineLineInformation[2 * i + 1]; // 得到边加密之后得到新的点编号
    }
    // 更新sharedinfo(新点和线上的新线)
    ParaDataClean(paradata);
    ParaDataAdd(paradata, LINEDATA, 1, MPI_INT, (void *)(refinemesh->Edge_Child_Vert));
    ParaDataAdd(paradata, LINEDATA, 1, MPI_INT, (void *)(refinemesh->Edge_Child_Line));
    RECDATA *recdata = NULL;
    // 然后进行信息传输
    check_comm = 1;
    ParaDataCommunicate(paradata, &recdata);
    // 更新线上的新点和非继承的新线
    SHAREDGEO *sharedline, *sharedvert;
    INT neignum = mesh->SharedInfo->NumNeighborRanks;

    INT position = 0;
    INT *neig_child_vert = (INT *)(recdata->LineDatas[0]);
    INT *neig_child_line = (INT *)(recdata->LineDatas[1]);
    INT sharedlinenum, sharedvertnum, lineindex;
    // 对于RECDATA的理解：第0个邻居进程中的数据[], 第1个邻居进程中的数据, neigum: 邻居进程个数
    for (i = 0; i < neignum; i++) // 对每个邻居
    {
        sharedvert = &(mesh->SharedInfo->SharedVerts[i]);
        sharedline = &(mesh->SharedInfo->SharedLines[i]);
        sharedvertnum = sharedvert->SharedNum;
        sharedlinenum = sharedline->SharedNum;
        // 计数, 便于后面为SharedInfo开辟内存
        for (j = position; j < position + sharedline->SharedNum; j++)
        {
            if (neig_child_vert[j] != -1) // 如果有加密点
            {
                sharedvertnum++;
            }
            if (neig_child_line[j] != -1) // 如果有加密线
            {
                sharedlinenum++;
            }
        } // end for j
        // 下面这个过程应该可以写一个统一的子函数
        sharedvert->Index = realloc(sharedvert->Index, sharedvertnum * sizeof(INT));
        sharedvert->SharedIndex = realloc(sharedvert->SharedIndex, sharedvertnum * sizeof(INT));
        sharedvert->Owner = realloc(sharedvert->Owner, sharedvertnum * sizeof(INT));
        sharedline->Index = realloc(sharedline->Index, sharedlinenum * sizeof(INT));
        sharedline->SharedIndex = realloc(sharedline->SharedIndex, sharedlinenum * sizeof(INT));
        sharedline->Owner = realloc(sharedline->Owner, sharedlinenum * sizeof(INT));
        sharedvertnum = sharedvert->SharedNum; // 得到旧的共享点的个数
        sharedlinenum = sharedline->SharedNum;
        for (j = position; j < position + sharedline->SharedNum; j++) // sharedline->SharedNum可以用sharedlinenum代替？
        {
            if (neig_child_vert[j] != -1) // 表示从目前邻居进程中接收到的第j条线上有加密得到的节点
            {
                lineindex = recdata->LineIndex[j];                                  // 该边在本进程中的编号
                sharedvert->Index[sharedvertnum] = Edge_Child_Vert[lineindex];      // 得到新的共享点在本进程中的编号
                sharedvert->Owner[sharedvertnum] = sharedline->Owner[j - position]; // 所在线的宿主进程号
                sharedvert->SharedIndex[sharedvertnum] = neig_child_vert[j];        // 在邻居进程中的编号
                sharedvertnum++;
            }
            if (neig_child_line[j] != -1) // 表示从目前邻居进程中接收到的第j条线上有加密得到新的边编号
            {
                lineindex = recdata->LineIndex[j];
                sharedline->Index[sharedlinenum] = Edge_Child_Line[lineindex];
                sharedline->Owner[sharedlinenum] = sharedline->Owner[j - position];
                sharedline->SharedIndex[sharedlinenum] = neig_child_line[j];
                sharedlinenum++;
            }
        }
        position += sharedline->SharedNum;
        sharedvert->SharedNum = sharedvertnum;
        sharedline->SharedNum = sharedlinenum;
    } // end for i: rank
    OpenPFEM_Free(refinemesh->Edge_Child_Line);
    OpenPFEM_Free(refinemesh->Edge_Child_Vert);

    INT tmp, *face_type, pos;
    face_type = (INT *)malloc(num_faces * sizeof(INT));
    memset(face_type, -1, num_faces * sizeof(INT));
    // 用于旧面生成新线和新面(非继承)
    refinemesh->Face_Child_Line = (INT *)malloc(3 * num_faces * sizeof(INT));
    memset((refinemesh->Face_Child_Line), -1, 3 * num_faces * sizeof(INT));
    INT *Face_Child_Line = refinemesh->Face_Child_Line;
    refinemesh->Face_Child_Face = (INT *)malloc(4 * num_faces * sizeof(INT));
    memset((refinemesh->Face_Child_Face), -1, 4 * num_faces * sizeof(INT));
    INT *Face_Child_Face = refinemesh->Face_Child_Face; // refinemesh->FaceChildren;
    // Face_Child_Face:存储每个面加密得到的子面的编号
    for (i = 0; i < num_faces; i++)
    {
        for (j = 0; j < 4; j++)
        {
            Face_Child_Face[i * 4 + j] = refinemesh->FaceChildren[i * 4 + j];
        }
    }
    // 根据FaceLineMid（旧面上的加密中点信息）确定好，面加密的类型
    //  type0: 只有第0个位置大于-1，另外两个位置为-1，一分为二
    //  type1：一分为3，第0个位置和第2个位置大于-1，第1个位置为-1，
    //  type2：第0个位置和第1个位置大于-1，第2个位置为-1，一分为三
    //  type3：一分为4 ，三个位置都大于-1
    INT *FaceLineMid = refinemesh->FaceLineMidOld;
    for (i = 0; i < num_faces; i++)
    {
        pos = i;
        if (FaceLineMid[i * 3] > -1)
        {
            // 如果第一个位置为-1则表示该面不需要加密，则face_type取默认值-1，否则有如下四种类型
            if (FaceLineMid[i * 3 + 1] == -1)
            {
                if (FaceLineMid[i * 3 + 2] == -1)
                {
                    face_type[pos] = 0; // type0: 只有第0个位置大于-1，另外两个位置为-1，一分为二 （两个-1）
                }
                else
                {
                    face_type[pos] = 1; // type1: 只有第1个位置为-1，第0个位置和第2个位置不为-1，（一个-1）
                }
            }
            else
            {
                if (FaceLineMid[i * 3 + 2] == -1)
                {
                    face_type[pos] = 2; // type2：只有第2个位置为-1，第0个位置和第1个位置不为-1，（一个-1）
                }
                else
                {
                    face_type[pos] = 3; // type3：三个位置都不为-1 （0个-1）
                }
            }
        } // end if(FaceLineMid[i*3] > -1)
    }     // end for(i=0; i<num_faces; i++)

    FACE face;
    // 更新Face_Child_Line(由旧面产生的旧面内的线，最多三条)的信息
    // 注意现在mesh已经改变了
    //  mesh = refinemesh->Mesh; //可以测试一下去掉该行是否有问题！！！！
    for (i = 0; i < num_faces; i++)
    {
        face = mesh->Faces[i]; // 出第0个继承面
        switch (face_type[i])
        {
        case 0:
            // 只有一条加密边（编号应放在第0个位置），该边的编号为，继承面的第2条边的编号
            Face_Child_Line[i * 3] = face.Line4Face[2];
            break;
        case 1:
            // type1，有2条加密边（编号应放在第0和1位置），这2条边的编号为，继承面的第0条边和第2条边的编号
            Face_Child_Line[i * 3] = face.Line4Face[0];
            Face_Child_Line[i * 3 + 1] = face.Line4Face[2];
            break;
        case 2:
            // type2，有2条加密边（编号应放在第0和2位置），这2条边的编号为，继承面的第2条边和1号新面的第2条边的编号
            Face_Child_Line[i * 3] = face.Line4Face[2];
            pos = Face_Child_Face[i * 4 + 1];
            face = mesh->Faces[pos];
            Face_Child_Line[i * 3 + 2] = face.Line4Face[2]; // 为什么放在第三个位置？
            break;
        case 3:
            // type3，有3条加密边（编号应放在第0和1和2位置），这3条边的编号为，继承面的第0和2条边和1号新面的第2条边的编号
            Face_Child_Line[i * 3] = face.Line4Face[0];
            Face_Child_Line[i * 3 + 1] = face.Line4Face[2];
            pos = Face_Child_Face[i * 4 + 1];
            face = mesh->Faces[pos];
            Face_Child_Line[i * 3 + 2] = face.Line4Face[2];
            break;
        }
    } // end for(i=0; i<num_faces; i++)
    // 更新sharedinfo(面上新线和面上的新面(非继承))
    ParaDataClean(paradata);
    ParaDataAdd(paradata, 2, 3, MPI_INT, (void *)Face_Child_Line);
    ParaDataAdd(paradata, 2, 4, MPI_INT, (void *)Face_Child_Face);
    RECDATA *recdata2 = NULL;
    ParaDataCommunicate(paradata, &recdata2);
    // 更新线上的新线和非继承的新面
    SHAREDGEO *sharedface;
    // INT neignum = mesh->SharedInfo->NumNeighborRanks;
    position = 0;
    neig_child_line = (INT *)(recdata2->FaceDatas[0]);      // 存储加密的三条线
    INT *neig_child_face = (INT *)(recdata2->FaceDatas[1]); // 存储的加密得到的三个新面
    INT sharedfacenum;
    sharedlinenum = 0;

    INT po = 0;
    for (i = 0; i < neignum; i++) // 对每个邻居
    {
        sharedface = &(mesh->SharedInfo->SharedFaces[i]);
        sharedfacenum = sharedface->SharedNum;
        for (j = po; j < po + sharedface->SharedNum; j++)
        {
            faceindex = recdata2->FaceIndex[j]; // 本地进程中的编号
        }                                       // end for j
        po = po + sharedfacenum;
    } // end for i rank

    for (i = 0; i < neignum; i++) // 对每个邻居
    {
        sharedface = &(mesh->SharedInfo->SharedFaces[i]);
        sharedline = &(mesh->SharedInfo->SharedLines[i]);
        sharedfacenum = sharedface->SharedNum;
        sharedlinenum = sharedline->SharedNum;
        // 计数, 便于后面为SharedInfo开辟内存
        //  OpenPFEM_RankPrint(DEFAULT_COMM,"sharedface->SharedNum=%d\n",sharedface->SharedNum);
        for (j = position; j < position + sharedface->SharedNum; j++)
        {
            for (k = 0; k < 3; k++)
            {
                if (neig_child_line[j * 3 + k] != -1) // 如果有加密线
                {
                    sharedlinenum++;
                }
                if (neig_child_face[j * 4 + 1 + k] != -1) // 如果有加密面
                {
                    sharedfacenum++;
                }
            }
        } // 完成计数 //end for j
        sharedline->Index = realloc(sharedline->Index, sharedlinenum * sizeof(INT));
        sharedline->SharedIndex = realloc(sharedline->SharedIndex, sharedlinenum * sizeof(INT));
        sharedline->Owner = realloc(sharedline->Owner, sharedlinenum * sizeof(INT));
        mesh->SharedInfo->SharedFaces[i].Index = realloc(mesh->SharedInfo->SharedFaces[i].Index, sharedfacenum * sizeof(INT));
        mesh->SharedInfo->SharedFaces[i].SharedIndex = realloc(mesh->SharedInfo->SharedFaces[i].SharedIndex, sharedfacenum * sizeof(INT));
        mesh->SharedInfo->SharedFaces[i].Owner = realloc(mesh->SharedInfo->SharedFaces[i].Owner, sharedfacenum * sizeof(INT));
        sharedface = &(mesh->SharedInfo->SharedFaces[i]);
        sharedlinenum = sharedline->SharedNum;
        sharedfacenum = sharedface->SharedNum;
        for (j = position; j < position + sharedface->SharedNum; j++)
        {
            faceindex = recdata2->FaceIndex[j]; // 本地进程中的编号
            // 对面遍历
            for (k = 0; k < 3; k++)
            {
                if (neig_child_line[j * 3 + k] != -1) // 如果有加密线
                {
                    sharedline->Index[sharedlinenum] = Face_Child_Line[faceindex * 3 + k];
                    sharedline->Owner[sharedlinenum] = sharedface->Owner[j - position];
                    sharedline->SharedIndex[sharedlinenum] = neig_child_line[j * 3 + k];
                    sharedlinenum++;
                }
                if (neig_child_face[j * 4 + 1 + k] != -1) // 如果有加密面
                {

                    faceindex = recdata2->FaceIndex[j];
                    sharedface->Index[sharedfacenum] = Face_Child_Face[faceindex * 4 + 1 + k];
                    sharedface->Owner[sharedfacenum] = sharedface->Owner[j - position];
                    sharedface->SharedIndex[sharedfacenum] = neig_child_face[j * 4 + 1 + k];
                    sharedfacenum++;
                }
            }
        } // end for j
        position += sharedface->SharedNum;
        sharedline->SharedNum = sharedlinenum;
        sharedface->SharedNum = sharedfacenum;
    } // end for i rank
    RecDataDestroy(&recdata);
    OpenPFEM_Free(face_type);
    OpenPFEM_Free(refinemesh->Face_Child_Line);
    OpenPFEM_Free(refinemesh->Face_Child_Face);
}

// 寻找两个面的交线编号
void GetInterLine4Faces(FACE face0, FACE face1, INT *LineVerts)
{
    INT i, j, pos = 0, NumVerts0 = face0.NumVerts, NumVerts1 = face1.NumVerts;
    for (i = 0; i < NumVerts0; i++)
    {
        for (j = 0; j < NumVerts1; j++)
        {
            if (face0.Vert4Face[i] == face1.Vert4Face[j])
            {
                // 找到了一个公共的点
                LineVerts[pos] = face0.Vert4Face[i];
                pos++;
                j = NumVerts1 + 1; // 直接跳出j的循环
            }
        }
    } // 至此得到line01的节点编号
    if (pos < 2)
        printf("XXXXXXXXXX: can not find the inter lines!\n");
}

// 寻找两个四面体的公共面的编号
INT GetInterFace4Volus(VOLU volu0, VOLU volu1)
{
    INT i, j, numface0 = volu0.NumFaces, numface1 = volu1.NumFaces, faceind = -1;
    for (i = 0; i < numface0; i++)
    {
        for (j = 0; j < numface1; j++)
        {
            if (volu0.Face4Volu[i] == volu1.Face4Volu[j])
            {
                faceind = volu0.Face4Volu[i];
                i = numface0 + 1;
                j = numface1 + 1;
            }
        }
    }
    if (faceind == -1)
        printf("ZZZZZZZZZZ: can not find the inter face!\n");
    return faceind;
}

// 寻找两个面的交点编号
INT GetInterVert4Lines(INT *Line0, INT *Line1)
{
    INT i, j, vertind = -1;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            if (Line0[i] == Line1[j])
            {
                vertind = Line0[i]; // 至此找到了交点的编号
            }
        }
    }
    if (vertind == -1)
        printf("YYYYYYYY: can not find the inter vert!\n");
    return vertind;
}