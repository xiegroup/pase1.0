#include "mesh.h"
#include "meshcomm.h"
#include "constants.h"

#define PRINT_INFO 0

INT rank;
INT size;

// 0进程读入网格文件，并且建立并行有限元网格
void MeshBuild(MESH *mesh, const char *filename, MESHDATATYPE meshdttp, MESHTYPE meshtype)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == ROOT_RANK)
    {
        switch (meshdttp)
        {
        case LINETYPE:
            switch (meshtype)
            {
            case LINEMESH:
                Read_Data_line_root(mesh, filename);
                break;
            }
            break;
        case MATLAB:
            switch (meshtype)
            {
            case TRIANGLE:
                // 读取从MATLAB剖分得到的二维三角形网格，并且建立网格
                Read_MatlabData_tri_root(mesh, filename);
                Mesh2DLabel(mesh);
                break;
            }
            break;
        case SIMPLEX:
            switch (meshtype)
            {
            case TETHEDRAL:
                Read_Data_tethedral_root(mesh, filename);
                // printf("Build the mesh!\n");
                Mesh3D4BisectionLabel(mesh); // 对网格进行局部编号调整，使得可以进行一致加密和自适应加密
                // printf("Build the mesh after label!\n");
                break;
            }
            break;
        }
    }
}

void PTWCreate(PTW **ptw)
{
    PTW *p = (PTW *)malloc(sizeof(PTW));
    p->NumRows = -1;
    p->NumCols = -1;
    p->Ptw = NULL;
    p->Entries = NULL;
    p->AndEntries = NULL;
    *ptw = p;
}

void VertsCreate(VERT **verts, INT num, INT worlddim)
{
    INT i;
    VERT *Verts = (VERT *)malloc(num * sizeof(VERT));
    for (i = 0; i < num; i++)
    {
        Verts[i].Index = i;
        Verts[i].Coord = (DOUBLE *)malloc(worlddim * sizeof(DOUBLE));
    }
    *verts = Verts;
}

void LinesCreate(LINE **lines, INT num)
{
    INT i;
    LINE *Lines = (LINE *)malloc(num * sizeof(LINE));
    for (i = 0; i < num; i++)
    {
        Lines[i].Index = i;
    }
    *lines = Lines;
}

void FacesCreate(FACE **faces, INT num)
{
    INT i;
    FACE *Faces = (FACE *)malloc(num * sizeof(FACE));
    for (i = 0; i < num; i++)
    {
        Faces[i].Index = i;
        Faces[i].NumVerts = MAXVERT4FACE;
        Faces[i].NumLines = MAXLINE4FACE;
    }
    *faces = Faces;
}

void VolusCreate(VOLU **volus, INT num)
{
    INT i;
    VOLU *Volus = (VOLU *)malloc(num * sizeof(VOLU));
    for (i = 0; i < num; i++)
    {
        Volus[i].Index = i;
        Volus[i].NumVerts = MAXVERT4VOLU;
        Volus[i].NumLines = MAXLINE4VOLU;
        Volus[i].NumFaces = MAXFACE4VOLU;
    }
    *volus = Volus;
}

void MeshCreate(MESH **mesh, INT worlddim, MPI_Comm comm)
{
    if (*mesh != NULL)
    {
        OpenPFEM_Print("cannot create mesh with a non-null pointer MESH*mesh\n");
        return;
    }
    MESH *parmesh = (MESH *)malloc(sizeof(MESH));
#if MPI_USE
    parmesh->comm = comm;
#elif
    parmesh->comm = SQEMODE;
#endif
    parmesh->worlddim = worlddim;
    parmesh->num_vert = -1;
    parmesh->num_line = -1;
    parmesh->num_face = -1;
    parmesh->num_volu = -1;
    parmesh->Verts = NULL;
    parmesh->Lines = NULL;
    parmesh->Faces = NULL;
    parmesh->Volus = NULL;
    parmesh->Fathers = NULL;
    parmesh->Ancestors = NULL;
    parmesh->level_id = 0;
    parmesh->IsJacobiInfoAvailable = 0;
    parmesh->JacobiOfAllElem = NULL;
    parmesh->InvJacobiOfAllElem = NULL;
    parmesh->VolumnOfAllElem = NULL;
    parmesh->SharedInfo = NULL;
    *mesh = parmesh;
    return;
}

void MeshDestroy(MESH **mesh)
{
    INT i;
    for (i = 0; i < (*mesh)->num_vert; i++)
    {
        OpenPFEM_Free((*mesh)->Verts[i].Coord);
    }
    OpenPFEM_Free((*mesh)->Verts);
    OpenPFEM_Free((*mesh)->Lines);
    OpenPFEM_Free((*mesh)->Faces);
    OpenPFEM_Free((*mesh)->Volus);
    OpenPFEM_Free((*mesh)->Fathers);
    OpenPFEM_Free((*mesh)->Ancestors);
    if ((*mesh)->IsJacobiInfoAvailable == 1)
    {
        OpenPFEM_Free((*mesh)->JacobiOfAllElem);
        OpenPFEM_Free((*mesh)->InvJacobiOfAllElem);
        OpenPFEM_Free((*mesh)->VolumnOfAllElem);
    }
    SharedInfoDestroy(&((*mesh)->SharedInfo));
    OpenPFEM_Free((*mesh));
}

void SharedInfoDestroy(SHAREDINFO **SharedInfo)
{
    if ((*SharedInfo) == NULL)
        return;
    INT i, num = (*SharedInfo)->NumNeighborRanks;
    OpenPFEM_Free((*SharedInfo)->NeighborRanks);
    for (i = 0; i < num; i++)
    {
        SharedGeoDestroy(&((*SharedInfo)->SharedVerts[i]));
        if ((*SharedInfo)->SharedLines != NULL)
        {
            SharedGeoDestroy(&((*SharedInfo)->SharedLines[i]));
        }
        if ((*SharedInfo)->SharedFaces != NULL)
        {
            SharedGeoDestroy(&((*SharedInfo)->SharedFaces[i]));
        }
    }
    OpenPFEM_Free((*SharedInfo)->SharedVerts);
    if ((*SharedInfo)->SharedLines != NULL)
    {
        OpenPFEM_Free((*SharedInfo)->SharedLines);
    }
    if ((*SharedInfo)->SharedFaces != NULL)
    {
        OpenPFEM_Free((*SharedInfo)->SharedFaces);
    }
    OpenPFEM_Free((*SharedInfo));
}

void SharedGeoDestroy(SHAREDGEO *SharedGeo)
{
    if (SharedGeo == NULL || SharedGeo->SharedNum == 0)
        return;
    OpenPFEM_Free(SharedGeo->Index);
    OpenPFEM_Free(SharedGeo->Owner);
    OpenPFEM_Free(SharedGeo->SharedIndex);
}

void VertsReCreate(VERT **verts, INT num, INT new_num, INT worlddim)
{
    VERT *Verts = *verts;
    INT idx;
    Verts = (VERT *)realloc(Verts, new_num * sizeof(VERT));
    for (idx = num; idx < new_num; idx++)
    {
        Verts[idx].Index = idx;
        Verts[idx].Coord = (DOUBLE *)malloc(worlddim * sizeof(DOUBLE));
    }
    *verts = Verts;
}

void LinesReCreate(LINE **lines, INT num, INT new_num)
{
    LINE *Lines = *lines;
    INT idx;
    Lines = (LINE *)realloc(Lines, new_num * sizeof(LINE));
    for (idx = num; idx < new_num; idx++)
    {
        Lines[idx].Index = idx;
    }
    *lines = Lines;
}

void FacesReCreate(FACE **faces, INT num, INT new_num)
{
    FACE *Faces = *faces;
    INT idx;
    Faces = (FACE *)realloc(Faces, new_num * sizeof(FACE));
    for (idx = num; idx < new_num; idx++)
    {
        Faces[idx].Index = idx;
        Faces[idx].NumVerts = MAXVERT4FACE;
        Faces[idx].NumLines = MAXLINE4FACE;
    }
    *faces = Faces;
}

void VolusReCreate(VOLU **volus, INT num, INT new_num)
{
    VOLU *Volus = *volus;
    INT idx;
    Volus = (VOLU *)realloc(Volus, new_num * sizeof(VOLU));
    for (idx = num; idx < new_num; idx++)
    {
        Volus[idx].Index = idx;
        Volus[idx].NumVerts = MAXVERT4VOLU;
        Volus[idx].NumLines = MAXLINE4VOLU;
        Volus[idx].NumFaces = MAXFACE4VOLU;
    }
    *volus = Volus;
}

static void Get_VertBDID_from_LineBDID(MESH *mesh)
{
    INT i;
    VERT *Verts = mesh->Verts, *vert;
    LINE *Lines = mesh->Lines, *line;
    /* 0 for interior */
    for (i = 0; i < mesh->num_vert; i++)
    {
        Verts[i].BD_ID = 0;
    }
    for (i = 0; i < mesh->num_line; i++)
    {
        line = &(Lines[i]);
        if (line->BD_ID != 0)
        {
            vert = &(Verts[line->Vert4Line[0]]);
            if (vert->BD_ID == 0)
            {
                vert->BD_ID = line->BD_ID;
            }
            vert = &(Verts[line->Vert4Line[1]]);
            if (vert->BD_ID == 0)
            {
                vert->BD_ID = line->BD_ID;
            }
        }
    }
    return;
}

// 从线的节点信息和面的节点信息重建面的线信息
static void BuildLine4Face_fromVert4Face(MESH *mesh)
{
    INT idx_face, idx_line, j1, vert0ind, vert1ind, j2, j3, start, end, j, tmp;
    // 产生Vert2Lines的信息
    PTW *Vert2Lines;
    PTWCreate(&Vert2Lines);
    GetVert2Lines(mesh, Vert2Lines);
    INT *Ptw = Vert2Lines->Ptw, *Entries = Vert2Lines->Entries, *AndEntries = Vert2Lines->AndEntries;
    LINE *Lines = mesh->Lines, *line;
    FACE *Faces = mesh->Faces, *face;
    for (idx_face = 0; idx_face < mesh->num_face; idx_face++)
    {
        face = &(Faces[idx_face]);
        for (j1 = 0; j1 < face->NumLines; j1++)
        {
            j2 = (j1 + 1) % (face->NumLines); // 第一个节点的局部编号
            j3 = (j1 + 2) % (face->NumLines); // 第二个节点的局部编号
            vert0ind = face->Vert4Face[j2];
            vert1ind = face->Vert4Face[j3];
            // 对上面两点编号进行排序
            if (vert0ind > vert1ind)
            {
                tmp = vert0ind;
                vert0ind = vert1ind;
                vert1ind = tmp;
            }
            // 开始进行寻找线的编号
            start = Ptw[vert0ind];
            end = Ptw[vert0ind + 1];
            for (j = start; j < end; j++)
            {
                if (Entries[j] == vert1ind)
                { // 找到了线的另一个节点, 则就可以得到相应的边的编号
                    mesh->Faces[idx_face].Line4Face[j1] = AndEntries[j];
                } // end if
            }     // end for j
        }         // end for j1
    }             // end for idx_face
    PTWDestroy(&Vert2Lines);
} // end of this program

// 下面的程序是用来读入Matlab格式的二维网格文件
// 需要介绍一下网格的格式！！！！
void Read_Data_line_root(MESH *mesh, const char *filename)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == ROOT_RANK)
    {
        INT status = 0, i = 0;
        FILE *file = fopen(filename, "r");
        assert(file);
        // 读入网格的节点个数和线段个数
        if (EOF == fscanf(file, "%d %d", &(mesh->num_vert), &(mesh->num_line)))
        {
            status = -1;
        }
        assert(status + 1);
#if PRINT_INFO
        OpenPFEM_Print("read %d vertices and %d lines for the mesh.\n", mesh->num_vert, mesh->num_line);
#endif
        VertsCreate(&(mesh->Verts), mesh->num_vert, 2);
        LinesCreate(&(mesh->Lines), mesh->num_line);
        /* vert */
        VERT *Verts = NULL;
        for (i = 0; i < mesh->num_vert; i++)
        {
            Verts = &(mesh->Verts[i]);
            Verts->Index = i;
            if (EOF == fscanf(file, "%lf,  %d", &(Verts->Coord[0]), &(Verts->BD_ID)))
            {
                status = -1;
            }
        }
        assert(status + 1);
        /* line */
        LINE *Lines = NULL;
        for (i = 0; i < mesh->num_line; i++)
        {
            Lines = &(mesh->Lines[i]);
            Lines->Index = i;
            if (EOF == fscanf(file, "%d %d", &(Lines->Vert4Line[0]), &(Lines->Vert4Line[1])))
            {
                status = -1;
            }
        }
        assert(status + 1);
        fclose(file);
    } // end if (rank == ROOT_RANK)
}

// 下面的程序是用来读入Matlab格式的二维网格文件
// 需要介绍一下网格的格式！！！！
void Read_MatlabData_tri_root(MESH *mesh, const char *filename)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == ROOT_RANK)
    {
        INT status = 0, i = 0;
        FILE *file = fopen(filename, "r");
        assert(file);
        if (EOF == fscanf(file, "%d %d %d", &(mesh->num_vert), &(mesh->num_line), &(mesh->num_face)))
        {
            status = -1;
        }
        assert(status + 1);
#if PRINT_INFO
        OpenPFEM_Print("read %d vertices %d lines and %d faces for the mesh.\n", mesh->num_vert, mesh->num_line, mesh->num_face);
#endif
        VertsCreate(&(mesh->Verts), mesh->num_vert, 2);
        LinesCreate(&(mesh->Lines), mesh->num_line);
        FacesCreate(&(mesh->Faces), mesh->num_face);
        /* vert */
        VERT *Verts = NULL;
        for (i = 0; i < mesh->num_vert; i++)
        {
            Verts = &(mesh->Verts[i]);
            Verts->Index = i;
            if (EOF == fscanf(file, "%lf %lf", &(Verts->Coord[0]), &(Verts->Coord[1])))
            {
                status = -1;
            }
        }
        assert(status + 1);
        /* line */
        LINE *Lines = NULL;
        for (i = 0; i < mesh->num_line; i++)
        {
            Lines = &(mesh->Lines[i]);
            Lines->Index = i;
            if (EOF == fscanf(file, "%d %d %d", &(Lines->Vert4Line[0]), &(Lines->Vert4Line[1]), &(Lines->BD_ID)))
            {
                status = -1;
            }
            Lines->Vert4Line[0]--;
            Lines->Vert4Line[1]--;
        }
        assert(status + 1);
        /* face */
        FACE *Faces = NULL;
        for (i = 0; i < mesh->num_face; i++)
        {
            Faces = &(mesh->Faces[i]);
            if (EOF == fscanf(file, "%d %d %d", &(Faces->Vert4Face[0]), &(Faces->Vert4Face[1]), &(Faces->Vert4Face[2])))
            {
                status = -1;
            }
            Faces->Vert4Face[0]--;
            Faces->Vert4Face[1]--;
            Faces->Vert4Face[2]--;
        }
        assert(status + 1);
        fclose(file);
        Get_VertBDID_from_LineBDID(mesh);
        BuildLine4Face_fromVert4Face(mesh);
    }
}

void Read_Data_tethedral_root(MESH *mesh, const char *filename)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == ROOT_RANK)
    {
        INT status = 0, i = 0;
        FILE *file = fopen(filename, "r");
        assert(file);

        char wd[9];
        INT checkwd;
        if (EOF == fscanf(file, "%s %d", wd, &(checkwd)))
        {
            status = -1;
        }
        if ((strncmp(wd, "WORLDDIM", 8)) || (checkwd != 3))
        {
            status = -1;
        }
        assert(status + 1);
#if PRINT_INFO
        OpenPFEM_Print("read %d vertices %d lines and %d faces for the mesh.\n", mesh->num_vert, mesh->num_line, mesh->num_face);
#endif

        /* vert */
        char s1[20], s2[20], s3[20];
        if (EOF == fscanf(file, "%s %s %s %d", s1, s2, s3, &(mesh->num_vert)))
        {
            status = -1;
        }
        if ((strncmp(s1, "number", 6)) || (strncmp(s2, "of", 2)) || (strncmp(s3, "vertices", 8)))
        {
            status = -1;
        }
        assert(status + 1);
        VertsCreate(&(mesh->Verts), mesh->num_vert, 3);
        VERT *Verts = NULL;

        /* volu */
        if (EOF == fscanf(file, "%s %s %s %d", s1, s2, s3, &(mesh->num_volu)))
        {
            status = -1;
        }
        if ((strncmp(s1, "number", 6)) || (strncmp(s2, "of", 2)) || (strncmp(s3, "elements", 8)))
        {
            status = -1;
        }
        assert(status + 1);
        VolusCreate(&(mesh->Volus), mesh->num_volu);
        VOLU *Volus = NULL;

        /* vert */
        if (EOF == fscanf(file, "%s %s", s1, s2))
        {
            status = -1;
        }
        if ((strncmp(s1, "vertex", 6)) || (strncmp(s2, "coordinates", 11)))
        {
            status = -1;
        }
        assert(status + 1);
        for (i = 0; i < mesh->num_vert; i++)
        {
            Verts = &(mesh->Verts[i]);
            Verts->Index = i;
            if (EOF == fscanf(file, "%lf %lf %lf", &(Verts->Coord[0]), &(Verts->Coord[1]), &(Verts->Coord[2])))
            {
                status = -1;
            }
        }
        assert(status + 1);

        /* volu */
        if (EOF == fscanf(file, "%s %s", s1, s2))
        {
            status = -1;
        }
        if ((strncmp(s1, "element", 7)) || (strncmp(s2, "vertices", 8)))
        {
            status = -1;
        }
        assert(status + 1);
        for (i = 0; i < mesh->num_volu; i++)
        {
            Volus = &(mesh->Volus[i]);
            Volus->Index = i;
            if (EOF == fscanf(file, "%d %d %d %d", &(Volus->Vert4Volu[0]), &(Volus->Vert4Volu[1]), &(Volus->Vert4Volu[2]), &(Volus->Vert4Volu[3])))
            {
                status = -1;
            }
        }
        assert(status + 1);

        /* boundary */
        if (EOF == fscanf(file, "%s %s", s1, s2))
        {
            status = -1;
        }
        if ((strncmp(s1, "element", 6)) || (strncmp(s2, "boundaries", 10)))
        {
            status = -1;
        }
        assert(status + 1);
        INT *elembd = (INT *)malloc(MAXFACE4VOLU * mesh->num_volu * sizeof(INT));
        for (i = 0; i < mesh->num_volu; i++)
        {
            if (EOF == fscanf(file, "%d %d %d %d", &(elembd[4 * i]), &(elembd[4 * i + 1]), &(elembd[4 * i + 2]), &(elembd[4 * i + 3])))
            {
                status = -1;
            }
        }
        assert(status + 1);

        fclose(file);
        // 然后再把网格信息完备化
        Mesh3DComplete(mesh, elembd);
        OpenPFEM_Free(elembd);
    }
}

void MeshUniformRefine(MESH *mesh, INT time)
{
    INT i;
    switch (mesh->worlddim)
    {
    case 1:
        for (i = 0; i < time; i++)
        {
            MeshUniformRefine1D(mesh);
        }
        break;
    case 2:
        for (i = 0; i < time; i++)
        {
            MeshUniformRefine2D(mesh);
        }
        break;
    case 3:
        for (i = 0; i < time; i++)
        {
            MeshUniformRefine3D(mesh);
        }
        break;
    }

    if (time > 0)
    {
        // 清空Jacobian的数据
        if (mesh->IsJacobiInfoAvailable == 1)
        {
            OpenPFEM_Free(mesh->JacobiOfAllElem);
            OpenPFEM_Free(mesh->InvJacobiOfAllElem);
            OpenPFEM_Free(mesh->VolumnOfAllElem);
            mesh->IsJacobiInfoAvailable = 0;
        }
        // 更新网格层数
        for (i = 0; i < time; i++)
        {
            mesh->level_id++;
        }
    }
#if PRINT_INFO
    OpenPFEM_Print("============================================\n");
    OpenPFEM_Print("UniformRefineMesh %d time(s) complete!\n", time);
    OpenPFEM_Print("============================================\n");
#endif
}

void MeshAdaptiveRefine(MESH *mesh, DOUBLE *ElemErrors, DOUBLE theta)
{
    INT i;
    switch (mesh->worlddim)
    {
    case 1:
        MeshAdaptiveRefine1D(mesh, ElemErrors, theta);
        break;
    case 2:
        MeshAdaptiveRefine2D(mesh, ElemErrors, theta);
        break;
    case 3:
        MeshAdaptiveRefine3D(mesh, ElemErrors, theta);
        break;
    }
    // 清空Jacobian的数据
    if (mesh->IsJacobiInfoAvailable == 1)
    {
        OpenPFEM_Free(mesh->JacobiOfAllElem);
        OpenPFEM_Free(mesh->InvJacobiOfAllElem);
        OpenPFEM_Free(mesh->VolumnOfAllElem);
        mesh->IsJacobiInfoAvailable = 0;
    }
    // 更新网格层数
    mesh->level_id++;
}

// 23.01.28
// 将当前网格设置为祖先网格 使用该网格进行加密后的网格存储的ancestor为对应的该网格中的单元编号
void MeshSetAsAncestor(MESH *mesh)
{
    mesh->level_id = 0;
    OpenPFEM_Free(mesh->Fathers);
    OpenPFEM_Free(mesh->Ancestors);
}

// 复制网格, 硬Copy
MESH *MeshDuplicate(MESH *mesh)
{
    MESH *mesh_new = malloc(sizeof(MESH));
    mesh_new->worlddim = mesh->worlddim;
    mesh_new->num_vert = mesh->num_vert;
    mesh_new->num_line = mesh->num_line;
    mesh_new->num_face = mesh->num_face;
    // verts
    mesh_new->Verts = malloc(mesh_new->num_vert * sizeof(VERT));
    memcpy(mesh_new->Verts, mesh->Verts, mesh_new->num_vert * sizeof(VERT));
    int i;
    for (i = 0; i < mesh_new->num_vert; i++)
    {
        DOUBLE *tmp = (DOUBLE *)malloc(mesh->worlddim * sizeof(DOUBLE));
        mesh_new->Verts[i].Coord = tmp;
        memcpy(mesh_new->Verts[i].Coord, mesh->Verts[i].Coord, mesh_new->worlddim * sizeof(DOUBLE));
    }
    // lines
    mesh_new->Lines = malloc(mesh_new->num_line * sizeof(LINE));
    memcpy(mesh_new->Lines, mesh->Lines, mesh_new->num_line * sizeof(LINE));
    // faces & volus
    if (mesh->worlddim >= 2)
    {
        mesh_new->Faces = malloc(mesh_new->num_face * sizeof(FACE));
        memcpy(mesh_new->Faces, mesh->Faces, mesh_new->num_face * sizeof(FACE));
        if (mesh->worlddim == 3)
        {
            mesh_new->num_volu = mesh->num_volu;
            mesh_new->Volus = malloc(mesh_new->num_volu * sizeof(VOLU));
            memcpy(mesh_new->Volus, mesh->Volus, mesh_new->num_volu * sizeof(VOLU));
        }
    }

    // fathers
    if (mesh->Fathers != NULL)
    {
        switch (mesh->worlddim)
        {
        case 1:
            mesh_new->Fathers = malloc(mesh_new->num_line * sizeof(INT));
            memcpy(mesh_new->Fathers, mesh->Fathers, mesh_new->num_line * sizeof(INT));
            break;
        case 2:
            mesh_new->Fathers = malloc(mesh_new->num_face * sizeof(INT));
            memcpy(mesh_new->Fathers, mesh->Fathers, mesh_new->num_face * sizeof(INT));
            break;
        case 3:
            mesh_new->Fathers = malloc(mesh_new->num_volu * sizeof(INT));
            memcpy(mesh_new->Fathers, mesh->Fathers, mesh_new->num_volu * sizeof(INT));
            break;
        }
    }
    else
    {
        mesh_new->Fathers = NULL;
    }

    // ancestors
    if (mesh->Ancestors != NULL)
    {
        switch (mesh->worlddim)
        {
        case 1:
            mesh_new->Ancestors = malloc(mesh_new->num_line * sizeof(INT));
            memcpy(mesh_new->Ancestors, mesh->Ancestors, mesh_new->num_line * sizeof(INT));
            break;
        case 2:
            mesh_new->Ancestors = malloc(mesh_new->num_face * sizeof(INT));
            memcpy(mesh_new->Ancestors, mesh->Ancestors, mesh_new->num_face * sizeof(INT));
            break;
        case 3:
            mesh_new->Ancestors = malloc(mesh_new->num_volu * sizeof(INT));
            memcpy(mesh_new->Ancestors, mesh->Ancestors, mesh_new->num_volu * sizeof(INT));
            break;
        }
        mesh_new->Ancestors = malloc(mesh_new->num_volu * sizeof(INT));
        memcpy(mesh_new->Ancestors, mesh->Ancestors, mesh_new->num_volu * sizeof(INT));
    }
    else
    {
        mesh_new->Ancestors = NULL;
    }

    // level_id
    mesh_new->level_id = mesh->level_id;

    // IsJacobiInfoAvailable
    mesh_new->IsJacobiInfoAvailable = mesh->IsJacobiInfoAvailable;

    // JacobiOfAllElem
    if (mesh->IsJacobiInfoAvailable == 1)
    {
        switch (mesh->worlddim)
        {
        case 1:
            mesh_new->JacobiOfAllElem = malloc(mesh_new->num_line * sizeof(INT));
            memcpy(mesh_new->JacobiOfAllElem, mesh->JacobiOfAllElem, mesh_new->num_line * sizeof(INT));
            mesh_new->InvJacobiOfAllElem = malloc(mesh_new->num_line * sizeof(INT));
            memcpy(mesh_new->InvJacobiOfAllElem, mesh->InvJacobiOfAllElem, mesh_new->num_line * sizeof(INT));
            mesh_new->VolumnOfAllElem = malloc(mesh_new->num_line * sizeof(INT));
            memcpy(mesh_new->VolumnOfAllElem, mesh->VolumnOfAllElem, mesh_new->num_line * sizeof(INT));
            break;
        case 2:
            mesh_new->JacobiOfAllElem = malloc(4 * mesh_new->num_face * sizeof(INT));
            memcpy(mesh_new->JacobiOfAllElem, mesh->JacobiOfAllElem, 4 * mesh_new->num_face * sizeof(INT));
            mesh_new->InvJacobiOfAllElem = malloc(4 * mesh_new->num_face * sizeof(INT));
            memcpy(mesh_new->InvJacobiOfAllElem, mesh->InvJacobiOfAllElem, 4 * mesh_new->num_face * sizeof(INT));
            mesh_new->VolumnOfAllElem = malloc(mesh_new->num_face * sizeof(INT));
            memcpy(mesh_new->VolumnOfAllElem, mesh->VolumnOfAllElem, mesh_new->num_face * sizeof(INT));
            break;
        case 3:
            mesh_new->JacobiOfAllElem = malloc(9 * mesh_new->num_volu * sizeof(INT));
            memcpy(mesh_new->JacobiOfAllElem, mesh->JacobiOfAllElem, 9 * mesh_new->num_volu * sizeof(INT));
            mesh_new->InvJacobiOfAllElem = malloc(9 * mesh_new->num_volu * sizeof(INT));
            memcpy(mesh_new->InvJacobiOfAllElem, mesh->InvJacobiOfAllElem, 9 * mesh_new->num_volu * sizeof(INT));
            mesh_new->VolumnOfAllElem = malloc(mesh_new->num_volu * sizeof(INT));
            memcpy(mesh_new->VolumnOfAllElem, mesh->VolumnOfAllElem, mesh_new->num_volu * sizeof(INT));
            break;
        }
    }
    else
    {
        mesh_new->JacobiOfAllElem = NULL;
        mesh_new->InvJacobiOfAllElem = NULL;
        mesh_new->VolumnOfAllElem = NULL;
    }

    mesh_new->comm = mesh->comm;
    mesh_new->SharedInfo = SharedInfoDuplicate(mesh->SharedInfo);
    return mesh_new;
}

SHAREDINFO *SharedInfoDuplicate(SHAREDINFO *sharedinfo)
{
    SHAREDINFO *sharedinfo_new = (SHAREDINFO *)malloc(sizeof(SHAREDINFO));
    sharedinfo_new->worlddim = sharedinfo->worlddim;
    sharedinfo_new->neighborcomm = sharedinfo->neighborcomm;
    sharedinfo_new->NumNeighborRanks = sharedinfo->NumNeighborRanks;
    sharedinfo_new->NeighborRanks = (INT *)malloc(sharedinfo_new->NumNeighborRanks * sizeof(INT));
    memcpy(sharedinfo_new->NeighborRanks, sharedinfo->NeighborRanks, sharedinfo_new->NumNeighborRanks * sizeof(INT));
    sharedinfo_new->SharedVerts = (SHAREDGEO *)malloc(sharedinfo_new->NumNeighborRanks * sizeof(SHAREDGEO));
    if (sharedinfo->worlddim >= 2)
    {
        sharedinfo_new->SharedLines = (SHAREDGEO *)malloc(sharedinfo_new->NumNeighborRanks * sizeof(SHAREDGEO));
    }
    else
    {
        sharedinfo_new->SharedLines = NULL;
    }
    if (sharedinfo->worlddim == 3)
    {
        sharedinfo_new->SharedFaces = (SHAREDGEO *)malloc(sharedinfo_new->NumNeighborRanks * sizeof(SHAREDGEO));
    }
    else
    {
        sharedinfo_new->SharedFaces = NULL;
    }
    sharedinfo_new->SharedVolus = NULL;
    INT i;
    for (i = 0; i < sharedinfo_new->NumNeighborRanks; i++)
    {
        sharedinfo_new->SharedVerts[i].SharedNum = sharedinfo->SharedVerts[i].SharedNum;
        if (sharedinfo_new->SharedVerts[i].SharedNum)
        {
            sharedinfo_new->SharedVerts[i].Index = (INT *)malloc(sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
            memcpy(sharedinfo_new->SharedVerts[i].Index, sharedinfo->SharedVerts[i].Index, sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
            sharedinfo_new->SharedVerts[i].Owner = (INT *)malloc(sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
            memcpy(sharedinfo_new->SharedVerts[i].Owner, sharedinfo->SharedVerts[i].Owner, sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
            sharedinfo_new->SharedVerts[i].SharedIndex = (INT *)malloc(sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
            memcpy(sharedinfo_new->SharedVerts[i].SharedIndex, sharedinfo->SharedVerts[i].SharedIndex, sharedinfo_new->SharedVerts[i].SharedNum * sizeof(INT));
        }
        else
        {
            sharedinfo_new->SharedVerts[i].Index = NULL;
            sharedinfo_new->SharedVerts[i].Owner = NULL;
            sharedinfo_new->SharedVerts[i].SharedIndex = NULL;
        }
        if (sharedinfo->worlddim >= 2)
        {
            sharedinfo_new->SharedLines[i].SharedNum = sharedinfo->SharedLines[i].SharedNum;
            if (sharedinfo_new->SharedLines[i].SharedNum)
            {
                sharedinfo_new->SharedLines[i].Index = (INT *)malloc(sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
                memcpy(sharedinfo_new->SharedLines[i].Index, sharedinfo->SharedLines[i].Index, sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
                sharedinfo_new->SharedLines[i].Owner = (INT *)malloc(sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
                memcpy(sharedinfo_new->SharedLines[i].Owner, sharedinfo->SharedLines[i].Owner, sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
                sharedinfo_new->SharedLines[i].SharedIndex = (INT *)malloc(sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
                memcpy(sharedinfo_new->SharedLines[i].SharedIndex, sharedinfo->SharedLines[i].SharedIndex, sharedinfo_new->SharedLines[i].SharedNum * sizeof(INT));
            }
            else
            {
                sharedinfo_new->SharedLines[i].Index = NULL;
                sharedinfo_new->SharedLines[i].Owner = NULL;
                sharedinfo_new->SharedLines[i].SharedIndex = NULL;
            }
            if (sharedinfo->worlddim == 3)
            {
                sharedinfo_new->SharedFaces[i].SharedNum = sharedinfo->SharedFaces[i].SharedNum;
                if (sharedinfo_new->SharedFaces[i].SharedNum)
                {
                    sharedinfo_new->SharedFaces[i].Index = (INT *)malloc(sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                    memcpy(sharedinfo_new->SharedFaces[i].Index, sharedinfo->SharedFaces[i].Index, sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                    sharedinfo_new->SharedFaces[i].Owner = (INT *)malloc(sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                    memcpy(sharedinfo_new->SharedFaces[i].Owner, sharedinfo->SharedFaces[i].Owner, sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                    sharedinfo_new->SharedFaces[i].SharedIndex = (INT *)malloc(sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                    memcpy(sharedinfo_new->SharedFaces[i].SharedIndex, sharedinfo->SharedFaces[i].SharedIndex, sharedinfo_new->SharedFaces[i].SharedNum * sizeof(INT));
                }
                else
                {
                    sharedinfo_new->SharedFaces[i].Index = NULL;
                    sharedinfo_new->SharedFaces[i].Owner = NULL;
                    sharedinfo_new->SharedFaces[i].SharedIndex = NULL;
                }
            }
        }
    }

    return sharedinfo_new;
}

static INT Get_Size_of_Sharedinfo4GEO(SHAREDINFO4GEO *SharedInfo4Verts)
{
    INT size = 0;
    INT sharednum = SharedInfo4Verts->SharedNum;
    INT infonum = SharedInfo4Verts->Ptw[sharednum];
    size += sizeof(INT);
    size += sharednum * sizeof(INT);
    size += (sharednum + 1) * sizeof(INT);
    size += 2 * infonum * sizeof(INT);
    return size;
}

INT PackageSizeVertInfo(MESH *mesh, PTW *Vert4Ranks, SHAREDINFO4GEO **SharedInfo4Verts, INT size,
                        INT **vertcount, INT **vertdisplc)
{
    INT i, j, num;
    INT *ptwv4r = Vert4Ranks->Ptw;
    INT *v4r = Vert4Ranks->Entries;
    // 计算package大小
    INT *package_size = (INT *)calloc(size, sizeof(INT));
    for (i = 0; i < size; i++)
    {
        num = ptwv4r[i + 1] - ptwv4r[i];
        package_size[i] += sizeof(INT); // 点的个数
        package_size[i] += num * mesh->worlddim * sizeof(DOUBLE);
        package_size[i] += num * sizeof(INT);
        package_size[i] += Get_Size_of_Sharedinfo4GEO(SharedInfo4Verts[i]);
    }

    INT *package_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = package_size[0];
    package_displc[0] = 0;
    for (i = 1; i < size; i++)
    {
        package_displc[i] = package_displc[i - 1] + package_size[i - 1];
        totalsize += package_size[i];
    }

    *vertcount = package_size;
    *vertdisplc = package_displc;

    return totalsize;
}

INT PackageSizeLineInfo(MESH *mesh, PTW *Line4Ranks, SHAREDINFO4GEO **SharedInfo4Lines,
                        INT size, INT **LineCount, INT **LineDisplc)
{
    INT i, j, num;
    INT *ptwl4r = Line4Ranks->Ptw;
    INT *l4r = Line4Ranks->Entries;
    // 计算package大小
    INT *package_size = (INT *)calloc(size, sizeof(INT));
    for (i = 0; i < size; i++)
    {
        num = ptwl4r[i + 1] - ptwl4r[i];
        package_size[i] += sizeof(INT); // 线的个数
        package_size[i] += num * 2 * sizeof(INT);
        package_size[i] += num * sizeof(INT);
        package_size[i] += Get_Size_of_Sharedinfo4GEO(SharedInfo4Lines[i]);
    }
    INT *package_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = package_size[0];
    package_displc[0] = 0;
    for (i = 1; i < size; i++)
    {
        package_displc[i] = package_displc[i - 1] + package_size[i - 1];
        totalsize += package_size[i];
    }

    *LineCount = package_size;
    *LineDisplc = package_displc;

    return totalsize;
}

INT PackageSizeFaceInfo(MESH *mesh, PTW *Face4Ranks, SHAREDINFO4GEO **SharedInfo4Faces, INT size,
                        INT **FaceCount, INT **FaceDisplc)
{
    INT i, j, num;
    INT *ptwf4r = Face4Ranks->Ptw;
    INT *f4r = Face4Ranks->Entries;
    // 计算package大小
    INT *package_size = (INT *)calloc(size, sizeof(INT));
    for (i = 0; i < size; i++)
    {
        num = ptwf4r[i + 1] - ptwf4r[i];
        package_size[i] += sizeof(INT);                      // 面的个数
        package_size[i] += num * MAXVERT4FACE * sizeof(INT); // 面上点的编号
        package_size[i] += num * MAXLINE4FACE * sizeof(INT); // 面上线的编号
        if (mesh->worlddim == 3)
        {
            package_size[i] += num * sizeof(INT); // 面的BD_ID
            package_size[i] += Get_Size_of_Sharedinfo4GEO(SharedInfo4Faces[i]);
        }
    }
    INT *package_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = package_size[0];
    package_displc[0] = 0;
    for (i = 1; i < size; i++)
    {
        package_displc[i] = package_displc[i - 1] + package_size[i - 1];
        totalsize += package_size[i];
    }

    *FaceCount = package_size;
    *FaceDisplc = package_displc;

    return totalsize;
}

INT PackageSizeVoluInfo(MESH *mesh, PTW *Volu4Ranks, INT size, INT **VoluCount, INT **VoluDisplc)
{
    INT i, j, num;
    INT *ptwu4r = Volu4Ranks->Ptw;
    INT *u4r = Volu4Ranks->Entries;
    // 计算package大小
    INT *package_size = (INT *)calloc(size, sizeof(INT));
    for (i = 0; i < size; i++)
    {
        num = ptwu4r[i + 1] - ptwu4r[i];
        package_size[i] += sizeof(INT); // 体的个数
        package_size[i] += num * MAXVERT4VOLU * sizeof(INT);
        package_size[i] += num * MAXLINE4VOLU * sizeof(INT);
        package_size[i] += num * MAXFACE4VOLU * sizeof(INT);
    }
    INT *package_displc = (INT *)malloc(size * sizeof(INT));
    INT totalsize = package_size[0];
    package_displc[0] = 0;
    for (i = 1; i < size; i++)
    {
        package_displc[i] = package_displc[i - 1] + package_size[i - 1];
        totalsize += package_size[i];
    }

    *VoluCount = package_size;
    *VoluDisplc = package_displc;

    return totalsize;
}

static int Get_Sharedinfo4GEO_Packed(SHAREDINFO4GEO *SharedInfo4Verts, char *package, INT totalsize, INT *position)
{
    INT sharednum = SharedInfo4Verts->SharedNum;
    INT infonum = SharedInfo4Verts->Ptw[sharednum];
    MPI_Pack(&(SharedInfo4Verts->SharedNum), 1, MPI_INT, package, totalsize, position, MPI_COMM_SELF);
    MPI_Pack(SharedInfo4Verts->Owners, sharednum, MPI_INT, package, totalsize, position, MPI_COMM_SELF);
    MPI_Pack(SharedInfo4Verts->Ptw, sharednum + 1, MPI_INT, package, totalsize, position, MPI_COMM_SELF);
    MPI_Pack(SharedInfo4Verts->SharedRanks, infonum, MPI_INT, package, totalsize, position, MPI_COMM_SELF);
    MPI_Pack(SharedInfo4Verts->LocalIndex, infonum, MPI_INT, package, totalsize, position, MPI_COMM_SELF);
    return 0;
}

void PackageVertInfo(MESH *mesh, PTW *Vert4Ranks, SHAREDINFO4GEO **SharedInfo4Verts, INT size,
                     char **VertPackage, INT totalsize)
{
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0, num = 0;
    INT i, j;
    INT *ptwv4r = Vert4Ranks->Ptw;
    INT *v4r = Vert4Ranks->Entries;
    VERT *verts = mesh->Verts, *vert;
    MPI_Comm comm = mesh->comm;
    for (i = 0; i < size; i++)
    {
        num = ptwv4r[i + 1] - ptwv4r[i];
        MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        for (j = ptwv4r[i]; j < ptwv4r[i + 1]; j++)
        {
            vert = &(verts[v4r[j]]);
            MPI_Pack(vert->Coord, mesh->worlddim, MPI_DOUBLE, package, totalsize, &position, comm);
            MPI_Pack(&(vert->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
        }
        Get_Sharedinfo4GEO_Packed(SharedInfo4Verts[i], package, totalsize, &position);
    }

    *VertPackage = package;
    return;
}

void PackageLineInfo(MESH *mesh, PTW *Line4Ranks, SHAREDINFO4GEO **SharedInfo4Lines, PTW **LocalVert4Lines,
                     INT size, char **LinePackage, INT totalsize)
{
    MPI_Comm comm = mesh->comm;
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0, num = 0;
    INT i, j;
    INT *ptwl4r = Line4Ranks->Ptw;
    INT *l4r = Line4Ranks->Entries;
    INT *ptwv4l, *v4l;
    LINE *lines = mesh->Lines, *line;
    for (i = 0; i < size; i++)
    {
        num = ptwl4r[i + 1] - ptwl4r[i];
        v4l = LocalVert4Lines[i]->Entries;
        MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        for (j = 0; j < num; j++)
        {
            MPI_Pack(&(v4l[2 * j]), 2, MPI_INT, package, totalsize, &position, comm);
            line = &(lines[l4r[j + ptwl4r[i]]]);
            MPI_Pack(&(line->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
        }
        Get_Sharedinfo4GEO_Packed(SharedInfo4Lines[i], package, totalsize, &position);
    }
    *LinePackage = package;
    return;
}

void PackageFaceInfo(MESH *mesh, PTW *Face4Ranks, SHAREDINFO4GEO **SharedInfo4Faces, PTW **LocalVert4Faces, PTW **LocalLine4Faces,
                     INT size, char **FacePackage, INT totalsize)
{
    MPI_Comm comm = mesh->comm;
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0, num = 0;
    INT i, j;
    INT *ptwf4r = Face4Ranks->Ptw;
    INT *f4r = Face4Ranks->Entries;
    INT *v4f, *l4f;
    FACE *faces = mesh->Faces, *face;
    for (i = 0; i < size; i++)
    {
        num = ptwf4r[i + 1] - ptwf4r[i];
        v4f = LocalVert4Faces[i]->Entries;
        l4f = LocalLine4Faces[i]->Entries;
        MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm);
        for (j = 0; j < num; j++)
        {
            MPI_Pack(&(v4f[MAXVERT4FACE * j]), MAXVERT4FACE, MPI_INT, package, totalsize, &position, comm);
            MPI_Pack(&(l4f[MAXLINE4FACE * j]), MAXLINE4FACE, MPI_INT, package, totalsize, &position, comm);
            if (mesh->worlddim == 3)
            {
                face = &(faces[f4r[j + ptwf4r[i]]]);
                MPI_Pack(&(face->BD_ID), 1, MPI_INT, package, totalsize, &position, comm);
            }
        }
        if (mesh->worlddim == 3)
        {
            Get_Sharedinfo4GEO_Packed(SharedInfo4Faces[i], package, totalsize, &position);
        }
    }
    *FacePackage = package;
    return;
}

void PackageVoluInfo(MESH *mesh, PTW *Volu4Ranks, PTW **LocalVert4Volus, PTW **LocalLine4Volus, PTW **LocalFace4Volus,
                     INT size, char **VoluPackage, INT totalsize)
{
    MPI_Comm comm = mesh->comm;
    char *package = (char *)malloc(totalsize * sizeof(char));
    INT position = 0, num = 0;
    INT i, j;
    INT *ptwu4r = Volu4Ranks->Ptw;
    INT *u4r = Volu4Ranks->Entries;
    INT *v4u, *l4u, *f4u;
    for (i = 0; i < size; i++)
    {
        num = ptwu4r[i + 1] - ptwu4r[i];
        v4u = LocalVert4Volus[i]->Entries;
        l4u = LocalLine4Volus[i]->Entries;
        f4u = LocalFace4Volus[i]->Entries;
        MPI_Pack(&num, 1, MPI_INT, package, totalsize, &position, comm); // 体的个数
        for (j = 0; j < num; j++)
        {
            MPI_Pack(&(v4u[MAXVERT4VOLU * j]), MAXVERT4VOLU, MPI_INT, package, totalsize, &position, comm);
            MPI_Pack(&(l4u[MAXLINE4VOLU * j]), MAXLINE4VOLU, MPI_INT, package, totalsize, &position, comm);
            MPI_Pack(&(f4u[MAXFACE4VOLU * j]), MAXFACE4VOLU, MPI_INT, package, totalsize, &position, comm);
        }
    }
    *VoluPackage = package;
    return;
}

void UnpackedSharedInfo2Mesh(MESH *mesh, INT geotype, char *Package, INT PackageSize, INT *position)
{
    MPI_Comm comm = mesh->comm;
    INT sharednum = 0;
    MPI_Unpack(Package, PackageSize, position, &sharednum, 1, MPI_INT, comm);
#if PRINT_INFO
    MPI_Barrier(comm);
    OpenPFEM_RankPrint(comm, "解包shared中: 收到%d个几何元素\n", sharednum);
#endif
    if (mesh->SharedInfo == NULL)
    {
        SharedInfoCreate(mesh);
    }
    SHAREDINFO4GEO sharedgeo;
    sharedgeo.SharedNum = sharednum;
    sharedgeo.Owners = (INT *)malloc(sharednum * sizeof(INT));
    sharedgeo.Ptw = (INT *)malloc((sharednum + 1) * sizeof(INT));
    MPI_Unpack(Package, PackageSize, position, sharedgeo.Owners, sharednum, MPI_INT, comm);
    MPI_Unpack(Package, PackageSize, position, sharedgeo.Ptw, sharednum + 1, MPI_INT, comm);
    INT num = sharedgeo.Ptw[sharednum];
    sharedgeo.SharedRanks = (INT *)malloc(num * sizeof(INT));
    sharedgeo.LocalIndex = (INT *)malloc(num * sizeof(INT));
    MPI_Unpack(Package, PackageSize, position, sharedgeo.SharedRanks, num, MPI_INT, comm);
    MPI_Unpack(Package, PackageSize, position, sharedgeo.LocalIndex, num, MPI_INT, comm);
    SharedInfoUpdate(mesh, geotype, sharedgeo);
    return;
}

void SharedInfoCreate(MESH *mesh)
{
    SHAREDINFO *sharedinfo = (SHAREDINFO *)malloc(sizeof(SHAREDINFO));
    sharedinfo->worlddim = mesh->worlddim;
    sharedinfo->neighborcomm = MPI_COMM_NULL;
    sharedinfo->NumNeighborRanks = 0;
    sharedinfo->NeighborRanks = NULL;
    sharedinfo->SharedVerts = NULL;
    sharedinfo->SharedLines = NULL;
    sharedinfo->SharedFaces = NULL;
    sharedinfo->SharedVolus = NULL;
    mesh->SharedInfo = sharedinfo;
}

void SharedInfoUpdate(MESH *mesh, INT geotype, SHAREDINFO4GEO sharedgeo)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    INT neignum = sharedinfo->NumNeighborRanks;
    INT *neig = sharedinfo->NeighborRanks;

    // 统计邻居情况
    INT sharednum = sharedgeo.SharedNum;
    INT num = sharedgeo.Ptw[sharednum];
    INT *newrank = (INT *)malloc(num * sizeof(INT));
    memset(newrank, -1, num * sizeof(INT));
    INT newranknum = 0;
    INT *sharedrank = sharedgeo.SharedRanks;
    INT i, j, k, neigrank, flag;
    for (i = 0; i < num; i++)
    {
        flag = 0;
        neigrank = sharedrank[i];
        // 循环已有邻居
        if (neigrank != rank)
        {
            if (neig != NULL)
            {
                for (j = 0; j < neignum; j++)
                {
                    if (neig[j] == neigrank)
                    {
                        flag = 1;
                        break;
                    }
                }
            }
            // 循环已有点
            for (j = 0; j < newranknum; j++)
            {
                if (neigrank == newrank[j])
                {
                    flag = 1;
                    break;
                }
            }
        }
        else
        {
            flag = 1;
        }
        if (flag == 0)
        {
            newrank[newranknum] = neigrank;
            newranknum++;
        }
    }
    neignum += newranknum;
    if (neig == NULL)
    {
        neig = (INT *)malloc(neignum * sizeof(INT));
        memcpy(neig, newrank, neignum * sizeof(neignum));
    }
    else
    {
        neig = (INT *)realloc(neig, neignum * sizeof(INT));
        memcpy(&(neig[neignum - newranknum]), newrank, newranknum * sizeof(INT));
    }
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "解包shared中：有%d个邻居\n", newranknum);
#endif
    OpenPFEM_Free(newrank);

    // 更新sharedgeo
    // 统计和每个邻居的共享个数
    INT *neigsharednum = (INT *)calloc(neignum, sizeof(INT));
    INT *localindex = (INT *)malloc(sharednum * sizeof(INT));
    INT *ptw = sharedgeo.Ptw;
    INT *sharedindex = sharedgeo.LocalIndex;
    for (i = 0; i < sharednum; i++)
    {
        for (k = ptw[i]; k < ptw[i + 1]; k++)
        {
            neigrank = sharedrank[k];
            if (neigrank != rank)
            {
                for (j = 0; j < neignum; j++)
                {
                    if (neigrank == neig[j])
                    {
                        neigsharednum[j]++;
                        sharedrank[k] = j; // 第j个
                    }
                }
            }
            else
            {
                sharedrank[k] = -1; // 本进程
                localindex[i] = sharedindex[k];
            }
        }
    }
    SHAREDGEO *SharedGeo = NULL;
    SharedGeoCreate(&SharedGeo, neignum, neigsharednum);
    // 更新内容物
    memset(neigsharednum, 0, neignum * sizeof(INT));
    INT *owner = sharedgeo.Owners;
    INT ind;
    for (i = 0; i < sharednum; i++) // 对每个点
    {
        for (j = ptw[i]; j < ptw[i + 1]; j++) // 找出其存在的所有进程
        {
            neigrank = sharedrank[j];
            if (neigrank != -1) // 不是本进程
            {
                ind = neigsharednum[neigrank];
                SharedGeo[neigrank].Index[ind] = localindex[i];
                SharedGeo[neigrank].Owner[ind] = owner[i];
                SharedGeo[neigrank].SharedIndex[ind] = sharedindex[j];
                neigsharednum[neigrank]++;
            }
        }
    }
    OpenPFEM_Free(neigsharednum);
    OpenPFEM_Free(localindex);

    switch (geotype)
    {
    case 0:
        sharedinfo->SharedVerts = SharedGeo;
        SharedGeoRenew(&(sharedinfo->SharedLines), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedFaces), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedVolus), sharedinfo->NumNeighborRanks, neignum);
        break;
    case 1:
        sharedinfo->SharedLines = SharedGeo;
        SharedGeoRenew(&(sharedinfo->SharedVerts), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedFaces), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedVolus), sharedinfo->NumNeighborRanks, neignum);
        break;
    case 2:
        sharedinfo->SharedFaces = SharedGeo;
        SharedGeoRenew(&(sharedinfo->SharedVerts), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedLines), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedVolus), sharedinfo->NumNeighborRanks, neignum);
        break;
    case 3:
        sharedinfo->SharedVolus = SharedGeo;
        SharedGeoRenew(&(sharedinfo->SharedVerts), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedLines), sharedinfo->NumNeighborRanks, neignum);
        SharedGeoRenew(&(sharedinfo->SharedFaces), sharedinfo->NumNeighborRanks, neignum);
        break;
    }
    sharedinfo->NumNeighborRanks = neignum;
    sharedinfo->NeighborRanks = neig;
}

void SharedGeoRenew(SHAREDGEO **SharedGeo, INT oldnum, INT newnum)
{
    if (*SharedGeo == NULL)
    {
        return;
    }
    else
    {
        SHAREDGEO *sharedgeo = *SharedGeo;
        sharedgeo = (SHAREDGEO *)realloc(sharedgeo, newnum * sizeof(SHAREDGEO));
        INT i;
        for (i = oldnum; i < newnum; i++)
        {
            sharedgeo[i].SharedNum = 0;
            sharedgeo[i].Owner = NULL;
            sharedgeo[i].Index = NULL;
            sharedgeo[i].SharedIndex = NULL;
        }
        *SharedGeo = sharedgeo;
    }
}

void SharedGeoCreate(SHAREDGEO **SharedGeo, INT num, INT *size)
{
    SHAREDGEO *sharedgeo = (SHAREDGEO *)malloc(num * sizeof(SHAREDGEO));
    INT i;
    for (i = 0; i < num; i++)
    {
        if (size[i])
        {
            sharedgeo[i].SharedNum = size[i];
            sharedgeo[i].Index = (INT *)malloc(size[i] * sizeof(INT));
            sharedgeo[i].SharedIndex = (INT *)malloc(size[i] * sizeof(INT));
            sharedgeo[i].Owner = (INT *)malloc(size[i] * sizeof(INT));
        }
        else
        {
            sharedgeo[i].SharedNum = 0;
            sharedgeo[i].Index = NULL;
            sharedgeo[i].SharedIndex = NULL;
            sharedgeo[i].Owner = NULL;
        }
    }
    *SharedGeo = sharedgeo;
}

void UnpackedVert2Mesh(MESH *mesh, char *VertPackage, INT PackageSize)
{
    INT position = 0;
    MPI_Comm comm = mesh->comm;
    INT num_vert = 0;

    // 点的个数
    MPI_Unpack(VertPackage, PackageSize, &position, &num_vert, 1, MPI_INT, comm);
    mesh->num_vert = num_vert;
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "解包中：收到%d个点\n", num_vert);
#endif

    INT i;
    VERT *verts, *vert;
    VertsCreate(&verts, num_vert, mesh->worlddim);
    for (i = 0; i < num_vert; i++)
    {
        vert = &(verts[i]);
        MPI_Unpack(VertPackage, PackageSize, &position, vert->Coord, mesh->worlddim, MPI_DOUBLE, comm);
        MPI_Unpack(VertPackage, PackageSize, &position, &(vert->BD_ID), 1, MPI_INT, comm);
    }
    if (mesh->Verts != NULL)
    {
        OpenPFEM_Free(mesh->Verts);
    }
    mesh->Verts = verts;
    UnpackedSharedInfo2Mesh(mesh, 0, VertPackage, PackageSize, &position);
    return;
}

void UnpackedLine2Mesh(MESH *mesh, char *LinePackage, INT PackageSize)
{
    INT position = 0;
    MPI_Comm comm = mesh->comm;
    INT num_line = 0;
    // 线的个数
    MPI_Unpack(LinePackage, PackageSize, &position, &num_line, 1, MPI_INT, comm);
    mesh->num_line = num_line;
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "解包中：收到%d条线\n", num_line);
#endif
    INT i;
    LINE *lines, *line;
    LinesCreate(&lines, num_line);
    for (i = 0; i < num_line; i++)
    {
        line = &(lines[i]);
        MPI_Unpack(LinePackage, PackageSize, &position, line->Vert4Line, 2, MPI_INT, comm);
        MPI_Unpack(LinePackage, PackageSize, &position, &(line->BD_ID), 1, MPI_INT, comm);
    }
    if (mesh->Lines != NULL)
    {
        OpenPFEM_Free(mesh->Lines);
    }
    mesh->Lines = lines;
    UnpackedSharedInfo2Mesh(mesh, 1, LinePackage, PackageSize, &position);
}

void UnpackedFace2Mesh(MESH *mesh, char *FacePackage, INT PackageSize)
{
    INT position = 0;
    MPI_Comm comm = mesh->comm;
    INT num_face = 0;
    // 线的个数
    MPI_Unpack(FacePackage, PackageSize, &position, &num_face, 1, MPI_INT, comm);
    mesh->num_face = num_face;
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "解包中：收到%d个面\n", num_face);
#endif
    INT i;
    FACE *faces, *face;
    FacesCreate(&faces, num_face);
    for (i = 0; i < num_face; i++)
    {
        face = &(faces[i]);
        MPI_Unpack(FacePackage, PackageSize, &position, face->Vert4Face, MAXVERT4FACE, MPI_INT, comm);
        MPI_Unpack(FacePackage, PackageSize, &position, face->Line4Face, MAXLINE4FACE, MPI_INT, comm);
        if (mesh->worlddim == 3)
        {
            MPI_Unpack(FacePackage, PackageSize, &position, &(face->BD_ID), 1, MPI_INT, comm);
        }
    }
    if (mesh->Faces != NULL)
    {
        OpenPFEM_Free(mesh->Faces);
    }
    mesh->Faces = faces;
    if (mesh->worlddim == 3)
    {
        UnpackedSharedInfo2Mesh(mesh, 2, FacePackage, PackageSize, &position);
    }
}

void UnpackedVolu2Mesh(MESH *mesh, char *VoluPackage, INT PackageSize)
{
    INT position = 0;
    MPI_Comm comm = mesh->comm;
    INT num_volu = 0;
    // 线的个数
    MPI_Unpack(VoluPackage, PackageSize, &position, &num_volu, 1, MPI_INT, comm);
    mesh->num_volu = num_volu;
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "解包中：收到%d个体\n", num_volu);
#endif
    INT i;
    VOLU *volus, *volu;
    VolusCreate(&volus, num_volu);
    for (i = 0; i < num_volu; i++)
    {
        volu = &(volus[i]);
        MPI_Unpack(VoluPackage, PackageSize, &position, volu->Vert4Volu, MAXVERT4VOLU, MPI_INT, comm);
        MPI_Unpack(VoluPackage, PackageSize, &position, volu->Line4Volu, MAXLINE4VOLU, MPI_INT, comm);
        MPI_Unpack(VoluPackage, PackageSize, &position, volu->Face4Volu, MAXFACE4VOLU, MPI_INT, comm);
    }
    if (mesh->Volus != NULL)
    {
        OpenPFEM_Free(mesh->Volus);
    }
    mesh->Volus = volus;
}

void SharedMeshPrint(MESH *mesh, INT printrank)
{
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    MPI_Comm_rank(mesh->comm, &rank);
    MPI_Barrier(mesh->comm);
    if (rank != printrank)
    {
        return;
    }
    INT i, j, k;
    SHAREDGEO *geo;
    printf("\n================================================\n");
    printf("[RANK %d] 网格有%d个邻居进程: \n\t\t\t", rank, sharedinfo->NumNeighborRanks);
    for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
    {
        printf("%d\t", sharedinfo->NeighborRanks[i]);
    }
    printf("\n================================================\n");

    printf("(1)共享点: \n");
    if (sharedinfo->SharedVerts == NULL)
    {
        printf("共享点信息尚未生成！\n");
    }
    else
    {
        for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
        {
            geo = &(sharedinfo->SharedVerts[i]);
            if (geo->SharedNum == 0)
            {
                printf("和%d进程不存在共享点\n", sharedinfo->NeighborRanks[i]);
            }
            else
            {
                printf("和%d进程共享%d个点,分别为\n", sharedinfo->NeighborRanks[i], geo->SharedNum);
                for (j = 0; j < geo->SharedNum; j++)
                {
                    printf("___Index=%d  SharedIndex=%d  Owner=%d\n", geo->Index[j], geo->SharedIndex[j], geo->Owner[j]);
                }
            }
        }
    }
    printf("-------------------------------------------------\n");

    printf("(2)共享线: \n");
    if (sharedinfo->SharedLines == NULL)
    {
        printf("共享线信息尚未生成！\n");
    }
    else
    {
        for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
        {
            geo = &(sharedinfo->SharedLines[i]);
            if (geo->SharedNum == 0)
            {
                printf("和%d进程不存在共享线\n", sharedinfo->NeighborRanks[i]);
            }
            else
            {
                printf("和%d进程共享%d条线,分别为\n", sharedinfo->NeighborRanks[i], geo->SharedNum);
                for (j = 0; j < geo->SharedNum; j++)
                {
                    printf("___Index=%d  SharedIndex=%d  Owner=%d\n", geo->Index[j], geo->SharedIndex[j], geo->Owner[j]);
                }
            }
        }
    }
    printf("-------------------------------------------------\n");

    printf("(2)共享面: \n");
    if (sharedinfo->SharedFaces == NULL)
    {
        printf("共享面信息尚未生成！\n");
    }
    else
    {
        for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
        {
            geo = &(sharedinfo->SharedFaces[i]);
            if (geo->SharedNum == 0)
            {
                printf("和%d进程不存在共享面\n", sharedinfo->NeighborRanks[i]);
            }
            else
            {
                printf("和%d进程共享%d个面,分别为\n", sharedinfo->NeighborRanks[i], geo->SharedNum);
                for (j = 0; j < geo->SharedNum; j++)
                {
                    printf("___Index=%d  SharedIndex=%d  Owner=%d\n", geo->Index[j], geo->SharedIndex[j], geo->Owner[j]);
                }
            }
        }
    }
    printf("-------------------------------------------------\n");

    printf("(4)共享体: \n");
    if (sharedinfo->SharedVolus == NULL)
    {
        printf("共享体信息尚未生成！\n");
    }
    else
    {
        for (i = 0; i < sharedinfo->NumNeighborRanks; i++)
        {
            geo = &(sharedinfo->SharedVolus[i]);
            if (geo->SharedNum == 0)
            {
                printf("和%d进程不存在共享体\n", sharedinfo->NeighborRanks[i]);
            }
            else
            {
                printf("和%d进程共享%d个体,分别为\n", sharedinfo->NeighborRanks[i], geo->SharedNum);
                for (j = 0; j < geo->SharedNum; j++)
                {
                    printf("___Index=%d  SharedIndex=%d  Owner=%d\n", geo->Index[j], geo->SharedIndex[j], geo->Owner[j]);
                }
            }
        }
    }
    printf("-------------------------------------------------\n");
}

void MeshPartition(MESH *mesh)
{
    if (MPI_USE == 0)
    {
        return;
    }

    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    if (size == 1)
    {
        return;
    }
    // 对点阶段会用到的全局变量进行定义
    INT NumRanks = size;
    INT NumElems = 0;
    INT *ElemRanks = NULL;
    char *VertPackage = NULL, *VertPackageRoot = NULL; // 点的包裹
    INT *VertCount = NULL, *VertDisplc = NULL;
    INT VertPackageSize = 0, VertPackageTotalSize = 0;
    PTW *Vert2Ranks = NULL, *Vert4Ranks = NULL, *Vert4Elems = NULL;
    SHAREDINFO4GEO **SharedInfo4Verts = NULL;
    // 在root进行metis初步单元划分
    if (rank == ROOT_RANK)
    {
        // 得到每个单元上的节点编号
        PTWCreate(&Vert4Elems);
        GetGEO4Elems(mesh, mesh->worlddim, 0, Vert4Elems);
        // 调用Metis得到单元的划分信息
        NumElems = Vert4Elems->NumRows;
        ElemRanks = (INT *)malloc(NumElems * sizeof(INT));
        MetisPartition(mesh, Vert4Elems, NumElems, ElemRanks);
    }
    // 在root进程计算点的信息
    if (rank == ROOT_RANK)
    {
        // 下面开始来处理每个进程所包含点的信息
        PTWCreate(&Vert2Ranks);
        PTWCreate(&Vert4Ranks);
        // 得到与每个节点相联系的进程号
        GetGEO2Ranks(mesh, NumRanks, ElemRanks, mesh->worlddim, 0, Vert2Ranks);
        // 得到每个进程号上的节点集合，Vert4Ranks中节点编号是全局编号
        PTWTranspose(Vert2Ranks, Vert4Ranks);
        // 然后生成每个进程上共享点的信息
        SharedInfo4Verts = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
        GetSharedINFO4GEO(Vert2Ranks, Vert4Ranks, SharedInfo4Verts);
        // 计算vert包裹的大小
        VertPackageTotalSize = PackageSizeVertInfo(mesh, Vert4Ranks, SharedInfo4Verts, NumRanks, &VertCount, &VertDisplc);
    }
    MPI_Request reqvertsize;
    MPI_Iscatter(VertCount, 1, MPI_INT, &VertPackageSize, 1, MPI_INT, ROOT_RANK, comm, &reqvertsize);
    if (rank == ROOT_RANK)
    {
        // 计算vert包裹
        PackageVertInfo(mesh, Vert4Ranks, SharedInfo4Verts, size, &VertPackageRoot, VertPackageTotalSize);
        INT idx;
        for (idx = 0; idx < NumRanks; idx++)
        {
            SharedInfo4GEODestroy(SharedInfo4Verts + idx);
        }
    }
    OpenPFEM_Free(Vert4Ranks);
    OpenPFEM_Free(SharedInfo4Verts);
    MPI_Wait(&reqvertsize, MPI_STATUS_IGNORE);
#if PRINT_INFO == 1
    OpenPFEM_RankPrint(comm, "将接收到ROOT进程%d大小的VERTpackage\n", VertPackageSize);
    return;
#endif
    VertPackage = (char *)malloc(VertPackageSize * sizeof(char));
    MPI_Request reqvert;
    INT flagv = 0;
    MPI_Iscatterv(VertPackageRoot, VertCount, VertDisplc, MPI_PACKED,
                  VertPackage, VertPackageSize, MPI_PACKED, ROOT_RANK, comm, &reqvert);
    MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
    if (flagv)
    {
        // 解包vert包裹填入mesh
        UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
        OpenPFEM_Free(VertPackage);
        OpenPFEM_Free(VertPackageRoot);
        OpenPFEM_Free(VertCount);
        OpenPFEM_Free(VertDisplc);
    }
    // 对线阶段会用到的全局变量进行定义
    char *LinePackage = NULL, *LinePackageRoot = NULL; // 点的包裹
    INT *LineCount = NULL, *LineDisplc = NULL;
    INT LinePackageSize = 0, LinePackageTotalSize = 0;
    PTW *Line2Ranks = NULL, *Line4Ranks = NULL, *Line4Elems = NULL;
    PTW **LocalVert4Lines = NULL;
    SHAREDINFO4GEO **SharedInfo4Lines = NULL;
    // 在root进程计算线的信息
    if (rank == ROOT_RANK)
    {
        // printf("44444444444: %d\n",mesh->num_vert);
        // 下面开始来处理每个进程所包含线的信息
        // 得到每个单元上线的编号, 注意这里单元有可能是线，有可能是面，
        // 也有可能是体得到每个单元上线的信息
        PTWCreate(&Line4Elems);
        GetGEO4Elems(mesh, mesh->worlddim, 1, Line4Elems);
        // 初始化具体的结构体对象
        PTWCreate(&Line2Ranks);
        PTWCreate(&Line4Ranks);
        // 得到与每条线相联系的进程号
        GetGEO2Ranks(mesh, NumRanks, ElemRanks, mesh->worlddim, 1, Line2Ranks);
        // 得到每个进程上线的编号集合
        PTWTranspose(Line2Ranks, Line4Ranks);
        // 得到线的共享信息
        SharedInfo4Lines = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
        GetSharedINFO4GEO(Line2Ranks, Line4Ranks, SharedInfo4Lines);
        // 下面来得到点与线的局部关系
        // 得到每条线上点的编号
        PTW *Vert4Lines = NULL;
        if (mesh->worlddim == 1)
        {
            // 如果单元就是一维的，那么就不用再产生了
            Vert4Lines = Vert4Elems;
        }
        else
        {
            PTWCreate(&Vert4Lines);
            // 得到每条线上的节点信息
            GetGEO4Elems(mesh, 1, 0, Vert4Lines);
            // 更新一下节点个数，因为0进程中目前的点的个数已经改变了
            Vert4Lines->NumCols = Vert4Elems->NumCols;
        }
        LocalVert4Lines = malloc(NumRanks * sizeof(PTW *));
        INT rankind;
        for (rankind = 0; rankind < NumRanks; rankind++)
        {
            PTWCreate(&(LocalVert4Lines[rankind]));
        }
        GetLocalGEO4Elems(Vert4Lines, Line4Ranks, Vert2Ranks, LocalVert4Lines);
        // 计算line包裹的大小
        LinePackageTotalSize = PackageSizeLineInfo(mesh, Line4Ranks, SharedInfo4Lines, NumRanks,
                                                   &LineCount, &LineDisplc);
    }

    if (!flagv)
    {
        MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
        if (flagv)
        {
            // 解包vert包裹填入mesh
            UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
            OpenPFEM_Free(VertPackage);
            OpenPFEM_Free(VertPackageRoot);
            OpenPFEM_Free(VertCount);
            OpenPFEM_Free(VertDisplc);
        }
    }
    MPI_Request reqlinesize;
    MPI_Iscatter(LineCount, 1, MPI_INT, &LinePackageSize, 1, MPI_INT, ROOT_RANK, comm, &reqlinesize);
    if (rank == ROOT_RANK)
    {
        // 计算line包裹
        PackageLineInfo(mesh, Line4Ranks, SharedInfo4Lines, LocalVert4Lines, size, &LinePackageRoot, LinePackageTotalSize);
        INT idx;
        for (idx = 0; idx < NumRanks; idx++)
        {
            SharedInfo4GEODestroy(SharedInfo4Lines + idx);
            PTWDestroy(LocalVert4Lines + idx);
        }
    }
    PTWDestroy(&Line4Ranks);
    OpenPFEM_Free(SharedInfo4Lines);
    OpenPFEM_Free(LocalVert4Lines);
    MPI_Wait(&reqlinesize, MPI_STATUS_IGNORE);
#if PRINT_INFO
    OpenPFEM_RankPrint(comm, "将接收到ROOT进程%d大小的LINEpackage\n", LinePackageSize);
#endif
    LinePackage = (char *)malloc(LinePackageSize * sizeof(char));
    MPI_Request reqline;
    INT flagl = 0, flagf = 0;
    ;
    MPI_Iscatterv(LinePackageRoot, LineCount, LineDisplc, MPI_PACKED,
                  LinePackage, LinePackageSize, MPI_PACKED, ROOT_RANK, comm, &reqline);

    if (!flagv)
    {
        MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
        if (flagv)
        {
            // 解包vert包裹填入mesh
            UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
            OpenPFEM_Free(VertPackage);
            OpenPFEM_Free(VertPackageRoot);
            OpenPFEM_Free(VertCount);
            OpenPFEM_Free(VertDisplc);
        }
    }

    MPI_Test(&reqline, &flagl, MPI_STATUS_IGNORE);
    if (flagl)
    {
        // 解包line包裹填入mesh
        UnpackedLine2Mesh(mesh, LinePackage, LinePackageSize);
        OpenPFEM_Free(LinePackage);
        OpenPFEM_Free(LinePackageRoot);
        OpenPFEM_Free(LineCount);
        OpenPFEM_Free(LineDisplc);
    }

    PTW *Volu2Ranks = NULL, *Face2Ranks = NULL, *Face4Elems = NULL;
    INT *FaceCount = NULL, *FaceDisplc = NULL;
    char *FacePackage = NULL, *FacePackageRoot = NULL; // 点的包裹
    INT FacePackageSize = 0;
    MPI_Request reqface;
    if (mesh->worlddim >= 2)
    {
        // 对面阶段会用到的全局变量进行定义
        INT FacePackageTotalSize = 0;
        PTW *Face4Ranks = NULL;
        SHAREDINFO4GEO **SharedInfo4Faces = NULL;
        PTW **LocalVert4Faces = NULL, **LocalLine4Faces = NULL;
        if (rank == ROOT_RANK)
        {
            //===============下面来处理面的信息======
            // 得到每个单元上面的编号
            PTWCreate(&Face4Elems);
            // 得到每个单元上面的信息
            GetGEO4Elems(mesh, mesh->worlddim, 2, Face4Elems);
            PTWCreate(&Face2Ranks);
            PTWCreate(&Face4Ranks);
            // 得到与每个面相联系的进程号
            GetGEO2Ranks(mesh, NumRanks, ElemRanks, mesh->worlddim, 2, Face2Ranks);
            // 得到每个进程上面的编号集合
            PTWTranspose(Face2Ranks, Face4Ranks);
            SharedInfo4Faces = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
            GetSharedINFO4GEO(Face2Ranks, Face4Ranks, SharedInfo4Faces); // 这其中会得到在每个进程上面面的局部编号
            // 下面来得到点与面的局部关系
            // 得到每个面上点的编号
            PTW *Vert4Faces = NULL;
            if (mesh->worlddim == 2)
            {
                Vert4Faces = Vert4Elems;
            }
            else
            {
                PTWCreate(&Vert4Faces);
                // 得到每个面上的节点信息
                GetGEO4Elems(mesh, 2, 0, Vert4Faces);
                Vert4Faces->NumCols = Vert4Elems->NumCols;
            }
            LocalVert4Faces = malloc(NumRanks * sizeof(PTW *));
            INT rankind;
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&LocalVert4Faces[rankind]);
            }
            GetLocalGEO4Elems(Vert4Faces, Face4Ranks, Vert2Ranks, LocalVert4Faces);
            // 下面来得到线与面的局部关系
            // 得到每个面上线的编号
            PTW *Line4Faces = NULL;
            if (mesh->worlddim == 2)
            {
                Line4Faces = Line4Elems;
            }
            else
            {
                PTWCreate(&Line4Faces);
                // 得到每个面上线的信息
                GetGEO4Elems(mesh, 2, 1, Line4Faces);
                Line4Faces->NumCols = Line4Elems->NumCols;
            }
            LocalLine4Faces = malloc(NumRanks * sizeof(PTW *));
            for (rankind = 0; rankind < NumRanks; rankind++)
            {
                PTWCreate(&(LocalLine4Faces[rankind]));
            }
            GetLocalGEO4Elems(Line4Faces, Face4Ranks, Line2Ranks, LocalLine4Faces);
            // 计算face包裹的大小
            FacePackageTotalSize = PackageSizeFaceInfo(mesh, Face4Ranks, SharedInfo4Faces, NumRanks,
                                                       &FaceCount, &FaceDisplc);
        }
        if (!flagv)
        {
            MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
            if (flagv)
            {
                // 解包vert包裹填入mesh
                UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
                OpenPFEM_Free(VertPackage);
                OpenPFEM_Free(VertPackageRoot);
                OpenPFEM_Free(VertCount);
                OpenPFEM_Free(VertDisplc);
            }
        }
        if (!flagl)
        {
            MPI_Test(&reqline, &flagl, MPI_STATUS_IGNORE);
            if (flagl)
            {
                // 解包line包裹填入mesh
                UnpackedLine2Mesh(mesh, LinePackage, LinePackageSize);
                OpenPFEM_Free(LinePackage);
                OpenPFEM_Free(LinePackageRoot);
                OpenPFEM_Free(LineCount);
                OpenPFEM_Free(LineDisplc);
            }
        }

        MPI_Request reqfacesize;
        MPI_Iscatter(FaceCount, 1, MPI_INT, &FacePackageSize, 1, MPI_INT, ROOT_RANK, comm, &reqfacesize);
        if (rank == ROOT_RANK)
        {
            // 计算face包裹
            PackageFaceInfo(mesh, Face4Ranks, SharedInfo4Faces, LocalVert4Faces, LocalLine4Faces,
                            size, &FacePackageRoot, FacePackageTotalSize);
            INT idx;
            for (idx = 0; idx < NumRanks; idx++)
            {
                SharedInfo4GEODestroy(SharedInfo4Faces + idx);
                PTWDestroy(LocalVert4Faces + idx);
                PTWDestroy(LocalLine4Faces + idx);
            }
        }
        OpenPFEM_Free(Face4Ranks);
        OpenPFEM_Free(SharedInfo4Faces);
        OpenPFEM_Free(LocalVert4Faces);
        OpenPFEM_Free(LocalLine4Faces);
        MPI_Wait(&reqfacesize, MPI_STATUS_IGNORE);
#if PRINT_INFO
        OpenPFEM_RankPrint(comm, "将接收到ROOT进程%d大小的FACEpackage\n", FacePackageSize);
#endif
        FacePackage = (char *)malloc(FacePackageSize * sizeof(char));
        MPI_Iscatterv(FacePackageRoot, FaceCount, FaceDisplc, MPI_PACKED,
                      FacePackage, FacePackageSize, MPI_PACKED, ROOT_RANK, comm, &reqface);

        if (!flagv)
        {
            MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
            if (flagv)
            {
                // 解包vert包裹填入mesh
                UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
                OpenPFEM_Free(VertPackage);
                OpenPFEM_Free(VertPackageRoot);
                OpenPFEM_Free(VertCount);
                OpenPFEM_Free(VertDisplc);
            }
        }
        if (!flagl)
        {
            MPI_Test(&reqline, &flagl, MPI_STATUS_IGNORE);
            if (flagl)
            {
                // 解包line包裹填入mesh
                UnpackedLine2Mesh(mesh, LinePackage, LinePackageSize);
                OpenPFEM_Free(LinePackage);
                OpenPFEM_Free(LinePackageRoot);
                OpenPFEM_Free(LineCount);
                OpenPFEM_Free(LineDisplc);
            }
        }

        MPI_Test(&reqface, &flagf, MPI_STATUS_IGNORE);
        if (flagf)
        {
            // 解包face包裹填入mesh
            UnpackedFace2Mesh(mesh, FacePackage, FacePackageSize);
            OpenPFEM_Free(FacePackage);
            OpenPFEM_Free(FacePackageRoot);
            OpenPFEM_Free(FaceCount);
            OpenPFEM_Free(FaceDisplc);
        }

        if (mesh->worlddim == 3)
        {
            // 对体阶段会用到的全局变量进行定义
            char *VoluPackage = NULL, *VoluPackageRoot = NULL; // 点的包裹
            INT *VoluCount = NULL, *VoluDisplc = NULL;
            INT VoluPackageSize = 0, VoluPackageTotalSize = 0;
            PTW *Volu4Ranks = NULL;
            PTW **LocalVert4Volus = NULL, **LocalLine4Volus = NULL, **LocalFace4Volus = NULL;
            if (rank == ROOT_RANK)
            {
                // 得到每个单元上体的编号
                PTW *Volu4Elems = NULL;
                PTWCreate(&Volu4Elems);
                // 得到每个单元上体的信息
                GetGEO4Elems(mesh, mesh->worlddim, 3, Volu4Elems);
                PTWCreate(&Volu2Ranks);
                PTWCreate(&Volu4Ranks);
                // 得到与每个体相联系的进程号
                GetGEO2Ranks(mesh, NumRanks, ElemRanks, mesh->worlddim, 3, Volu2Ranks);
                // 得到每个进程上体的编号集合
                PTWTranspose(Volu2Ranks, Volu4Ranks);
                SHAREDINFO4GEO **SharedInfo4Volus;
                SharedInfo4Volus = malloc(NumRanks * sizeof(SHAREDINFO4GEO *));
                GetSharedINFO4GEO(Volu2Ranks, Volu4Ranks, SharedInfo4Volus);
                // 下面来得到点与体的局部关系
                // 得到每个体上点的编号
                PTW *Vert4Volus = NULL;
                if (mesh->worlddim == 3)
                {
                    Vert4Volus = Vert4Elems;
                }
                else
                {
                    PTWCreate(&Vert4Volus);
                    // 得到每个体上的节点信息
                    GetGEO4Elems(mesh, 3, 0, Vert4Volus);
                    Vert4Volus->NumCols = Vert4Elems->NumCols;
                }
                LocalVert4Volus = malloc(NumRanks * sizeof(PTW *));
                INT rankind;
                for (rankind = 0; rankind < NumRanks; rankind++)
                {
                    PTWCreate(&LocalVert4Volus[rankind]);
                }
                GetLocalGEO4Elems(Vert4Volus, Volu4Ranks, Vert2Ranks, LocalVert4Volus);
                // 下面来得到线与体的局部关系
                // 得到每个体上线的编号
                PTW *Line4Volus = NULL;
                if (mesh->worlddim == 3)
                {
                    Line4Volus = Line4Elems;
                }
                else
                {
                    PTWCreate(&Line4Volus);
                    // 得到每个体上的线的信息
                    GetGEO4Elems(mesh, 3, 1, Line4Volus);
                    Line4Volus->NumCols = Line4Elems->NumCols;
                }
                LocalLine4Volus = malloc(NumRanks * sizeof(PTW *));
                for (rankind = 0; rankind < NumRanks; rankind++)
                {
                    PTWCreate(&LocalLine4Volus[rankind]);
                }
                GetLocalGEO4Elems(Line4Volus, Volu4Ranks, Line2Ranks, LocalLine4Volus);
                // 下面来得到面与体的局部关系
                // 得到每个体上面的编号
                PTW *Face4Volus = NULL;
                if (mesh->worlddim == 3)
                {
                    Face4Volus = Face4Elems;
                }
                else
                {
                    PTWCreate(&Face4Volus);
                    // 得到每个体上的线的信息
                    GetGEO4Elems(mesh, 3, 2, Face4Volus);
                    Face4Volus->NumCols = Face4Elems->NumCols;
                }
                LocalFace4Volus = malloc(NumRanks * sizeof(PTW *));
                for (rankind = 0; rankind < NumRanks; rankind++)
                {
                    PTWCreate(&LocalFace4Volus[rankind]);
                }
                GetLocalGEO4Elems(Face4Volus, Volu4Ranks, Face2Ranks, LocalFace4Volus);
                // 计算volu包裹的大小
                VoluPackageTotalSize = PackageSizeVoluInfo(mesh, Volu4Ranks, NumRanks, &VoluCount, &VoluDisplc);
            }

            if (!flagv)
            {
                MPI_Test(&reqvert, &flagv, MPI_STATUS_IGNORE);
                if (flagv)
                {
                    // 解包vert包裹填入mesh
                    UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
                    OpenPFEM_Free(VertPackage);
                    OpenPFEM_Free(VertPackageRoot);
                    OpenPFEM_Free(VertCount);
                    OpenPFEM_Free(VertDisplc);
                }
            }
            if (!flagl)
            {
                MPI_Test(&reqline, &flagl, MPI_STATUS_IGNORE);
                if (flagl)
                {
                    // 解包line包裹填入mesh
                    UnpackedLine2Mesh(mesh, LinePackage, LinePackageSize);
                    OpenPFEM_Free(LinePackage);
                    OpenPFEM_Free(LinePackageRoot);
                    OpenPFEM_Free(LineCount);
                    OpenPFEM_Free(LineDisplc);
                }
            }
            if (!flagf)
            {
                MPI_Test(&reqface, &flagf, MPI_STATUS_IGNORE);
                if (flagf)
                {
                    // 解包face包裹填入mesh
                    UnpackedFace2Mesh(mesh, FacePackage, FacePackageSize);
                    OpenPFEM_Free(FacePackage);
                    OpenPFEM_Free(FacePackageRoot);
                    OpenPFEM_Free(FaceCount);
                    OpenPFEM_Free(FaceDisplc);
                }
            }

            MPI_Request reqvolusize;
            MPI_Iscatter(VoluCount, 1, MPI_INT, &VoluPackageSize, 1, MPI_INT, ROOT_RANK, comm, &reqvolusize);
            if (rank == ROOT_RANK)
            {
                // 计算volu包裹
                PackageVoluInfo(mesh, Volu4Ranks, LocalVert4Volus, LocalLine4Volus, LocalFace4Volus,
                                size, &VoluPackageRoot, VoluPackageTotalSize);
                INT idx;
                for (idx = 0; idx < NumRanks; idx++)
                {
                    PTWDestroy(LocalVert4Volus + idx);
                    PTWDestroy(LocalLine4Volus + idx);
                    PTWDestroy(LocalFace4Volus + idx);
                }
            }
            PTWDestroy(&Volu4Ranks);
            OpenPFEM_Free(LocalVert4Volus);
            MPI_Wait(&reqvolusize, MPI_STATUS_IGNORE);
#if PRINT_INFO
            OpenPFEM_RankPrint(comm, "将接收到ROOT进程%d大小的VOLUpackage\n", VoluPackageSize);
#endif
            VoluPackage = (char *)malloc(VoluPackageSize * sizeof(char));
            MPI_Request reqvolu;
            INT flagu = 0;
            MPI_Iscatterv(VoluPackageRoot, VoluCount, VoluDisplc, MPI_PACKED,
                          VoluPackage, VoluPackageSize, MPI_PACKED, ROOT_RANK, comm, &reqvolu);

            MPI_Wait(&reqvolu, MPI_STATUS_IGNORE);
            // 解包face包裹填入mesh
            UnpackedVolu2Mesh(mesh, VoluPackage, VoluPackageSize);
            OpenPFEM_Free(VoluPackage);
            OpenPFEM_Free(VoluPackageRoot);
            OpenPFEM_Free(VoluCount);
            OpenPFEM_Free(VoluDisplc);
            OpenPFEM_Free(Line2Ranks);
            MPI_Request_free(&reqvolu);
        }
    }
    if (!flagv)
    {
        MPI_Wait(&reqvert, MPI_STATUS_IGNORE);
        // 解包vert包裹填入mesh
        UnpackedVert2Mesh(mesh, VertPackage, VertPackageSize);
        OpenPFEM_Free(VertPackage);
        OpenPFEM_Free(VertPackageRoot);
        OpenPFEM_Free(VertCount);
        OpenPFEM_Free(VertDisplc);
    }
    MPI_Request_free(&reqvert);

    if (!flagl)
    {
        MPI_Wait(&reqline, MPI_STATUS_IGNORE);
        // 解包line包裹填入mesh
        UnpackedLine2Mesh(mesh, LinePackage, LinePackageSize);
        OpenPFEM_Free(LinePackage);
        OpenPFEM_Free(LinePackageRoot);
        OpenPFEM_Free(LineCount);
        OpenPFEM_Free(LineDisplc);
    }
    MPI_Request_free(&reqline);

    if (!flagf)
    {
        MPI_Wait(&reqface, MPI_STATUS_IGNORE);
        // 解包face包裹填入mesh
        UnpackedFace2Mesh(mesh, FacePackage, FacePackageSize);
        OpenPFEM_Free(FacePackage);
        OpenPFEM_Free(FacePackageRoot);
        OpenPFEM_Free(FaceCount);
        OpenPFEM_Free(FaceDisplc);
    }
    MPI_Request_free(&reqface);

    // free
    OpenPFEM_Free(ElemRanks);
    PTWDestroy(&Vert2Ranks);
    PTWDestroy(&Vert4Elems);
    PTWDestroy(&Line4Elems);
    if (mesh->worlddim >= 2)
    {
        PTWDestroy(&Face2Ranks);
        PTWDestroy(&Face4Elems);
    }
    if (mesh->worlddim == 3)
    {
        PTWDestroy(&Volu2Ranks);
    }
    // 进行一个shared信息顺序一致性的处理：按照进程数较小的那方的顺序排列
} // 至此完成了对网格的划分

// 得到没给进程上单元的几何
void GetElem4Ranks(INT *ElemRanks, INT NumElems, PTW *Elem4Ranks)
{
    INT NumRanks = size;
    INT *Ptw = Elem4Ranks->Ptw;
    INT *Entries = Elem4Ranks->Entries;
    Ptw = malloc((NumRanks + 1) * sizeof(INT));
    memset(Ptw, 0, (NumRanks + 1) * sizeof(INT));
    INT k, pos, rankind;
    for (k = 0; k < NumElems; k++)
    {
        rankind = ElemRanks[k]; // 获得当前单元所属的进程号
        Ptw[rankind + 1]++;     // 相应进程上增加一个单元
    }
    Ptw[0] = 0;
    for (k = 0; k < NumRanks; k++)
    {
        Ptw[k + 1] += Ptw[k];
    }
    Entries = malloc(Ptw[NumRanks] * sizeof(INT));
    INT *RankPos = malloc(NumRanks * sizeof(INT));
    memset(RankPos, 0, NumRanks * sizeof(INT));
    for (k = 0; k < NumElems; k++)
    {
        rankind = ElemRanks[k]; // 获得当前单元所属的进程号
        pos = Ptw[rankind] + RankPos[rankind];
        Entries[pos] = k; // 在相应的地方记录下单元的编号
        RankPos[rankind]++;
    }
    OpenPFEM_Free(RankPos);
#if PRINT_INFO
    INT start, end, j;
    for (k = 0; k < NumRanks; k++)
    {
        start = Ptw[k];
        end = Ptw[k + 1];
        OpenPFEM_Print("第%d-个进程的单元: \n", k);
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,   ", Entries[j]);
        }
        OpenPFEM_Print("\n");
    }
#endif
} // end of this program

void MetisPartition(MESH *mesh, PTW *Vert4Elems, INT NumElems, INT *ElemRanks)
{

    idx_t i;
    idx_t elem_num = (idx_t)NumElems;
    idx_t vert_num = (idx_t)(Vert4Elems->NumCols);
    idx_t totalnum = (idx_t)(Vert4Elems->Ptw[NumElems]);
    idx_t *eptr = (idx_t *)malloc((elem_num + 1) * sizeof(idx_t));
    idx_t *eind = (idx_t *)malloc(totalnum * sizeof(idx_t));
    INT *PtwV4E = Vert4Elems->Ptw;
    INT *V4E = Vert4Elems->Entries;
    for (i = 0; i <= elem_num; i++)
    {
        eptr[i] = (idx_t)(PtwV4E[i]);
    }
    for (i = 0; i < totalnum; i++)
    {
        eind[i] = (idx_t)(V4E[i]);
    }
    idx_t *vwgt = NULL, *vsize = NULL, *options = NULL;
    idx_t ncommon = mesh->worlddim;
    MPI_Comm_size(mesh->comm, &size);
    idx_t part_num = (idx_t)size;
    real_t *tpwgts = NULL;
    idx_t *objval = (idx_t *)malloc(sizeof(idx_t));
    idx_t *epart = (idx_t *)malloc(elem_num * sizeof(idx_t));
    idx_t *npart = (idx_t *)malloc(vert_num * sizeof(idx_t));
    METIS_PartMeshDual(&elem_num, &vert_num, eptr, eind, vwgt, vsize, &ncommon,
                       &part_num, tpwgts, options, objval, epart, npart);

    for (i = 0; i < NumElems; i++)
    {
        ElemRanks[i] = (int)(epart[i]);
#if PRINT_INFO
        OpenPFEM_Print("第%d个单元被分给了%d进程\n", i, ElemRanks[i]);
#endif
        // OpenPFEM_Print("第%d个单元被分给了%d进程\n", i, ElemRanks[i]);
    }
    OpenPFEM_Free(eptr);
    OpenPFEM_Free(eind);
    OpenPFEM_Free(objval);
    OpenPFEM_Free(epart);
    OpenPFEM_Free(npart);
}

void Mesh2DPrint(MESH *mesh, INT printrank)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == printrank)
    {
        INT i, j;
        printf("\n================================================\n");
        printf("[RANK %d] 网格有%d个点 %d条线 %d个面: \n", rank, mesh->num_vert, mesh->num_line, mesh->num_face);
        printf("================================================\n");

        VERT *Verts = mesh->Verts;
        VERT *vert;
        printf("(1)点: \n");
        for (i = 0; i < mesh->num_vert; i++)
        {
            vert = &(Verts[i]);
            printf("点%d: 坐标(%g,%g) 边界信息%d 编号%d\n",
                   i, vert->Coord[0], vert->Coord[1], vert->BD_ID, vert->Index);
        }
        printf("-------------------------------------------------\n");

        LINE *Lines = mesh->Lines;
        LINE *line;
        printf("(2)线: \n");
        for (i = 0; i < mesh->num_line; i++)
        {
            line = &(Lines[i]);
            printf("线%d: 顶点(%d,%d) 边界信息%d 编号%d\n",
                   i, line->Vert4Line[0], line->Vert4Line[1], line->BD_ID, line->Index);
        }
        printf("-------------------------------------------------\n");

        FACE *Faces = mesh->Faces;
        FACE *face;
        printf("(3)面: \n");
        for (i = 0; i < mesh->num_face; i++)
        {
            face = &(Faces[i]);
            printf("面%d: 编号%d 顶点", i, face->Index);
            for (j = 0; j < face->NumVerts; j++)
            {
                printf("-%d", face->Vert4Face[j]);
            }
            printf(" 边");
            for (j = 0; j < face->NumLines; j++)
            {
                printf("-%d", face->Line4Face[j]);
            }
            printf("\n");
        }
        printf("================================================\n\n");
    }
    if (mesh->SharedInfo != NULL)
    {
        SharedMeshPrint(mesh, printrank);
    }
    MPI_Barrier(mesh->comm);
    return;
}

void Mesh3DPrint(MESH *mesh, INT printrank)
{
    MPI_Comm_rank(mesh->comm, &rank);
    if (rank == printrank)
    {
        INT i, j;
        printf("\n================================================\n");
        printf("[RANK %d] 网格有%d个点 %d条线 %d个面 %d个体: \n",
               rank, mesh->num_vert, mesh->num_line, mesh->num_face, mesh->num_volu);
        printf("================================================\n");

        VERT *Verts = mesh->Verts;
        VERT *vert;
        printf("(1)点: \n");
        for (i = 0; i < mesh->num_vert; i++)
        {
            vert = &(Verts[i]);
            printf("点%d: 坐标(%g,%g,%g) 边界信息%d 编号%d\n",
                   i, vert->Coord[0], vert->Coord[1], vert->Coord[2], vert->BD_ID, vert->Index);
        }
        printf("-------------------------------------------------\n");

        LINE *Lines = mesh->Lines;
        LINE *line;
        printf("(2)线: \n");
        for (i = 0; i < mesh->num_line; i++)
        {
            line = &(Lines[i]);
            printf("线%d: 顶点(%d,%d) 边界信息%d 编号%d\n",
                   i, line->Vert4Line[0], line->Vert4Line[1], line->BD_ID, line->Index);
        }
        printf("-------------------------------------------------\n");

        FACE *Faces = mesh->Faces;
        FACE *face;
        printf("(3)面: \n");
        for (i = 0; i < mesh->num_face; i++)
        {
            face = &(Faces[i]);
            printf("面%d: 编号%d 顶点", i, face->Index);
            for (j = 0; j < face->NumVerts; j++)
            {
                printf("-%d", face->Vert4Face[j]);
            }
            printf(" 边");
            for (j = 0; j < face->NumLines; j++)
            {
                printf("-%d", face->Line4Face[j]);
            }
            printf("\n");
        }
        printf("-------------------------------------------------\n");

        VOLU *Volus = mesh->Volus;
        VOLU *volu;
        printf("(4)体: \n");
        for (i = 0; i < mesh->num_volu; i++)
        {
            volu = &(Volus[i]);
            printf("体%d: 编号%d 顶点", i, volu->Index);
            for (j = 0; j < volu->NumVerts; j++)
            {
                printf("-%d", volu->Vert4Volu[j]);
            }
            printf(" 边");
            for (j = 0; j < volu->NumLines; j++)
            {
                printf("-%d", volu->Line4Volu[j]);
            }
            printf("\n");
            for (j = 0; j < volu->NumFaces; j++)
            {
                printf("-%d", volu->Face4Volu[j]);
            }
            printf("\n");
        }
        printf("================================================\n\n");
    }
    if (mesh->SharedInfo != NULL)
    {
        SharedMeshPrint(mesh, printrank);
    }
    MPI_Barrier(mesh->comm);
    return;
}

void MeshPrint(MESH *mesh)
{
    INT i, j;
    printf("\n================================================\n");
    printf("网格有%d个点 %d条线 %d个面 %d个体: \n", mesh->num_vert, mesh->num_line, mesh->num_face, mesh->num_volu);
    printf("================================================\n");
    VERT *Verts = mesh->Verts;
    VERT *vert;
    printf("(1)点: \n");
    for (i = 0; i < mesh->num_vert; i++)
    {
        vert = &(Verts[i]);
        switch (mesh->worlddim)
        {
        case 1:
            printf("点%d: 坐标: %g 边界信息%d 编号%d\n", i, vert->Coord[0], vert->BD_ID, vert->Index);
            break;
        case 2:
            printf("点%d: 坐标(%g, %g) 边界信息%d 编号%d\n", i, vert->Coord[0], vert->Coord[1], vert->BD_ID, vert->Index);
            break;
        case 3:
            printf("点%d: 坐标(%g, %g, %g) 边界信息%d 编号%d\n", i, vert->Coord[0], vert->Coord[1], vert->Coord[2],
                   vert->BD_ID, vert->Index);
            break;
        }
    }
    if (mesh->worlddim >= 1)
    {
        printf("-------------------------------------------------\n");
        LINE *Lines = mesh->Lines;
        LINE *line;
        printf("(2)线: \n");
        for (i = 0; i < mesh->num_line; i++)
        {
            line = &(Lines[i]);
            printf("线%d: 顶点(%d,%d) 边界信息%d 编号%d\n", i, line->Vert4Line[0], line->Vert4Line[1], line->BD_ID, line->Index);
        }
    }
    if (mesh->worlddim >= 2)
    {
        printf("-------------------------------------------------\n");
        FACE *Faces = mesh->Faces;
        FACE *face;
        printf("(3)FACE: \n");
        for (i = 0; i < mesh->num_face; i++)
        {
            face = &(Faces[i]);
            fflush(stdout);
            printf("FACE%d: INDEX %d VERTS(%d)", i, face->Index, face->NumVerts);
            // for (j = 0; j < 3; j++)
            {
                //         printf("-%d", 0);
            }
            //     printf(" 边(%d)", face->NumLines);
            //     for (j = 0; j < face->NumLines; j++)
            //     {
            //         printf("-%d", 0);
            //     }
            //     printf(" 边界: %d", face->BD_ID);
            printf("\n");
        }
    }

    if (mesh->worlddim >= 3)
    {
        printf("-------------------------------------------------\n");
        return;
        VOLU *Volus = mesh->Volus;
        VOLU *volu;
        printf("(4)体: \n");
        for (i = 0; i < 1; i++)
        {
            volu = &(Volus[i]);
            printf("体%d: 编号%d 顶点", i, volu->Index);
            for (j = 0; j < volu->NumVerts; j++)
            {
                printf("-%d", volu->Vert4Volu[j]);
            }
            printf(" 边");
            for (j = 0; j < volu->NumLines; j++)
            {
                printf("-%d", volu->Line4Volu[j]);
            }
            printf(" 面");
            for (j = 0; j < volu->NumFaces; j++)
            {
                printf("-%d", volu->Face4Volu[j]);
            }
            printf("\n");
        }
    }
    printf("================================================\n\n");
}

// 对PTW的结构体进行转置操作
void PTWTranspose(PTW *GEO4Elems, PTW *GEO2Elems)
{
    INT j, elemind, geoind, NumRows = GEO4Elems->NumCols, NumCols = GEO4Elems->NumRows;
    GEO2Elems->NumRows = NumRows;
    GEO2Elems->NumCols = NumCols;
    INT *Ptw4 = GEO4Elems->Ptw;
    INT *Entries4 = GEO4Elems->Entries;
    GEO2Elems->Ptw = malloc((NumRows + 1) * sizeof(INT));
    memset(GEO2Elems->Ptw, 0, (NumRows + 1) * sizeof(INT));
    INT *Ptw2 = GEO2Elems->Ptw;
    INT start, end;
    // printf("NumRows: %d, NumCols: %d\n",NumRows,NumCols);
    for (elemind = 0; elemind < NumCols; elemind++)
    {
        start = Ptw4[elemind];
        end = Ptw4[elemind + 1];
        for (j = start; j < end; j++)
        {
            Ptw2[Entries4[j] + 1]++;
            // printf("XXX Ptw2[%d]=%d\n", Entries4[j] + 1, Ptw2[Entries4[j] + 1]);
        } // end for j
    }     // end for elemind
    Ptw2[0] = 0;
    for (geoind = 0; geoind < NumRows; geoind++)
    {
        Ptw2[geoind + 1] += Ptw2[geoind];
    }
    // 最后再来统计每个进程所包含的节点编号
    GEO2Elems->Entries = malloc(Ptw2[NumRows] * sizeof(INT));
    INT *Entries2 = GEO2Elems->Entries;
    INT *GeoPos = malloc(NumRows * sizeof(INT));
    memset(GeoPos, 0, NumRows * sizeof(INT));
    // 进行赋值
    for (elemind = 0; elemind < NumCols; elemind++)
    {
        start = Ptw4[elemind];
        end = Ptw4[elemind + 1];
        for (j = start; j < end; j++)
        {
            // Elem2Ranks[j]: 目前节点所属的进程号，PtwVertRanks: 目前进程所在的起始位置
            geoind = Entries4[j];
            Entries2[Ptw2[geoind] + GeoPos[geoind]] = elemind;
            GeoPos[geoind]++;
        }
    }
    OpenPFEM_Free(GeoPos);
}

// 得到每个单元上的几何元素
//  ElemDim表示单元的维数,1表示是一维网格，2表示是二维网格，3表示是三维网格
//  GeoDim表示GEO元素的维数,0表示点，1表示线，2表示面，3表示体
void GetGEO4Elems(MESH *mesh, INT ElemDim, INT GeoDim, PTW *GEO4Elems)
{
    // 首先得到GEO4Elems
    INT k, j, pos, *Ptw = GEO4Elems->Ptw, *Entries = GEO4Elems->Entries;
    INT NumFaces, NumElems, NumVerts, NumLines;
    switch (GeoDim)
    {
    case 0:
        GEO4Elems->NumCols = mesh->num_vert;
        break;
    case 1:
        GEO4Elems->NumCols = mesh->num_line;
        break;
    case 2:
        GEO4Elems->NumCols = mesh->num_face;
        break;
    case 3:
        GEO4Elems->NumCols = mesh->num_volu;
        break;
    }
    LINE line, *Lines = mesh->Lines;
    FACE face, *Faces = mesh->Faces;
    VOLU volu, *Volus = mesh->Volus;
    switch (ElemDim)
    {
    case 1: // 表示是一维的网格，那么geodim必须为0
        GEO4Elems->NumRows = mesh->num_line;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Entries = malloc(2 * NumElems * sizeof(INT));
        // 对线进行循环得到相应的位置,并且GeoDim必须等于0
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            for (k = 0; k < NumElems; k++)
            {
                line = Lines[k];
                Ptw[k + 1] = 2 * (k + 1);
                Entries[2 * k] = line.Vert4Line[0];
                Entries[2 * k + 1] = line.Vert4Line[1];
            }
            break;
        case 1:
            Entries = malloc(NumElems * sizeof(INT));
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {
                Ptw[k + 1] = k + 1;
                Entries[k] = k;
            } // end for k
            break;
        } // end switchx
        break;
    case 2: // 二维网格的情况
        GEO4Elems->NumRows = mesh->num_face;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            GEO4Elems->NumCols = mesh->num_vert;
            // printf("GEO4Elems->NumCols=%d\n", GEO4Elems->NumCols);
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                Ptw[k + 1] = Ptw[k] + face.NumVerts;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                NumVerts = face.NumVerts;
                for (j = 0; j < NumVerts; j++)
                {
                    Entries[pos] = face.Vert4Face[j];
                    pos++;
                }
            }
            break;
        case 1:
            GEO4Elems->NumCols = mesh->num_line;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                Ptw[k + 1] = Ptw[k] + face.NumLines;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                face = Faces[k];
                NumLines = face.NumLines;
                for (j = 0; j < NumLines; j++)
                {
                    Entries[pos] = face.Line4Face[j];
                    pos++;
                }
            }
            break;
        case 2:
            Entries = malloc(NumElems * sizeof(INT));
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {
                Ptw[k + 1] = k + 1;
                Entries[k] = k;
            } // end for k
            break;
        } // end switch
        break;
    case 3:
        GEO4Elems->NumRows = mesh->num_volu;
        NumElems = GEO4Elems->NumRows;
        Ptw = malloc((NumElems + 1) * sizeof(INT));
        Ptw[0] = 0;
        switch (GeoDim)
        {
        case 0:
            GEO4Elems->NumCols = mesh->num_vert;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumVerts;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumVerts = volu.NumVerts;
                for (j = 0; j < NumVerts; j++)
                {
                    Entries[pos] = volu.Vert4Volu[j];
                    pos++;
                }
            }
            break;
        case 1:
            GEO4Elems->NumCols = mesh->num_line;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumLines;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumLines = volu.NumLines;
                for (j = 0; j < NumLines; j++)
                {
                    Entries[pos] = volu.Line4Volu[j];
                    pos++;
                }
            }
            break;
        case 2:
            GEO4Elems->NumCols = mesh->num_face;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                Ptw[k + 1] = Ptw[k] + volu.NumFaces;
            }
            Entries = malloc(Ptw[NumElems] * sizeof(INT));
            pos = 0;
            for (k = 0; k < NumElems; k++)
            {
                volu = Volus[k];
                NumFaces = volu.NumFaces;
                for (j = 0; j < NumFaces; j++)
                {
                    Entries[pos] = volu.Face4Volu[j];
                    pos++;
                }
            }
            break;
        case 3:
            Entries = malloc(NumElems * sizeof(INT));
            Ptw[0] = 0;
            for (k = 0; k < NumElems; k++)
            {
                Ptw[k + 1] = k + 1;
                Entries[k] = k;
            } // end for k
            break;
        } // end switch for GEODIM
        break;
    }
    GEO4Elems->Ptw = Ptw;
    GEO4Elems->Entries = Entries;
}

// 得到与每个几何元素相关联的单元集合
//  ElemDim表示单元的维数,1表示是一维网格，2表示是二维网格，3表示是三维网格
//  GeoDim表示GEO元素的维数,0表示点，1表示线，2表示面，3表示体
void GetGEO2Elems(MESH *mesh, INT ElemDim, INT GeoDim, PTW *GEO2Elems)
{
    PTW *GEO4Elems = NULL;
    PTWCreate(&(GEO4Elems));
    GetGEO4Elems(mesh, ElemDim, GeoDim, GEO4Elems);
    PTWTranspose(GEO4Elems, GEO2Elems);
    PTWDestroy(&GEO4Elems);
}

// 得到与几何元素相联系的进程信息
//  GeoDim: 表示几何元素的维数
void GetGEO2Ranks(MESH *mesh, INT NumRanks, INT *ElemRanks, INT ElemDim, INT GeoDim, PTW *GEO2Ranks)
{
    INT i, j, k;
    INT start0, end0, start, end, length, flag, rankind, elemind, pos;
    PTW *GEO4Elems;
    PTWCreate(&GEO4Elems);
    GetGEO4Elems(mesh, ElemDim, GeoDim, GEO4Elems);
    PTW *GEO2Elems;
    PTWCreate(&GEO2Elems);
    // 得到与几何元素相关的单元信息
    PTWTranspose(GEO4Elems, GEO2Elems);
    INT *Ptw0 = GEO2Elems->Ptw, *Entries0 = GEO2Elems->Entries;
    INT geoind, numgeo = GEO2Elems->NumRows;
    INT *CoarseGEO2Ranks = malloc(Ptw0[numgeo] * sizeof(INT));
    memset(CoarseGEO2Ranks, -1, Ptw0[numgeo] * sizeof(INT));
    // 线用Ptw表示CoarseGEO2Ranks中每行的当前位置
    GEO2Ranks->NumRows = numgeo;
    GEO2Ranks->NumCols = NumRanks;
    GEO2Ranks->Ptw = malloc((numgeo + 1) * sizeof(INT));
    memset(GEO2Ranks->Ptw, 0, (numgeo + 1) * sizeof(INT));
    INT *Ptw = GEO2Ranks->Ptw;
    for (geoind = 0; geoind < numgeo; geoind++)
    {
        start0 = Ptw0[geoind];
        end0 = Ptw0[geoind + 1];
        pos = Ptw[geoind + 1]; // 目前进程号的登记位置
        for (j = start0; j < end0; j++)
        {
            elemind = Entries0[j]; // 得到目前位置的单元编号
            // printf("elemind: %d\n", elemind);
            rankind = ElemRanks[elemind]; // 取出当前单元所在的进程号
            // printf("rankind: %d\n", rankind);
            flag = 0;
            for (i = 0; i < pos; i++)
            { // 检查该进程号是否已经被记录
                if (CoarseGEO2Ranks[start0 + i] == rankind)
                {
                    flag = 1;
                    i = pos + 1;
                } // end if
            }     // end for i
            // 如果flag==0即表示该进程号没有被记录
            if (flag == 0)
            {
                CoarseGEO2Ranks[start0 + pos] = rankind; // 记录该进程号
                pos++;
            } // end if flag==0
        }     // end for j
        Ptw[geoind + 1] = pos;
    } // end for geoind
    // 统计总共需要多长的数组来存储这些节点进程号的信息
    Ptw[0] = 0;
    for (k = 0; k < numgeo; k++)
    {
        Ptw[k + 1] += Ptw[k];
        // printf("Ptw[%d]=%d\n",k+1, Ptw[k+1]);
    }
    GEO2Ranks->Entries = malloc(Ptw[numgeo] * sizeof(INT));
    INT *Entries = GEO2Ranks->Entries;
    // 然后再进行单元循环，把相应的对应关系完备
    for (geoind = 0; geoind < numgeo; geoind++)
    {
        // 找到在CoarseVert2Ranks中的起始位置
        start0 = Ptw0[geoind];
        // 找到在GEO2Ranks中的起始位置
        start = Ptw[geoind]; // 这是起始位置
        length = Ptw[geoind + 1] - start;
        // 处理该几何元素上的所有进程信息
        for (i = 0; i < length; i++)
        {
            Entries[start + i] = CoarseGEO2Ranks[start0 + i];
        } // end for i
    }     // end for geoind
    // 释放内存空间
    OpenPFEM_Free(CoarseGEO2Ranks);
}

void GetGEO4Ranks(MESH *mesh, INT NumRanks, INT *ElemRanks, INT ElemDim, INT GeoDim, PTW GEO4Ranks)
{
}

// 根据GEO2Ranks和GEO4Ranks的信息产生几何元素的共享信息
// 输出信息：
// 与GEO2Ranks中的AndEntries存储的是LocalGEOIndex2Ranks:记录几何元素在相应进程上的局部编号
//  LocalGEOIndex2Ranks已经存储在GEO2Ranks的AndEntries数组中
//  2022-03-31: 加上Owners的信息
void GetSharedINFO4GEO(PTW *GEO2Ranks, PTW *GEO4Ranks, SHAREDINFO4GEO **SharedINFO4GEO)
{
    INT NumRanks = GEO4Ranks->NumRows; // 获得进程数
    INT rankind, geoind, i, j, k, start2, end2, start4, length, startrank;
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        SharedINFO4GEO[rankind] = malloc(sizeof(SHAREDINFO4GEO));
    }
    INT NumGeos = GEO2Ranks->NumRows; // 获得几何元素的个数
    INT *Ptw2 = GEO2Ranks->Ptw, *Ptw4 = GEO4Ranks->Ptw;
    INT *Entries2 = GEO2Ranks->Entries, *Entries4 = GEO4Ranks->Entries;
    // 记录每个全局节点在每个进程上的局部编号,与GEO2Ranks结构相同
    GEO2Ranks->AndEntries = malloc(Ptw2[NumGeos] * sizeof(INT));
    INT *LocalGEOIndex2Ranks = GEO2Ranks->AndEntries;
    for (rankind = 0; rankind < NumRanks; rankind++)
    { // 处理第rankind的进程
        start4 = Ptw4[rankind];
        length = Ptw4[rankind + 1] - start4; // 该进程几何元素的个数
        for (j = 0; j < length; j++)
        {                                  // 处理该进程上的第j个几何元素
            geoind = Entries4[start4 + j]; // 得到第j个几何元素的全局编号
            // 找到geoind在Entries2中的信息，将其相应的位置设置为该进程中的局部编号j
            start2 = Ptw2[geoind];
            end2 = Ptw2[geoind + 1];
            for (i = start2; i < end2; i++)
            { // 在相应的几何元素行找到相应的位置
                if (Entries2[i] == rankind)
                {
                    LocalGEOIndex2Ranks[i] = j; // 将相应的位置设置为GEO相应的局部编号
                    i = end2 + 1;               // 如此就跳出循环
                }                               // end if
            }                                   // end for i
        }                                       // end for j
        SharedINFO4GEO[rankind]->SharedNum = 0;
    } // end for rankind 如下就得到了每个GEO元素所在的进程信息和相应的局部编号信息
    // 接下来是为了产生几何元素的共享信息，可以从Ptw2中发现当GEO元素分布在
    // 多于一个进程的情况下就是一个共享几何元素，并且相应的进程号和相应的局部编号都可以找到
    // 首先统计每个进程需为共享元素申请的内存空间
    for (geoind = 0; geoind < NumGeos; geoind++)
    { // 查看每个几何元素是否是共享元素
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        if (end2 - start2 > 1)
        { // 表示该几何元素是共享的，需往相应的进程上加上相应的长度
            for (j = start2; j < end2; j++)
            {                                         // 处理该几何元素在每个进程上的信息
                rankind = Entries2[j];                // 得到几何元素所在的进程号
                SharedINFO4GEO[rankind]->SharedNum++; // 相应进程的共享元素加1
            }                                         // end for j
        }                                             // end for if
    }                                                 // end for geoind, 由此得到了每个进程上所包含的共享几何元素的个数
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        SharedINFO4GEO[rankind]->Ptw =
            malloc((SharedINFO4GEO[rankind]->SharedNum + 1) * sizeof(INT));
        SharedINFO4GEO[rankind]->Owners =
            malloc(SharedINFO4GEO[rankind]->SharedNum * sizeof(INT));
    }                                              // end for rankind
    INT *RankPos = malloc(NumRanks * sizeof(INT)); // 记录每个进程上共享元素的位置
    memset(RankPos, 0, NumRanks * sizeof(INT));    // 表示每个进程上没有共享元素
    for (geoind = 0; geoind < NumGeos; geoind++)
    { // 查看每个几何元素是否是共享元素
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        length = end2 - start2;
        if (length > 1)
        { // 表示该几何元素是共享的，需往相应的进程上加上相应的长度
            for (j = start2; j < end2; j++)
            {                          // 处理该几何元素在每个进程上的信息
                rankind = Entries2[j]; // 得到几何元素所在的进程号
                // 所在的进程应该增加该共享元素所需要的空间(每个共享元素应该有的长度)
                SharedINFO4GEO[rankind]->Ptw[RankPos[rankind] + 1] = length;
                // 第一个进程为宿主
                SharedINFO4GEO[rankind]->Owners[RankPos[rankind]] = Entries2[start2];
                RankPos[rankind]++;
            } // end for j
        }     // end for if
    }         // end for geoind, 由此得到了每个进程上所包含的共享几何元素的个数
    // 接下来给每个进程中申请记录共享元素的空间
    INT SharedNum;
    for (rankind = 0; rankind < NumRanks; rankind++)
    {
        SharedINFO4GEO[rankind]->Ptw[0] = 0;
        SharedNum = SharedINFO4GEO[rankind]->SharedNum;
        for (k = 0; k < SharedNum; k++)
        {
            SharedINFO4GEO[rankind]->Ptw[k + 1] += SharedINFO4GEO[rankind]->Ptw[k];
        } // end for k
        // 申请空间
        SharedINFO4GEO[rankind]->SharedRanks = malloc(SharedINFO4GEO[rankind]->Ptw[SharedNum] * sizeof(INT));
        SharedINFO4GEO[rankind]->LocalIndex = malloc(SharedINFO4GEO[rankind]->Ptw[SharedNum] * sizeof(INT));
        // printf("The length for the %d-th rank: %d\n", rankind, SharedINFO4GEO[rankind]->Ptw[SharedNum]);
    } // end for rankind
    // 接下来对每个进程上的共享信息赋值
    memset(RankPos, 0, NumRanks * sizeof(INT));
    for (geoind = 0; geoind < NumGeos; geoind++)
    {
        start2 = Ptw2[geoind];
        end2 = Ptw2[geoind + 1];
        length = end2 - start2;
        if (length > 1)
        { // 该共享元素的长度为length
            for (j = start2; j < end2; j++)
            {
                rankind = Entries2[j];        // 获得进程号
                startrank = RankPos[rankind]; // 获得当前进程的起始位置
                for (i = 0; i < length; i++)
                {
                    SharedINFO4GEO[rankind]->SharedRanks[startrank + i] = Entries2[start2 + i];           // 记录共享元素所在的进程号
                    SharedINFO4GEO[rankind]->LocalIndex[startrank + i] = LocalGEOIndex2Ranks[start2 + i]; // 记录共享元素在所在进程上的局部编号
                }                                                                                         // end for i
                RankPos[rankind] += length;
            } // end for j
        }     // end for if(length>1)
    }         // end for geoind 至此每个进程上的共享信息产生完备
    OpenPFEM_Free(RankPos);
} // 至此得到了几何元素在各个进程中的共享信息

// 功能: GEO4Elems和GEO2Ranks来得到LocalGEO4Elems
//  GEO4Elems: 表示每个单元上几何元素的原始编号信息,Elem4Ranks表示每个单元所在的进程信息,GEO2Ranks表示每个几何元素所在的进程信息,
//  LocalGEOIndex2Ranks表示每个几何元素的局部编号信息(与GEO2Ranks一样的位置结构),
//  LocalGEO4Elems表示每个进程上每个单元几何元素的局部编号信息, 第一层指标是进程号, 即表示一个进程是一个PTW
// 如何得到Elem4Ranks的信息？？？
void GetLocalGEO4Elems(PTW *GEO4Elems, PTW *Elem4Ranks, PTW *GEO2Ranks, PTW **LocalGEO4Elems)
{
    INT NumRanks = Elem4Ranks->NumRows;
    INT NumELems = GEO4Elems->NumRows;
    INT NumGeos = GEO2Ranks->NumRows;
    // LocalGEO4Elems = malloc(NumRanks * sizeof(PTW *)); //每个进程对应一个PTW*的对象
    INT *LocalGEOIndex2Ranks = GEO2Ranks->AndEntries; // 取出每个几何元素在每个进程上的局部编号
    INT rankind, i, j, k, num_elem, start, end, start2, start3, start4, end4,
        elemind, geoind, length;
    // for (k = 0; k < NumRanks; k++)
    //     LocalGEO4Elems[k] = malloc(sizeof(PTW)); //每个进程申请空间
    PTW *localgeo4elems; // 用来临时指向每个进程的输出变量
    for (rankind = 0; rankind < NumRanks; rankind++)
    { // 处理第rankind个进程
        start = Elem4Ranks->Ptw[rankind];
        end = Elem4Ranks->Ptw[rankind + 1];
        num_elem = end - start;                   // 该进程上的单元个数
        localgeo4elems = LocalGEO4Elems[rankind]; // 指向目前进程中的对象
        localgeo4elems->NumRows = num_elem;       // 单元的个数
        localgeo4elems->Ptw = malloc((num_elem + 1) * sizeof(INT));
        memset(localgeo4elems->Ptw, 0, (num_elem + 1) * sizeof(INT));
        for (k = 0; k < num_elem; k++)
        {
            elemind = Elem4Ranks->Entries[start + k]; // 得到单元的全局编号
            localgeo4elems->Ptw[k + 1] = GEO4Elems->Ptw[elemind + 1] - GEO4Elems->Ptw[elemind];
        }
        localgeo4elems->Ptw[0] = 0;
        for (k = 0; k < num_elem; k++)
        {
            localgeo4elems->Ptw[k + 1] += localgeo4elems->Ptw[k];
        }
        // 具体给局部编号
        localgeo4elems->Entries = malloc(localgeo4elems->Ptw[num_elem] * sizeof(INT));
        for (k = 0; k < num_elem; k++)
        {
            elemind = Elem4Ranks->Entries[start + k];     // 得到单元的全局编号
            start2 = GEO4Elems->Ptw[elemind];             // 得到该单元上几何元素的全局编号起始位置
            start3 = localgeo4elems->Ptw[k];              // 得到目标数组的起始位置
            length = localgeo4elems->Ptw[k + 1] - start3; // 目前单元上的几何元素的个数
            for (j = 0; j < length; j++)
            {                                            // 目前单元的第j个几何元素
                geoind = GEO4Elems->Entries[start2 + j]; // 得到几何元素的全局编号
                // 去寻找相应的局部编号
                start4 = GEO2Ranks->Ptw[geoind];
                end4 = GEO2Ranks->Ptw[geoind + 1];
                for (i = start4; i < end4; i++)
                { // 搜索相应的位置
                    if (GEO2Ranks->Entries[i] == rankind)
                    { // 进行赋值
                        localgeo4elems->Entries[start3 + j] = LocalGEOIndex2Ranks[i];
                    } // end if
                }     // end for i
            }         // end for j
        }             // end for k
    }                 // end for rankind
} // end for this program

// 根据得到的共享几何元素的信息来生成针对每个邻居进程的共享信息
//  SharedInfor4GEO: 记录了本进程中针对每个共享几何元素的信息
// 输出: SharedGEO4Ranks: 记录了针对每个邻居进程的共享元素信息
void ColletSharedGEO4Ranks(SHAREDINFO4GEO *SharedInfo4GEO, INT myrank, SHAREDGEO4RANK *SharedGEO4Ranks)
{
    INT SharedNumGeos = SharedInfo4GEO->SharedNum; // 得到共享元素的个数
    INT *LocalIndex = SharedInfo4GEO->LocalIndex;
    INT *Ptw0 = SharedInfo4GEO->Ptw;
    INT *SharedRanks0 = SharedInfo4GEO->SharedRanks;
    INT *CoarseNeighRanks = malloc(Ptw0[SharedNumGeos] * sizeof(INT));
    memset(CoarseNeighRanks, -1, Ptw0[SharedNumGeos] * sizeof(INT));
    // INT *CoarsePtw4 = malloc((Ptw2[SharedNumGeos]+1)*sizeof(INT));
    // memset(CoarsePtw4,0,Ptw2[SharedNumGeos]*sizeof(INT));
    INT *GEOOwners = SharedInfo4GEO->Owners;
    INT *Owner4Ranks = SharedGEO4Ranks->Owners;
    INT posind, pos4, pos, localind, flag, i, j, k, rankind, start0, end0, owner, localindex;
    // 首先得统计处相应的邻居进程的个数(有什么好方法吗？)
    INT SharedNumRanks = 0;
    for (k = 0; k < SharedNumGeos; k++)
    { // 第k个几何元素
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1]; // 得到起始和终点位置
        for (j = start0; j < end0; j++)
        {                              // 处理第k个共享元素
            rankind = SharedRanks0[j]; // 得到此处的进程号
            if (rankind != myrank)
            { // 表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                {
                    flag = 0; // 用来表示是否已经存储了目前的进程
                    if (CoarseNeighRanks[i] == rankind)
                    {
                        flag = 1;
                        i = SharedNumRanks + 1; // 跳出循环
                    }                           // end if(CoarseNeighRanks[i]==rankind)
                }                               // end for i
                if (flag == 0)
                { // 得到了一个新的进程号
                    CoarseNeighRanks[SharedNumRanks] = rankind;
                    SharedNumRanks++;
                } // end if flag==0
            }     // end if(rankind!=myrank)
        }         // end for j
    }             // end for k 得到了进程个数:SharedNumRanks
    SharedGEO4Ranks->SharedNumRanks = SharedNumRanks;
    INT *SharedRanks = SharedGEO4Ranks->SharedRanks;
    SharedRanks = malloc(SharedNumRanks * sizeof(INT));
    INT *Ptw = SharedGEO4Ranks->Ptw;
    Ptw = malloc((SharedNumRanks + 1) * sizeof(INT));
    memset(Ptw, 0, (SharedNumRanks + 1) * sizeof(INT));
    for (k = 0; k < SharedNumRanks; k++)
    {
        SharedRanks[k] = CoarseNeighRanks[k]; // 得到邻居进程号
    }
    OpenPFEM_Free(CoarseNeighRanks); // shif
    for (k = 0; k < SharedNumGeos; k++)
    { // 第k个共享几何元素
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1]; // 得到该几何元素的起始和终点位置
        for (j = start0; j < end0; j++)
        {                              // 处理第k个共享元素
            rankind = SharedRanks0[j]; // 得到此处的进程号
            if (rankind != myrank)
            { // 表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                { // 寻找该进程的位置
                    if (SharedRanks[i] == rankind)
                    {                           // 找到了进程所在的位置
                        Ptw[i + 1]++;           // 将相应的位置加1
                        i = SharedNumRanks + 1; // 跳出循环
                    }                           // end if(CoarseNeighRanks[i]==rankind)
                }                               // end for i
            }                                   // end for if
        }                                       // end for j
    }                                           // end for k 得到了每个进程中共享元素的个数
    Ptw[0] = 0;
    for (k = 0; k < SharedNumRanks; k++)
        Ptw[k + 1] += Ptw[k];
    INT *SharedIndex = SharedGEO4Ranks->SharedIndex;
    INT *Index = SharedGEO4Ranks->Index;
    INT *Owners = SharedGEO4Ranks->Owners;
    SharedIndex = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    Index = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    Owners = malloc(Ptw[SharedNumRanks] * sizeof(INT));
    INT *RankPos = malloc(SharedNumRanks * sizeof(INT));
    memset(RankPos, 0, SharedNumRanks * sizeof(INT));
    // 下面来进行赋值
    for (k = 0; k < SharedNumGeos; k++)
    {
        start0 = Ptw0[k];
        end0 = Ptw0[k + 1];   // 得到该几何元素的起始和终点位置
        owner = GEOOwners[k]; // 获得本元素的宿主进程
        for (j = start0; j < end0; j++)
        {
            rankind = SharedRanks0[j];
            if (rankind == myrank)
            {
                localindex = LocalIndex[j]; // 找到了该元素在本进程的编号
            }                               // end if
        }                                   // end for j
        for (j = start0; j < end0; j++)
        {                              // 处理第k个共享元素
            rankind = SharedRanks0[j]; // 得到此处的进程号
            if (rankind != myrank)
            { // 表示是一个邻居进程，进行进程号的登记
                for (i = 0; i < SharedNumRanks; i++)
                { // 寻找该进程的位置
                    if (SharedRanks[i] == rankind)
                    { // 找到了进程所在的位置i
                        pos = Ptw[i] + RankPos[i];
                        SharedIndex[pos] = LocalIndex[j]; // 记录邻居进程中的编号
                        Index[pos] = localindex;          // 记录本进程中的编号
                        Owner4Ranks[pos] = owner;
                        RankPos[i]++; // 该进程的位置加1
                    }                 // end if
                }                     // end for i
            }                         // end if(rankind!=myrank)
        }                             // end fof j
    }                                 // end for k
    OpenPFEM_Free(RankPos);
} // end for this program

// 打印出PTW中的内容
void PTWPrint(PTW *ptw)
{
    INT k, NumRows = ptw->NumRows, j, start, end;
    OpenPFEM_Print("NumRows=%d,  NumCols=%d\n", ptw->NumRows, ptw->NumCols);
    for (k = 0; k < NumRows; k++)
    {
        OpenPFEM_Print("The %d-th row\n", k);
        start = ptw->Ptw[k];
        end = ptw->Ptw[k + 1];
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->Entries[j]);
        }
        OpenPFEM_Print("\n");
    }
}

void PTWPrint2(PTW *ptw)
{
    INT k, NumRows = ptw->NumRows, j, start, end;
    OpenPFEM_Print("NumRows=%d,  NumCols=%d\n", ptw->NumRows, ptw->NumCols);
    OpenPFEM_Print("The [first entries, second entries]:\n");
    for (k = 0; k < NumRows; k++)
    {
        OpenPFEM_Print("The %d-th row\n", k);
        start = ptw->Ptw[k];
        end = ptw->Ptw[k + 1];
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->Entries[j]);
        }
        OpenPFEM_Print("\n");
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d,  ", ptw->AndEntries[j]);
        }
        OpenPFEM_Print("\n");
    }
}

void SharedInfo4GEOPrint(SHAREDINFO4GEO *SharedInfo4GEO)
{

    INT k, SharedNum = SharedInfo4GEO->SharedNum, start, end, j;
    OpenPFEM_Print("SharedNum: %d\n Owners: \n", SharedNum);
    for (k = 0; k < SharedNum; k++)
    {
        OpenPFEM_Print("%d,  ", SharedInfo4GEO->Owners[k]);
    }
    OpenPFEM_Print("\n");
    INT *Ptw = SharedInfo4GEO->Ptw, *SharedRanks = SharedInfo4GEO->SharedRanks, *LocalIndex = SharedInfo4GEO->LocalIndex;
    OpenPFEM_Print("The SharedRanks and Local index: \n");
    for (k = 0; k < SharedNum; k++)
    {
        start = Ptw[k];
        end = Ptw[k + 1];
        OpenPFEM_Print("The %d-th row\n", k);
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d, ", SharedRanks[j]);
        }
        OpenPFEM_Print("\n");
        for (j = start; j < end; j++)
        {
            OpenPFEM_Print("%d, ", LocalIndex[j]);
        }
        OpenPFEM_Print("\n");
    }
}
// 调整每个三角形的局部节点和边的顺序，使得第一条边为最长边，同时也调整相应的节点的局部顺序
void Mesh2DLabel(MESH *mesh)
{
    INT i, j, k;
    FACE *faces = mesh->Faces, *face;
    LINE *lines = mesh->Lines, *line;
    VERT *verts = mesh->Verts, *vert0, *vert1;
    DOUBLE *Line_Length, L_max;
    Line_Length = (DOUBLE *)malloc(MAXLINE4FACE * sizeof(DOUBLE));
    INT num_faces = mesh->num_face;
    for (i = 0; i < num_faces; i++)
    {
        face = &(faces[i]);
        // 计算每条边
        for (j = 0; j < face->NumLines; j++)
        {
            line = &(lines[face->Line4Face[j]]);
            vert0 = &(verts[line->Vert4Line[0]]);
            vert1 = &(verts[line->Vert4Line[1]]);
            Line_Length[j] = (vert0->Coord[0] - vert1->Coord[0]) * (vert0->Coord[0] - vert1->Coord[0]) +
                             (vert0->Coord[1] - vert1->Coord[1]) * (vert0->Coord[1] - vert1->Coord[1]);
        }
        k = 0;
        L_max = Line_Length[0];
        if (Line_Length[1] > L_max)
        {
            k = 1;
            L_max = Line_Length[1];
        }
        if (Line_Length[2] > L_max)
        {
            k = 2;
        }
        switch (k)
        {
        case 1:
            j = face->Vert4Face[0];
            face->Vert4Face[0] = face->Vert4Face[1];
            face->Vert4Face[1] = face->Vert4Face[2];
            face->Vert4Face[2] = j;
            j = face->Line4Face[0];
            face->Line4Face[0] = face->Line4Face[1];
            face->Line4Face[1] = face->Line4Face[2];
            face->Line4Face[2] = j;
            break;
        case 2:
            j = face->Vert4Face[0];
            face->Vert4Face[0] = face->Vert4Face[2];
            face->Vert4Face[2] = face->Vert4Face[1];
            face->Vert4Face[1] = j;
            j = face->Line4Face[0];
            face->Line4Face[0] = face->Line4Face[2];
            face->Line4Face[2] = face->Line4Face[1];
            face->Line4Face[1] = j;
            break;
        }
    }
    OpenPFEM_Free(Line_Length);
    return;
}

// 使得最长边是第1或者4条边: 为了进行一致加密
void Mesh3DLabel(MESH *mesh)
{
    INT i, j, k, ind;
    VERT vert0, vert1, *Verts = mesh->Verts;
    LINE *Lines = mesh->Lines;
    FACE *Faces = mesh->Faces;
    VOLU *Volus = mesh->Volus, volu;
    INT num_volu = mesh->num_volu;
    int linevert0[6] = {0, 0, 0, 1, 1, 2};
    int linevert1[6] = {1, 2, 3, 2, 3, 3};
    int facevert0[4] = {1, 0, 0, 0};
    int facevert1[4] = {2, 3, 1, 2};
    int facevert2[4] = {3, 2, 3, 1};
    int faceline0[4] = {5, 5, 4, 3};
    int faceline1[4] = {4, 1, 2, 0};
    int faceline2[4] = {3, 2, 0, 1};
    INT newvertandface05[4] = {0, 3, 1, 2}, newvertandface23[4] = {0, 2, 3, 1};
    INT newline05[6] = {2, 0, 1, 4, 5, 3}, newline23[6] = {1, 2, 0, 5, 3, 4};

    INT AuxVerts[4], AuxLines[6], AuxFaces[4];

    DOUBLE LineLengths[6], Vec[3];
    INT maxid;
    // 体的遍历
    for (ind = 0; ind < num_volu; ind++)
    {
        volu = Volus[ind];
        // 计算出6条边的长度
        for (i = 0; i < 6; i++)
        {
            vert0 = Verts[volu.Vert4Volu[linevert0[i]]];
            vert1 = Verts[volu.Vert4Volu[linevert1[i]]];
            Vec[0] = vert1.Coord[0] - vert0.Coord[0];
            Vec[1] = vert1.Coord[1] - vert0.Coord[1];
            Vec[2] = vert1.Coord[2] - vert0.Coord[2];
            LineLengths[i] = sqrt(Vec[0] * Vec[0] + Vec[1] * Vec[1] + Vec[2] * Vec[2]);
            AuxLines[i] = volu.Line4Volu[i];
        } // end for(i=0;i<6;i++)
        for (i = 0; i < 4; i++)
        {
            AuxVerts[i] = volu.Vert4Volu[i];
            AuxFaces[i] = volu.Face4Volu[i];
        } // end for(i=0;i<4;i++)
        maxid = MaxPosition(&LineLengths[0], 0, 5);
        // printf("maxid=%d\n", maxid);
        if ((maxid == 0) || (maxid == 5))
        {
            // 将当前体的点换成 (0,3,1,2)
            for (i = 0; i < 4; i++)
            {
                mesh->Volus[ind].Vert4Volu[i] = AuxVerts[newvertandface05[i]];
                mesh->Volus[ind].Face4Volu[i] = AuxFaces[newvertandface05[i]];
            }

            for (i = 0; i < 6; i++)
            {
                mesh->Volus[ind].Line4Volu[i] = AuxLines[newline05[i]];
            }
        } // end if((maxid==0)||(maxid==5))
        if ((maxid == 2) || (maxid == 3))
        {
            // 将当前体的点换成 (0,2,3,1)
            for (i = 0; i < 4; i++)
            {
                mesh->Volus[ind].Vert4Volu[i] = AuxVerts[newvertandface23[i]];
                mesh->Volus[ind].Face4Volu[i] = AuxFaces[newvertandface23[i]];
            }
            for (i = 0; i < 6; i++)
            {
                mesh->Volus[ind].Line4Volu[i] = AuxLines[newline23[i]];
            }
        } // end if((maxid==2)||(maxid==3))
    }     // end for(int=0;ind<num_volu;ind++)
} // end void LabeMesh3D(MESH *mesh)

// 将一个网格转变成可以进行Bisection的格式
void Mesh3D4BisectionLabel(MESH *mesh)
{

    VERT *Verts = mesh->Verts, vert0, vert1;
    LINE *Lines = mesh->Lines, line;
    FACE *Faces = mesh->Faces, face;
    VOLU *Volus = mesh->Volus, volu;
    INT num_line = mesh->num_line;
    INT num_face = mesh->num_face;
    INT num_volu = mesh->num_volu;
    DOUBLE *LineLengthes = malloc(num_line * sizeof(DOUBLE)), value;
    memset(LineLengthes, 0.0, num_line * sizeof(DOUBLE));
    INT i, j, k, ind, tmp, lineind, faceind, voluind, start, end, localstart, flag;
    INT *LinePosition = malloc(num_line * sizeof(INT));

    // 先计算所有边的长度，作为用来编号的最根本的依据
    for (k = 0; k < num_line; k++)
    {
        line = Lines[k];                  // 取出当前的线对象
        LinePosition[k] = k;              // 记录当前边的位置
        vert0 = Verts[line.Vert4Line[0]]; // 取出当前边的起点
        vert1 = Verts[line.Vert4Line[1]]; // 取出当前边的终点
        LineLengthes[k] = -1.0 * Distance4Verts(vert0, vert1, mesh->worlddim);
    }
    // 对所有的边长进行排序, 排在前面位置表示边长更长
    // QuickSortRealValueVector(LineLengthes, LinePosition, 0, num_line - 1);
    MergeSortRealRecursion(LineLengthes, LinePosition, 0, num_line - 1);

#if MPI_USE
    if (mesh->SharedInfo != NULL)
    {
        // starttime = GetTime();
        MPI_Comm_rank(mesh->comm, &rank);
        INT *LineGIndex = malloc(num_line * sizeof(INT));
        LineGlobalIndexGenerate(mesh);
        start = 0;
        end = start + 1;
        LineGIndex[start] = mesh->Lines[LinePosition[start]].Index; // 获得最长边的全局编号
        value = LineLengthes[start];                                // 目前比较的边长
        while (end < num_line)
        {
            LineGIndex[end] = Lines[LinePosition[end]].Index;
            if ((fabs(LineLengthes[end] - value) > MESHEPS) || (end >= num_line - 1))
            {
                if (end == num_line - 1)
                    end = num_line;
                if (end - start > 1)
                {
                    // 需要进行排序
                    // end he start 相差很大！！！
                    // QuickSortIntValueVector(LineGIndex, LinePosition, start, end - 1);
                    MergeSortRecursion(LineGIndex, LinePosition, start, end - 1);
                }
                start = end;                 // 更新目前的开始位置
                value = LineLengthes[start]; // 更新目前的比较边长
            }
            end++; // 中点位置继续往后走
        }
        free(LineGIndex);
        for (k = 0; k < num_line; k++)
            mesh->Lines[k].Index = k;
    }
#endif
    // 为排列局部编号做准备
    INT *FaceIndicator = malloc(num_face * sizeof(INT)); // 记录每个单元是否已经被处理
    memset(FaceIndicator, 0, num_face * sizeof(INT));    // 0表示未被处理，1表示已经被处理
    INT *ElemIndicator = malloc(num_volu * sizeof(INT)); // 记录每个面是否被处理
    memset(ElemIndicator, 0, num_volu * sizeof(INT));    // 0表示未被处理，1表示已经被处理
    // 定义一些指针，简化书写
    PTW *PTWLine2Faces = NULL;
    PTW *PTWLine2Volus = NULL;
    PTWCreate(&PTWLine2Faces);
    PTWCreate(&PTWLine2Volus);
    // 获得Line2Faces的信息
    // GetGEO2Elems(mesh, 2, 1, PTWLine2Faces);
    GetLine2Faces(mesh, PTWLine2Faces);
    // 获得Line2Volus的信息
    GetGEO2Elems(mesh, 3, 1, PTWLine2Volus);
    // GetLine2Volus(mesh, PTWLine2Volus);
    INT *PtwLine2Faces = PTWLine2Faces->Ptw;
    INT *Line2Faces = PTWLine2Faces->Entries;
    INT *PtwLine2Volus = PTWLine2Volus->Ptw;
    INT *Line2Volus = PTWLine2Volus->Entries;

    // 下面几个数组表示用不同的局部编号为主点(主边)的时候, 局部编号的变化
    INT LocalVertorLine4Face[9] = {0, 1, 2, 1, 2, 0, 2, 0, 1};                                                                             // 三角形的点和线
    INT LocalVertorFace4Volu[24] = {0, 1, 2, 3, 0, 2, 3, 1, 0, 3, 1, 2, 1, 2, 0, 3, 1, 3, 2, 0, 2, 3, 0, 1};                               // 四面体的点和面
    INT LocalLine4Volu[36] = {0, 1, 2, 3, 4, 5, 1, 2, 0, 5, 3, 4, 2, 0, 1, 4, 5, 3, 3, 0, 4, 1, 5, 2, 4, 3, 0, 5, 2, 1, 5, 1, 3, 2, 4, 0}; // 三维的线
    // 下面开始对所有的线进行处理，由最长的线开始，一直到最后一条线
    for (k = 0; k < num_line; k++)
    {
        // printf("rank: %d, k: %d,    num_line: %d\n", rank, k, num_line);
        // 取出排列第k位的边的编号
        lineind = LinePosition[k];
        // 取出与这条边相关的面出来进行编号
        start = PtwLine2Faces[lineind];
        end = PtwLine2Faces[lineind + 1];
        for (j = start; j < end; j++)
        {
            faceind = Line2Faces[j]; // 得到当前面的编号
            // 如果该面没有编号，则对之进行编号
            if (FaceIndicator[faceind] == 0)
            {
                face = Faces[faceind]; // 取出相应的面的对象
                // 找到该边目前所在的位置
                ind = 0;
                flag = 0;
                for (i = 0; i < 3; i++)
                {
                    if (lineind == face.Line4Face[i])
                    {
                        ind = i;
                        flag = 1;
                        i = 4;
                    } // end if lineind
                }     // end for i
                if (flag == 0)
                {
                    printf("Can not find the local position for %d-th line in %d-th face!\n", lineind, faceind);
                    printf("lineind: %d, facelines:[%d, %d, %d]\n", lineind, face.Line4Face[0], face.Line4Face[1], face.Line4Face[2]);
                }
                // 下面对ind进行分别处理
                localstart = 3 * ind; // 由ind来决定具体变换的方式
                for (i = 0; i < 3; i++)
                {
                    mesh->Faces[faceind].Vert4Face[i] = face.Vert4Face[LocalVertorLine4Face[localstart + i]];
                    mesh->Faces[faceind].Line4Face[i] = face.Line4Face[LocalVertorLine4Face[localstart + i]];
                }
                FaceIndicator[faceind] = 1; // 表示该面已经被处理了
            }                               // end if(FaceIndicator[faceind]==0)
        }                                   // end for(j=start;j<end;j++) 处理了与该边的面
        // 接下来处理与该边联系的单元
        start = PtwLine2Volus[lineind];
        end = PtwLine2Volus[lineind + 1];
        for (j = start; j < end; j++)
        {
            // 取出四面体的编号
            voluind = Line2Volus[j];
            if (ElemIndicator[voluind] == 0)
            {
                // 取出单元的对象
                volu = Volus[voluind];
                // 找到目前的边所在的局部编号
                ind = 0;
                flag = 0;
                for (i = 0; i < 6; i++)
                {
                    if (lineind == volu.Line4Volu[i])
                    {
                        ind = i;
                        flag = 1;
                        i = 7;
                    } // end for iflineind
                }     // end for i
                // 根据ind来进行四面体的更新
                localstart = 4 * ind;
                for (i = 0; i < 4; i++)
                {
                    mesh->Volus[voluind].Vert4Volu[i] = volu.Vert4Volu[LocalVertorFace4Volu[localstart + i]];
                    mesh->Volus[voluind].Face4Volu[i] = volu.Face4Volu[LocalVertorFace4Volu[localstart + i]];
                }
                localstart = 6 * ind;
                for (i = 0; i < 6; i++)
                {
                    mesh->Volus[voluind].Line4Volu[i] = volu.Line4Volu[LocalLine4Volu[localstart + i]];
                }
                ElemIndicator[voluind] = 1;
            } // end if(ElemIndicator[voluind]==0)
        }     // end for j
    }         // end for k
    free(LineLengthes);
    free(LinePosition);
    free(FaceIndicator);
    free(ElemIndicator);
    PTWDestroy(&PTWLine2Faces);
    PTWDestroy(&PTWLine2Volus);
} // 变换网格的局部编号完备，现在可以进行自适应网格加密了

// 找出 向量Vec[start,end]中最大值的位置
INT MaxPosition(DOUBLE *VEC, INT start, INT end)
{
    DOUBLE tmp = VEC[start];
    INT pos = start;
    INT i;
    for (i = start + 1; i <= end; i++)
    {
        if (VEC[i] > tmp)
        {
            tmp = VEC[i];
            pos = i;
        }
    }
    return pos;
}

// 获得与每条线相联系的面的集合
void GetLine2Faces(MESH *mesh, PTW *PTWLine2Faces)
{
    INT num_line = mesh->num_line;
    INT num_face = mesh->num_face;
    PTWLine2Faces->NumRows = num_line;
    PTWLine2Faces->NumCols = mesh->num_face;
    PTWLine2Faces->Ptw = malloc((num_line + 1) * sizeof(INT));
    memset(PTWLine2Faces->Ptw, 0, (num_line + 1) * sizeof(INT));
    INT *Ptw = PTWLine2Faces->Ptw;
    INT i, k, lineind, start, Pos;
    FACE *Faces = mesh->Faces, face;
    // 进行线的遍历，将每个边进行统计，这样得到每个节点所对应线的个数
    // 每一条线存储在编号较小的节点所对应的那一行
    for (k = 0; k < num_face; k++)
    {
        // 得到目前线对象
        face = Faces[k];
        for (i = 0; i < face.NumLines; i++)
        {
            Ptw[face.Line4Face[i] + 1]++;
        }
    } // 如此得到了每个结点上所对应的线的个数
    // 然后将PtwVert2Line处理成稀疏矩阵的方式
    Ptw[0] = 0;
    for (k = 0; k < num_line; k++)
    {
        Ptw[k + 1] += Ptw[k];
    }
    PTWLine2Faces->Entries = malloc(Ptw[num_line] * sizeof(INT)); // 用来存储每条边的第二个节点的标号
    INT *Entries = PTWLine2Faces->Entries;
    INT *LinePos = malloc(num_line * sizeof(INT));
    memset(LinePos, 0, num_line * sizeof(INT));
    // 然后再进行边循环，把相应的对应关系完备
    for (k = 0; k < num_face; k++)
    {
        // 得到目前的线对象
        face = Faces[k];
        for (i = 0; i < face.NumLines; i++)
        {
            lineind = face.Line4Face[i]; // 边的编号
            start = Ptw[lineind];
            Pos = LinePos[lineind];
            PTWLine2Faces->Entries[start + Pos] = k;
            LinePos[lineind]++;
        }
    }
    // 释放内存空间
    OpenPFEM_Free(LinePos);
}

// 获得与每条线相联系的面的集合
void GetLine2Volus(MESH *mesh, PTW *PTWLine2Volus)
{
    INT num_line = mesh->num_line;
    INT num_volu = mesh->num_volu;
    PTWLine2Volus->NumRows = num_line;
    PTWLine2Volus->NumCols = mesh->num_volu;
    PTWLine2Volus->Ptw = malloc((num_line + 1) * sizeof(INT));
    memset(PTWLine2Volus->Ptw, 0, (num_line + 1) * sizeof(INT));
    INT *Ptw = PTWLine2Volus->Ptw;
    INT i, k, lineind, start, Pos;
    VOLU *Volus = mesh->Volus, volu;
    // 进行线的遍历，将每个边进行统计，这样得到每个节点所对应线的个数
    // 每一条线存储在编号较小的节点所对应的那一行
    for (k = 0; k < num_volu; k++)
    {
        // 得到目前线对象
        volu = Volus[k];
        for (i = 0; i < volu.NumLines; i++)
        {
            Ptw[volu.Line4Volu[i] + 1]++;
        }
    } // 如此得到了每个结点上所对应的线的个数
    // 然后将PtwVert2Line处理成稀疏矩阵的方式
    Ptw[0] = 0;
    for (k = 0; k < num_line; k++)
    {
        Ptw[k + 1] += Ptw[k];
    }
    PTWLine2Volus->Entries = malloc(Ptw[num_line] * sizeof(INT)); // 用来存储每条边的第二个节点的标号
    INT *Entries = PTWLine2Volus->Entries;
    INT *LinePos = malloc(num_line * sizeof(INT));
    memset(LinePos, 0, num_line * sizeof(INT));
    // 然后再进行边循环，把相应的对应关系完备
    for (k = 0; k < num_volu; k++)
    {
        // 得到目前的线对象
        volu = Volus[k];
        for (i = 0; i < volu.NumLines; i++)
        {
            lineind = volu.Line4Volu[i]; // 边的编号
            start = Ptw[lineind];
            Pos = LinePos[lineind];
            PTWLine2Volus->Entries[start + Pos] = k;
            LinePos[lineind]++;
        }
    }
    // 释放内存空间
    OpenPFEM_Free(LinePos);
}

// 检查得到的网格是否协调, 面上的点、线和线上的点是否一致，体上的面、线、点与面上线、点，线上的点是否一致
void Mesh3DCheck(MESH *mesh)
{

    int i, j, k, v0, v1, v2, LV0, LV1, LV2, LV3, m;
    int tmp;
    int Vert4Line[2], Vert4Face[3], Vert4Volu[4], Line4Face[3], Line4Volu[6], Face4Volu[4];

    VERT vert, *Verts = mesh->Verts, vert0, vert1;
    LINE line, *Lines = mesh->Lines;
    FACE face, *Faces = mesh->Faces;
    VOLU volu;
    int linevert0[6] = {0, 0, 0, 1, 1, 2};
    int linevert1[6] = {1, 2, 3, 2, 3, 3};
    int facevert0[4] = {1, 0, 0, 0};
    int facevert1[4] = {2, 3, 1, 2};
    int facevert2[4] = {3, 2, 3, 1};
    int faceline0[4] = {5, 5, 4, 3};
    int faceline1[4] = {4, 1, 2, 0};
    int faceline2[4] = {3, 2, 0, 1};
    int V[3], FV[3], FL[3], L[3];
    DOUBLE Lens[6];
    INT num_line = mesh->num_line, num_face = mesh->num_face, num_volu = mesh->num_volu;
    MPI_Comm_rank(mesh->comm, &rank);

    DOUBLE *LineLengthes = malloc(num_line * sizeof(DOUBLE)), value;
    memset(LineLengthes, 0.0, num_line * sizeof(DOUBLE));
    // 先计算所有边的长度，作为用来编号的最根本的依据
    for (k = 0; k < num_line; k++)
    {
        line = Lines[k];                  // 取出当前的线对象
        vert0 = Verts[line.Vert4Line[0]]; // 取出当前边的起点
        vert1 = Verts[line.Vert4Line[1]]; // 取出当前边的终点
        for (i = 0; i < mesh->worlddim; i++)
        {
            value = vert1.Coord[i] - vert0.Coord[i]; // 得到边向量第i分量
            LineLengthes[k] += value * value;
        }
        LineLengthes[k] = sqrt(LineLengthes[k]);
    } // end for k
    // printf("rRank: %d, XXXXXXXXXXXX\n", rank);
    // 检查面的第0条边是否是最长边
    for (k = 0; k < num_face; k++)
    {
        face = Faces[k];
        for (j = 0; j < 3; j++)
        {
            Lens[j] = LineLengthes[face.Line4Face[j]];
        }
        for (j = 1; j < 3; j++)
        {
            if (Lens[0] < Lens[j])
            {
                printf("[rank %d] The %d-th face has wrong base line!\n", rank, k);
                printf("faceverts:[%d, %d, %d], facelines: [%d, %d, %d], Lens: [%2.10f, %2.10f, %2.10f]\n",
                       face.Vert4Face[0], face.Vert4Face[1], face.Vert4Face[2],
                       face.Line4Face[0], face.Line4Face[1], face.Line4Face[2], Lens[0], Lens[1], Lens[2]);
            }
        }
    } // end for k
    // 检查网格的四面体是否合理
    for (i = 0; i < num_volu; i++)
    {

        volu = mesh->Volus[i];
        // 取出当前四面体的四个点编号
        for (j = 0; j < 4; j++)
        {
            Vert4Volu[j] = volu.Vert4Volu[j];
        }
        // 取出6条边的编号
        for (j = 0; j < 6; j++)
        {
            Line4Volu[j] = volu.Line4Volu[j];
            Lens[j] = LineLengthes[Line4Volu[j]];
        }
        // 取出4个面的编号
        for (j = 0; j < 4; j++)
        {
            Face4Volu[j] = volu.Face4Volu[j];
        }
        // 下面来检查本四面体的一致性
        // 检查点和线是否一致
        for (j = 0; j < 6; j++)
        {
            // 检查每一条线的点的编号是否跟单元的节点是否一致
            // 取出相应的线
            line = Lines[Line4Volu[j]];
            LV0 = line.Vert4Line[0];
            LV1 = line.Vert4Line[1];
            // 取出从体得到的本线上的两个节点
            v0 = volu.Vert4Volu[linevert0[j]];
            v1 = volu.Vert4Volu[linevert1[j]];
            tmp = 0;
            if ((v0 == LV0) && (v1 == LV1))
            {
                tmp = 1;
            }
            if ((v0 == LV1) && (v1 == LV0))
            {
                tmp = 1;
            }
            if (tmp == 0)
            {
                printf("The %d-th volu, %d-th line has wrong vert numbers\n", i, j);
                return;
            } // end if tmp==0
            if ((j > 0) && (Lens[0] < Lens[j]))
            { // 表示目前的边比该单元的第0条边长, 不符合主边的定义
                printf("The %d-th volu has wrong base line!\n", i);
                printf("Verts: [ ");
                for (k = 0; k < 4; k++)
                    printf("%d, ", Vert4Volu[k]);
                printf("]\n");
            }
        } // end for(j=0;j<6;j++)
        // 检查4个面
        for (j = 0; j < 4; j++)
        {
            // get the current face
            face = Faces[Face4Volu[j]];
            // 取出该面上的3个点
            FV[0] = face.Vert4Face[0];
            FV[1] = face.Vert4Face[1];
            FV[2] = face.Vert4Face[2];
            // 取出从体上得到的3个节点
            V[0] = volu.Vert4Volu[facevert0[j]];
            V[1] = volu.Vert4Volu[facevert1[j]];
            V[2] = volu.Vert4Volu[facevert2[j]];
            // 检查上面两组节点编号是否一致，有6种情况
            tmp = 0;
            for (k = 0; k < 3; k++)
            {
                for (m = 0; m < 3; m++)
                {
                    if (FV[k] == V[m])
                        tmp++;
                }
            } // end for k
            if (tmp < 3)
            {
                printf("The %d-th volu, %d-th face's verts number is wrong! facevert=(%d, %d, %d), voluvert=(%d, %d, %d)\n", i, j, FV[0], FV[1], FV[2], V[0], V[1], V[2]);
            }
            // 取出该面上的三条线的编号
            FL[0] = face.Line4Face[0];
            FL[1] = face.Line4Face[1];
            FL[2] = face.Line4Face[2];
            // 取出从体上来的3条线的编号
            L[0] = volu.Line4Volu[faceline0[j]];
            L[1] = volu.Line4Volu[faceline1[j]];
            L[2] = volu.Line4Volu[faceline2[j]];
            // 检查上面线的编号是否一致
            tmp = 0;
            for (k = 0; k < 3; k++)
            {
                for (m = 0; m < 3; m++)
                {
                    if (FL[k] == L[m])
                        tmp++;
                }
            }
            if (tmp < 3)
            {
                printf("The %d-th volu, %d-th face, %d-th face's lines number is wrong: facelines=(%d, %d, %d), volulines=(%d, %d, %d)!\n", i, Face4Volu[j], j, FL[0], FL[1], FL[2], L[0], L[1], L[2]);
                return;
            }
        } // end  for(j=0;j<4;j++)
    }     // for(i=0;i<mesh->num_volu;i++)
} // end checkmesh3D

void BDIDCheck(MESH *mesh)
{
    INT i, j, k;
    INT num_vert = mesh->num_vert;
    INT num_line = mesh->num_line;
    INT num_face = mesh->num_face;
    VERT *Verts = mesh->Verts;
    LINE *Lines = mesh->Lines;
    FACE *Faces = mesh->Faces;
    for (k = 0; k < num_vert; k++)
    {
        if ((Verts[k].BD_ID != 0) && (Verts[k].BD_ID != 1))
        {
            printf("The %d-th vert has BDID problem!\n", k);
        }
    }

    for (k = 0; k < num_line; k++)
    {
        if ((Lines[k].BD_ID != 0) && (Lines[k].BD_ID != 1))
        {
            printf("The %d-th line has BDID problem!\n", k);
        }
    }

    for (k = 0; k < num_face; k++)
    {
        if ((Faces[k].BD_ID != 0) && (Faces[k].BD_ID != 1))
        {
            printf("The %d-th face has BDID problem!\n", k);
        }
    }
}

// 获得每两个点相对应的线的编号, 编号小的节点来确定所在的行
void GetVert2Lines(MESH *mesh, PTW *Vert2Lines)
{
    INT num_vert = mesh->num_vert;
    Vert2Lines->NumRows = mesh->num_vert;
    Vert2Lines->NumCols = mesh->num_line;
    Vert2Lines->Ptw = malloc((num_vert + 1) * sizeof(INT));
    memset(Vert2Lines->Ptw, 0, (num_vert + 1) * sizeof(INT));
    INT *Ptw = Vert2Lines->Ptw;
    INT i, j, k, num_line = mesh->num_line, ind0, ind1, Pos, tmp;
    LINE *Lines = mesh->Lines, line;
    // 进行线的遍历，将每个边进行统计，这样得到每个节点所对应线的个数
    // 每一条线存储在编号较小的节点所对应的那一行
    for (k = 0; k < num_line; k++)
    {
        // 得到目前线对象
        line = Lines[k];
        ind0 = line.Vert4Line[0];
        ind1 = line.Vert4Line[1];
        if (ind0 > ind1)
            ind0 = ind1; // 两个节点中取较小的那个
        Ptw[ind0 + 1]++;
    } // 如此得到了每个结点上所对应的线的个数
    // 然后将PtwVert2Line处理成稀疏矩阵的方式
    Ptw[0] = 0;
    for (k = 0; k < num_vert; k++)
    {
        Ptw[k + 1] += Ptw[k];
    }
    Vert2Lines->Entries = malloc(Ptw[num_vert] * sizeof(INT));    // 用来存储每条边的第二个节点的标号
    Vert2Lines->AndEntries = malloc(Ptw[num_vert] * sizeof(INT)); // 用来存储相应边的编号
    INT *Entries = Vert2Lines->Entries;
    INT *AndEntries = Vert2Lines->AndEntries;
    INT *VertPos = malloc(num_vert * sizeof(INT));
    memset(VertPos, 0, num_vert * sizeof(INT));
    // 然后再进行边循环，把相应的对应关系完备
    for (k = 0; k < num_line; k++)
    {
        // 得到目前的线对象
        line = Lines[k];
        // 处理该线上的所有的点
        ind0 = line.Vert4Line[0];
        ind1 = line.Vert4Line[1];
        if (ind0 > ind1)
        {
            tmp = ind0;
            ind0 = ind1;
            ind1 = tmp;
        }
        Pos = Vert2Lines->Ptw[ind0] + VertPos[ind0];
        Entries[Pos] = ind1;
        AndEntries[Pos] = k; // 记录目前线的编号
        VertPos[ind0]++;
    }
    // 释放内存空间
    OpenPFEM_Free(VertPos);
}

void PTWDestroy(PTW **ptw)
{
    if (*ptw == NULL)
        return;
    OpenPFEM_Free((*ptw)->Ptw);
    OpenPFEM_Free((*ptw)->Entries);
    OpenPFEM_Free((*ptw)->AndEntries);
    OpenPFEM_Free((*ptw));
}

void SharedInfo4GEODestroy(SHAREDINFO4GEO **SharedInfo4GEO)
{
    if ((*SharedInfo4GEO) == NULL)
        return;
    OpenPFEM_Free((*SharedInfo4GEO)->SharedRanks);
    OpenPFEM_Free((*SharedInfo4GEO)->Ptw);
    OpenPFEM_Free((*SharedInfo4GEO)->Owners);
    OpenPFEM_Free((*SharedInfo4GEO)->LocalIndex);
    OpenPFEM_Free((*SharedInfo4GEO));
}

void Mesh2DMatlabWrite(MESH *mesh, char *file)
{
    FILE *fp = fopen(file, "w");
    INT i, j;
    fprintf(fp, "#DataFile of 2D mesh to matlab!\n");
    fprintf(fp, "POINTS %d double\n", mesh->num_vert);
    // 输出节点信息
    VERT vert;
    for (i = 0; i < mesh->num_vert; i++)
    {
        vert = mesh->Verts[i];
        fprintf(fp, "%f %f  \n", vert.Coord[0], vert.Coord[1]);
    }
    /* 输出面的信息 */
    fprintf(fp, "CELLS %d %d\n", mesh->num_face, mesh->num_face * 3);
    FACE face;
    INT numverts;
    for (i = 0; i < mesh->num_face; i++)
    {
        face = mesh->Faces[i];
        numverts = face.NumVerts;
        fprintf(fp, "%d %d %d %d\n", numverts, face.Vert4Face[0] + 1, face.Vert4Face[1] + 1, face.Vert4Face[2] + 1);
    }
    fclose(fp);
}

// 下面的函数是获得每个进程上每个单元上的几何元素在宿主进程中的编号
//  GeoDim表示GEO元素的维数,0表示点，1表示线，2表示面，3表示体
void GetOwnerGEO4Elems(MESH *mesh, INT ElemDim, INT GeoDim, PTW *OwnerGEO4Elems)
{
    // 先获得本进程中的GEO4Elems
    GetGEO4Elems(mesh, ElemDim, GeoDim, OwnerGEO4Elems);
    // 然后利用mesh中SharedInfo来进行更新
    PTW *GEO2Elems;
    PTWCreate(&GEO2Elems);
    PTWTranspose(OwnerGEO4Elems, GEO2Elems);
    SHAREDINFO *SharedInfo = mesh->SharedInfo;
    SHAREDGEO **SharedGeos, *sharedgeo;
    switch (GeoDim)
    {
    case 0:
        SharedGeos = &(SharedInfo->SharedVerts);
        break;
    case 1:
        SharedGeos = &(SharedInfo->SharedLines);
        break;
    case 2:
        SharedGeos = &(SharedInfo->SharedFaces);
        break;
    case 3:
        SharedGeos = &(SharedInfo->SharedVolus);
        break;
    }
    INT neignum = SharedInfo->NumNeighborRanks;
    INT *NeighborRanks = SharedInfo->NeighborRanks;
    INT ind, rankind, geoind, index, sharedindex, owner, sharednum, elemind, k, j, elemstart, elemend, geostart, geoend;
    for (ind = 0; ind < neignum; ind++)
    {
        // 获得邻居进程号
        rankind = NeighborRanks[ind];
        sharedgeo = SharedGeos[ind];      // 取出相应的共享几何对象
        sharednum = sharedgeo->SharedNum; // 得到本进程上共享几何对象的个数
        for (geoind = 0; geoind < sharednum; geoind++)
        {                                     // 对本进程上共享几何元素进行循环
            owner = sharedgeo->Owner[geoind]; // 获得该几何元素的宿主进程
            if (owner == rankind)
            {                                                 // 本进程等于owner，即表示该几何元素的宿主进程了
                index = sharedgeo->Index[geoind];             // 获得该几何元素在本地的编号
                sharedindex = sharedgeo->SharedIndex[geoind]; // 获得该几何元素在宿主进程中的编号
                // 利用index找到与本几何元素相对应的单元的位置
                geostart = GEO2Elems->Ptw[geoind];   // 得到几何元素在GEO2Elems中的起始位置
                geoend = GEO2Elems->Ptw[geoind + 1]; // 得到相应的终点位置
                for (k = geostart; k < geoend; k++)
                {
                    elemind = GEO2Elems->Entries[k];            // 得到单元的编号
                    elemstart = OwnerGEO4Elems->Ptw[elemind];   // 得到该单元的起始位置
                    elemend = OwnerGEO4Elems->Ptw[elemind + 1]; // 得到相应的终点位置
                    for (j = elemstart; j < elemend; j++)
                    { // 对该单元所对应的几何元素进行循环
                        if (index == OwnerGEO4Elems->Entries[j])
                        {
                            // 找到了相应的共享几何元素所在的位置，那么就可以更形成宿主进程中的几何元素编号
                            OwnerGEO4Elems->Entries[j] = sharedindex; // 更新成宿主进程的编号
                            j = elemend + 2;                          // 跳出循环
                        }                                             // end if(index == OwnerGEO4Elems->Entries[j])
                    }                                                 // end for(j=elemstart;j<elemend;j++)
                }                                                     // end for(k=geostart;k<geoend;k++)
            }                                                         // end if(owner==rankind)
        }                                                             // end for(geoind=0;geoind<sharednum;geoind++)
    }                                                                 // for(ind=0;ind<neignum;ind++)
} // end for this program

// 下面的函数是获得每个单元在其宿主进程上所包含的局部几何元素的编号，即几何元素在单元进程上的编号
//  GeoDim表示GEO元素的维数,0表示点，1表示线，2表示面，3表示体
void GetGEO4ElemsOnOwner(MESH *mesh, INT ElemDim, INT GeoDim, PTW *GEO4ElemsOnOwner)
{
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // 先获得本进程中的GEO4Elems
    GetGEO4Elems(mesh, ElemDim, GeoDim, GEO4ElemsOnOwner);
    // 然后利用mesh中SharedInfo来进行更新
    PTW *GEO2Elems;
    PTWCreate(&GEO2Elems);
    PTWTranspose(GEO4ElemsOnOwner, GEO2Elems);
    SHAREDINFO *SharedInfo = mesh->SharedInfo;
    SHAREDGEO *SharedGeos, *sharedgeo;
    switch (GeoDim)
    {
    case 0:
        SharedGeos = SharedInfo->SharedVerts;
        break;
    case 1:
        SharedGeos = SharedInfo->SharedLines;
        break;
    case 2:
        SharedGeos = SharedInfo->SharedFaces;
        break;
    case 3:
        SharedGeos = SharedInfo->SharedVolus;
        break;
    } // end switch(GeoDim)
    // 获得本进程中共享单元的对象
    SHAREDGEO *SharedElems, *sharedelems;
    INT num_elem;
    switch (ElemDim)
    {
    case 0:
        SharedElems = SharedInfo->SharedVerts;
        num_elem = mesh->num_vert;
        break;
    case 1:
        SharedElems = SharedInfo->SharedLines;
        num_elem = mesh->num_line;
        break;
    case 2:
        SharedElems = SharedInfo->SharedFaces;
        num_elem = mesh->num_face;
        break;
    case 3:
        SharedElems = SharedInfo->SharedVolus;
        num_elem = mesh->num_volu;
        break;
    } // end switch(ElemDim)
    INT neignum = SharedInfo->NumNeighborRanks;
    INT *NeighborRanks = SharedInfo->NeighborRanks;
    INT ind, rankind, geoind, index, sharedindex, owner, sharednum, elemind, k, j,
        elemstart, elemend, geostart, geoend, elemrank;
    // 首先获得每个单元上的宿主进程
    INT *ElemsRank = malloc(num_elem * sizeof(INT));
    memset(ElemsRank, rank, num_elem * sizeof(INT)); // 先全部设为本进程的进程号
    // 对邻居进程进行循环，更新共享单元的宿主进程信息
    for (ind = 0; ind < neignum; ind++)
    {
        // 获得邻居进程号
        rankind = NeighborRanks[ind];
        sharedelems = &(SharedElems[ind]);  // 取出相应的共享几何对象
        sharednum = sharedelems->SharedNum; // 得到本进程上共享几何对象的个数
        for (k = 0; k < sharednum; k++)
        {
            elemind = sharedelems->Index[k];            // 得到单元在本进程中的编号
            ElemsRank[elemind] = sharedelems->Owner[k]; // 将宿主进程赋给相应的单元
        }                                               // end for(k=0;k<sharednum;k++)
    }                                                   // end for(ind=0;ind<neignum;ind++)
    // 下面对相应的共享几何元素进行遍历，赋值给相应的单元
    for (ind = 0; ind < neignum; ind++)
    {
        rankind = NeighborRanks[ind];     // 获得相应的邻居进程号
        sharedgeo = &(SharedGeos[ind]);   // 取出相应的共享几何对象
        sharednum = sharedgeo->SharedNum; // 得到本进程上共享几何对象的个数
        for (geoind = 0; geoind < sharednum; geoind++)
        {                                        // 对本进程上共享几何元素进行循环
            geostart = GEO2Elems->Ptw[geoind];   // 得到几何元素在GEO2Elems中的起始位置
            geoend = GEO2Elems->Ptw[geoind + 1]; // 得到相应的终点位置
            for (k = geostart; k < geoend; k++)
            {
                elemind = GEO2Elems->Entries[k]; // 得到单元的编号
                elemrank = ElemsRank[elemind];   // 得到单元的宿主进程
                // 如果单元的宿主进程与目前正在遍历的进程号相等的话，就进行赋值
                if (elemrank == rankind)
                {
                    index = sharedgeo->Index[geoind];             // 获得该几何元素在本地的编号
                    sharedindex = sharedgeo->SharedIndex[geoind]; // 获得该几何元素在单元进程中编号
                    elemstart = GEO4ElemsOnOwner->Ptw[elemind];   // 得到该单元的起始位置
                    elemend = GEO4ElemsOnOwner->Ptw[elemind + 1]; // 得到相应的终点位置
                    for (j = elemstart; j < elemend; j++)
                    { // 对该单元所对应的几何元素进行循环
                        if (index == GEO4ElemsOnOwner->Entries[j])
                        {
                            // 找到了相应的共享几何元素所在的位置，那么就可以更形成宿主进程中的几何元素编号
                            GEO4ElemsOnOwner->Entries[j] = sharedindex; // 更新单元宿主进程中的编号
                            j = elemend + 2;                            // 跳出循环
                        }                                               // end if(index == GEO4ElemsOnOwner->Entries[j])
                    }                                                   // end for(j=elemstart;j<elemend;j++)
                }                                                       // end if(elemrank==rankind)
            }                                                           // end for(k=geostart;k<geoend;k++)
        }                                                               // end for(geoind=0;geoind<sharednum;geoind++)
    }                                                                   // end for(ind=0;ind<neignum;ind++)
    PTWDestroy(&GEO2Elems);
    free(ElemsRank);
} // end for this program

// 网格信息的完备化
// 根据网格中的节点坐标、单元的节点编号、以及单元的三个表面的边界信息来生成一个完备的网格。
void Mesh3DComplete(MESH *mesh, INT *ElemBoundaries)
{
    // 目前的信息：网格点的个数、单元个数、每个单元所包含的节点编号、每个单元四个面的边界信息
    // 先产生线的信息
    INT num_volu = mesh->num_volu, k, i, j, vertind, lineind, faceind, voluind;
    INT num_vert = mesh->num_vert;
    VERT *Verts = mesh->Verts;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        Verts[vertind].Index = vertind;
        Verts[vertind].BD_ID = 0;
    }
    PTW *Vert2Lines;
    PTWCreate(&Vert2Lines);
    // printf("num_vert: %d\n", num_vert);
    Vert2Lines->Ptw = malloc((num_vert + 1) * sizeof(INT));
    memset(Vert2Lines->Ptw, 0, (num_vert + 1) * sizeof(INT));
    INT LocalLineVert0[6] = {0, 0, 0, 1, 1, 2};
    INT LocalLineVert1[6] = {1, 2, 3, 2, 3, 3};
    INT VoluNumLines = 6, VoluNumVerts = 4, VoluNumFaces = 4;
    INT vert0, vert1, num_face;
    VOLU *Volus = mesh->Volus, volu;
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        Volus[voluind].NumVerts = 4;
        Volus[voluind].NumLines = 6;
        Volus[voluind].NumFaces = 4;
        volu = Volus[voluind]; // 获得当前的四面体对象
        for (k = 0; k < VoluNumLines; k++)
        {
            vert0 = volu.Vert4Volu[LocalLineVert0[k]];
            vert1 = volu.Vert4Volu[LocalLineVert1[k]];
            // 我们将每条线的节点从小到大来进行排列
            if (vert0 < vert1)
            {
                Vert2Lines->Ptw[vert0 + 1]++;
            }
            else
            {
                Vert2Lines->Ptw[vert1 + 1]++;
            } // end if
        }     // end k
    }         // end voluind
    // 下面统计一下与每个节点关联的线的条数
    Vert2Lines->Ptw[0] = 0;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        Vert2Lines->Ptw[vertind + 1] += Vert2Lines->Ptw[vertind];
    }
    INT *CoarseEntries = malloc(Vert2Lines->Ptw[num_vert] * sizeof(INT));
    memset(CoarseEntries, -1, Vert2Lines->Ptw[num_vert] * sizeof(INT));
    INT *VertPos = malloc(num_vert * sizeof(INT));
    memset(VertPos, 0, num_vert * sizeof(INT));
    // 进行具体的线的赋值
    INT tmp, start, end, flag;
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        for (k = 0; k < VoluNumLines; k++)
        {
            vert0 = volu.Vert4Volu[LocalLineVert0[k]];
            vert1 = volu.Vert4Volu[LocalLineVert1[k]];
            if (vert0 > vert1)
            { // 将两个点调换一下顺序
                tmp = vert1;
                vert1 = vert0;
                vert0 = tmp;
            }
            start = Vert2Lines->Ptw[vert0];
            end = start + VertPos[vert0]; // Vert2Lines->Ptw[vert0]
            flag = 0;
            for (j = start; j < end; j++)
            {
                if (CoarseEntries[j] == vert1)
                    flag = 1; // 表示该边已经被存储过
            }
            if (flag == 0)
            {
                CoarseEntries[end] = vert1; // 碰到了一条新的线，登记上
                VertPos[vert0]++;
            } // end if flag==0
        }     // end k
    }         // end for voluind
    // 继续更新Ptw和Entries的数据
    Vert2Lines->Ptw[0] = 0;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        Vert2Lines->Ptw[vertind + 1] = Vert2Lines->Ptw[vertind] + VertPos[vertind];
    }
    OpenPFEM_Free(CoarseEntries);
    INT num_line = Vert2Lines->Ptw[num_vert]; // 总的线的条数
    mesh->num_line = num_line;
    // printf("num_line: %d\n", num_line);
    Vert2Lines->NumRows = num_vert;
    Vert2Lines->Entries = malloc(num_line * sizeof(INT));
    memset(Vert2Lines->Entries, -1, num_line * sizeof(INT));
    memset(VertPos, 0, num_vert * sizeof(INT));
    // 进行具体线的赋值
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        for (k = 0; k < VoluNumLines; k++)
        {
            vert0 = volu.Vert4Volu[LocalLineVert0[k]];
            vert1 = volu.Vert4Volu[LocalLineVert1[k]];
            if (vert0 > vert1)
            {
                tmp = vert1;
                vert1 = vert0;
                vert0 = tmp;
            }
            start = Vert2Lines->Ptw[vert0];
            end = start + VertPos[vert0];
            flag = 0;
            for (j = start; j < end; j++)
            {
                if (Vert2Lines->Entries[j] == vert1)
                {
                    flag = 1;
                    j = end + 2; // 跳出循环
                }                // end if
            }                    // end for j
            if (flag == 0)
            {
                Vert2Lines->Entries[end] = vert1;
                VertPos[vert0]++;
            } // end if(flag==0)
        }     // end for k
    }         // end for voluind
    mesh->Lines = malloc(num_line * sizeof(LINE));
    LINE *Lines = mesh->Lines;
    INT index = 0;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        start = Vert2Lines->Ptw[vertind];
        end = Vert2Lines->Ptw[vertind + 1];
        for (j = start; j < end; j++)
        {
            Lines[index].Vert4Line[0] = vertind;
            Lines[index].Vert4Line[1] = Vert2Lines->Entries[j];
            Lines[index].Index = index;
            Lines[index].BD_ID = 0; // 默认为内部线
            index++;                // 更新
        }                           // end for j
    }                               // end for vertind
    // 给每个体赋上线的编号
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        for (k = 0; k < VoluNumLines; k++)
        {
            vert0 = volu.Vert4Volu[LocalLineVert0[k]];
            vert1 = volu.Vert4Volu[LocalLineVert1[k]];
            if (vert0 > vert1)
            {
                tmp = vert1;
                vert1 = vert0;
                vert0 = tmp;
            }
            start = Vert2Lines->Ptw[vert0];
            end = Vert2Lines->Ptw[vert0 + 1];
            for (j = start; j < end; j++)
            {
                if (Vert2Lines->Entries[j] == vert1)
                {                                    // 找到相应的边的另一点
                    Volus[voluind].Line4Volu[k] = j; // 给该四面体的第k条边上赋值线的编号
                }                                    // end if
            }                                        // end for j
        }                                            // end for k
    }                                                // end for voluind
    // 还有线和点的边界信息没有确定
    // 产生面的信息，也是根据四面体上节点的信息来进行产生
    INT FaceVerts[12] = {1, 2, 3, 0, 3, 2, 0, 1, 3, 0, 2, 1};
    INT LocalVerts[3]; // 用来存储当前面上的3个节点编号
    // 总体面的个数 num_face
    //  printf("num_volu: %d\n", num_volu);
    // 对网格的四面体进行循环，将每个面上的节点编号进行重新排序，编号从小到大
    PTW *Vert2Faces;
    PTWCreate(&Vert2Faces);
    Vert2Faces->NumRows = num_vert;
    Vert2Faces->Ptw = malloc((num_vert + 1) * sizeof(INT));
    memset(Vert2Faces->Ptw, 0, (num_vert + 1) * sizeof(INT));
    // 下面用来存储每个面上的3个节点编号, 每个四面体上4个面
    INT *FaceVert0 = malloc(4 * num_volu * sizeof(int));
    INT *FaceVert1 = malloc(4 * num_volu * sizeof(int));
    INT *FaceVert2 = malloc(4 * num_volu * sizeof(int));
    INT pos = 0, facepos = 0, vertpos = 0;
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        for (k = 0; k < VoluNumFaces; k++)
        { // 对4个面进行循环, 得到每个面的节点编号
            for (j = 0; j < 3; j++)
            {
                // 每个面上有三个点
                LocalVerts[j] = volu.Vert4Volu[FaceVerts[3 * k + j]];
            }                                   // end for j
            QuickSort_Int(LocalVerts, 0, 2);    // 对目前面的节点编号进行排序
            FaceVert0[facepos] = LocalVerts[0]; // 登记目前面的第一个节点
            facepos++;
        } // end for k
    }     // end for elemeind
    // 对每个面的最小编号的点进行排序
    QuickSort_Int(FaceVert0, 0, 4 * num_volu - 1);
    // 对每个点所对应的面的个数进行统计
    end = 4 * num_volu;
    for (k = 0; k < end; k++)
    {
        Vert2Faces->Ptw[FaceVert0[k] + 1]++; // 登记每个点所对应的面的个数
    }
    Vert2Faces->Ptw[0] = 0;
    for (k = 0; k < num_vert; k++)
    {
        Vert2Faces->Ptw[k + 1] += Vert2Faces->Ptw[k]; // 记录每个节点所对应的面的个数
    }
    // 然后进行面的节点编号登记
    memset(VertPos, 0, num_vert * sizeof(INT));
    // 再次对四面体进行循环
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        Volus[voluind].Index = voluind;
        for (k = 0; k < VoluNumFaces; k++)
        { // 对4个面进行循环, 得到每个面的节点编号
            for (j = 0; j < 3; j++)
            {
                LocalVerts[j] = volu.Vert4Volu[FaceVerts[3 * k + j]];      // 登记该面的三个节点
            }                                                              // end for j
            QuickSort_Int(LocalVerts, 0, 2);                               // 对目前面的节点编号进行排序
            pos = Vert2Faces->Ptw[LocalVerts[0]] + VertPos[LocalVerts[0]]; // 获得目前节点所在的位置
            FaceVert1[pos] = LocalVerts[1];
            FaceVert2[pos] = LocalVerts[2];
            VertPos[LocalVerts[0]]++; // 将当前节点位置的位置加1
        }                             // end for k
    }                                 // end for voluind
    // 接下来统计真正面的个数
    //  pos = 0;
    INT start1, end1, vert2;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        start = Vert2Faces->Ptw[vertind];   // 目前第0个节点的起始位置
        end = Vert2Faces->Ptw[vertind + 1]; // 下一个第0个节点的起始位置
        // 对第1个点进行排序，同时第2个节点做相应的变化
        SortTwoVec_Int(FaceVert1, FaceVert2, start, end - 1);
        // 接下来应该要对同样的第1个节点的面进行第2个节点进行排序
        vert1 = FaceVert1[start]; // 获得第1个节点的编号
        start1 = start;           // 第1个节点的起始位置
        for (k = start; k < end; k++)
        {
            if (FaceVert1[k] != vert1)
            {
                // 就说明[start1:k-1]具有相同的vert1，然后对vert2进行排序
                QuickSort_Int(FaceVert2, start1, k - 1);
                vert1 = FaceVert1[k]; // 更新比较对象vert1
                start1 = k;           // 从该位置继续进行检查生成新的面
            }
            else if (k == end - 1)
            {
                QuickSort_Int(FaceVert2, start1, k);
            }
        }               // end for k
    }                   // 对vert0, Vert1, vert2都排好序了
    end = 4 * num_volu; // 每个单元含有4个面
    pos = 0;
    vert0 = FaceVert0[pos];
    vert1 = FaceVert1[pos];
    vert2 = FaceVert2[pos];
    num_face = 1;
    for (k = 1; k < end; k++)
    {
        if ((FaceVert0[k] != vert0) || (FaceVert1[k] != vert1) || (FaceVert2[k] != vert2))
        {
            pos = k;
            vert0 = FaceVert0[pos];
            vert1 = FaceVert1[pos];
            vert2 = FaceVert2[pos];
            num_face++;
        }
        else
        {
            FaceVert0[k] = -1;
            FaceVert1[k] = -1;
            FaceVert2[k] = -1;
        }
    }
    mesh->num_face = num_face;
    mesh->Faces = malloc(num_face * sizeof(FACE));
    FACE *Faces = mesh->Faces; // 创建相应的空间
    memset(VertPos, 0, num_vert * sizeof(INT));
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        start = Vert2Faces->Ptw[vertind];
        end = Vert2Faces->Ptw[vertind + 1];
        for (k = start; k < end; k++)
        {
            if (FaceVert0[k] != -1)
            {
                // 找到了一个面
                VertPos[vertind]++; // 该点作为最小节点编号的面
            }                       // end if
        }                           // end for k
    }                               // end for vertind
    Vert2Faces->Ptw[0] = 0;
    for (vertind = 0; vertind < num_vert; vertind++)
    {
        Vert2Faces->Ptw[vertind + 1] = Vert2Faces->Ptw[vertind] + VertPos[vertind];
    }
    Vert2Faces->Entries = malloc(Vert2Faces->Ptw[num_vert] * sizeof(INT));
    Vert2Faces->AndEntries = malloc(Vert2Faces->Ptw[num_vert] * sizeof(INT));
    end = 4 * num_volu;
    pos = 0;
    memset(VertPos, 0, num_vert * sizeof(INT));
    for (k = 0; k < end; k++)
    {
        if (FaceVert0[k] != -1)
        {
            vertind = FaceVert0[k];
            pos = Vert2Faces->Ptw[vertind] + VertPos[vertind];
            Vert2Faces->Entries[pos] = FaceVert1[k];
            Vert2Faces->AndEntries[pos] = FaceVert2[k];
            VertPos[vertind]++;
        } // end if
    }     // end for k
    OpenPFEM_Free(FaceVert0);
    OpenPFEM_Free(FaceVert1);
    OpenPFEM_Free(FaceVert2);
    OpenPFEM_Free(VertPos);
    // 至此，我们已经对所有的面进行了编号
    // 下面再次对所有的四面体进行遍历，得到每一个四面体所对应的面的编号
    INT *Entries = Vert2Faces->Entries;                  // 每个面第二个节点的编号
    INT *AndEntries = Vert2Faces->AndEntries;            // 每个面第三个点的编号
    INT *FaceIndicator = malloc(num_face * sizeof(INT)); //
    INT bd_id;
    memset(FaceIndicator, 0, num_face * sizeof(INT));
    // 对所有的四面体进行遍历
    for (voluind = 0; voluind < num_volu; voluind++)
    {
        volu = Volus[voluind];
        for (k = 0; k < VoluNumFaces; k++)
        {
            for (j = 0; j < 3; j++)
            {
                LocalVerts[j] = volu.Vert4Volu[FaceVerts[3 * k + j]]; // 登记该面的三个节点
            }                                                         // end for j
            QuickSort_Int(LocalVerts, 0, 2);                          // 对目前面的节点编号进行排序
            // 找到相应面的编号
            //  if ((voluind == 0) && (k == 0))
            //  {
            //      printf("LocalVerts: ");
            //      for (j = 0; j < 3; j++)
            //          printf("%d  ", LocalVerts[j]);
            //      printf("\n");
            //  }
            start = Vert2Faces->Ptw[LocalVerts[0]]; // 从第0个节点开始寻找面的编号
            end = Vert2Faces->Ptw[LocalVerts[0] + 1];
            for (j = start; j < end; j++)
            {
                if ((Entries[j] == LocalVerts[1]) && (AndEntries[j] == LocalVerts[2]))
                {
                    // 找到该面的编号为j
                    Volus[voluind].Face4Volu[k] = j;
                    if (FaceIndicator[j] == 0)
                    {                                            // 需要为这个面进行节点赋值
                        bd_id = ElemBoundaries[voluind * 4 + k]; // 目前是在处理第voluind个面的第k面
                        for (i = 0; i < 3; i++)
                        { // 为这个面赋上节点的编号
                            vertind = volu.Vert4Volu[FaceVerts[3 * k + i]];
                            Faces[j].Vert4Face[i] = vertind;
                            if (bd_id > 0)
                                Verts[vertind].BD_ID = bd_id; // 对该面上的节点赋上边界编号
                            Faces[j].Index = j;               // 目前面的编号为j
                        }                                     // end for
                        Faces[j].BD_ID = bd_id;               // 赋值面的边界编号
                        FaceIndicator[j] = 1;
                    } // end if
                }     // end if((Entries[j]==LocalVert2[1])&&(AndEntries[j]==LocalVerts[2]))
            }         // end for j
        }             // end for k
    }                 // end for voluind
    // 最后为面赋值上线的编号，可以直接为Vert2Line来得到
    OpenPFEM_Free(FaceIndicator);
    PTWDestroy(&Vert2Faces);
    FACE face;
    INT LocalFaceVert0[3] = {1, 2, 0};
    INT LocalFaceVert1[3] = {2, 0, 1};
    for (faceind = 0; faceind < num_face; faceind++)
    {
        face = Faces[faceind];
        Faces[faceind].NumVerts = 3;
        Faces[faceind].NumLines = 3;
        for (k = 0; k < 3; k++)
        {
            vert0 = face.Vert4Face[LocalFaceVert0[k]];
            vert1 = face.Vert4Face[LocalFaceVert1[k]];
            if (vert0 > vert1)
            {
                tmp = vert1;
                vert1 = vert0;
                vert0 = tmp;
            }
            start = Vert2Lines->Ptw[vert0];
            end = Vert2Lines->Ptw[vert0 + 1];
            for (j = start; j < end; j++)
            {
                if (Vert2Lines->Entries[j] == vert1)
                {
                    Faces[faceind].Line4Face[k] = j; // 得到该面的第k条边
                    bd_id = face.BD_ID;
                    if (bd_id > 0)
                        Lines[j].BD_ID = bd_id;
                } // end if
            }     // end for j
        }         // end for k
    }             // end for faceind
    // 释放内存消耗
    PTWDestroy(&Vert2Lines);
} // end of this program

// 产生线的全局编号
void LineGlobalIndexGenerate(MESH *mesh)
{
    MPI_Comm comm = mesh->comm;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    INT myLineNum = mesh->num_line;
    INT neignum = mesh->SharedInfo->NumNeighborRanks;
    SHAREDINFO *sharedinfo = mesh->SharedInfo;
    SHAREDGEO *sharedline = sharedinfo->SharedLines;
    INT idx_neig, i, neigrank;
    INT *Owner = NULL;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = sharedinfo->NeighborRanks[idx_neig];
        Owner = sharedline[idx_neig].Owner;
        for (i = 0; i < sharedline[idx_neig].SharedNum; i++)
        {
            if (Owner[i] == neigrank)
            {
                myLineNum--;
            }
        }
    }
#if PRINT_INFO
    // OpenPFEM_RankPrint(comm, "myLineNum = %d\n", myLineNum);
#endif

    INT start_g, end_g;
    MPI_Scan(&myLineNum, &end_g, 1, MPI_INT, MPI_SUM, comm);
    start_g = end_g - myLineNum;
#if PRINT_INFO
    // OpenPFEM_RankPrint(comm, "global index %d - %d\n", start_g, end_g);
#endif

    INT *Gindex = (INT *)calloc(mesh->num_line, sizeof(INT));
    INT *Index = NULL;
    LINE *lines = mesh->Lines;
    for (idx_neig = 0; idx_neig < neignum; idx_neig++)
    {
        neigrank = sharedinfo->NeighborRanks[idx_neig];
        Owner = sharedline[idx_neig].Owner;
        Index = sharedline[idx_neig].Index;
        for (i = 0; i < sharedline[idx_neig].SharedNum; i++)
        {
            if (Owner[i] != rank)
            {
                Gindex[Index[i]] = -1;
            }
        }
    }

    INT pos = start_g;
    for (i = 0; i < mesh->num_line; i++)
    {
        if (!Gindex[i])
        {
            Gindex[i] = pos;
            pos++;
        }
    }

    PARADATA *paradata = NULL;
    ParaDataCreate(&paradata, mesh);
    ParaDataAdd(paradata, LINEDATA, 1, MPI_INT, (void *)Gindex);
    RECDATA *recdata = NULL;
    ParaDataCommunicate(paradata, &recdata);
    INT *linesharedGindex = (INT *)(recdata->LineDatas[0]);

    INT j, currindex;
    INT *lineindex = recdata->LineIndex;
    for (i = 0; i < recdata->LineNum; i++)
    {
        if (linesharedGindex[i] != -1)
        {
            currindex = lineindex[i];
            Gindex[currindex] = linesharedGindex[i];
        }
    }
    ParaDataDestroy(&paradata);
    RecDataDestroy(&recdata);

    for (i = 0; i < mesh->num_line; i++)
    {
        lines[i].Index = Gindex[i];
    }
    OpenPFEM_Free(Gindex);
}

//===================================================================
// 按vtk格式输出网格信息
void Mesh3DVTKWrite(MESH *mesh, char *file)
{
    FILE *fp = fopen(file, "w");
    int i, j;
    /* 输出体的信息 */
    fprintf(fp, "# vtk DataFile Version 2.0\n");
    fprintf(fp, "Rho density\n");
    fprintf(fp, "ASCII\n");
    fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");
    fprintf(fp, "POINTS %d double\n", mesh->num_vert);
    // 输出节点信息
    VERT vert;
    for (i = 0; i < mesh->num_vert; i++)
    {
        vert = mesh->Verts[i];
        // printf ("i=%d, %f %f %f\n", i, vert.Coord[0],vert.Coord[1],vert.Coord[2]);
        fprintf(fp, "%f %f %f\n", vert.Coord[0], vert.Coord[1], vert.Coord[2]);
    }
    /* 输出面的信息 */
    fprintf(fp, "CELLS %d %d\n", mesh->num_volu, mesh->num_volu * 5);
    VOLU volu;
    int numverts;
    for (i = 0; i < mesh->num_volu; i++)
    {
        volu = mesh->Volus[i];
        numverts = volu.NumVerts;
        fprintf(fp, "%d %d %d %d %d\n", numverts, volu.Vert4Volu[0], volu.Vert4Volu[1], volu.Vert4Volu[2], volu.Vert4Volu[3]);
    }
    fprintf(fp, "CELL_TYPES %d\n", mesh->num_volu);
    for (i = 0; i < mesh->num_volu; i++)
        fprintf(fp, "%d\n", 10);

    // 2022年1月11日注释
    // fprintf (fp,  "POINT_DATA %d\n", mesh->num_vert);
    // fprintf (fp,  "SCALARS Scalars double %d\n", 1);
    // fprintf (fp,  "LOOKUP_TABLE default\n");

    // for(i=0;i<mesh->num_vert;i++)
    // {
    //  fprintf (fp,  "%f\n", fabs(fefunction->Values[i]));
    // }
    fclose(fp);
    MPI_Barrier(mesh->comm);
    return;
}

DOUBLE Distance4Verts(VERT vert0, VERT vert1, INT worlddim)
{
    DOUBLE length = 0, tmp;
    INT k;
    for (k = 0; k < worlddim; k++)
    {
        tmp = vert1.Coord[k] - vert0.Coord[k];
        length += tmp * tmp;
    }
    return sqrt(length);
}

// 根据边界类型调整网格低层级元素的边界ID
void MeshAdjustForDirichletBound(MESH *mesh, BOUNDARYTYPEFUNCTION *BoundType)
{
    INT worlddim = mesh->worlddim;
    INT num_line, num_face, ind_line, ind_face, BD_ID;
    INT *vert4line, *line4face;
    switch (worlddim)
    {
    case 1:
        break;

    case 2:
        // 如果边的边界类型为Dirichlet 将边的两个顶点的边界类型变为Dirichlet
        num_line = mesh->num_line;
        for (ind_line = 0; ind_line < num_line; ind_line++)
        {
            BD_ID = mesh->Lines[ind_line].BD_ID;
            vert4line = mesh->Lines[ind_line].Vert4Line;
            if(BoundType(BD_ID) == DIRICHLET || BoundType(BD_ID) == STIFFDIRICHLET || BoundType(BD_ID) == MASSDIRICHLET)
            {
                mesh->Verts[vert4line[0]].BD_ID = BD_ID;
                mesh->Verts[vert4line[1]].BD_ID = BD_ID;
            }
        }
        break;

    case 3:
        // 如果面的边界类型为Dirichlet 将面的三个边的边界类型变为Dirichlet
        num_face = mesh->num_face;
        for (ind_face = 0; ind_face < num_face; ind_face++)
        {
            BD_ID = mesh->Faces[ind_face].BD_ID;
            line4face = mesh->Faces[ind_face].Line4Face;
            if(BoundType(BD_ID) == DIRICHLET || BoundType(BD_ID) == STIFFDIRICHLET || BoundType(BD_ID) == MASSDIRICHLET)
            {
                mesh->Lines[line4face[0]].BD_ID = BD_ID;
                mesh->Lines[line4face[1]].BD_ID = BD_ID;
                mesh->Lines[line4face[2]].BD_ID = BD_ID;
            }
        }
        // 如果边的边界类型为Dirichlet 将边的两个顶点的边界类型变为Dirichlet
        num_line = mesh->num_line;
        for (ind_line = 0; ind_line < num_line; ind_line++)
        {
            BD_ID = mesh->Lines[ind_line].BD_ID;
            vert4line = mesh->Lines[ind_line].Vert4Line;
            if(BoundType(BD_ID) == DIRICHLET || BoundType(BD_ID) == STIFFDIRICHLET || BoundType(BD_ID) == MASSDIRICHLET)
            {
                mesh->Verts[vert4line[0]].BD_ID = BD_ID;
                mesh->Verts[vert4line[1]].BD_ID = BD_ID;
            }
        }
        break;
    }
}