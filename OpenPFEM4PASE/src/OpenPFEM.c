#include "OpenPFEM.h"
#include <stdio.h>
#include <malloc.h>
#include <sys/resource.h>

int OpenPFEMInitialized;

// Init和Finalize
void OpenPFEM_Init(int *argc, char ***argv)
{
    if (OpenPFEMInitialized)
    {
        return;
    }
    else
    {
        OpenPFEMInitialized = 1;
#if MPI_USE
        MPI_Init(argc, argv);
#endif
// 对特征值问题进行slepc的初始化
#if SLEPC_USE == 1
        SlepcInitialize(argc, argv, (char *)0, NULL);
#elif PETSC_USE == 1
        PetscInitialize(argc, argv, (char *)0, NULL);
#endif
        return;
    }
}

void OpenPFEM_Finalize(void)
{
    if (!OpenPFEMInitialized)
    {
        return;
    }
    else
    {
        OpenPFEMInitialized = 0;
#if SLEPC_USE == 1
        SlepcFinalize();
#elif PETSC_USE == 1
        PetscFinalize();
#endif
#if MPI_USE
        MPI_Finalize();
#endif
        return;
    }
}

void OpenPFEM_Print(const char *fmt, ...)
{
#if MPI_USE
    int rank = -1;
    MPI_Comm_rank(DEFAULT_COMM, &rank);
    if (PRINT_RANK == rank)
    {
#endif
        va_list vp;
        va_start(vp, fmt);
        vprintf(fmt, vp);
        va_end(vp);
#if MPI_USE
    }
#endif
    return;
}

#if MPI_USE
void OpenPFEM_RankPrint(MPI_Comm comm, const char *fmt, ...)
{
    int rank = -1;
    MPI_Comm_rank(DEFAULT_COMM, &rank);
    if (rank >= 0)
    {
        printf("[RANK %d] ", rank);
        va_list vp;
        va_start(vp, fmt);
        vprintf(fmt, vp);
        va_end(vp);
    }
}
void SplitComm(INT num_block, MPI_Comm **comm_output, INT *my_id)
{
    // 获取本进程 进程号和进程总数
    INT myid, procnum;
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &procnum);
    // 计算每块的进程数
    INT num_proc, left_proc;
    if (procnum < num_block)
    {
        OpenPFEM_Print("进程数小于分块数！\n");
    }
    else
    {
        num_proc = procnum / num_block;
        left_proc = procnum % num_block;
    }

    MPI_Group worldGroup;                        // 创建一个进程组
    MPI_Comm_group(MPI_COMM_WORLD, &worldGroup); // 将该进程组与MPI_COMM_WORLD通信域进行绑定

    MPI_Group *group = malloc(num_block * sizeof(MPI_Group));
    INT *groupid = malloc(procnum * sizeof(INT));
    INT i;
    INT proc_id = 0;
    for (i = 0; i < procnum; i++)
    {
        groupid[i] = i;
    }
    for (i = 0; i < left_proc; i++)
    {
        MPI_Group_incl(worldGroup, (num_proc + 1), &(groupid[proc_id]), &(group[i]));
        proc_id += (num_proc + 1);
    }
    for (i = left_proc; i < procnum; i++)
    {
        MPI_Group_incl(worldGroup, num_proc, &(groupid[proc_id]), &(group[i]));
        proc_id += num_proc;
    }
    MPI_Comm *comm = malloc(num_block * sizeof(MPI_Comm));
    for (i = 0; i < num_block; i++)
    {
        MPI_Comm_create(MPI_COMM_WORLD, group[i], &(comm[i])); // 将进程组group与通信域comm进行绑定
    }
    // 获取本进程所在的局部通讯域的编号
    INT my_block;
    INT temp = left_proc * (num_proc + 1);
    if (myid < temp)
    {
        my_block = myid / (num_proc + 1);
    }
    else
    {
        temp = myid - temp;
        my_block = left_proc + temp / num_proc;
    }
    *comm_output = comm;
    *my_id = my_block;
    free(groupid);
    free(group);
}

#endif

DOUBLE GetTime()
{
    struct rusage usage;
    DOUBLE ret;

    if (getrusage(RUSAGE_SELF, &usage) == -1)
        printf("Error in GetTime!\n");
    ret = ((DOUBLE)usage.ru_utime.tv_usec) / 1000000;
    ret += usage.ru_utime.tv_sec;

    return ret;
}

DOUBLE GetMemory()
{
    struct mallinfo MALLINFO;
    MALLINFO = mallinfo();
    return (DOUBLE)(MALLINFO.usmblks + MALLINFO.uordblks) / 1048576;
}