#include "eigensolver.h"

#if defined(PASE_USE) && SLEPC_USE && PETSC_USE

// 用来存放本solver各类数据和参数的结构体，会保存在linearsolver->data中，随着destory而销毁
typedef struct PASE_DATA
{
    DATATYPE datatype;
    PASE_PARAMETER param;
    PASE_MG_SOLVER pase_solver;
    bool save_A_pre, save_B_pre;
} PASE_DATA;

static void Setup_PASE(EIGENSOLVER *solver)
{
    MULTILEVEL *ml = solver->multilevel;
    PASE_DATA *data = (PASE_DATA *)malloc(sizeof(PASE_DATA));
    data->datatype = ml->datatype;
    data->save_A_pre = false, data->save_B_pre = false;
    PASE_PARAMETER_Create(&(data->param), ml->num_levels, solver->nev, solver->tol, PASE_GMG);
    Mat *A_array = (Mat *)malloc(ml->num_levels * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(ml->num_levels * sizeof(Mat));
    Mat *P_array = (Mat *)malloc((ml->num_levels - 1) * sizeof(Mat));
    int i;
    for (i = 0; i < ml->num_levels; i++)
    {
        if (DataTypePetscEqual(ml->stiff_matrices[i]->type))
            A_array[i] = (Mat)(ml->stiff_matrices[i]->data_petsc);
        else
            RaiseError("Setup_PASE", "the type of stiffness matrix is not PETSc.");
        if (DataTypePetscEqual(ml->mass_matrices[i]->type))
            B_array[i] = (Mat)(ml->mass_matrices[i]->data_petsc);
        else
            RaiseError("Setup_PASE", "the type of mass matrix is not PETSc.");
    }
    for (i = 0; i < ml->num_levels - 1; i++)
    {
        if (DataTypePetscEqual(ml->prolongs[i]->type))
            P_array[i] = (Mat)(ml->prolongs[i]->data_petsc);
        else
            RaiseError("Setup_PASE", "the type of prolongation matrix is not PETSc.");
    }
    data->param->A_array = (void **)(A_array);
    data->param->B_array = (void **)(B_array);
    data->param->P_array = (void **)(P_array);
    data->param->aux_coarse_level = ml->num_levels - 1;
    data->param->initial_level = data->param->aux_coarse_level;
    data->param->aux_rtol = solver->tol;
    data->param->pc_type = PRECOND_A;
    data->param->max_direct_count = 40;
    data->param->max_cycle_count = 2;
    data->param->max_pre_count = 40;
    data->param->max_post_count = 40;
    data->param->if_error_estimate = false;
    data->pase_solver = NULL;

    solver->data = (void *)data;
}

static void Solve_PASE(EIGENSOLVER *solver)
{
    PASE_DATA *data = (PASE_DATA *)(solver->data);
    MULTILEVEL *ml = solver->multilevel;
    int level_num = data->param->num_levels;
    // OpenPFEM_Print("矩阵尺寸:%d\n", ml->stiff_matrices[level_num - 1]->global_nrows);
    if (level_num == 0)
    {
        RaiseError("Solve_PASE", "the number of level(s) cannot be zero.");
    }
    else if (level_num == 1)
    {
        /* gcge */
        EIGENSOLVER *GCGE_solver = NULL;
        EigenSolverCreate(&GCGE_solver, GCGE);
        EigenSolverSolve(GCGE_solver, ml->stiff_matrices[level_num - 1], ml->mass_matrices[level_num - 1], solver->evals, solver->evecs, solver->nev);
        EigenSolverDestroy(&GCGE_solver);
    }
    else
    {
        /* pase */
        OPS *gcge_ops;
        OPS_Create(&gcge_ops);
        OPS_SLEPC_Set(gcge_ops);
        OPS_Setup(gcge_ops);
        PASE_OPS *pase_ops;
        PASE_OPS_Create(&pase_ops, gcge_ops);
        PASE_MG_SOLVER pase_solver = PASE_Mg_solver_create(data->param, pase_ops);
        data->pase_solver = pase_solver;
        PASE_Mg_set_up(pase_solver, data->param);
        PASE_Mg_solve(pase_solver);

        double *origin = NULL, *aim = NULL;
        BVGetArray((BV)(pase_solver->solution[0]), &origin);
        VectorsGetArray(solver->evecs, &aim);
        memcpy(solver->evals, pase_solver->eigenvalues, solver->nev * sizeof(DOUBLE));
        memcpy(aim, origin, solver->nev * solver->evecs->local_length * sizeof(DOUBLE));
        BVRestoreArray((BV)(pase_solver->solution[0]), &origin);
        VectorsRestoreArray(solver->evecs, &aim);
    }
}

static void Destory_PASE(EIGENSOLVER *solver)
{
    PASE_DATA *data = (PASE_DATA *)(solver->data);
    if (data->param != NULL)
        PASE_PARAMETER_Destroy(&(data->param));
    if (data->pase_solver != NULL)
    {
        PASE_MG_SOLVER pase_solver = (PASE_MG_SOLVER)data->pase_solver;
        PASE_MULTIGRID_Destroy_Partial(&(pase_solver->multigrid));
        if (data->save_A_pre || data->save_B_pre)
            PASE_Mg_solver_destroy_wofactorization(data->pase_solver, data->save_A_pre, data->save_B_pre);
        else
            PASE_Mg_solver_destroy(data->pase_solver);
    }
    OpenPFEM_Free(data);
    solver->data = NULL;
}

void EigenSolverCreate_PASE(EIGENSOLVER *solver)
{
    EIGENSOLVER_OPS *ops = (EIGENSOLVER_OPS *)malloc(sizeof(EIGENSOLVER_OPS));
    ops->setup = Setup_PASE;
    ops->solve = Solve_PASE;
    ops->destory = Destory_PASE;
    solver->ops = (void *)ops;
}

/////////////////////////////////////////////////////////////////////////////
/*                                外部接口                                  */
/////////////////////////////////////////////////////////////////////////////

// 这里设置之后，pase会当成求nev个特征对去执行，但是当setup时设定的nev个收敛之后就会停止迭代
void EigenSolverSetPASENev(EIGENSOLVER *solver, int nev)
{
    if (!solver->alreadysetup)
        RaiseError("EigenSolverSetPASENev", "Have not set up!");

    if (SolverTypeEqual_PASE(solver->solvertype))
    {
        PASE_DATA *data = (PASE_DATA *)(solver->data);
        data->param->nev = nev;
    }
    else
    {
        RaiseError("EigenSolverSetPASENev", "SolverType should be PASE!");
    }
}

void EigenSolverSetPASEInitialSolution(EIGENSOLVER *solver, int level, VECTORS *solution)
{
    if (!solver->alreadysetup)
        RaiseError("EigenSolverSetPASEInitialSolution", "Have not set up!");

    if (SolverTypeEqual_PASE(solver->solvertype))
    {
        PASE_DATA *data = (PASE_DATA *)(solver->data);
        data->param->num_given_eigs = solution->nvecs;
        data->param->initial_level = data->param->num_levels - level - 1;
        data->param->initial_solution = solution->data_slepc;
        solution->data_slepc = NULL;
    }
    else
    {
        RaiseError("EigenSolverSetPASEInitialSolution", "SolverType should be PASE!");
    }
}

void EigenSolverPASESolve(EIGENSOLVER *solver, MATRIX *A, MATRIX *B, DOUBLE **eval, VECTORS **evec, INT *num)
{
    PASE_DATA *data = (PASE_DATA *)(solver->data);
    MULTILEVEL *ml = solver->multilevel;
    int level_num = data->param->num_levels;
    // OpenPFEM_Print("矩阵尺寸:%d\n", ml->stiff_matrices[level_num - 1]->global_nrows);
    if (level_num == 0)
    {
        RaiseError("Solve_PASE", "the number of level(s) cannot be zero.");
    }
    else if (level_num == 1)
    {
        /* gcge */
        EIGENSOLVER *GCGE_solver = NULL;
        EigenSolverCreate(&GCGE_solver, GCGE);
        *num = solver->nev;
        GCGE_solver->tol = 1e-10;
        EigenSolverGCGESolve(GCGE_solver, ml->stiff_matrices[level_num - 1], ml->mass_matrices[level_num - 1], eval, evec, num);
        EigenSolverDestroy(&GCGE_solver);
    }
    else
    {
        /* pase */
        OPS *gcge_ops;
        OPS_Create(&gcge_ops);
        OPS_SLEPC_Set(gcge_ops);
        OPS_Setup(gcge_ops);
        PASE_OPS *pase_ops;
        PASE_OPS_Create(&pase_ops, gcge_ops);
        PASE_MG_SOLVER pase_solver = PASE_Mg_solver_create(data->param, pase_ops);
        data->pase_solver = pase_solver;
        PASE_Mg_set_up(pase_solver, data->param);
        PASE_Mg_solve(pase_solver);

        int real_nev = pase_solver->current_nev_start + pase_solver->conv_nev;
        *num = real_nev;
        VectorsCreateByMatrix(evec, A, real_nev);
        VectorsSetRange(*evec, 0, real_nev);
        double *origin = NULL, *aim = NULL;
        BVGetArray((BV)(pase_solver->solution[0]), &origin);
        VectorsGetArray(*evec, &aim);
        *eval = (double *)malloc(real_nev * sizeof(double));
        memcpy(*eval, pase_solver->eigenvalues, real_nev * sizeof(DOUBLE));
        memcpy(aim, origin, real_nev * (*evec)->local_length * sizeof(DOUBLE));
        BVRestoreArray((BV)(pase_solver->solution[0]), &origin);
        VectorsRestoreArray(*evec, &aim);
    }
}

void EigenSolverGetPASEPreconditioner(EIGENSOLVER *solver, void **A_preconditioner, void **B_preconditioner)
{
    if (!solver->alreadysetup)
        RaiseError("EigenSolverGetPASEPreconditioner", "Have not set up!");
    if (SolverTypeEqual_PASE(solver->solvertype))
    {
        PASE_DATA *data = (PASE_DATA *)(solver->data);
        if (A_preconditioner != NULL)
        {
            data->save_A_pre = true;
            *A_preconditioner = data->pase_solver->aux_A->factorization;
        }
        if (B_preconditioner != NULL)
        {
            data->save_B_pre = true;
            *B_preconditioner = data->pase_solver->aux_B->factorization;
        }
    }
    else
    {
        RaiseError("EigenSolverGetPASEPreconditioner", "SolverType should be PASE!");
    }
}

void EigenSolverSetPASEPreconditioner(EIGENSOLVER *solver, int coarse_level,
                                      void *A_preconditioner, void *B_preconditioner)
{
    if (!solver->alreadysetup)
        RaiseError("EIgenSolverSetPASEPreconditioner", "Have not set up!");

    if (SolverTypeEqual_PASE(solver->solvertype))
    {
        PASE_DATA *data = (PASE_DATA *)(solver->data);
        MULTILEVEL *ml = solver->multilevel;
        if (coarse_level != ml->num_levels - 1 - data->param->aux_coarse_level)
            RaiseError("EIgenSolverSetPASEPreconditioner", "Wrong Coarse Level for PASE!");
        if (A_preconditioner)
            data->param->A_pre = A_preconditioner;
        if (B_preconditioner)
            data->param->B_pre = B_preconditioner;
    }
    else
    {
        RaiseError("EIgenSolverSetPASEPreconditioner", "SolverType should be PASE!");
    }
}

#endif