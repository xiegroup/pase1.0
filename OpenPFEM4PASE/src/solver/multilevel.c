#include "multilevel.h"

// 需不需要在结构体里保留特征值特征向量等数据
#define DEV_ML_ES 1

void MultiLevelCreate(MULTILEVEL **multilevel, DATATYPE type)
{
    *multilevel = (MULTILEVEL *)malloc(sizeof(MULTILEVEL));
    /* 设置 */
    (*multilevel)->datatype = type;
    /* 属性 */
    (*multilevel)->num_levels = 0;
    /* 记录 */
    (*multilevel)->femspace = NULL;
    /* 数据 */
    (*multilevel)->stiff_matrices = NULL;
    (*multilevel)->mass_matrices = NULL;
    (*multilevel)->prolongs = NULL;
    (*multilevel)->restricts = NULL;
    (*multilevel)->solution = NULL;
    (*multilevel)->rhs = NULL;
    (*multilevel)->num_eig = 0;
    (*multilevel)->eigvalues = NULL;
    (*multilevel)->eigvecs = NULL;
}

void MultiLevelDestroy(MULTILEVEL **multilevel)
{
    (*multilevel)->femspace = NULL;
    INT i;
    for (i = 0; i < (*multilevel)->num_levels; i++)
    {
        if ((*multilevel)->stiff_matrices != NULL && (*multilevel)->stiff_matrices[i] != NULL)
            MatrixDestroy((*multilevel)->stiff_matrices + i);
        if ((*multilevel)->mass_matrices != NULL && (*multilevel)->mass_matrices[i] != NULL)
            MatrixDestroy((*multilevel)->mass_matrices + i);
        if ((*multilevel)->solution != NULL && (*multilevel)->solution[i] != NULL)
            VectorDestroy((*multilevel)->solution + i);
        if ((*multilevel)->rhs != NULL && (*multilevel)->rhs[i] != NULL)
            VectorDestroy((*multilevel)->rhs + i);
        if ((*multilevel)->eigvecs != NULL && (*multilevel)->eigvecs[i] != NULL)
            VectorsDestroy((*multilevel)->eigvecs + i);
    }
    OpenPFEM_Free((*multilevel)->stiff_matrices);
    OpenPFEM_Free((*multilevel)->mass_matrices);
    OpenPFEM_Free((*multilevel)->solution);
    OpenPFEM_Free((*multilevel)->rhs);
    OpenPFEM_Free((*multilevel)->eigvecs);
    for (i = 0; i < (*multilevel)->num_levels - 1; i++)
    {
        if ((*multilevel)->prolongs != NULL && (*multilevel)->prolongs[i] != NULL)
            MatrixDestroy((*multilevel)->prolongs + i);
        if ((*multilevel)->restricts != NULL && (*multilevel)->restricts[i] != NULL)
            MatrixDestroy((*multilevel)->restricts + i);
    }
    OpenPFEM_Free((*multilevel)->prolongs);
    OpenPFEM_Free((*multilevel)->restricts);
    OpenPFEM_Free((*multilevel)->eigvalues);
    OpenPFEM_Free((*multilevel));
}

void MultiLevelOutput(MULTILEVEL *multilevel, const char *name)
{
    INT idx_level;
    for (idx_level = 0; idx_level < multilevel->num_levels; idx_level++)
    {
        char filename[128], level[20], end[20], type[20];
        strcpy(filename, name);
        strcpy(type, "_A");
        strcpy(end, ".bin");
        sprintf(level, "%d", idx_level);
        strcat(filename, type);
        strcat(filename, level);
        strcat(filename, end);
        MatrixOutput(multilevel->stiff_matrices[idx_level], filename);
    }
    if (multilevel->mass_matrices != NULL)
    {
        for (idx_level = 0; idx_level < multilevel->num_levels; idx_level++)
        {
            char filename[128], level[20], end[20], type[20];
            strcpy(filename, name);
            strcpy(type, "_B");
            strcpy(end, ".bin");
            sprintf(level, "%d", idx_level);
            strcat(filename, type);
            strcat(filename, level);
            strcat(filename, end);
            MatrixOutput(multilevel->mass_matrices[idx_level], filename);
        }
    }
    if (multilevel->rhs != NULL)
    {
        for (idx_level = 0; idx_level < multilevel->num_levels; idx_level++)
        {
            char filename[128], level[20], end[20], type[20];
            strcpy(filename, name);
            strcpy(type, "_b");
            strcpy(end, ".bin");
            sprintf(level, "%d", idx_level);
            strcat(filename, type);
            strcat(filename, level);
            strcat(filename, end);
            VectorOutput(multilevel->rhs[idx_level], filename);
        }
    }
    for (idx_level = 0; idx_level < multilevel->num_levels - 1; idx_level++)
    {
        char filename[128], level[20], end[20], type[20];
        strcpy(filename, name);
        strcpy(type, "_P");
        strcpy(end, ".bin");
        sprintf(level, "%d", idx_level);
        strcat(filename, type);
        strcat(filename, level);
        strcat(filename, end);
        MatrixOutput(multilevel->prolongs[idx_level], filename);
    }
}

void MultiLevelAddLevel(MULTILEVEL *multilevel, DISCRETEFORM *stiff_discreteform, DISCRETEFORM *mass_discreteform)
{
    DATATYPE type = multilevel->datatype;
    if (multilevel->num_levels == 0)
    {
        if (stiff_discreteform != NULL)
        {
            multilevel->stiff_matrices = (MATRIX **)malloc(sizeof(MATRIX *));
            multilevel->stiff_matrices[0] = NULL;
            if (mass_discreteform == NULL)
            {
                multilevel->mass_matrices = (MATRIX **)malloc(sizeof(MATRIX *));
                multilevel->mass_matrices[0] = NULL;
                multilevel->rhs = (VECTOR **)malloc(sizeof(VECTOR *));
                multilevel->rhs[0] = NULL;
                multilevel->solution = (VECTOR **)malloc(sizeof(VECTOR *));
                multilevel->solution[0] = NULL;
                MatrixAssemble(&(multilevel->stiff_matrices[0]), &(multilevel->rhs[0]), stiff_discreteform, type);
                VectorCreateByMatrix(&(multilevel->solution[0]), multilevel->stiff_matrices[0]);
            }
            else
            {
                MatrixAssemble(&(multilevel->stiff_matrices[0]), NULL, stiff_discreteform, type);
            }
        }
        if (mass_discreteform != NULL)
        {
#if DEV_ML_ES == 0
            if (multilevel->num_eig <= 0)
                RaiseError("MultiLevelAddLevel", "Please set num_eig > 0 for eigenvalue problem!");
#endif
            multilevel->mass_matrices = (MATRIX **)malloc(sizeof(MATRIX *));
            multilevel->mass_matrices[0] = NULL;
            MatrixAssemble(&(multilevel->mass_matrices[0]), NULL, mass_discreteform, type);
#if DEV_ML_ES == 0
            multilevel->eigvalues = (DOUBLE **)malloc(multilevel->num_eig * sizeof(DOUBLE *));
            multilevel->eigvecs = (VECTORS **)malloc(sizeof(VECTORS *));
            multilevel->eigvecs[0] = NULL;
            VectorsCreateByMatrix(&(multilevel->eigvecs[0]), multilevel->mass_matrices[0], multilevel->num_eig);
#endif
        }
    }
    else if (multilevel->num_levels == 1)
    {
        multilevel->prolongs = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->prolongs[0] = NULL;
        multilevel->restricts = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->restricts[0] = NULL;
        ProlongMatrixAssemble(&(multilevel->prolongs[0]), multilevel->femspace, stiff_discreteform->RightSpace, type);
        if (stiff_discreteform != NULL)
        {
            multilevel->stiff_matrices = realloc(multilevel->stiff_matrices, 2 * sizeof(MATRIX *));
            multilevel->stiff_matrices[1] = NULL;
            if (mass_discreteform == NULL)
            {
                multilevel->mass_matrices = realloc(multilevel->mass_matrices, 2 * sizeof(MATRIX *));
                multilevel->mass_matrices[1] = NULL;
                multilevel->rhs = realloc(multilevel->rhs, 2 * sizeof(VECTOR *));
                multilevel->rhs[1] = NULL;
                multilevel->solution = realloc(multilevel->solution, 2 * sizeof(VECTOR *));
                multilevel->solution[1] = NULL;
                MatrixAssemble(&(multilevel->stiff_matrices[1]), &(multilevel->rhs[1]), stiff_discreteform, type);
                VectorCreateByMatrix(&(multilevel->solution[1]), multilevel->stiff_matrices[1]);
            }
            else
            {
                MatrixAssemble(&(multilevel->stiff_matrices[1]), NULL, stiff_discreteform, type);
            }
        }
        if (mass_discreteform != NULL)
        {
            multilevel->mass_matrices = realloc(multilevel->mass_matrices, 2 * sizeof(MATRIX *));
            multilevel->mass_matrices[1] = NULL;
            MatrixAssemble(&(multilevel->mass_matrices[1]), NULL, mass_discreteform, type);
#if DEV_ML_ES == 0
            multilevel->eigvecs = realloc(multilevel->eigvecs, 2 * sizeof(VECTORS *));
            multilevel->eigvecs[1] = NULL;
            VectorsCreateByMatrix(&(multilevel->eigvecs[1]), multilevel->mass_matrices[1], multilevel->num_eig);
#endif
        }
    }
    else
    {
        INT num_levels = multilevel->num_levels;
        multilevel->prolongs = realloc(multilevel->prolongs, num_levels * sizeof(MATRIX *));
        multilevel->prolongs[num_levels - 1] = NULL;
        multilevel->restricts = realloc(multilevel->restricts, num_levels * sizeof(MATRIX *));
        multilevel->restricts[num_levels - 1] = NULL;
        ProlongMatrixAssemble(&(multilevel->prolongs[num_levels - 1]), multilevel->femspace, stiff_discreteform->RightSpace, type);
        if (stiff_discreteform != NULL)
        {
            multilevel->stiff_matrices = realloc(multilevel->stiff_matrices, (num_levels + 1) * sizeof(MATRIX *));
            multilevel->stiff_matrices[num_levels] = NULL;
            if (mass_discreteform == NULL)
            {
                multilevel->mass_matrices = realloc(multilevel->mass_matrices, (num_levels + 1) * sizeof(MATRIX *));
                multilevel->mass_matrices[num_levels] = NULL;
                multilevel->rhs = realloc(multilevel->rhs, (num_levels + 1) * sizeof(VECTOR *));
                multilevel->rhs[num_levels] = NULL;
                multilevel->solution = realloc(multilevel->solution, (num_levels + 1) * sizeof(VECTOR *));
                multilevel->solution[num_levels] = NULL;
                MatrixAssemble(&(multilevel->stiff_matrices[num_levels]), &(multilevel->rhs[num_levels]), stiff_discreteform, type);
                VectorCreateByMatrix(&(multilevel->solution[num_levels]), multilevel->stiff_matrices[num_levels]);
            }
            else
            {
                MatrixAssemble(&(multilevel->stiff_matrices[num_levels]), NULL, stiff_discreteform, type);
            }
        }
        if (mass_discreteform != NULL)
        {
            multilevel->mass_matrices = realloc(multilevel->mass_matrices, (num_levels + 1) * sizeof(MATRIX *));
            multilevel->mass_matrices[num_levels] = NULL;
            MatrixAssemble(&(multilevel->mass_matrices[num_levels]), NULL, mass_discreteform, type);
#if DEV_ML_ES == 0
            multilevel->eigvecs = realloc(multilevel->eigvecs, (num_levels + 1) * sizeof(VECTORS *));
            multilevel->eigvecs[num_levels] = NULL;
            VectorsCreateByMatrix(&(multilevel->eigvecs[num_levels]), multilevel->mass_matrices[num_levels], multilevel->num_eig);
#endif
        }
    }
    (multilevel->num_levels)++;
    multilevel->femspace = stiff_discreteform->RightSpace;
}

void MultiLevelAddLevelSimple(MULTILEVEL *multilevel, FEMSPACE *newspace)
{
    DATATYPE type = multilevel->datatype;
    if (multilevel->num_levels == 0)
    {
        multilevel->stiff_matrices = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->stiff_matrices[0] = NULL;
        multilevel->mass_matrices = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->mass_matrices[0] = NULL;
        multilevel->rhs = (VECTOR **)malloc(sizeof(VECTOR *));
        multilevel->rhs[0] = NULL;
        multilevel->solution = (VECTOR **)malloc(sizeof(VECTOR *));
        multilevel->solution[0] = NULL;
#if DEV_ML_ES == 0
        multilevel->eigvalues = (DOUBLE **)malloc(multilevel->num_eig * sizeof(DOUBLE *));
        multilevel->eigvecs = (VECTORS **)malloc(sizeof(VECTORS *));
        multilevel->eigvecs[0] = NULL;
#endif
    }
    else if (multilevel->num_levels == 1)
    {
        multilevel->prolongs = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->prolongs[0] = NULL;
        multilevel->restricts = (MATRIX **)malloc(sizeof(MATRIX *));
        multilevel->restricts[0] = NULL;
        ProlongMatrixAssemble(&(multilevel->prolongs[0]), multilevel->femspace, newspace, type);

        multilevel->stiff_matrices = realloc(multilevel->stiff_matrices, 2 * sizeof(MATRIX *));
        multilevel->stiff_matrices[1] = NULL;
        multilevel->mass_matrices = realloc(multilevel->mass_matrices, 2 * sizeof(MATRIX *));
        multilevel->mass_matrices[1] = NULL;
        multilevel->rhs = realloc(multilevel->rhs, 2 * sizeof(VECTOR *));
        multilevel->rhs[1] = NULL;
        multilevel->solution = realloc(multilevel->solution, 2 * sizeof(VECTOR *));
        multilevel->solution[1] = NULL;
#if DEV_ML_ES == 0
        multilevel->eigvecs = realloc(multilevel->eigvecs, 2 * sizeof(VECTORS *));
        multilevel->eigvecs[1] = NULL;
#endif
    }
    else
    {
        INT num_levels = multilevel->num_levels;
        multilevel->prolongs = realloc(multilevel->prolongs, num_levels * sizeof(MATRIX *));
        multilevel->prolongs[num_levels - 1] = NULL;
        multilevel->restricts = realloc(multilevel->restricts, num_levels * sizeof(MATRIX *));
        multilevel->restricts[num_levels - 1] = NULL;
        ProlongMatrixAssemble(&(multilevel->prolongs[num_levels - 1]), multilevel->femspace, newspace, type);

        multilevel->stiff_matrices = realloc(multilevel->stiff_matrices, (num_levels + 1) * sizeof(MATRIX *));
        multilevel->stiff_matrices[num_levels] = NULL;
        multilevel->mass_matrices = realloc(multilevel->mass_matrices, (num_levels + 1) * sizeof(MATRIX *));
        multilevel->mass_matrices[num_levels] = NULL;
        multilevel->rhs = realloc(multilevel->rhs, (num_levels + 1) * sizeof(VECTOR *));
        multilevel->rhs[num_levels] = NULL;
        multilevel->solution = realloc(multilevel->solution, (num_levels + 1) * sizeof(VECTOR *));
        multilevel->solution[num_levels] = NULL;
#if DEV_ML_ES == 0
        multilevel->eigvecs = realloc(multilevel->eigvecs, (num_levels + 1) * sizeof(VECTORS *));
        multilevel->eigvecs[num_levels] = NULL;
#endif
    }
    (multilevel->num_levels)++;
    multilevel->femspace = newspace;
}

void MultiLevelSetMatrix(MULTILEVEL *multilevel, MATRIX *stiff_matrix, MATRIX *mass_matrix, INT target_level)
{
    INT num_levels = multilevel->num_levels;
    if (num_levels <= target_level)
    {
        RaiseError("MultiLevelSetMatrix", "target level is too large.");
    }
    if (stiff_matrix != NULL)
        multilevel->stiff_matrices[target_level] = stiff_matrix;
    if (mass_matrix != NULL)
        multilevel->mass_matrices[target_level] = mass_matrix;
}

void MultiLevelSetVector(MULTILEVEL *multilevel, VECTOR *rhs, VECTOR *solution, INT target_level)
{
    INT num_levels = multilevel->num_levels;
    if (num_levels <= target_level)
    {
        RaiseError("MultiLevelSetMatrix", "target level is too large.");
    }
    if (solution != NULL)
        multilevel->solution[target_level] = solution;
    if (rhs != NULL)
        multilevel->rhs[target_level] = rhs;
}

void MultiLevelSetNev(MULTILEVEL *multilevel, INT nev)
{
    multilevel->num_eig = nev;
}