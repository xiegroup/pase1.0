WORLDDIM: 3
number of vertices: 8
number of elements: 6
  
vertex coordinates:
 -4.0 -4.0 -4.0
 4.0 -4.0 -4.0
 4.0 4.0 -4.0
 -4.0 4.0 -4.0
 -4.0 -4.0 4.0
 4.0 -4.0 4.0
 4.0 4.0 4.0
 -4.0 4.0 4.0
   
element vertices:
 1 7 3 0
 1 7 0 4
 1 7 4 5
 1 7 2 3
 1 7 6 2
 1 7 5 6
   
element boundaries:
 1 1 0 0
 1 1 0 0
 1 1 0 0
 1 1 0 0
 1 1 0 0
 1 1 0 0