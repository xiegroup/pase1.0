WORLDDIM: 3
number of vertices: 8
number of elements: 5
  
vertex coordinates:
 0.0 0.0 0.0
 1.0 0.0 0.0
 0.0 0.0 1.0
 1.0 0.0 1.0
 1.0 1.0 0.0
 1.0 1.0 1.0
 0.0 1.0 0.0
 0.0 1.0 1.0
   
element vertices:
 5 6 2 7
 5 1 3 2
 1 6 0 2
 1 6 5 4
 1 6 2 5
   
element boundaries:
 1 1 1 0
 1 1 0 1
 1 1 0 1
 1 1 1 0
 0 0 0 0