CC       = mpicc
CFLAGS   = -O1 -g -fcommon
MPIEXEC  = mpirun

RM = rm -rf

PETSCINC = -I${PETSC_DIR}/include -I${PETSC_DIR}/${PETSC_ARCH}/include -I${PETSC_DIR}/include/petsc/private -I${PETSC_DIR}/src/mat/impls/aij/mpi
LIBPETSC = -L${PETSC_DIR}/${PETSC_ARCH}/lib -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib -lpetsc -lsuperlu -lflapack -lfblas -lpthread -lm -lstdc++ -ldl -lmpifort -lmpi -lgfortran -lgcc_s -lquadmath 

SLEPCINC = -I${SLEPC_DIR}/include -I${SLEPC_DIR}/${PETSC_ARCH}/include -I${SLEPC_DIR}/include/slepc/private
LIBSLEPC = -L${SLEPC_DIR}/${PETSC_ARCH}/lib -Wl,-rpath,${SLEPC_DIR}/${PETSC_ARCH}/lib -lslepc

METISINC = -I${METIS_DIR}/include -I${METIS_DIR}/metis/include
LIBMETIS = -L${METIS_DIR}/${METIS_LIB} -Wl,-rpath,${METIS_DIR}/${METIS_LIB} -lmetis -L${METIS_DIR}/${PARMETIS_LIB} -Wl,-rpath,${METIS_DIR}/${PARMETIS_LIB} -lparmetis -lmetis -lm

.SUFFIXES: .c.o
vpath %.h ../include
vpath %.c ../test
SOURCE = $(wildcard ../src/*.c) $(wildcard ../src/mesh/*.c) $(wildcard ../src/fem/*.c) $(wildcard ../src/algebra/*.c) $(wildcard ../src/solver/eigensolver/*.c) $(wildcard ../src/solver/linearsolver/*.c) $(wildcard ../src/solver/*.c) 
OBJECTS = $(SOURCE:.c=.o)