/*
 * =====================================================================================
 *
 *       Filename:  D_T_P0_1D.h
 *
 *    Description:  define the base functions in 1D case
 *
 *        Version:  1.0
 *        Created:  2021/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   hhxie@lsec.cc.ac.cn
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __CLP11D__
#define __CLP11D__

#include <stdbool.h>
#include "enumerations.h"
#include "constants.h"
#include<stdio.h>
#include<stdlib.h>
// ***********************************************************************
// P1 element, conforming, 1D
// ***********************************************************************
static int C_L_P1_1D_dof[2] = {1,0}; //所有的自由度都是在单元内部
static int C_L_P1_1D_Num_Bas = 2;
static int C_L_P1_1D_Value_Dim = 1;
static int C_L_P1_1D_Inter_Dim = 1;
static int C_L_P1_1D_Polydeg = 1;
static bool C_L_P1_1D_IsOnlyDependOnRefCoord = 1;
static int C_L_P1_1D_Accuracy = 1;
static MAPTYPE C_L_P1_1D_Maptype = Affine;

static void C_L_P1_1D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{ 
  values[0]= 1.0 - RefCoord[0];
  values[1]= RefCoord[0];
}

// base function values
static void C_L_P1_1D_D0(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{ 
    values[0]= 1.0 - RefCoord[0];
    values[1]= RefCoord[0];
}

// values of the derivatives in xi direction
static void C_L_P1_1D_D1(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
    values[0]= -1.0;
    values[1]= 1.0;
}
// values of the derivatives in xi-xi  direction
static void C_L_P1_1D_D2(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]=0.0;
  values[1]=0.0;
}
// The Nodal functional definition for the interpolation
static void C_L_P1_1D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  double coord[1];
  int i;
  for(i=0;i<2;i++)
  {
    coord[0] = elem->Vert_X[i]; 
    fun(coord, dim, values+i*dim);
  }
}
#endif