/*
 * =====================================================================================
 *
 *       Filename:  D_T_P0_1D.h
 *
 *    Description:  define the base functions in 1D case
 *
 *        Version:  1.0
 *        Created:  2021/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   hhxie@lsec.cc.ac.cn
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __DLP01D__
#define __DLP01D__

#include <stdbool.h>
#include "enumerations.h"
#include "constants.h"
#include<stdio.h>
#include<stdlib.h>
// ***********************************************************************
// P1 element, discontinuouis, 1D
// ***********************************************************************
static int D_L_P0_1D_dof[2] = {0,1}; //所有的自由度都是在单元内部
static int D_L_P0_1D_Num_Bas = 1;
static int D_L_P0_1D_Value_Dim = 1;
static int D_L_P0_1D_Inter_Dim = 1;
static int D_L_P0_1D_Polydeg = 0;
static bool D_L_P0_1D_IsOnlyDependOnRefCoord = 1;
static int D_L_P0_1D_Accuracy = 1;
static MAPTYPE D_L_P0_1D_Maptype = Affine;

static void D_L_P0_1D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{ 
  values[0]=1.0;
}

// base function values
static void D_L_P0_1D_D0(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{ 
  values[0]=1.0;
}

// values of the derivatives in xi direction
static void D_L_P0_1D_D1(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]= 0.0;
}
// values of the derivatives in xi-xi  direction
static void D_L_P0_1D_D2(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0]=0.0;
}
// The Nodal functional definition for the interpolation
static void D_L_P0_1D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  double coord[1];
  int i;
  coord[0] = 0.0;
  for(i=0;i<2;i++)
  {
      coord[0] += elem->Vert_X[i]; 
  }
  coord[0] *= 1.0/2.0;
  fun(coord, dim, values);
}
#endif