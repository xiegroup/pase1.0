/*
 * =====================================================================================
 *
 *       Filename:  C_T_P1_2D_2D.h
 *
 *    Description:  define the base functions in 2D case
 *
 *        Version:  1.0
 *        Created:  2012/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   (), @lsec.cc.ac.cn
 *        Company:  
 *
 *
 * =====================================================================================
 */
#ifndef __CTP12D2D__
#define __CTP12D2D__

#include <stdbool.h>
#include "enumerations.h"
#include "constants.h"
#include<stdio.h>
#include<stdlib.h>
// ***********************************************************************
// (P1)^2 element, conforming, 2D
// ***********************************************************************
static int C_T_P1_2D_2D_dof[3] = {2,0,0}; 
static int C_T_P1_2D_2D_Num_Bas = 6;
static int C_T_P1_2D_2D_Value_Dim = 2;
static int C_T_P1_2D_2D_Inter_Dim = 2;
static int C_T_P1_2D_2D_Polydeg = 1;
static bool C_T_P1_2D_2D_IsOnlyDependOnRefCoord = 0;
static int C_T_P1_2D_2D_Accuracy = 1;
static MAPTYPE C_T_P1_2D_2D_Maptype = Affine;

static void C_T_P1_2D_2D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{ 
  //printf("Come to the base D00\n");
  double xi = RefCoord[0];
  double eta = RefCoord[1]; 
  //printf("get the ref coord!\n");
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  for(i=0;i<2;i++)
  {
  values[4*0+i*3]=1-xi-eta;
  values[4*1+i*3]=xi;
  values[4*2+i*3]=eta;
  //printf("use the P1 interfunction!\n");
  //printf("values = [%2.8f,  %2.8f,  %2.8f], End of base values!\n",values[4*0], values[4*1], values[4*2]);
  }
}

// base function values
static void C_T_P1_2D_2D_D00(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  //printf("Come to the base D00\n");
  double xi = RefCoord[0];
  double eta = RefCoord[1]; 
  //printf("get the ref coord!\n");
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  for(i=0;i<2;i++)
  {
  values[4*0+i*3]=1-xi-eta;
  values[4*1+i*3]=xi;
  values[4*2+i*3]=eta;
  //printf("values = [%2.8f,  %2.8f,  %2.8f], End of base values!\n",values[4*0], values[4*1], values[4*2]);
  }
}

// values of the derivatives in xi direction
static void C_T_P1_2D_2D_D10(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  for(i=0;i<2;i++)
  {
  values[4*0+i*3]=-1;
  values[4*1+i*3]= 1;
  values[4*2+i*3]= 0;  
  }
}

// values of the derivatives in eta direction
static void C_T_P1_2D_2D_D01(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  for(i=0;i<2;i++)
  {
    values[4*0+i*3]=-1;
    values[4*1+i*3]= 0;
    values[4*2+i*3]= 1;   
  }
}
// values of the derivatives in xi-xi  direction
static void C_T_P1_2D_2D_D20(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  // for(i=0;i<2;i++)
  // {
  // values[4*0+i*3]=0;
  // values[4*1+i*3]=0;
  // values[4*2+i*3]=0;
  // }
}
// values of the derivatives in xi-eta direction
static void C_T_P1_2D_2D_D11(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  // for(i=0;i<2;i++)
  // {
  //   values[4*0+i*3]=0;
  //   values[4*1+i*3]=0;
  //   values[4*2+i*3]=0;
  // }
}
// values of the derivatives in eta-eta direction
static void C_T_P1_2D_2D_D02(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<12;i++)   values[i] = 0.0; 
  // for(i=0;i<2;i++)
  // {
  //   values[4*0+i*3]=0;
  //   values[4*1+i*3]=0;
  //   values[4*2+i*3]=0;
  // }
}

static void C_T_P1_2D_2D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  //int dim = 1;  
  //printf("DO the interpolation\n");
  double coord[2];
  int i;
  for(i=0;i<3;i++)
  {
      coord[0] = elem->Vert_X[i]; 
      coord[1] = elem->Vert_Y[i];
      fun(coord, dim, values+i*dim);
  }
}
#endif