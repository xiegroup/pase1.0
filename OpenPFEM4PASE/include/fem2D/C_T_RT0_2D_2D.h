/*
 * =====================================================================================
 *
 *       Filename:  C_T_RT0_2D_2D.h
 *
 *    Description:  define the base functions in 2D case
 *
 *        Version:  1.0
 *        Created:  2012/04/03 03时02分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:   (), @lsec.cc.ac.cn
 *        Company:  
 *
 *
 * =====================================================================================
 */
#ifndef __CTRT02D2D__
#define __CTRT02D2D__

#include <stdbool.h>
#include "enumerations.h"
#include "constants.h"
#include<stdio.h>
#include<stdlib.h>
// ***********************************************************************
// (RT0)^2 element, conforming, 2D
// ***********************************************************************
static int C_T_RT0_2D_2D_dof[3] = {0,1,0}; 
static int C_T_RT0_2D_2D_Num_Bas = 3;
static int C_T_RT0_2D_2D_Value_Dim = 2;
static int C_T_RT0_2D_2D_Inter_Dim = 2;
static int C_T_RT0_2D_2D_Polydeg = 1;
static bool C_T_RT0_2D_2D_IsOnlyDependOnRefCoord = 0;
static int C_T_RT0_2D_2D_Accuracy = 1;
static MAPTYPE C_T_RT0_2D_2D_Maptype = Div;

static void C_T_RT0_2D_2D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{ 
  values[0] = RefCoord[0];
  values[1] = RefCoord[1];
  values[2] = RefCoord[0] - 1.0;
  values[3] = RefCoord[1];
  values[4] = RefCoord[0];
  values[5] = RefCoord[1] - 1.0;
}

// base function values
static void C_T_RT0_2D_2D_D00(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0] = RefCoord[0];
  values[1] = RefCoord[1];
  values[2] = RefCoord[0] - 1.0;
  values[3] = RefCoord[1];
  values[4] = RefCoord[0];
  values[5] = RefCoord[1] - 1.0;
}

// values of the derivatives in xi direction
static void C_T_RT0_2D_2D_D10(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0] = 1.0;
  values[1] = 0.0;
  values[2] = 1.0;
  values[3] = 0.0;
  values[4] = 1.0;
  values[5] = 0.0;
}

// values of the derivatives in eta direction
static void C_T_RT0_2D_2D_D01(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  values[0] = 0.0;
  values[1] = 1.0;
  values[2] = 0.0;
  values[3] = 1.0;
  values[4] = 0.0;
  values[5] = 1.0;
}
// values of the derivatives in xi-xi  direction
static void C_T_RT0_2D_2D_D20(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<6;i++)   values[i] = 0.0; 
}
// values of the derivatives in xi-eta direction
static void C_T_RT0_2D_2D_D11(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<6;i++)   values[i] = 0.0; 
}
// values of the derivatives in eta-eta direction
static void C_T_RT0_2D_2D_D02(ELEMENT *Elem, double Coord[], double RefCoord[], double *values)
{
  INT i; 
  for(i=0;i<6;i++)   values[i] = 0.0; 
}

static void C_T_RT0_2D_2D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
  //int dim = 1;  
  //printf("DO the interpolation\n");
  double coord[2];
  int i;
  for(i=0;i<3;i++)
  {
      coord[0] = elem->Vert_X[i]; 
      coord[1] = elem->Vert_Y[i];
      fun(coord, dim, values+i*dim);
  }
}
#endif