#ifndef __CURLTP43D3D__
#define __CURLTP43D3D__
#include <stdbool.h>
#include "mesh.h"
#include "enumerations.h"
#include "constants.h"
// ***********************************************************************
// Hcurl P4 element, CURL CONFORMING EDGE ELEMENTS OF NÉDÉLEC, 3D
// ***********************************************************************
static INT Curl_T_P4_3D_3D_dof[4] = {0, 4, 12, 12};
static INT Curl_T_P4_3D_3D_Num_Bas = 84;
static INT Curl_T_P4_3D_3D_Value_Dim = 3;
static INT Curl_T_P4_3D_3D_Inter_Dim = 3; // 现在Piola变换的基函数自由度排布时没有用到这个参数
static INT Curl_T_P4_3D_3D_Polydeg = 4;
static bool Curl_T_P4_3D_3D_IsOnlyDependOnRefCoord = 0;
static INT Curl_T_P4_3D_3D_Accuracy = 4;
static MAPTYPE Curl_T_P4_3D_3D_Maptype = Piola;

static void Curl_T_P4_3D_3D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 672*x*x*y*y - 160*y - 160*z - 120*x + 672*x*x*z*z + 1344*y*y*z*z + 720*x*y + 720*x*z + 960*y*z - 1260*x*y*y - 840*x*x*y + 672*x*y*y*y + 224*x*x*x*y - 1260*x*z*z - 840*x*x*z + 672*x*z*z*z + 224*x*x*x*z - 1680*y*z*z - 1680*y*y*z + 896*y*z*z*z + 896*y*y*y*z + 240*x*x - 140*x*x*x + 480*y*y - 560*y*y*y + 224*y*y*y*y + 480*z*z - 560*z*z*z + 224*z*z*z*z + 2016*x*y*z*z + 2016*x*y*y*z + 1344*x*x*y*z - 2520*x*y*z + 16;
   values[1] = 40*x - 672*x*x*y*y - 672*x*x*z*z - 240*x*y - 240*x*z + 420*x*y*y + 840*x*x*y - 224*x*y*y*y - 672*x*x*x*y + 420*x*z*z + 840*x*x*z - 224*x*z*z*z - 672*x*x*x*z - 240*x*x + 420*x*x*x - 224*x*x*x*x - 672*x*y*z*z - 672*x*y*y*z - 1344*x*x*y*z + 840*x*y*z;
   values[2] = 40*x - 672*x*x*y*y - 672*x*x*z*z - 240*x*y - 240*x*z + 420*x*y*y + 840*x*x*y - 224*x*y*y*y - 672*x*x*x*y + 420*x*z*z + 840*x*x*z - 224*x*z*z*z - 672*x*x*x*z - 240*x*x + 420*x*x*x - 224*x*x*x*x - 672*x*y*z*z - 672*x*y*y*z - 1344*x*x*y*z + 840*x*y*z;
   values[3] = 1200*x + 840*y + 840*z - 6048*x*x*y*y - 6048*x*x*z*z - 4032*y*y*z*z - 5760*x*y - 5760*x*z - 3960*y*z + 8568*x*y*y + 8316*x*x*y - 4032*x*y*y*y - 2688*x*x*x*y + 8568*x*z*z + 8316*x*x*z - 4032*x*z*z*z - 2688*x*x*x*z + 5796*y*z*z + 5796*y*y*z - 2688*y*z*z*z - 2688*y*y*y*z - 2700*x*x + 1680*x*x*x - 1980*y*y + 1932*y*y*y - 672*y*y*y*y - 1980*z*z + 1932*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[4] = 4032*x*x*y*y - 360*x + 4032*x*x*z*z + 1440*x*y + 1440*x*z - 1764*x*y*y - 6552*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 1764*x*z*z - 6552*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 2520*x*x - 4788*x*x*x + 2688*x*x*x*x + 2016*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 3528*x*y*z;
   values[5] = 4032*x*x*y*y - 360*x + 4032*x*x*z*z + 1440*x*y + 1440*x*z - 1764*x*y*y - 6552*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 1764*x*z*z - 6552*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 2520*x*x - 4788*x*x*x + 2688*x*x*x*x + 2016*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 3528*x*y*z;
   values[6] = 12096*x*x*y*y - 1320*y - 1320*z - 2700*x + 12096*x*x*z*z + 4032*y*y*z*z + 11016*x*y + 11016*x*z + 5184*y*z - 14364*x*y*y - 18144*x*x*y + 6048*x*y*y*y + 6720*x*x*x*y - 14364*x*z*z - 18144*x*x*z + 6048*x*z*z*z + 6720*x*x*x*z - 6552*y*z*z - 6552*y*y*z + 2688*y*z*z*z + 2688*y*y*y*z + 6480*x*x - 4200*x*x*x + 2592*y*y - 2184*y*y*y + 672*y*y*y*y + 2592*z*z - 2184*z*z*z + 672*z*z*z*z + 18144*x*y*z*z + 18144*x*y*y*z + 24192*x*x*y*z - 28728*x*y*z + 240;
   values[7] = 780*x - 6048*x*x*y*y - 6048*x*x*z*z - 2376*x*y - 2376*x*z + 2268*x*y*y + 12096*x*x*y - 672*x*y*y*y - 12096*x*x*x*y + 2268*x*z*z + 12096*x*x*z - 672*x*z*z*z - 12096*x*x*x*z - 5832*x*x + 11592*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 4536*x*y*z;
   values[8] = 780*x - 6048*x*x*y*y - 6048*x*x*z*z - 2376*x*y - 2376*x*z + 2268*x*y*y + 12096*x*x*y - 672*x*y*y*y - 12096*x*x*x*y + 2268*x*z*z + 12096*x*x*z - 672*x*z*z*z - 12096*x*x*x*z - 5832*x*x + 11592*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 4536*x*y*z;
   values[9] = 1680*x + 644*y + 644*z - 6720*x*x*y*y - 6720*x*x*z*z - 1344*y*y*z*z - 6048*x*y - 6048*x*z - 2184*y*z + 7056*x*y*y + 10920*x*x*y - 2688*x*y*y*y - 4480*x*x*x*y + 7056*x*z*z + 10920*x*x*z - 2688*x*z*z*z - 4480*x*x*x*z + 2436*y*z*z + 2436*y*y*z - 896*y*z*z*z - 896*y*y*y*z - 4200*x*x + 2800*x*x*x - 1092*y*y + 812*y*y*y - 224*y*y*y*y - 1092*z*z + 812*z*z*z - 224*z*z*z*z - 8064*x*y*z*z - 8064*x*y*y*z - 13440*x*x*y*z + 14112*x*y*z - 140;
   values[10] = 2688*x*x*y*y - 476*x + 2688*x*x*z*z + 1176*x*y + 1176*x*z - 924*x*y*y - 6384*x*x*y + 224*x*y*y*y + 6720*x*x*x*y - 924*x*z*z - 6384*x*x*z + 224*x*z*z*z + 6720*x*x*x*z + 3696*x*x - 7560*x*x*x + 4480*x*x*x*x + 672*x*y*z*z + 672*x*y*y*z + 5376*x*x*y*z - 1848*x*y*z;
   values[11] = 2688*x*x*y*y - 476*x + 2688*x*x*z*z + 1176*x*y + 1176*x*z - 924*x*y*y - 6384*x*x*y + 224*x*y*y*y + 6720*x*x*x*y - 924*x*z*z - 6384*x*x*z + 224*x*z*z*z + 6720*x*x*x*z + 3696*x*x - 7560*x*x*x + 4480*x*x*x*x + 672*x*y*z*z + 672*x*y*y*z + 5376*x*x*y*z - 1848*x*y*z;
   values[12] = 40*y - 672*x*x*y*y - 672*y*y*z*z - 240*x*y - 240*y*z + 840*x*y*y + 420*x*x*y - 672*x*y*y*y - 224*x*x*x*y + 420*y*z*z + 840*y*y*z - 224*y*z*z*z - 672*y*y*y*z - 240*y*y + 420*y*y*y - 224*y*y*y*y - 672*x*y*z*z - 1344*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[13] = 672*x*x*y*y - 120*y - 160*z - 160*x + 1344*x*x*z*z + 672*y*y*z*z + 720*x*y + 960*x*z + 720*y*z - 840*x*y*y - 1260*x*x*y + 224*x*y*y*y + 672*x*x*x*y - 1680*x*z*z - 1680*x*x*z + 896*x*z*z*z + 896*x*x*x*z - 1260*y*z*z - 840*y*y*z + 672*y*z*z*z + 224*y*y*y*z + 480*x*x - 560*x*x*x + 224*x*x*x*x + 240*y*y - 140*y*y*y + 480*z*z - 560*z*z*z + 224*z*z*z*z + 2016*x*y*z*z + 1344*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z + 16;
   values[14] = 40*y - 672*x*x*y*y - 672*y*y*z*z - 240*x*y - 240*y*z + 840*x*y*y + 420*x*x*y - 672*x*y*y*y - 224*x*x*x*y + 420*y*z*z + 840*y*y*z - 224*y*z*z*z - 672*y*y*y*z - 240*y*y + 420*y*y*y - 224*y*y*y*y - 672*x*y*z*z - 1344*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[15] = 4032*x*x*y*y - 360*y + 4032*y*y*z*z + 1440*x*y + 1440*y*z - 6552*x*y*y - 1764*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 1764*y*z*z - 6552*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 2520*y*y - 4788*y*y*y + 2688*y*y*y*y + 2016*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[16] = 840*x + 1200*y + 840*z - 6048*x*x*y*y - 4032*x*x*z*z - 6048*y*y*z*z - 5760*x*y - 3960*x*z - 5760*y*z + 8316*x*y*y + 8568*x*x*y - 2688*x*y*y*y - 4032*x*x*x*y + 5796*x*z*z + 5796*x*x*z - 2688*x*z*z*z - 2688*x*x*x*z + 8568*y*z*z + 8316*y*y*z - 4032*y*z*z*z - 2688*y*y*y*z - 1980*x*x + 1932*x*x*x - 672*x*x*x*x - 2700*y*y + 1680*y*y*y - 1980*z*z + 1932*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[17] = 4032*x*x*y*y - 360*y + 4032*y*y*z*z + 1440*x*y + 1440*y*z - 6552*x*y*y - 1764*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 1764*y*z*z - 6552*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 2520*y*y - 4788*y*y*y + 2688*y*y*y*y + 2016*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[18] = 780*y - 6048*x*x*y*y - 6048*y*y*z*z - 2376*x*y - 2376*y*z + 12096*x*y*y + 2268*x*x*y - 12096*x*y*y*y - 672*x*x*x*y + 2268*y*z*z + 12096*y*y*z - 672*y*z*z*z - 12096*y*y*y*z - 5832*y*y + 11592*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[19] = 12096*x*x*y*y - 2700*y - 1320*z - 1320*x + 4032*x*x*z*z + 12096*y*y*z*z + 11016*x*y + 5184*x*z + 11016*y*z - 18144*x*y*y - 14364*x*x*y + 6720*x*y*y*y + 6048*x*x*x*y - 6552*x*z*z - 6552*x*x*z + 2688*x*z*z*z + 2688*x*x*x*z - 14364*y*z*z - 18144*y*y*z + 6048*y*z*z*z + 6720*y*y*y*z + 2592*x*x - 2184*x*x*x + 672*x*x*x*x + 6480*y*y - 4200*y*y*y + 2592*z*z - 2184*z*z*z + 672*z*z*z*z + 18144*x*y*z*z + 24192*x*y*y*z + 18144*x*x*y*z - 28728*x*y*z + 240;
   values[20] = 780*y - 6048*x*x*y*y - 6048*y*y*z*z - 2376*x*y - 2376*y*z + 12096*x*y*y + 2268*x*x*y - 12096*x*y*y*y - 672*x*x*x*y + 2268*y*z*z + 12096*y*y*z - 672*y*z*z*z - 12096*y*y*y*z - 5832*y*y + 11592*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[21] = 2688*x*x*y*y - 476*y + 2688*y*y*z*z + 1176*x*y + 1176*y*z - 6384*x*y*y - 924*x*x*y + 6720*x*y*y*y + 224*x*x*x*y - 924*y*z*z - 6384*y*y*z + 224*y*z*z*z + 6720*y*y*y*z + 3696*y*y - 7560*y*y*y + 4480*y*y*y*y + 672*x*y*z*z + 5376*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[22] = 644*x + 1680*y + 644*z - 6720*x*x*y*y - 1344*x*x*z*z - 6720*y*y*z*z - 6048*x*y - 2184*x*z - 6048*y*z + 10920*x*y*y + 7056*x*x*y - 4480*x*y*y*y - 2688*x*x*x*y + 2436*x*z*z + 2436*x*x*z - 896*x*z*z*z - 896*x*x*x*z + 7056*y*z*z + 10920*y*y*z - 2688*y*z*z*z - 4480*y*y*y*z - 1092*x*x + 812*x*x*x - 224*x*x*x*x - 4200*y*y + 2800*y*y*y - 1092*z*z + 812*z*z*z - 224*z*z*z*z - 8064*x*y*z*z - 13440*x*y*y*z - 8064*x*x*y*z + 14112*x*y*z - 140;
   values[23] = 2688*x*x*y*y - 476*y + 2688*y*y*z*z + 1176*x*y + 1176*y*z - 6384*x*y*y - 924*x*x*y + 6720*x*y*y*y + 224*x*x*x*y - 924*y*z*z - 6384*y*y*z + 224*y*z*z*z + 6720*y*y*y*z + 3696*y*y - 7560*y*y*y + 4480*y*y*y*y + 672*x*y*z*z + 5376*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[24] = 40*z - 672*x*x*z*z - 672*y*y*z*z - 240*x*z - 240*y*z + 840*x*z*z + 420*x*x*z - 672*x*z*z*z - 224*x*x*x*z + 840*y*z*z + 420*y*y*z - 672*y*z*z*z - 224*y*y*y*z - 240*z*z + 420*z*z*z - 224*z*z*z*z - 1344*x*y*z*z - 672*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[25] = 40*z - 672*x*x*z*z - 672*y*y*z*z - 240*x*z - 240*y*z + 840*x*z*z + 420*x*x*z - 672*x*z*z*z - 224*x*x*x*z + 840*y*z*z + 420*y*y*z - 672*y*z*z*z - 224*y*y*y*z - 240*z*z + 420*z*z*z - 224*z*z*z*z - 1344*x*y*z*z - 672*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[26] = 1344*x*x*y*y - 160*y - 120*z - 160*x + 672*x*x*z*z + 672*y*y*z*z + 960*x*y + 720*x*z + 720*y*z - 1680*x*y*y - 1680*x*x*y + 896*x*y*y*y + 896*x*x*x*y - 840*x*z*z - 1260*x*x*z + 224*x*z*z*z + 672*x*x*x*z - 840*y*z*z - 1260*y*y*z + 224*y*z*z*z + 672*y*y*y*z + 480*x*x - 560*x*x*x + 224*x*x*x*x + 480*y*y - 560*y*y*y + 224*y*y*y*y + 240*z*z - 140*z*z*z + 1344*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z + 16;
   values[27] = 4032*x*x*z*z - 360*z + 4032*y*y*z*z + 1440*x*z + 1440*y*z - 6552*x*z*z - 1764*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 6552*y*z*z - 1764*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 2520*z*z - 4788*z*z*z + 2688*z*z*z*z + 8064*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[28] = 4032*x*x*z*z - 360*z + 4032*y*y*z*z + 1440*x*z + 1440*y*z - 6552*x*z*z - 1764*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 6552*y*z*z - 1764*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 2520*z*z - 4788*z*z*z + 2688*z*z*z*z + 8064*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[29] = 840*x + 840*y + 1200*z - 4032*x*x*y*y - 6048*x*x*z*z - 6048*y*y*z*z - 3960*x*y - 5760*x*z - 5760*y*z + 5796*x*y*y + 5796*x*x*y - 2688*x*y*y*y - 2688*x*x*x*y + 8316*x*z*z + 8568*x*x*z - 2688*x*z*z*z - 4032*x*x*x*z + 8316*y*z*z + 8568*y*y*z - 2688*y*z*z*z - 4032*y*y*y*z - 1980*x*x + 1932*x*x*x - 672*x*x*x*x - 1980*y*y + 1932*y*y*y - 672*y*y*y*y - 2700*z*z + 1680*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[30] = 780*z - 6048*x*x*z*z - 6048*y*y*z*z - 2376*x*z - 2376*y*z + 12096*x*z*z + 2268*x*x*z - 12096*x*z*z*z - 672*x*x*x*z + 12096*y*z*z + 2268*y*y*z - 12096*y*z*z*z - 672*y*y*y*z - 5832*z*z + 11592*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[31] = 780*z - 6048*x*x*z*z - 6048*y*y*z*z - 2376*x*z - 2376*y*z + 12096*x*z*z + 2268*x*x*z - 12096*x*z*z*z - 672*x*x*x*z + 12096*y*z*z + 2268*y*y*z - 12096*y*z*z*z - 672*y*y*y*z - 5832*z*z + 11592*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[32] = 4032*x*x*y*y - 1320*y - 2700*z - 1320*x + 12096*x*x*z*z + 12096*y*y*z*z + 5184*x*y + 11016*x*z + 11016*y*z - 6552*x*y*y - 6552*x*x*y + 2688*x*y*y*y + 2688*x*x*x*y - 18144*x*z*z - 14364*x*x*z + 6720*x*z*z*z + 6048*x*x*x*z - 18144*y*z*z - 14364*y*y*z + 6720*y*z*z*z + 6048*y*y*y*z + 2592*x*x - 2184*x*x*x + 672*x*x*x*x + 2592*y*y - 2184*y*y*y + 672*y*y*y*y + 6480*z*z - 4200*z*z*z + 24192*x*y*z*z + 18144*x*y*y*z + 18144*x*x*y*z - 28728*x*y*z + 240;
   values[33] = 2688*x*x*z*z - 476*z + 2688*y*y*z*z + 1176*x*z + 1176*y*z - 6384*x*z*z - 924*x*x*z + 6720*x*z*z*z + 224*x*x*x*z - 6384*y*z*z - 924*y*y*z + 6720*y*z*z*z + 224*y*y*y*z + 3696*z*z - 7560*z*z*z + 4480*z*z*z*z + 5376*x*y*z*z + 672*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[34] = 2688*x*x*z*z - 476*z + 2688*y*y*z*z + 1176*x*z + 1176*y*z - 6384*x*z*z - 924*x*x*z + 6720*x*z*z*z + 224*x*x*x*z - 6384*y*z*z - 924*y*y*z + 6720*y*z*z*z + 224*y*y*y*z + 3696*z*z - 7560*z*z*z + 4480*z*z*z*z + 5376*x*y*z*z + 672*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[35] = 644*x + 644*y + 1680*z - 1344*x*x*y*y - 6720*x*x*z*z - 6720*y*y*z*z - 2184*x*y - 6048*x*z - 6048*y*z + 2436*x*y*y + 2436*x*x*y - 896*x*y*y*y - 896*x*x*x*y + 10920*x*z*z + 7056*x*x*z - 4480*x*z*z*z - 2688*x*x*x*z + 10920*y*z*z + 7056*y*y*z - 4480*y*z*z*z - 2688*y*y*y*z - 1092*x*x + 812*x*x*x - 224*x*x*x*x - 1092*y*y + 812*y*y*y - 224*y*y*y*y - 4200*z*z + 2800*z*z*z - 13440*x*y*z*z - 8064*x*y*y*z - 8064*x*x*y*z + 14112*x*y*z - 140;
   values[36] = 4*y - 72*x*y + 252*x*x*y - 224*x*x*x*y;
   values[37] = 144*x*x - 16*x - 336*x*x*x + 224*x*x*x*x;
   values[38] = 0;
   values[39] = 12*y - 2016*x*x*y*y - 72*x*y + 1008*x*y*y - 252*x*x*y + 672*x*x*x*y - 72*y*y;
   values[40] = 12*x + 216*x*y - 1512*x*x*y + 2016*x*x*x*y - 216*x*x + 756*x*x*x - 672*x*x*x*x;
   values[41] = 0;
   values[42] = 4032*x*x*y*y + 144*x*y - 504*x*y*y - 252*x*x*y - 2016*x*y*y*y - 672*x*x*x*y - 72*y*y + 252*y*y*y;
   values[43] = 2016*x*x*y*y - 144*x*y - 504*x*y*y + 2016*x*x*y - 4032*x*x*x*y + 72*x*x - 504*x*x*x + 672*x*x*x*x;
   values[44] = 0;
   values[45] = 252*x*x*y - 504*x*y*y - 2016*x*x*y*y + 2016*x*y*y*y + 224*x*x*x*y + 84*y*y*y - 224*y*y*y*y;
   values[46] = 252*x*y*y - 2016*x*x*y*y - 504*x*x*y + 224*x*y*y*y + 2016*x*x*x*y + 84*x*x*x - 224*x*x*x*x;
   values[47] = 0;
   values[48] = 4*z - 72*x*z + 252*x*x*z - 224*x*x*x*z;
   values[49] = 0;
   values[50] = 144*x*x - 16*x - 336*x*x*x + 224*x*x*x*x;
   values[51] = 12*z - 2016*x*x*z*z - 72*x*z + 1008*x*z*z - 252*x*x*z + 672*x*x*x*z - 72*z*z;
   values[52] = 0;
   values[53] = 12*x + 216*x*z - 1512*x*x*z + 2016*x*x*x*z - 216*x*x + 756*x*x*x - 672*x*x*x*x;
   values[54] = 4032*x*x*z*z + 144*x*z - 504*x*z*z - 252*x*x*z - 2016*x*z*z*z - 672*x*x*x*z - 72*z*z + 252*z*z*z;
   values[55] = 0;
   values[56] = 2016*x*x*z*z - 144*x*z - 504*x*z*z + 2016*x*x*z - 4032*x*x*x*z + 72*x*x - 504*x*x*x + 672*x*x*x*x;
   values[57] = 252*x*x*z - 504*x*z*z - 2016*x*x*z*z + 2016*x*z*z*z + 224*x*x*x*z + 84*z*z*z - 224*z*z*z*z;
   values[58] = 0;
   values[59] = 252*x*z*z - 2016*x*x*z*z - 504*x*x*z + 224*x*z*z*z + 2016*x*x*x*z + 84*x*x*x - 224*x*x*x*x;
   values[60] = 0;
   values[61] = 4*z - 72*y*z + 252*y*y*z - 224*y*y*y*z;
   values[62] = 144*y*y - 16*y - 336*y*y*y + 224*y*y*y*y;
   values[63] = 0;
   values[64] = 12*z - 2016*y*y*z*z - 72*y*z + 1008*y*z*z - 252*y*y*z + 672*y*y*y*z - 72*z*z;
   values[65] = 12*y + 216*y*z - 1512*y*y*z + 2016*y*y*y*z - 216*y*y + 756*y*y*y - 672*y*y*y*y;
   values[66] = 0;
   values[67] = 4032*y*y*z*z + 144*y*z - 504*y*z*z - 252*y*y*z - 2016*y*z*z*z - 672*y*y*y*z - 72*z*z + 252*z*z*z;
   values[68] = 2016*y*y*z*z - 144*y*z - 504*y*z*z + 2016*y*y*z - 4032*y*y*y*z + 72*y*y - 504*y*y*y + 672*y*y*y*y;
   values[69] = 0;
   values[70] = 252*y*y*z - 504*y*z*z - 2016*y*y*z*z + 2016*y*z*z*z + 224*y*y*y*z + 84*z*z*z - 224*z*z*z*z;
   values[71] = 252*y*z*z - 2016*y*y*z*z - 504*y*y*z + 224*y*z*z*z + 2016*y*y*y*z + 84*y*y*y - 224*y*y*y*y;
   values[72] = 504*x*y*z - 1008*x*x*y*z - 36*y*z;
   values[73] = 144*x*z - 1008*x*x*z + 1344*x*x*x*z;
   values[74] = 252*x*x*y - 36*x*y - 336*x*x*x*y;
   values[75] = 252*y*z*z - 2016*x*y*z*z + 2016*x*x*y*z - 504*x*y*z;
   values[76] = 4032*x*x*z*z - 1008*x*z*z + 1008*x*x*z - 2688*x*x*x*z;
   values[77] = 672*x*x*x*y - 252*x*x*y - 2016*x*x*y*z + 504*x*y*z;
   values[78] = 2016*x*y*z*z - 336*y*z*z*z - 1008*x*x*y*z;
   values[79] = 1344*x*z*z*z - 4032*x*x*z*z + 1344*x*x*x*z;
   values[80] = 2016*x*x*y*z - 1008*x*y*z*z - 336*x*x*x*y;
   values[81] = 504*y*y*z - 72*y*z - 4032*x*y*y*z + 2016*x*x*y*z;
   values[82] = 1260*x*x*z - 72*x*z - 2688*x*x*x*z + 6048*x*x*y*z - 1512*x*y*z;
   values[83] = 504*x*y*y - 72*x*y - 2016*x*x*y*y + 672*x*x*x*y;
   values[84] = 252*y*z*z - 2016*y*y*z*z + 2016*x*y*z*z + 4032*x*y*y*z - 2016*x*x*y*z - 504*x*y*z;
   values[85] = 252*x*z*z - 4032*x*x*z*z - 252*x*x*z + 2688*x*x*x*z + 6048*x*y*z*z - 6048*x*x*y*z;
   values[86] = 2016*x*x*y*y - 252*x*x*y - 672*x*x*x*y - 4032*x*y*y*z + 2016*x*x*y*z + 504*x*y*z;
   values[87] = 252*y*y*z - 1008*y*y*y*z + 4032*x*y*y*z - 1008*x*x*y*z - 504*x*y*z;
   values[88] = 1344*x*x*x*z - 252*x*x*z + 2016*x*y*y*z - 6048*x*x*y*z + 504*x*y*z;
   values[89] = 2016*x*x*y*y + 252*x*y*y - 252*x*x*y - 1008*x*y*y*y - 336*x*x*x*y;
   values[90] = 504*x*y*z - 1008*x*x*y*z - 36*y*z;
   values[91] = 252*x*x*z - 36*x*z - 336*x*x*x*z;
   values[92] = 144*x*y - 1008*x*x*y + 1344*x*x*x*y;
   values[93] = 504*y*z*z - 72*y*z - 4032*x*y*z*z + 2016*x*x*y*z;
   values[94] = 504*x*z*z - 72*x*z - 2016*x*x*z*z + 672*x*x*x*z;
   values[95] = 1260*x*x*y - 72*x*y - 2688*x*x*x*y + 6048*x*x*y*z - 1512*x*y*z;
   values[96] = 252*y*z*z - 1008*y*z*z*z + 4032*x*y*z*z - 1008*x*x*y*z - 504*x*y*z;
   values[97] = 2016*x*x*z*z + 252*x*z*z - 252*x*x*z - 1008*x*z*z*z - 336*x*x*x*z;
   values[98] = 1344*x*x*x*y - 252*x*x*y + 2016*x*y*z*z - 6048*x*x*y*z + 504*x*y*z;
   values[99] = 252*y*y*z - 2016*x*y*y*z + 2016*x*x*y*z - 504*x*y*z;
   values[100] = 672*x*x*x*z - 252*x*x*z - 2016*x*x*y*z + 504*x*y*z;
   values[101] = 4032*x*x*y*y - 1008*x*y*y + 1008*x*x*y - 2688*x*x*x*y;
   values[102] = 252*y*y*z - 2016*y*y*z*z + 4032*x*y*z*z + 2016*x*y*y*z - 2016*x*x*y*z - 504*x*y*z;
   values[103] = 2016*x*x*z*z - 252*x*x*z - 672*x*x*x*z - 4032*x*y*z*z + 2016*x*x*y*z + 504*x*y*z;
   values[104] = 252*x*y*y - 4032*x*x*y*y - 252*x*x*y + 2688*x*x*x*y + 6048*x*y*y*z - 6048*x*x*y*z;
   values[105] = 2016*x*y*y*z - 336*y*y*y*z - 1008*x*x*y*z;
   values[106] = 2016*x*x*y*z - 1008*x*y*y*z - 336*x*x*x*z;
   values[107] = 1344*x*y*y*y - 4032*x*x*y*y + 1344*x*x*x*y;
   values[108] = 2016*y*y*z*z + 540*y*z - 1512*y*z*z - 1512*y*y*z + 1008*y*z*z*z + 1008*y*y*y*z + 2016*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 1512*x*y*z;
   values[109] = 480*z - 4032*x*x*z*z - 2016*y*y*z*z - 2160*x*z - 1620*y*z + 6048*x*z*z + 3024*x*x*z - 4032*x*z*z*z - 1344*x*x*x*z + 4536*y*z*z + 1512*y*y*z - 3024*y*z*z*z - 336*y*y*y*z - 2160*z*z + 3024*z*z*z - 1344*z*z*z*z - 6048*x*y*z*z - 2016*x*y*y*z - 3024*x*x*y*z + 4536*x*y*z;
   values[110] = 1008*x*x*y*y - 120*y + 3024*y*y*z*z + 540*x*y + 1080*y*z - 1512*x*y*y - 756*x*x*y + 1008*x*y*y*y + 336*x*x*x*y - 2268*y*z*z - 3024*y*y*z + 1344*y*z*z*z + 2016*y*y*y*z + 540*y*y - 756*y*y*y + 336*y*y*y*y + 3024*x*y*z*z + 4032*x*y*y*z + 2016*x*x*y*z - 3024*x*y*z;
   values[111] = 5292*y*z*z - 1512*y*z - 6048*y*y*z*z + 3528*y*y*z - 4032*y*z*z*z - 2016*y*y*y*z - 6048*x*y*z*z - 4032*x*y*y*z - 2016*x*x*y*z + 3528*x*y*z;
   values[112] = 12096*x*x*z*z - 1680*z + 6048*y*y*z*z + 6048*x*z + 4536*y*z - 21168*x*z*z - 7056*x*x*z + 16128*x*z*z*z + 2688*x*x*x*z - 15876*y*z*z - 3528*y*y*z + 12096*y*z*z*z + 672*y*y*y*z + 9072*z*z - 14112*z*z*z + 6720*z*z*z*z + 18144*x*y*z*z + 4032*x*y*y*z + 6048*x*x*y*z - 10584*x*y*z;
   values[113] = 420*y - 2016*x*x*y*y - 12096*y*y*z*z - 1512*x*y - 4536*y*z + 3528*x*y*y + 1764*x*x*y - 2016*x*y*y*y - 672*x*x*x*y + 10584*y*z*z + 10584*y*y*z - 6720*y*z*z*z - 6048*y*y*y*z - 1512*y*y + 1764*y*y*y - 672*y*y*y*y - 12096*x*y*z*z - 12096*x*y*y*z - 6048*x*x*y*z + 10584*x*y*z;
   values[114] = 4032*y*y*z*z + 1008*y*z - 4032*y*z*z - 2016*y*y*z + 3360*y*z*z*z + 1008*y*y*y*z + 4032*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 2016*x*y*z;
   values[115] = 1344*z - 8064*x*x*z*z - 4032*y*y*z*z - 4032*x*z - 3024*y*z + 16128*x*z*z + 4032*x*x*z - 13440*x*z*z*z - 1344*x*x*x*z + 12096*y*z*z + 2016*y*y*z - 10080*y*z*z*z - 336*y*y*y*z - 8064*z*z + 13440*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 3024*x*x*y*z + 6048*x*y*z;
   values[116] = 1008*x*x*y*y - 336*y + 10080*y*y*z*z + 1008*x*y + 4032*y*z - 2016*x*y*y - 1008*x*x*y + 1008*x*y*y*y + 336*x*x*x*y - 10080*y*z*z - 8064*y*y*z + 6720*y*z*z*z + 4032*y*y*y*z + 1008*y*y - 1008*y*y*y + 336*y*y*y*y + 10080*x*y*z*z + 8064*x*y*y*z + 4032*x*x*y*z - 8064*x*y*z;
   values[117] = 4032*y*z*z - 1944*y*z - 8064*y*y*z*z + 7560*y*y*z - 2016*y*z*z*z - 6048*y*y*y*z - 4032*x*y*z*z - 8064*x*y*y*z - 2016*x*x*y*z + 4032*x*y*z;
   values[118] = 8064*x*x*z*z - 1500*z + 12096*y*y*z*z + 5616*x*z + 8208*y*z - 13608*x*z*z - 6804*x*x*z + 8064*x*z*z*z + 2688*x*x*x*z - 20160*y*z*z - 9828*y*y*z + 12096*y*z*z*z + 2688*y*y*y*z + 5616*z*z - 6804*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[119] = 600*y - 4032*x*x*y*y - 12096*y*y*z*z - 1944*x*y - 3888*y*z + 7560*x*y*y + 2016*x*x*y - 6048*x*y*y*y - 672*x*x*x*y + 6048*y*z*z + 15120*y*y*z - 2688*y*z*z*z - 12096*y*y*y*z - 3456*y*y + 5544*y*y*y - 2688*y*y*y*y - 6048*x*y*z*z - 16128*x*y*y*z - 4032*x*x*y*z + 8064*x*y*z;
   values[120] = 12096*y*y*z*z + 2520*y*z - 6804*y*z*z - 8568*y*y*z + 4032*y*z*z*z + 6048*y*y*y*z + 6048*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 4536*x*y*z;
   values[121] = 2436*z - 12096*x*x*z*z - 18144*y*y*z*z - 7560*x*z - 11088*y*z + 23436*x*z*z + 7812*x*x*z - 16128*x*z*z*z - 2688*x*x*x*z + 34776*y*z*z + 11340*y*y*z - 24192*y*z*z*z - 2688*y*y*y*z - 11340*z*z + 15624*z*z*z - 6720*z*z*z*z - 36288*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[122] = 4032*x*x*y*y - 924*y + 24192*y*y*z*z + 2520*x*y + 7560*y*z - 8568*x*y*y - 2268*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 13608*y*z*z - 25704*y*y*z + 6720*y*z*z*z + 18144*y*y*y*z + 4536*y*y - 6300*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 6048*x*x*y*z - 13608*x*y*z;
   values[123] = 6048*y*y*z*z + 1512*y*z - 2520*y*z*z - 6804*y*y*z + 1008*y*z*z*z + 6048*y*y*y*z + 2016*x*y*z*z + 6048*x*y*y*z + 1008*x*x*y*z - 2520*x*y*z;
   values[124] = 1092*z - 4032*x*x*z*z - 12096*y*y*z*z - 3528*x*z - 7560*y*z + 7560*x*z*z + 3780*x*x*z - 4032*x*z*z*z - 1344*x*x*x*z + 16632*y*z*z + 10584*y*y*z - 9072*y*z*z*z - 3360*y*y*y*z - 3528*z*z + 3780*z*z*z - 1344*z*z*z*z - 18144*x*y*z*z - 12096*x*y*y*z - 9072*x*x*y*z + 16632*x*y*z;
   values[125] = 3024*x*x*y*y - 588*y + 9072*y*y*z*z + 1512*x*y + 3024*y*z - 6804*x*y*y - 1260*x*x*y + 6048*x*y*y*y + 336*x*x*x*y - 3780*y*z*z - 13608*y*y*z + 1344*y*z*z*z + 12096*y*y*y*z + 3780*y*y - 6552*y*y*y + 3360*y*y*y*y + 3024*x*y*z*z + 12096*x*y*y*z + 2016*x*x*y*z - 5040*x*y*z;
   values[126] = 2016*y*y*z*z + 540*y*z - 1512*y*z*z - 1512*y*y*z + 1008*y*z*z*z + 1008*y*y*y*z + 2016*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 1512*x*y*z;
   values[127] = 1008*x*x*z*z - 120*z + 3024*y*y*z*z + 540*x*z + 1080*y*z - 1512*x*z*z - 756*x*x*z + 1008*x*z*z*z + 336*x*x*x*z - 3024*y*z*z - 2268*y*y*z + 2016*y*z*z*z + 1344*y*y*y*z + 540*z*z - 756*z*z*z + 336*z*z*z*z + 4032*x*y*z*z + 3024*x*y*y*z + 2016*x*x*y*z - 3024*x*y*z;
   values[128] = 480*y - 4032*x*x*y*y - 2016*y*y*z*z - 2160*x*y - 1620*y*z + 6048*x*y*y + 3024*x*x*y - 4032*x*y*y*y - 1344*x*x*x*y + 1512*y*z*z + 4536*y*y*z - 336*y*z*z*z - 3024*y*y*y*z - 2160*y*y + 3024*y*y*y - 1344*y*y*y*y - 2016*x*y*z*z - 6048*x*y*y*z - 3024*x*x*y*z + 4536*x*y*z;
   values[129] = 7560*y*z*z - 1944*y*z - 8064*y*y*z*z + 4032*y*y*z - 6048*y*z*z*z - 2016*y*y*y*z - 8064*x*y*z*z - 4032*x*y*y*z - 2016*x*x*y*z + 4032*x*y*z;
   values[130] = 600*z - 4032*x*x*z*z - 12096*y*y*z*z - 1944*x*z - 3888*y*z + 7560*x*z*z + 2016*x*x*z - 6048*x*z*z*z - 672*x*x*x*z + 15120*y*z*z + 6048*y*y*z - 12096*y*z*z*z - 2688*y*y*y*z - 3456*z*z + 5544*z*z*z - 2688*z*z*z*z - 16128*x*y*z*z - 6048*x*y*y*z - 4032*x*x*y*z + 8064*x*y*z;
   values[131] = 8064*x*x*y*y - 1500*y + 12096*y*y*z*z + 5616*x*y + 8208*y*z - 13608*x*y*y - 6804*x*x*y + 8064*x*y*y*y + 2688*x*x*x*y - 9828*y*z*z - 20160*y*y*z + 2688*y*z*z*z + 12096*y*y*y*z + 5616*y*y - 6804*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[132] = 6048*y*y*z*z + 1512*y*z - 6804*y*z*z - 2520*y*y*z + 6048*y*z*z*z + 1008*y*y*y*z + 6048*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 2520*x*y*z;
   values[133] = 3024*x*x*z*z - 588*z + 9072*y*y*z*z + 1512*x*z + 3024*y*z - 6804*x*z*z - 1260*x*x*z + 6048*x*z*z*z + 336*x*x*x*z - 13608*y*z*z - 3780*y*y*z + 12096*y*z*z*z + 1344*y*y*y*z + 3780*z*z - 6552*z*z*z + 3360*z*z*z*z + 12096*x*y*z*z + 3024*x*y*y*z + 2016*x*x*y*z - 5040*x*y*z;
   values[134] = 1092*y - 4032*x*x*y*y - 12096*y*y*z*z - 3528*x*y - 7560*y*z + 7560*x*y*y + 3780*x*x*y - 4032*x*y*y*y - 1344*x*x*x*y + 10584*y*z*z + 16632*y*y*z - 3360*y*z*z*z - 9072*y*y*y*z - 3528*y*y + 3780*y*y*y - 1344*y*y*y*y - 12096*x*y*z*z - 18144*x*y*y*z - 9072*x*x*y*z + 16632*x*y*z;
   values[135] = 3528*y*z*z - 1512*y*z - 6048*y*y*z*z + 5292*y*y*z - 2016*y*z*z*z - 4032*y*y*y*z - 4032*x*y*z*z - 6048*x*y*y*z - 2016*x*x*y*z + 3528*x*y*z;
   values[136] = 420*z - 2016*x*x*z*z - 12096*y*y*z*z - 1512*x*z - 4536*y*z + 3528*x*z*z + 1764*x*x*z - 2016*x*z*z*z - 672*x*x*x*z + 10584*y*z*z + 10584*y*y*z - 6048*y*z*z*z - 6720*y*y*y*z - 1512*z*z + 1764*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 6048*x*x*y*z + 10584*x*y*z;
   values[137] = 12096*x*x*y*y - 1680*y + 6048*y*y*z*z + 6048*x*y + 4536*y*z - 21168*x*y*y - 7056*x*x*y + 16128*x*y*y*y + 2688*x*x*x*y - 3528*y*z*z - 15876*y*y*z + 672*y*z*z*z + 12096*y*y*y*z + 9072*y*y - 14112*y*y*y + 6720*y*y*y*y + 4032*x*y*z*z + 18144*x*y*y*z + 6048*x*x*y*z - 10584*x*y*z;
   values[138] = 12096*y*y*z*z + 2520*y*z - 8568*y*z*z - 6804*y*y*z + 6048*y*z*z*z + 4032*y*y*y*z + 8064*x*y*z*z + 6048*x*y*y*z + 2016*x*x*y*z - 4536*x*y*z;
   values[139] = 4032*x*x*z*z - 924*z + 24192*y*y*z*z + 2520*x*z + 7560*y*z - 8568*x*z*z - 2268*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 25704*y*z*z - 13608*y*y*z + 18144*y*z*z*z + 6720*y*y*y*z + 4536*z*z - 6300*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 6048*x*x*y*z - 13608*x*y*z;
   values[140] = 2436*y - 12096*x*x*y*y - 18144*y*y*z*z - 7560*x*y - 11088*y*z + 23436*x*y*y + 7812*x*x*y - 16128*x*y*y*y - 2688*x*x*x*y + 11340*y*z*z + 34776*y*y*z - 2688*y*z*z*z - 24192*y*y*y*z - 11340*y*y + 15624*y*y*y - 6720*y*y*y*y - 12096*x*y*z*z - 36288*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[141] = 4032*y*y*z*z + 1008*y*z - 2016*y*z*z - 4032*y*y*z + 1008*y*z*z*z + 3360*y*y*y*z + 2016*x*y*z*z + 4032*x*y*y*z + 1008*x*x*y*z - 2016*x*y*z;
   values[142] = 1008*x*x*z*z - 336*z + 10080*y*y*z*z + 1008*x*z + 4032*y*z - 2016*x*z*z - 1008*x*x*z + 1008*x*z*z*z + 336*x*x*x*z - 8064*y*z*z - 10080*y*y*z + 4032*y*z*z*z + 6720*y*y*y*z + 1008*z*z - 1008*z*z*z + 336*z*z*z*z + 8064*x*y*z*z + 10080*x*y*y*z + 4032*x*x*y*z - 8064*x*y*z;
   values[143] = 1344*y - 8064*x*x*y*y - 4032*y*y*z*z - 4032*x*y - 3024*y*z + 16128*x*y*y + 4032*x*x*y - 13440*x*y*y*y - 1344*x*x*x*y + 2016*y*z*z + 12096*y*y*z - 336*y*z*z*z - 10080*y*y*y*z - 8064*y*y + 13440*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 3024*x*x*y*z + 6048*x*y*z;
   values[144] = 480*z - 2016*x*x*z*z - 4032*y*y*z*z - 1620*x*z - 2160*y*z + 4536*x*z*z + 1512*x*x*z - 3024*x*z*z*z - 336*x*x*x*z + 6048*y*z*z + 3024*y*y*z - 4032*y*z*z*z - 1344*y*y*y*z - 2160*z*z + 3024*z*z*z - 1344*z*z*z*z - 6048*x*y*z*z - 3024*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[145] = 2016*x*x*z*z + 540*x*z - 1512*x*z*z - 1512*x*x*z + 1008*x*z*z*z + 1008*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[146] = 1008*x*x*y*y - 120*x + 3024*x*x*z*z + 540*x*y + 1080*x*z - 756*x*y*y - 1512*x*x*y + 336*x*y*y*y + 1008*x*x*x*y - 2268*x*z*z - 3024*x*x*z + 1344*x*z*z*z + 2016*x*x*x*z + 540*x*x - 756*x*x*x + 336*x*x*x*x + 3024*x*y*z*z + 2016*x*y*y*z + 4032*x*x*y*z - 3024*x*y*z;
   values[147] = 6048*x*x*z*z - 1680*z + 12096*y*y*z*z + 4536*x*z + 6048*y*z - 15876*x*z*z - 3528*x*x*z + 12096*x*z*z*z + 672*x*x*x*z - 21168*y*z*z - 7056*y*y*z + 16128*y*z*z*z + 2688*y*y*y*z + 9072*z*z - 14112*z*z*z + 6720*z*z*z*z + 18144*x*y*z*z + 6048*x*y*y*z + 4032*x*x*y*z - 10584*x*y*z;
   values[148] = 5292*x*z*z - 1512*x*z - 6048*x*x*z*z + 3528*x*x*z - 4032*x*z*z*z - 2016*x*x*x*z - 6048*x*y*z*z - 2016*x*y*y*z - 4032*x*x*y*z + 3528*x*y*z;
   values[149] = 420*x - 2016*x*x*y*y - 12096*x*x*z*z - 1512*x*y - 4536*x*z + 1764*x*y*y + 3528*x*x*y - 672*x*y*y*y - 2016*x*x*x*y + 10584*x*z*z + 10584*x*x*z - 6720*x*z*z*z - 6048*x*x*x*z - 1512*x*x + 1764*x*x*x - 672*x*x*x*x - 12096*x*y*z*z - 6048*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[150] = 1344*z - 4032*x*x*z*z - 8064*y*y*z*z - 3024*x*z - 4032*y*z + 12096*x*z*z + 2016*x*x*z - 10080*x*z*z*z - 336*x*x*x*z + 16128*y*z*z + 4032*y*y*z - 13440*y*z*z*z - 1344*y*y*y*z - 8064*z*z + 13440*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 3024*x*y*y*z - 2016*x*x*y*z + 6048*x*y*z;
   values[151] = 4032*x*x*z*z + 1008*x*z - 4032*x*z*z - 2016*x*x*z + 3360*x*z*z*z + 1008*x*x*x*z + 4032*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 2016*x*y*z;
   values[152] = 1008*x*x*y*y - 336*x + 10080*x*x*z*z + 1008*x*y + 4032*x*z - 1008*x*y*y - 2016*x*x*y + 336*x*y*y*y + 1008*x*x*x*y - 10080*x*z*z - 8064*x*x*z + 6720*x*z*z*z + 4032*x*x*x*z + 1008*x*x - 1008*x*x*x + 336*x*x*x*x + 10080*x*y*z*z + 4032*x*y*y*z + 8064*x*x*y*z - 8064*x*y*z;
   values[153] = 12096*x*x*z*z - 1500*z + 8064*y*y*z*z + 8208*x*z + 5616*y*z - 20160*x*z*z - 9828*x*x*z + 12096*x*z*z*z + 2688*x*x*x*z - 13608*y*z*z - 6804*y*y*z + 8064*y*z*z*z + 2688*y*y*y*z + 5616*z*z - 6804*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[154] = 4032*x*z*z - 1944*x*z - 8064*x*x*z*z + 7560*x*x*z - 2016*x*z*z*z - 6048*x*x*x*z - 4032*x*y*z*z - 2016*x*y*y*z - 8064*x*x*y*z + 4032*x*y*z;
   values[155] = 600*x - 4032*x*x*y*y - 12096*x*x*z*z - 1944*x*y - 3888*x*z + 2016*x*y*y + 7560*x*x*y - 672*x*y*y*y - 6048*x*x*x*y + 6048*x*z*z + 15120*x*x*z - 2688*x*z*z*z - 12096*x*x*x*z - 3456*x*x + 5544*x*x*x - 2688*x*x*x*x - 6048*x*y*z*z - 4032*x*y*y*z - 16128*x*x*y*z + 8064*x*y*z;
   values[156] = 2436*z - 18144*x*x*z*z - 12096*y*y*z*z - 11088*x*z - 7560*y*z + 34776*x*z*z + 11340*x*x*z - 24192*x*z*z*z - 2688*x*x*x*z + 23436*y*z*z + 7812*y*y*z - 16128*y*z*z*z - 2688*y*y*y*z - 11340*z*z + 15624*z*z*z - 6720*z*z*z*z - 36288*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[157] = 12096*x*x*z*z + 2520*x*z - 6804*x*z*z - 8568*x*x*z + 4032*x*z*z*z + 6048*x*x*x*z + 6048*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 4536*x*y*z;
   values[158] = 4032*x*x*y*y - 924*x + 24192*x*x*z*z + 2520*x*y + 7560*x*z - 2268*x*y*y - 8568*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 13608*x*z*z - 25704*x*x*z + 6720*x*z*z*z + 18144*x*x*x*z + 4536*x*x - 6300*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 6048*x*y*y*z + 24192*x*x*y*z - 13608*x*y*z;
   values[159] = 1092*z - 12096*x*x*z*z - 4032*y*y*z*z - 7560*x*z - 3528*y*z + 16632*x*z*z + 10584*x*x*z - 9072*x*z*z*z - 3360*x*x*x*z + 7560*y*z*z + 3780*y*y*z - 4032*y*z*z*z - 1344*y*y*y*z - 3528*z*z + 3780*z*z*z - 1344*z*z*z*z - 18144*x*y*z*z - 9072*x*y*y*z - 12096*x*x*y*z + 16632*x*y*z;
   values[160] = 6048*x*x*z*z + 1512*x*z - 2520*x*z*z - 6804*x*x*z + 1008*x*z*z*z + 6048*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 6048*x*x*y*z - 2520*x*y*z;
   values[161] = 3024*x*x*y*y - 588*x + 9072*x*x*z*z + 1512*x*y + 3024*x*z - 1260*x*y*y - 6804*x*x*y + 336*x*y*y*y + 6048*x*x*x*y - 3780*x*z*z - 13608*x*x*z + 1344*x*z*z*z + 12096*x*x*x*z + 3780*x*x - 6552*x*x*x + 3360*x*x*x*x + 3024*x*y*z*z + 2016*x*y*y*z + 12096*x*x*y*z - 5040*x*y*z;
   values[162] = 3024*x*x*z*z - 120*z + 1008*y*y*z*z + 1080*x*z + 540*y*z - 3024*x*z*z - 2268*x*x*z + 2016*x*z*z*z + 1344*x*x*x*z - 1512*y*z*z - 756*y*y*z + 1008*y*z*z*z + 336*y*y*y*z + 540*z*z - 756*z*z*z + 336*z*z*z*z + 4032*x*y*z*z + 2016*x*y*y*z + 3024*x*x*y*z - 3024*x*y*z;
   values[163] = 2016*x*x*z*z + 540*x*z - 1512*x*z*z - 1512*x*x*z + 1008*x*z*z*z + 1008*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[164] = 480*x - 4032*x*x*y*y - 2016*x*x*z*z - 2160*x*y - 1620*x*z + 3024*x*y*y + 6048*x*x*y - 1344*x*y*y*y - 4032*x*x*x*y + 1512*x*z*z + 4536*x*x*z - 336*x*z*z*z - 3024*x*x*x*z - 2160*x*x + 3024*x*x*x - 1344*x*x*x*x - 2016*x*y*z*z - 3024*x*y*y*z - 6048*x*x*y*z + 4536*x*y*z;
   values[165] = 600*z - 12096*x*x*z*z - 4032*y*y*z*z - 3888*x*z - 1944*y*z + 15120*x*z*z + 6048*x*x*z - 12096*x*z*z*z - 2688*x*x*x*z + 7560*y*z*z + 2016*y*y*z - 6048*y*z*z*z - 672*y*y*y*z - 3456*z*z + 5544*z*z*z - 2688*z*z*z*z - 16128*x*y*z*z - 4032*x*y*y*z - 6048*x*x*y*z + 8064*x*y*z;
   values[166] = 7560*x*z*z - 1944*x*z - 8064*x*x*z*z + 4032*x*x*z - 6048*x*z*z*z - 2016*x*x*x*z - 8064*x*y*z*z - 2016*x*y*y*z - 4032*x*x*y*z + 4032*x*y*z;
   values[167] = 8064*x*x*y*y - 1500*x + 12096*x*x*z*z + 5616*x*y + 8208*x*z - 6804*x*y*y - 13608*x*x*y + 2688*x*y*y*y + 8064*x*x*x*y - 9828*x*z*z - 20160*x*x*z + 2688*x*z*z*z + 12096*x*x*x*z + 5616*x*x - 6804*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 20160*x*y*z;
   values[168] = 9072*x*x*z*z - 588*z + 3024*y*y*z*z + 3024*x*z + 1512*y*z - 13608*x*z*z - 3780*x*x*z + 12096*x*z*z*z + 1344*x*x*x*z - 6804*y*z*z - 1260*y*y*z + 6048*y*z*z*z + 336*y*y*y*z + 3780*z*z - 6552*z*z*z + 3360*z*z*z*z + 12096*x*y*z*z + 2016*x*y*y*z + 3024*x*x*y*z - 5040*x*y*z;
   values[169] = 6048*x*x*z*z + 1512*x*z - 6804*x*z*z - 2520*x*x*z + 6048*x*z*z*z + 1008*x*x*x*z + 6048*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z;
   values[170] = 1092*x - 4032*x*x*y*y - 12096*x*x*z*z - 3528*x*y - 7560*x*z + 3780*x*y*y + 7560*x*x*y - 1344*x*y*y*y - 4032*x*x*x*y + 10584*x*z*z + 16632*x*x*z - 3360*x*z*z*z - 9072*x*x*x*z - 3528*x*x + 3780*x*x*x - 1344*x*x*x*x - 12096*x*y*z*z - 9072*x*y*y*z - 18144*x*x*y*z + 16632*x*y*z;
   values[171] = 420*z - 12096*x*x*z*z - 2016*y*y*z*z - 4536*x*z - 1512*y*z + 10584*x*z*z + 10584*x*x*z - 6048*x*z*z*z - 6720*x*x*x*z + 3528*y*z*z + 1764*y*y*z - 2016*y*z*z*z - 672*y*y*y*z - 1512*z*z + 1764*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 6048*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[172] = 3528*x*z*z - 1512*x*z - 6048*x*x*z*z + 5292*x*x*z - 2016*x*z*z*z - 4032*x*x*x*z - 4032*x*y*z*z - 2016*x*y*y*z - 6048*x*x*y*z + 3528*x*y*z;
   values[173] = 12096*x*x*y*y - 1680*x + 6048*x*x*z*z + 6048*x*y + 4536*x*z - 7056*x*y*y - 21168*x*x*y + 2688*x*y*y*y + 16128*x*x*x*y - 3528*x*z*z - 15876*x*x*z + 672*x*z*z*z + 12096*x*x*x*z + 9072*x*x - 14112*x*x*x + 6720*x*x*x*x + 4032*x*y*z*z + 6048*x*y*y*z + 18144*x*x*y*z - 10584*x*y*z;
   values[174] = 24192*x*x*z*z - 924*z + 4032*y*y*z*z + 7560*x*z + 2520*y*z - 25704*x*z*z - 13608*x*x*z + 18144*x*z*z*z + 6720*x*x*x*z - 8568*y*z*z - 2268*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 4536*z*z - 6300*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 6048*x*y*y*z + 12096*x*x*y*z - 13608*x*y*z;
   values[175] = 12096*x*x*z*z + 2520*x*z - 8568*x*z*z - 6804*x*x*z + 6048*x*z*z*z + 4032*x*x*x*z + 8064*x*y*z*z + 2016*x*y*y*z + 6048*x*x*y*z - 4536*x*y*z;
   values[176] = 2436*x - 12096*x*x*y*y - 18144*x*x*z*z - 7560*x*y - 11088*x*z + 7812*x*y*y + 23436*x*x*y - 2688*x*y*y*y - 16128*x*x*x*y + 11340*x*z*z + 34776*x*x*z - 2688*x*z*z*z - 24192*x*x*x*z - 11340*x*x + 15624*x*x*x - 6720*x*x*x*x - 12096*x*y*z*z - 12096*x*y*y*z - 36288*x*x*y*z + 23184*x*y*z;
   values[177] = 10080*x*x*z*z - 336*z + 1008*y*y*z*z + 4032*x*z + 1008*y*z - 8064*x*z*z - 10080*x*x*z + 4032*x*z*z*z + 6720*x*x*x*z - 2016*y*z*z - 1008*y*y*z + 1008*y*z*z*z + 336*y*y*y*z + 1008*z*z - 1008*z*z*z + 336*z*z*z*z + 8064*x*y*z*z + 4032*x*y*y*z + 10080*x*x*y*z - 8064*x*y*z;
   values[178] = 4032*x*x*z*z + 1008*x*z - 2016*x*z*z - 4032*x*x*z + 1008*x*z*z*z + 3360*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 4032*x*x*y*z - 2016*x*y*z;
   values[179] = 1344*x - 8064*x*x*y*y - 4032*x*x*z*z - 4032*x*y - 3024*x*z + 4032*x*y*y + 16128*x*x*y - 1344*x*y*y*y - 13440*x*x*x*y + 2016*x*z*z + 12096*x*x*z - 336*x*z*z*z - 10080*x*x*x*z - 8064*x*x + 13440*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 3024*x*y*y*z - 12096*x*x*y*z + 6048*x*y*z;
   values[180] = 480*y - 2016*x*x*y*y - 4032*y*y*z*z - 1620*x*y - 2160*y*z + 4536*x*y*y + 1512*x*x*y - 3024*x*y*y*y - 336*x*x*x*y + 3024*y*z*z + 6048*y*y*z - 1344*y*z*z*z - 4032*y*y*y*z - 2160*y*y + 3024*y*y*y - 1344*y*y*y*y - 3024*x*y*z*z - 6048*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[181] = 3024*x*x*y*y - 120*x + 1008*x*x*z*z + 1080*x*y + 540*x*z - 2268*x*y*y - 3024*x*x*y + 1344*x*y*y*y + 2016*x*x*x*y - 756*x*z*z - 1512*x*x*z + 336*x*z*z*z + 1008*x*x*x*z + 540*x*x - 756*x*x*x + 336*x*x*x*x + 2016*x*y*z*z + 3024*x*y*y*z + 4032*x*x*y*z - 3024*x*y*z;
   values[182] = 2016*x*x*y*y + 540*x*y - 1512*x*y*y - 1512*x*x*y + 1008*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[183] = 6048*x*x*y*y - 1680*y + 12096*y*y*z*z + 4536*x*y + 6048*y*z - 15876*x*y*y - 3528*x*x*y + 12096*x*y*y*y + 672*x*x*x*y - 7056*y*z*z - 21168*y*y*z + 2688*y*z*z*z + 16128*y*y*y*z + 9072*y*y - 14112*y*y*y + 6720*y*y*y*y + 6048*x*y*z*z + 18144*x*y*y*z + 4032*x*x*y*z - 10584*x*y*z;
   values[184] = 420*x - 12096*x*x*y*y - 2016*x*x*z*z - 4536*x*y - 1512*x*z + 10584*x*y*y + 10584*x*x*y - 6720*x*y*y*y - 6048*x*x*x*y + 1764*x*z*z + 3528*x*x*z - 672*x*z*z*z - 2016*x*x*x*z - 1512*x*x + 1764*x*x*x - 672*x*x*x*x - 6048*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[185] = 5292*x*y*y - 1512*x*y - 6048*x*x*y*y + 3528*x*x*y - 4032*x*y*y*y - 2016*x*x*x*y - 2016*x*y*z*z - 6048*x*y*y*z - 4032*x*x*y*z + 3528*x*y*z;
   values[186] = 1344*y - 4032*x*x*y*y - 8064*y*y*z*z - 3024*x*y - 4032*y*z + 12096*x*y*y + 2016*x*x*y - 10080*x*y*y*y - 336*x*x*x*y + 4032*y*z*z + 16128*y*y*z - 1344*y*z*z*z - 13440*y*y*y*z - 8064*y*y + 13440*y*y*y - 6720*y*y*y*y - 3024*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 6048*x*y*z;
   values[187] = 10080*x*x*y*y - 336*x + 1008*x*x*z*z + 4032*x*y + 1008*x*z - 10080*x*y*y - 8064*x*x*y + 6720*x*y*y*y + 4032*x*x*x*y - 1008*x*z*z - 2016*x*x*z + 336*x*z*z*z + 1008*x*x*x*z + 1008*x*x - 1008*x*x*x + 336*x*x*x*x + 4032*x*y*z*z + 10080*x*y*y*z + 8064*x*x*y*z - 8064*x*y*z;
   values[188] = 4032*x*x*y*y + 1008*x*y - 4032*x*y*y - 2016*x*x*y + 3360*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 4032*x*y*y*z + 2016*x*x*y*z - 2016*x*y*z;
   values[189] = 12096*x*x*y*y - 1500*y + 8064*y*y*z*z + 8208*x*y + 5616*y*z - 20160*x*y*y - 9828*x*x*y + 12096*x*y*y*y + 2688*x*x*x*y - 6804*y*z*z - 13608*y*y*z + 2688*y*z*z*z + 8064*y*y*y*z + 5616*y*y - 6804*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[190] = 600*x - 12096*x*x*y*y - 4032*x*x*z*z - 3888*x*y - 1944*x*z + 6048*x*y*y + 15120*x*x*y - 2688*x*y*y*y - 12096*x*x*x*y + 2016*x*z*z + 7560*x*x*z - 672*x*z*z*z - 6048*x*x*x*z - 3456*x*x + 5544*x*x*x - 2688*x*x*x*x - 4032*x*y*z*z - 6048*x*y*y*z - 16128*x*x*y*z + 8064*x*y*z;
   values[191] = 4032*x*y*y - 1944*x*y - 8064*x*x*y*y + 7560*x*x*y - 2016*x*y*y*y - 6048*x*x*x*y - 2016*x*y*z*z - 4032*x*y*y*z - 8064*x*x*y*z + 4032*x*y*z;
   values[192] = 2436*y - 18144*x*x*y*y - 12096*y*y*z*z - 11088*x*y - 7560*y*z + 34776*x*y*y + 11340*x*x*y - 24192*x*y*y*y - 2688*x*x*x*y + 7812*y*z*z + 23436*y*y*z - 2688*y*z*z*z - 16128*y*y*y*z - 11340*y*y + 15624*y*y*y - 6720*y*y*y*y - 12096*x*y*z*z - 36288*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[193] = 24192*x*x*y*y - 924*x + 4032*x*x*z*z + 7560*x*y + 2520*x*z - 13608*x*y*y - 25704*x*x*y + 6720*x*y*y*y + 18144*x*x*x*y - 2268*x*z*z - 8568*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 4536*x*x - 6300*x*x*x + 2688*x*x*x*x + 6048*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 13608*x*y*z;
   values[194] = 12096*x*x*y*y + 2520*x*y - 6804*x*y*y - 8568*x*x*y + 4032*x*y*y*y + 6048*x*x*x*y + 2016*x*y*z*z + 6048*x*y*y*z + 8064*x*x*y*z - 4536*x*y*z;
   values[195] = 1092*y - 12096*x*x*y*y - 4032*y*y*z*z - 7560*x*y - 3528*y*z + 16632*x*y*y + 10584*x*x*y - 9072*x*y*y*y - 3360*x*x*x*y + 3780*y*z*z + 7560*y*y*z - 1344*y*z*z*z - 4032*y*y*y*z - 3528*y*y + 3780*y*y*y - 1344*y*y*y*y - 9072*x*y*z*z - 18144*x*y*y*z - 12096*x*x*y*z + 16632*x*y*z;
   values[196] = 9072*x*x*y*y - 588*x + 3024*x*x*z*z + 3024*x*y + 1512*x*z - 3780*x*y*y - 13608*x*x*y + 1344*x*y*y*y + 12096*x*x*x*y - 1260*x*z*z - 6804*x*x*z + 336*x*z*z*z + 6048*x*x*x*z + 3780*x*x - 6552*x*x*x + 3360*x*x*x*x + 2016*x*y*z*z + 3024*x*y*y*z + 12096*x*x*y*z - 5040*x*y*z;
   values[197] = 6048*x*x*y*y + 1512*x*y - 2520*x*y*y - 6804*x*x*y + 1008*x*y*y*y + 6048*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 6048*x*x*y*z - 2520*x*y*z;
   values[198] = 3024*x*x*y*y - 120*y + 1008*y*y*z*z + 1080*x*y + 540*y*z - 3024*x*y*y - 2268*x*x*y + 2016*x*y*y*y + 1344*x*x*x*y - 756*y*z*z - 1512*y*y*z + 336*y*z*z*z + 1008*y*y*y*z + 540*y*y - 756*y*y*y + 336*y*y*y*y + 2016*x*y*z*z + 4032*x*y*y*z + 3024*x*x*y*z - 3024*x*y*z;
   values[199] = 480*x - 2016*x*x*y*y - 4032*x*x*z*z - 1620*x*y - 2160*x*z + 1512*x*y*y + 4536*x*x*y - 336*x*y*y*y - 3024*x*x*x*y + 3024*x*z*z + 6048*x*x*z - 1344*x*z*z*z - 4032*x*x*x*z - 2160*x*x + 3024*x*x*x - 1344*x*x*x*x - 3024*x*y*z*z - 2016*x*y*y*z - 6048*x*x*y*z + 4536*x*y*z;
   values[200] = 2016*x*x*y*y + 540*x*y - 1512*x*y*y - 1512*x*x*y + 1008*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[201] = 600*y - 12096*x*x*y*y - 4032*y*y*z*z - 3888*x*y - 1944*y*z + 15120*x*y*y + 6048*x*x*y - 12096*x*y*y*y - 2688*x*x*x*y + 2016*y*z*z + 7560*y*y*z - 672*y*z*z*z - 6048*y*y*y*z - 3456*y*y + 5544*y*y*y - 2688*y*y*y*y - 4032*x*y*z*z - 16128*x*y*y*z - 6048*x*x*y*z + 8064*x*y*z;
   values[202] = 12096*x*x*y*y - 1500*x + 8064*x*x*z*z + 8208*x*y + 5616*x*z - 9828*x*y*y - 20160*x*x*y + 2688*x*y*y*y + 12096*x*x*x*y - 6804*x*z*z - 13608*x*x*z + 2688*x*z*z*z + 8064*x*x*x*z + 5616*x*x - 6804*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 20160*x*y*z;
   values[203] = 7560*x*y*y - 1944*x*y - 8064*x*x*y*y + 4032*x*x*y - 6048*x*y*y*y - 2016*x*x*x*y - 2016*x*y*z*z - 8064*x*y*y*z - 4032*x*x*y*z + 4032*x*y*z;
   values[204] = 9072*x*x*y*y - 588*y + 3024*y*y*z*z + 3024*x*y + 1512*y*z - 13608*x*y*y - 3780*x*x*y + 12096*x*y*y*y + 1344*x*x*x*y - 1260*y*z*z - 6804*y*y*z + 336*y*z*z*z + 6048*y*y*y*z + 3780*y*y - 6552*y*y*y + 3360*y*y*y*y + 2016*x*y*z*z + 12096*x*y*y*z + 3024*x*x*y*z - 5040*x*y*z;
   values[205] = 1092*x - 12096*x*x*y*y - 4032*x*x*z*z - 7560*x*y - 3528*x*z + 10584*x*y*y + 16632*x*x*y - 3360*x*y*y*y - 9072*x*x*x*y + 3780*x*z*z + 7560*x*x*z - 1344*x*z*z*z - 4032*x*x*x*z - 3528*x*x + 3780*x*x*x - 1344*x*x*x*x - 9072*x*y*z*z - 12096*x*y*y*z - 18144*x*x*y*z + 16632*x*y*z;
   values[206] = 6048*x*x*y*y + 1512*x*y - 6804*x*y*y - 2520*x*x*y + 6048*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 6048*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z;
   values[207] = 420*y - 12096*x*x*y*y - 2016*y*y*z*z - 4536*x*y - 1512*y*z + 10584*x*y*y + 10584*x*x*y - 6048*x*y*y*y - 6720*x*x*x*y + 1764*y*z*z + 3528*y*y*z - 672*y*z*z*z - 2016*y*y*y*z - 1512*y*y + 1764*y*y*y - 672*y*y*y*y - 6048*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[208] = 6048*x*x*y*y - 1680*x + 12096*x*x*z*z + 4536*x*y + 6048*x*z - 3528*x*y*y - 15876*x*x*y + 672*x*y*y*y + 12096*x*x*x*y - 7056*x*z*z - 21168*x*x*z + 2688*x*z*z*z + 16128*x*x*x*z + 9072*x*x - 14112*x*x*x + 6720*x*x*x*x + 6048*x*y*z*z + 4032*x*y*y*z + 18144*x*x*y*z - 10584*x*y*z;
   values[209] = 3528*x*y*y - 1512*x*y - 6048*x*x*y*y + 5292*x*x*y - 2016*x*y*y*y - 4032*x*x*x*y - 2016*x*y*z*z - 4032*x*y*y*z - 6048*x*x*y*z + 3528*x*y*z;
   values[210] = 24192*x*x*y*y - 924*y + 4032*y*y*z*z + 7560*x*y + 2520*y*z - 25704*x*y*y - 13608*x*x*y + 18144*x*y*y*y + 6720*x*x*x*y - 2268*y*z*z - 8568*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 4536*y*y - 6300*y*y*y + 2688*y*y*y*y + 6048*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 13608*x*y*z;
   values[211] = 2436*x - 18144*x*x*y*y - 12096*x*x*z*z - 11088*x*y - 7560*x*z + 11340*x*y*y + 34776*x*x*y - 2688*x*y*y*y - 24192*x*x*x*y + 7812*x*z*z + 23436*x*x*z - 2688*x*z*z*z - 16128*x*x*x*z - 11340*x*x + 15624*x*x*x - 6720*x*x*x*x - 12096*x*y*z*z - 12096*x*y*y*z - 36288*x*x*y*z + 23184*x*y*z;
   values[212] = 12096*x*x*y*y + 2520*x*y - 8568*x*y*y - 6804*x*x*y + 6048*x*y*y*y + 4032*x*x*x*y + 2016*x*y*z*z + 8064*x*y*y*z + 6048*x*x*y*z - 4536*x*y*z;
   values[213] = 10080*x*x*y*y - 336*y + 1008*y*y*z*z + 4032*x*y + 1008*y*z - 8064*x*y*y - 10080*x*x*y + 4032*x*y*y*y + 6720*x*x*x*y - 1008*y*z*z - 2016*y*y*z + 336*y*z*z*z + 1008*y*y*y*z + 1008*y*y - 1008*y*y*y + 336*y*y*y*y + 4032*x*y*z*z + 8064*x*y*y*z + 10080*x*x*y*z - 8064*x*y*z;
   values[214] = 1344*x - 4032*x*x*y*y - 8064*x*x*z*z - 3024*x*y - 4032*x*z + 2016*x*y*y + 12096*x*x*y - 336*x*y*y*y - 10080*x*x*x*y + 4032*x*z*z + 16128*x*x*z - 1344*x*z*z*z - 13440*x*x*x*z - 8064*x*x + 13440*x*x*x - 6720*x*x*x*x - 3024*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 6048*x*y*z;
   values[215] = 4032*x*x*y*y + 1008*x*y - 2016*x*y*y - 4032*x*x*y + 1008*x*y*y*y + 3360*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 4032*x*x*y*z - 2016*x*y*z;
   values[216] = 32256*y*y*z*z + 12096*y*z - 28224*y*z*z - 28224*y*y*z + 16128*y*z*z*z + 16128*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 8064*x*x*y*z - 21168*x*y*z;
   values[217] = 7056*x*z*z - 3024*x*z - 8064*x*x*z*z + 7056*x*x*z - 4032*x*z*z*z - 4032*x*x*x*z - 16128*x*y*z*z - 12096*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[218] = 7056*x*y*y - 3024*x*y - 8064*x*x*y*y + 7056*x*x*y - 4032*x*y*y*y - 4032*x*x*x*y - 12096*x*y*z*z - 16128*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[219] = 7056*y*z*z - 3024*y*z - 8064*y*y*z*z + 7056*y*y*z - 4032*y*z*z*z - 4032*y*y*y*z - 16128*x*y*z*z - 16128*x*y*y*z - 12096*x*x*y*z + 14112*x*y*z;
   values[220] = 32256*x*x*z*z + 12096*x*z - 28224*x*z*z - 28224*x*x*z + 16128*x*z*z*z + 16128*x*x*x*z + 24192*x*y*z*z + 8064*x*y*y*z + 24192*x*x*y*z - 21168*x*y*z;
   values[221] = 7056*x*y*y - 3024*x*y - 8064*x*x*y*y + 7056*x*x*y - 4032*x*y*y*y - 4032*x*x*x*y - 12096*x*y*z*z - 16128*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[222] = 7056*y*z*z - 3024*y*z - 8064*y*y*z*z + 7056*y*y*z - 4032*y*z*z*z - 4032*y*y*y*z - 16128*x*y*z*z - 16128*x*y*y*z - 12096*x*x*y*z + 14112*x*y*z;
   values[223] = 7056*x*z*z - 3024*x*z - 8064*x*x*z*z + 7056*x*x*z - 4032*x*z*z*z - 4032*x*x*x*z - 16128*x*y*z*z - 12096*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[224] = 32256*x*x*y*y + 12096*x*y - 28224*x*y*y - 28224*x*x*y + 16128*x*y*y*y + 16128*x*x*x*y + 8064*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 21168*x*y*z;
   values[225] = 48384*y*z*z - 16128*y*z - 48384*y*y*z*z + 32256*y*y*z - 32256*y*z*z*z - 16128*y*y*y*z - 36288*x*y*z*z - 24192*x*y*y*z - 8064*x*x*y*z + 24192*x*y*z;
   values[226] = 12096*x*x*z*z + 4032*x*z - 12096*x*z*z - 8064*x*x*z + 8064*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 12096*x*y*y*z + 16128*x*x*y*z - 16128*x*y*z;
   values[227] = 8064*x*x*y*y + 4032*x*y - 8064*x*y*y - 8064*x*x*y + 4032*x*y*y*y + 4032*x*x*x*y + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[228] = 12096*y*y*z*z + 4032*y*z - 12096*y*z*z - 8064*y*y*z + 8064*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 16128*x*y*y*z + 12096*x*x*y*z - 16128*x*y*z;
   values[229] = 48384*x*z*z - 16128*x*z - 48384*x*x*z*z + 32256*x*x*z - 32256*x*z*z*z - 16128*x*x*x*z - 36288*x*y*z*z - 8064*x*y*y*z - 24192*x*x*y*z + 24192*x*y*z;
   values[230] = 8064*x*x*y*y + 4032*x*y - 8064*x*y*y - 8064*x*x*y + 4032*x*y*y*y + 4032*x*x*x*y + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[231] = 16128*y*y*z*z + 5040*y*z - 17136*y*z*z - 9072*y*y*z + 12096*y*z*z*z + 4032*y*y*y*z + 32256*x*y*z*z + 16128*x*y*y*z + 12096*x*x*y*z - 18144*x*y*z;
   values[232] = 16128*x*x*z*z + 5040*x*z - 17136*x*z*z - 9072*x*x*z + 12096*x*z*z*z + 4032*x*x*x*z + 32256*x*y*z*z + 12096*x*y*y*z + 16128*x*x*y*z - 18144*x*y*z;
   values[233] = 31248*x*y*y - 15120*x*y - 32256*x*x*y*y + 31248*x*x*y - 16128*x*y*y*y - 16128*x*x*x*y - 24192*x*y*z*z - 48384*x*y*y*z - 48384*x*x*y*z + 46368*x*y*z;
   values[234] = 32256*y*z*z - 16128*y*z - 48384*y*y*z*z + 48384*y*y*z - 16128*y*z*z*z - 32256*y*y*y*z - 24192*x*y*z*z - 36288*x*y*y*z - 8064*x*x*y*z + 24192*x*y*z;
   values[235] = 8064*x*x*z*z + 4032*x*z - 8064*x*z*z - 8064*x*x*z + 4032*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[236] = 12096*x*x*y*y + 4032*x*y - 12096*x*y*y - 8064*x*x*y + 8064*x*y*y*y + 4032*x*x*x*y + 12096*x*y*z*z + 24192*x*y*y*z + 16128*x*x*y*z - 16128*x*y*z;
   values[237] = 16128*y*y*z*z + 5040*y*z - 9072*y*z*z - 17136*y*y*z + 4032*y*z*z*z + 12096*y*y*y*z + 16128*x*y*z*z + 32256*x*y*y*z + 12096*x*x*y*z - 18144*x*y*z;
   values[238] = 31248*x*z*z - 15120*x*z - 32256*x*x*z*z + 31248*x*x*z - 16128*x*z*z*z - 16128*x*x*x*z - 48384*x*y*z*z - 24192*x*y*y*z - 48384*x*x*y*z + 46368*x*y*z;
   values[239] = 16128*x*x*y*y + 5040*x*y - 17136*x*y*y - 9072*x*x*y + 12096*x*y*y*y + 4032*x*x*x*y + 12096*x*y*z*z + 32256*x*y*y*z + 16128*x*x*y*z - 18144*x*y*z;
   values[240] = 12096*y*y*z*z + 4032*y*z - 8064*y*z*z - 12096*y*y*z + 4032*y*z*z*z + 8064*y*y*y*z + 16128*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 16128*x*y*z;
   values[241] = 8064*x*x*z*z + 4032*x*z - 8064*x*z*z - 8064*x*x*z + 4032*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[242] = 48384*x*y*y - 16128*x*y - 48384*x*x*y*y + 32256*x*x*y - 32256*x*y*y*y - 16128*x*x*x*y - 8064*x*y*z*z - 36288*x*y*y*z - 24192*x*x*y*z + 24192*x*y*z;
   values[243] = 31248*y*z*z - 15120*y*z - 32256*y*y*z*z + 31248*y*y*z - 16128*y*z*z*z - 16128*y*y*y*z - 48384*x*y*z*z - 48384*x*y*y*z - 24192*x*x*y*z + 46368*x*y*z;
   values[244] = 16128*x*x*z*z + 5040*x*z - 9072*x*z*z - 17136*x*x*z + 4032*x*z*z*z + 12096*x*x*x*z + 16128*x*y*z*z + 12096*x*y*y*z + 32256*x*x*y*z - 18144*x*y*z;
   values[245] = 16128*x*x*y*y + 5040*x*y - 9072*x*y*y - 17136*x*x*y + 4032*x*y*y*y + 12096*x*x*x*y + 12096*x*y*z*z + 16128*x*y*y*z + 32256*x*x*y*z - 18144*x*y*z;
   values[246] = 8064*y*y*z*z + 4032*y*z - 8064*y*z*z - 8064*y*y*z + 4032*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[247] = 32256*x*z*z - 16128*x*z - 48384*x*x*z*z + 48384*x*x*z - 16128*x*z*z*z - 32256*x*x*x*z - 24192*x*y*z*z - 8064*x*y*y*z - 36288*x*x*y*z + 24192*x*y*z;
   values[248] = 12096*x*x*y*y + 4032*x*y - 8064*x*y*y - 12096*x*x*y + 4032*x*y*y*y + 8064*x*x*x*y + 12096*x*y*z*z + 16128*x*y*y*z + 24192*x*x*y*z - 16128*x*y*z;
   values[249] = 8064*y*y*z*z + 4032*y*z - 8064*y*z*z - 8064*y*y*z + 4032*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[250] = 12096*x*x*z*z + 4032*x*z - 8064*x*z*z - 12096*x*x*z + 4032*x*z*z*z + 8064*x*x*x*z + 16128*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 16128*x*y*z;
   values[251] = 32256*x*y*y - 16128*x*y - 48384*x*x*y*y + 48384*x*x*y - 16128*x*y*y*y - 32256*x*x*x*y - 8064*x*y*z*z - 24192*x*y*y*z - 36288*x*x*y*z + 24192*x*y*z;
}
static void Curl_T_P4_3D_3D_D000(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 672*x*x*y*y - 160*y - 160*z - 120*x + 672*x*x*z*z + 1344*y*y*z*z + 720*x*y + 720*x*z + 960*y*z - 1260*x*y*y - 840*x*x*y + 672*x*y*y*y + 224*x*x*x*y - 1260*x*z*z - 840*x*x*z + 672*x*z*z*z + 224*x*x*x*z - 1680*y*z*z - 1680*y*y*z + 896*y*z*z*z + 896*y*y*y*z + 240*x*x - 140*x*x*x + 480*y*y - 560*y*y*y + 224*y*y*y*y + 480*z*z - 560*z*z*z + 224*z*z*z*z + 2016*x*y*z*z + 2016*x*y*y*z + 1344*x*x*y*z - 2520*x*y*z + 16;
   values[1] = 40*x - 672*x*x*y*y - 672*x*x*z*z - 240*x*y - 240*x*z + 420*x*y*y + 840*x*x*y - 224*x*y*y*y - 672*x*x*x*y + 420*x*z*z + 840*x*x*z - 224*x*z*z*z - 672*x*x*x*z - 240*x*x + 420*x*x*x - 224*x*x*x*x - 672*x*y*z*z - 672*x*y*y*z - 1344*x*x*y*z + 840*x*y*z;
   values[2] = 40*x - 672*x*x*y*y - 672*x*x*z*z - 240*x*y - 240*x*z + 420*x*y*y + 840*x*x*y - 224*x*y*y*y - 672*x*x*x*y + 420*x*z*z + 840*x*x*z - 224*x*z*z*z - 672*x*x*x*z - 240*x*x + 420*x*x*x - 224*x*x*x*x - 672*x*y*z*z - 672*x*y*y*z - 1344*x*x*y*z + 840*x*y*z;
   values[3] = 1200*x + 840*y + 840*z - 6048*x*x*y*y - 6048*x*x*z*z - 4032*y*y*z*z - 5760*x*y - 5760*x*z - 3960*y*z + 8568*x*y*y + 8316*x*x*y - 4032*x*y*y*y - 2688*x*x*x*y + 8568*x*z*z + 8316*x*x*z - 4032*x*z*z*z - 2688*x*x*x*z + 5796*y*z*z + 5796*y*y*z - 2688*y*z*z*z - 2688*y*y*y*z - 2700*x*x + 1680*x*x*x - 1980*y*y + 1932*y*y*y - 672*y*y*y*y - 1980*z*z + 1932*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[4] = 4032*x*x*y*y - 360*x + 4032*x*x*z*z + 1440*x*y + 1440*x*z - 1764*x*y*y - 6552*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 1764*x*z*z - 6552*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 2520*x*x - 4788*x*x*x + 2688*x*x*x*x + 2016*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 3528*x*y*z;
   values[5] = 4032*x*x*y*y - 360*x + 4032*x*x*z*z + 1440*x*y + 1440*x*z - 1764*x*y*y - 6552*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 1764*x*z*z - 6552*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 2520*x*x - 4788*x*x*x + 2688*x*x*x*x + 2016*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 3528*x*y*z;
   values[6] = 12096*x*x*y*y - 1320*y - 1320*z - 2700*x + 12096*x*x*z*z + 4032*y*y*z*z + 11016*x*y + 11016*x*z + 5184*y*z - 14364*x*y*y - 18144*x*x*y + 6048*x*y*y*y + 6720*x*x*x*y - 14364*x*z*z - 18144*x*x*z + 6048*x*z*z*z + 6720*x*x*x*z - 6552*y*z*z - 6552*y*y*z + 2688*y*z*z*z + 2688*y*y*y*z + 6480*x*x - 4200*x*x*x + 2592*y*y - 2184*y*y*y + 672*y*y*y*y + 2592*z*z - 2184*z*z*z + 672*z*z*z*z + 18144*x*y*z*z + 18144*x*y*y*z + 24192*x*x*y*z - 28728*x*y*z + 240;
   values[7] = 780*x - 6048*x*x*y*y - 6048*x*x*z*z - 2376*x*y - 2376*x*z + 2268*x*y*y + 12096*x*x*y - 672*x*y*y*y - 12096*x*x*x*y + 2268*x*z*z + 12096*x*x*z - 672*x*z*z*z - 12096*x*x*x*z - 5832*x*x + 11592*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 4536*x*y*z;
   values[8] = 780*x - 6048*x*x*y*y - 6048*x*x*z*z - 2376*x*y - 2376*x*z + 2268*x*y*y + 12096*x*x*y - 672*x*y*y*y - 12096*x*x*x*y + 2268*x*z*z + 12096*x*x*z - 672*x*z*z*z - 12096*x*x*x*z - 5832*x*x + 11592*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 4536*x*y*z;
   values[9] = 1680*x + 644*y + 644*z - 6720*x*x*y*y - 6720*x*x*z*z - 1344*y*y*z*z - 6048*x*y - 6048*x*z - 2184*y*z + 7056*x*y*y + 10920*x*x*y - 2688*x*y*y*y - 4480*x*x*x*y + 7056*x*z*z + 10920*x*x*z - 2688*x*z*z*z - 4480*x*x*x*z + 2436*y*z*z + 2436*y*y*z - 896*y*z*z*z - 896*y*y*y*z - 4200*x*x + 2800*x*x*x - 1092*y*y + 812*y*y*y - 224*y*y*y*y - 1092*z*z + 812*z*z*z - 224*z*z*z*z - 8064*x*y*z*z - 8064*x*y*y*z - 13440*x*x*y*z + 14112*x*y*z - 140;
   values[10] = 2688*x*x*y*y - 476*x + 2688*x*x*z*z + 1176*x*y + 1176*x*z - 924*x*y*y - 6384*x*x*y + 224*x*y*y*y + 6720*x*x*x*y - 924*x*z*z - 6384*x*x*z + 224*x*z*z*z + 6720*x*x*x*z + 3696*x*x - 7560*x*x*x + 4480*x*x*x*x + 672*x*y*z*z + 672*x*y*y*z + 5376*x*x*y*z - 1848*x*y*z;
   values[11] = 2688*x*x*y*y - 476*x + 2688*x*x*z*z + 1176*x*y + 1176*x*z - 924*x*y*y - 6384*x*x*y + 224*x*y*y*y + 6720*x*x*x*y - 924*x*z*z - 6384*x*x*z + 224*x*z*z*z + 6720*x*x*x*z + 3696*x*x - 7560*x*x*x + 4480*x*x*x*x + 672*x*y*z*z + 672*x*y*y*z + 5376*x*x*y*z - 1848*x*y*z;
   values[12] = 40*y - 672*x*x*y*y - 672*y*y*z*z - 240*x*y - 240*y*z + 840*x*y*y + 420*x*x*y - 672*x*y*y*y - 224*x*x*x*y + 420*y*z*z + 840*y*y*z - 224*y*z*z*z - 672*y*y*y*z - 240*y*y + 420*y*y*y - 224*y*y*y*y - 672*x*y*z*z - 1344*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[13] = 672*x*x*y*y - 120*y - 160*z - 160*x + 1344*x*x*z*z + 672*y*y*z*z + 720*x*y + 960*x*z + 720*y*z - 840*x*y*y - 1260*x*x*y + 224*x*y*y*y + 672*x*x*x*y - 1680*x*z*z - 1680*x*x*z + 896*x*z*z*z + 896*x*x*x*z - 1260*y*z*z - 840*y*y*z + 672*y*z*z*z + 224*y*y*y*z + 480*x*x - 560*x*x*x + 224*x*x*x*x + 240*y*y - 140*y*y*y + 480*z*z - 560*z*z*z + 224*z*z*z*z + 2016*x*y*z*z + 1344*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z + 16;
   values[14] = 40*y - 672*x*x*y*y - 672*y*y*z*z - 240*x*y - 240*y*z + 840*x*y*y + 420*x*x*y - 672*x*y*y*y - 224*x*x*x*y + 420*y*z*z + 840*y*y*z - 224*y*z*z*z - 672*y*y*y*z - 240*y*y + 420*y*y*y - 224*y*y*y*y - 672*x*y*z*z - 1344*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[15] = 4032*x*x*y*y - 360*y + 4032*y*y*z*z + 1440*x*y + 1440*y*z - 6552*x*y*y - 1764*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 1764*y*z*z - 6552*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 2520*y*y - 4788*y*y*y + 2688*y*y*y*y + 2016*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[16] = 840*x + 1200*y + 840*z - 6048*x*x*y*y - 4032*x*x*z*z - 6048*y*y*z*z - 5760*x*y - 3960*x*z - 5760*y*z + 8316*x*y*y + 8568*x*x*y - 2688*x*y*y*y - 4032*x*x*x*y + 5796*x*z*z + 5796*x*x*z - 2688*x*z*z*z - 2688*x*x*x*z + 8568*y*z*z + 8316*y*y*z - 4032*y*z*z*z - 2688*y*y*y*z - 1980*x*x + 1932*x*x*x - 672*x*x*x*x - 2700*y*y + 1680*y*y*y - 1980*z*z + 1932*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[17] = 4032*x*x*y*y - 360*y + 4032*y*y*z*z + 1440*x*y + 1440*y*z - 6552*x*y*y - 1764*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 1764*y*z*z - 6552*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 2520*y*y - 4788*y*y*y + 2688*y*y*y*y + 2016*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[18] = 780*y - 6048*x*x*y*y - 6048*y*y*z*z - 2376*x*y - 2376*y*z + 12096*x*y*y + 2268*x*x*y - 12096*x*y*y*y - 672*x*x*x*y + 2268*y*z*z + 12096*y*y*z - 672*y*z*z*z - 12096*y*y*y*z - 5832*y*y + 11592*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[19] = 12096*x*x*y*y - 2700*y - 1320*z - 1320*x + 4032*x*x*z*z + 12096*y*y*z*z + 11016*x*y + 5184*x*z + 11016*y*z - 18144*x*y*y - 14364*x*x*y + 6720*x*y*y*y + 6048*x*x*x*y - 6552*x*z*z - 6552*x*x*z + 2688*x*z*z*z + 2688*x*x*x*z - 14364*y*z*z - 18144*y*y*z + 6048*y*z*z*z + 6720*y*y*y*z + 2592*x*x - 2184*x*x*x + 672*x*x*x*x + 6480*y*y - 4200*y*y*y + 2592*z*z - 2184*z*z*z + 672*z*z*z*z + 18144*x*y*z*z + 24192*x*y*y*z + 18144*x*x*y*z - 28728*x*y*z + 240;
   values[20] = 780*y - 6048*x*x*y*y - 6048*y*y*z*z - 2376*x*y - 2376*y*z + 12096*x*y*y + 2268*x*x*y - 12096*x*y*y*y - 672*x*x*x*y + 2268*y*z*z + 12096*y*y*z - 672*y*z*z*z - 12096*y*y*y*z - 5832*y*y + 11592*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[21] = 2688*x*x*y*y - 476*y + 2688*y*y*z*z + 1176*x*y + 1176*y*z - 6384*x*y*y - 924*x*x*y + 6720*x*y*y*y + 224*x*x*x*y - 924*y*z*z - 6384*y*y*z + 224*y*z*z*z + 6720*y*y*y*z + 3696*y*y - 7560*y*y*y + 4480*y*y*y*y + 672*x*y*z*z + 5376*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[22] = 644*x + 1680*y + 644*z - 6720*x*x*y*y - 1344*x*x*z*z - 6720*y*y*z*z - 6048*x*y - 2184*x*z - 6048*y*z + 10920*x*y*y + 7056*x*x*y - 4480*x*y*y*y - 2688*x*x*x*y + 2436*x*z*z + 2436*x*x*z - 896*x*z*z*z - 896*x*x*x*z + 7056*y*z*z + 10920*y*y*z - 2688*y*z*z*z - 4480*y*y*y*z - 1092*x*x + 812*x*x*x - 224*x*x*x*x - 4200*y*y + 2800*y*y*y - 1092*z*z + 812*z*z*z - 224*z*z*z*z - 8064*x*y*z*z - 13440*x*y*y*z - 8064*x*x*y*z + 14112*x*y*z - 140;
   values[23] = 2688*x*x*y*y - 476*y + 2688*y*y*z*z + 1176*x*y + 1176*y*z - 6384*x*y*y - 924*x*x*y + 6720*x*y*y*y + 224*x*x*x*y - 924*y*z*z - 6384*y*y*z + 224*y*z*z*z + 6720*y*y*y*z + 3696*y*y - 7560*y*y*y + 4480*y*y*y*y + 672*x*y*z*z + 5376*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[24] = 40*z - 672*x*x*z*z - 672*y*y*z*z - 240*x*z - 240*y*z + 840*x*z*z + 420*x*x*z - 672*x*z*z*z - 224*x*x*x*z + 840*y*z*z + 420*y*y*z - 672*y*z*z*z - 224*y*y*y*z - 240*z*z + 420*z*z*z - 224*z*z*z*z - 1344*x*y*z*z - 672*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[25] = 40*z - 672*x*x*z*z - 672*y*y*z*z - 240*x*z - 240*y*z + 840*x*z*z + 420*x*x*z - 672*x*z*z*z - 224*x*x*x*z + 840*y*z*z + 420*y*y*z - 672*y*z*z*z - 224*y*y*y*z - 240*z*z + 420*z*z*z - 224*z*z*z*z - 1344*x*y*z*z - 672*x*y*y*z - 672*x*x*y*z + 840*x*y*z;
   values[26] = 1344*x*x*y*y - 160*y - 120*z - 160*x + 672*x*x*z*z + 672*y*y*z*z + 960*x*y + 720*x*z + 720*y*z - 1680*x*y*y - 1680*x*x*y + 896*x*y*y*y + 896*x*x*x*y - 840*x*z*z - 1260*x*x*z + 224*x*z*z*z + 672*x*x*x*z - 840*y*z*z - 1260*y*y*z + 224*y*z*z*z + 672*y*y*y*z + 480*x*x - 560*x*x*x + 224*x*x*x*x + 480*y*y - 560*y*y*y + 224*y*y*y*y + 240*z*z - 140*z*z*z + 1344*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z + 16;
   values[27] = 4032*x*x*z*z - 360*z + 4032*y*y*z*z + 1440*x*z + 1440*y*z - 6552*x*z*z - 1764*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 6552*y*z*z - 1764*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 2520*z*z - 4788*z*z*z + 2688*z*z*z*z + 8064*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[28] = 4032*x*x*z*z - 360*z + 4032*y*y*z*z + 1440*x*z + 1440*y*z - 6552*x*z*z - 1764*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 6552*y*z*z - 1764*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 2520*z*z - 4788*z*z*z + 2688*z*z*z*z + 8064*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 3528*x*y*z;
   values[29] = 840*x + 840*y + 1200*z - 4032*x*x*y*y - 6048*x*x*z*z - 6048*y*y*z*z - 3960*x*y - 5760*x*z - 5760*y*z + 5796*x*y*y + 5796*x*x*y - 2688*x*y*y*y - 2688*x*x*x*y + 8316*x*z*z + 8568*x*x*z - 2688*x*z*z*z - 4032*x*x*x*z + 8316*y*z*z + 8568*y*y*z - 2688*y*z*z*z - 4032*y*y*y*z - 1980*x*x + 1932*x*x*x - 672*x*x*x*x - 1980*y*y + 1932*y*y*y - 672*y*y*y*y - 2700*z*z + 1680*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 17136*x*y*z - 120;
   values[30] = 780*z - 6048*x*x*z*z - 6048*y*y*z*z - 2376*x*z - 2376*y*z + 12096*x*z*z + 2268*x*x*z - 12096*x*z*z*z - 672*x*x*x*z + 12096*y*z*z + 2268*y*y*z - 12096*y*z*z*z - 672*y*y*y*z - 5832*z*z + 11592*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[31] = 780*z - 6048*x*x*z*z - 6048*y*y*z*z - 2376*x*z - 2376*y*z + 12096*x*z*z + 2268*x*x*z - 12096*x*z*z*z - 672*x*x*x*z + 12096*y*z*z + 2268*y*y*z - 12096*y*z*z*z - 672*y*y*y*z - 5832*z*z + 11592*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[32] = 4032*x*x*y*y - 1320*y - 2700*z - 1320*x + 12096*x*x*z*z + 12096*y*y*z*z + 5184*x*y + 11016*x*z + 11016*y*z - 6552*x*y*y - 6552*x*x*y + 2688*x*y*y*y + 2688*x*x*x*y - 18144*x*z*z - 14364*x*x*z + 6720*x*z*z*z + 6048*x*x*x*z - 18144*y*z*z - 14364*y*y*z + 6720*y*z*z*z + 6048*y*y*y*z + 2592*x*x - 2184*x*x*x + 672*x*x*x*x + 2592*y*y - 2184*y*y*y + 672*y*y*y*y + 6480*z*z - 4200*z*z*z + 24192*x*y*z*z + 18144*x*y*y*z + 18144*x*x*y*z - 28728*x*y*z + 240;
   values[33] = 2688*x*x*z*z - 476*z + 2688*y*y*z*z + 1176*x*z + 1176*y*z - 6384*x*z*z - 924*x*x*z + 6720*x*z*z*z + 224*x*x*x*z - 6384*y*z*z - 924*y*y*z + 6720*y*z*z*z + 224*y*y*y*z + 3696*z*z - 7560*z*z*z + 4480*z*z*z*z + 5376*x*y*z*z + 672*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[34] = 2688*x*x*z*z - 476*z + 2688*y*y*z*z + 1176*x*z + 1176*y*z - 6384*x*z*z - 924*x*x*z + 6720*x*z*z*z + 224*x*x*x*z - 6384*y*z*z - 924*y*y*z + 6720*y*z*z*z + 224*y*y*y*z + 3696*z*z - 7560*z*z*z + 4480*z*z*z*z + 5376*x*y*z*z + 672*x*y*y*z + 672*x*x*y*z - 1848*x*y*z;
   values[35] = 644*x + 644*y + 1680*z - 1344*x*x*y*y - 6720*x*x*z*z - 6720*y*y*z*z - 2184*x*y - 6048*x*z - 6048*y*z + 2436*x*y*y + 2436*x*x*y - 896*x*y*y*y - 896*x*x*x*y + 10920*x*z*z + 7056*x*x*z - 4480*x*z*z*z - 2688*x*x*x*z + 10920*y*z*z + 7056*y*y*z - 4480*y*z*z*z - 2688*y*y*y*z - 1092*x*x + 812*x*x*x - 224*x*x*x*x - 1092*y*y + 812*y*y*y - 224*y*y*y*y - 4200*z*z + 2800*z*z*z - 13440*x*y*z*z - 8064*x*y*y*z - 8064*x*x*y*z + 14112*x*y*z - 140;
   values[36] = 4*y - 72*x*y + 252*x*x*y - 224*x*x*x*y;
   values[37] = 144*x*x - 16*x - 336*x*x*x + 224*x*x*x*x;
   values[38] = 0;
   values[39] = 12*y - 2016*x*x*y*y - 72*x*y + 1008*x*y*y - 252*x*x*y + 672*x*x*x*y - 72*y*y;
   values[40] = 12*x + 216*x*y - 1512*x*x*y + 2016*x*x*x*y - 216*x*x + 756*x*x*x - 672*x*x*x*x;
   values[41] = 0;
   values[42] = 4032*x*x*y*y + 144*x*y - 504*x*y*y - 252*x*x*y - 2016*x*y*y*y - 672*x*x*x*y - 72*y*y + 252*y*y*y;
   values[43] = 2016*x*x*y*y - 144*x*y - 504*x*y*y + 2016*x*x*y - 4032*x*x*x*y + 72*x*x - 504*x*x*x + 672*x*x*x*x;
   values[44] = 0;
   values[45] = 252*x*x*y - 504*x*y*y - 2016*x*x*y*y + 2016*x*y*y*y + 224*x*x*x*y + 84*y*y*y - 224*y*y*y*y;
   values[46] = 252*x*y*y - 2016*x*x*y*y - 504*x*x*y + 224*x*y*y*y + 2016*x*x*x*y + 84*x*x*x - 224*x*x*x*x;
   values[47] = 0;
   values[48] = 4*z - 72*x*z + 252*x*x*z - 224*x*x*x*z;
   values[49] = 0;
   values[50] = 144*x*x - 16*x - 336*x*x*x + 224*x*x*x*x;
   values[51] = 12*z - 2016*x*x*z*z - 72*x*z + 1008*x*z*z - 252*x*x*z + 672*x*x*x*z - 72*z*z;
   values[52] = 0;
   values[53] = 12*x + 216*x*z - 1512*x*x*z + 2016*x*x*x*z - 216*x*x + 756*x*x*x - 672*x*x*x*x;
   values[54] = 4032*x*x*z*z + 144*x*z - 504*x*z*z - 252*x*x*z - 2016*x*z*z*z - 672*x*x*x*z - 72*z*z + 252*z*z*z;
   values[55] = 0;
   values[56] = 2016*x*x*z*z - 144*x*z - 504*x*z*z + 2016*x*x*z - 4032*x*x*x*z + 72*x*x - 504*x*x*x + 672*x*x*x*x;
   values[57] = 252*x*x*z - 504*x*z*z - 2016*x*x*z*z + 2016*x*z*z*z + 224*x*x*x*z + 84*z*z*z - 224*z*z*z*z;
   values[58] = 0;
   values[59] = 252*x*z*z - 2016*x*x*z*z - 504*x*x*z + 224*x*z*z*z + 2016*x*x*x*z + 84*x*x*x - 224*x*x*x*x;
   values[60] = 0;
   values[61] = 4*z - 72*y*z + 252*y*y*z - 224*y*y*y*z;
   values[62] = 144*y*y - 16*y - 336*y*y*y + 224*y*y*y*y;
   values[63] = 0;
   values[64] = 12*z - 2016*y*y*z*z - 72*y*z + 1008*y*z*z - 252*y*y*z + 672*y*y*y*z - 72*z*z;
   values[65] = 12*y + 216*y*z - 1512*y*y*z + 2016*y*y*y*z - 216*y*y + 756*y*y*y - 672*y*y*y*y;
   values[66] = 0;
   values[67] = 4032*y*y*z*z + 144*y*z - 504*y*z*z - 252*y*y*z - 2016*y*z*z*z - 672*y*y*y*z - 72*z*z + 252*z*z*z;
   values[68] = 2016*y*y*z*z - 144*y*z - 504*y*z*z + 2016*y*y*z - 4032*y*y*y*z + 72*y*y - 504*y*y*y + 672*y*y*y*y;
   values[69] = 0;
   values[70] = 252*y*y*z - 504*y*z*z - 2016*y*y*z*z + 2016*y*z*z*z + 224*y*y*y*z + 84*z*z*z - 224*z*z*z*z;
   values[71] = 252*y*z*z - 2016*y*y*z*z - 504*y*y*z + 224*y*z*z*z + 2016*y*y*y*z + 84*y*y*y - 224*y*y*y*y;
   values[72] = 504*x*y*z - 1008*x*x*y*z - 36*y*z;
   values[73] = 144*x*z - 1008*x*x*z + 1344*x*x*x*z;
   values[74] = 252*x*x*y - 36*x*y - 336*x*x*x*y;
   values[75] = 252*y*z*z - 2016*x*y*z*z + 2016*x*x*y*z - 504*x*y*z;
   values[76] = 4032*x*x*z*z - 1008*x*z*z + 1008*x*x*z - 2688*x*x*x*z;
   values[77] = 672*x*x*x*y - 252*x*x*y - 2016*x*x*y*z + 504*x*y*z;
   values[78] = 2016*x*y*z*z - 336*y*z*z*z - 1008*x*x*y*z;
   values[79] = 1344*x*z*z*z - 4032*x*x*z*z + 1344*x*x*x*z;
   values[80] = 2016*x*x*y*z - 1008*x*y*z*z - 336*x*x*x*y;
   values[81] = 504*y*y*z - 72*y*z - 4032*x*y*y*z + 2016*x*x*y*z;
   values[82] = 1260*x*x*z - 72*x*z - 2688*x*x*x*z + 6048*x*x*y*z - 1512*x*y*z;
   values[83] = 504*x*y*y - 72*x*y - 2016*x*x*y*y + 672*x*x*x*y;
   values[84] = 252*y*z*z - 2016*y*y*z*z + 2016*x*y*z*z + 4032*x*y*y*z - 2016*x*x*y*z - 504*x*y*z;
   values[85] = 252*x*z*z - 4032*x*x*z*z - 252*x*x*z + 2688*x*x*x*z + 6048*x*y*z*z - 6048*x*x*y*z;
   values[86] = 2016*x*x*y*y - 252*x*x*y - 672*x*x*x*y - 4032*x*y*y*z + 2016*x*x*y*z + 504*x*y*z;
   values[87] = 252*y*y*z - 1008*y*y*y*z + 4032*x*y*y*z - 1008*x*x*y*z - 504*x*y*z;
   values[88] = 1344*x*x*x*z - 252*x*x*z + 2016*x*y*y*z - 6048*x*x*y*z + 504*x*y*z;
   values[89] = 2016*x*x*y*y + 252*x*y*y - 252*x*x*y - 1008*x*y*y*y - 336*x*x*x*y;
   values[90] = 504*x*y*z - 1008*x*x*y*z - 36*y*z;
   values[91] = 252*x*x*z - 36*x*z - 336*x*x*x*z;
   values[92] = 144*x*y - 1008*x*x*y + 1344*x*x*x*y;
   values[93] = 504*y*z*z - 72*y*z - 4032*x*y*z*z + 2016*x*x*y*z;
   values[94] = 504*x*z*z - 72*x*z - 2016*x*x*z*z + 672*x*x*x*z;
   values[95] = 1260*x*x*y - 72*x*y - 2688*x*x*x*y + 6048*x*x*y*z - 1512*x*y*z;
   values[96] = 252*y*z*z - 1008*y*z*z*z + 4032*x*y*z*z - 1008*x*x*y*z - 504*x*y*z;
   values[97] = 2016*x*x*z*z + 252*x*z*z - 252*x*x*z - 1008*x*z*z*z - 336*x*x*x*z;
   values[98] = 1344*x*x*x*y - 252*x*x*y + 2016*x*y*z*z - 6048*x*x*y*z + 504*x*y*z;
   values[99] = 252*y*y*z - 2016*x*y*y*z + 2016*x*x*y*z - 504*x*y*z;
   values[100] = 672*x*x*x*z - 252*x*x*z - 2016*x*x*y*z + 504*x*y*z;
   values[101] = 4032*x*x*y*y - 1008*x*y*y + 1008*x*x*y - 2688*x*x*x*y;
   values[102] = 252*y*y*z - 2016*y*y*z*z + 4032*x*y*z*z + 2016*x*y*y*z - 2016*x*x*y*z - 504*x*y*z;
   values[103] = 2016*x*x*z*z - 252*x*x*z - 672*x*x*x*z - 4032*x*y*z*z + 2016*x*x*y*z + 504*x*y*z;
   values[104] = 252*x*y*y - 4032*x*x*y*y - 252*x*x*y + 2688*x*x*x*y + 6048*x*y*y*z - 6048*x*x*y*z;
   values[105] = 2016*x*y*y*z - 336*y*y*y*z - 1008*x*x*y*z;
   values[106] = 2016*x*x*y*z - 1008*x*y*y*z - 336*x*x*x*z;
   values[107] = 1344*x*y*y*y - 4032*x*x*y*y + 1344*x*x*x*y;
   values[108] = 2016*y*y*z*z + 540*y*z - 1512*y*z*z - 1512*y*y*z + 1008*y*z*z*z + 1008*y*y*y*z + 2016*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 1512*x*y*z;
   values[109] = 480*z - 4032*x*x*z*z - 2016*y*y*z*z - 2160*x*z - 1620*y*z + 6048*x*z*z + 3024*x*x*z - 4032*x*z*z*z - 1344*x*x*x*z + 4536*y*z*z + 1512*y*y*z - 3024*y*z*z*z - 336*y*y*y*z - 2160*z*z + 3024*z*z*z - 1344*z*z*z*z - 6048*x*y*z*z - 2016*x*y*y*z - 3024*x*x*y*z + 4536*x*y*z;
   values[110] = 1008*x*x*y*y - 120*y + 3024*y*y*z*z + 540*x*y + 1080*y*z - 1512*x*y*y - 756*x*x*y + 1008*x*y*y*y + 336*x*x*x*y - 2268*y*z*z - 3024*y*y*z + 1344*y*z*z*z + 2016*y*y*y*z + 540*y*y - 756*y*y*y + 336*y*y*y*y + 3024*x*y*z*z + 4032*x*y*y*z + 2016*x*x*y*z - 3024*x*y*z;
   values[111] = 5292*y*z*z - 1512*y*z - 6048*y*y*z*z + 3528*y*y*z - 4032*y*z*z*z - 2016*y*y*y*z - 6048*x*y*z*z - 4032*x*y*y*z - 2016*x*x*y*z + 3528*x*y*z;
   values[112] = 12096*x*x*z*z - 1680*z + 6048*y*y*z*z + 6048*x*z + 4536*y*z - 21168*x*z*z - 7056*x*x*z + 16128*x*z*z*z + 2688*x*x*x*z - 15876*y*z*z - 3528*y*y*z + 12096*y*z*z*z + 672*y*y*y*z + 9072*z*z - 14112*z*z*z + 6720*z*z*z*z + 18144*x*y*z*z + 4032*x*y*y*z + 6048*x*x*y*z - 10584*x*y*z;
   values[113] = 420*y - 2016*x*x*y*y - 12096*y*y*z*z - 1512*x*y - 4536*y*z + 3528*x*y*y + 1764*x*x*y - 2016*x*y*y*y - 672*x*x*x*y + 10584*y*z*z + 10584*y*y*z - 6720*y*z*z*z - 6048*y*y*y*z - 1512*y*y + 1764*y*y*y - 672*y*y*y*y - 12096*x*y*z*z - 12096*x*y*y*z - 6048*x*x*y*z + 10584*x*y*z;
   values[114] = 4032*y*y*z*z + 1008*y*z - 4032*y*z*z - 2016*y*y*z + 3360*y*z*z*z + 1008*y*y*y*z + 4032*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 2016*x*y*z;
   values[115] = 1344*z - 8064*x*x*z*z - 4032*y*y*z*z - 4032*x*z - 3024*y*z + 16128*x*z*z + 4032*x*x*z - 13440*x*z*z*z - 1344*x*x*x*z + 12096*y*z*z + 2016*y*y*z - 10080*y*z*z*z - 336*y*y*y*z - 8064*z*z + 13440*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 2016*x*y*y*z - 3024*x*x*y*z + 6048*x*y*z;
   values[116] = 1008*x*x*y*y - 336*y + 10080*y*y*z*z + 1008*x*y + 4032*y*z - 2016*x*y*y - 1008*x*x*y + 1008*x*y*y*y + 336*x*x*x*y - 10080*y*z*z - 8064*y*y*z + 6720*y*z*z*z + 4032*y*y*y*z + 1008*y*y - 1008*y*y*y + 336*y*y*y*y + 10080*x*y*z*z + 8064*x*y*y*z + 4032*x*x*y*z - 8064*x*y*z;
   values[117] = 4032*y*z*z - 1944*y*z - 8064*y*y*z*z + 7560*y*y*z - 2016*y*z*z*z - 6048*y*y*y*z - 4032*x*y*z*z - 8064*x*y*y*z - 2016*x*x*y*z + 4032*x*y*z;
   values[118] = 8064*x*x*z*z - 1500*z + 12096*y*y*z*z + 5616*x*z + 8208*y*z - 13608*x*z*z - 6804*x*x*z + 8064*x*z*z*z + 2688*x*x*x*z - 20160*y*z*z - 9828*y*y*z + 12096*y*z*z*z + 2688*y*y*y*z + 5616*z*z - 6804*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[119] = 600*y - 4032*x*x*y*y - 12096*y*y*z*z - 1944*x*y - 3888*y*z + 7560*x*y*y + 2016*x*x*y - 6048*x*y*y*y - 672*x*x*x*y + 6048*y*z*z + 15120*y*y*z - 2688*y*z*z*z - 12096*y*y*y*z - 3456*y*y + 5544*y*y*y - 2688*y*y*y*y - 6048*x*y*z*z - 16128*x*y*y*z - 4032*x*x*y*z + 8064*x*y*z;
   values[120] = 12096*y*y*z*z + 2520*y*z - 6804*y*z*z - 8568*y*y*z + 4032*y*z*z*z + 6048*y*y*y*z + 6048*x*y*z*z + 8064*x*y*y*z + 2016*x*x*y*z - 4536*x*y*z;
   values[121] = 2436*z - 12096*x*x*z*z - 18144*y*y*z*z - 7560*x*z - 11088*y*z + 23436*x*z*z + 7812*x*x*z - 16128*x*z*z*z - 2688*x*x*x*z + 34776*y*z*z + 11340*y*y*z - 24192*y*z*z*z - 2688*y*y*y*z - 11340*z*z + 15624*z*z*z - 6720*z*z*z*z - 36288*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[122] = 4032*x*x*y*y - 924*y + 24192*y*y*z*z + 2520*x*y + 7560*y*z - 8568*x*y*y - 2268*x*x*y + 6048*x*y*y*y + 672*x*x*x*y - 13608*y*z*z - 25704*y*y*z + 6720*y*z*z*z + 18144*y*y*y*z + 4536*y*y - 6300*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 6048*x*x*y*z - 13608*x*y*z;
   values[123] = 6048*y*y*z*z + 1512*y*z - 2520*y*z*z - 6804*y*y*z + 1008*y*z*z*z + 6048*y*y*y*z + 2016*x*y*z*z + 6048*x*y*y*z + 1008*x*x*y*z - 2520*x*y*z;
   values[124] = 1092*z - 4032*x*x*z*z - 12096*y*y*z*z - 3528*x*z - 7560*y*z + 7560*x*z*z + 3780*x*x*z - 4032*x*z*z*z - 1344*x*x*x*z + 16632*y*z*z + 10584*y*y*z - 9072*y*z*z*z - 3360*y*y*y*z - 3528*z*z + 3780*z*z*z - 1344*z*z*z*z - 18144*x*y*z*z - 12096*x*y*y*z - 9072*x*x*y*z + 16632*x*y*z;
   values[125] = 3024*x*x*y*y - 588*y + 9072*y*y*z*z + 1512*x*y + 3024*y*z - 6804*x*y*y - 1260*x*x*y + 6048*x*y*y*y + 336*x*x*x*y - 3780*y*z*z - 13608*y*y*z + 1344*y*z*z*z + 12096*y*y*y*z + 3780*y*y - 6552*y*y*y + 3360*y*y*y*y + 3024*x*y*z*z + 12096*x*y*y*z + 2016*x*x*y*z - 5040*x*y*z;
   values[126] = 2016*y*y*z*z + 540*y*z - 1512*y*z*z - 1512*y*y*z + 1008*y*z*z*z + 1008*y*y*y*z + 2016*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 1512*x*y*z;
   values[127] = 1008*x*x*z*z - 120*z + 3024*y*y*z*z + 540*x*z + 1080*y*z - 1512*x*z*z - 756*x*x*z + 1008*x*z*z*z + 336*x*x*x*z - 3024*y*z*z - 2268*y*y*z + 2016*y*z*z*z + 1344*y*y*y*z + 540*z*z - 756*z*z*z + 336*z*z*z*z + 4032*x*y*z*z + 3024*x*y*y*z + 2016*x*x*y*z - 3024*x*y*z;
   values[128] = 480*y - 4032*x*x*y*y - 2016*y*y*z*z - 2160*x*y - 1620*y*z + 6048*x*y*y + 3024*x*x*y - 4032*x*y*y*y - 1344*x*x*x*y + 1512*y*z*z + 4536*y*y*z - 336*y*z*z*z - 3024*y*y*y*z - 2160*y*y + 3024*y*y*y - 1344*y*y*y*y - 2016*x*y*z*z - 6048*x*y*y*z - 3024*x*x*y*z + 4536*x*y*z;
   values[129] = 7560*y*z*z - 1944*y*z - 8064*y*y*z*z + 4032*y*y*z - 6048*y*z*z*z - 2016*y*y*y*z - 8064*x*y*z*z - 4032*x*y*y*z - 2016*x*x*y*z + 4032*x*y*z;
   values[130] = 600*z - 4032*x*x*z*z - 12096*y*y*z*z - 1944*x*z - 3888*y*z + 7560*x*z*z + 2016*x*x*z - 6048*x*z*z*z - 672*x*x*x*z + 15120*y*z*z + 6048*y*y*z - 12096*y*z*z*z - 2688*y*y*y*z - 3456*z*z + 5544*z*z*z - 2688*z*z*z*z - 16128*x*y*z*z - 6048*x*y*y*z - 4032*x*x*y*z + 8064*x*y*z;
   values[131] = 8064*x*x*y*y - 1500*y + 12096*y*y*z*z + 5616*x*y + 8208*y*z - 13608*x*y*y - 6804*x*x*y + 8064*x*y*y*y + 2688*x*x*x*y - 9828*y*z*z - 20160*y*y*z + 2688*y*z*z*z + 12096*y*y*y*z + 5616*y*y - 6804*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[132] = 6048*y*y*z*z + 1512*y*z - 6804*y*z*z - 2520*y*y*z + 6048*y*z*z*z + 1008*y*y*y*z + 6048*x*y*z*z + 2016*x*y*y*z + 1008*x*x*y*z - 2520*x*y*z;
   values[133] = 3024*x*x*z*z - 588*z + 9072*y*y*z*z + 1512*x*z + 3024*y*z - 6804*x*z*z - 1260*x*x*z + 6048*x*z*z*z + 336*x*x*x*z - 13608*y*z*z - 3780*y*y*z + 12096*y*z*z*z + 1344*y*y*y*z + 3780*z*z - 6552*z*z*z + 3360*z*z*z*z + 12096*x*y*z*z + 3024*x*y*y*z + 2016*x*x*y*z - 5040*x*y*z;
   values[134] = 1092*y - 4032*x*x*y*y - 12096*y*y*z*z - 3528*x*y - 7560*y*z + 7560*x*y*y + 3780*x*x*y - 4032*x*y*y*y - 1344*x*x*x*y + 10584*y*z*z + 16632*y*y*z - 3360*y*z*z*z - 9072*y*y*y*z - 3528*y*y + 3780*y*y*y - 1344*y*y*y*y - 12096*x*y*z*z - 18144*x*y*y*z - 9072*x*x*y*z + 16632*x*y*z;
   values[135] = 3528*y*z*z - 1512*y*z - 6048*y*y*z*z + 5292*y*y*z - 2016*y*z*z*z - 4032*y*y*y*z - 4032*x*y*z*z - 6048*x*y*y*z - 2016*x*x*y*z + 3528*x*y*z;
   values[136] = 420*z - 2016*x*x*z*z - 12096*y*y*z*z - 1512*x*z - 4536*y*z + 3528*x*z*z + 1764*x*x*z - 2016*x*z*z*z - 672*x*x*x*z + 10584*y*z*z + 10584*y*y*z - 6048*y*z*z*z - 6720*y*y*y*z - 1512*z*z + 1764*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 12096*x*y*y*z - 6048*x*x*y*z + 10584*x*y*z;
   values[137] = 12096*x*x*y*y - 1680*y + 6048*y*y*z*z + 6048*x*y + 4536*y*z - 21168*x*y*y - 7056*x*x*y + 16128*x*y*y*y + 2688*x*x*x*y - 3528*y*z*z - 15876*y*y*z + 672*y*z*z*z + 12096*y*y*y*z + 9072*y*y - 14112*y*y*y + 6720*y*y*y*y + 4032*x*y*z*z + 18144*x*y*y*z + 6048*x*x*y*z - 10584*x*y*z;
   values[138] = 12096*y*y*z*z + 2520*y*z - 8568*y*z*z - 6804*y*y*z + 6048*y*z*z*z + 4032*y*y*y*z + 8064*x*y*z*z + 6048*x*y*y*z + 2016*x*x*y*z - 4536*x*y*z;
   values[139] = 4032*x*x*z*z - 924*z + 24192*y*y*z*z + 2520*x*z + 7560*y*z - 8568*x*z*z - 2268*x*x*z + 6048*x*z*z*z + 672*x*x*x*z - 25704*y*z*z - 13608*y*y*z + 18144*y*z*z*z + 6720*y*y*y*z + 4536*z*z - 6300*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 6048*x*x*y*z - 13608*x*y*z;
   values[140] = 2436*y - 12096*x*x*y*y - 18144*y*y*z*z - 7560*x*y - 11088*y*z + 23436*x*y*y + 7812*x*x*y - 16128*x*y*y*y - 2688*x*x*x*y + 11340*y*z*z + 34776*y*y*z - 2688*y*z*z*z - 24192*y*y*y*z - 11340*y*y + 15624*y*y*y - 6720*y*y*y*y - 12096*x*y*z*z - 36288*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[141] = 4032*y*y*z*z + 1008*y*z - 2016*y*z*z - 4032*y*y*z + 1008*y*z*z*z + 3360*y*y*y*z + 2016*x*y*z*z + 4032*x*y*y*z + 1008*x*x*y*z - 2016*x*y*z;
   values[142] = 1008*x*x*z*z - 336*z + 10080*y*y*z*z + 1008*x*z + 4032*y*z - 2016*x*z*z - 1008*x*x*z + 1008*x*z*z*z + 336*x*x*x*z - 8064*y*z*z - 10080*y*y*z + 4032*y*z*z*z + 6720*y*y*y*z + 1008*z*z - 1008*z*z*z + 336*z*z*z*z + 8064*x*y*z*z + 10080*x*y*y*z + 4032*x*x*y*z - 8064*x*y*z;
   values[143] = 1344*y - 8064*x*x*y*y - 4032*y*y*z*z - 4032*x*y - 3024*y*z + 16128*x*y*y + 4032*x*x*y - 13440*x*y*y*y - 1344*x*x*x*y + 2016*y*z*z + 12096*y*y*z - 336*y*z*z*z - 10080*y*y*y*z - 8064*y*y + 13440*y*y*y - 6720*y*y*y*y - 2016*x*y*z*z - 12096*x*y*y*z - 3024*x*x*y*z + 6048*x*y*z;
   values[144] = 480*z - 2016*x*x*z*z - 4032*y*y*z*z - 1620*x*z - 2160*y*z + 4536*x*z*z + 1512*x*x*z - 3024*x*z*z*z - 336*x*x*x*z + 6048*y*z*z + 3024*y*y*z - 4032*y*z*z*z - 1344*y*y*y*z - 2160*z*z + 3024*z*z*z - 1344*z*z*z*z - 6048*x*y*z*z - 3024*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[145] = 2016*x*x*z*z + 540*x*z - 1512*x*z*z - 1512*x*x*z + 1008*x*z*z*z + 1008*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[146] = 1008*x*x*y*y - 120*x + 3024*x*x*z*z + 540*x*y + 1080*x*z - 756*x*y*y - 1512*x*x*y + 336*x*y*y*y + 1008*x*x*x*y - 2268*x*z*z - 3024*x*x*z + 1344*x*z*z*z + 2016*x*x*x*z + 540*x*x - 756*x*x*x + 336*x*x*x*x + 3024*x*y*z*z + 2016*x*y*y*z + 4032*x*x*y*z - 3024*x*y*z;
   values[147] = 6048*x*x*z*z - 1680*z + 12096*y*y*z*z + 4536*x*z + 6048*y*z - 15876*x*z*z - 3528*x*x*z + 12096*x*z*z*z + 672*x*x*x*z - 21168*y*z*z - 7056*y*y*z + 16128*y*z*z*z + 2688*y*y*y*z + 9072*z*z - 14112*z*z*z + 6720*z*z*z*z + 18144*x*y*z*z + 6048*x*y*y*z + 4032*x*x*y*z - 10584*x*y*z;
   values[148] = 5292*x*z*z - 1512*x*z - 6048*x*x*z*z + 3528*x*x*z - 4032*x*z*z*z - 2016*x*x*x*z - 6048*x*y*z*z - 2016*x*y*y*z - 4032*x*x*y*z + 3528*x*y*z;
   values[149] = 420*x - 2016*x*x*y*y - 12096*x*x*z*z - 1512*x*y - 4536*x*z + 1764*x*y*y + 3528*x*x*y - 672*x*y*y*y - 2016*x*x*x*y + 10584*x*z*z + 10584*x*x*z - 6720*x*z*z*z - 6048*x*x*x*z - 1512*x*x + 1764*x*x*x - 672*x*x*x*x - 12096*x*y*z*z - 6048*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[150] = 1344*z - 4032*x*x*z*z - 8064*y*y*z*z - 3024*x*z - 4032*y*z + 12096*x*z*z + 2016*x*x*z - 10080*x*z*z*z - 336*x*x*x*z + 16128*y*z*z + 4032*y*y*z - 13440*y*z*z*z - 1344*y*y*y*z - 8064*z*z + 13440*z*z*z - 6720*z*z*z*z - 12096*x*y*z*z - 3024*x*y*y*z - 2016*x*x*y*z + 6048*x*y*z;
   values[151] = 4032*x*x*z*z + 1008*x*z - 4032*x*z*z - 2016*x*x*z + 3360*x*z*z*z + 1008*x*x*x*z + 4032*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 2016*x*y*z;
   values[152] = 1008*x*x*y*y - 336*x + 10080*x*x*z*z + 1008*x*y + 4032*x*z - 1008*x*y*y - 2016*x*x*y + 336*x*y*y*y + 1008*x*x*x*y - 10080*x*z*z - 8064*x*x*z + 6720*x*z*z*z + 4032*x*x*x*z + 1008*x*x - 1008*x*x*x + 336*x*x*x*x + 10080*x*y*z*z + 4032*x*y*y*z + 8064*x*x*y*z - 8064*x*y*z;
   values[153] = 12096*x*x*z*z - 1500*z + 8064*y*y*z*z + 8208*x*z + 5616*y*z - 20160*x*z*z - 9828*x*x*z + 12096*x*z*z*z + 2688*x*x*x*z - 13608*y*z*z - 6804*y*y*z + 8064*y*z*z*z + 2688*y*y*y*z + 5616*z*z - 6804*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 12096*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[154] = 4032*x*z*z - 1944*x*z - 8064*x*x*z*z + 7560*x*x*z - 2016*x*z*z*z - 6048*x*x*x*z - 4032*x*y*z*z - 2016*x*y*y*z - 8064*x*x*y*z + 4032*x*y*z;
   values[155] = 600*x - 4032*x*x*y*y - 12096*x*x*z*z - 1944*x*y - 3888*x*z + 2016*x*y*y + 7560*x*x*y - 672*x*y*y*y - 6048*x*x*x*y + 6048*x*z*z + 15120*x*x*z - 2688*x*z*z*z - 12096*x*x*x*z - 3456*x*x + 5544*x*x*x - 2688*x*x*x*x - 6048*x*y*z*z - 4032*x*y*y*z - 16128*x*x*y*z + 8064*x*y*z;
   values[156] = 2436*z - 18144*x*x*z*z - 12096*y*y*z*z - 11088*x*z - 7560*y*z + 34776*x*z*z + 11340*x*x*z - 24192*x*z*z*z - 2688*x*x*x*z + 23436*y*z*z + 7812*y*y*z - 16128*y*z*z*z - 2688*y*y*y*z - 11340*z*z + 15624*z*z*z - 6720*z*z*z*z - 36288*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[157] = 12096*x*x*z*z + 2520*x*z - 6804*x*z*z - 8568*x*x*z + 4032*x*z*z*z + 6048*x*x*x*z + 6048*x*y*z*z + 2016*x*y*y*z + 8064*x*x*y*z - 4536*x*y*z;
   values[158] = 4032*x*x*y*y - 924*x + 24192*x*x*z*z + 2520*x*y + 7560*x*z - 2268*x*y*y - 8568*x*x*y + 672*x*y*y*y + 6048*x*x*x*y - 13608*x*z*z - 25704*x*x*z + 6720*x*z*z*z + 18144*x*x*x*z + 4536*x*x - 6300*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 6048*x*y*y*z + 24192*x*x*y*z - 13608*x*y*z;
   values[159] = 1092*z - 12096*x*x*z*z - 4032*y*y*z*z - 7560*x*z - 3528*y*z + 16632*x*z*z + 10584*x*x*z - 9072*x*z*z*z - 3360*x*x*x*z + 7560*y*z*z + 3780*y*y*z - 4032*y*z*z*z - 1344*y*y*y*z - 3528*z*z + 3780*z*z*z - 1344*z*z*z*z - 18144*x*y*z*z - 9072*x*y*y*z - 12096*x*x*y*z + 16632*x*y*z;
   values[160] = 6048*x*x*z*z + 1512*x*z - 2520*x*z*z - 6804*x*x*z + 1008*x*z*z*z + 6048*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 6048*x*x*y*z - 2520*x*y*z;
   values[161] = 3024*x*x*y*y - 588*x + 9072*x*x*z*z + 1512*x*y + 3024*x*z - 1260*x*y*y - 6804*x*x*y + 336*x*y*y*y + 6048*x*x*x*y - 3780*x*z*z - 13608*x*x*z + 1344*x*z*z*z + 12096*x*x*x*z + 3780*x*x - 6552*x*x*x + 3360*x*x*x*x + 3024*x*y*z*z + 2016*x*y*y*z + 12096*x*x*y*z - 5040*x*y*z;
   values[162] = 3024*x*x*z*z - 120*z + 1008*y*y*z*z + 1080*x*z + 540*y*z - 3024*x*z*z - 2268*x*x*z + 2016*x*z*z*z + 1344*x*x*x*z - 1512*y*z*z - 756*y*y*z + 1008*y*z*z*z + 336*y*y*y*z + 540*z*z - 756*z*z*z + 336*z*z*z*z + 4032*x*y*z*z + 2016*x*y*y*z + 3024*x*x*y*z - 3024*x*y*z;
   values[163] = 2016*x*x*z*z + 540*x*z - 1512*x*z*z - 1512*x*x*z + 1008*x*z*z*z + 1008*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[164] = 480*x - 4032*x*x*y*y - 2016*x*x*z*z - 2160*x*y - 1620*x*z + 3024*x*y*y + 6048*x*x*y - 1344*x*y*y*y - 4032*x*x*x*y + 1512*x*z*z + 4536*x*x*z - 336*x*z*z*z - 3024*x*x*x*z - 2160*x*x + 3024*x*x*x - 1344*x*x*x*x - 2016*x*y*z*z - 3024*x*y*y*z - 6048*x*x*y*z + 4536*x*y*z;
   values[165] = 600*z - 12096*x*x*z*z - 4032*y*y*z*z - 3888*x*z - 1944*y*z + 15120*x*z*z + 6048*x*x*z - 12096*x*z*z*z - 2688*x*x*x*z + 7560*y*z*z + 2016*y*y*z - 6048*y*z*z*z - 672*y*y*y*z - 3456*z*z + 5544*z*z*z - 2688*z*z*z*z - 16128*x*y*z*z - 4032*x*y*y*z - 6048*x*x*y*z + 8064*x*y*z;
   values[166] = 7560*x*z*z - 1944*x*z - 8064*x*x*z*z + 4032*x*x*z - 6048*x*z*z*z - 2016*x*x*x*z - 8064*x*y*z*z - 2016*x*y*y*z - 4032*x*x*y*z + 4032*x*y*z;
   values[167] = 8064*x*x*y*y - 1500*x + 12096*x*x*z*z + 5616*x*y + 8208*x*z - 6804*x*y*y - 13608*x*x*y + 2688*x*y*y*y + 8064*x*x*x*y - 9828*x*z*z - 20160*x*x*z + 2688*x*z*z*z + 12096*x*x*x*z + 5616*x*x - 6804*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 20160*x*y*z;
   values[168] = 9072*x*x*z*z - 588*z + 3024*y*y*z*z + 3024*x*z + 1512*y*z - 13608*x*z*z - 3780*x*x*z + 12096*x*z*z*z + 1344*x*x*x*z - 6804*y*z*z - 1260*y*y*z + 6048*y*z*z*z + 336*y*y*y*z + 3780*z*z - 6552*z*z*z + 3360*z*z*z*z + 12096*x*y*z*z + 2016*x*y*y*z + 3024*x*x*y*z - 5040*x*y*z;
   values[169] = 6048*x*x*z*z + 1512*x*z - 6804*x*z*z - 2520*x*x*z + 6048*x*z*z*z + 1008*x*x*x*z + 6048*x*y*z*z + 1008*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z;
   values[170] = 1092*x - 4032*x*x*y*y - 12096*x*x*z*z - 3528*x*y - 7560*x*z + 3780*x*y*y + 7560*x*x*y - 1344*x*y*y*y - 4032*x*x*x*y + 10584*x*z*z + 16632*x*x*z - 3360*x*z*z*z - 9072*x*x*x*z - 3528*x*x + 3780*x*x*x - 1344*x*x*x*x - 12096*x*y*z*z - 9072*x*y*y*z - 18144*x*x*y*z + 16632*x*y*z;
   values[171] = 420*z - 12096*x*x*z*z - 2016*y*y*z*z - 4536*x*z - 1512*y*z + 10584*x*z*z + 10584*x*x*z - 6048*x*z*z*z - 6720*x*x*x*z + 3528*y*z*z + 1764*y*y*z - 2016*y*z*z*z - 672*y*y*y*z - 1512*z*z + 1764*z*z*z - 672*z*z*z*z - 12096*x*y*z*z - 6048*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[172] = 3528*x*z*z - 1512*x*z - 6048*x*x*z*z + 5292*x*x*z - 2016*x*z*z*z - 4032*x*x*x*z - 4032*x*y*z*z - 2016*x*y*y*z - 6048*x*x*y*z + 3528*x*y*z;
   values[173] = 12096*x*x*y*y - 1680*x + 6048*x*x*z*z + 6048*x*y + 4536*x*z - 7056*x*y*y - 21168*x*x*y + 2688*x*y*y*y + 16128*x*x*x*y - 3528*x*z*z - 15876*x*x*z + 672*x*z*z*z + 12096*x*x*x*z + 9072*x*x - 14112*x*x*x + 6720*x*x*x*x + 4032*x*y*z*z + 6048*x*y*y*z + 18144*x*x*y*z - 10584*x*y*z;
   values[174] = 24192*x*x*z*z - 924*z + 4032*y*y*z*z + 7560*x*z + 2520*y*z - 25704*x*z*z - 13608*x*x*z + 18144*x*z*z*z + 6720*x*x*x*z - 8568*y*z*z - 2268*y*y*z + 6048*y*z*z*z + 672*y*y*y*z + 4536*z*z - 6300*z*z*z + 2688*z*z*z*z + 24192*x*y*z*z + 6048*x*y*y*z + 12096*x*x*y*z - 13608*x*y*z;
   values[175] = 12096*x*x*z*z + 2520*x*z - 8568*x*z*z - 6804*x*x*z + 6048*x*z*z*z + 4032*x*x*x*z + 8064*x*y*z*z + 2016*x*y*y*z + 6048*x*x*y*z - 4536*x*y*z;
   values[176] = 2436*x - 12096*x*x*y*y - 18144*x*x*z*z - 7560*x*y - 11088*x*z + 7812*x*y*y + 23436*x*x*y - 2688*x*y*y*y - 16128*x*x*x*y + 11340*x*z*z + 34776*x*x*z - 2688*x*z*z*z - 24192*x*x*x*z - 11340*x*x + 15624*x*x*x - 6720*x*x*x*x - 12096*x*y*z*z - 12096*x*y*y*z - 36288*x*x*y*z + 23184*x*y*z;
   values[177] = 10080*x*x*z*z - 336*z + 1008*y*y*z*z + 4032*x*z + 1008*y*z - 8064*x*z*z - 10080*x*x*z + 4032*x*z*z*z + 6720*x*x*x*z - 2016*y*z*z - 1008*y*y*z + 1008*y*z*z*z + 336*y*y*y*z + 1008*z*z - 1008*z*z*z + 336*z*z*z*z + 8064*x*y*z*z + 4032*x*y*y*z + 10080*x*x*y*z - 8064*x*y*z;
   values[178] = 4032*x*x*z*z + 1008*x*z - 2016*x*z*z - 4032*x*x*z + 1008*x*z*z*z + 3360*x*x*x*z + 2016*x*y*z*z + 1008*x*y*y*z + 4032*x*x*y*z - 2016*x*y*z;
   values[179] = 1344*x - 8064*x*x*y*y - 4032*x*x*z*z - 4032*x*y - 3024*x*z + 4032*x*y*y + 16128*x*x*y - 1344*x*y*y*y - 13440*x*x*x*y + 2016*x*z*z + 12096*x*x*z - 336*x*z*z*z - 10080*x*x*x*z - 8064*x*x + 13440*x*x*x - 6720*x*x*x*x - 2016*x*y*z*z - 3024*x*y*y*z - 12096*x*x*y*z + 6048*x*y*z;
   values[180] = 480*y - 2016*x*x*y*y - 4032*y*y*z*z - 1620*x*y - 2160*y*z + 4536*x*y*y + 1512*x*x*y - 3024*x*y*y*y - 336*x*x*x*y + 3024*y*z*z + 6048*y*y*z - 1344*y*z*z*z - 4032*y*y*y*z - 2160*y*y + 3024*y*y*y - 1344*y*y*y*y - 3024*x*y*z*z - 6048*x*y*y*z - 2016*x*x*y*z + 4536*x*y*z;
   values[181] = 3024*x*x*y*y - 120*x + 1008*x*x*z*z + 1080*x*y + 540*x*z - 2268*x*y*y - 3024*x*x*y + 1344*x*y*y*y + 2016*x*x*x*y - 756*x*z*z - 1512*x*x*z + 336*x*z*z*z + 1008*x*x*x*z + 540*x*x - 756*x*x*x + 336*x*x*x*x + 2016*x*y*z*z + 3024*x*y*y*z + 4032*x*x*y*z - 3024*x*y*z;
   values[182] = 2016*x*x*y*y + 540*x*y - 1512*x*y*y - 1512*x*x*y + 1008*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[183] = 6048*x*x*y*y - 1680*y + 12096*y*y*z*z + 4536*x*y + 6048*y*z - 15876*x*y*y - 3528*x*x*y + 12096*x*y*y*y + 672*x*x*x*y - 7056*y*z*z - 21168*y*y*z + 2688*y*z*z*z + 16128*y*y*y*z + 9072*y*y - 14112*y*y*y + 6720*y*y*y*y + 6048*x*y*z*z + 18144*x*y*y*z + 4032*x*x*y*z - 10584*x*y*z;
   values[184] = 420*x - 12096*x*x*y*y - 2016*x*x*z*z - 4536*x*y - 1512*x*z + 10584*x*y*y + 10584*x*x*y - 6720*x*y*y*y - 6048*x*x*x*y + 1764*x*z*z + 3528*x*x*z - 672*x*z*z*z - 2016*x*x*x*z - 1512*x*x + 1764*x*x*x - 672*x*x*x*x - 6048*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[185] = 5292*x*y*y - 1512*x*y - 6048*x*x*y*y + 3528*x*x*y - 4032*x*y*y*y - 2016*x*x*x*y - 2016*x*y*z*z - 6048*x*y*y*z - 4032*x*x*y*z + 3528*x*y*z;
   values[186] = 1344*y - 4032*x*x*y*y - 8064*y*y*z*z - 3024*x*y - 4032*y*z + 12096*x*y*y + 2016*x*x*y - 10080*x*y*y*y - 336*x*x*x*y + 4032*y*z*z + 16128*y*y*z - 1344*y*z*z*z - 13440*y*y*y*z - 8064*y*y + 13440*y*y*y - 6720*y*y*y*y - 3024*x*y*z*z - 12096*x*y*y*z - 2016*x*x*y*z + 6048*x*y*z;
   values[187] = 10080*x*x*y*y - 336*x + 1008*x*x*z*z + 4032*x*y + 1008*x*z - 10080*x*y*y - 8064*x*x*y + 6720*x*y*y*y + 4032*x*x*x*y - 1008*x*z*z - 2016*x*x*z + 336*x*z*z*z + 1008*x*x*x*z + 1008*x*x - 1008*x*x*x + 336*x*x*x*x + 4032*x*y*z*z + 10080*x*y*y*z + 8064*x*x*y*z - 8064*x*y*z;
   values[188] = 4032*x*x*y*y + 1008*x*y - 4032*x*y*y - 2016*x*x*y + 3360*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 4032*x*y*y*z + 2016*x*x*y*z - 2016*x*y*z;
   values[189] = 12096*x*x*y*y - 1500*y + 8064*y*y*z*z + 8208*x*y + 5616*y*z - 20160*x*y*y - 9828*x*x*y + 12096*x*y*y*y + 2688*x*x*x*y - 6804*y*z*z - 13608*y*y*z + 2688*y*z*z*z + 8064*y*y*y*z + 5616*y*y - 6804*y*y*y + 2688*y*y*y*y + 12096*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 20160*x*y*z;
   values[190] = 600*x - 12096*x*x*y*y - 4032*x*x*z*z - 3888*x*y - 1944*x*z + 6048*x*y*y + 15120*x*x*y - 2688*x*y*y*y - 12096*x*x*x*y + 2016*x*z*z + 7560*x*x*z - 672*x*z*z*z - 6048*x*x*x*z - 3456*x*x + 5544*x*x*x - 2688*x*x*x*x - 4032*x*y*z*z - 6048*x*y*y*z - 16128*x*x*y*z + 8064*x*y*z;
   values[191] = 4032*x*y*y - 1944*x*y - 8064*x*x*y*y + 7560*x*x*y - 2016*x*y*y*y - 6048*x*x*x*y - 2016*x*y*z*z - 4032*x*y*y*z - 8064*x*x*y*z + 4032*x*y*z;
   values[192] = 2436*y - 18144*x*x*y*y - 12096*y*y*z*z - 11088*x*y - 7560*y*z + 34776*x*y*y + 11340*x*x*y - 24192*x*y*y*y - 2688*x*x*x*y + 7812*y*z*z + 23436*y*y*z - 2688*y*z*z*z - 16128*y*y*y*z - 11340*y*y + 15624*y*y*y - 6720*y*y*y*y - 12096*x*y*z*z - 36288*x*y*y*z - 12096*x*x*y*z + 23184*x*y*z;
   values[193] = 24192*x*x*y*y - 924*x + 4032*x*x*z*z + 7560*x*y + 2520*x*z - 13608*x*y*y - 25704*x*x*y + 6720*x*y*y*y + 18144*x*x*x*y - 2268*x*z*z - 8568*x*x*z + 672*x*z*z*z + 6048*x*x*x*z + 4536*x*x - 6300*x*x*x + 2688*x*x*x*x + 6048*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 13608*x*y*z;
   values[194] = 12096*x*x*y*y + 2520*x*y - 6804*x*y*y - 8568*x*x*y + 4032*x*y*y*y + 6048*x*x*x*y + 2016*x*y*z*z + 6048*x*y*y*z + 8064*x*x*y*z - 4536*x*y*z;
   values[195] = 1092*y - 12096*x*x*y*y - 4032*y*y*z*z - 7560*x*y - 3528*y*z + 16632*x*y*y + 10584*x*x*y - 9072*x*y*y*y - 3360*x*x*x*y + 3780*y*z*z + 7560*y*y*z - 1344*y*z*z*z - 4032*y*y*y*z - 3528*y*y + 3780*y*y*y - 1344*y*y*y*y - 9072*x*y*z*z - 18144*x*y*y*z - 12096*x*x*y*z + 16632*x*y*z;
   values[196] = 9072*x*x*y*y - 588*x + 3024*x*x*z*z + 3024*x*y + 1512*x*z - 3780*x*y*y - 13608*x*x*y + 1344*x*y*y*y + 12096*x*x*x*y - 1260*x*z*z - 6804*x*x*z + 336*x*z*z*z + 6048*x*x*x*z + 3780*x*x - 6552*x*x*x + 3360*x*x*x*x + 2016*x*y*z*z + 3024*x*y*y*z + 12096*x*x*y*z - 5040*x*y*z;
   values[197] = 6048*x*x*y*y + 1512*x*y - 2520*x*y*y - 6804*x*x*y + 1008*x*y*y*y + 6048*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 6048*x*x*y*z - 2520*x*y*z;
   values[198] = 3024*x*x*y*y - 120*y + 1008*y*y*z*z + 1080*x*y + 540*y*z - 3024*x*y*y - 2268*x*x*y + 2016*x*y*y*y + 1344*x*x*x*y - 756*y*z*z - 1512*y*y*z + 336*y*z*z*z + 1008*y*y*y*z + 540*y*y - 756*y*y*y + 336*y*y*y*y + 2016*x*y*z*z + 4032*x*y*y*z + 3024*x*x*y*z - 3024*x*y*z;
   values[199] = 480*x - 2016*x*x*y*y - 4032*x*x*z*z - 1620*x*y - 2160*x*z + 1512*x*y*y + 4536*x*x*y - 336*x*y*y*y - 3024*x*x*x*y + 3024*x*z*z + 6048*x*x*z - 1344*x*z*z*z - 4032*x*x*x*z - 2160*x*x + 3024*x*x*x - 1344*x*x*x*x - 3024*x*y*z*z - 2016*x*y*y*z - 6048*x*x*y*z + 4536*x*y*z;
   values[200] = 2016*x*x*y*y + 540*x*y - 1512*x*y*y - 1512*x*x*y + 1008*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 2016*x*x*y*z - 1512*x*y*z;
   values[201] = 600*y - 12096*x*x*y*y - 4032*y*y*z*z - 3888*x*y - 1944*y*z + 15120*x*y*y + 6048*x*x*y - 12096*x*y*y*y - 2688*x*x*x*y + 2016*y*z*z + 7560*y*y*z - 672*y*z*z*z - 6048*y*y*y*z - 3456*y*y + 5544*y*y*y - 2688*y*y*y*y - 4032*x*y*z*z - 16128*x*y*y*z - 6048*x*x*y*z + 8064*x*y*z;
   values[202] = 12096*x*x*y*y - 1500*x + 8064*x*x*z*z + 8208*x*y + 5616*x*z - 9828*x*y*y - 20160*x*x*y + 2688*x*y*y*y + 12096*x*x*x*y - 6804*x*z*z - 13608*x*x*z + 2688*x*z*z*z + 8064*x*x*x*z + 5616*x*x - 6804*x*x*x + 2688*x*x*x*x + 12096*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 20160*x*y*z;
   values[203] = 7560*x*y*y - 1944*x*y - 8064*x*x*y*y + 4032*x*x*y - 6048*x*y*y*y - 2016*x*x*x*y - 2016*x*y*z*z - 8064*x*y*y*z - 4032*x*x*y*z + 4032*x*y*z;
   values[204] = 9072*x*x*y*y - 588*y + 3024*y*y*z*z + 3024*x*y + 1512*y*z - 13608*x*y*y - 3780*x*x*y + 12096*x*y*y*y + 1344*x*x*x*y - 1260*y*z*z - 6804*y*y*z + 336*y*z*z*z + 6048*y*y*y*z + 3780*y*y - 6552*y*y*y + 3360*y*y*y*y + 2016*x*y*z*z + 12096*x*y*y*z + 3024*x*x*y*z - 5040*x*y*z;
   values[205] = 1092*x - 12096*x*x*y*y - 4032*x*x*z*z - 7560*x*y - 3528*x*z + 10584*x*y*y + 16632*x*x*y - 3360*x*y*y*y - 9072*x*x*x*y + 3780*x*z*z + 7560*x*x*z - 1344*x*z*z*z - 4032*x*x*x*z - 3528*x*x + 3780*x*x*x - 1344*x*x*x*x - 9072*x*y*z*z - 12096*x*y*y*z - 18144*x*x*y*z + 16632*x*y*z;
   values[206] = 6048*x*x*y*y + 1512*x*y - 6804*x*y*y - 2520*x*x*y + 6048*x*y*y*y + 1008*x*x*x*y + 1008*x*y*z*z + 6048*x*y*y*z + 2016*x*x*y*z - 2520*x*y*z;
   values[207] = 420*y - 12096*x*x*y*y - 2016*y*y*z*z - 4536*x*y - 1512*y*z + 10584*x*y*y + 10584*x*x*y - 6048*x*y*y*y - 6720*x*x*x*y + 1764*y*z*z + 3528*y*y*z - 672*y*z*z*z - 2016*y*y*y*z - 1512*y*y + 1764*y*y*y - 672*y*y*y*y - 6048*x*y*z*z - 12096*x*y*y*z - 12096*x*x*y*z + 10584*x*y*z;
   values[208] = 6048*x*x*y*y - 1680*x + 12096*x*x*z*z + 4536*x*y + 6048*x*z - 3528*x*y*y - 15876*x*x*y + 672*x*y*y*y + 12096*x*x*x*y - 7056*x*z*z - 21168*x*x*z + 2688*x*z*z*z + 16128*x*x*x*z + 9072*x*x - 14112*x*x*x + 6720*x*x*x*x + 6048*x*y*z*z + 4032*x*y*y*z + 18144*x*x*y*z - 10584*x*y*z;
   values[209] = 3528*x*y*y - 1512*x*y - 6048*x*x*y*y + 5292*x*x*y - 2016*x*y*y*y - 4032*x*x*x*y - 2016*x*y*z*z - 4032*x*y*y*z - 6048*x*x*y*z + 3528*x*y*z;
   values[210] = 24192*x*x*y*y - 924*y + 4032*y*y*z*z + 7560*x*y + 2520*y*z - 25704*x*y*y - 13608*x*x*y + 18144*x*y*y*y + 6720*x*x*x*y - 2268*y*z*z - 8568*y*y*z + 672*y*z*z*z + 6048*y*y*y*z + 4536*y*y - 6300*y*y*y + 2688*y*y*y*y + 6048*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 13608*x*y*z;
   values[211] = 2436*x - 18144*x*x*y*y - 12096*x*x*z*z - 11088*x*y - 7560*x*z + 11340*x*y*y + 34776*x*x*y - 2688*x*y*y*y - 24192*x*x*x*y + 7812*x*z*z + 23436*x*x*z - 2688*x*z*z*z - 16128*x*x*x*z - 11340*x*x + 15624*x*x*x - 6720*x*x*x*x - 12096*x*y*z*z - 12096*x*y*y*z - 36288*x*x*y*z + 23184*x*y*z;
   values[212] = 12096*x*x*y*y + 2520*x*y - 8568*x*y*y - 6804*x*x*y + 6048*x*y*y*y + 4032*x*x*x*y + 2016*x*y*z*z + 8064*x*y*y*z + 6048*x*x*y*z - 4536*x*y*z;
   values[213] = 10080*x*x*y*y - 336*y + 1008*y*y*z*z + 4032*x*y + 1008*y*z - 8064*x*y*y - 10080*x*x*y + 4032*x*y*y*y + 6720*x*x*x*y - 1008*y*z*z - 2016*y*y*z + 336*y*z*z*z + 1008*y*y*y*z + 1008*y*y - 1008*y*y*y + 336*y*y*y*y + 4032*x*y*z*z + 8064*x*y*y*z + 10080*x*x*y*z - 8064*x*y*z;
   values[214] = 1344*x - 4032*x*x*y*y - 8064*x*x*z*z - 3024*x*y - 4032*x*z + 2016*x*y*y + 12096*x*x*y - 336*x*y*y*y - 10080*x*x*x*y + 4032*x*z*z + 16128*x*x*z - 1344*x*z*z*z - 13440*x*x*x*z - 8064*x*x + 13440*x*x*x - 6720*x*x*x*x - 3024*x*y*z*z - 2016*x*y*y*z - 12096*x*x*y*z + 6048*x*y*z;
   values[215] = 4032*x*x*y*y + 1008*x*y - 2016*x*y*y - 4032*x*x*y + 1008*x*y*y*y + 3360*x*x*x*y + 1008*x*y*z*z + 2016*x*y*y*z + 4032*x*x*y*z - 2016*x*y*z;
   values[216] = 32256*y*y*z*z + 12096*y*z - 28224*y*z*z - 28224*y*y*z + 16128*y*z*z*z + 16128*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 8064*x*x*y*z - 21168*x*y*z;
   values[217] = 7056*x*z*z - 3024*x*z - 8064*x*x*z*z + 7056*x*x*z - 4032*x*z*z*z - 4032*x*x*x*z - 16128*x*y*z*z - 12096*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[218] = 7056*x*y*y - 3024*x*y - 8064*x*x*y*y + 7056*x*x*y - 4032*x*y*y*y - 4032*x*x*x*y - 12096*x*y*z*z - 16128*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[219] = 7056*y*z*z - 3024*y*z - 8064*y*y*z*z + 7056*y*y*z - 4032*y*z*z*z - 4032*y*y*y*z - 16128*x*y*z*z - 16128*x*y*y*z - 12096*x*x*y*z + 14112*x*y*z;
   values[220] = 32256*x*x*z*z + 12096*x*z - 28224*x*z*z - 28224*x*x*z + 16128*x*z*z*z + 16128*x*x*x*z + 24192*x*y*z*z + 8064*x*y*y*z + 24192*x*x*y*z - 21168*x*y*z;
   values[221] = 7056*x*y*y - 3024*x*y - 8064*x*x*y*y + 7056*x*x*y - 4032*x*y*y*y - 4032*x*x*x*y - 12096*x*y*z*z - 16128*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[222] = 7056*y*z*z - 3024*y*z - 8064*y*y*z*z + 7056*y*y*z - 4032*y*z*z*z - 4032*y*y*y*z - 16128*x*y*z*z - 16128*x*y*y*z - 12096*x*x*y*z + 14112*x*y*z;
   values[223] = 7056*x*z*z - 3024*x*z - 8064*x*x*z*z + 7056*x*x*z - 4032*x*z*z*z - 4032*x*x*x*z - 16128*x*y*z*z - 12096*x*y*y*z - 16128*x*x*y*z + 14112*x*y*z;
   values[224] = 32256*x*x*y*y + 12096*x*y - 28224*x*y*y - 28224*x*x*y + 16128*x*y*y*y + 16128*x*x*x*y + 8064*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 21168*x*y*z;
   values[225] = 48384*y*z*z - 16128*y*z - 48384*y*y*z*z + 32256*y*y*z - 32256*y*z*z*z - 16128*y*y*y*z - 36288*x*y*z*z - 24192*x*y*y*z - 8064*x*x*y*z + 24192*x*y*z;
   values[226] = 12096*x*x*z*z + 4032*x*z - 12096*x*z*z - 8064*x*x*z + 8064*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 12096*x*y*y*z + 16128*x*x*y*z - 16128*x*y*z;
   values[227] = 8064*x*x*y*y + 4032*x*y - 8064*x*y*y - 8064*x*x*y + 4032*x*y*y*y + 4032*x*x*x*y + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[228] = 12096*y*y*z*z + 4032*y*z - 12096*y*z*z - 8064*y*y*z + 8064*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 16128*x*y*y*z + 12096*x*x*y*z - 16128*x*y*z;
   values[229] = 48384*x*z*z - 16128*x*z - 48384*x*x*z*z + 32256*x*x*z - 32256*x*z*z*z - 16128*x*x*x*z - 36288*x*y*z*z - 8064*x*y*y*z - 24192*x*x*y*z + 24192*x*y*z;
   values[230] = 8064*x*x*y*y + 4032*x*y - 8064*x*y*y - 8064*x*x*y + 4032*x*y*y*y + 4032*x*x*x*y + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[231] = 16128*y*y*z*z + 5040*y*z - 17136*y*z*z - 9072*y*y*z + 12096*y*z*z*z + 4032*y*y*y*z + 32256*x*y*z*z + 16128*x*y*y*z + 12096*x*x*y*z - 18144*x*y*z;
   values[232] = 16128*x*x*z*z + 5040*x*z - 17136*x*z*z - 9072*x*x*z + 12096*x*z*z*z + 4032*x*x*x*z + 32256*x*y*z*z + 12096*x*y*y*z + 16128*x*x*y*z - 18144*x*y*z;
   values[233] = 31248*x*y*y - 15120*x*y - 32256*x*x*y*y + 31248*x*x*y - 16128*x*y*y*y - 16128*x*x*x*y - 24192*x*y*z*z - 48384*x*y*y*z - 48384*x*x*y*z + 46368*x*y*z;
   values[234] = 32256*y*z*z - 16128*y*z - 48384*y*y*z*z + 48384*y*y*z - 16128*y*z*z*z - 32256*y*y*y*z - 24192*x*y*z*z - 36288*x*y*y*z - 8064*x*x*y*z + 24192*x*y*z;
   values[235] = 8064*x*x*z*z + 4032*x*z - 8064*x*z*z - 8064*x*x*z + 4032*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[236] = 12096*x*x*y*y + 4032*x*y - 12096*x*y*y - 8064*x*x*y + 8064*x*y*y*y + 4032*x*x*x*y + 12096*x*y*z*z + 24192*x*y*y*z + 16128*x*x*y*z - 16128*x*y*z;
   values[237] = 16128*y*y*z*z + 5040*y*z - 9072*y*z*z - 17136*y*y*z + 4032*y*z*z*z + 12096*y*y*y*z + 16128*x*y*z*z + 32256*x*y*y*z + 12096*x*x*y*z - 18144*x*y*z;
   values[238] = 31248*x*z*z - 15120*x*z - 32256*x*x*z*z + 31248*x*x*z - 16128*x*z*z*z - 16128*x*x*x*z - 48384*x*y*z*z - 24192*x*y*y*z - 48384*x*x*y*z + 46368*x*y*z;
   values[239] = 16128*x*x*y*y + 5040*x*y - 17136*x*y*y - 9072*x*x*y + 12096*x*y*y*y + 4032*x*x*x*y + 12096*x*y*z*z + 32256*x*y*y*z + 16128*x*x*y*z - 18144*x*y*z;
   values[240] = 12096*y*y*z*z + 4032*y*z - 8064*y*z*z - 12096*y*y*z + 4032*y*z*z*z + 8064*y*y*y*z + 16128*x*y*z*z + 24192*x*y*y*z + 12096*x*x*y*z - 16128*x*y*z;
   values[241] = 8064*x*x*z*z + 4032*x*z - 8064*x*z*z - 8064*x*x*z + 4032*x*z*z*z + 4032*x*x*x*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[242] = 48384*x*y*y - 16128*x*y - 48384*x*x*y*y + 32256*x*x*y - 32256*x*y*y*y - 16128*x*x*x*y - 8064*x*y*z*z - 36288*x*y*y*z - 24192*x*x*y*z + 24192*x*y*z;
   values[243] = 31248*y*z*z - 15120*y*z - 32256*y*y*z*z + 31248*y*y*z - 16128*y*z*z*z - 16128*y*y*y*z - 48384*x*y*z*z - 48384*x*y*y*z - 24192*x*x*y*z + 46368*x*y*z;
   values[244] = 16128*x*x*z*z + 5040*x*z - 9072*x*z*z - 17136*x*x*z + 4032*x*z*z*z + 12096*x*x*x*z + 16128*x*y*z*z + 12096*x*y*y*z + 32256*x*x*y*z - 18144*x*y*z;
   values[245] = 16128*x*x*y*y + 5040*x*y - 9072*x*y*y - 17136*x*x*y + 4032*x*y*y*y + 12096*x*x*x*y + 12096*x*y*z*z + 16128*x*y*y*z + 32256*x*x*y*z - 18144*x*y*z;
   values[246] = 8064*y*y*z*z + 4032*y*z - 8064*y*z*z - 8064*y*y*z + 4032*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[247] = 32256*x*z*z - 16128*x*z - 48384*x*x*z*z + 48384*x*x*z - 16128*x*z*z*z - 32256*x*x*x*z - 24192*x*y*z*z - 8064*x*y*y*z - 36288*x*x*y*z + 24192*x*y*z;
   values[248] = 12096*x*x*y*y + 4032*x*y - 8064*x*y*y - 12096*x*x*y + 4032*x*y*y*y + 8064*x*x*x*y + 12096*x*y*z*z + 16128*x*y*y*z + 24192*x*x*y*z - 16128*x*y*z;
   values[249] = 8064*y*y*z*z + 4032*y*z - 8064*y*z*z - 8064*y*y*z + 4032*y*z*z*z + 4032*y*y*y*z + 24192*x*y*z*z + 24192*x*y*y*z + 24192*x*x*y*z - 24192*x*y*z;
   values[250] = 12096*x*x*z*z + 4032*x*z - 8064*x*z*z - 12096*x*x*z + 4032*x*z*z*z + 8064*x*x*x*z + 16128*x*y*z*z + 12096*x*y*y*z + 24192*x*x*y*z - 16128*x*y*z;
   values[251] = 32256*x*y*y - 16128*x*y - 48384*x*x*y*y + 48384*x*x*y - 16128*x*y*y*y - 32256*x*x*x*y - 8064*x*y*z*z - 24192*x*y*y*z - 36288*x*x*y*z + 24192*x*y*z;
}
static void Curl_T_P4_3D_3D_D100(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 480*x + 720*y + 720*z - 1680*x*y - 1680*x*z - 2520*y*z + 1344*x*y*y + 672*x*x*y + 1344*x*z*z + 672*x*x*z + 2016*y*z*z + 2016*y*y*z - 420*x*x - 1260*y*y + 672*y*y*y - 1260*z*z + 672*z*z*z + 2688*x*y*z - 120;
   values[1] = 1680*x*y - 240*y - 240*z - 480*x + 1680*x*z + 840*y*z - 1344*x*y*y - 2016*x*x*y - 1344*x*z*z - 2016*x*x*z - 672*y*z*z - 672*y*y*z + 1260*x*x - 896*x*x*x + 420*y*y - 224*y*y*y + 420*z*z - 224*z*z*z - 2688*x*y*z + 40;
   values[2] = 1680*x*y - 240*y - 240*z - 480*x + 1680*x*z + 840*y*z - 1344*x*y*y - 2016*x*x*y - 1344*x*z*z - 2016*x*x*z - 672*y*z*z - 672*y*y*z + 1260*x*x - 896*x*x*x + 420*y*y - 224*y*y*y + 420*z*z - 224*z*z*z - 2688*x*y*z + 40;
   values[3] = 16632*x*y - 5760*y - 5760*z - 5400*x + 16632*x*z + 17136*y*z - 12096*x*y*y - 8064*x*x*y - 12096*x*z*z - 8064*x*x*z - 12096*y*z*z - 12096*y*y*z + 5040*x*x + 8568*y*y - 4032*y*y*y + 8568*z*z - 4032*z*z*z - 24192*x*y*z + 1200;
   values[4] = 5040*x + 1440*y + 1440*z - 13104*x*y - 13104*x*z - 3528*y*z + 8064*x*y*y + 18144*x*x*y + 8064*x*z*z + 18144*x*x*z + 2016*y*z*z + 2016*y*y*z - 14364*x*x + 10752*x*x*x - 1764*y*y + 672*y*y*y - 1764*z*z + 672*z*z*z + 16128*x*y*z - 360;
   values[5] = 5040*x + 1440*y + 1440*z - 13104*x*y - 13104*x*z - 3528*y*z + 8064*x*y*y + 18144*x*x*y + 8064*x*z*z + 18144*x*x*z + 2016*y*z*z + 2016*y*y*z - 14364*x*x + 10752*x*x*x - 1764*y*y + 672*y*y*y - 1764*z*z + 672*z*z*z + 16128*x*y*z - 360;
   values[6] = 12960*x + 11016*y + 11016*z - 36288*x*y - 36288*x*z - 28728*y*z + 24192*x*y*y + 20160*x*x*y + 24192*x*z*z + 20160*x*x*z + 18144*y*z*z + 18144*y*y*z - 12600*x*x - 14364*y*y + 6048*y*y*y - 14364*z*z + 6048*z*z*z + 48384*x*y*z - 2700;
   values[7] = 24192*x*y - 2376*y - 2376*z - 11664*x + 24192*x*z + 4536*y*z - 12096*x*y*y - 36288*x*x*y - 12096*x*z*z - 36288*x*x*z - 2016*y*z*z - 2016*y*y*z + 34776*x*x - 26880*x*x*x + 2268*y*y - 672*y*y*y + 2268*z*z - 672*z*z*z - 24192*x*y*z + 780;
   values[8] = 24192*x*y - 2376*y - 2376*z - 11664*x + 24192*x*z + 4536*y*z - 12096*x*y*y - 36288*x*x*y - 12096*x*z*z - 36288*x*x*z - 2016*y*z*z - 2016*y*y*z + 34776*x*x - 26880*x*x*x + 2268*y*y - 672*y*y*y + 2268*z*z - 672*z*z*z - 24192*x*y*z + 780;
   values[9] = 21840*x*y - 6048*y - 6048*z - 8400*x + 21840*x*z + 14112*y*z - 13440*x*y*y - 13440*x*x*y - 13440*x*z*z - 13440*x*x*z - 8064*y*z*z - 8064*y*y*z + 8400*x*x + 7056*y*y - 2688*y*y*y + 7056*z*z - 2688*z*z*z - 26880*x*y*z + 1680;
   values[10] = 7392*x + 1176*y + 1176*z - 12768*x*y - 12768*x*z - 1848*y*z + 5376*x*y*y + 20160*x*x*y + 5376*x*z*z + 20160*x*x*z + 672*y*z*z + 672*y*y*z - 22680*x*x + 17920*x*x*x - 924*y*y + 224*y*y*y - 924*z*z + 224*z*z*z + 10752*x*y*z - 476;
   values[11] = 7392*x + 1176*y + 1176*z - 12768*x*y - 12768*x*z - 1848*y*z + 5376*x*y*y + 20160*x*x*y + 5376*x*z*z + 20160*x*x*z + 672*y*z*z + 672*y*y*z - 22680*x*x + 17920*x*x*x - 924*y*y + 224*y*y*y - 924*z*z + 224*z*z*z + 10752*x*y*z - 476;
   values[12] = 840*x*y - 240*y + 840*y*z - 1344*x*y*y - 672*x*x*y - 672*y*z*z - 1344*y*y*z + 840*y*y - 672*y*y*y - 1344*x*y*z;
   values[13] = 960*x + 720*y + 960*z - 2520*x*y - 3360*x*z - 2520*y*z + 1344*x*y*y + 2016*x*x*y + 2688*x*z*z + 2688*x*x*z + 2016*y*z*z + 1344*y*y*z - 1680*x*x + 896*x*x*x - 840*y*y + 224*y*y*y - 1680*z*z + 896*z*z*z + 4032*x*y*z - 160;
   values[14] = 840*x*y - 240*y + 840*y*z - 1344*x*y*y - 672*x*x*y - 672*y*z*z - 1344*y*y*z + 840*y*y - 672*y*y*y - 1344*x*y*z;
   values[15] = 1440*y - 3528*x*y - 3528*y*z + 8064*x*y*y + 2016*x*x*y + 2016*y*z*z + 8064*y*y*z - 6552*y*y + 6048*y*y*y + 4032*x*y*z;
   values[16] = 17136*x*y - 5760*y - 3960*z - 3960*x + 11592*x*z + 17136*y*z - 12096*x*y*y - 12096*x*x*y - 8064*x*z*z - 8064*x*x*z - 12096*y*z*z - 12096*y*y*z + 5796*x*x - 2688*x*x*x + 8316*y*y - 2688*y*y*y + 5796*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[17] = 1440*y - 3528*x*y - 3528*y*z + 8064*x*y*y + 2016*x*x*y + 2016*y*z*z + 8064*y*y*z - 6552*y*y + 6048*y*y*y + 4032*x*y*z;
   values[18] = 4536*x*y - 2376*y + 4536*y*z - 12096*x*y*y - 2016*x*x*y - 2016*y*z*z - 12096*y*y*z + 12096*y*y - 12096*y*y*y - 4032*x*y*z;
   values[19] = 5184*x + 11016*y + 5184*z - 28728*x*y - 13104*x*z - 28728*y*z + 24192*x*y*y + 18144*x*x*y + 8064*x*z*z + 8064*x*x*z + 18144*y*z*z + 24192*y*y*z - 6552*x*x + 2688*x*x*x - 18144*y*y + 6720*y*y*y - 6552*z*z + 2688*z*z*z + 36288*x*y*z - 1320;
   values[20] = 4536*x*y - 2376*y + 4536*y*z - 12096*x*y*y - 2016*x*x*y - 2016*y*z*z - 12096*y*y*z + 12096*y*y - 12096*y*y*y - 4032*x*y*z;
   values[21] = 1176*y - 1848*x*y - 1848*y*z + 5376*x*y*y + 672*x*x*y + 672*y*z*z + 5376*y*y*z - 6384*y*y + 6720*y*y*y + 1344*x*y*z;
   values[22] = 14112*x*y - 6048*y - 2184*z - 2184*x + 4872*x*z + 14112*y*z - 13440*x*y*y - 8064*x*x*y - 2688*x*z*z - 2688*x*x*z - 8064*y*z*z - 13440*y*y*z + 2436*x*x - 896*x*x*x + 10920*y*y - 4480*y*y*y + 2436*z*z - 896*z*z*z - 16128*x*y*z + 644;
   values[23] = 1176*y - 1848*x*y - 1848*y*z + 5376*x*y*y + 672*x*x*y + 672*y*z*z + 5376*y*y*z - 6384*y*y + 6720*y*y*y + 1344*x*y*z;
   values[24] = 840*x*z - 240*z + 840*y*z - 1344*x*z*z - 672*x*x*z - 1344*y*z*z - 672*y*y*z + 840*z*z - 672*z*z*z - 1344*x*y*z;
   values[25] = 840*x*z - 240*z + 840*y*z - 1344*x*z*z - 672*x*x*z - 1344*y*z*z - 672*y*y*z + 840*z*z - 672*z*z*z - 1344*x*y*z;
   values[26] = 960*x + 960*y + 720*z - 3360*x*y - 2520*x*z - 2520*y*z + 2688*x*y*y + 2688*x*x*y + 1344*x*z*z + 2016*x*x*z + 1344*y*z*z + 2016*y*y*z - 1680*x*x + 896*x*x*x - 1680*y*y + 896*y*y*y - 840*z*z + 224*z*z*z + 4032*x*y*z - 160;
   values[27] = 1440*z - 3528*x*z - 3528*y*z + 8064*x*z*z + 2016*x*x*z + 8064*y*z*z + 2016*y*y*z - 6552*z*z + 6048*z*z*z + 4032*x*y*z;
   values[28] = 1440*z - 3528*x*z - 3528*y*z + 8064*x*z*z + 2016*x*x*z + 8064*y*z*z + 2016*y*y*z - 6552*z*z + 6048*z*z*z + 4032*x*y*z;
   values[29] = 11592*x*y - 3960*y - 5760*z - 3960*x + 17136*x*z + 17136*y*z - 8064*x*y*y - 8064*x*x*y - 12096*x*z*z - 12096*x*x*z - 12096*y*z*z - 12096*y*y*z + 5796*x*x - 2688*x*x*x + 5796*y*y - 2688*y*y*y + 8316*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[30] = 4536*x*z - 2376*z + 4536*y*z - 12096*x*z*z - 2016*x*x*z - 12096*y*z*z - 2016*y*y*z + 12096*z*z - 12096*z*z*z - 4032*x*y*z;
   values[31] = 4536*x*z - 2376*z + 4536*y*z - 12096*x*z*z - 2016*x*x*z - 12096*y*z*z - 2016*y*y*z + 12096*z*z - 12096*z*z*z - 4032*x*y*z;
   values[32] = 5184*x + 5184*y + 11016*z - 13104*x*y - 28728*x*z - 28728*y*z + 8064*x*y*y + 8064*x*x*y + 24192*x*z*z + 18144*x*x*z + 24192*y*z*z + 18144*y*y*z - 6552*x*x + 2688*x*x*x - 6552*y*y + 2688*y*y*y - 18144*z*z + 6720*z*z*z + 36288*x*y*z - 1320;
   values[33] = 1176*z - 1848*x*z - 1848*y*z + 5376*x*z*z + 672*x*x*z + 5376*y*z*z + 672*y*y*z - 6384*z*z + 6720*z*z*z + 1344*x*y*z;
   values[34] = 1176*z - 1848*x*z - 1848*y*z + 5376*x*z*z + 672*x*x*z + 5376*y*z*z + 672*y*y*z - 6384*z*z + 6720*z*z*z + 1344*x*y*z;
   values[35] = 4872*x*y - 2184*y - 6048*z - 2184*x + 14112*x*z + 14112*y*z - 2688*x*y*y - 2688*x*x*y - 13440*x*z*z - 8064*x*x*z - 13440*y*z*z - 8064*y*y*z + 2436*x*x - 896*x*x*x + 2436*y*y - 896*y*y*y + 10920*z*z - 4480*z*z*z - 16128*x*y*z + 644;
   values[36] = 504*x*y - 72*y - 672*x*x*y;
   values[37] = 288*x - 1008*x*x + 896*x*x*x - 16;
   values[38] = 0;
   values[39] = 2016*x*x*y - 504*x*y - 4032*x*y*y - 72*y + 1008*y*y;
   values[40] = 216*y - 432*x - 3024*x*y + 6048*x*x*y + 2268*x*x - 2688*x*x*x + 12;
   values[41] = 0;
   values[42] = 144*y - 504*x*y + 8064*x*y*y - 2016*x*x*y - 504*y*y - 2016*y*y*y;
   values[43] = 144*x - 144*y + 4032*x*y + 4032*x*y*y - 12096*x*x*y - 1512*x*x + 2688*x*x*x - 504*y*y;
   values[44] = 0;
   values[45] = 504*x*y - 4032*x*y*y + 672*x*x*y - 504*y*y + 2016*y*y*y;
   values[46] = 6048*x*x*y - 4032*x*y*y - 1008*x*y + 252*x*x - 896*x*x*x + 252*y*y + 224*y*y*y;
   values[47] = 0;
   values[48] = 504*x*z - 72*z - 672*x*x*z;
   values[49] = 0;
   values[50] = 288*x - 1008*x*x + 896*x*x*x - 16;
   values[51] = 2016*x*x*z - 504*x*z - 4032*x*z*z - 72*z + 1008*z*z;
   values[52] = 0;
   values[53] = 216*z - 432*x - 3024*x*z + 6048*x*x*z + 2268*x*x - 2688*x*x*x + 12;
   values[54] = 144*z - 504*x*z + 8064*x*z*z - 2016*x*x*z - 504*z*z - 2016*z*z*z;
   values[55] = 0;
   values[56] = 144*x - 144*z + 4032*x*z + 4032*x*z*z - 12096*x*x*z - 1512*x*x + 2688*x*x*x - 504*z*z;
   values[57] = 504*x*z - 4032*x*z*z + 672*x*x*z - 504*z*z + 2016*z*z*z;
   values[58] = 0;
   values[59] = 6048*x*x*z - 4032*x*z*z - 1008*x*z + 252*x*x - 896*x*x*x + 252*z*z + 224*z*z*z;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = 0;
   values[67] = 0;
   values[68] = 0;
   values[69] = 0;
   values[70] = 0;
   values[71] = 0;
   values[72] = 504*y*z - 2016*x*y*z;
   values[73] = 144*z - 2016*x*z + 4032*x*x*z;
   values[74] = 504*x*y - 36*y - 1008*x*x*y;
   values[75] = 4032*x*y*z - 2016*y*z*z - 504*y*z;
   values[76] = 2016*x*z + 8064*x*z*z - 8064*x*x*z - 1008*z*z;
   values[77] = 504*y*z - 504*x*y + 2016*x*x*y - 4032*x*y*z;
   values[78] = 2016*y*z*z - 2016*x*y*z;
   values[79] = 4032*x*x*z - 8064*x*z*z + 1344*z*z*z;
   values[80] = 4032*x*y*z - 1008*y*z*z - 1008*x*x*y;
   values[81] = 4032*x*y*z - 4032*y*y*z;
   values[82] = 2520*x*z - 72*z - 1512*y*z - 8064*x*x*z + 12096*x*y*z;
   values[83] = 2016*x*x*y - 4032*x*y*y - 72*y + 504*y*y;
   values[84] = 2016*y*z*z - 504*y*z + 4032*y*y*z - 4032*x*y*z;
   values[85] = 8064*x*x*z - 8064*x*z*z - 504*x*z + 6048*y*z*z + 252*z*z - 12096*x*y*z;
   values[86] = 504*y*z - 504*x*y + 4032*x*y*y - 2016*x*x*y - 4032*y*y*z + 4032*x*y*z;
   values[87] = 4032*y*y*z - 504*y*z - 2016*x*y*z;
   values[88] = 504*y*z - 504*x*z + 4032*x*x*z + 2016*y*y*z - 12096*x*y*z;
   values[89] = 4032*x*y*y - 504*x*y - 1008*x*x*y + 252*y*y - 1008*y*y*y;
   values[90] = 504*y*z - 2016*x*y*z;
   values[91] = 504*x*z - 36*z - 1008*x*x*z;
   values[92] = 144*y - 2016*x*y + 4032*x*x*y;
   values[93] = 4032*x*y*z - 4032*y*z*z;
   values[94] = 2016*x*x*z - 4032*x*z*z - 72*z + 504*z*z;
   values[95] = 2520*x*y - 72*y - 1512*y*z - 8064*x*x*y + 12096*x*y*z;
   values[96] = 4032*y*z*z - 504*y*z - 2016*x*y*z;
   values[97] = 4032*x*z*z - 504*x*z - 1008*x*x*z + 252*z*z - 1008*z*z*z;
   values[98] = 504*y*z - 504*x*y + 4032*x*x*y + 2016*y*z*z - 12096*x*y*z;
   values[99] = 4032*x*y*z - 2016*y*y*z - 504*y*z;
   values[100] = 504*y*z - 504*x*z + 2016*x*x*z - 4032*x*y*z;
   values[101] = 2016*x*y + 8064*x*y*y - 8064*x*x*y - 1008*y*y;
   values[102] = 4032*y*z*z - 504*y*z + 2016*y*y*z - 4032*x*y*z;
   values[103] = 504*y*z - 504*x*z + 4032*x*z*z - 2016*x*x*z - 4032*y*z*z + 4032*x*y*z;
   values[104] = 8064*x*x*y - 8064*x*y*y - 504*x*y + 6048*y*y*z + 252*y*y - 12096*x*y*z;
   values[105] = 2016*y*y*z - 2016*x*y*z;
   values[106] = 4032*x*y*z - 1008*y*y*z - 1008*x*x*z;
   values[107] = 4032*x*x*y - 8064*x*y*y + 1344*y*y*y;
   values[108] = 2016*y*z*z - 1512*y*z + 2016*y*y*z + 2016*x*y*z;
   values[109] = 6048*x*z - 2160*z + 4536*y*z - 8064*x*z*z - 4032*x*x*z - 6048*y*z*z - 2016*y*y*z + 6048*z*z - 4032*z*z*z - 6048*x*y*z;
   values[110] = 540*y - 1512*x*y - 3024*y*z + 2016*x*y*y + 1008*x*x*y + 3024*y*z*z + 4032*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[111] = 3528*y*z - 6048*y*z*z - 4032*y*y*z - 4032*x*y*z;
   values[112] = 6048*z - 14112*x*z - 10584*y*z + 24192*x*z*z + 8064*x*x*z + 18144*y*z*z + 4032*y*y*z - 21168*z*z + 16128*z*z*z + 12096*x*y*z;
   values[113] = 3528*x*y - 1512*y + 10584*y*z - 4032*x*y*y - 2016*x*x*y - 12096*y*z*z - 12096*y*y*z + 3528*y*y - 2016*y*y*y - 12096*x*y*z;
   values[114] = 4032*y*z*z - 2016*y*z + 2016*y*y*z + 2016*x*y*z;
   values[115] = 8064*x*z - 4032*z + 6048*y*z - 16128*x*z*z - 4032*x*x*z - 12096*y*z*z - 2016*y*y*z + 16128*z*z - 13440*z*z*z - 6048*x*y*z;
   values[116] = 1008*y - 2016*x*y - 8064*y*z + 2016*x*y*y + 1008*x*x*y + 10080*y*z*z + 8064*y*y*z - 2016*y*y + 1008*y*y*y + 8064*x*y*z;
   values[117] = 4032*y*z - 4032*y*z*z - 8064*y*y*z - 4032*x*y*z;
   values[118] = 5616*z - 13608*x*z - 20160*y*z + 16128*x*z*z + 8064*x*x*z + 24192*y*z*z + 12096*y*y*z - 13608*z*z + 8064*z*z*z + 24192*x*y*z;
   values[119] = 4032*x*y - 1944*y + 8064*y*z - 8064*x*y*y - 2016*x*x*y - 6048*y*z*z - 16128*y*y*z + 7560*y*y - 6048*y*y*y - 8064*x*y*z;
   values[120] = 6048*y*z*z - 4536*y*z + 8064*y*y*z + 4032*x*y*z;
   values[121] = 15624*x*z - 7560*z + 23184*y*z - 24192*x*z*z - 8064*x*x*z - 36288*y*z*z - 12096*y*y*z + 23436*z*z - 16128*z*z*z - 24192*x*y*z;
   values[122] = 2520*y - 4536*x*y - 13608*y*z + 8064*x*y*y + 2016*x*x*y + 12096*y*z*z + 24192*y*y*z - 8568*y*y + 6048*y*y*y + 12096*x*y*z;
   values[123] = 2016*y*z*z - 2520*y*z + 6048*y*y*z + 2016*x*y*z;
   values[124] = 7560*x*z - 3528*z + 16632*y*z - 8064*x*z*z - 4032*x*x*z - 18144*y*z*z - 12096*y*y*z + 7560*z*z - 4032*z*z*z - 18144*x*y*z;
   values[125] = 1512*y - 2520*x*y - 5040*y*z + 6048*x*y*y + 1008*x*x*y + 3024*y*z*z + 12096*y*y*z - 6804*y*y + 6048*y*y*y + 4032*x*y*z;
   values[126] = 2016*y*z*z - 1512*y*z + 2016*y*y*z + 2016*x*y*z;
   values[127] = 540*z - 1512*x*z - 3024*y*z + 2016*x*z*z + 1008*x*x*z + 4032*y*z*z + 3024*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[128] = 6048*x*y - 2160*y + 4536*y*z - 8064*x*y*y - 4032*x*x*y - 2016*y*z*z - 6048*y*y*z + 6048*y*y - 4032*y*y*y - 6048*x*y*z;
   values[129] = 4032*y*z - 8064*y*z*z - 4032*y*y*z - 4032*x*y*z;
   values[130] = 4032*x*z - 1944*z + 8064*y*z - 8064*x*z*z - 2016*x*x*z - 16128*y*z*z - 6048*y*y*z + 7560*z*z - 6048*z*z*z - 8064*x*y*z;
   values[131] = 5616*y - 13608*x*y - 20160*y*z + 16128*x*y*y + 8064*x*x*y + 12096*y*z*z + 24192*y*y*z - 13608*y*y + 8064*y*y*y + 24192*x*y*z;
   values[132] = 6048*y*z*z - 2520*y*z + 2016*y*y*z + 2016*x*y*z;
   values[133] = 1512*z - 2520*x*z - 5040*y*z + 6048*x*z*z + 1008*x*x*z + 12096*y*z*z + 3024*y*y*z - 6804*z*z + 6048*z*z*z + 4032*x*y*z;
   values[134] = 7560*x*y - 3528*y + 16632*y*z - 8064*x*y*y - 4032*x*x*y - 12096*y*z*z - 18144*y*y*z + 7560*y*y - 4032*y*y*y - 18144*x*y*z;
   values[135] = 3528*y*z - 4032*y*z*z - 6048*y*y*z - 4032*x*y*z;
   values[136] = 3528*x*z - 1512*z + 10584*y*z - 4032*x*z*z - 2016*x*x*z - 12096*y*z*z - 12096*y*y*z + 3528*z*z - 2016*z*z*z - 12096*x*y*z;
   values[137] = 6048*y - 14112*x*y - 10584*y*z + 24192*x*y*y + 8064*x*x*y + 4032*y*z*z + 18144*y*y*z - 21168*y*y + 16128*y*y*y + 12096*x*y*z;
   values[138] = 8064*y*z*z - 4536*y*z + 6048*y*y*z + 4032*x*y*z;
   values[139] = 2520*z - 4536*x*z - 13608*y*z + 8064*x*z*z + 2016*x*x*z + 24192*y*z*z + 12096*y*y*z - 8568*z*z + 6048*z*z*z + 12096*x*y*z;
   values[140] = 15624*x*y - 7560*y + 23184*y*z - 24192*x*y*y - 8064*x*x*y - 12096*y*z*z - 36288*y*y*z + 23436*y*y - 16128*y*y*y - 24192*x*y*z;
   values[141] = 2016*y*z*z - 2016*y*z + 4032*y*y*z + 2016*x*y*z;
   values[142] = 1008*z - 2016*x*z - 8064*y*z + 2016*x*z*z + 1008*x*x*z + 8064*y*z*z + 10080*y*y*z - 2016*z*z + 1008*z*z*z + 8064*x*y*z;
   values[143] = 8064*x*y - 4032*y + 6048*y*z - 16128*x*y*y - 4032*x*x*y - 2016*y*z*z - 12096*y*y*z + 16128*y*y - 13440*y*y*y - 6048*x*y*z;
   values[144] = 3024*x*z - 1620*z + 4536*y*z - 4032*x*z*z - 1008*x*x*z - 6048*y*z*z - 3024*y*y*z + 4536*z*z - 3024*z*z*z - 4032*x*y*z;
   values[145] = 540*z - 3024*x*z - 1512*y*z + 4032*x*z*z + 3024*x*x*z + 2016*y*z*z + 1008*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[146] = 1080*x + 540*y + 1080*z - 3024*x*y - 6048*x*z - 3024*y*z + 2016*x*y*y + 3024*x*x*y + 6048*x*z*z + 6048*x*x*z + 3024*y*z*z + 2016*y*y*z - 2268*x*x + 1344*x*x*x - 756*y*y + 336*y*y*y - 2268*z*z + 1344*z*z*z + 8064*x*y*z - 120;
   values[147] = 4536*z - 7056*x*z - 10584*y*z + 12096*x*z*z + 2016*x*x*z + 18144*y*z*z + 6048*y*y*z - 15876*z*z + 12096*z*z*z + 8064*x*y*z;
   values[148] = 7056*x*z - 1512*z + 3528*y*z - 12096*x*z*z - 6048*x*x*z - 6048*y*z*z - 2016*y*y*z + 5292*z*z - 4032*z*z*z - 8064*x*y*z;
   values[149] = 7056*x*y - 1512*y - 4536*z - 3024*x + 21168*x*z + 10584*y*z - 4032*x*y*y - 6048*x*x*y - 24192*x*z*z - 18144*x*x*z - 12096*y*z*z - 6048*y*y*z + 5292*x*x - 2688*x*x*x + 1764*y*y - 672*y*y*y + 10584*z*z - 6720*z*z*z - 24192*x*y*z + 420;
   values[150] = 4032*x*z - 3024*z + 6048*y*z - 8064*x*z*z - 1008*x*x*z - 12096*y*z*z - 3024*y*y*z + 12096*z*z - 10080*z*z*z - 4032*x*y*z;
   values[151] = 1008*z - 4032*x*z - 2016*y*z + 8064*x*z*z + 3024*x*x*z + 4032*y*z*z + 1008*y*y*z - 4032*z*z + 3360*z*z*z + 4032*x*y*z;
   values[152] = 2016*x + 1008*y + 4032*z - 4032*x*y - 16128*x*z - 8064*y*z + 2016*x*y*y + 3024*x*x*y + 20160*x*z*z + 12096*x*x*z + 10080*y*z*z + 4032*y*y*z - 3024*x*x + 1344*x*x*x - 1008*y*y + 336*y*y*y - 10080*z*z + 6720*z*z*z + 16128*x*y*z - 336;
   values[153] = 8208*z - 19656*x*z - 20160*y*z + 24192*x*z*z + 8064*x*x*z + 24192*y*z*z + 12096*y*y*z - 20160*z*z + 12096*z*z*z + 24192*x*y*z;
   values[154] = 15120*x*z - 1944*z + 4032*y*z - 16128*x*z*z - 18144*x*x*z - 4032*y*z*z - 2016*y*y*z + 4032*z*z - 2016*z*z*z - 16128*x*y*z;
   values[155] = 15120*x*y - 1944*y - 3888*z - 6912*x + 30240*x*z + 8064*y*z - 8064*x*y*y - 18144*x*x*y - 24192*x*z*z - 36288*x*x*z - 6048*y*z*z - 4032*y*y*z + 16632*x*x - 10752*x*x*x + 2016*y*y - 672*y*y*y + 6048*z*z - 2688*z*z*z - 32256*x*y*z + 600;
   values[156] = 22680*x*z - 11088*z + 23184*y*z - 36288*x*z*z - 8064*x*x*z - 36288*y*z*z - 12096*y*y*z + 34776*z*z - 24192*z*z*z - 24192*x*y*z;
   values[157] = 2520*z - 17136*x*z - 4536*y*z + 24192*x*z*z + 18144*x*x*z + 6048*y*z*z + 2016*y*y*z - 6804*z*z + 4032*z*z*z + 16128*x*y*z;
   values[158] = 9072*x + 2520*y + 7560*z - 17136*x*y - 51408*x*z - 13608*y*z + 8064*x*y*y + 18144*x*x*y + 48384*x*z*z + 54432*x*x*z + 12096*y*z*z + 6048*y*y*z - 18900*x*x + 10752*x*x*x - 2268*y*y + 672*y*y*y - 13608*z*z + 6720*z*z*z + 48384*x*y*z - 924;
   values[159] = 21168*x*z - 7560*z + 16632*y*z - 24192*x*z*z - 10080*x*x*z - 18144*y*z*z - 9072*y*y*z + 16632*z*z - 9072*z*z*z - 24192*x*y*z;
   values[160] = 1512*z - 13608*x*z - 2520*y*z + 12096*x*z*z + 18144*x*x*z + 2016*y*z*z + 1008*y*y*z - 2520*z*z + 1008*z*z*z + 12096*x*y*z;
   values[161] = 7560*x + 1512*y + 3024*z - 13608*x*y - 27216*x*z - 5040*y*z + 6048*x*y*y + 18144*x*x*y + 18144*x*z*z + 36288*x*x*z + 3024*y*z*z + 2016*y*y*z - 19656*x*x + 13440*x*x*x - 1260*y*y + 336*y*y*y - 3780*z*z + 1344*z*z*z + 24192*x*y*z - 588;
   values[162] = 1080*z - 4536*x*z - 3024*y*z + 6048*x*z*z + 4032*x*x*z + 4032*y*z*z + 2016*y*y*z - 3024*z*z + 2016*z*z*z + 6048*x*y*z;
   values[163] = 540*z - 3024*x*z - 1512*y*z + 4032*x*z*z + 3024*x*x*z + 2016*y*z*z + 1008*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[164] = 12096*x*y - 2160*y - 1620*z - 4320*x + 9072*x*z + 4536*y*z - 8064*x*y*y - 12096*x*x*y - 4032*x*z*z - 9072*x*x*z - 2016*y*z*z - 3024*y*y*z + 9072*x*x - 5376*x*x*x + 3024*y*y - 1344*y*y*y + 1512*z*z - 336*z*z*z - 12096*x*y*z + 480;
   values[165] = 12096*x*z - 3888*z + 8064*y*z - 24192*x*z*z - 8064*x*x*z - 16128*y*z*z - 4032*y*y*z + 15120*z*z - 12096*z*z*z - 12096*x*y*z;
   values[166] = 8064*x*z - 1944*z + 4032*y*z - 16128*x*z*z - 6048*x*x*z - 8064*y*z*z - 2016*y*y*z + 7560*z*z - 6048*z*z*z - 8064*x*y*z;
   values[167] = 11232*x + 5616*y + 8208*z - 27216*x*y - 40320*x*z - 20160*y*z + 16128*x*y*y + 24192*x*x*y + 24192*x*z*z + 36288*x*x*z + 12096*y*z*z + 12096*y*y*z - 20412*x*x + 10752*x*x*x - 6804*y*y + 2688*y*y*y - 9828*z*z + 2688*z*z*z + 48384*x*y*z - 1500;
   values[168] = 3024*z - 7560*x*z - 5040*y*z + 18144*x*z*z + 4032*x*x*z + 12096*y*z*z + 2016*y*y*z - 13608*z*z + 12096*z*z*z + 6048*x*y*z;
   values[169] = 1512*z - 5040*x*z - 2520*y*z + 12096*x*z*z + 3024*x*x*z + 6048*y*z*z + 1008*y*y*z - 6804*z*z + 6048*z*z*z + 4032*x*y*z;
   values[170] = 15120*x*y - 3528*y - 7560*z - 7056*x + 33264*x*z + 16632*y*z - 8064*x*y*y - 12096*x*x*y - 24192*x*z*z - 27216*x*x*z - 12096*y*z*z - 9072*y*y*z + 11340*x*x - 5376*x*x*x + 3780*y*y - 1344*y*y*y + 10584*z*z - 3360*z*z*z - 36288*x*y*z + 1092;
   values[171] = 21168*x*z - 4536*z + 10584*y*z - 24192*x*z*z - 20160*x*x*z - 12096*y*z*z - 6048*y*y*z + 10584*z*z - 6048*z*z*z - 24192*x*y*z;
   values[172] = 10584*x*z - 1512*z + 3528*y*z - 12096*x*z*z - 12096*x*x*z - 4032*y*z*z - 2016*y*y*z + 3528*z*z - 2016*z*z*z - 12096*x*y*z;
   values[173] = 18144*x + 6048*y + 4536*z - 42336*x*y - 31752*x*z - 10584*y*z + 24192*x*y*y + 48384*x*x*y + 12096*x*z*z + 36288*x*x*z + 4032*y*z*z + 6048*y*y*z - 42336*x*x + 26880*x*x*x - 7056*y*y + 2688*y*y*y - 3528*z*z + 672*z*z*z + 36288*x*y*z - 1680;
   values[174] = 7560*z - 27216*x*z - 13608*y*z + 48384*x*z*z + 20160*x*x*z + 24192*y*z*z + 6048*y*y*z - 25704*z*z + 18144*z*z*z + 24192*x*y*z;
   values[175] = 2520*z - 13608*x*z - 4536*y*z + 24192*x*z*z + 12096*x*x*z + 8064*y*z*z + 2016*y*y*z - 8568*z*z + 6048*z*z*z + 12096*x*y*z;
   values[176] = 46872*x*y - 7560*y - 11088*z - 22680*x + 69552*x*z + 23184*y*z - 24192*x*y*y - 48384*x*x*y - 36288*x*z*z - 72576*x*x*z - 12096*y*z*z - 12096*y*y*z + 46872*x*x - 26880*x*x*x + 7812*y*y - 2688*y*y*y + 11340*z*z - 2688*z*z*z - 72576*x*y*z + 2436;
   values[177] = 4032*z - 20160*x*z - 8064*y*z + 20160*x*z*z + 20160*x*x*z + 8064*y*z*z + 4032*y*y*z - 8064*z*z + 4032*z*z*z + 20160*x*y*z;
   values[178] = 1008*z - 8064*x*z - 2016*y*z + 8064*x*z*z + 10080*x*x*z + 2016*y*z*z + 1008*y*y*z - 2016*z*z + 1008*z*z*z + 8064*x*y*z;
   values[179] = 32256*x*y - 4032*y - 3024*z - 16128*x + 24192*x*z + 6048*y*z - 16128*x*y*y - 40320*x*x*y - 8064*x*z*z - 30240*x*x*z - 2016*y*z*z - 3024*y*y*z + 40320*x*x - 26880*x*x*x + 4032*y*y - 1344*y*y*y + 2016*z*z - 336*z*z*z - 24192*x*y*z + 1344;
   values[180] = 3024*x*y - 1620*y + 4536*y*z - 4032*x*y*y - 1008*x*x*y - 3024*y*z*z - 6048*y*y*z + 4536*y*y - 3024*y*y*y - 4032*x*y*z;
   values[181] = 1080*x + 1080*y + 540*z - 6048*x*y - 3024*x*z - 3024*y*z + 6048*x*y*y + 6048*x*x*y + 2016*x*z*z + 3024*x*x*z + 2016*y*z*z + 3024*y*y*z - 2268*x*x + 1344*x*x*x - 2268*y*y + 1344*y*y*y - 756*z*z + 336*z*z*z + 8064*x*y*z - 120;
   values[182] = 540*y - 3024*x*y - 1512*y*z + 4032*x*y*y + 3024*x*x*y + 1008*y*z*z + 2016*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[183] = 4536*y - 7056*x*y - 10584*y*z + 12096*x*y*y + 2016*x*x*y + 6048*y*z*z + 18144*y*y*z - 15876*y*y + 12096*y*y*y + 8064*x*y*z;
   values[184] = 21168*x*y - 4536*y - 1512*z - 3024*x + 7056*x*z + 10584*y*z - 24192*x*y*y - 18144*x*x*y - 4032*x*z*z - 6048*x*x*z - 6048*y*z*z - 12096*y*y*z + 5292*x*x - 2688*x*x*x + 10584*y*y - 6720*y*y*y + 1764*z*z - 672*z*z*z - 24192*x*y*z + 420;
   values[185] = 7056*x*y - 1512*y + 3528*y*z - 12096*x*y*y - 6048*x*x*y - 2016*y*z*z - 6048*y*y*z + 5292*y*y - 4032*y*y*y - 8064*x*y*z;
   values[186] = 4032*x*y - 3024*y + 6048*y*z - 8064*x*y*y - 1008*x*x*y - 3024*y*z*z - 12096*y*y*z + 12096*y*y - 10080*y*y*y - 4032*x*y*z;
   values[187] = 2016*x + 4032*y + 1008*z - 16128*x*y - 4032*x*z - 8064*y*z + 20160*x*y*y + 12096*x*x*y + 2016*x*z*z + 3024*x*x*z + 4032*y*z*z + 10080*y*y*z - 3024*x*x + 1344*x*x*x - 10080*y*y + 6720*y*y*y - 1008*z*z + 336*z*z*z + 16128*x*y*z - 336;
   values[188] = 1008*y - 4032*x*y - 2016*y*z + 8064*x*y*y + 3024*x*x*y + 1008*y*z*z + 4032*y*y*z - 4032*y*y + 3360*y*y*y + 4032*x*y*z;
   values[189] = 8208*y - 19656*x*y - 20160*y*z + 24192*x*y*y + 8064*x*x*y + 12096*y*z*z + 24192*y*y*z - 20160*y*y + 12096*y*y*y + 24192*x*y*z;
   values[190] = 30240*x*y - 3888*y - 1944*z - 6912*x + 15120*x*z + 8064*y*z - 24192*x*y*y - 36288*x*x*y - 8064*x*z*z - 18144*x*x*z - 4032*y*z*z - 6048*y*y*z + 16632*x*x - 10752*x*x*x + 6048*y*y - 2688*y*y*y + 2016*z*z - 672*z*z*z - 32256*x*y*z + 600;
   values[191] = 15120*x*y - 1944*y + 4032*y*z - 16128*x*y*y - 18144*x*x*y - 2016*y*z*z - 4032*y*y*z + 4032*y*y - 2016*y*y*y - 16128*x*y*z;
   values[192] = 22680*x*y - 11088*y + 23184*y*z - 36288*x*y*y - 8064*x*x*y - 12096*y*z*z - 36288*y*y*z + 34776*y*y - 24192*y*y*y - 24192*x*y*z;
   values[193] = 9072*x + 7560*y + 2520*z - 51408*x*y - 17136*x*z - 13608*y*z + 48384*x*y*y + 54432*x*x*y + 8064*x*z*z + 18144*x*x*z + 6048*y*z*z + 12096*y*y*z - 18900*x*x + 10752*x*x*x - 13608*y*y + 6720*y*y*y - 2268*z*z + 672*z*z*z + 48384*x*y*z - 924;
   values[194] = 2520*y - 17136*x*y - 4536*y*z + 24192*x*y*y + 18144*x*x*y + 2016*y*z*z + 6048*y*y*z - 6804*y*y + 4032*y*y*y + 16128*x*y*z;
   values[195] = 21168*x*y - 7560*y + 16632*y*z - 24192*x*y*y - 10080*x*x*y - 9072*y*z*z - 18144*y*y*z + 16632*y*y - 9072*y*y*y - 24192*x*y*z;
   values[196] = 7560*x + 3024*y + 1512*z - 27216*x*y - 13608*x*z - 5040*y*z + 18144*x*y*y + 36288*x*x*y + 6048*x*z*z + 18144*x*x*z + 2016*y*z*z + 3024*y*y*z - 19656*x*x + 13440*x*x*x - 3780*y*y + 1344*y*y*y - 1260*z*z + 336*z*z*z + 24192*x*y*z - 588;
   values[197] = 1512*y - 13608*x*y - 2520*y*z + 12096*x*y*y + 18144*x*x*y + 1008*y*z*z + 2016*y*y*z - 2520*y*y + 1008*y*y*y + 12096*x*y*z;
   values[198] = 1080*y - 4536*x*y - 3024*y*z + 6048*x*y*y + 4032*x*x*y + 2016*y*z*z + 4032*y*y*z - 3024*y*y + 2016*y*y*y + 6048*x*y*z;
   values[199] = 9072*x*y - 1620*y - 2160*z - 4320*x + 12096*x*z + 4536*y*z - 4032*x*y*y - 9072*x*x*y - 8064*x*z*z - 12096*x*x*z - 3024*y*z*z - 2016*y*y*z + 9072*x*x - 5376*x*x*x + 1512*y*y - 336*y*y*y + 3024*z*z - 1344*z*z*z - 12096*x*y*z + 480;
   values[200] = 540*y - 3024*x*y - 1512*y*z + 4032*x*y*y + 3024*x*x*y + 1008*y*z*z + 2016*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[201] = 12096*x*y - 3888*y + 8064*y*z - 24192*x*y*y - 8064*x*x*y - 4032*y*z*z - 16128*y*y*z + 15120*y*y - 12096*y*y*y - 12096*x*y*z;
   values[202] = 11232*x + 8208*y + 5616*z - 40320*x*y - 27216*x*z - 20160*y*z + 24192*x*y*y + 36288*x*x*y + 16128*x*z*z + 24192*x*x*z + 12096*y*z*z + 12096*y*y*z - 20412*x*x + 10752*x*x*x - 9828*y*y + 2688*y*y*y - 6804*z*z + 2688*z*z*z + 48384*x*y*z - 1500;
   values[203] = 8064*x*y - 1944*y + 4032*y*z - 16128*x*y*y - 6048*x*x*y - 2016*y*z*z - 8064*y*y*z + 7560*y*y - 6048*y*y*y - 8064*x*y*z;
   values[204] = 3024*y - 7560*x*y - 5040*y*z + 18144*x*y*y + 4032*x*x*y + 2016*y*z*z + 12096*y*y*z - 13608*y*y + 12096*y*y*y + 6048*x*y*z;
   values[205] = 33264*x*y - 7560*y - 3528*z - 7056*x + 15120*x*z + 16632*y*z - 24192*x*y*y - 27216*x*x*y - 8064*x*z*z - 12096*x*x*z - 9072*y*z*z - 12096*y*y*z + 11340*x*x - 5376*x*x*x + 10584*y*y - 3360*y*y*y + 3780*z*z - 1344*z*z*z - 36288*x*y*z + 1092;
   values[206] = 1512*y - 5040*x*y - 2520*y*z + 12096*x*y*y + 3024*x*x*y + 1008*y*z*z + 6048*y*y*z - 6804*y*y + 6048*y*y*y + 4032*x*y*z;
   values[207] = 21168*x*y - 4536*y + 10584*y*z - 24192*x*y*y - 20160*x*x*y - 6048*y*z*z - 12096*y*y*z + 10584*y*y - 6048*y*y*y - 24192*x*y*z;
   values[208] = 18144*x + 4536*y + 6048*z - 31752*x*y - 42336*x*z - 10584*y*z + 12096*x*y*y + 36288*x*x*y + 24192*x*z*z + 48384*x*x*z + 6048*y*z*z + 4032*y*y*z - 42336*x*x + 26880*x*x*x - 3528*y*y + 672*y*y*y - 7056*z*z + 2688*z*z*z + 36288*x*y*z - 1680;
   values[209] = 10584*x*y - 1512*y + 3528*y*z - 12096*x*y*y - 12096*x*x*y - 2016*y*z*z - 4032*y*y*z + 3528*y*y - 2016*y*y*y - 12096*x*y*z;
   values[210] = 7560*y - 27216*x*y - 13608*y*z + 48384*x*y*y + 20160*x*x*y + 6048*y*z*z + 24192*y*y*z - 25704*y*y + 18144*y*y*y + 24192*x*y*z;
   values[211] = 69552*x*y - 11088*y - 7560*z - 22680*x + 46872*x*z + 23184*y*z - 36288*x*y*y - 72576*x*x*y - 24192*x*z*z - 48384*x*x*z - 12096*y*z*z - 12096*y*y*z + 46872*x*x - 26880*x*x*x + 11340*y*y - 2688*y*y*y + 7812*z*z - 2688*z*z*z - 72576*x*y*z + 2436;
   values[212] = 2520*y - 13608*x*y - 4536*y*z + 24192*x*y*y + 12096*x*x*y + 2016*y*z*z + 8064*y*y*z - 8568*y*y + 6048*y*y*y + 12096*x*y*z;
   values[213] = 4032*y - 20160*x*y - 8064*y*z + 20160*x*y*y + 20160*x*x*y + 4032*y*z*z + 8064*y*y*z - 8064*y*y + 4032*y*y*y + 20160*x*y*z;
   values[214] = 24192*x*y - 3024*y - 4032*z - 16128*x + 32256*x*z + 6048*y*z - 8064*x*y*y - 30240*x*x*y - 16128*x*z*z - 40320*x*x*z - 3024*y*z*z - 2016*y*y*z + 40320*x*x - 26880*x*x*x + 2016*y*y - 336*y*y*y + 4032*z*z - 1344*z*z*z - 24192*x*y*z + 1344;
   values[215] = 1008*y - 8064*x*y - 2016*y*z + 8064*x*y*y + 10080*x*x*y + 1008*y*z*z + 2016*y*y*z - 2016*y*y + 1008*y*y*y + 8064*x*y*z;
   values[216] = 24192*y*z*z - 21168*y*z + 24192*y*y*z + 16128*x*y*z;
   values[217] = 14112*x*z - 3024*z + 14112*y*z - 16128*x*z*z - 12096*x*x*z - 16128*y*z*z - 12096*y*y*z + 7056*z*z - 4032*z*z*z - 32256*x*y*z;
   values[218] = 14112*x*y - 3024*y + 14112*y*z - 16128*x*y*y - 12096*x*x*y - 12096*y*z*z - 16128*y*y*z + 7056*y*y - 4032*y*y*y - 32256*x*y*z;
   values[219] = 14112*y*z - 16128*y*z*z - 16128*y*y*z - 24192*x*y*z;
   values[220] = 12096*z - 56448*x*z - 21168*y*z + 64512*x*z*z + 48384*x*x*z + 24192*y*z*z + 8064*y*y*z - 28224*z*z + 16128*z*z*z + 48384*x*y*z;
   values[221] = 14112*x*y - 3024*y + 14112*y*z - 16128*x*y*y - 12096*x*x*y - 12096*y*z*z - 16128*y*y*z + 7056*y*y - 4032*y*y*y - 32256*x*y*z;
   values[222] = 14112*y*z - 16128*y*z*z - 16128*y*y*z - 24192*x*y*z;
   values[223] = 14112*x*z - 3024*z + 14112*y*z - 16128*x*z*z - 12096*x*x*z - 16128*y*z*z - 12096*y*y*z + 7056*z*z - 4032*z*z*z - 32256*x*y*z;
   values[224] = 12096*y - 56448*x*y - 21168*y*z + 64512*x*y*y + 48384*x*x*y + 8064*y*z*z + 24192*y*y*z - 28224*y*y + 16128*y*y*y + 48384*x*y*z;
   values[225] = 24192*y*z - 36288*y*z*z - 24192*y*y*z - 16128*x*y*z;
   values[226] = 4032*z - 16128*x*z - 16128*y*z + 24192*x*z*z + 12096*x*x*z + 24192*y*z*z + 12096*y*y*z - 12096*z*z + 8064*z*z*z + 32256*x*y*z;
   values[227] = 4032*y - 16128*x*y - 24192*y*z + 16128*x*y*y + 12096*x*x*y + 24192*y*z*z + 24192*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[228] = 24192*y*z*z - 16128*y*z + 16128*y*y*z + 24192*x*y*z;
   values[229] = 64512*x*z - 16128*z + 24192*y*z - 96768*x*z*z - 48384*x*x*z - 36288*y*z*z - 8064*y*y*z + 48384*z*z - 32256*z*z*z - 48384*x*y*z;
   values[230] = 4032*y - 16128*x*y - 24192*y*z + 16128*x*y*y + 12096*x*x*y + 24192*y*z*z + 24192*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[231] = 32256*y*z*z - 18144*y*z + 16128*y*y*z + 24192*x*y*z;
   values[232] = 5040*z - 18144*x*z - 18144*y*z + 32256*x*z*z + 12096*x*x*z + 32256*y*z*z + 12096*y*y*z - 17136*z*z + 12096*z*z*z + 32256*x*y*z;
   values[233] = 62496*x*y - 15120*y + 46368*y*z - 64512*x*y*y - 48384*x*x*y - 24192*y*z*z - 48384*y*y*z + 31248*y*y - 16128*y*y*y - 96768*x*y*z;
   values[234] = 24192*y*z - 24192*y*z*z - 36288*y*y*z - 16128*x*y*z;
   values[235] = 4032*z - 16128*x*z - 24192*y*z + 16128*x*z*z + 12096*x*x*z + 24192*y*z*z + 24192*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[236] = 4032*y - 16128*x*y - 16128*y*z + 24192*x*y*y + 12096*x*x*y + 12096*y*z*z + 24192*y*y*z - 12096*y*y + 8064*y*y*y + 32256*x*y*z;
   values[237] = 16128*y*z*z - 18144*y*z + 32256*y*y*z + 24192*x*y*z;
   values[238] = 62496*x*z - 15120*z + 46368*y*z - 64512*x*z*z - 48384*x*x*z - 48384*y*z*z - 24192*y*y*z + 31248*z*z - 16128*z*z*z - 96768*x*y*z;
   values[239] = 5040*y - 18144*x*y - 18144*y*z + 32256*x*y*y + 12096*x*x*y + 12096*y*z*z + 32256*y*y*z - 17136*y*y + 12096*y*y*y + 32256*x*y*z;
   values[240] = 16128*y*z*z - 16128*y*z + 24192*y*y*z + 24192*x*y*z;
   values[241] = 4032*z - 16128*x*z - 24192*y*z + 16128*x*z*z + 12096*x*x*z + 24192*y*z*z + 24192*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[242] = 64512*x*y - 16128*y + 24192*y*z - 96768*x*y*y - 48384*x*x*y - 8064*y*z*z - 36288*y*y*z + 48384*y*y - 32256*y*y*y - 48384*x*y*z;
   values[243] = 46368*y*z - 48384*y*z*z - 48384*y*y*z - 48384*x*y*z;
   values[244] = 5040*z - 34272*x*z - 18144*y*z + 32256*x*z*z + 36288*x*x*z + 16128*y*z*z + 12096*y*y*z - 9072*z*z + 4032*z*z*z + 64512*x*y*z;
   values[245] = 5040*y - 34272*x*y - 18144*y*z + 32256*x*y*y + 36288*x*x*y + 12096*y*z*z + 16128*y*y*z - 9072*y*y + 4032*y*y*y + 64512*x*y*z;
   values[246] = 24192*y*z*z - 24192*y*z + 24192*y*y*z + 48384*x*y*z;
   values[247] = 96768*x*z - 16128*z + 24192*y*z - 96768*x*z*z - 96768*x*x*z - 24192*y*z*z - 8064*y*y*z + 32256*z*z - 16128*z*z*z - 72576*x*y*z;
   values[248] = 4032*y - 24192*x*y - 16128*y*z + 24192*x*y*y + 24192*x*x*y + 12096*y*z*z + 16128*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[249] = 24192*y*z*z - 24192*y*z + 24192*y*y*z + 48384*x*y*z;
   values[250] = 4032*z - 24192*x*z - 16128*y*z + 24192*x*z*z + 24192*x*x*z + 16128*y*z*z + 12096*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[251] = 96768*x*y - 16128*y + 24192*y*z - 96768*x*y*y - 96768*x*x*y - 8064*y*z*z - 24192*y*y*z + 32256*y*y - 16128*y*y*y - 72576*x*y*z;
}
static void Curl_T_P4_3D_3D_D010(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 720*x + 960*y + 960*z - 2520*x*y - 2520*x*z - 3360*y*z + 2016*x*y*y + 1344*x*x*y + 2016*x*z*z + 1344*x*x*z + 2688*y*z*z + 2688*y*y*z - 840*x*x + 224*x*x*x - 1680*y*y + 896*y*y*y - 1680*z*z + 896*z*z*z + 4032*x*y*z - 160;
   values[1] = 840*x*y - 240*x + 840*x*z - 672*x*y*y - 1344*x*x*y - 672*x*z*z - 1344*x*x*z + 840*x*x - 672*x*x*x - 1344*x*y*z;
   values[2] = 840*x*y - 240*x + 840*x*z - 672*x*y*y - 1344*x*x*y - 672*x*z*z - 1344*x*x*z + 840*x*x - 672*x*x*x - 1344*x*y*z;
   values[3] = 17136*x*y - 3960*y - 3960*z - 5760*x + 17136*x*z + 11592*y*z - 12096*x*y*y - 12096*x*x*y - 12096*x*z*z - 12096*x*x*z - 8064*y*z*z - 8064*y*y*z + 8316*x*x - 2688*x*x*x + 5796*y*y - 2688*y*y*y + 5796*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[4] = 1440*x - 3528*x*y - 3528*x*z + 2016*x*y*y + 8064*x*x*y + 2016*x*z*z + 8064*x*x*z - 6552*x*x + 6048*x*x*x + 4032*x*y*z;
   values[5] = 1440*x - 3528*x*y - 3528*x*z + 2016*x*y*y + 8064*x*x*y + 2016*x*z*z + 8064*x*x*z - 6552*x*x + 6048*x*x*x + 4032*x*y*z;
   values[6] = 11016*x + 5184*y + 5184*z - 28728*x*y - 28728*x*z - 13104*y*z + 18144*x*y*y + 24192*x*x*y + 18144*x*z*z + 24192*x*x*z + 8064*y*z*z + 8064*y*y*z - 18144*x*x + 6720*x*x*x - 6552*y*y + 2688*y*y*y - 6552*z*z + 2688*z*z*z + 36288*x*y*z - 1320;
   values[7] = 4536*x*y - 2376*x + 4536*x*z - 2016*x*y*y - 12096*x*x*y - 2016*x*z*z - 12096*x*x*z + 12096*x*x - 12096*x*x*x - 4032*x*y*z;
   values[8] = 4536*x*y - 2376*x + 4536*x*z - 2016*x*y*y - 12096*x*x*y - 2016*x*z*z - 12096*x*x*z + 12096*x*x - 12096*x*x*x - 4032*x*y*z;
   values[9] = 14112*x*y - 2184*y - 2184*z - 6048*x + 14112*x*z + 4872*y*z - 8064*x*y*y - 13440*x*x*y - 8064*x*z*z - 13440*x*x*z - 2688*y*z*z - 2688*y*y*z + 10920*x*x - 4480*x*x*x + 2436*y*y - 896*y*y*y + 2436*z*z - 896*z*z*z - 16128*x*y*z + 644;
   values[10] = 1176*x - 1848*x*y - 1848*x*z + 672*x*y*y + 5376*x*x*y + 672*x*z*z + 5376*x*x*z - 6384*x*x + 6720*x*x*x + 1344*x*y*z;
   values[11] = 1176*x - 1848*x*y - 1848*x*z + 672*x*y*y + 5376*x*x*y + 672*x*z*z + 5376*x*x*z - 6384*x*x + 6720*x*x*x + 1344*x*y*z;
   values[12] = 1680*x*y - 480*y - 240*z - 240*x + 840*x*z + 1680*y*z - 2016*x*y*y - 1344*x*x*y - 672*x*z*z - 672*x*x*z - 1344*y*z*z - 2016*y*y*z + 420*x*x - 224*x*x*x + 1260*y*y - 896*y*y*y + 420*z*z - 224*z*z*z - 2688*x*y*z + 40;
   values[13] = 720*x + 480*y + 720*z - 1680*x*y - 2520*x*z - 1680*y*z + 672*x*y*y + 1344*x*x*y + 2016*x*z*z + 2016*x*x*z + 1344*y*z*z + 672*y*y*z - 1260*x*x + 672*x*x*x - 420*y*y - 1260*z*z + 672*z*z*z + 2688*x*y*z - 120;
   values[14] = 1680*x*y - 480*y - 240*z - 240*x + 840*x*z + 1680*y*z - 2016*x*y*y - 1344*x*x*y - 672*x*z*z - 672*x*x*z - 1344*y*z*z - 2016*y*y*z + 420*x*x - 224*x*x*x + 1260*y*y - 896*y*y*y + 420*z*z - 224*z*z*z - 2688*x*y*z + 40;
   values[15] = 1440*x + 5040*y + 1440*z - 13104*x*y - 3528*x*z - 13104*y*z + 18144*x*y*y + 8064*x*x*y + 2016*x*z*z + 2016*x*x*z + 8064*y*z*z + 18144*y*y*z - 1764*x*x + 672*x*x*x - 14364*y*y + 10752*y*y*y - 1764*z*z + 672*z*z*z + 16128*x*y*z - 360;
   values[16] = 16632*x*y - 5400*y - 5760*z - 5760*x + 17136*x*z + 16632*y*z - 8064*x*y*y - 12096*x*x*y - 12096*x*z*z - 12096*x*x*z - 12096*y*z*z - 8064*y*y*z + 8568*x*x - 4032*x*x*x + 5040*y*y + 8568*z*z - 4032*z*z*z - 24192*x*y*z + 1200;
   values[17] = 1440*x + 5040*y + 1440*z - 13104*x*y - 3528*x*z - 13104*y*z + 18144*x*y*y + 8064*x*x*y + 2016*x*z*z + 2016*x*x*z + 8064*y*z*z + 18144*y*y*z - 1764*x*x + 672*x*x*x - 14364*y*y + 10752*y*y*y - 1764*z*z + 672*z*z*z + 16128*x*y*z - 360;
   values[18] = 24192*x*y - 11664*y - 2376*z - 2376*x + 4536*x*z + 24192*y*z - 36288*x*y*y - 12096*x*x*y - 2016*x*z*z - 2016*x*x*z - 12096*y*z*z - 36288*y*y*z + 2268*x*x - 672*x*x*x + 34776*y*y - 26880*y*y*y + 2268*z*z - 672*z*z*z - 24192*x*y*z + 780;
   values[19] = 11016*x + 12960*y + 11016*z - 36288*x*y - 28728*x*z - 36288*y*z + 20160*x*y*y + 24192*x*x*y + 18144*x*z*z + 18144*x*x*z + 24192*y*z*z + 20160*y*y*z - 14364*x*x + 6048*x*x*x - 12600*y*y - 14364*z*z + 6048*z*z*z + 48384*x*y*z - 2700;
   values[20] = 24192*x*y - 11664*y - 2376*z - 2376*x + 4536*x*z + 24192*y*z - 36288*x*y*y - 12096*x*x*y - 2016*x*z*z - 2016*x*x*z - 12096*y*z*z - 36288*y*y*z + 2268*x*x - 672*x*x*x + 34776*y*y - 26880*y*y*y + 2268*z*z - 672*z*z*z - 24192*x*y*z + 780;
   values[21] = 1176*x + 7392*y + 1176*z - 12768*x*y - 1848*x*z - 12768*y*z + 20160*x*y*y + 5376*x*x*y + 672*x*z*z + 672*x*x*z + 5376*y*z*z + 20160*y*y*z - 924*x*x + 224*x*x*x - 22680*y*y + 17920*y*y*y - 924*z*z + 224*z*z*z + 10752*x*y*z - 476;
   values[22] = 21840*x*y - 8400*y - 6048*z - 6048*x + 14112*x*z + 21840*y*z - 13440*x*y*y - 13440*x*x*y - 8064*x*z*z - 8064*x*x*z - 13440*y*z*z - 13440*y*y*z + 7056*x*x - 2688*x*x*x + 8400*y*y + 7056*z*z - 2688*z*z*z - 26880*x*y*z + 1680;
   values[23] = 1176*x + 7392*y + 1176*z - 12768*x*y - 1848*x*z - 12768*y*z + 20160*x*y*y + 5376*x*x*y + 672*x*z*z + 672*x*x*z + 5376*y*z*z + 20160*y*y*z - 924*x*x + 224*x*x*x - 22680*y*y + 17920*y*y*y - 924*z*z + 224*z*z*z + 10752*x*y*z - 476;
   values[24] = 840*x*z - 240*z + 840*y*z - 1344*x*z*z - 672*x*x*z - 1344*y*z*z - 672*y*y*z + 840*z*z - 672*z*z*z - 1344*x*y*z;
   values[25] = 840*x*z - 240*z + 840*y*z - 1344*x*z*z - 672*x*x*z - 1344*y*z*z - 672*y*y*z + 840*z*z - 672*z*z*z - 1344*x*y*z;
   values[26] = 960*x + 960*y + 720*z - 3360*x*y - 2520*x*z - 2520*y*z + 2688*x*y*y + 2688*x*x*y + 1344*x*z*z + 2016*x*x*z + 1344*y*z*z + 2016*y*y*z - 1680*x*x + 896*x*x*x - 1680*y*y + 896*y*y*y - 840*z*z + 224*z*z*z + 4032*x*y*z - 160;
   values[27] = 1440*z - 3528*x*z - 3528*y*z + 8064*x*z*z + 2016*x*x*z + 8064*y*z*z + 2016*y*y*z - 6552*z*z + 6048*z*z*z + 4032*x*y*z;
   values[28] = 1440*z - 3528*x*z - 3528*y*z + 8064*x*z*z + 2016*x*x*z + 8064*y*z*z + 2016*y*y*z - 6552*z*z + 6048*z*z*z + 4032*x*y*z;
   values[29] = 11592*x*y - 3960*y - 5760*z - 3960*x + 17136*x*z + 17136*y*z - 8064*x*y*y - 8064*x*x*y - 12096*x*z*z - 12096*x*x*z - 12096*y*z*z - 12096*y*y*z + 5796*x*x - 2688*x*x*x + 5796*y*y - 2688*y*y*y + 8316*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[30] = 4536*x*z - 2376*z + 4536*y*z - 12096*x*z*z - 2016*x*x*z - 12096*y*z*z - 2016*y*y*z + 12096*z*z - 12096*z*z*z - 4032*x*y*z;
   values[31] = 4536*x*z - 2376*z + 4536*y*z - 12096*x*z*z - 2016*x*x*z - 12096*y*z*z - 2016*y*y*z + 12096*z*z - 12096*z*z*z - 4032*x*y*z;
   values[32] = 5184*x + 5184*y + 11016*z - 13104*x*y - 28728*x*z - 28728*y*z + 8064*x*y*y + 8064*x*x*y + 24192*x*z*z + 18144*x*x*z + 24192*y*z*z + 18144*y*y*z - 6552*x*x + 2688*x*x*x - 6552*y*y + 2688*y*y*y - 18144*z*z + 6720*z*z*z + 36288*x*y*z - 1320;
   values[33] = 1176*z - 1848*x*z - 1848*y*z + 5376*x*z*z + 672*x*x*z + 5376*y*z*z + 672*y*y*z - 6384*z*z + 6720*z*z*z + 1344*x*y*z;
   values[34] = 1176*z - 1848*x*z - 1848*y*z + 5376*x*z*z + 672*x*x*z + 5376*y*z*z + 672*y*y*z - 6384*z*z + 6720*z*z*z + 1344*x*y*z;
   values[35] = 4872*x*y - 2184*y - 6048*z - 2184*x + 14112*x*z + 14112*y*z - 2688*x*y*y - 2688*x*x*y - 13440*x*z*z - 8064*x*x*z - 13440*y*z*z - 8064*y*y*z + 2436*x*x - 896*x*x*x + 2436*y*y - 896*y*y*y + 10920*z*z - 4480*z*z*z - 16128*x*y*z + 644;
   values[36] = 252*x*x - 72*x - 224*x*x*x + 4;
   values[37] = 0;
   values[38] = 0;
   values[39] = 2016*x*y - 144*y - 72*x - 4032*x*x*y - 252*x*x + 672*x*x*x + 12;
   values[40] = 216*x - 1512*x*x + 2016*x*x*x;
   values[41] = 0;
   values[42] = 144*x - 144*y - 1008*x*y - 6048*x*y*y + 8064*x*x*y - 252*x*x - 672*x*x*x + 756*y*y;
   values[43] = 4032*x*x*y - 1008*x*y - 144*x + 2016*x*x - 4032*x*x*x;
   values[44] = 0;
   values[45] = 6048*x*y*y - 1008*x*y - 4032*x*x*y + 252*x*x + 224*x*x*x + 252*y*y - 896*y*y*y;
   values[46] = 504*x*y + 672*x*y*y - 4032*x*x*y - 504*x*x + 2016*x*x*x;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = 0;
   values[58] = 0;
   values[59] = 0;
   values[60] = 0;
   values[61] = 504*y*z - 72*z - 672*y*y*z;
   values[62] = 288*y - 1008*y*y + 896*y*y*y - 16;
   values[63] = 0;
   values[64] = 2016*y*y*z - 504*y*z - 4032*y*z*z - 72*z + 1008*z*z;
   values[65] = 216*z - 432*y - 3024*y*z + 6048*y*y*z + 2268*y*y - 2688*y*y*y + 12;
   values[66] = 0;
   values[67] = 144*z - 504*y*z + 8064*y*z*z - 2016*y*y*z - 504*z*z - 2016*z*z*z;
   values[68] = 144*y - 144*z + 4032*y*z + 4032*y*z*z - 12096*y*y*z - 1512*y*y + 2688*y*y*y - 504*z*z;
   values[69] = 0;
   values[70] = 504*y*z - 4032*y*z*z + 672*y*y*z - 504*z*z + 2016*z*z*z;
   values[71] = 6048*y*y*z - 4032*y*z*z - 1008*y*z + 252*y*y - 896*y*y*y + 252*z*z + 224*z*z*z;
   values[72] = 504*x*z - 36*z - 1008*x*x*z;
   values[73] = 0;
   values[74] = 252*x*x - 36*x - 336*x*x*x;
   values[75] = 2016*x*x*z - 2016*x*z*z - 504*x*z + 252*z*z;
   values[76] = 0;
   values[77] = 504*x*z - 2016*x*x*z - 252*x*x + 672*x*x*x;
   values[78] = 2016*x*z*z - 1008*x*x*z - 336*z*z*z;
   values[79] = 0;
   values[80] = 2016*x*x*z - 1008*x*z*z - 336*x*x*x;
   values[81] = 1008*y*z - 72*z + 2016*x*x*z - 8064*x*y*z;
   values[82] = 6048*x*x*z - 1512*x*z;
   values[83] = 1008*x*y - 72*x - 4032*x*x*y + 672*x*x*x;
   values[84] = 2016*x*z*z - 504*x*z - 2016*x*x*z - 4032*y*z*z + 252*z*z + 8064*x*y*z;
   values[85] = 6048*x*z*z - 6048*x*x*z;
   values[86] = 504*x*z + 4032*x*x*y + 2016*x*x*z - 252*x*x - 672*x*x*x - 8064*x*y*z;
   values[87] = 504*y*z - 504*x*z - 1008*x*x*z - 3024*y*y*z + 8064*x*y*z;
   values[88] = 504*x*z - 6048*x*x*z + 4032*x*y*z;
   values[89] = 504*x*y - 3024*x*y*y + 4032*x*x*y - 252*x*x - 336*x*x*x;
   values[90] = 504*x*z - 36*z - 1008*x*x*z;
   values[91] = 0;
   values[92] = 144*x - 1008*x*x + 1344*x*x*x;
   values[93] = 2016*x*x*z - 4032*x*z*z - 72*z + 504*z*z;
   values[94] = 0;
   values[95] = 6048*x*x*z - 1512*x*z - 72*x + 1260*x*x - 2688*x*x*x;
   values[96] = 4032*x*z*z - 504*x*z - 1008*x*x*z + 252*z*z - 1008*z*z*z;
   values[97] = 0;
   values[98] = 504*x*z + 2016*x*z*z - 6048*x*x*z - 252*x*x + 1344*x*x*x;
   values[99] = 504*y*z - 504*x*z + 2016*x*x*z - 4032*x*y*z;
   values[100] = 504*x*z - 2016*x*x*z;
   values[101] = 8064*x*x*y - 2016*x*y + 1008*x*x - 2688*x*x*x;
   values[102] = 504*y*z - 504*x*z + 4032*x*z*z - 2016*x*x*z - 4032*y*z*z + 4032*x*y*z;
   values[103] = 504*x*z - 4032*x*z*z + 2016*x*x*z;
   values[104] = 504*x*y - 8064*x*x*y - 6048*x*x*z - 252*x*x + 2688*x*x*x + 12096*x*y*z;
   values[105] = 4032*x*y*z - 1008*y*y*z - 1008*x*x*z;
   values[106] = 2016*x*x*z - 2016*x*y*z;
   values[107] = 4032*x*y*y - 8064*x*x*y + 1344*x*x*x;
   values[108] = 540*z - 1512*x*z - 3024*y*z + 2016*x*z*z + 1008*x*x*z + 4032*y*z*z + 3024*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[109] = 4536*x*z - 1620*z + 3024*y*z - 6048*x*z*z - 3024*x*x*z - 4032*y*z*z - 1008*y*y*z + 4536*z*z - 3024*z*z*z - 4032*x*y*z;
   values[110] = 540*x + 1080*y + 1080*z - 3024*x*y - 3024*x*z - 6048*y*z + 3024*x*y*y + 2016*x*x*y + 3024*x*z*z + 2016*x*x*z + 6048*y*z*z + 6048*y*y*z - 756*x*x + 336*x*x*x - 2268*y*y + 1344*y*y*y - 2268*z*z + 1344*z*z*z + 8064*x*y*z - 120;
   values[111] = 3528*x*z - 1512*z + 7056*y*z - 6048*x*z*z - 2016*x*x*z - 12096*y*z*z - 6048*y*y*z + 5292*z*z - 4032*z*z*z - 8064*x*y*z;
   values[112] = 4536*z - 10584*x*z - 7056*y*z + 18144*x*z*z + 6048*x*x*z + 12096*y*z*z + 2016*y*y*z - 15876*z*z + 12096*z*z*z + 8064*x*y*z;
   values[113] = 7056*x*y - 3024*y - 4536*z - 1512*x + 10584*x*z + 21168*y*z - 6048*x*y*y - 4032*x*x*y - 12096*x*z*z - 6048*x*x*z - 24192*y*z*z - 18144*y*y*z + 1764*x*x - 672*x*x*x + 5292*y*y - 2688*y*y*y + 10584*z*z - 6720*z*z*z - 24192*x*y*z + 420;
   values[114] = 1008*z - 2016*x*z - 4032*y*z + 4032*x*z*z + 1008*x*x*z + 8064*y*z*z + 3024*y*y*z - 4032*z*z + 3360*z*z*z + 4032*x*y*z;
   values[115] = 6048*x*z - 3024*z + 4032*y*z - 12096*x*z*z - 3024*x*x*z - 8064*y*z*z - 1008*y*y*z + 12096*z*z - 10080*z*z*z - 4032*x*y*z;
   values[116] = 1008*x + 2016*y + 4032*z - 4032*x*y - 8064*x*z - 16128*y*z + 3024*x*y*y + 2016*x*x*y + 10080*x*z*z + 4032*x*x*z + 20160*y*z*z + 12096*y*y*z - 1008*x*x + 336*x*x*x - 3024*y*y + 1344*y*y*y - 10080*z*z + 6720*z*z*z + 16128*x*y*z - 336;
   values[117] = 4032*x*z - 1944*z + 15120*y*z - 4032*x*z*z - 2016*x*x*z - 16128*y*z*z - 18144*y*y*z + 4032*z*z - 2016*z*z*z - 16128*x*y*z;
   values[118] = 8208*z - 20160*x*z - 19656*y*z + 24192*x*z*z + 12096*x*x*z + 24192*y*z*z + 8064*y*y*z - 20160*z*z + 12096*z*z*z + 24192*x*y*z;
   values[119] = 15120*x*y - 6912*y - 3888*z - 1944*x + 8064*x*z + 30240*y*z - 18144*x*y*y - 8064*x*x*y - 6048*x*z*z - 4032*x*x*z - 24192*y*z*z - 36288*y*y*z + 2016*x*x - 672*x*x*x + 16632*y*y - 10752*y*y*y + 6048*z*z - 2688*z*z*z - 32256*x*y*z + 600;
   values[120] = 2520*z - 4536*x*z - 17136*y*z + 6048*x*z*z + 2016*x*x*z + 24192*y*z*z + 18144*y*y*z - 6804*z*z + 4032*z*z*z + 16128*x*y*z;
   values[121] = 23184*x*z - 11088*z + 22680*y*z - 36288*x*z*z - 12096*x*x*z - 36288*y*z*z - 8064*y*y*z + 34776*z*z - 24192*z*z*z - 24192*x*y*z;
   values[122] = 2520*x + 9072*y + 7560*z - 17136*x*y - 13608*x*z - 51408*y*z + 18144*x*y*y + 8064*x*x*y + 12096*x*z*z + 6048*x*x*z + 48384*y*z*z + 54432*y*y*z - 2268*x*x + 672*x*x*x - 18900*y*y + 10752*y*y*y - 13608*z*z + 6720*z*z*z + 48384*x*y*z - 924;
   values[123] = 1512*z - 2520*x*z - 13608*y*z + 2016*x*z*z + 1008*x*x*z + 12096*y*z*z + 18144*y*y*z - 2520*z*z + 1008*z*z*z + 12096*x*y*z;
   values[124] = 16632*x*z - 7560*z + 21168*y*z - 18144*x*z*z - 9072*x*x*z - 24192*y*z*z - 10080*y*y*z + 16632*z*z - 9072*z*z*z - 24192*x*y*z;
   values[125] = 1512*x + 7560*y + 3024*z - 13608*x*y - 5040*x*z - 27216*y*z + 18144*x*y*y + 6048*x*x*y + 3024*x*z*z + 2016*x*x*z + 18144*y*z*z + 36288*y*y*z - 1260*x*x + 336*x*x*x - 19656*y*y + 13440*y*y*y - 3780*z*z + 1344*z*z*z + 24192*x*y*z - 588;
   values[126] = 540*z - 1512*x*z - 3024*y*z + 2016*x*z*z + 1008*x*x*z + 4032*y*z*z + 3024*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[127] = 1080*z - 3024*x*z - 4536*y*z + 4032*x*z*z + 2016*x*x*z + 6048*y*z*z + 4032*y*y*z - 3024*z*z + 2016*z*z*z + 6048*x*y*z;
   values[128] = 12096*x*y - 4320*y - 1620*z - 2160*x + 4536*x*z + 9072*y*z - 12096*x*y*y - 8064*x*x*y - 2016*x*z*z - 3024*x*x*z - 4032*y*z*z - 9072*y*y*z + 3024*x*x - 1344*x*x*x + 9072*y*y - 5376*y*y*y + 1512*z*z - 336*z*z*z - 12096*x*y*z + 480;
   values[129] = 4032*x*z - 1944*z + 8064*y*z - 8064*x*z*z - 2016*x*x*z - 16128*y*z*z - 6048*y*y*z + 7560*z*z - 6048*z*z*z - 8064*x*y*z;
   values[130] = 8064*x*z - 3888*z + 12096*y*z - 16128*x*z*z - 4032*x*x*z - 24192*y*z*z - 8064*y*y*z + 15120*z*z - 12096*z*z*z - 12096*x*y*z;
   values[131] = 5616*x + 11232*y + 8208*z - 27216*x*y - 20160*x*z - 40320*y*z + 24192*x*y*y + 16128*x*x*y + 12096*x*z*z + 12096*x*x*z + 24192*y*z*z + 36288*y*y*z - 6804*x*x + 2688*x*x*x - 20412*y*y + 10752*y*y*y - 9828*z*z + 2688*z*z*z + 48384*x*y*z - 1500;
   values[132] = 1512*z - 2520*x*z - 5040*y*z + 6048*x*z*z + 1008*x*x*z + 12096*y*z*z + 3024*y*y*z - 6804*z*z + 6048*z*z*z + 4032*x*y*z;
   values[133] = 3024*z - 5040*x*z - 7560*y*z + 12096*x*z*z + 2016*x*x*z + 18144*y*z*z + 4032*y*y*z - 13608*z*z + 12096*z*z*z + 6048*x*y*z;
   values[134] = 15120*x*y - 7056*y - 7560*z - 3528*x + 16632*x*z + 33264*y*z - 12096*x*y*y - 8064*x*x*y - 12096*x*z*z - 9072*x*x*z - 24192*y*z*z - 27216*y*y*z + 3780*x*x - 1344*x*x*x + 11340*y*y - 5376*y*y*y + 10584*z*z - 3360*z*z*z - 36288*x*y*z + 1092;
   values[135] = 3528*x*z - 1512*z + 10584*y*z - 4032*x*z*z - 2016*x*x*z - 12096*y*z*z - 12096*y*y*z + 3528*z*z - 2016*z*z*z - 12096*x*y*z;
   values[136] = 10584*x*z - 4536*z + 21168*y*z - 12096*x*z*z - 6048*x*x*z - 24192*y*z*z - 20160*y*y*z + 10584*z*z - 6048*z*z*z - 24192*x*y*z;
   values[137] = 6048*x + 18144*y + 4536*z - 42336*x*y - 10584*x*z - 31752*y*z + 48384*x*y*y + 24192*x*x*y + 4032*x*z*z + 6048*x*x*z + 12096*y*z*z + 36288*y*y*z - 7056*x*x + 2688*x*x*x - 42336*y*y + 26880*y*y*y - 3528*z*z + 672*z*z*z + 36288*x*y*z - 1680;
   values[138] = 2520*z - 4536*x*z - 13608*y*z + 8064*x*z*z + 2016*x*x*z + 24192*y*z*z + 12096*y*y*z - 8568*z*z + 6048*z*z*z + 12096*x*y*z;
   values[139] = 7560*z - 13608*x*z - 27216*y*z + 24192*x*z*z + 6048*x*x*z + 48384*y*z*z + 20160*y*y*z - 25704*z*z + 18144*z*z*z + 24192*x*y*z;
   values[140] = 46872*x*y - 22680*y - 11088*z - 7560*x + 23184*x*z + 69552*y*z - 48384*x*y*y - 24192*x*x*y - 12096*x*z*z - 12096*x*x*z - 36288*y*z*z - 72576*y*y*z + 7812*x*x - 2688*x*x*x + 46872*y*y - 26880*y*y*y + 11340*z*z - 2688*z*z*z - 72576*x*y*z + 2436;
   values[141] = 1008*z - 2016*x*z - 8064*y*z + 2016*x*z*z + 1008*x*x*z + 8064*y*z*z + 10080*y*y*z - 2016*z*z + 1008*z*z*z + 8064*x*y*z;
   values[142] = 4032*z - 8064*x*z - 20160*y*z + 8064*x*z*z + 4032*x*x*z + 20160*y*z*z + 20160*y*y*z - 8064*z*z + 4032*z*z*z + 20160*x*y*z;
   values[143] = 32256*x*y - 16128*y - 3024*z - 4032*x + 6048*x*z + 24192*y*z - 40320*x*y*y - 16128*x*x*y - 2016*x*z*z - 3024*x*x*z - 8064*y*z*z - 30240*y*y*z + 4032*x*x - 1344*x*x*x + 40320*y*y - 26880*y*y*y + 2016*z*z - 336*z*z*z - 24192*x*y*z + 1344;
   values[144] = 4536*x*z - 2160*z + 6048*y*z - 6048*x*z*z - 2016*x*x*z - 8064*y*z*z - 4032*y*y*z + 6048*z*z - 4032*z*z*z - 6048*x*y*z;
   values[145] = 2016*x*z*z - 1512*x*z + 2016*x*x*z + 2016*x*y*z;
   values[146] = 540*x - 1512*x*y - 3024*x*z + 1008*x*y*y + 2016*x*x*y + 3024*x*z*z + 4032*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[147] = 6048*z - 10584*x*z - 14112*y*z + 18144*x*z*z + 4032*x*x*z + 24192*y*z*z + 8064*y*y*z - 21168*z*z + 16128*z*z*z + 12096*x*y*z;
   values[148] = 3528*x*z - 6048*x*z*z - 4032*x*x*z - 4032*x*y*z;
   values[149] = 3528*x*y - 1512*x + 10584*x*z - 2016*x*y*y - 4032*x*x*y - 12096*x*z*z - 12096*x*x*z + 3528*x*x - 2016*x*x*x - 12096*x*y*z;
   values[150] = 6048*x*z - 4032*z + 8064*y*z - 12096*x*z*z - 2016*x*x*z - 16128*y*z*z - 4032*y*y*z + 16128*z*z - 13440*z*z*z - 6048*x*y*z;
   values[151] = 4032*x*z*z - 2016*x*z + 2016*x*x*z + 2016*x*y*z;
   values[152] = 1008*x - 2016*x*y - 8064*x*z + 1008*x*y*y + 2016*x*x*y + 10080*x*z*z + 8064*x*x*z - 2016*x*x + 1008*x*x*x + 8064*x*y*z;
   values[153] = 5616*z - 20160*x*z - 13608*y*z + 24192*x*z*z + 12096*x*x*z + 16128*y*z*z + 8064*y*y*z - 13608*z*z + 8064*z*z*z + 24192*x*y*z;
   values[154] = 4032*x*z - 4032*x*z*z - 8064*x*x*z - 4032*x*y*z;
   values[155] = 4032*x*y - 1944*x + 8064*x*z - 2016*x*y*y - 8064*x*x*y - 6048*x*z*z - 16128*x*x*z + 7560*x*x - 6048*x*x*x - 8064*x*y*z;
   values[156] = 23184*x*z - 7560*z + 15624*y*z - 36288*x*z*z - 12096*x*x*z - 24192*y*z*z - 8064*y*y*z + 23436*z*z - 16128*z*z*z - 24192*x*y*z;
   values[157] = 6048*x*z*z - 4536*x*z + 8064*x*x*z + 4032*x*y*z;
   values[158] = 2520*x - 4536*x*y - 13608*x*z + 2016*x*y*y + 8064*x*x*y + 12096*x*z*z + 24192*x*x*z - 8568*x*x + 6048*x*x*x + 12096*x*y*z;
   values[159] = 16632*x*z - 3528*z + 7560*y*z - 18144*x*z*z - 12096*x*x*z - 8064*y*z*z - 4032*y*y*z + 7560*z*z - 4032*z*z*z - 18144*x*y*z;
   values[160] = 2016*x*z*z - 2520*x*z + 6048*x*x*z + 2016*x*y*z;
   values[161] = 1512*x - 2520*x*y - 5040*x*z + 1008*x*y*y + 6048*x*x*y + 3024*x*z*z + 12096*x*x*z - 6804*x*x + 6048*x*x*x + 4032*x*y*z;
   values[162] = 540*z - 3024*x*z - 1512*y*z + 4032*x*z*z + 3024*x*x*z + 2016*y*z*z + 1008*y*y*z - 1512*z*z + 1008*z*z*z + 4032*x*y*z;
   values[163] = 2016*x*z*z - 1512*x*z + 2016*x*x*z + 2016*x*y*z;
   values[164] = 6048*x*y - 2160*x + 4536*x*z - 4032*x*y*y - 8064*x*x*y - 2016*x*z*z - 6048*x*x*z + 6048*x*x - 4032*x*x*x - 6048*x*y*z;
   values[165] = 8064*x*z - 1944*z + 4032*y*z - 16128*x*z*z - 6048*x*x*z - 8064*y*z*z - 2016*y*y*z + 7560*z*z - 6048*z*z*z - 8064*x*y*z;
   values[166] = 4032*x*z - 8064*x*z*z - 4032*x*x*z - 4032*x*y*z;
   values[167] = 5616*x - 13608*x*y - 20160*x*z + 8064*x*y*y + 16128*x*x*y + 12096*x*z*z + 24192*x*x*z - 13608*x*x + 8064*x*x*x + 24192*x*y*z;
   values[168] = 1512*z - 5040*x*z - 2520*y*z + 12096*x*z*z + 3024*x*x*z + 6048*y*z*z + 1008*y*y*z - 6804*z*z + 6048*z*z*z + 4032*x*y*z;
   values[169] = 6048*x*z*z - 2520*x*z + 2016*x*x*z + 2016*x*y*z;
   values[170] = 7560*x*y - 3528*x + 16632*x*z - 4032*x*y*y - 8064*x*x*y - 12096*x*z*z - 18144*x*x*z + 7560*x*x - 4032*x*x*x - 18144*x*y*z;
   values[171] = 10584*x*z - 1512*z + 3528*y*z - 12096*x*z*z - 12096*x*x*z - 4032*y*z*z - 2016*y*y*z + 3528*z*z - 2016*z*z*z - 12096*x*y*z;
   values[172] = 3528*x*z - 4032*x*z*z - 6048*x*x*z - 4032*x*y*z;
   values[173] = 6048*x - 14112*x*y - 10584*x*z + 8064*x*y*y + 24192*x*x*y + 4032*x*z*z + 18144*x*x*z - 21168*x*x + 16128*x*x*x + 12096*x*y*z;
   values[174] = 2520*z - 13608*x*z - 4536*y*z + 24192*x*z*z + 12096*x*x*z + 8064*y*z*z + 2016*y*y*z - 8568*z*z + 6048*z*z*z + 12096*x*y*z;
   values[175] = 8064*x*z*z - 4536*x*z + 6048*x*x*z + 4032*x*y*z;
   values[176] = 15624*x*y - 7560*x + 23184*x*z - 8064*x*y*y - 24192*x*x*y - 12096*x*z*z - 36288*x*x*z + 23436*x*x - 16128*x*x*x - 24192*x*y*z;
   values[177] = 1008*z - 8064*x*z - 2016*y*z + 8064*x*z*z + 10080*x*x*z + 2016*y*z*z + 1008*y*y*z - 2016*z*z + 1008*z*z*z + 8064*x*y*z;
   values[178] = 2016*x*z*z - 2016*x*z + 4032*x*x*z + 2016*x*y*z;
   values[179] = 8064*x*y - 4032*x + 6048*x*z - 4032*x*y*y - 16128*x*x*y - 2016*x*z*z - 12096*x*x*z + 16128*x*x - 13440*x*x*x - 6048*x*y*z;
   values[180] = 9072*x*y - 4320*y - 2160*z - 1620*x + 4536*x*z + 12096*y*z - 9072*x*y*y - 4032*x*x*y - 3024*x*z*z - 2016*x*x*z - 8064*y*z*z - 12096*y*y*z + 1512*x*x - 336*x*x*x + 9072*y*y - 5376*y*y*y + 3024*z*z - 1344*z*z*z - 12096*x*y*z + 480;
   values[181] = 1080*x - 4536*x*y - 3024*x*z + 4032*x*y*y + 6048*x*x*y + 2016*x*z*z + 4032*x*x*z - 3024*x*x + 2016*x*x*x + 6048*x*y*z;
   values[182] = 540*x - 3024*x*y - 1512*x*z + 3024*x*y*y + 4032*x*x*y + 1008*x*z*z + 2016*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[183] = 4536*x + 18144*y + 6048*z - 31752*x*y - 10584*x*z - 42336*y*z + 36288*x*y*y + 12096*x*x*y + 6048*x*z*z + 4032*x*x*z + 24192*y*z*z + 48384*y*y*z - 3528*x*x + 672*x*x*x - 42336*y*y + 26880*y*y*y - 7056*z*z + 2688*z*z*z + 36288*x*y*z - 1680;
   values[184] = 21168*x*y - 4536*x + 10584*x*z - 20160*x*y*y - 24192*x*x*y - 6048*x*z*z - 12096*x*x*z + 10584*x*x - 6048*x*x*x - 24192*x*y*z;
   values[185] = 10584*x*y - 1512*x + 3528*x*z - 12096*x*y*y - 12096*x*x*y - 2016*x*z*z - 4032*x*x*z + 3528*x*x - 2016*x*x*x - 12096*x*y*z;
   values[186] = 24192*x*y - 16128*y - 4032*z - 3024*x + 6048*x*z + 32256*y*z - 30240*x*y*y - 8064*x*x*y - 3024*x*z*z - 2016*x*x*z - 16128*y*z*z - 40320*y*y*z + 2016*x*x - 336*x*x*x + 40320*y*y - 26880*y*y*y + 4032*z*z - 1344*z*z*z - 24192*x*y*z + 1344;
   values[187] = 4032*x - 20160*x*y - 8064*x*z + 20160*x*y*y + 20160*x*x*y + 4032*x*z*z + 8064*x*x*z - 8064*x*x + 4032*x*x*x + 20160*x*y*z;
   values[188] = 1008*x - 8064*x*y - 2016*x*z + 10080*x*y*y + 8064*x*x*y + 1008*x*z*z + 2016*x*x*z - 2016*x*x + 1008*x*x*x + 8064*x*y*z;
   values[189] = 8208*x + 11232*y + 5616*z - 40320*x*y - 20160*x*z - 27216*y*z + 36288*x*y*y + 24192*x*x*y + 12096*x*z*z + 12096*x*x*z + 16128*y*z*z + 24192*y*y*z - 9828*x*x + 2688*x*x*x - 20412*y*y + 10752*y*y*y - 6804*z*z + 2688*z*z*z + 48384*x*y*z - 1500;
   values[190] = 12096*x*y - 3888*x + 8064*x*z - 8064*x*y*y - 24192*x*x*y - 4032*x*z*z - 16128*x*x*z + 15120*x*x - 12096*x*x*x - 12096*x*y*z;
   values[191] = 8064*x*y - 1944*x + 4032*x*z - 6048*x*y*y - 16128*x*x*y - 2016*x*z*z - 8064*x*x*z + 7560*x*x - 6048*x*x*x - 8064*x*y*z;
   values[192] = 69552*x*y - 22680*y - 7560*z - 11088*x + 23184*x*z + 46872*y*z - 72576*x*y*y - 36288*x*x*y - 12096*x*z*z - 12096*x*x*z - 24192*y*z*z - 48384*y*y*z + 11340*x*x - 2688*x*x*x + 46872*y*y - 26880*y*y*y + 7812*z*z - 2688*z*z*z - 72576*x*y*z + 2436;
   values[193] = 7560*x - 27216*x*y - 13608*x*z + 20160*x*y*y + 48384*x*x*y + 6048*x*z*z + 24192*x*x*z - 25704*x*x + 18144*x*x*x + 24192*x*y*z;
   values[194] = 2520*x - 13608*x*y - 4536*x*z + 12096*x*y*y + 24192*x*x*y + 2016*x*z*z + 8064*x*x*z - 8568*x*x + 6048*x*x*x + 12096*x*y*z;
   values[195] = 33264*x*y - 7056*y - 3528*z - 7560*x + 16632*x*z + 15120*y*z - 27216*x*y*y - 24192*x*x*y - 9072*x*z*z - 12096*x*x*z - 8064*y*z*z - 12096*y*y*z + 10584*x*x - 3360*x*x*x + 11340*y*y - 5376*y*y*y + 3780*z*z - 1344*z*z*z - 36288*x*y*z + 1092;
   values[196] = 3024*x - 7560*x*y - 5040*x*z + 4032*x*y*y + 18144*x*x*y + 2016*x*z*z + 12096*x*x*z - 13608*x*x + 12096*x*x*x + 6048*x*y*z;
   values[197] = 1512*x - 5040*x*y - 2520*x*z + 3024*x*y*y + 12096*x*x*y + 1008*x*z*z + 6048*x*x*z - 6804*x*x + 6048*x*x*x + 4032*x*y*z;
   values[198] = 1080*x + 1080*y + 540*z - 6048*x*y - 3024*x*z - 3024*y*z + 6048*x*y*y + 6048*x*x*y + 2016*x*z*z + 3024*x*x*z + 2016*y*z*z + 3024*y*y*z - 2268*x*x + 1344*x*x*x - 2268*y*y + 1344*y*y*y - 756*z*z + 336*z*z*z + 8064*x*y*z - 120;
   values[199] = 3024*x*y - 1620*x + 4536*x*z - 1008*x*y*y - 4032*x*x*y - 3024*x*z*z - 6048*x*x*z + 4536*x*x - 3024*x*x*x - 4032*x*y*z;
   values[200] = 540*x - 3024*x*y - 1512*x*z + 3024*x*y*y + 4032*x*x*y + 1008*x*z*z + 2016*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[201] = 30240*x*y - 6912*y - 1944*z - 3888*x + 8064*x*z + 15120*y*z - 36288*x*y*y - 24192*x*x*y - 4032*x*z*z - 6048*x*x*z - 8064*y*z*z - 18144*y*y*z + 6048*x*x - 2688*x*x*x + 16632*y*y - 10752*y*y*y + 2016*z*z - 672*z*z*z - 32256*x*y*z + 600;
   values[202] = 8208*x - 19656*x*y - 20160*x*z + 8064*x*y*y + 24192*x*x*y + 12096*x*z*z + 24192*x*x*z - 20160*x*x + 12096*x*x*x + 24192*x*y*z;
   values[203] = 15120*x*y - 1944*x + 4032*x*z - 18144*x*y*y - 16128*x*x*y - 2016*x*z*z - 4032*x*x*z + 4032*x*x - 2016*x*x*x - 16128*x*y*z;
   values[204] = 3024*x + 7560*y + 1512*z - 27216*x*y - 5040*x*z - 13608*y*z + 36288*x*y*y + 18144*x*x*y + 2016*x*z*z + 3024*x*x*z + 6048*y*z*z + 18144*y*y*z - 3780*x*x + 1344*x*x*x - 19656*y*y + 13440*y*y*y - 1260*z*z + 336*z*z*z + 24192*x*y*z - 588;
   values[205] = 21168*x*y - 7560*x + 16632*x*z - 10080*x*y*y - 24192*x*x*y - 9072*x*z*z - 18144*x*x*z + 16632*x*x - 9072*x*x*x - 24192*x*y*z;
   values[206] = 1512*x - 13608*x*y - 2520*x*z + 18144*x*y*y + 12096*x*x*y + 1008*x*z*z + 2016*x*x*z - 2520*x*x + 1008*x*x*x + 12096*x*y*z;
   values[207] = 21168*x*y - 3024*y - 1512*z - 4536*x + 10584*x*z + 7056*y*z - 18144*x*y*y - 24192*x*x*y - 6048*x*z*z - 12096*x*x*z - 4032*y*z*z - 6048*y*y*z + 10584*x*x - 6720*x*x*x + 5292*y*y - 2688*y*y*y + 1764*z*z - 672*z*z*z - 24192*x*y*z + 420;
   values[208] = 4536*x - 7056*x*y - 10584*x*z + 2016*x*y*y + 12096*x*x*y + 6048*x*z*z + 18144*x*x*z - 15876*x*x + 12096*x*x*x + 8064*x*y*z;
   values[209] = 7056*x*y - 1512*x + 3528*x*z - 6048*x*y*y - 12096*x*x*y - 2016*x*z*z - 6048*x*x*z + 5292*x*x - 4032*x*x*x - 8064*x*y*z;
   values[210] = 7560*x + 9072*y + 2520*z - 51408*x*y - 13608*x*z - 17136*y*z + 54432*x*y*y + 48384*x*x*y + 6048*x*z*z + 12096*x*x*z + 8064*y*z*z + 18144*y*y*z - 13608*x*x + 6720*x*x*x - 18900*y*y + 10752*y*y*y - 2268*z*z + 672*z*z*z + 48384*x*y*z - 924;
   values[211] = 22680*x*y - 11088*x + 23184*x*z - 8064*x*y*y - 36288*x*x*y - 12096*x*z*z - 36288*x*x*z + 34776*x*x - 24192*x*x*x - 24192*x*y*z;
   values[212] = 2520*x - 17136*x*y - 4536*x*z + 18144*x*y*y + 24192*x*x*y + 2016*x*z*z + 6048*x*x*z - 6804*x*x + 4032*x*x*x + 16128*x*y*z;
   values[213] = 4032*x + 2016*y + 1008*z - 16128*x*y - 8064*x*z - 4032*y*z + 12096*x*y*y + 20160*x*x*y + 4032*x*z*z + 10080*x*x*z + 2016*y*z*z + 3024*y*y*z - 10080*x*x + 6720*x*x*x - 3024*y*y + 1344*y*y*y - 1008*z*z + 336*z*z*z + 16128*x*y*z - 336;
   values[214] = 4032*x*y - 3024*x + 6048*x*z - 1008*x*y*y - 8064*x*x*y - 3024*x*z*z - 12096*x*x*z + 12096*x*x - 10080*x*x*x - 4032*x*y*z;
   values[215] = 1008*x - 4032*x*y - 2016*x*z + 3024*x*y*y + 8064*x*x*y + 1008*x*z*z + 4032*x*x*z - 4032*x*x + 3360*x*x*x + 4032*x*y*z;
   values[216] = 12096*z - 21168*x*z - 56448*y*z + 24192*x*z*z + 8064*x*x*z + 64512*y*z*z + 48384*y*y*z - 28224*z*z + 16128*z*z*z + 48384*x*y*z;
   values[217] = 14112*x*z - 16128*x*z*z - 16128*x*x*z - 24192*x*y*z;
   values[218] = 14112*x*y - 3024*x + 14112*x*z - 12096*x*y*y - 16128*x*x*y - 12096*x*z*z - 16128*x*x*z + 7056*x*x - 4032*x*x*x - 32256*x*y*z;
   values[219] = 14112*x*z - 3024*z + 14112*y*z - 16128*x*z*z - 12096*x*x*z - 16128*y*z*z - 12096*y*y*z + 7056*z*z - 4032*z*z*z - 32256*x*y*z;
   values[220] = 24192*x*z*z - 21168*x*z + 24192*x*x*z + 16128*x*y*z;
   values[221] = 14112*x*y - 3024*x + 14112*x*z - 12096*x*y*y - 16128*x*x*y - 12096*x*z*z - 16128*x*x*z + 7056*x*x - 4032*x*x*x - 32256*x*y*z;
   values[222] = 14112*x*z - 3024*z + 14112*y*z - 16128*x*z*z - 12096*x*x*z - 16128*y*z*z - 12096*y*y*z + 7056*z*z - 4032*z*z*z - 32256*x*y*z;
   values[223] = 14112*x*z - 16128*x*z*z - 16128*x*x*z - 24192*x*y*z;
   values[224] = 12096*x - 56448*x*y - 21168*x*z + 48384*x*y*y + 64512*x*x*y + 8064*x*z*z + 24192*x*x*z - 28224*x*x + 16128*x*x*x + 48384*x*y*z;
   values[225] = 24192*x*z - 16128*z + 64512*y*z - 36288*x*z*z - 8064*x*x*z - 96768*y*z*z - 48384*y*y*z + 48384*z*z - 32256*z*z*z - 48384*x*y*z;
   values[226] = 24192*x*z*z - 16128*x*z + 16128*x*x*z + 24192*x*y*z;
   values[227] = 4032*x - 16128*x*y - 24192*x*z + 12096*x*y*y + 16128*x*x*y + 24192*x*z*z + 24192*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[228] = 4032*z - 16128*x*z - 16128*y*z + 24192*x*z*z + 12096*x*x*z + 24192*y*z*z + 12096*y*y*z - 12096*z*z + 8064*z*z*z + 32256*x*y*z;
   values[229] = 24192*x*z - 36288*x*z*z - 24192*x*x*z - 16128*x*y*z;
   values[230] = 4032*x - 16128*x*y - 24192*x*z + 12096*x*y*y + 16128*x*x*y + 24192*x*z*z + 24192*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[231] = 5040*z - 18144*x*z - 18144*y*z + 32256*x*z*z + 12096*x*x*z + 32256*y*z*z + 12096*y*y*z - 17136*z*z + 12096*z*z*z + 32256*x*y*z;
   values[232] = 32256*x*z*z - 18144*x*z + 16128*x*x*z + 24192*x*y*z;
   values[233] = 62496*x*y - 15120*x + 46368*x*z - 48384*x*y*y - 64512*x*x*y - 24192*x*z*z - 48384*x*x*z + 31248*x*x - 16128*x*x*x - 96768*x*y*z;
   values[234] = 24192*x*z - 16128*z + 96768*y*z - 24192*x*z*z - 8064*x*x*z - 96768*y*z*z - 96768*y*y*z + 32256*z*z - 16128*z*z*z - 72576*x*y*z;
   values[235] = 24192*x*z*z - 24192*x*z + 24192*x*x*z + 48384*x*y*z;
   values[236] = 4032*x - 24192*x*y - 16128*x*z + 24192*x*y*y + 24192*x*x*y + 12096*x*z*z + 16128*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[237] = 5040*z - 18144*x*z - 34272*y*z + 16128*x*z*z + 12096*x*x*z + 32256*y*z*z + 36288*y*y*z - 9072*z*z + 4032*z*z*z + 64512*x*y*z;
   values[238] = 46368*x*z - 48384*x*z*z - 48384*x*x*z - 48384*x*y*z;
   values[239] = 5040*x - 34272*x*y - 18144*x*z + 36288*x*y*y + 32256*x*x*y + 12096*x*z*z + 16128*x*x*z - 9072*x*x + 4032*x*x*x + 64512*x*y*z;
   values[240] = 4032*z - 16128*x*z - 24192*y*z + 16128*x*z*z + 12096*x*x*z + 24192*y*z*z + 24192*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[241] = 24192*x*z*z - 24192*x*z + 24192*x*x*z + 48384*x*y*z;
   values[242] = 96768*x*y - 16128*x + 24192*x*z - 96768*x*y*y - 96768*x*x*y - 8064*x*z*z - 24192*x*x*z + 32256*x*x - 16128*x*x*x - 72576*x*y*z;
   values[243] = 46368*x*z - 15120*z + 62496*y*z - 48384*x*z*z - 24192*x*x*z - 64512*y*z*z - 48384*y*y*z + 31248*z*z - 16128*z*z*z - 96768*x*y*z;
   values[244] = 16128*x*z*z - 18144*x*z + 32256*x*x*z + 24192*x*y*z;
   values[245] = 5040*x - 18144*x*y - 18144*x*z + 12096*x*y*y + 32256*x*x*y + 12096*x*z*z + 32256*x*x*z - 17136*x*x + 12096*x*x*x + 32256*x*y*z;
   values[246] = 4032*z - 24192*x*z - 16128*y*z + 24192*x*z*z + 24192*x*x*z + 16128*y*z*z + 12096*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[247] = 24192*x*z - 24192*x*z*z - 36288*x*x*z - 16128*x*y*z;
   values[248] = 4032*x - 16128*x*y - 16128*x*z + 12096*x*y*y + 24192*x*x*y + 12096*x*z*z + 24192*x*x*z - 12096*x*x + 8064*x*x*x + 32256*x*y*z;
   values[249] = 4032*z - 24192*x*z - 16128*y*z + 24192*x*z*z + 24192*x*x*z + 16128*y*z*z + 12096*y*y*z - 8064*z*z + 4032*z*z*z + 48384*x*y*z;
   values[250] = 16128*x*z*z - 16128*x*z + 24192*x*x*z + 24192*x*y*z;
   values[251] = 64512*x*y - 16128*x + 24192*x*z - 48384*x*y*y - 96768*x*x*y - 8064*x*z*z - 36288*x*x*z + 48384*x*x - 32256*x*x*x - 48384*x*y*z;
}
static void Curl_T_P4_3D_3D_D001(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 720*x + 960*y + 960*z - 2520*x*y - 2520*x*z - 3360*y*z + 2016*x*y*y + 1344*x*x*y + 2016*x*z*z + 1344*x*x*z + 2688*y*z*z + 2688*y*y*z - 840*x*x + 224*x*x*x - 1680*y*y + 896*y*y*y - 1680*z*z + 896*z*z*z + 4032*x*y*z - 160;
   values[1] = 840*x*y - 240*x + 840*x*z - 672*x*y*y - 1344*x*x*y - 672*x*z*z - 1344*x*x*z + 840*x*x - 672*x*x*x - 1344*x*y*z;
   values[2] = 840*x*y - 240*x + 840*x*z - 672*x*y*y - 1344*x*x*y - 672*x*z*z - 1344*x*x*z + 840*x*x - 672*x*x*x - 1344*x*y*z;
   values[3] = 17136*x*y - 3960*y - 3960*z - 5760*x + 17136*x*z + 11592*y*z - 12096*x*y*y - 12096*x*x*y - 12096*x*z*z - 12096*x*x*z - 8064*y*z*z - 8064*y*y*z + 8316*x*x - 2688*x*x*x + 5796*y*y - 2688*y*y*y + 5796*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[4] = 1440*x - 3528*x*y - 3528*x*z + 2016*x*y*y + 8064*x*x*y + 2016*x*z*z + 8064*x*x*z - 6552*x*x + 6048*x*x*x + 4032*x*y*z;
   values[5] = 1440*x - 3528*x*y - 3528*x*z + 2016*x*y*y + 8064*x*x*y + 2016*x*z*z + 8064*x*x*z - 6552*x*x + 6048*x*x*x + 4032*x*y*z;
   values[6] = 11016*x + 5184*y + 5184*z - 28728*x*y - 28728*x*z - 13104*y*z + 18144*x*y*y + 24192*x*x*y + 18144*x*z*z + 24192*x*x*z + 8064*y*z*z + 8064*y*y*z - 18144*x*x + 6720*x*x*x - 6552*y*y + 2688*y*y*y - 6552*z*z + 2688*z*z*z + 36288*x*y*z - 1320;
   values[7] = 4536*x*y - 2376*x + 4536*x*z - 2016*x*y*y - 12096*x*x*y - 2016*x*z*z - 12096*x*x*z + 12096*x*x - 12096*x*x*x - 4032*x*y*z;
   values[8] = 4536*x*y - 2376*x + 4536*x*z - 2016*x*y*y - 12096*x*x*y - 2016*x*z*z - 12096*x*x*z + 12096*x*x - 12096*x*x*x - 4032*x*y*z;
   values[9] = 14112*x*y - 2184*y - 2184*z - 6048*x + 14112*x*z + 4872*y*z - 8064*x*y*y - 13440*x*x*y - 8064*x*z*z - 13440*x*x*z - 2688*y*z*z - 2688*y*y*z + 10920*x*x - 4480*x*x*x + 2436*y*y - 896*y*y*y + 2436*z*z - 896*z*z*z - 16128*x*y*z + 644;
   values[10] = 1176*x - 1848*x*y - 1848*x*z + 672*x*y*y + 5376*x*x*y + 672*x*z*z + 5376*x*x*z - 6384*x*x + 6720*x*x*x + 1344*x*y*z;
   values[11] = 1176*x - 1848*x*y - 1848*x*z + 672*x*y*y + 5376*x*x*y + 672*x*z*z + 5376*x*x*z - 6384*x*x + 6720*x*x*x + 1344*x*y*z;
   values[12] = 840*x*y - 240*y + 840*y*z - 1344*x*y*y - 672*x*x*y - 672*y*z*z - 1344*y*y*z + 840*y*y - 672*y*y*y - 1344*x*y*z;
   values[13] = 960*x + 720*y + 960*z - 2520*x*y - 3360*x*z - 2520*y*z + 1344*x*y*y + 2016*x*x*y + 2688*x*z*z + 2688*x*x*z + 2016*y*z*z + 1344*y*y*z - 1680*x*x + 896*x*x*x - 840*y*y + 224*y*y*y - 1680*z*z + 896*z*z*z + 4032*x*y*z - 160;
   values[14] = 840*x*y - 240*y + 840*y*z - 1344*x*y*y - 672*x*x*y - 672*y*z*z - 1344*y*y*z + 840*y*y - 672*y*y*y - 1344*x*y*z;
   values[15] = 1440*y - 3528*x*y - 3528*y*z + 8064*x*y*y + 2016*x*x*y + 2016*y*z*z + 8064*y*y*z - 6552*y*y + 6048*y*y*y + 4032*x*y*z;
   values[16] = 17136*x*y - 5760*y - 3960*z - 3960*x + 11592*x*z + 17136*y*z - 12096*x*y*y - 12096*x*x*y - 8064*x*z*z - 8064*x*x*z - 12096*y*z*z - 12096*y*y*z + 5796*x*x - 2688*x*x*x + 8316*y*y - 2688*y*y*y + 5796*z*z - 2688*z*z*z - 24192*x*y*z + 840;
   values[17] = 1440*y - 3528*x*y - 3528*y*z + 8064*x*y*y + 2016*x*x*y + 2016*y*z*z + 8064*y*y*z - 6552*y*y + 6048*y*y*y + 4032*x*y*z;
   values[18] = 4536*x*y - 2376*y + 4536*y*z - 12096*x*y*y - 2016*x*x*y - 2016*y*z*z - 12096*y*y*z + 12096*y*y - 12096*y*y*y - 4032*x*y*z;
   values[19] = 5184*x + 11016*y + 5184*z - 28728*x*y - 13104*x*z - 28728*y*z + 24192*x*y*y + 18144*x*x*y + 8064*x*z*z + 8064*x*x*z + 18144*y*z*z + 24192*y*y*z - 6552*x*x + 2688*x*x*x - 18144*y*y + 6720*y*y*y - 6552*z*z + 2688*z*z*z + 36288*x*y*z - 1320;
   values[20] = 4536*x*y - 2376*y + 4536*y*z - 12096*x*y*y - 2016*x*x*y - 2016*y*z*z - 12096*y*y*z + 12096*y*y - 12096*y*y*y - 4032*x*y*z;
   values[21] = 1176*y - 1848*x*y - 1848*y*z + 5376*x*y*y + 672*x*x*y + 672*y*z*z + 5376*y*y*z - 6384*y*y + 6720*y*y*y + 1344*x*y*z;
   values[22] = 14112*x*y - 6048*y - 2184*z - 2184*x + 4872*x*z + 14112*y*z - 13440*x*y*y - 8064*x*x*y - 2688*x*z*z - 2688*x*x*z - 8064*y*z*z - 13440*y*y*z + 2436*x*x - 896*x*x*x + 10920*y*y - 4480*y*y*y + 2436*z*z - 896*z*z*z - 16128*x*y*z + 644;
   values[23] = 1176*y - 1848*x*y - 1848*y*z + 5376*x*y*y + 672*x*x*y + 672*y*z*z + 5376*y*y*z - 6384*y*y + 6720*y*y*y + 1344*x*y*z;
   values[24] = 840*x*y - 240*y - 480*z - 240*x + 1680*x*z + 1680*y*z - 672*x*y*y - 672*x*x*y - 2016*x*z*z - 1344*x*x*z - 2016*y*z*z - 1344*y*y*z + 420*x*x - 224*x*x*x + 420*y*y - 224*y*y*y + 1260*z*z - 896*z*z*z - 2688*x*y*z + 40;
   values[25] = 840*x*y - 240*y - 480*z - 240*x + 1680*x*z + 1680*y*z - 672*x*y*y - 672*x*x*y - 2016*x*z*z - 1344*x*x*z - 2016*y*z*z - 1344*y*y*z + 420*x*x - 224*x*x*x + 420*y*y - 224*y*y*y + 1260*z*z - 896*z*z*z - 2688*x*y*z + 40;
   values[26] = 720*x + 720*y + 480*z - 2520*x*y - 1680*x*z - 1680*y*z + 2016*x*y*y + 2016*x*x*y + 672*x*z*z + 1344*x*x*z + 672*y*z*z + 1344*y*y*z - 1260*x*x + 672*x*x*x - 1260*y*y + 672*y*y*y - 420*z*z + 2688*x*y*z - 120;
   values[27] = 1440*x + 1440*y + 5040*z - 3528*x*y - 13104*x*z - 13104*y*z + 2016*x*y*y + 2016*x*x*y + 18144*x*z*z + 8064*x*x*z + 18144*y*z*z + 8064*y*y*z - 1764*x*x + 672*x*x*x - 1764*y*y + 672*y*y*y - 14364*z*z + 10752*z*z*z + 16128*x*y*z - 360;
   values[28] = 1440*x + 1440*y + 5040*z - 3528*x*y - 13104*x*z - 13104*y*z + 2016*x*y*y + 2016*x*x*y + 18144*x*z*z + 8064*x*x*z + 18144*y*z*z + 8064*y*y*z - 1764*x*x + 672*x*x*x - 1764*y*y + 672*y*y*y - 14364*z*z + 10752*z*z*z + 16128*x*y*z - 360;
   values[29] = 17136*x*y - 5760*y - 5400*z - 5760*x + 16632*x*z + 16632*y*z - 12096*x*y*y - 12096*x*x*y - 8064*x*z*z - 12096*x*x*z - 8064*y*z*z - 12096*y*y*z + 8568*x*x - 4032*x*x*x + 8568*y*y - 4032*y*y*y + 5040*z*z - 24192*x*y*z + 1200;
   values[30] = 4536*x*y - 2376*y - 11664*z - 2376*x + 24192*x*z + 24192*y*z - 2016*x*y*y - 2016*x*x*y - 36288*x*z*z - 12096*x*x*z - 36288*y*z*z - 12096*y*y*z + 2268*x*x - 672*x*x*x + 2268*y*y - 672*y*y*y + 34776*z*z - 26880*z*z*z - 24192*x*y*z + 780;
   values[31] = 4536*x*y - 2376*y - 11664*z - 2376*x + 24192*x*z + 24192*y*z - 2016*x*y*y - 2016*x*x*y - 36288*x*z*z - 12096*x*x*z - 36288*y*z*z - 12096*y*y*z + 2268*x*x - 672*x*x*x + 2268*y*y - 672*y*y*y + 34776*z*z - 26880*z*z*z - 24192*x*y*z + 780;
   values[32] = 11016*x + 11016*y + 12960*z - 28728*x*y - 36288*x*z - 36288*y*z + 18144*x*y*y + 18144*x*x*y + 20160*x*z*z + 24192*x*x*z + 20160*y*z*z + 24192*y*y*z - 14364*x*x + 6048*x*x*x - 14364*y*y + 6048*y*y*y - 12600*z*z + 48384*x*y*z - 2700;
   values[33] = 1176*x + 1176*y + 7392*z - 1848*x*y - 12768*x*z - 12768*y*z + 672*x*y*y + 672*x*x*y + 20160*x*z*z + 5376*x*x*z + 20160*y*z*z + 5376*y*y*z - 924*x*x + 224*x*x*x - 924*y*y + 224*y*y*y - 22680*z*z + 17920*z*z*z + 10752*x*y*z - 476;
   values[34] = 1176*x + 1176*y + 7392*z - 1848*x*y - 12768*x*z - 12768*y*z + 672*x*y*y + 672*x*x*y + 20160*x*z*z + 5376*x*x*z + 20160*y*z*z + 5376*y*y*z - 924*x*x + 224*x*x*x - 924*y*y + 224*y*y*y - 22680*z*z + 17920*z*z*z + 10752*x*y*z - 476;
   values[35] = 14112*x*y - 6048*y - 8400*z - 6048*x + 21840*x*z + 21840*y*z - 8064*x*y*y - 8064*x*x*y - 13440*x*z*z - 13440*x*x*z - 13440*y*z*z - 13440*y*y*z + 7056*x*x - 2688*x*x*x + 7056*y*y - 2688*y*y*y + 8400*z*z - 26880*x*y*z + 1680;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 252*x*x - 72*x - 224*x*x*x + 4;
   values[49] = 0;
   values[50] = 0;
   values[51] = 2016*x*z - 144*z - 72*x - 4032*x*x*z - 252*x*x + 672*x*x*x + 12;
   values[52] = 0;
   values[53] = 216*x - 1512*x*x + 2016*x*x*x;
   values[54] = 144*x - 144*z - 1008*x*z - 6048*x*z*z + 8064*x*x*z - 252*x*x - 672*x*x*x + 756*z*z;
   values[55] = 0;
   values[56] = 4032*x*x*z - 1008*x*z - 144*x + 2016*x*x - 4032*x*x*x;
   values[57] = 6048*x*z*z - 1008*x*z - 4032*x*x*z + 252*x*x + 224*x*x*x + 252*z*z - 896*z*z*z;
   values[58] = 0;
   values[59] = 504*x*z + 672*x*z*z - 4032*x*x*z - 504*x*x + 2016*x*x*x;
   values[60] = 0;
   values[61] = 252*y*y - 72*y - 224*y*y*y + 4;
   values[62] = 0;
   values[63] = 0;
   values[64] = 2016*y*z - 144*z - 72*y - 4032*y*y*z - 252*y*y + 672*y*y*y + 12;
   values[65] = 216*y - 1512*y*y + 2016*y*y*y;
   values[66] = 0;
   values[67] = 144*y - 144*z - 1008*y*z - 6048*y*z*z + 8064*y*y*z - 252*y*y - 672*y*y*y + 756*z*z;
   values[68] = 4032*y*y*z - 1008*y*z - 144*y + 2016*y*y - 4032*y*y*y;
   values[69] = 0;
   values[70] = 6048*y*z*z - 1008*y*z - 4032*y*y*z + 252*y*y + 224*y*y*y + 252*z*z - 896*z*z*z;
   values[71] = 504*y*z + 672*y*z*z - 4032*y*y*z - 504*y*y + 2016*y*y*y;
   values[72] = 504*x*y - 36*y - 1008*x*x*y;
   values[73] = 144*x - 1008*x*x + 1344*x*x*x;
   values[74] = 0;
   values[75] = 504*y*z - 504*x*y + 2016*x*x*y - 4032*x*y*z;
   values[76] = 8064*x*x*z - 2016*x*z + 1008*x*x - 2688*x*x*x;
   values[77] = 504*x*y - 2016*x*x*y;
   values[78] = 4032*x*y*z - 1008*y*z*z - 1008*x*x*y;
   values[79] = 4032*x*z*z - 8064*x*x*z + 1344*x*x*x;
   values[80] = 2016*x*x*y - 2016*x*y*z;
   values[81] = 2016*x*x*y - 4032*x*y*y - 72*y + 504*y*y;
   values[82] = 6048*x*x*y - 1512*x*y - 72*x + 1260*x*x - 2688*x*x*x;
   values[83] = 0;
   values[84] = 504*y*z - 504*x*y + 4032*x*y*y - 2016*x*x*y - 4032*y*y*z + 4032*x*y*z;
   values[85] = 504*x*z - 6048*x*x*y - 8064*x*x*z - 252*x*x + 2688*x*x*x + 12096*x*y*z;
   values[86] = 504*x*y - 4032*x*y*y + 2016*x*x*y;
   values[87] = 4032*x*y*y - 504*x*y - 1008*x*x*y + 252*y*y - 1008*y*y*y;
   values[88] = 504*x*y + 2016*x*y*y - 6048*x*x*y - 252*x*x + 1344*x*x*x;
   values[89] = 0;
   values[90] = 504*x*y - 36*y - 1008*x*x*y;
   values[91] = 252*x*x - 36*x - 336*x*x*x;
   values[92] = 0;
   values[93] = 1008*y*z - 72*y + 2016*x*x*y - 8064*x*y*z;
   values[94] = 1008*x*z - 72*x - 4032*x*x*z + 672*x*x*x;
   values[95] = 6048*x*x*y - 1512*x*y;
   values[96] = 504*y*z - 504*x*y - 1008*x*x*y - 3024*y*z*z + 8064*x*y*z;
   values[97] = 504*x*z - 3024*x*z*z + 4032*x*x*z - 252*x*x - 336*x*x*x;
   values[98] = 504*x*y - 6048*x*x*y + 4032*x*y*z;
   values[99] = 2016*x*x*y - 2016*x*y*y - 504*x*y + 252*y*y;
   values[100] = 504*x*y - 2016*x*x*y - 252*x*x + 672*x*x*x;
   values[101] = 0;
   values[102] = 2016*x*y*y - 504*x*y - 2016*x*x*y - 4032*y*y*z + 252*y*y + 8064*x*y*z;
   values[103] = 504*x*y + 2016*x*x*y + 4032*x*x*z - 252*x*x - 672*x*x*x - 8064*x*y*z;
   values[104] = 6048*x*y*y - 6048*x*x*y;
   values[105] = 2016*x*y*y - 1008*x*x*y - 336*y*y*y;
   values[106] = 2016*x*x*y - 1008*x*y*y - 336*x*x*x;
   values[107] = 0;
   values[108] = 540*y - 1512*x*y - 3024*y*z + 2016*x*y*y + 1008*x*x*y + 3024*y*z*z + 4032*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[109] = 4536*x*y - 1620*y - 4320*z - 2160*x + 12096*x*z + 9072*y*z - 2016*x*y*y - 3024*x*x*y - 12096*x*z*z - 8064*x*x*z - 9072*y*z*z - 4032*y*y*z + 3024*x*x - 1344*x*x*x + 1512*y*y - 336*y*y*y + 9072*z*z - 5376*z*z*z - 12096*x*y*z + 480;
   values[110] = 1080*y - 3024*x*y - 4536*y*z + 4032*x*y*y + 2016*x*x*y + 4032*y*z*z + 6048*y*y*z - 3024*y*y + 2016*y*y*y + 6048*x*y*z;
   values[111] = 3528*x*y - 1512*y + 10584*y*z - 4032*x*y*y - 2016*x*x*y - 12096*y*z*z - 12096*y*y*z + 3528*y*y - 2016*y*y*y - 12096*x*y*z;
   values[112] = 6048*x + 4536*y + 18144*z - 10584*x*y - 42336*x*z - 31752*y*z + 4032*x*y*y + 6048*x*x*y + 48384*x*z*z + 24192*x*x*z + 36288*y*z*z + 12096*y*y*z - 7056*x*x + 2688*x*x*x - 3528*y*y + 672*y*y*y - 42336*z*z + 26880*z*z*z + 36288*x*y*z - 1680;
   values[113] = 10584*x*y - 4536*y + 21168*y*z - 12096*x*y*y - 6048*x*x*y - 20160*y*z*z - 24192*y*y*z + 10584*y*y - 6048*y*y*y - 24192*x*y*z;
   values[114] = 1008*y - 2016*x*y - 8064*y*z + 2016*x*y*y + 1008*x*x*y + 10080*y*z*z + 8064*y*y*z - 2016*y*y + 1008*y*y*y + 8064*x*y*z;
   values[115] = 6048*x*y - 3024*y - 16128*z - 4032*x + 32256*x*z + 24192*y*z - 2016*x*y*y - 3024*x*x*y - 40320*x*z*z - 16128*x*x*z - 30240*y*z*z - 8064*y*y*z + 4032*x*x - 1344*x*x*x + 2016*y*y - 336*y*y*y + 40320*z*z - 26880*z*z*z - 24192*x*y*z + 1344;
   values[116] = 4032*y - 8064*x*y - 20160*y*z + 8064*x*y*y + 4032*x*x*y + 20160*y*z*z + 20160*y*y*z - 8064*y*y + 4032*y*y*y + 20160*x*y*z;
   values[117] = 4032*x*y - 1944*y + 8064*y*z - 8064*x*y*y - 2016*x*x*y - 6048*y*z*z - 16128*y*y*z + 7560*y*y - 6048*y*y*y - 8064*x*y*z;
   values[118] = 5616*x + 8208*y + 11232*z - 20160*x*y - 27216*x*z - 40320*y*z + 12096*x*y*y + 12096*x*x*y + 24192*x*z*z + 16128*x*x*z + 36288*y*z*z + 24192*y*y*z - 6804*x*x + 2688*x*x*x - 9828*y*y + 2688*y*y*y - 20412*z*z + 10752*z*z*z + 48384*x*y*z - 1500;
   values[119] = 8064*x*y - 3888*y + 12096*y*z - 16128*x*y*y - 4032*x*x*y - 8064*y*z*z - 24192*y*y*z + 15120*y*y - 12096*y*y*y - 12096*x*y*z;
   values[120] = 2520*y - 4536*x*y - 13608*y*z + 8064*x*y*y + 2016*x*x*y + 12096*y*z*z + 24192*y*y*z - 8568*y*y + 6048*y*y*y + 12096*x*y*z;
   values[121] = 23184*x*y - 11088*y - 22680*z - 7560*x + 46872*x*z + 69552*y*z - 12096*x*y*y - 12096*x*x*y - 48384*x*z*z - 24192*x*x*z - 72576*y*z*z - 36288*y*y*z + 7812*x*x - 2688*x*x*x + 11340*y*y - 2688*y*y*y + 46872*z*z - 26880*z*z*z - 72576*x*y*z + 2436;
   values[122] = 7560*y - 13608*x*y - 27216*y*z + 24192*x*y*y + 6048*x*x*y + 20160*y*z*z + 48384*y*y*z - 25704*y*y + 18144*y*y*y + 24192*x*y*z;
   values[123] = 1512*y - 2520*x*y - 5040*y*z + 6048*x*y*y + 1008*x*x*y + 3024*y*z*z + 12096*y*y*z - 6804*y*y + 6048*y*y*y + 4032*x*y*z;
   values[124] = 16632*x*y - 7560*y - 7056*z - 3528*x + 15120*x*z + 33264*y*z - 12096*x*y*y - 9072*x*x*y - 12096*x*z*z - 8064*x*x*z - 27216*y*z*z - 24192*y*y*z + 3780*x*x - 1344*x*x*x + 10584*y*y - 3360*y*y*y + 11340*z*z - 5376*z*z*z - 36288*x*y*z + 1092;
   values[125] = 3024*y - 5040*x*y - 7560*y*z + 12096*x*y*y + 2016*x*x*y + 4032*y*z*z + 18144*y*y*z - 13608*y*y + 12096*y*y*y + 6048*x*y*z;
   values[126] = 540*y - 1512*x*y - 3024*y*z + 2016*x*y*y + 1008*x*x*y + 3024*y*z*z + 4032*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[127] = 540*x + 1080*y + 1080*z - 3024*x*y - 3024*x*z - 6048*y*z + 3024*x*y*y + 2016*x*x*y + 3024*x*z*z + 2016*x*x*z + 6048*y*z*z + 6048*y*y*z - 756*x*x + 336*x*x*x - 2268*y*y + 1344*y*y*y - 2268*z*z + 1344*z*z*z + 8064*x*y*z - 120;
   values[128] = 4536*x*y - 1620*y + 3024*y*z - 6048*x*y*y - 3024*x*x*y - 1008*y*z*z - 4032*y*y*z + 4536*y*y - 3024*y*y*y - 4032*x*y*z;
   values[129] = 4032*x*y - 1944*y + 15120*y*z - 4032*x*y*y - 2016*x*x*y - 18144*y*z*z - 16128*y*y*z + 4032*y*y - 2016*y*y*y - 16128*x*y*z;
   values[130] = 8064*x*y - 3888*y - 6912*z - 1944*x + 15120*x*z + 30240*y*z - 6048*x*y*y - 4032*x*x*y - 18144*x*z*z - 8064*x*x*z - 36288*y*z*z - 24192*y*y*z + 2016*x*x - 672*x*x*x + 6048*y*y - 2688*y*y*y + 16632*z*z - 10752*z*z*z - 32256*x*y*z + 600;
   values[131] = 8208*y - 20160*x*y - 19656*y*z + 24192*x*y*y + 12096*x*x*y + 8064*y*z*z + 24192*y*y*z - 20160*y*y + 12096*y*y*y + 24192*x*y*z;
   values[132] = 1512*y - 2520*x*y - 13608*y*z + 2016*x*y*y + 1008*x*x*y + 18144*y*z*z + 12096*y*y*z - 2520*y*y + 1008*y*y*y + 12096*x*y*z;
   values[133] = 1512*x + 3024*y + 7560*z - 5040*x*y - 13608*x*z - 27216*y*z + 3024*x*y*y + 2016*x*x*y + 18144*x*z*z + 6048*x*x*z + 36288*y*z*z + 18144*y*y*z - 1260*x*x + 336*x*x*x - 3780*y*y + 1344*y*y*y - 19656*z*z + 13440*z*z*z + 24192*x*y*z - 588;
   values[134] = 16632*x*y - 7560*y + 21168*y*z - 18144*x*y*y - 9072*x*x*y - 10080*y*z*z - 24192*y*y*z + 16632*y*y - 9072*y*y*y - 24192*x*y*z;
   values[135] = 3528*x*y - 1512*y + 7056*y*z - 6048*x*y*y - 2016*x*x*y - 6048*y*z*z - 12096*y*y*z + 5292*y*y - 4032*y*y*y - 8064*x*y*z;
   values[136] = 10584*x*y - 4536*y - 3024*z - 1512*x + 7056*x*z + 21168*y*z - 12096*x*y*y - 6048*x*x*y - 6048*x*z*z - 4032*x*x*z - 18144*y*z*z - 24192*y*y*z + 1764*x*x - 672*x*x*x + 10584*y*y - 6720*y*y*y + 5292*z*z - 2688*z*z*z - 24192*x*y*z + 420;
   values[137] = 4536*y - 10584*x*y - 7056*y*z + 18144*x*y*y + 6048*x*x*y + 2016*y*z*z + 12096*y*y*z - 15876*y*y + 12096*y*y*y + 8064*x*y*z;
   values[138] = 2520*y - 4536*x*y - 17136*y*z + 6048*x*y*y + 2016*x*x*y + 18144*y*z*z + 24192*y*y*z - 6804*y*y + 4032*y*y*y + 16128*x*y*z;
   values[139] = 2520*x + 7560*y + 9072*z - 13608*x*y - 17136*x*z - 51408*y*z + 12096*x*y*y + 6048*x*x*y + 18144*x*z*z + 8064*x*x*z + 54432*y*z*z + 48384*y*y*z - 2268*x*x + 672*x*x*x - 13608*y*y + 6720*y*y*y - 18900*z*z + 10752*z*z*z + 48384*x*y*z - 924;
   values[140] = 23184*x*y - 11088*y + 22680*y*z - 36288*x*y*y - 12096*x*x*y - 8064*y*z*z - 36288*y*y*z + 34776*y*y - 24192*y*y*y - 24192*x*y*z;
   values[141] = 1008*y - 2016*x*y - 4032*y*z + 4032*x*y*y + 1008*x*x*y + 3024*y*z*z + 8064*y*y*z - 4032*y*y + 3360*y*y*y + 4032*x*y*z;
   values[142] = 1008*x + 4032*y + 2016*z - 8064*x*y - 4032*x*z - 16128*y*z + 10080*x*y*y + 4032*x*x*y + 3024*x*z*z + 2016*x*x*z + 12096*y*z*z + 20160*y*y*z - 1008*x*x + 336*x*x*x - 10080*y*y + 6720*y*y*y - 3024*z*z + 1344*z*z*z + 16128*x*y*z - 336;
   values[143] = 6048*x*y - 3024*y + 4032*y*z - 12096*x*y*y - 3024*x*x*y - 1008*y*z*z - 8064*y*y*z + 12096*y*y - 10080*y*y*y - 4032*x*y*z;
   values[144] = 4536*x*y - 2160*y - 4320*z - 1620*x + 9072*x*z + 12096*y*z - 3024*x*y*y - 2016*x*x*y - 9072*x*z*z - 4032*x*x*z - 12096*y*z*z - 8064*y*y*z + 1512*x*x - 336*x*x*x + 3024*y*y - 1344*y*y*y + 9072*z*z - 5376*z*z*z - 12096*x*y*z + 480;
   values[145] = 540*x - 1512*x*y - 3024*x*z + 1008*x*y*y + 2016*x*x*y + 3024*x*z*z + 4032*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[146] = 1080*x - 3024*x*y - 4536*x*z + 2016*x*y*y + 4032*x*x*y + 4032*x*z*z + 6048*x*x*z - 3024*x*x + 2016*x*x*x + 6048*x*y*z;
   values[147] = 4536*x + 6048*y + 18144*z - 10584*x*y - 31752*x*z - 42336*y*z + 6048*x*y*y + 4032*x*x*y + 36288*x*z*z + 12096*x*x*z + 48384*y*z*z + 24192*y*y*z - 3528*x*x + 672*x*x*x - 7056*y*y + 2688*y*y*y - 42336*z*z + 26880*z*z*z + 36288*x*y*z - 1680;
   values[148] = 3528*x*y - 1512*x + 10584*x*z - 2016*x*y*y - 4032*x*x*y - 12096*x*z*z - 12096*x*x*z + 3528*x*x - 2016*x*x*x - 12096*x*y*z;
   values[149] = 10584*x*y - 4536*x + 21168*x*z - 6048*x*y*y - 12096*x*x*y - 20160*x*z*z - 24192*x*x*z + 10584*x*x - 6048*x*x*x - 24192*x*y*z;
   values[150] = 6048*x*y - 4032*y - 16128*z - 3024*x + 24192*x*z + 32256*y*z - 3024*x*y*y - 2016*x*x*y - 30240*x*z*z - 8064*x*x*z - 40320*y*z*z - 16128*y*y*z + 2016*x*x - 336*x*x*x + 4032*y*y - 1344*y*y*y + 40320*z*z - 26880*z*z*z - 24192*x*y*z + 1344;
   values[151] = 1008*x - 2016*x*y - 8064*x*z + 1008*x*y*y + 2016*x*x*y + 10080*x*z*z + 8064*x*x*z - 2016*x*x + 1008*x*x*x + 8064*x*y*z;
   values[152] = 4032*x - 8064*x*y - 20160*x*z + 4032*x*y*y + 8064*x*x*y + 20160*x*z*z + 20160*x*x*z - 8064*x*x + 4032*x*x*x + 20160*x*y*z;
   values[153] = 8208*x + 5616*y + 11232*z - 20160*x*y - 40320*x*z - 27216*y*z + 12096*x*y*y + 12096*x*x*y + 36288*x*z*z + 24192*x*x*z + 24192*y*z*z + 16128*y*y*z - 9828*x*x + 2688*x*x*x - 6804*y*y + 2688*y*y*y - 20412*z*z + 10752*z*z*z + 48384*x*y*z - 1500;
   values[154] = 4032*x*y - 1944*x + 8064*x*z - 2016*x*y*y - 8064*x*x*y - 6048*x*z*z - 16128*x*x*z + 7560*x*x - 6048*x*x*x - 8064*x*y*z;
   values[155] = 8064*x*y - 3888*x + 12096*x*z - 4032*x*y*y - 16128*x*x*y - 8064*x*z*z - 24192*x*x*z + 15120*x*x - 12096*x*x*x - 12096*x*y*z;
   values[156] = 23184*x*y - 7560*y - 22680*z - 11088*x + 69552*x*z + 46872*y*z - 12096*x*y*y - 12096*x*x*y - 72576*x*z*z - 36288*x*x*z - 48384*y*z*z - 24192*y*y*z + 11340*x*x - 2688*x*x*x + 7812*y*y - 2688*y*y*y + 46872*z*z - 26880*z*z*z - 72576*x*y*z + 2436;
   values[157] = 2520*x - 4536*x*y - 13608*x*z + 2016*x*y*y + 8064*x*x*y + 12096*x*z*z + 24192*x*x*z - 8568*x*x + 6048*x*x*x + 12096*x*y*z;
   values[158] = 7560*x - 13608*x*y - 27216*x*z + 6048*x*y*y + 24192*x*x*y + 20160*x*z*z + 48384*x*x*z - 25704*x*x + 18144*x*x*x + 24192*x*y*z;
   values[159] = 16632*x*y - 3528*y - 7056*z - 7560*x + 33264*x*z + 15120*y*z - 9072*x*y*y - 12096*x*x*y - 27216*x*z*z - 24192*x*x*z - 12096*y*z*z - 8064*y*y*z + 10584*x*x - 3360*x*x*x + 3780*y*y - 1344*y*y*y + 11340*z*z - 5376*z*z*z - 36288*x*y*z + 1092;
   values[160] = 1512*x - 2520*x*y - 5040*x*z + 1008*x*y*y + 6048*x*x*y + 3024*x*z*z + 12096*x*x*z - 6804*x*x + 6048*x*x*x + 4032*x*y*z;
   values[161] = 3024*x - 5040*x*y - 7560*x*z + 2016*x*y*y + 12096*x*x*y + 4032*x*z*z + 18144*x*x*z - 13608*x*x + 12096*x*x*x + 6048*x*y*z;
   values[162] = 1080*x + 540*y + 1080*z - 3024*x*y - 6048*x*z - 3024*y*z + 2016*x*y*y + 3024*x*x*y + 6048*x*z*z + 6048*x*x*z + 3024*y*z*z + 2016*y*y*z - 2268*x*x + 1344*x*x*x - 756*y*y + 336*y*y*y - 2268*z*z + 1344*z*z*z + 8064*x*y*z - 120;
   values[163] = 540*x - 1512*x*y - 3024*x*z + 1008*x*y*y + 2016*x*x*y + 3024*x*z*z + 4032*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[164] = 4536*x*y - 1620*x + 3024*x*z - 3024*x*y*y - 6048*x*x*y - 1008*x*z*z - 4032*x*x*z + 4536*x*x - 3024*x*x*x - 4032*x*y*z;
   values[165] = 8064*x*y - 1944*y - 6912*z - 3888*x + 30240*x*z + 15120*y*z - 4032*x*y*y - 6048*x*x*y - 36288*x*z*z - 24192*x*x*z - 18144*y*z*z - 8064*y*y*z + 6048*x*x - 2688*x*x*x + 2016*y*y - 672*y*y*y + 16632*z*z - 10752*z*z*z - 32256*x*y*z + 600;
   values[166] = 4032*x*y - 1944*x + 15120*x*z - 2016*x*y*y - 4032*x*x*y - 18144*x*z*z - 16128*x*x*z + 4032*x*x - 2016*x*x*x - 16128*x*y*z;
   values[167] = 8208*x - 20160*x*y - 19656*x*z + 12096*x*y*y + 24192*x*x*y + 8064*x*z*z + 24192*x*x*z - 20160*x*x + 12096*x*x*x + 24192*x*y*z;
   values[168] = 3024*x + 1512*y + 7560*z - 5040*x*y - 27216*x*z - 13608*y*z + 2016*x*y*y + 3024*x*x*y + 36288*x*z*z + 18144*x*x*z + 18144*y*z*z + 6048*y*y*z - 3780*x*x + 1344*x*x*x - 1260*y*y + 336*y*y*y - 19656*z*z + 13440*z*z*z + 24192*x*y*z - 588;
   values[169] = 1512*x - 2520*x*y - 13608*x*z + 1008*x*y*y + 2016*x*x*y + 18144*x*z*z + 12096*x*x*z - 2520*x*x + 1008*x*x*x + 12096*x*y*z;
   values[170] = 16632*x*y - 7560*x + 21168*x*z - 9072*x*y*y - 18144*x*x*y - 10080*x*z*z - 24192*x*x*z + 16632*x*x - 9072*x*x*x - 24192*x*y*z;
   values[171] = 10584*x*y - 1512*y - 3024*z - 4536*x + 21168*x*z + 7056*y*z - 6048*x*y*y - 12096*x*x*y - 18144*x*z*z - 24192*x*x*z - 6048*y*z*z - 4032*y*y*z + 10584*x*x - 6720*x*x*x + 1764*y*y - 672*y*y*y + 5292*z*z - 2688*z*z*z - 24192*x*y*z + 420;
   values[172] = 3528*x*y - 1512*x + 7056*x*z - 2016*x*y*y - 6048*x*x*y - 6048*x*z*z - 12096*x*x*z + 5292*x*x - 4032*x*x*x - 8064*x*y*z;
   values[173] = 4536*x - 10584*x*y - 7056*x*z + 6048*x*y*y + 18144*x*x*y + 2016*x*z*z + 12096*x*x*z - 15876*x*x + 12096*x*x*x + 8064*x*y*z;
   values[174] = 7560*x + 2520*y + 9072*z - 13608*x*y - 51408*x*z - 17136*y*z + 6048*x*y*y + 12096*x*x*y + 54432*x*z*z + 48384*x*x*z + 18144*y*z*z + 8064*y*y*z - 13608*x*x + 6720*x*x*x - 2268*y*y + 672*y*y*y - 18900*z*z + 10752*z*z*z + 48384*x*y*z - 924;
   values[175] = 2520*x - 4536*x*y - 17136*x*z + 2016*x*y*y + 6048*x*x*y + 18144*x*z*z + 24192*x*x*z - 6804*x*x + 4032*x*x*x + 16128*x*y*z;
   values[176] = 23184*x*y - 11088*x + 22680*x*z - 12096*x*y*y - 36288*x*x*y - 8064*x*z*z - 36288*x*x*z + 34776*x*x - 24192*x*x*x - 24192*x*y*z;
   values[177] = 4032*x + 1008*y + 2016*z - 8064*x*y - 16128*x*z - 4032*y*z + 4032*x*y*y + 10080*x*x*y + 12096*x*z*z + 20160*x*x*z + 3024*y*z*z + 2016*y*y*z - 10080*x*x + 6720*x*x*x - 1008*y*y + 336*y*y*y - 3024*z*z + 1344*z*z*z + 16128*x*y*z - 336;
   values[178] = 1008*x - 2016*x*y - 4032*x*z + 1008*x*y*y + 4032*x*x*y + 3024*x*z*z + 8064*x*x*z - 4032*x*x + 3360*x*x*x + 4032*x*y*z;
   values[179] = 6048*x*y - 3024*x + 4032*x*z - 3024*x*y*y - 12096*x*x*y - 1008*x*z*z - 8064*x*x*z + 12096*x*x - 10080*x*x*x - 4032*x*y*z;
   values[180] = 4536*x*y - 2160*y + 6048*y*z - 6048*x*y*y - 2016*x*x*y - 4032*y*z*z - 8064*y*y*z + 6048*y*y - 4032*y*y*y - 6048*x*y*z;
   values[181] = 540*x - 3024*x*y - 1512*x*z + 3024*x*y*y + 4032*x*x*y + 1008*x*z*z + 2016*x*x*z - 1512*x*x + 1008*x*x*x + 4032*x*y*z;
   values[182] = 2016*x*y*y - 1512*x*y + 2016*x*x*y + 2016*x*y*z;
   values[183] = 6048*y - 10584*x*y - 14112*y*z + 18144*x*y*y + 4032*x*x*y + 8064*y*z*z + 24192*y*y*z - 21168*y*y + 16128*y*y*y + 12096*x*y*z;
   values[184] = 10584*x*y - 1512*x + 3528*x*z - 12096*x*y*y - 12096*x*x*y - 2016*x*z*z - 4032*x*x*z + 3528*x*x - 2016*x*x*x - 12096*x*y*z;
   values[185] = 3528*x*y - 6048*x*y*y - 4032*x*x*y - 4032*x*y*z;
   values[186] = 6048*x*y - 4032*y + 8064*y*z - 12096*x*y*y - 2016*x*x*y - 4032*y*z*z - 16128*y*y*z + 16128*y*y - 13440*y*y*y - 6048*x*y*z;
   values[187] = 1008*x - 8064*x*y - 2016*x*z + 10080*x*y*y + 8064*x*x*y + 1008*x*z*z + 2016*x*x*z - 2016*x*x + 1008*x*x*x + 8064*x*y*z;
   values[188] = 4032*x*y*y - 2016*x*y + 2016*x*x*y + 2016*x*y*z;
   values[189] = 5616*y - 20160*x*y - 13608*y*z + 24192*x*y*y + 12096*x*x*y + 8064*y*z*z + 16128*y*y*z - 13608*y*y + 8064*y*y*y + 24192*x*y*z;
   values[190] = 8064*x*y - 1944*x + 4032*x*z - 6048*x*y*y - 16128*x*x*y - 2016*x*z*z - 8064*x*x*z + 7560*x*x - 6048*x*x*x - 8064*x*y*z;
   values[191] = 4032*x*y - 4032*x*y*y - 8064*x*x*y - 4032*x*y*z;
   values[192] = 23184*x*y - 7560*y + 15624*y*z - 36288*x*y*y - 12096*x*x*y - 8064*y*z*z - 24192*y*y*z + 23436*y*y - 16128*y*y*y - 24192*x*y*z;
   values[193] = 2520*x - 13608*x*y - 4536*x*z + 12096*x*y*y + 24192*x*x*y + 2016*x*z*z + 8064*x*x*z - 8568*x*x + 6048*x*x*x + 12096*x*y*z;
   values[194] = 6048*x*y*y - 4536*x*y + 8064*x*x*y + 4032*x*y*z;
   values[195] = 16632*x*y - 3528*y + 7560*y*z - 18144*x*y*y - 12096*x*x*y - 4032*y*z*z - 8064*y*y*z + 7560*y*y - 4032*y*y*y - 18144*x*y*z;
   values[196] = 1512*x - 5040*x*y - 2520*x*z + 3024*x*y*y + 12096*x*x*y + 1008*x*z*z + 6048*x*x*z - 6804*x*x + 6048*x*x*x + 4032*x*y*z;
   values[197] = 2016*x*y*y - 2520*x*y + 6048*x*x*y + 2016*x*y*z;
   values[198] = 540*y - 3024*x*y - 1512*y*z + 4032*x*y*y + 3024*x*x*y + 1008*y*z*z + 2016*y*y*z - 1512*y*y + 1008*y*y*y + 4032*x*y*z;
   values[199] = 4536*x*y - 2160*x + 6048*x*z - 2016*x*y*y - 6048*x*x*y - 4032*x*z*z - 8064*x*x*z + 6048*x*x - 4032*x*x*x - 6048*x*y*z;
   values[200] = 2016*x*y*y - 1512*x*y + 2016*x*x*y + 2016*x*y*z;
   values[201] = 8064*x*y - 1944*y + 4032*y*z - 16128*x*y*y - 6048*x*x*y - 2016*y*z*z - 8064*y*y*z + 7560*y*y - 6048*y*y*y - 8064*x*y*z;
   values[202] = 5616*x - 20160*x*y - 13608*x*z + 12096*x*y*y + 24192*x*x*y + 8064*x*z*z + 16128*x*x*z - 13608*x*x + 8064*x*x*x + 24192*x*y*z;
   values[203] = 4032*x*y - 8064*x*y*y - 4032*x*x*y - 4032*x*y*z;
   values[204] = 1512*y - 5040*x*y - 2520*y*z + 12096*x*y*y + 3024*x*x*y + 1008*y*z*z + 6048*y*y*z - 6804*y*y + 6048*y*y*y + 4032*x*y*z;
   values[205] = 16632*x*y - 3528*x + 7560*x*z - 12096*x*y*y - 18144*x*x*y - 4032*x*z*z - 8064*x*x*z + 7560*x*x - 4032*x*x*x - 18144*x*y*z;
   values[206] = 6048*x*y*y - 2520*x*y + 2016*x*x*y + 2016*x*y*z;
   values[207] = 10584*x*y - 1512*y + 3528*y*z - 12096*x*y*y - 12096*x*x*y - 2016*y*z*z - 4032*y*y*z + 3528*y*y - 2016*y*y*y - 12096*x*y*z;
   values[208] = 6048*x - 10584*x*y - 14112*x*z + 4032*x*y*y + 18144*x*x*y + 8064*x*z*z + 24192*x*x*z - 21168*x*x + 16128*x*x*x + 12096*x*y*z;
   values[209] = 3528*x*y - 4032*x*y*y - 6048*x*x*y - 4032*x*y*z;
   values[210] = 2520*y - 13608*x*y - 4536*y*z + 24192*x*y*y + 12096*x*x*y + 2016*y*z*z + 8064*y*y*z - 8568*y*y + 6048*y*y*y + 12096*x*y*z;
   values[211] = 23184*x*y - 7560*x + 15624*x*z - 12096*x*y*y - 36288*x*x*y - 8064*x*z*z - 24192*x*x*z + 23436*x*x - 16128*x*x*x - 24192*x*y*z;
   values[212] = 8064*x*y*y - 4536*x*y + 6048*x*x*y + 4032*x*y*z;
   values[213] = 1008*y - 8064*x*y - 2016*y*z + 8064*x*y*y + 10080*x*x*y + 1008*y*z*z + 2016*y*y*z - 2016*y*y + 1008*y*y*y + 8064*x*y*z;
   values[214] = 6048*x*y - 4032*x + 8064*x*z - 2016*x*y*y - 12096*x*x*y - 4032*x*z*z - 16128*x*x*z + 16128*x*x - 13440*x*x*x - 6048*x*y*z;
   values[215] = 2016*x*y*y - 2016*x*y + 4032*x*x*y + 2016*x*y*z;
   values[216] = 12096*y - 21168*x*y - 56448*y*z + 24192*x*y*y + 8064*x*x*y + 48384*y*z*z + 64512*y*y*z - 28224*y*y + 16128*y*y*y + 48384*x*y*z;
   values[217] = 14112*x*y - 3024*x + 14112*x*z - 12096*x*y*y - 16128*x*x*y - 12096*x*z*z - 16128*x*x*z + 7056*x*x - 4032*x*x*x - 32256*x*y*z;
   values[218] = 14112*x*y - 16128*x*y*y - 16128*x*x*y - 24192*x*y*z;
   values[219] = 14112*x*y - 3024*y + 14112*y*z - 16128*x*y*y - 12096*x*x*y - 12096*y*z*z - 16128*y*y*z + 7056*y*y - 4032*y*y*y - 32256*x*y*z;
   values[220] = 12096*x - 21168*x*y - 56448*x*z + 8064*x*y*y + 24192*x*x*y + 48384*x*z*z + 64512*x*x*z - 28224*x*x + 16128*x*x*x + 48384*x*y*z;
   values[221] = 14112*x*y - 16128*x*y*y - 16128*x*x*y - 24192*x*y*z;
   values[222] = 14112*x*y - 3024*y + 14112*y*z - 16128*x*y*y - 12096*x*x*y - 12096*y*z*z - 16128*y*y*z + 7056*y*y - 4032*y*y*y - 32256*x*y*z;
   values[223] = 14112*x*y - 3024*x + 14112*x*z - 12096*x*y*y - 16128*x*x*y - 12096*x*z*z - 16128*x*x*z + 7056*x*x - 4032*x*x*x - 32256*x*y*z;
   values[224] = 24192*x*y*y - 21168*x*y + 24192*x*x*y + 16128*x*y*z;
   values[225] = 24192*x*y - 16128*y + 96768*y*z - 24192*x*y*y - 8064*x*x*y - 96768*y*z*z - 96768*y*y*z + 32256*y*y - 16128*y*y*y - 72576*x*y*z;
   values[226] = 4032*x - 16128*x*y - 24192*x*z + 12096*x*y*y + 16128*x*x*y + 24192*x*z*z + 24192*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[227] = 24192*x*y*y - 24192*x*y + 24192*x*x*y + 48384*x*y*z;
   values[228] = 4032*y - 16128*x*y - 24192*y*z + 16128*x*y*y + 12096*x*x*y + 24192*y*z*z + 24192*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[229] = 24192*x*y - 16128*x + 96768*x*z - 8064*x*y*y - 24192*x*x*y - 96768*x*z*z - 96768*x*x*z + 32256*x*x - 16128*x*x*x - 72576*x*y*z;
   values[230] = 24192*x*y*y - 24192*x*y + 24192*x*x*y + 48384*x*y*z;
   values[231] = 5040*y - 18144*x*y - 34272*y*z + 16128*x*y*y + 12096*x*x*y + 36288*y*z*z + 32256*y*y*z - 9072*y*y + 4032*y*y*y + 64512*x*y*z;
   values[232] = 5040*x - 18144*x*y - 34272*x*z + 12096*x*y*y + 16128*x*x*y + 36288*x*z*z + 32256*x*x*z - 9072*x*x + 4032*x*x*x + 64512*x*y*z;
   values[233] = 46368*x*y - 48384*x*y*y - 48384*x*x*y - 48384*x*y*z;
   values[234] = 24192*x*y - 16128*y + 64512*y*z - 36288*x*y*y - 8064*x*x*y - 48384*y*z*z - 96768*y*y*z + 48384*y*y - 32256*y*y*y - 48384*x*y*z;
   values[235] = 4032*x - 24192*x*y - 16128*x*z + 24192*x*y*y + 24192*x*x*y + 12096*x*z*z + 16128*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[236] = 24192*x*y*y - 16128*x*y + 16128*x*x*y + 24192*x*y*z;
   values[237] = 5040*y - 18144*x*y - 18144*y*z + 32256*x*y*y + 12096*x*x*y + 12096*y*z*z + 32256*y*y*z - 17136*y*y + 12096*y*y*y + 32256*x*y*z;
   values[238] = 46368*x*y - 15120*x + 62496*x*z - 24192*x*y*y - 48384*x*x*y - 48384*x*z*z - 64512*x*x*z + 31248*x*x - 16128*x*x*x - 96768*x*y*z;
   values[239] = 32256*x*y*y - 18144*x*y + 16128*x*x*y + 24192*x*y*z;
   values[240] = 4032*y - 16128*x*y - 16128*y*z + 24192*x*y*y + 12096*x*x*y + 12096*y*z*z + 24192*y*y*z - 12096*y*y + 8064*y*y*y + 32256*x*y*z;
   values[241] = 4032*x - 24192*x*y - 16128*x*z + 24192*x*y*y + 24192*x*x*y + 12096*x*z*z + 16128*x*x*z - 8064*x*x + 4032*x*x*x + 48384*x*y*z;
   values[242] = 24192*x*y - 36288*x*y*y - 24192*x*x*y - 16128*x*y*z;
   values[243] = 46368*x*y - 15120*y + 62496*y*z - 48384*x*y*y - 24192*x*x*y - 48384*y*z*z - 64512*y*y*z + 31248*y*y - 16128*y*y*y - 96768*x*y*z;
   values[244] = 5040*x - 18144*x*y - 18144*x*z + 12096*x*y*y + 32256*x*x*y + 12096*x*z*z + 32256*x*x*z - 17136*x*x + 12096*x*x*x + 32256*x*y*z;
   values[245] = 16128*x*y*y - 18144*x*y + 32256*x*x*y + 24192*x*y*z;
   values[246] = 4032*y - 24192*x*y - 16128*y*z + 24192*x*y*y + 24192*x*x*y + 12096*y*z*z + 16128*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[247] = 24192*x*y - 16128*x + 64512*x*z - 8064*x*y*y - 36288*x*x*y - 48384*x*z*z - 96768*x*x*z + 48384*x*x - 32256*x*x*x - 48384*x*y*z;
   values[248] = 16128*x*y*y - 16128*x*y + 24192*x*x*y + 24192*x*y*z;
   values[249] = 4032*y - 24192*x*y - 16128*y*z + 24192*x*y*y + 24192*x*x*y + 12096*y*z*z + 16128*y*y*z - 8064*y*y + 4032*y*y*y + 48384*x*y*z;
   values[250] = 4032*x - 16128*x*y - 16128*x*z + 12096*x*y*y + 24192*x*x*y + 12096*x*z*z + 24192*x*x*z - 12096*x*x + 8064*x*x*x + 32256*x*y*z;
   values[251] = 24192*x*y - 24192*x*y*y - 36288*x*x*y - 16128*x*y*z;
}
static void Curl_T_P4_3D_3D_D200(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 1344*x*y - 1680*y - 1680*z - 840*x + 1344*x*z + 2688*y*z + 1344*y*y + 1344*z*z + 480;
   values[1] = 2520*x + 1680*y + 1680*z - 4032*x*y - 4032*x*z - 2688*y*z - 2688*x*x - 1344*y*y - 1344*z*z - 480;
   values[2] = 2520*x + 1680*y + 1680*z - 4032*x*y - 4032*x*z - 2688*y*z - 2688*x*x - 1344*y*y - 1344*z*z - 480;
   values[3] = 10080*x + 16632*y + 16632*z - 16128*x*y - 16128*x*z - 24192*y*z - 12096*y*y - 12096*z*z - 5400;
   values[4] = 36288*x*y - 13104*y - 13104*z - 28728*x + 36288*x*z + 16128*y*z + 32256*x*x + 8064*y*y + 8064*z*z + 5040;
   values[5] = 36288*x*y - 13104*y - 13104*z - 28728*x + 36288*x*z + 16128*y*z + 32256*x*x + 8064*y*y + 8064*z*z + 5040;
   values[6] = 40320*x*y - 36288*y - 36288*z - 25200*x + 40320*x*z + 48384*y*z + 24192*y*y + 24192*z*z + 12960;
   values[7] = 69552*x + 24192*y + 24192*z - 72576*x*y - 72576*x*z - 24192*y*z - 80640*x*x - 12096*y*y - 12096*z*z - 11664;
   values[8] = 69552*x + 24192*y + 24192*z - 72576*x*y - 72576*x*z - 24192*y*z - 80640*x*x - 12096*y*y - 12096*z*z - 11664;
   values[9] = 16800*x + 21840*y + 21840*z - 26880*x*y - 26880*x*z - 26880*y*z - 13440*y*y - 13440*z*z - 8400;
   values[10] = 40320*x*y - 12768*y - 12768*z - 45360*x + 40320*x*z + 10752*y*z + 53760*x*x + 5376*y*y + 5376*z*z + 7392;
   values[11] = 40320*x*y - 12768*y - 12768*z - 45360*x + 40320*x*z + 10752*y*z + 53760*x*x + 5376*y*y + 5376*z*z + 7392;
   values[12] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[13] = 4032*x*y - 2520*y - 3360*z - 3360*x + 5376*x*z + 4032*y*z + 2688*x*x + 1344*y*y + 2688*z*z + 960;
   values[14] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[15] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[16] = 11592*x + 17136*y + 11592*z - 24192*x*y - 16128*x*z - 24192*y*z - 8064*x*x - 12096*y*y - 8064*z*z - 3960;
   values[17] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[18] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[19] = 36288*x*y - 28728*y - 13104*z - 13104*x + 16128*x*z + 36288*y*z + 8064*x*x + 24192*y*y + 8064*z*z + 5184;
   values[20] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[21] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[22] = 4872*x + 14112*y + 4872*z - 16128*x*y - 5376*x*z - 16128*y*z - 2688*x*x - 13440*y*y - 2688*z*z - 2184;
   values[23] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[24] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[25] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[26] = 5376*x*y - 3360*y - 2520*z - 3360*x + 4032*x*z + 4032*y*z + 2688*x*x + 2688*y*y + 1344*z*z + 960;
   values[27] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[28] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[29] = 11592*x + 11592*y + 17136*z - 16128*x*y - 24192*x*z - 24192*y*z - 8064*x*x - 8064*y*y - 12096*z*z - 3960;
   values[30] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[31] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[32] = 16128*x*y - 13104*y - 28728*z - 13104*x + 36288*x*z + 36288*y*z + 8064*x*x + 8064*y*y + 24192*z*z + 5184;
   values[33] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[34] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[35] = 4872*x + 4872*y + 14112*z - 5376*x*y - 16128*x*z - 16128*y*z - 2688*x*x - 2688*y*y - 13440*z*z - 2184;
   values[36] = 504*y - 1344*x*y;
   values[37] = 2688*x*x - 2016*x + 288;
   values[38] = 0;
   values[39] = 4032*x*y - 504*y - 4032*y*y;
   values[40] = 4536*x - 3024*y + 12096*x*y - 8064*x*x - 432;
   values[41] = 0;
   values[42] = 8064*y*y - 4032*x*y - 504*y;
   values[43] = 4032*y - 3024*x - 24192*x*y + 8064*x*x + 4032*y*y + 144;
   values[44] = 0;
   values[45] = 504*y + 1344*x*y - 4032*y*y;
   values[46] = 504*x - 1008*y + 12096*x*y - 2688*x*x - 4032*y*y;
   values[47] = 0;
   values[48] = 504*z - 1344*x*z;
   values[49] = 0;
   values[50] = 2688*x*x - 2016*x + 288;
   values[51] = 4032*x*z - 504*z - 4032*z*z;
   values[52] = 0;
   values[53] = 4536*x - 3024*z + 12096*x*z - 8064*x*x - 432;
   values[54] = 8064*z*z - 4032*x*z - 504*z;
   values[55] = 0;
   values[56] = 4032*z - 3024*x - 24192*x*z + 8064*x*x + 4032*z*z + 144;
   values[57] = 504*z + 1344*x*z - 4032*z*z;
   values[58] = 0;
   values[59] = 504*x - 1008*z + 12096*x*z - 2688*x*x - 4032*z*z;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = 0;
   values[67] = 0;
   values[68] = 0;
   values[69] = 0;
   values[70] = 0;
   values[71] = 0;
   values[72] = -2016*y*z;
   values[73] = 8064*x*z - 2016*z;
   values[74] = 504*y - 2016*x*y;
   values[75] = 4032*y*z;
   values[76] = 2016*z - 16128*x*z + 8064*z*z;
   values[77] = 4032*x*y - 504*y - 4032*y*z;
   values[78] = -2016*y*z;
   values[79] = 8064*x*z - 8064*z*z;
   values[80] = 4032*y*z - 2016*x*y;
   values[81] = 4032*y*z;
   values[82] = 2520*z - 16128*x*z + 12096*y*z;
   values[83] = 4032*x*y - 4032*y*y;
   values[84] = -4032*y*z;
   values[85] = 16128*x*z - 504*z - 12096*y*z - 8064*z*z;
   values[86] = 4032*y*z - 4032*x*y - 504*y + 4032*y*y;
   values[87] = -2016*y*z;
   values[88] = 8064*x*z - 504*z - 12096*y*z;
   values[89] = 4032*y*y - 2016*x*y - 504*y;
   values[90] = -2016*y*z;
   values[91] = 504*z - 2016*x*z;
   values[92] = 8064*x*y - 2016*y;
   values[93] = 4032*y*z;
   values[94] = 4032*x*z - 4032*z*z;
   values[95] = 2520*y - 16128*x*y + 12096*y*z;
   values[96] = -2016*y*z;
   values[97] = 4032*z*z - 2016*x*z - 504*z;
   values[98] = 8064*x*y - 504*y - 12096*y*z;
   values[99] = 4032*y*z;
   values[100] = 4032*x*z - 504*z - 4032*y*z;
   values[101] = 2016*y - 16128*x*y + 8064*y*y;
   values[102] = -4032*y*z;
   values[103] = 4032*y*z - 4032*x*z - 504*z + 4032*z*z;
   values[104] = 16128*x*y - 504*y - 12096*y*z - 8064*y*y;
   values[105] = -2016*y*z;
   values[106] = 4032*y*z - 2016*x*z;
   values[107] = 8064*x*y - 8064*y*y;
   values[108] = 2016*y*z;
   values[109] = 6048*z - 8064*x*z - 6048*y*z - 8064*z*z;
   values[110] = 2016*x*y - 1512*y + 4032*y*z + 2016*y*y;
   values[111] = -4032*y*z;
   values[112] = 16128*x*z - 14112*z + 12096*y*z + 24192*z*z;
   values[113] = 3528*y - 4032*x*y - 12096*y*z - 4032*y*y;
   values[114] = 2016*y*z;
   values[115] = 8064*z - 8064*x*z - 6048*y*z - 16128*z*z;
   values[116] = 2016*x*y - 2016*y + 8064*y*z + 2016*y*y;
   values[117] = -4032*y*z;
   values[118] = 16128*x*z - 13608*z + 24192*y*z + 16128*z*z;
   values[119] = 4032*y - 4032*x*y - 8064*y*z - 8064*y*y;
   values[120] = 4032*y*z;
   values[121] = 15624*z - 16128*x*z - 24192*y*z - 24192*z*z;
   values[122] = 4032*x*y - 4536*y + 12096*y*z + 8064*y*y;
   values[123] = 2016*y*z;
   values[124] = 7560*z - 8064*x*z - 18144*y*z - 8064*z*z;
   values[125] = 2016*x*y - 2520*y + 4032*y*z + 6048*y*y;
   values[126] = 2016*y*z;
   values[127] = 2016*x*z - 1512*z + 4032*y*z + 2016*z*z;
   values[128] = 6048*y - 8064*x*y - 6048*y*z - 8064*y*y;
   values[129] = -4032*y*z;
   values[130] = 4032*z - 4032*x*z - 8064*y*z - 8064*z*z;
   values[131] = 16128*x*y - 13608*y + 24192*y*z + 16128*y*y;
   values[132] = 2016*y*z;
   values[133] = 2016*x*z - 2520*z + 4032*y*z + 6048*z*z;
   values[134] = 7560*y - 8064*x*y - 18144*y*z - 8064*y*y;
   values[135] = -4032*y*z;
   values[136] = 3528*z - 4032*x*z - 12096*y*z - 4032*z*z;
   values[137] = 16128*x*y - 14112*y + 12096*y*z + 24192*y*y;
   values[138] = 4032*y*z;
   values[139] = 4032*x*z - 4536*z + 12096*y*z + 8064*z*z;
   values[140] = 15624*y - 16128*x*y - 24192*y*z - 24192*y*y;
   values[141] = 2016*y*z;
   values[142] = 2016*x*z - 2016*z + 8064*y*z + 2016*z*z;
   values[143] = 8064*y - 8064*x*y - 6048*y*z - 16128*y*y;
   values[144] = 3024*z - 2016*x*z - 4032*y*z - 4032*z*z;
   values[145] = 6048*x*z - 3024*z + 4032*y*z + 4032*z*z;
   values[146] = 6048*x*y - 3024*y - 6048*z - 4536*x + 12096*x*z + 8064*y*z + 4032*x*x + 2016*y*y + 6048*z*z + 1080;
   values[147] = 4032*x*z - 7056*z + 8064*y*z + 12096*z*z;
   values[148] = 7056*z - 12096*x*z - 8064*y*z - 12096*z*z;
   values[149] = 10584*x + 7056*y + 21168*z - 12096*x*y - 36288*x*z - 24192*y*z - 8064*x*x - 4032*y*y - 24192*z*z - 3024;
   values[150] = 4032*z - 2016*x*z - 4032*y*z - 8064*z*z;
   values[151] = 6048*x*z - 4032*z + 4032*y*z + 8064*z*z;
   values[152] = 6048*x*y - 4032*y - 16128*z - 6048*x + 24192*x*z + 16128*y*z + 4032*x*x + 2016*y*y + 20160*z*z + 2016;
   values[153] = 16128*x*z - 19656*z + 24192*y*z + 24192*z*z;
   values[154] = 15120*z - 36288*x*z - 16128*y*z - 16128*z*z;
   values[155] = 33264*x + 15120*y + 30240*z - 36288*x*y - 72576*x*z - 32256*y*z - 32256*x*x - 8064*y*y - 24192*z*z - 6912;
   values[156] = 22680*z - 16128*x*z - 24192*y*z - 36288*z*z;
   values[157] = 36288*x*z - 17136*z + 16128*y*z + 24192*z*z;
   values[158] = 36288*x*y - 17136*y - 51408*z - 37800*x + 108864*x*z + 48384*y*z + 32256*x*x + 8064*y*y + 48384*z*z + 9072;
   values[159] = 21168*z - 20160*x*z - 24192*y*z - 24192*z*z;
   values[160] = 36288*x*z - 13608*z + 12096*y*z + 12096*z*z;
   values[161] = 36288*x*y - 13608*y - 27216*z - 39312*x + 72576*x*z + 24192*y*z + 40320*x*x + 6048*y*y + 18144*z*z + 7560;
   values[162] = 8064*x*z - 4536*z + 6048*y*z + 6048*z*z;
   values[163] = 6048*x*z - 3024*z + 4032*y*z + 4032*z*z;
   values[164] = 18144*x + 12096*y + 9072*z - 24192*x*y - 18144*x*z - 12096*y*z - 16128*x*x - 8064*y*y - 4032*z*z - 4320;
   values[165] = 12096*z - 16128*x*z - 12096*y*z - 24192*z*z;
   values[166] = 8064*z - 12096*x*z - 8064*y*z - 16128*z*z;
   values[167] = 48384*x*y - 27216*y - 40320*z - 40824*x + 72576*x*z + 48384*y*z + 32256*x*x + 16128*y*y + 24192*z*z + 11232;
   values[168] = 8064*x*z - 7560*z + 6048*y*z + 18144*z*z;
   values[169] = 6048*x*z - 5040*z + 4032*y*z + 12096*z*z;
   values[170] = 22680*x + 15120*y + 33264*z - 24192*x*y - 54432*x*z - 36288*y*z - 16128*x*x - 8064*y*y - 24192*z*z - 7056;
   values[171] = 21168*z - 40320*x*z - 24192*y*z - 24192*z*z;
   values[172] = 10584*z - 24192*x*z - 12096*y*z - 12096*z*z;
   values[173] = 96768*x*y - 42336*y - 31752*z - 84672*x + 72576*x*z + 36288*y*z + 80640*x*x + 24192*y*y + 12096*z*z + 18144;
   values[174] = 40320*x*z - 27216*z + 24192*y*z + 48384*z*z;
   values[175] = 24192*x*z - 13608*z + 12096*y*z + 24192*z*z;
   values[176] = 93744*x + 46872*y + 69552*z - 96768*x*y - 145152*x*z - 72576*y*z - 80640*x*x - 24192*y*y - 36288*z*z - 22680;
   values[177] = 40320*x*z - 20160*z + 20160*y*z + 20160*z*z;
   values[178] = 20160*x*z - 8064*z + 8064*y*z + 8064*z*z;
   values[179] = 80640*x + 32256*y + 24192*z - 80640*x*y - 60480*x*z - 24192*y*z - 80640*x*x - 16128*y*y - 8064*z*z - 16128;
   values[180] = 3024*y - 2016*x*y - 4032*y*z - 4032*y*y;
   values[181] = 12096*x*y - 6048*y - 3024*z - 4536*x + 6048*x*z + 8064*y*z + 4032*x*x + 6048*y*y + 2016*z*z + 1080;
   values[182] = 6048*x*y - 3024*y + 4032*y*z + 4032*y*y;
   values[183] = 4032*x*y - 7056*y + 8064*y*z + 12096*y*y;
   values[184] = 10584*x + 21168*y + 7056*z - 36288*x*y - 12096*x*z - 24192*y*z - 8064*x*x - 24192*y*y - 4032*z*z - 3024;
   values[185] = 7056*y - 12096*x*y - 8064*y*z - 12096*y*y;
   values[186] = 4032*y - 2016*x*y - 4032*y*z - 8064*y*y;
   values[187] = 24192*x*y - 16128*y - 4032*z - 6048*x + 6048*x*z + 16128*y*z + 4032*x*x + 20160*y*y + 2016*z*z + 2016;
   values[188] = 6048*x*y - 4032*y + 4032*y*z + 8064*y*y;
   values[189] = 16128*x*y - 19656*y + 24192*y*z + 24192*y*y;
   values[190] = 33264*x + 30240*y + 15120*z - 72576*x*y - 36288*x*z - 32256*y*z - 32256*x*x - 24192*y*y - 8064*z*z - 6912;
   values[191] = 15120*y - 36288*x*y - 16128*y*z - 16128*y*y;
   values[192] = 22680*y - 16128*x*y - 24192*y*z - 36288*y*y;
   values[193] = 108864*x*y - 51408*y - 17136*z - 37800*x + 36288*x*z + 48384*y*z + 32256*x*x + 48384*y*y + 8064*z*z + 9072;
   values[194] = 36288*x*y - 17136*y + 16128*y*z + 24192*y*y;
   values[195] = 21168*y - 20160*x*y - 24192*y*z - 24192*y*y;
   values[196] = 72576*x*y - 27216*y - 13608*z - 39312*x + 36288*x*z + 24192*y*z + 40320*x*x + 18144*y*y + 6048*z*z + 7560;
   values[197] = 36288*x*y - 13608*y + 12096*y*z + 12096*y*y;
   values[198] = 8064*x*y - 4536*y + 6048*y*z + 6048*y*y;
   values[199] = 18144*x + 9072*y + 12096*z - 18144*x*y - 24192*x*z - 12096*y*z - 16128*x*x - 4032*y*y - 8064*z*z - 4320;
   values[200] = 6048*x*y - 3024*y + 4032*y*z + 4032*y*y;
   values[201] = 12096*y - 16128*x*y - 12096*y*z - 24192*y*y;
   values[202] = 72576*x*y - 40320*y - 27216*z - 40824*x + 48384*x*z + 48384*y*z + 32256*x*x + 24192*y*y + 16128*z*z + 11232;
   values[203] = 8064*y - 12096*x*y - 8064*y*z - 16128*y*y;
   values[204] = 8064*x*y - 7560*y + 6048*y*z + 18144*y*y;
   values[205] = 22680*x + 33264*y + 15120*z - 54432*x*y - 24192*x*z - 36288*y*z - 16128*x*x - 24192*y*y - 8064*z*z - 7056;
   values[206] = 6048*x*y - 5040*y + 4032*y*z + 12096*y*y;
   values[207] = 21168*y - 40320*x*y - 24192*y*z - 24192*y*y;
   values[208] = 72576*x*y - 31752*y - 42336*z - 84672*x + 96768*x*z + 36288*y*z + 80640*x*x + 12096*y*y + 24192*z*z + 18144;
   values[209] = 10584*y - 24192*x*y - 12096*y*z - 12096*y*y;
   values[210] = 40320*x*y - 27216*y + 24192*y*z + 48384*y*y;
   values[211] = 93744*x + 69552*y + 46872*z - 145152*x*y - 96768*x*z - 72576*y*z - 80640*x*x - 36288*y*y - 24192*z*z - 22680;
   values[212] = 24192*x*y - 13608*y + 12096*y*z + 24192*y*y;
   values[213] = 40320*x*y - 20160*y + 20160*y*z + 20160*y*y;
   values[214] = 80640*x + 24192*y + 32256*z - 60480*x*y - 80640*x*z - 24192*y*z - 80640*x*x - 8064*y*y - 16128*z*z - 16128;
   values[215] = 20160*x*y - 8064*y + 8064*y*z + 8064*y*y;
   values[216] = 16128*y*z;
   values[217] = 14112*z - 24192*x*z - 32256*y*z - 16128*z*z;
   values[218] = 14112*y - 24192*x*y - 32256*y*z - 16128*y*y;
   values[219] = -24192*y*z;
   values[220] = 96768*x*z - 56448*z + 48384*y*z + 64512*z*z;
   values[221] = 14112*y - 24192*x*y - 32256*y*z - 16128*y*y;
   values[222] = -24192*y*z;
   values[223] = 14112*z - 24192*x*z - 32256*y*z - 16128*z*z;
   values[224] = 96768*x*y - 56448*y + 48384*y*z + 64512*y*y;
   values[225] = -16128*y*z;
   values[226] = 24192*x*z - 16128*z + 32256*y*z + 24192*z*z;
   values[227] = 24192*x*y - 16128*y + 48384*y*z + 16128*y*y;
   values[228] = 24192*y*z;
   values[229] = 64512*z - 96768*x*z - 48384*y*z - 96768*z*z;
   values[230] = 24192*x*y - 16128*y + 48384*y*z + 16128*y*y;
   values[231] = 24192*y*z;
   values[232] = 24192*x*z - 18144*z + 32256*y*z + 32256*z*z;
   values[233] = 62496*y - 96768*x*y - 96768*y*z - 64512*y*y;
   values[234] = -16128*y*z;
   values[235] = 24192*x*z - 16128*z + 48384*y*z + 16128*z*z;
   values[236] = 24192*x*y - 16128*y + 32256*y*z + 24192*y*y;
   values[237] = 24192*y*z;
   values[238] = 62496*z - 96768*x*z - 96768*y*z - 64512*z*z;
   values[239] = 24192*x*y - 18144*y + 32256*y*z + 32256*y*y;
   values[240] = 24192*y*z;
   values[241] = 24192*x*z - 16128*z + 48384*y*z + 16128*z*z;
   values[242] = 64512*y - 96768*x*y - 48384*y*z - 96768*y*y;
   values[243] = -48384*y*z;
   values[244] = 72576*x*z - 34272*z + 64512*y*z + 32256*z*z;
   values[245] = 72576*x*y - 34272*y + 64512*y*z + 32256*y*y;
   values[246] = 48384*y*z;
   values[247] = 96768*z - 193536*x*z - 72576*y*z - 96768*z*z;
   values[248] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[249] = 48384*y*z;
   values[250] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[251] = 96768*y - 193536*x*y - 72576*y*z - 96768*y*y;
}
static void Curl_T_P4_3D_3D_D020(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 4032*x*y - 3360*y - 3360*z - 2520*x + 4032*x*z + 5376*y*z + 1344*x*x + 2688*y*y + 2688*z*z + 960;
   values[1] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[2] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[3] = 17136*x + 11592*y + 11592*z - 24192*x*y - 24192*x*z - 16128*y*z - 12096*x*x - 8064*y*y - 8064*z*z - 3960;
   values[4] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[5] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[6] = 36288*x*y - 13104*y - 13104*z - 28728*x + 36288*x*z + 16128*y*z + 24192*x*x + 8064*y*y + 8064*z*z + 5184;
   values[7] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[8] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[9] = 14112*x + 4872*y + 4872*z - 16128*x*y - 16128*x*z - 5376*y*z - 13440*x*x - 2688*y*y - 2688*z*z - 2184;
   values[10] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[11] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[12] = 1680*x + 2520*y + 1680*z - 4032*x*y - 2688*x*z - 4032*y*z - 1344*x*x - 2688*y*y - 1344*z*z - 480;
   values[13] = 1344*x*y - 840*y - 1680*z - 1680*x + 2688*x*z + 1344*y*z + 1344*x*x + 1344*z*z + 480;
   values[14] = 1680*x + 2520*y + 1680*z - 4032*x*y - 2688*x*z - 4032*y*z - 1344*x*x - 2688*y*y - 1344*z*z - 480;
   values[15] = 36288*x*y - 28728*y - 13104*z - 13104*x + 16128*x*z + 36288*y*z + 8064*x*x + 32256*y*y + 8064*z*z + 5040;
   values[16] = 16632*x + 10080*y + 16632*z - 16128*x*y - 24192*x*z - 16128*y*z - 12096*x*x - 12096*z*z - 5400;
   values[17] = 36288*x*y - 28728*y - 13104*z - 13104*x + 16128*x*z + 36288*y*z + 8064*x*x + 32256*y*y + 8064*z*z + 5040;
   values[18] = 24192*x + 69552*y + 24192*z - 72576*x*y - 24192*x*z - 72576*y*z - 12096*x*x - 80640*y*y - 12096*z*z - 11664;
   values[19] = 40320*x*y - 25200*y - 36288*z - 36288*x + 48384*x*z + 40320*y*z + 24192*x*x + 24192*z*z + 12960;
   values[20] = 24192*x + 69552*y + 24192*z - 72576*x*y - 24192*x*z - 72576*y*z - 12096*x*x - 80640*y*y - 12096*z*z - 11664;
   values[21] = 40320*x*y - 45360*y - 12768*z - 12768*x + 10752*x*z + 40320*y*z + 5376*x*x + 53760*y*y + 5376*z*z + 7392;
   values[22] = 21840*x + 16800*y + 21840*z - 26880*x*y - 26880*x*z - 26880*y*z - 13440*x*x - 13440*z*z - 8400;
   values[23] = 40320*x*y - 45360*y - 12768*z - 12768*x + 10752*x*z + 40320*y*z + 5376*x*x + 53760*y*y + 5376*z*z + 7392;
   values[24] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[25] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[26] = 5376*x*y - 3360*y - 2520*z - 3360*x + 4032*x*z + 4032*y*z + 2688*x*x + 2688*y*y + 1344*z*z + 960;
   values[27] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[28] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[29] = 11592*x + 11592*y + 17136*z - 16128*x*y - 24192*x*z - 24192*y*z - 8064*x*x - 8064*y*y - 12096*z*z - 3960;
   values[30] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[31] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[32] = 16128*x*y - 13104*y - 28728*z - 13104*x + 36288*x*z + 36288*y*z + 8064*x*x + 8064*y*y + 24192*z*z + 5184;
   values[33] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[34] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[35] = 4872*x + 4872*y + 14112*z - 5376*x*y - 16128*x*z - 16128*y*z - 2688*x*x - 2688*y*y - 13440*z*z - 2184;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 2016*x - 4032*x*x - 144;
   values[40] = 0;
   values[41] = 0;
   values[42] = 1512*y - 1008*x - 12096*x*y + 8064*x*x - 144;
   values[43] = 4032*x*x - 1008*x;
   values[44] = 0;
   values[45] = 504*y - 1008*x + 12096*x*y - 4032*x*x - 2688*y*y;
   values[46] = 504*x + 1344*x*y - 4032*x*x;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = 0;
   values[58] = 0;
   values[59] = 0;
   values[60] = 0;
   values[61] = 504*z - 1344*y*z;
   values[62] = 2688*y*y - 2016*y + 288;
   values[63] = 0;
   values[64] = 4032*y*z - 504*z - 4032*z*z;
   values[65] = 4536*y - 3024*z + 12096*y*z - 8064*y*y - 432;
   values[66] = 0;
   values[67] = 8064*z*z - 4032*y*z - 504*z;
   values[68] = 4032*z - 3024*y - 24192*y*z + 8064*y*y + 4032*z*z + 144;
   values[69] = 0;
   values[70] = 504*z + 1344*y*z - 4032*z*z;
   values[71] = 504*y - 1008*z + 12096*y*z - 2688*y*y - 4032*z*z;
   values[72] = 0;
   values[73] = 0;
   values[74] = 0;
   values[75] = 0;
   values[76] = 0;
   values[77] = 0;
   values[78] = 0;
   values[79] = 0;
   values[80] = 0;
   values[81] = 1008*z - 8064*x*z;
   values[82] = 0;
   values[83] = 1008*x - 4032*x*x;
   values[84] = 8064*x*z - 4032*z*z;
   values[85] = 0;
   values[86] = 4032*x*x - 8064*x*z;
   values[87] = 504*z + 8064*x*z - 6048*y*z;
   values[88] = 4032*x*z;
   values[89] = 504*x - 6048*x*y + 4032*x*x;
   values[90] = 0;
   values[91] = 0;
   values[92] = 0;
   values[93] = 0;
   values[94] = 0;
   values[95] = 0;
   values[96] = 0;
   values[97] = 0;
   values[98] = 0;
   values[99] = 504*z - 4032*x*z;
   values[100] = 0;
   values[101] = 8064*x*x - 2016*x;
   values[102] = 504*z + 4032*x*z - 4032*z*z;
   values[103] = 0;
   values[104] = 504*x + 12096*x*z - 8064*x*x;
   values[105] = 4032*x*z - 2016*y*z;
   values[106] = -2016*x*z;
   values[107] = 8064*x*y - 8064*x*x;
   values[108] = 4032*x*z - 3024*z + 6048*y*z + 4032*z*z;
   values[109] = 3024*z - 4032*x*z - 2016*y*z - 4032*z*z;
   values[110] = 6048*x*y - 4536*y - 6048*z - 3024*x + 8064*x*z + 12096*y*z + 2016*x*x + 4032*y*y + 6048*z*z + 1080;
   values[111] = 7056*z - 8064*x*z - 12096*y*z - 12096*z*z;
   values[112] = 8064*x*z - 7056*z + 4032*y*z + 12096*z*z;
   values[113] = 7056*x + 10584*y + 21168*z - 12096*x*y - 24192*x*z - 36288*y*z - 4032*x*x - 8064*y*y - 24192*z*z - 3024;
   values[114] = 4032*x*z - 4032*z + 6048*y*z + 8064*z*z;
   values[115] = 4032*z - 4032*x*z - 2016*y*z - 8064*z*z;
   values[116] = 6048*x*y - 6048*y - 16128*z - 4032*x + 16128*x*z + 24192*y*z + 2016*x*x + 4032*y*y + 20160*z*z + 2016;
   values[117] = 15120*z - 16128*x*z - 36288*y*z - 16128*z*z;
   values[118] = 24192*x*z - 19656*z + 16128*y*z + 24192*z*z;
   values[119] = 15120*x + 33264*y + 30240*z - 36288*x*y - 32256*x*z - 72576*y*z - 8064*x*x - 32256*y*y - 24192*z*z - 6912;
   values[120] = 16128*x*z - 17136*z + 36288*y*z + 24192*z*z;
   values[121] = 22680*z - 24192*x*z - 16128*y*z - 36288*z*z;
   values[122] = 36288*x*y - 37800*y - 51408*z - 17136*x + 48384*x*z + 108864*y*z + 8064*x*x + 32256*y*y + 48384*z*z + 9072;
   values[123] = 12096*x*z - 13608*z + 36288*y*z + 12096*z*z;
   values[124] = 21168*z - 24192*x*z - 20160*y*z - 24192*z*z;
   values[125] = 36288*x*y - 39312*y - 27216*z - 13608*x + 24192*x*z + 72576*y*z + 6048*x*x + 40320*y*y + 18144*z*z + 7560;
   values[126] = 4032*x*z - 3024*z + 6048*y*z + 4032*z*z;
   values[127] = 6048*x*z - 4536*z + 8064*y*z + 6048*z*z;
   values[128] = 12096*x + 18144*y + 9072*z - 24192*x*y - 12096*x*z - 18144*y*z - 8064*x*x - 16128*y*y - 4032*z*z - 4320;
   values[129] = 8064*z - 8064*x*z - 12096*y*z - 16128*z*z;
   values[130] = 12096*z - 12096*x*z - 16128*y*z - 24192*z*z;
   values[131] = 48384*x*y - 40824*y - 40320*z - 27216*x + 48384*x*z + 72576*y*z + 16128*x*x + 32256*y*y + 24192*z*z + 11232;
   values[132] = 4032*x*z - 5040*z + 6048*y*z + 12096*z*z;
   values[133] = 6048*x*z - 7560*z + 8064*y*z + 18144*z*z;
   values[134] = 15120*x + 22680*y + 33264*z - 24192*x*y - 36288*x*z - 54432*y*z - 8064*x*x - 16128*y*y - 24192*z*z - 7056;
   values[135] = 10584*z - 12096*x*z - 24192*y*z - 12096*z*z;
   values[136] = 21168*z - 24192*x*z - 40320*y*z - 24192*z*z;
   values[137] = 96768*x*y - 84672*y - 31752*z - 42336*x + 36288*x*z + 72576*y*z + 24192*x*x + 80640*y*y + 12096*z*z + 18144;
   values[138] = 12096*x*z - 13608*z + 24192*y*z + 24192*z*z;
   values[139] = 24192*x*z - 27216*z + 40320*y*z + 48384*z*z;
   values[140] = 46872*x + 93744*y + 69552*z - 96768*x*y - 72576*x*z - 145152*y*z - 24192*x*x - 80640*y*y - 36288*z*z - 22680;
   values[141] = 8064*x*z - 8064*z + 20160*y*z + 8064*z*z;
   values[142] = 20160*x*z - 20160*z + 40320*y*z + 20160*z*z;
   values[143] = 32256*x + 80640*y + 24192*z - 80640*x*y - 24192*x*z - 60480*y*z - 16128*x*x - 80640*y*y - 8064*z*z - 16128;
   values[144] = 6048*z - 6048*x*z - 8064*y*z - 8064*z*z;
   values[145] = 2016*x*z;
   values[146] = 2016*x*y - 1512*x + 4032*x*z + 2016*x*x;
   values[147] = 12096*x*z - 14112*z + 16128*y*z + 24192*z*z;
   values[148] = -4032*x*z;
   values[149] = 3528*x - 4032*x*y - 12096*x*z - 4032*x*x;
   values[150] = 8064*z - 6048*x*z - 8064*y*z - 16128*z*z;
   values[151] = 2016*x*z;
   values[152] = 2016*x*y - 2016*x + 8064*x*z + 2016*x*x;
   values[153] = 24192*x*z - 13608*z + 16128*y*z + 16128*z*z;
   values[154] = -4032*x*z;
   values[155] = 4032*x - 4032*x*y - 8064*x*z - 8064*x*x;
   values[156] = 15624*z - 24192*x*z - 16128*y*z - 24192*z*z;
   values[157] = 4032*x*z;
   values[158] = 4032*x*y - 4536*x + 12096*x*z + 8064*x*x;
   values[159] = 7560*z - 18144*x*z - 8064*y*z - 8064*z*z;
   values[160] = 2016*x*z;
   values[161] = 2016*x*y - 2520*x + 4032*x*z + 6048*x*x;
   values[162] = 4032*x*z - 1512*z + 2016*y*z + 2016*z*z;
   values[163] = 2016*x*z;
   values[164] = 6048*x - 8064*x*y - 6048*x*z - 8064*x*x;
   values[165] = 4032*z - 8064*x*z - 4032*y*z - 8064*z*z;
   values[166] = -4032*x*z;
   values[167] = 16128*x*y - 13608*x + 24192*x*z + 16128*x*x;
   values[168] = 4032*x*z - 2520*z + 2016*y*z + 6048*z*z;
   values[169] = 2016*x*z;
   values[170] = 7560*x - 8064*x*y - 18144*x*z - 8064*x*x;
   values[171] = 3528*z - 12096*x*z - 4032*y*z - 4032*z*z;
   values[172] = -4032*x*z;
   values[173] = 16128*x*y - 14112*x + 12096*x*z + 24192*x*x;
   values[174] = 12096*x*z - 4536*z + 4032*y*z + 8064*z*z;
   values[175] = 4032*x*z;
   values[176] = 15624*x - 16128*x*y - 24192*x*z - 24192*x*x;
   values[177] = 8064*x*z - 2016*z + 2016*y*z + 2016*z*z;
   values[178] = 2016*x*z;
   values[179] = 8064*x - 8064*x*y - 6048*x*z - 16128*x*x;
   values[180] = 9072*x + 18144*y + 12096*z - 18144*x*y - 12096*x*z - 24192*y*z - 4032*x*x - 16128*y*y - 8064*z*z - 4320;
   values[181] = 8064*x*y - 4536*x + 6048*x*z + 6048*x*x;
   values[182] = 6048*x*y - 3024*x + 4032*x*z + 4032*x*x;
   values[183] = 72576*x*y - 84672*y - 42336*z - 31752*x + 36288*x*z + 96768*y*z + 12096*x*x + 80640*y*y + 24192*z*z + 18144;
   values[184] = 21168*x - 40320*x*y - 24192*x*z - 24192*x*x;
   values[185] = 10584*x - 24192*x*y - 12096*x*z - 12096*x*x;
   values[186] = 24192*x + 80640*y + 32256*z - 60480*x*y - 24192*x*z - 80640*y*z - 8064*x*x - 80640*y*y - 16128*z*z - 16128;
   values[187] = 40320*x*y - 20160*x + 20160*x*z + 20160*x*x;
   values[188] = 20160*x*y - 8064*x + 8064*x*z + 8064*x*x;
   values[189] = 72576*x*y - 40824*y - 27216*z - 40320*x + 48384*x*z + 48384*y*z + 24192*x*x + 32256*y*y + 16128*z*z + 11232;
   values[190] = 12096*x - 16128*x*y - 12096*x*z - 24192*x*x;
   values[191] = 8064*x - 12096*x*y - 8064*x*z - 16128*x*x;
   values[192] = 69552*x + 93744*y + 46872*z - 145152*x*y - 72576*x*z - 96768*y*z - 36288*x*x - 80640*y*y - 24192*z*z - 22680;
   values[193] = 40320*x*y - 27216*x + 24192*x*z + 48384*x*x;
   values[194] = 24192*x*y - 13608*x + 12096*x*z + 24192*x*x;
   values[195] = 33264*x + 22680*y + 15120*z - 54432*x*y - 36288*x*z - 24192*y*z - 24192*x*x - 16128*y*y - 8064*z*z - 7056;
   values[196] = 8064*x*y - 7560*x + 6048*x*z + 18144*x*x;
   values[197] = 6048*x*y - 5040*x + 4032*x*z + 12096*x*x;
   values[198] = 12096*x*y - 4536*y - 3024*z - 6048*x + 8064*x*z + 6048*y*z + 6048*x*x + 4032*y*y + 2016*z*z + 1080;
   values[199] = 3024*x - 2016*x*y - 4032*x*z - 4032*x*x;
   values[200] = 6048*x*y - 3024*x + 4032*x*z + 4032*x*x;
   values[201] = 30240*x + 33264*y + 15120*z - 72576*x*y - 32256*x*z - 36288*y*z - 24192*x*x - 32256*y*y - 8064*z*z - 6912;
   values[202] = 16128*x*y - 19656*x + 24192*x*z + 24192*x*x;
   values[203] = 15120*x - 36288*x*y - 16128*x*z - 16128*x*x;
   values[204] = 72576*x*y - 39312*y - 13608*z - 27216*x + 24192*x*z + 36288*y*z + 18144*x*x + 40320*y*y + 6048*z*z + 7560;
   values[205] = 21168*x - 20160*x*y - 24192*x*z - 24192*x*x;
   values[206] = 36288*x*y - 13608*x + 12096*x*z + 12096*x*x;
   values[207] = 21168*x + 10584*y + 7056*z - 36288*x*y - 24192*x*z - 12096*y*z - 24192*x*x - 8064*y*y - 4032*z*z - 3024;
   values[208] = 4032*x*y - 7056*x + 8064*x*z + 12096*x*x;
   values[209] = 7056*x - 12096*x*y - 8064*x*z - 12096*x*x;
   values[210] = 108864*x*y - 37800*y - 17136*z - 51408*x + 48384*x*z + 36288*y*z + 48384*x*x + 32256*y*y + 8064*z*z + 9072;
   values[211] = 22680*x - 16128*x*y - 24192*x*z - 36288*x*x;
   values[212] = 36288*x*y - 17136*x + 16128*x*z + 24192*x*x;
   values[213] = 24192*x*y - 6048*y - 4032*z - 16128*x + 16128*x*z + 6048*y*z + 20160*x*x + 4032*y*y + 2016*z*z + 2016;
   values[214] = 4032*x - 2016*x*y - 4032*x*z - 8064*x*x;
   values[215] = 6048*x*y - 4032*x + 4032*x*z + 8064*x*x;
   values[216] = 48384*x*z - 56448*z + 96768*y*z + 64512*z*z;
   values[217] = -24192*x*z;
   values[218] = 14112*x - 24192*x*y - 32256*x*z - 16128*x*x;
   values[219] = 14112*z - 32256*x*z - 24192*y*z - 16128*z*z;
   values[220] = 16128*x*z;
   values[221] = 14112*x - 24192*x*y - 32256*x*z - 16128*x*x;
   values[222] = 14112*z - 32256*x*z - 24192*y*z - 16128*z*z;
   values[223] = -24192*x*z;
   values[224] = 96768*x*y - 56448*x + 48384*x*z + 64512*x*x;
   values[225] = 64512*z - 48384*x*z - 96768*y*z - 96768*z*z;
   values[226] = 24192*x*z;
   values[227] = 24192*x*y - 16128*x + 48384*x*z + 16128*x*x;
   values[228] = 32256*x*z - 16128*z + 24192*y*z + 24192*z*z;
   values[229] = -16128*x*z;
   values[230] = 24192*x*y - 16128*x + 48384*x*z + 16128*x*x;
   values[231] = 32256*x*z - 18144*z + 24192*y*z + 32256*z*z;
   values[232] = 24192*x*z;
   values[233] = 62496*x - 96768*x*y - 96768*x*z - 64512*x*x;
   values[234] = 96768*z - 72576*x*z - 193536*y*z - 96768*z*z;
   values[235] = 48384*x*z;
   values[236] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[237] = 64512*x*z - 34272*z + 72576*y*z + 32256*z*z;
   values[238] = -48384*x*z;
   values[239] = 72576*x*y - 34272*x + 64512*x*z + 32256*x*x;
   values[240] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[241] = 48384*x*z;
   values[242] = 96768*x - 193536*x*y - 72576*x*z - 96768*x*x;
   values[243] = 62496*z - 96768*x*z - 96768*y*z - 64512*z*z;
   values[244] = 24192*x*z;
   values[245] = 24192*x*y - 18144*x + 32256*x*z + 32256*x*x;
   values[246] = 48384*x*z - 16128*z + 24192*y*z + 16128*z*z;
   values[247] = -16128*x*z;
   values[248] = 24192*x*y - 16128*x + 32256*x*z + 24192*x*x;
   values[249] = 48384*x*z - 16128*z + 24192*y*z + 16128*z*z;
   values[250] = 24192*x*z;
   values[251] = 64512*x - 96768*x*y - 48384*x*z - 96768*x*x;
}
static void Curl_T_P4_3D_3D_D002(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 4032*x*y - 3360*y - 3360*z - 2520*x + 4032*x*z + 5376*y*z + 1344*x*x + 2688*y*y + 2688*z*z + 960;
   values[1] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[2] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[3] = 17136*x + 11592*y + 11592*z - 24192*x*y - 24192*x*z - 16128*y*z - 12096*x*x - 8064*y*y - 8064*z*z - 3960;
   values[4] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[5] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[6] = 36288*x*y - 13104*y - 13104*z - 28728*x + 36288*x*z + 16128*y*z + 24192*x*x + 8064*y*y + 8064*z*z + 5184;
   values[7] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[8] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[9] = 14112*x + 4872*y + 4872*z - 16128*x*y - 16128*x*z - 5376*y*z - 13440*x*x - 2688*y*y - 2688*z*z - 2184;
   values[10] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[11] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[12] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[13] = 4032*x*y - 2520*y - 3360*z - 3360*x + 5376*x*z + 4032*y*z + 2688*x*x + 1344*y*y + 2688*z*z + 960;
   values[14] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[15] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[16] = 11592*x + 17136*y + 11592*z - 24192*x*y - 16128*x*z - 24192*y*z - 8064*x*x - 12096*y*y - 8064*z*z - 3960;
   values[17] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[18] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[19] = 36288*x*y - 28728*y - 13104*z - 13104*x + 16128*x*z + 36288*y*z + 8064*x*x + 24192*y*y + 8064*z*z + 5184;
   values[20] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[21] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[22] = 4872*x + 14112*y + 4872*z - 16128*x*y - 5376*x*z - 16128*y*z - 2688*x*x - 13440*y*y - 2688*z*z - 2184;
   values[23] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[24] = 1680*x + 1680*y + 2520*z - 2688*x*y - 4032*x*z - 4032*y*z - 1344*x*x - 1344*y*y - 2688*z*z - 480;
   values[25] = 1680*x + 1680*y + 2520*z - 2688*x*y - 4032*x*z - 4032*y*z - 1344*x*x - 1344*y*y - 2688*z*z - 480;
   values[26] = 2688*x*y - 1680*y - 840*z - 1680*x + 1344*x*z + 1344*y*z + 1344*x*x + 1344*y*y + 480;
   values[27] = 16128*x*y - 13104*y - 28728*z - 13104*x + 36288*x*z + 36288*y*z + 8064*x*x + 8064*y*y + 32256*z*z + 5040;
   values[28] = 16128*x*y - 13104*y - 28728*z - 13104*x + 36288*x*z + 36288*y*z + 8064*x*x + 8064*y*y + 32256*z*z + 5040;
   values[29] = 16632*x + 16632*y + 10080*z - 24192*x*y - 16128*x*z - 16128*y*z - 12096*x*x - 12096*y*y - 5400;
   values[30] = 24192*x + 24192*y + 69552*z - 24192*x*y - 72576*x*z - 72576*y*z - 12096*x*x - 12096*y*y - 80640*z*z - 11664;
   values[31] = 24192*x + 24192*y + 69552*z - 24192*x*y - 72576*x*z - 72576*y*z - 12096*x*x - 12096*y*y - 80640*z*z - 11664;
   values[32] = 48384*x*y - 36288*y - 25200*z - 36288*x + 40320*x*z + 40320*y*z + 24192*x*x + 24192*y*y + 12960;
   values[33] = 10752*x*y - 12768*y - 45360*z - 12768*x + 40320*x*z + 40320*y*z + 5376*x*x + 5376*y*y + 53760*z*z + 7392;
   values[34] = 10752*x*y - 12768*y - 45360*z - 12768*x + 40320*x*z + 40320*y*z + 5376*x*x + 5376*y*y + 53760*z*z + 7392;
   values[35] = 21840*x + 21840*y + 16800*z - 26880*x*y - 26880*x*z - 26880*y*z - 13440*x*x - 13440*y*y - 8400;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 2016*x - 4032*x*x - 144;
   values[52] = 0;
   values[53] = 0;
   values[54] = 1512*z - 1008*x - 12096*x*z + 8064*x*x - 144;
   values[55] = 0;
   values[56] = 4032*x*x - 1008*x;
   values[57] = 504*z - 1008*x + 12096*x*z - 4032*x*x - 2688*z*z;
   values[58] = 0;
   values[59] = 504*x + 1344*x*z - 4032*x*x;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 2016*y - 4032*y*y - 144;
   values[65] = 0;
   values[66] = 0;
   values[67] = 1512*z - 1008*y - 12096*y*z + 8064*y*y - 144;
   values[68] = 4032*y*y - 1008*y;
   values[69] = 0;
   values[70] = 504*z - 1008*y + 12096*y*z - 4032*y*y - 2688*z*z;
   values[71] = 504*y + 1344*y*z - 4032*y*y;
   values[72] = 0;
   values[73] = 0;
   values[74] = 0;
   values[75] = 504*y - 4032*x*y;
   values[76] = 8064*x*x - 2016*x;
   values[77] = 0;
   values[78] = 4032*x*y - 2016*y*z;
   values[79] = 8064*x*z - 8064*x*x;
   values[80] = -2016*x*y;
   values[81] = 0;
   values[82] = 0;
   values[83] = 0;
   values[84] = 504*y + 4032*x*y - 4032*y*y;
   values[85] = 504*x + 12096*x*y - 8064*x*x;
   values[86] = 0;
   values[87] = 0;
   values[88] = 0;
   values[89] = 0;
   values[90] = 0;
   values[91] = 0;
   values[92] = 0;
   values[93] = 1008*y - 8064*x*y;
   values[94] = 1008*x - 4032*x*x;
   values[95] = 0;
   values[96] = 504*y + 8064*x*y - 6048*y*z;
   values[97] = 504*x - 6048*x*z + 4032*x*x;
   values[98] = 4032*x*y;
   values[99] = 0;
   values[100] = 0;
   values[101] = 0;
   values[102] = 8064*x*y - 4032*y*y;
   values[103] = 4032*x*x - 8064*x*y;
   values[104] = 0;
   values[105] = 0;
   values[106] = 0;
   values[107] = 0;
   values[108] = 4032*x*y - 3024*y + 6048*y*z + 4032*y*y;
   values[109] = 12096*x + 9072*y + 18144*z - 12096*x*y - 24192*x*z - 18144*y*z - 8064*x*x - 4032*y*y - 16128*z*z - 4320;
   values[110] = 6048*x*y - 4536*y + 8064*y*z + 6048*y*y;
   values[111] = 10584*y - 12096*x*y - 24192*y*z - 12096*y*y;
   values[112] = 36288*x*y - 31752*y - 84672*z - 42336*x + 96768*x*z + 72576*y*z + 24192*x*x + 12096*y*y + 80640*z*z + 18144;
   values[113] = 21168*y - 24192*x*y - 40320*y*z - 24192*y*y;
   values[114] = 8064*x*y - 8064*y + 20160*y*z + 8064*y*y;
   values[115] = 32256*x + 24192*y + 80640*z - 24192*x*y - 80640*x*z - 60480*y*z - 16128*x*x - 8064*y*y - 80640*z*z - 16128;
   values[116] = 20160*x*y - 20160*y + 40320*y*z + 20160*y*y;
   values[117] = 8064*y - 8064*x*y - 12096*y*z - 16128*y*y;
   values[118] = 48384*x*y - 40320*y - 40824*z - 27216*x + 48384*x*z + 72576*y*z + 16128*x*x + 24192*y*y + 32256*z*z + 11232;
   values[119] = 12096*y - 12096*x*y - 16128*y*z - 24192*y*y;
   values[120] = 12096*x*y - 13608*y + 24192*y*z + 24192*y*y;
   values[121] = 46872*x + 69552*y + 93744*z - 72576*x*y - 96768*x*z - 145152*y*z - 24192*x*x - 36288*y*y - 80640*z*z - 22680;
   values[122] = 24192*x*y - 27216*y + 40320*y*z + 48384*y*y;
   values[123] = 4032*x*y - 5040*y + 6048*y*z + 12096*y*y;
   values[124] = 15120*x + 33264*y + 22680*z - 36288*x*y - 24192*x*z - 54432*y*z - 8064*x*x - 24192*y*y - 16128*z*z - 7056;
   values[125] = 6048*x*y - 7560*y + 8064*y*z + 18144*y*y;
   values[126] = 4032*x*y - 3024*y + 6048*y*z + 4032*y*y;
   values[127] = 8064*x*y - 6048*y - 4536*z - 3024*x + 6048*x*z + 12096*y*z + 2016*x*x + 6048*y*y + 4032*z*z + 1080;
   values[128] = 3024*y - 4032*x*y - 2016*y*z - 4032*y*y;
   values[129] = 15120*y - 16128*x*y - 36288*y*z - 16128*y*y;
   values[130] = 15120*x + 30240*y + 33264*z - 32256*x*y - 36288*x*z - 72576*y*z - 8064*x*x - 24192*y*y - 32256*z*z - 6912;
   values[131] = 24192*x*y - 19656*y + 16128*y*z + 24192*y*y;
   values[132] = 12096*x*y - 13608*y + 36288*y*z + 12096*y*y;
   values[133] = 24192*x*y - 27216*y - 39312*z - 13608*x + 36288*x*z + 72576*y*z + 6048*x*x + 18144*y*y + 40320*z*z + 7560;
   values[134] = 21168*y - 24192*x*y - 20160*y*z - 24192*y*y;
   values[135] = 7056*y - 8064*x*y - 12096*y*z - 12096*y*y;
   values[136] = 7056*x + 21168*y + 10584*z - 24192*x*y - 12096*x*z - 36288*y*z - 4032*x*x - 24192*y*y - 8064*z*z - 3024;
   values[137] = 8064*x*y - 7056*y + 4032*y*z + 12096*y*y;
   values[138] = 16128*x*y - 17136*y + 36288*y*z + 24192*y*y;
   values[139] = 48384*x*y - 51408*y - 37800*z - 17136*x + 36288*x*z + 108864*y*z + 8064*x*x + 48384*y*y + 32256*z*z + 9072;
   values[140] = 22680*y - 24192*x*y - 16128*y*z - 36288*y*y;
   values[141] = 4032*x*y - 4032*y + 6048*y*z + 8064*y*y;
   values[142] = 16128*x*y - 16128*y - 6048*z - 4032*x + 6048*x*z + 24192*y*z + 2016*x*x + 20160*y*y + 4032*z*z + 2016;
   values[143] = 4032*y - 4032*x*y - 2016*y*z - 8064*y*y;
   values[144] = 9072*x + 12096*y + 18144*z - 12096*x*y - 18144*x*z - 24192*y*z - 4032*x*x - 8064*y*y - 16128*z*z - 4320;
   values[145] = 4032*x*y - 3024*x + 6048*x*z + 4032*x*x;
   values[146] = 6048*x*y - 4536*x + 8064*x*z + 6048*x*x;
   values[147] = 36288*x*y - 42336*y - 84672*z - 31752*x + 72576*x*z + 96768*y*z + 12096*x*x + 24192*y*y + 80640*z*z + 18144;
   values[148] = 10584*x - 12096*x*y - 24192*x*z - 12096*x*x;
   values[149] = 21168*x - 24192*x*y - 40320*x*z - 24192*x*x;
   values[150] = 24192*x + 32256*y + 80640*z - 24192*x*y - 60480*x*z - 80640*y*z - 8064*x*x - 16128*y*y - 80640*z*z - 16128;
   values[151] = 8064*x*y - 8064*x + 20160*x*z + 8064*x*x;
   values[152] = 20160*x*y - 20160*x + 40320*x*z + 20160*x*x;
   values[153] = 48384*x*y - 27216*y - 40824*z - 40320*x + 72576*x*z + 48384*y*z + 24192*x*x + 16128*y*y + 32256*z*z + 11232;
   values[154] = 8064*x - 8064*x*y - 12096*x*z - 16128*x*x;
   values[155] = 12096*x - 12096*x*y - 16128*x*z - 24192*x*x;
   values[156] = 69552*x + 46872*y + 93744*z - 72576*x*y - 145152*x*z - 96768*y*z - 36288*x*x - 24192*y*y - 80640*z*z - 22680;
   values[157] = 12096*x*y - 13608*x + 24192*x*z + 24192*x*x;
   values[158] = 24192*x*y - 27216*x + 40320*x*z + 48384*x*x;
   values[159] = 33264*x + 15120*y + 22680*z - 36288*x*y - 54432*x*z - 24192*y*z - 24192*x*x - 8064*y*y - 16128*z*z - 7056;
   values[160] = 4032*x*y - 5040*x + 6048*x*z + 12096*x*x;
   values[161] = 6048*x*y - 7560*x + 8064*x*z + 18144*x*x;
   values[162] = 8064*x*y - 3024*y - 4536*z - 6048*x + 12096*x*z + 6048*y*z + 6048*x*x + 2016*y*y + 4032*z*z + 1080;
   values[163] = 4032*x*y - 3024*x + 6048*x*z + 4032*x*x;
   values[164] = 3024*x - 4032*x*y - 2016*x*z - 4032*x*x;
   values[165] = 30240*x + 15120*y + 33264*z - 32256*x*y - 72576*x*z - 36288*y*z - 24192*x*x - 8064*y*y - 32256*z*z - 6912;
   values[166] = 15120*x - 16128*x*y - 36288*x*z - 16128*x*x;
   values[167] = 24192*x*y - 19656*x + 16128*x*z + 24192*x*x;
   values[168] = 24192*x*y - 13608*y - 39312*z - 27216*x + 72576*x*z + 36288*y*z + 18144*x*x + 6048*y*y + 40320*z*z + 7560;
   values[169] = 12096*x*y - 13608*x + 36288*x*z + 12096*x*x;
   values[170] = 21168*x - 24192*x*y - 20160*x*z - 24192*x*x;
   values[171] = 21168*x + 7056*y + 10584*z - 24192*x*y - 36288*x*z - 12096*y*z - 24192*x*x - 4032*y*y - 8064*z*z - 3024;
   values[172] = 7056*x - 8064*x*y - 12096*x*z - 12096*x*x;
   values[173] = 8064*x*y - 7056*x + 4032*x*z + 12096*x*x;
   values[174] = 48384*x*y - 17136*y - 37800*z - 51408*x + 108864*x*z + 36288*y*z + 48384*x*x + 8064*y*y + 32256*z*z + 9072;
   values[175] = 16128*x*y - 17136*x + 36288*x*z + 24192*x*x;
   values[176] = 22680*x - 24192*x*y - 16128*x*z - 36288*x*x;
   values[177] = 16128*x*y - 4032*y - 6048*z - 16128*x + 24192*x*z + 6048*y*z + 20160*x*x + 2016*y*y + 4032*z*z + 2016;
   values[178] = 4032*x*y - 4032*x + 6048*x*z + 8064*x*x;
   values[179] = 4032*x - 4032*x*y - 2016*x*z - 8064*x*x;
   values[180] = 6048*y - 6048*x*y - 8064*y*z - 8064*y*y;
   values[181] = 4032*x*y - 1512*x + 2016*x*z + 2016*x*x;
   values[182] = 2016*x*y;
   values[183] = 12096*x*y - 14112*y + 16128*y*z + 24192*y*y;
   values[184] = 3528*x - 12096*x*y - 4032*x*z - 4032*x*x;
   values[185] = -4032*x*y;
   values[186] = 8064*y - 6048*x*y - 8064*y*z - 16128*y*y;
   values[187] = 8064*x*y - 2016*x + 2016*x*z + 2016*x*x;
   values[188] = 2016*x*y;
   values[189] = 24192*x*y - 13608*y + 16128*y*z + 16128*y*y;
   values[190] = 4032*x - 8064*x*y - 4032*x*z - 8064*x*x;
   values[191] = -4032*x*y;
   values[192] = 15624*y - 24192*x*y - 16128*y*z - 24192*y*y;
   values[193] = 12096*x*y - 4536*x + 4032*x*z + 8064*x*x;
   values[194] = 4032*x*y;
   values[195] = 7560*y - 18144*x*y - 8064*y*z - 8064*y*y;
   values[196] = 4032*x*y - 2520*x + 2016*x*z + 6048*x*x;
   values[197] = 2016*x*y;
   values[198] = 4032*x*y - 1512*y + 2016*y*z + 2016*y*y;
   values[199] = 6048*x - 6048*x*y - 8064*x*z - 8064*x*x;
   values[200] = 2016*x*y;
   values[201] = 4032*y - 8064*x*y - 4032*y*z - 8064*y*y;
   values[202] = 24192*x*y - 13608*x + 16128*x*z + 16128*x*x;
   values[203] = -4032*x*y;
   values[204] = 4032*x*y - 2520*y + 2016*y*z + 6048*y*y;
   values[205] = 7560*x - 18144*x*y - 8064*x*z - 8064*x*x;
   values[206] = 2016*x*y;
   values[207] = 3528*y - 12096*x*y - 4032*y*z - 4032*y*y;
   values[208] = 12096*x*y - 14112*x + 16128*x*z + 24192*x*x;
   values[209] = -4032*x*y;
   values[210] = 12096*x*y - 4536*y + 4032*y*z + 8064*y*y;
   values[211] = 15624*x - 24192*x*y - 16128*x*z - 24192*x*x;
   values[212] = 4032*x*y;
   values[213] = 8064*x*y - 2016*y + 2016*y*z + 2016*y*y;
   values[214] = 8064*x - 6048*x*y - 8064*x*z - 16128*x*x;
   values[215] = 2016*x*y;
   values[216] = 48384*x*y - 56448*y + 96768*y*z + 64512*y*y;
   values[217] = 14112*x - 32256*x*y - 24192*x*z - 16128*x*x;
   values[218] = -24192*x*y;
   values[219] = 14112*y - 32256*x*y - 24192*y*z - 16128*y*y;
   values[220] = 48384*x*y - 56448*x + 96768*x*z + 64512*x*x;
   values[221] = -24192*x*y;
   values[222] = 14112*y - 32256*x*y - 24192*y*z - 16128*y*y;
   values[223] = 14112*x - 32256*x*y - 24192*x*z - 16128*x*x;
   values[224] = 16128*x*y;
   values[225] = 96768*y - 72576*x*y - 193536*y*z - 96768*y*y;
   values[226] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[227] = 48384*x*y;
   values[228] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[229] = 96768*x - 72576*x*y - 193536*x*z - 96768*x*x;
   values[230] = 48384*x*y;
   values[231] = 64512*x*y - 34272*y + 72576*y*z + 32256*y*y;
   values[232] = 64512*x*y - 34272*x + 72576*x*z + 32256*x*x;
   values[233] = -48384*x*y;
   values[234] = 64512*y - 48384*x*y - 96768*y*z - 96768*y*y;
   values[235] = 48384*x*y - 16128*x + 24192*x*z + 16128*x*x;
   values[236] = 24192*x*y;
   values[237] = 32256*x*y - 18144*y + 24192*y*z + 32256*y*y;
   values[238] = 62496*x - 96768*x*y - 96768*x*z - 64512*x*x;
   values[239] = 24192*x*y;
   values[240] = 32256*x*y - 16128*y + 24192*y*z + 24192*y*y;
   values[241] = 48384*x*y - 16128*x + 24192*x*z + 16128*x*x;
   values[242] = -16128*x*y;
   values[243] = 62496*y - 96768*x*y - 96768*y*z - 64512*y*y;
   values[244] = 32256*x*y - 18144*x + 24192*x*z + 32256*x*x;
   values[245] = 24192*x*y;
   values[246] = 48384*x*y - 16128*y + 24192*y*z + 16128*y*y;
   values[247] = 64512*x - 48384*x*y - 96768*x*z - 96768*x*x;
   values[248] = 24192*x*y;
   values[249] = 48384*x*y - 16128*y + 24192*y*z + 16128*y*y;
   values[250] = 32256*x*y - 16128*x + 24192*x*z + 24192*x*x;
   values[251] = -16128*x*y;
}
static void Curl_T_P4_3D_3D_D110(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 2688*x*y - 2520*y - 2520*z - 1680*x + 2688*x*z + 4032*y*z + 672*x*x + 2016*y*y + 2016*z*z + 720;
   values[1] = 1680*x + 840*y + 840*z - 2688*x*y - 2688*x*z - 1344*y*z - 2016*x*x - 672*y*y - 672*z*z - 240;
   values[2] = 1680*x + 840*y + 840*z - 2688*x*y - 2688*x*z - 1344*y*z - 2016*x*x - 672*y*y - 672*z*z - 240;
   values[3] = 16632*x + 17136*y + 17136*z - 24192*x*y - 24192*x*z - 24192*y*z - 8064*x*x - 12096*y*y - 12096*z*z - 5760;
   values[4] = 16128*x*y - 3528*y - 3528*z - 13104*x + 16128*x*z + 4032*y*z + 18144*x*x + 2016*y*y + 2016*z*z + 1440;
   values[5] = 16128*x*y - 3528*y - 3528*z - 13104*x + 16128*x*z + 4032*y*z + 18144*x*x + 2016*y*y + 2016*z*z + 1440;
   values[6] = 48384*x*y - 28728*y - 28728*z - 36288*x + 48384*x*z + 36288*y*z + 20160*x*x + 18144*y*y + 18144*z*z + 11016;
   values[7] = 24192*x + 4536*y + 4536*z - 24192*x*y - 24192*x*z - 4032*y*z - 36288*x*x - 2016*y*y - 2016*z*z - 2376;
   values[8] = 24192*x + 4536*y + 4536*z - 24192*x*y - 24192*x*z - 4032*y*z - 36288*x*x - 2016*y*y - 2016*z*z - 2376;
   values[9] = 21840*x + 14112*y + 14112*z - 26880*x*y - 26880*x*z - 16128*y*z - 13440*x*x - 8064*y*y - 8064*z*z - 6048;
   values[10] = 10752*x*y - 1848*y - 1848*z - 12768*x + 10752*x*z + 1344*y*z + 20160*x*x + 672*y*y + 672*z*z + 1176;
   values[11] = 10752*x*y - 1848*y - 1848*z - 12768*x + 10752*x*z + 1344*y*z + 20160*x*x + 672*y*y + 672*z*z + 1176;
   values[12] = 840*x + 1680*y + 840*z - 2688*x*y - 1344*x*z - 2688*y*z - 672*x*x - 2016*y*y - 672*z*z - 240;
   values[13] = 2688*x*y - 1680*y - 2520*z - 2520*x + 4032*x*z + 2688*y*z + 2016*x*x + 672*y*y + 2016*z*z + 720;
   values[14] = 840*x + 1680*y + 840*z - 2688*x*y - 1344*x*z - 2688*y*z - 672*x*x - 2016*y*y - 672*z*z - 240;
   values[15] = 16128*x*y - 13104*y - 3528*z - 3528*x + 4032*x*z + 16128*y*z + 2016*x*x + 18144*y*y + 2016*z*z + 1440;
   values[16] = 17136*x + 16632*y + 17136*z - 24192*x*y - 24192*x*z - 24192*y*z - 12096*x*x - 8064*y*y - 12096*z*z - 5760;
   values[17] = 16128*x*y - 13104*y - 3528*z - 3528*x + 4032*x*z + 16128*y*z + 2016*x*x + 18144*y*y + 2016*z*z + 1440;
   values[18] = 4536*x + 24192*y + 4536*z - 24192*x*y - 4032*x*z - 24192*y*z - 2016*x*x - 36288*y*y - 2016*z*z - 2376;
   values[19] = 48384*x*y - 36288*y - 28728*z - 28728*x + 36288*x*z + 48384*y*z + 18144*x*x + 20160*y*y + 18144*z*z + 11016;
   values[20] = 4536*x + 24192*y + 4536*z - 24192*x*y - 4032*x*z - 24192*y*z - 2016*x*x - 36288*y*y - 2016*z*z - 2376;
   values[21] = 10752*x*y - 12768*y - 1848*z - 1848*x + 1344*x*z + 10752*y*z + 672*x*x + 20160*y*y + 672*z*z + 1176;
   values[22] = 14112*x + 21840*y + 14112*z - 26880*x*y - 16128*x*z - 26880*y*z - 8064*x*x - 13440*y*y - 8064*z*z - 6048;
   values[23] = 10752*x*y - 12768*y - 1848*z - 1848*x + 1344*x*z + 10752*y*z + 672*x*x + 20160*y*y + 672*z*z + 1176;
   values[24] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[25] = 840*z - 1344*x*z - 1344*y*z - 1344*z*z;
   values[26] = 5376*x*y - 3360*y - 2520*z - 3360*x + 4032*x*z + 4032*y*z + 2688*x*x + 2688*y*y + 1344*z*z + 960;
   values[27] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[28] = 4032*x*z - 3528*z + 4032*y*z + 8064*z*z;
   values[29] = 11592*x + 11592*y + 17136*z - 16128*x*y - 24192*x*z - 24192*y*z - 8064*x*x - 8064*y*y - 12096*z*z - 3960;
   values[30] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[31] = 4536*z - 4032*x*z - 4032*y*z - 12096*z*z;
   values[32] = 16128*x*y - 13104*y - 28728*z - 13104*x + 36288*x*z + 36288*y*z + 8064*x*x + 8064*y*y + 24192*z*z + 5184;
   values[33] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[34] = 1344*x*z - 1848*z + 1344*y*z + 5376*z*z;
   values[35] = 4872*x + 4872*y + 14112*z - 5376*x*y - 16128*x*z - 16128*y*z - 2688*x*x - 2688*y*y - 13440*z*z - 2184;
   values[36] = 504*x - 672*x*x - 72;
   values[37] = 0;
   values[38] = 0;
   values[39] = 2016*y - 504*x - 8064*x*y + 2016*x*x - 72;
   values[40] = 6048*x*x - 3024*x + 216;
   values[41] = 0;
   values[42] = 16128*x*y - 1008*y - 504*x - 2016*x*x - 6048*y*y + 144;
   values[43] = 4032*x - 1008*y + 8064*x*y - 12096*x*x - 144;
   values[44] = 0;
   values[45] = 504*x - 1008*y - 8064*x*y + 672*x*x + 6048*y*y;
   values[46] = 504*y - 1008*x - 8064*x*y + 6048*x*x + 672*y*y;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = 0;
   values[58] = 0;
   values[59] = 0;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = 0;
   values[67] = 0;
   values[68] = 0;
   values[69] = 0;
   values[70] = 0;
   values[71] = 0;
   values[72] = 504*z - 2016*x*z;
   values[73] = 0;
   values[74] = 504*x - 1008*x*x - 36;
   values[75] = 4032*x*z - 504*z - 2016*z*z;
   values[76] = 0;
   values[77] = 504*z - 504*x - 4032*x*z + 2016*x*x;
   values[78] = 2016*z*z - 2016*x*z;
   values[79] = 0;
   values[80] = 4032*x*z - 1008*x*x - 1008*z*z;
   values[81] = 4032*x*z - 8064*y*z;
   values[82] = 12096*x*z - 1512*z;
   values[83] = 1008*y - 8064*x*y + 2016*x*x - 72;
   values[84] = 8064*y*z - 4032*x*z - 504*z + 2016*z*z;
   values[85] = 6048*z*z - 12096*x*z;
   values[86] = 504*z - 504*x + 8064*x*y + 4032*x*z - 8064*y*z - 2016*x*x;
   values[87] = 8064*y*z - 2016*x*z - 504*z;
   values[88] = 504*z - 12096*x*z + 4032*y*z;
   values[89] = 504*y - 504*x + 8064*x*y - 1008*x*x - 3024*y*y;
   values[90] = 504*z - 2016*x*z;
   values[91] = 0;
   values[92] = 4032*x*x - 2016*x + 144;
   values[93] = 4032*x*z - 4032*z*z;
   values[94] = 0;
   values[95] = 2520*x - 1512*z + 12096*x*z - 8064*x*x - 72;
   values[96] = 4032*z*z - 2016*x*z - 504*z;
   values[97] = 0;
   values[98] = 504*z - 504*x - 12096*x*z + 4032*x*x + 2016*z*z;
   values[99] = 4032*x*z - 504*z - 4032*y*z;
   values[100] = 504*z - 4032*x*z;
   values[101] = 2016*x - 2016*y + 16128*x*y - 8064*x*x;
   values[102] = 4032*y*z - 4032*x*z - 504*z + 4032*z*z;
   values[103] = 504*z + 4032*x*z - 4032*z*z;
   values[104] = 504*y - 504*x - 16128*x*y - 12096*x*z + 12096*y*z + 8064*x*x;
   values[105] = 4032*y*z - 2016*x*z;
   values[106] = 4032*x*z - 2016*y*z;
   values[107] = 4032*x*x - 16128*x*y + 4032*y*y;
   values[108] = 2016*x*z - 1512*z + 4032*y*z + 2016*z*z;
   values[109] = 4536*z - 6048*x*z - 4032*y*z - 6048*z*z;
   values[110] = 4032*x*y - 3024*y - 3024*z - 1512*x + 4032*x*z + 8064*y*z + 1008*x*x + 3024*y*y + 3024*z*z + 540;
   values[111] = 3528*z - 4032*x*z - 8064*y*z - 6048*z*z;
   values[112] = 12096*x*z - 10584*z + 8064*y*z + 18144*z*z;
   values[113] = 3528*x + 7056*y + 10584*z - 8064*x*y - 12096*x*z - 24192*y*z - 2016*x*x - 6048*y*y - 12096*z*z - 1512;
   values[114] = 2016*x*z - 2016*z + 4032*y*z + 4032*z*z;
   values[115] = 6048*z - 6048*x*z - 4032*y*z - 12096*z*z;
   values[116] = 4032*x*y - 4032*y - 8064*z - 2016*x + 8064*x*z + 16128*y*z + 1008*x*x + 3024*y*y + 10080*z*z + 1008;
   values[117] = 4032*z - 4032*x*z - 16128*y*z - 4032*z*z;
   values[118] = 24192*x*z - 20160*z + 24192*y*z + 24192*z*z;
   values[119] = 4032*x + 15120*y + 8064*z - 16128*x*y - 8064*x*z - 32256*y*z - 2016*x*x - 18144*y*y - 6048*z*z - 1944;
   values[120] = 4032*x*z - 4536*z + 16128*y*z + 6048*z*z;
   values[121] = 23184*z - 24192*x*z - 24192*y*z - 36288*z*z;
   values[122] = 16128*x*y - 17136*y - 13608*z - 4536*x + 12096*x*z + 48384*y*z + 2016*x*x + 18144*y*y + 12096*z*z + 2520;
   values[123] = 2016*x*z - 2520*z + 12096*y*z + 2016*z*z;
   values[124] = 16632*z - 18144*x*z - 24192*y*z - 18144*z*z;
   values[125] = 12096*x*y - 13608*y - 5040*z - 2520*x + 4032*x*z + 24192*y*z + 1008*x*x + 18144*y*y + 3024*z*z + 1512;
   values[126] = 2016*x*z - 1512*z + 4032*y*z + 2016*z*z;
   values[127] = 4032*x*z - 3024*z + 6048*y*z + 4032*z*z;
   values[128] = 6048*x + 12096*y + 4536*z - 16128*x*y - 6048*x*z - 12096*y*z - 4032*x*x - 12096*y*y - 2016*z*z - 2160;
   values[129] = 4032*z - 4032*x*z - 8064*y*z - 8064*z*z;
   values[130] = 8064*z - 8064*x*z - 12096*y*z - 16128*z*z;
   values[131] = 32256*x*y - 27216*y - 20160*z - 13608*x + 24192*x*z + 48384*y*z + 8064*x*x + 24192*y*y + 12096*z*z + 5616;
   values[132] = 2016*x*z - 2520*z + 4032*y*z + 6048*z*z;
   values[133] = 4032*x*z - 5040*z + 6048*y*z + 12096*z*z;
   values[134] = 7560*x + 15120*y + 16632*z - 16128*x*y - 18144*x*z - 36288*y*z - 4032*x*x - 12096*y*y - 12096*z*z - 3528;
   values[135] = 3528*z - 4032*x*z - 12096*y*z - 4032*z*z;
   values[136] = 10584*z - 12096*x*z - 24192*y*z - 12096*z*z;
   values[137] = 48384*x*y - 42336*y - 10584*z - 14112*x + 12096*x*z + 36288*y*z + 8064*x*x + 48384*y*y + 4032*z*z + 6048;
   values[138] = 4032*x*z - 4536*z + 12096*y*z + 8064*z*z;
   values[139] = 12096*x*z - 13608*z + 24192*y*z + 24192*z*z;
   values[140] = 15624*x + 46872*y + 23184*z - 48384*x*y - 24192*x*z - 72576*y*z - 8064*x*x - 48384*y*y - 12096*z*z - 7560;
   values[141] = 2016*x*z - 2016*z + 8064*y*z + 2016*z*z;
   values[142] = 8064*x*z - 8064*z + 20160*y*z + 8064*z*z;
   values[143] = 8064*x + 32256*y + 6048*z - 32256*x*y - 6048*x*z - 24192*y*z - 4032*x*x - 40320*y*y - 2016*z*z - 4032;
   values[144] = 4536*z - 4032*x*z - 6048*y*z - 6048*z*z;
   values[145] = 4032*x*z - 1512*z + 2016*y*z + 2016*z*z;
   values[146] = 4032*x*y - 1512*y - 3024*z - 3024*x + 8064*x*z + 4032*y*z + 3024*x*x + 1008*y*y + 3024*z*z + 540;
   values[147] = 8064*x*z - 10584*z + 12096*y*z + 18144*z*z;
   values[148] = 3528*z - 8064*x*z - 4032*y*z - 6048*z*z;
   values[149] = 7056*x + 3528*y + 10584*z - 8064*x*y - 24192*x*z - 12096*y*z - 6048*x*x - 2016*y*y - 12096*z*z - 1512;
   values[150] = 6048*z - 4032*x*z - 6048*y*z - 12096*z*z;
   values[151] = 4032*x*z - 2016*z + 2016*y*z + 4032*z*z;
   values[152] = 4032*x*y - 2016*y - 8064*z - 4032*x + 16128*x*z + 8064*y*z + 3024*x*x + 1008*y*y + 10080*z*z + 1008;
   values[153] = 24192*x*z - 20160*z + 24192*y*z + 24192*z*z;
   values[154] = 4032*z - 16128*x*z - 4032*y*z - 4032*z*z;
   values[155] = 15120*x + 4032*y + 8064*z - 16128*x*y - 32256*x*z - 8064*y*z - 18144*x*x - 2016*y*y - 6048*z*z - 1944;
   values[156] = 23184*z - 24192*x*z - 24192*y*z - 36288*z*z;
   values[157] = 16128*x*z - 4536*z + 4032*y*z + 6048*z*z;
   values[158] = 16128*x*y - 4536*y - 13608*z - 17136*x + 48384*x*z + 12096*y*z + 18144*x*x + 2016*y*y + 12096*z*z + 2520;
   values[159] = 16632*z - 24192*x*z - 18144*y*z - 18144*z*z;
   values[160] = 12096*x*z - 2520*z + 2016*y*z + 2016*z*z;
   values[161] = 12096*x*y - 2520*y - 5040*z - 13608*x + 24192*x*z + 4032*y*z + 18144*x*x + 1008*y*y + 3024*z*z + 1512;
   values[162] = 6048*x*z - 3024*z + 4032*y*z + 4032*z*z;
   values[163] = 4032*x*z - 1512*z + 2016*y*z + 2016*z*z;
   values[164] = 12096*x + 6048*y + 4536*z - 16128*x*y - 12096*x*z - 6048*y*z - 12096*x*x - 4032*y*y - 2016*z*z - 2160;
   values[165] = 8064*z - 12096*x*z - 8064*y*z - 16128*z*z;
   values[166] = 4032*z - 8064*x*z - 4032*y*z - 8064*z*z;
   values[167] = 32256*x*y - 13608*y - 20160*z - 27216*x + 48384*x*z + 24192*y*z + 24192*x*x + 8064*y*y + 12096*z*z + 5616;
   values[168] = 6048*x*z - 5040*z + 4032*y*z + 12096*z*z;
   values[169] = 4032*x*z - 2520*z + 2016*y*z + 6048*z*z;
   values[170] = 15120*x + 7560*y + 16632*z - 16128*x*y - 36288*x*z - 18144*y*z - 12096*x*x - 4032*y*y - 12096*z*z - 3528;
   values[171] = 10584*z - 24192*x*z - 12096*y*z - 12096*z*z;
   values[172] = 3528*z - 12096*x*z - 4032*y*z - 4032*z*z;
   values[173] = 48384*x*y - 14112*y - 10584*z - 42336*x + 36288*x*z + 12096*y*z + 48384*x*x + 8064*y*y + 4032*z*z + 6048;
   values[174] = 24192*x*z - 13608*z + 12096*y*z + 24192*z*z;
   values[175] = 12096*x*z - 4536*z + 4032*y*z + 8064*z*z;
   values[176] = 46872*x + 15624*y + 23184*z - 48384*x*y - 72576*x*z - 24192*y*z - 48384*x*x - 8064*y*y - 12096*z*z - 7560;
   values[177] = 20160*x*z - 8064*z + 8064*y*z + 8064*z*z;
   values[178] = 8064*x*z - 2016*z + 2016*y*z + 2016*z*z;
   values[179] = 32256*x + 8064*y + 6048*z - 32256*x*y - 24192*x*z - 6048*y*z - 40320*x*x - 4032*y*y - 2016*z*z - 4032;
   values[180] = 3024*x + 9072*y + 4536*z - 8064*x*y - 4032*x*z - 12096*y*z - 1008*x*x - 9072*y*y - 3024*z*z - 1620;
   values[181] = 12096*x*y - 4536*y - 3024*z - 6048*x + 8064*x*z + 6048*y*z + 6048*x*x + 4032*y*y + 2016*z*z + 1080;
   values[182] = 8064*x*y - 3024*y - 1512*z - 3024*x + 4032*x*z + 4032*y*z + 3024*x*x + 3024*y*y + 1008*z*z + 540;
   values[183] = 24192*x*y - 31752*y - 10584*z - 7056*x + 8064*x*z + 36288*y*z + 2016*x*x + 36288*y*y + 6048*z*z + 4536;
   values[184] = 21168*x + 21168*y + 10584*z - 48384*x*y - 24192*x*z - 24192*y*z - 18144*x*x - 20160*y*y - 6048*z*z - 4536;
   values[185] = 7056*x + 10584*y + 3528*z - 24192*x*y - 8064*x*z - 12096*y*z - 6048*x*x - 12096*y*y - 2016*z*z - 1512;
   values[186] = 4032*x + 24192*y + 6048*z - 16128*x*y - 4032*x*z - 24192*y*z - 1008*x*x - 30240*y*y - 3024*z*z - 3024;
   values[187] = 40320*x*y - 20160*y - 8064*z - 16128*x + 16128*x*z + 20160*y*z + 12096*x*x + 20160*y*y + 4032*z*z + 4032;
   values[188] = 16128*x*y - 8064*y - 2016*z - 4032*x + 4032*x*z + 8064*y*z + 3024*x*x + 10080*y*y + 1008*z*z + 1008;
   values[189] = 48384*x*y - 40320*y - 20160*z - 19656*x + 24192*x*z + 48384*y*z + 8064*x*x + 36288*y*y + 12096*z*z + 8208;
   values[190] = 30240*x + 12096*y + 8064*z - 48384*x*y - 32256*x*z - 12096*y*z - 36288*x*x - 8064*y*y - 4032*z*z - 3888;
   values[191] = 15120*x + 8064*y + 4032*z - 32256*x*y - 16128*x*z - 8064*y*z - 18144*x*x - 6048*y*y - 2016*z*z - 1944;
   values[192] = 22680*x + 69552*y + 23184*z - 72576*x*y - 24192*x*z - 72576*y*z - 8064*x*x - 72576*y*y - 12096*z*z - 11088;
   values[193] = 96768*x*y - 27216*y - 13608*z - 51408*x + 48384*x*z + 24192*y*z + 54432*x*x + 20160*y*y + 6048*z*z + 7560;
   values[194] = 48384*x*y - 13608*y - 4536*z - 17136*x + 16128*x*z + 12096*y*z + 18144*x*x + 12096*y*y + 2016*z*z + 2520;
   values[195] = 21168*x + 33264*y + 16632*z - 48384*x*y - 24192*x*z - 36288*y*z - 10080*x*x - 27216*y*y - 9072*z*z - 7560;
   values[196] = 36288*x*y - 7560*y - 5040*z - 27216*x + 24192*x*z + 6048*y*z + 36288*x*x + 4032*y*y + 2016*z*z + 3024;
   values[197] = 24192*x*y - 5040*y - 2520*z - 13608*x + 12096*x*z + 4032*y*z + 18144*x*x + 3024*y*y + 1008*z*z + 1512;
   values[198] = 12096*x*y - 6048*y - 3024*z - 4536*x + 6048*x*z + 8064*y*z + 4032*x*x + 6048*y*y + 2016*z*z + 1080;
   values[199] = 9072*x + 3024*y + 4536*z - 8064*x*y - 12096*x*z - 4032*y*z - 9072*x*x - 1008*y*y - 3024*z*z - 1620;
   values[200] = 8064*x*y - 3024*y - 1512*z - 3024*x + 4032*x*z + 4032*y*z + 3024*x*x + 3024*y*y + 1008*z*z + 540;
   values[201] = 12096*x + 30240*y + 8064*z - 48384*x*y - 12096*x*z - 32256*y*z - 8064*x*x - 36288*y*y - 4032*z*z - 3888;
   values[202] = 48384*x*y - 19656*y - 20160*z - 40320*x + 48384*x*z + 24192*y*z + 36288*x*x + 8064*y*y + 12096*z*z + 8208;
   values[203] = 8064*x + 15120*y + 4032*z - 32256*x*y - 8064*x*z - 16128*y*z - 6048*x*x - 18144*y*y - 2016*z*z - 1944;
   values[204] = 36288*x*y - 27216*y - 5040*z - 7560*x + 6048*x*z + 24192*y*z + 4032*x*x + 36288*y*y + 2016*z*z + 3024;
   values[205] = 33264*x + 21168*y + 16632*z - 48384*x*y - 36288*x*z - 24192*y*z - 27216*x*x - 10080*y*y - 9072*z*z - 7560;
   values[206] = 24192*x*y - 13608*y - 2520*z - 5040*x + 4032*x*z + 12096*y*z + 3024*x*x + 18144*y*y + 1008*z*z + 1512;
   values[207] = 21168*x + 21168*y + 10584*z - 48384*x*y - 24192*x*z - 24192*y*z - 20160*x*x - 18144*y*y - 6048*z*z - 4536;
   values[208] = 24192*x*y - 7056*y - 10584*z - 31752*x + 36288*x*z + 8064*y*z + 36288*x*x + 2016*y*y + 6048*z*z + 4536;
   values[209] = 10584*x + 7056*y + 3528*z - 24192*x*y - 12096*x*z - 8064*y*z - 12096*x*x - 6048*y*y - 2016*z*z - 1512;
   values[210] = 96768*x*y - 51408*y - 13608*z - 27216*x + 24192*x*z + 48384*y*z + 20160*x*x + 54432*y*y + 6048*z*z + 7560;
   values[211] = 69552*x + 22680*y + 23184*z - 72576*x*y - 72576*x*z - 24192*y*z - 72576*x*x - 8064*y*y - 12096*z*z - 11088;
   values[212] = 48384*x*y - 17136*y - 4536*z - 13608*x + 12096*x*z + 16128*y*z + 12096*x*x + 18144*y*y + 2016*z*z + 2520;
   values[213] = 40320*x*y - 16128*y - 8064*z - 20160*x + 20160*x*z + 16128*y*z + 20160*x*x + 12096*y*y + 4032*z*z + 4032;
   values[214] = 24192*x + 4032*y + 6048*z - 16128*x*y - 24192*x*z - 4032*y*z - 30240*x*x - 1008*y*y - 3024*z*z - 3024;
   values[215] = 16128*x*y - 4032*y - 2016*z - 8064*x + 8064*x*z + 4032*y*z + 10080*x*x + 3024*y*y + 1008*z*z + 1008;
   values[216] = 16128*x*z - 21168*z + 48384*y*z + 24192*z*z;
   values[217] = 14112*z - 32256*x*z - 24192*y*z - 16128*z*z;
   values[218] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[219] = 14112*z - 24192*x*z - 32256*y*z - 16128*z*z;
   values[220] = 48384*x*z - 21168*z + 16128*y*z + 24192*z*z;
   values[221] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[222] = 14112*z - 24192*x*z - 32256*y*z - 16128*z*z;
   values[223] = 14112*z - 32256*x*z - 24192*y*z - 16128*z*z;
   values[224] = 129024*x*y - 56448*y - 21168*z - 56448*x + 48384*x*z + 48384*y*z + 48384*x*x + 48384*y*y + 8064*z*z + 12096;
   values[225] = 24192*z - 16128*x*z - 48384*y*z - 36288*z*z;
   values[226] = 32256*x*z - 16128*z + 24192*y*z + 24192*z*z;
   values[227] = 32256*x*y - 16128*y - 24192*z - 16128*x + 48384*x*z + 48384*y*z + 12096*x*x + 12096*y*y + 24192*z*z + 4032;
   values[228] = 24192*x*z - 16128*z + 32256*y*z + 24192*z*z;
   values[229] = 24192*z - 48384*x*z - 16128*y*z - 36288*z*z;
   values[230] = 32256*x*y - 16128*y - 24192*z - 16128*x + 48384*x*z + 48384*y*z + 12096*x*x + 12096*y*y + 24192*z*z + 4032;
   values[231] = 24192*x*z - 18144*z + 32256*y*z + 32256*z*z;
   values[232] = 32256*x*z - 18144*z + 24192*y*z + 32256*z*z;
   values[233] = 62496*x + 62496*y + 46368*z - 129024*x*y - 96768*x*z - 96768*y*z - 48384*x*x - 48384*y*y - 24192*z*z - 15120;
   values[234] = 24192*z - 16128*x*z - 72576*y*z - 24192*z*z;
   values[235] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[236] = 48384*x*y - 24192*y - 16128*z - 16128*x + 32256*x*z + 48384*y*z + 12096*x*x + 24192*y*y + 12096*z*z + 4032;
   values[237] = 24192*x*z - 18144*z + 64512*y*z + 16128*z*z;
   values[238] = 46368*z - 96768*x*z - 48384*y*z - 48384*z*z;
   values[239] = 64512*x*y - 34272*y - 18144*z - 18144*x + 32256*x*z + 64512*y*z + 12096*x*x + 36288*y*y + 12096*z*z + 5040;
   values[240] = 24192*x*z - 16128*z + 48384*y*z + 16128*z*z;
   values[241] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[242] = 64512*x + 96768*y + 24192*z - 193536*x*y - 48384*x*z - 72576*y*z - 48384*x*x - 96768*y*y - 8064*z*z - 16128;
   values[243] = 46368*z - 48384*x*z - 96768*y*z - 48384*z*z;
   values[244] = 64512*x*z - 18144*z + 24192*y*z + 16128*z*z;
   values[245] = 64512*x*y - 18144*y - 18144*z - 34272*x + 64512*x*z + 32256*y*z + 36288*x*x + 12096*y*y + 12096*z*z + 5040;
   values[246] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[247] = 24192*z - 72576*x*z - 16128*y*z - 24192*z*z;
   values[248] = 48384*x*y - 16128*y - 16128*z - 24192*x + 48384*x*z + 32256*y*z + 24192*x*x + 12096*y*y + 12096*z*z + 4032;
   values[249] = 48384*x*z - 24192*z + 48384*y*z + 24192*z*z;
   values[250] = 48384*x*z - 16128*z + 24192*y*z + 16128*z*z;
   values[251] = 96768*x + 64512*y + 24192*z - 193536*x*y - 72576*x*z - 48384*y*z - 96768*x*x - 48384*y*y - 8064*z*z - 16128;
}
static void Curl_T_P4_3D_3D_D101(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 2688*x*y - 2520*y - 2520*z - 1680*x + 2688*x*z + 4032*y*z + 672*x*x + 2016*y*y + 2016*z*z + 720;
   values[1] = 1680*x + 840*y + 840*z - 2688*x*y - 2688*x*z - 1344*y*z - 2016*x*x - 672*y*y - 672*z*z - 240;
   values[2] = 1680*x + 840*y + 840*z - 2688*x*y - 2688*x*z - 1344*y*z - 2016*x*x - 672*y*y - 672*z*z - 240;
   values[3] = 16632*x + 17136*y + 17136*z - 24192*x*y - 24192*x*z - 24192*y*z - 8064*x*x - 12096*y*y - 12096*z*z - 5760;
   values[4] = 16128*x*y - 3528*y - 3528*z - 13104*x + 16128*x*z + 4032*y*z + 18144*x*x + 2016*y*y + 2016*z*z + 1440;
   values[5] = 16128*x*y - 3528*y - 3528*z - 13104*x + 16128*x*z + 4032*y*z + 18144*x*x + 2016*y*y + 2016*z*z + 1440;
   values[6] = 48384*x*y - 28728*y - 28728*z - 36288*x + 48384*x*z + 36288*y*z + 20160*x*x + 18144*y*y + 18144*z*z + 11016;
   values[7] = 24192*x + 4536*y + 4536*z - 24192*x*y - 24192*x*z - 4032*y*z - 36288*x*x - 2016*y*y - 2016*z*z - 2376;
   values[8] = 24192*x + 4536*y + 4536*z - 24192*x*y - 24192*x*z - 4032*y*z - 36288*x*x - 2016*y*y - 2016*z*z - 2376;
   values[9] = 21840*x + 14112*y + 14112*z - 26880*x*y - 26880*x*z - 16128*y*z - 13440*x*x - 8064*y*y - 8064*z*z - 6048;
   values[10] = 10752*x*y - 1848*y - 1848*z - 12768*x + 10752*x*z + 1344*y*z + 20160*x*x + 672*y*y + 672*z*z + 1176;
   values[11] = 10752*x*y - 1848*y - 1848*z - 12768*x + 10752*x*z + 1344*y*z + 20160*x*x + 672*y*y + 672*z*z + 1176;
   values[12] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[13] = 4032*x*y - 2520*y - 3360*z - 3360*x + 5376*x*z + 4032*y*z + 2688*x*x + 1344*y*y + 2688*z*z + 960;
   values[14] = 840*y - 1344*x*y - 1344*y*z - 1344*y*y;
   values[15] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[16] = 11592*x + 17136*y + 11592*z - 24192*x*y - 16128*x*z - 24192*y*z - 8064*x*x - 12096*y*y - 8064*z*z - 3960;
   values[17] = 4032*x*y - 3528*y + 4032*y*z + 8064*y*y;
   values[18] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[19] = 36288*x*y - 28728*y - 13104*z - 13104*x + 16128*x*z + 36288*y*z + 8064*x*x + 24192*y*y + 8064*z*z + 5184;
   values[20] = 4536*y - 4032*x*y - 4032*y*z - 12096*y*y;
   values[21] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[22] = 4872*x + 14112*y + 4872*z - 16128*x*y - 5376*x*z - 16128*y*z - 2688*x*x - 13440*y*y - 2688*z*z - 2184;
   values[23] = 1344*x*y - 1848*y + 1344*y*z + 5376*y*y;
   values[24] = 840*x + 840*y + 1680*z - 1344*x*y - 2688*x*z - 2688*y*z - 672*x*x - 672*y*y - 2016*z*z - 240;
   values[25] = 840*x + 840*y + 1680*z - 1344*x*y - 2688*x*z - 2688*y*z - 672*x*x - 672*y*y - 2016*z*z - 240;
   values[26] = 4032*x*y - 2520*y - 1680*z - 2520*x + 2688*x*z + 2688*y*z + 2016*x*x + 2016*y*y + 672*z*z + 720;
   values[27] = 4032*x*y - 3528*y - 13104*z - 3528*x + 16128*x*z + 16128*y*z + 2016*x*x + 2016*y*y + 18144*z*z + 1440;
   values[28] = 4032*x*y - 3528*y - 13104*z - 3528*x + 16128*x*z + 16128*y*z + 2016*x*x + 2016*y*y + 18144*z*z + 1440;
   values[29] = 17136*x + 17136*y + 16632*z - 24192*x*y - 24192*x*z - 24192*y*z - 12096*x*x - 12096*y*y - 8064*z*z - 5760;
   values[30] = 4536*x + 4536*y + 24192*z - 4032*x*y - 24192*x*z - 24192*y*z - 2016*x*x - 2016*y*y - 36288*z*z - 2376;
   values[31] = 4536*x + 4536*y + 24192*z - 4032*x*y - 24192*x*z - 24192*y*z - 2016*x*x - 2016*y*y - 36288*z*z - 2376;
   values[32] = 36288*x*y - 28728*y - 36288*z - 28728*x + 48384*x*z + 48384*y*z + 18144*x*x + 18144*y*y + 20160*z*z + 11016;
   values[33] = 1344*x*y - 1848*y - 12768*z - 1848*x + 10752*x*z + 10752*y*z + 672*x*x + 672*y*y + 20160*z*z + 1176;
   values[34] = 1344*x*y - 1848*y - 12768*z - 1848*x + 10752*x*z + 10752*y*z + 672*x*x + 672*y*y + 20160*z*z + 1176;
   values[35] = 14112*x + 14112*y + 21840*z - 16128*x*y - 26880*x*z - 26880*y*z - 8064*x*x - 8064*y*y - 13440*z*z - 6048;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 504*x - 672*x*x - 72;
   values[49] = 0;
   values[50] = 0;
   values[51] = 2016*z - 504*x - 8064*x*z + 2016*x*x - 72;
   values[52] = 0;
   values[53] = 6048*x*x - 3024*x + 216;
   values[54] = 16128*x*z - 1008*z - 504*x - 2016*x*x - 6048*z*z + 144;
   values[55] = 0;
   values[56] = 4032*x - 1008*z + 8064*x*z - 12096*x*x - 144;
   values[57] = 504*x - 1008*z - 8064*x*z + 672*x*x + 6048*z*z;
   values[58] = 0;
   values[59] = 504*z - 1008*x - 8064*x*z + 6048*x*x + 672*z*z;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = 0;
   values[67] = 0;
   values[68] = 0;
   values[69] = 0;
   values[70] = 0;
   values[71] = 0;
   values[72] = 504*y - 2016*x*y;
   values[73] = 4032*x*x - 2016*x + 144;
   values[74] = 0;
   values[75] = 4032*x*y - 504*y - 4032*y*z;
   values[76] = 2016*x - 2016*z + 16128*x*z - 8064*x*x;
   values[77] = 504*y - 4032*x*y;
   values[78] = 4032*y*z - 2016*x*y;
   values[79] = 4032*x*x - 16128*x*z + 4032*z*z;
   values[80] = 4032*x*y - 2016*y*z;
   values[81] = 4032*x*y - 4032*y*y;
   values[82] = 2520*x - 1512*y + 12096*x*y - 8064*x*x - 72;
   values[83] = 0;
   values[84] = 4032*y*z - 4032*x*y - 504*y + 4032*y*y;
   values[85] = 504*z - 504*x - 12096*x*y - 16128*x*z + 12096*y*z + 8064*x*x;
   values[86] = 504*y + 4032*x*y - 4032*y*y;
   values[87] = 4032*y*y - 2016*x*y - 504*y;
   values[88] = 504*y - 504*x - 12096*x*y + 4032*x*x + 2016*y*y;
   values[89] = 0;
   values[90] = 504*y - 2016*x*y;
   values[91] = 504*x - 1008*x*x - 36;
   values[92] = 0;
   values[93] = 4032*x*y - 8064*y*z;
   values[94] = 1008*z - 8064*x*z + 2016*x*x - 72;
   values[95] = 12096*x*y - 1512*y;
   values[96] = 8064*y*z - 2016*x*y - 504*y;
   values[97] = 504*z - 504*x + 8064*x*z - 1008*x*x - 3024*z*z;
   values[98] = 504*y - 12096*x*y + 4032*y*z;
   values[99] = 4032*x*y - 504*y - 2016*y*y;
   values[100] = 504*y - 504*x - 4032*x*y + 2016*x*x;
   values[101] = 0;
   values[102] = 8064*y*z - 4032*x*y - 504*y + 2016*y*y;
   values[103] = 504*y - 504*x + 4032*x*y + 8064*x*z - 8064*y*z - 2016*x*x;
   values[104] = 6048*y*y - 12096*x*y;
   values[105] = 2016*y*y - 2016*x*y;
   values[106] = 4032*x*y - 1008*x*x - 1008*y*y;
   values[107] = 0;
   values[108] = 2016*x*y - 1512*y + 4032*y*z + 2016*y*y;
   values[109] = 6048*x + 4536*y + 12096*z - 6048*x*y - 16128*x*z - 12096*y*z - 4032*x*x - 2016*y*y - 12096*z*z - 2160;
   values[110] = 4032*x*y - 3024*y + 6048*y*z + 4032*y*y;
   values[111] = 3528*y - 4032*x*y - 12096*y*z - 4032*y*y;
   values[112] = 12096*x*y - 10584*y - 42336*z - 14112*x + 48384*x*z + 36288*y*z + 8064*x*x + 4032*y*y + 48384*z*z + 6048;
   values[113] = 10584*y - 12096*x*y - 24192*y*z - 12096*y*y;
   values[114] = 2016*x*y - 2016*y + 8064*y*z + 2016*y*y;
   values[115] = 8064*x + 6048*y + 32256*z - 6048*x*y - 32256*x*z - 24192*y*z - 4032*x*x - 2016*y*y - 40320*z*z - 4032;
   values[116] = 8064*x*y - 8064*y + 20160*y*z + 8064*y*y;
   values[117] = 4032*y - 4032*x*y - 8064*y*z - 8064*y*y;
   values[118] = 24192*x*y - 20160*y - 27216*z - 13608*x + 32256*x*z + 48384*y*z + 8064*x*x + 12096*y*y + 24192*z*z + 5616;
   values[119] = 8064*y - 8064*x*y - 12096*y*z - 16128*y*y;
   values[120] = 4032*x*y - 4536*y + 12096*y*z + 8064*y*y;
   values[121] = 15624*x + 23184*y + 46872*z - 24192*x*y - 48384*x*z - 72576*y*z - 8064*x*x - 12096*y*y - 48384*z*z - 7560;
   values[122] = 12096*x*y - 13608*y + 24192*y*z + 24192*y*y;
   values[123] = 2016*x*y - 2520*y + 4032*y*z + 6048*y*y;
   values[124] = 7560*x + 16632*y + 15120*z - 18144*x*y - 16128*x*z - 36288*y*z - 4032*x*x - 12096*y*y - 12096*z*z - 3528;
   values[125] = 4032*x*y - 5040*y + 6048*y*z + 12096*y*y;
   values[126] = 2016*x*y - 1512*y + 4032*y*z + 2016*y*y;
   values[127] = 4032*x*y - 3024*y - 3024*z - 1512*x + 4032*x*z + 8064*y*z + 1008*x*x + 3024*y*y + 3024*z*z + 540;
   values[128] = 4536*y - 6048*x*y - 4032*y*z - 6048*y*y;
   values[129] = 4032*y - 4032*x*y - 16128*y*z - 4032*y*y;
   values[130] = 4032*x + 8064*y + 15120*z - 8064*x*y - 16128*x*z - 32256*y*z - 2016*x*x - 6048*y*y - 18144*z*z - 1944;
   values[131] = 24192*x*y - 20160*y + 24192*y*z + 24192*y*y;
   values[132] = 2016*x*y - 2520*y + 12096*y*z + 2016*y*y;
   values[133] = 4032*x*y - 5040*y - 13608*z - 2520*x + 12096*x*z + 24192*y*z + 1008*x*x + 3024*y*y + 18144*z*z + 1512;
   values[134] = 16632*y - 18144*x*y - 24192*y*z - 18144*y*y;
   values[135] = 3528*y - 4032*x*y - 8064*y*z - 6048*y*y;
   values[136] = 3528*x + 10584*y + 7056*z - 12096*x*y - 8064*x*z - 24192*y*z - 2016*x*x - 12096*y*y - 6048*z*z - 1512;
   values[137] = 12096*x*y - 10584*y + 8064*y*z + 18144*y*y;
   values[138] = 4032*x*y - 4536*y + 16128*y*z + 6048*y*y;
   values[139] = 12096*x*y - 13608*y - 17136*z - 4536*x + 16128*x*z + 48384*y*z + 2016*x*x + 12096*y*y + 18144*z*z + 2520;
   values[140] = 23184*y - 24192*x*y - 24192*y*z - 36288*y*y;
   values[141] = 2016*x*y - 2016*y + 4032*y*z + 4032*y*y;
   values[142] = 8064*x*y - 8064*y - 4032*z - 2016*x + 4032*x*z + 16128*y*z + 1008*x*x + 10080*y*y + 3024*z*z + 1008;
   values[143] = 6048*y - 6048*x*y - 4032*y*z - 12096*y*y;
   values[144] = 3024*x + 4536*y + 9072*z - 4032*x*y - 8064*x*z - 12096*y*z - 1008*x*x - 3024*y*y - 9072*z*z - 1620;
   values[145] = 4032*x*y - 1512*y - 3024*z - 3024*x + 8064*x*z + 4032*y*z + 3024*x*x + 1008*y*y + 3024*z*z + 540;
   values[146] = 8064*x*y - 3024*y - 4536*z - 6048*x + 12096*x*z + 6048*y*z + 6048*x*x + 2016*y*y + 4032*z*z + 1080;
   values[147] = 8064*x*y - 10584*y - 31752*z - 7056*x + 24192*x*z + 36288*y*z + 2016*x*x + 6048*y*y + 36288*z*z + 4536;
   values[148] = 7056*x + 3528*y + 10584*z - 8064*x*y - 24192*x*z - 12096*y*z - 6048*x*x - 2016*y*y - 12096*z*z - 1512;
   values[149] = 21168*x + 10584*y + 21168*z - 24192*x*y - 48384*x*z - 24192*y*z - 18144*x*x - 6048*y*y - 20160*z*z - 4536;
   values[150] = 4032*x + 6048*y + 24192*z - 4032*x*y - 16128*x*z - 24192*y*z - 1008*x*x - 3024*y*y - 30240*z*z - 3024;
   values[151] = 4032*x*y - 2016*y - 8064*z - 4032*x + 16128*x*z + 8064*y*z + 3024*x*x + 1008*y*y + 10080*z*z + 1008;
   values[152] = 16128*x*y - 8064*y - 20160*z - 16128*x + 40320*x*z + 20160*y*z + 12096*x*x + 4032*y*y + 20160*z*z + 4032;
   values[153] = 24192*x*y - 20160*y - 40320*z - 19656*x + 48384*x*z + 48384*y*z + 8064*x*x + 12096*y*y + 36288*z*z + 8208;
   values[154] = 15120*x + 4032*y + 8064*z - 16128*x*y - 32256*x*z - 8064*y*z - 18144*x*x - 2016*y*y - 6048*z*z - 1944;
   values[155] = 30240*x + 8064*y + 12096*z - 32256*x*y - 48384*x*z - 12096*y*z - 36288*x*x - 4032*y*y - 8064*z*z - 3888;
   values[156] = 22680*x + 23184*y + 69552*z - 24192*x*y - 72576*x*z - 72576*y*z - 8064*x*x - 12096*y*y - 72576*z*z - 11088;
   values[157] = 16128*x*y - 4536*y - 13608*z - 17136*x + 48384*x*z + 12096*y*z + 18144*x*x + 2016*y*y + 12096*z*z + 2520;
   values[158] = 48384*x*y - 13608*y - 27216*z - 51408*x + 96768*x*z + 24192*y*z + 54432*x*x + 6048*y*y + 20160*z*z + 7560;
   values[159] = 21168*x + 16632*y + 33264*z - 24192*x*y - 48384*x*z - 36288*y*z - 10080*x*x - 9072*y*y - 27216*z*z - 7560;
   values[160] = 12096*x*y - 2520*y - 5040*z - 13608*x + 24192*x*z + 4032*y*z + 18144*x*x + 1008*y*y + 3024*z*z + 1512;
   values[161] = 24192*x*y - 5040*y - 7560*z - 27216*x + 36288*x*z + 6048*y*z + 36288*x*x + 2016*y*y + 4032*z*z + 3024;
   values[162] = 6048*x*y - 3024*y - 6048*z - 4536*x + 12096*x*z + 8064*y*z + 4032*x*x + 2016*y*y + 6048*z*z + 1080;
   values[163] = 4032*x*y - 1512*y - 3024*z - 3024*x + 8064*x*z + 4032*y*z + 3024*x*x + 1008*y*y + 3024*z*z + 540;
   values[164] = 9072*x + 4536*y + 3024*z - 12096*x*y - 8064*x*z - 4032*y*z - 9072*x*x - 3024*y*y - 1008*z*z - 1620;
   values[165] = 12096*x + 8064*y + 30240*z - 12096*x*y - 48384*x*z - 32256*y*z - 8064*x*x - 4032*y*y - 36288*z*z - 3888;
   values[166] = 8064*x + 4032*y + 15120*z - 8064*x*y - 32256*x*z - 16128*y*z - 6048*x*x - 2016*y*y - 18144*z*z - 1944;
   values[167] = 48384*x*y - 20160*y - 19656*z - 40320*x + 48384*x*z + 24192*y*z + 36288*x*x + 12096*y*y + 8064*z*z + 8208;
   values[168] = 6048*x*y - 5040*y - 27216*z - 7560*x + 36288*x*z + 24192*y*z + 4032*x*x + 2016*y*y + 36288*z*z + 3024;
   values[169] = 4032*x*y - 2520*y - 13608*z - 5040*x + 24192*x*z + 12096*y*z + 3024*x*x + 1008*y*y + 18144*z*z + 1512;
   values[170] = 33264*x + 16632*y + 21168*z - 36288*x*y - 48384*x*z - 24192*y*z - 27216*x*x - 9072*y*y - 10080*z*z - 7560;
   values[171] = 21168*x + 10584*y + 21168*z - 24192*x*y - 48384*x*z - 24192*y*z - 20160*x*x - 6048*y*y - 18144*z*z - 4536;
   values[172] = 10584*x + 3528*y + 7056*z - 12096*x*y - 24192*x*z - 8064*y*z - 12096*x*x - 2016*y*y - 6048*z*z - 1512;
   values[173] = 36288*x*y - 10584*y - 7056*z - 31752*x + 24192*x*z + 8064*y*z + 36288*x*x + 6048*y*y + 2016*z*z + 4536;
   values[174] = 24192*x*y - 13608*y - 51408*z - 27216*x + 96768*x*z + 48384*y*z + 20160*x*x + 6048*y*y + 54432*z*z + 7560;
   values[175] = 12096*x*y - 4536*y - 17136*z - 13608*x + 48384*x*z + 16128*y*z + 12096*x*x + 2016*y*y + 18144*z*z + 2520;
   values[176] = 69552*x + 23184*y + 22680*z - 72576*x*y - 72576*x*z - 24192*y*z - 72576*x*x - 12096*y*y - 8064*z*z - 11088;
   values[177] = 20160*x*y - 8064*y - 16128*z - 20160*x + 40320*x*z + 16128*y*z + 20160*x*x + 4032*y*y + 12096*z*z + 4032;
   values[178] = 8064*x*y - 2016*y - 4032*z - 8064*x + 16128*x*z + 4032*y*z + 10080*x*x + 1008*y*y + 3024*z*z + 1008;
   values[179] = 24192*x + 6048*y + 4032*z - 24192*x*y - 16128*x*z - 4032*y*z - 30240*x*x - 3024*y*y - 1008*z*z - 3024;
   values[180] = 4536*y - 4032*x*y - 6048*y*z - 6048*y*y;
   values[181] = 8064*x*y - 3024*y - 1512*z - 3024*x + 4032*x*z + 4032*y*z + 3024*x*x + 3024*y*y + 1008*z*z + 540;
   values[182] = 4032*x*y - 1512*y + 2016*y*z + 2016*y*y;
   values[183] = 8064*x*y - 10584*y + 12096*y*z + 18144*y*y;
   values[184] = 7056*x + 10584*y + 3528*z - 24192*x*y - 8064*x*z - 12096*y*z - 6048*x*x - 12096*y*y - 2016*z*z - 1512;
   values[185] = 3528*y - 8064*x*y - 4032*y*z - 6048*y*y;
   values[186] = 6048*y - 4032*x*y - 6048*y*z - 12096*y*y;
   values[187] = 16128*x*y - 8064*y - 2016*z - 4032*x + 4032*x*z + 8064*y*z + 3024*x*x + 10080*y*y + 1008*z*z + 1008;
   values[188] = 4032*x*y - 2016*y + 2016*y*z + 4032*y*y;
   values[189] = 24192*x*y - 20160*y + 24192*y*z + 24192*y*y;
   values[190] = 15120*x + 8064*y + 4032*z - 32256*x*y - 16128*x*z - 8064*y*z - 18144*x*x - 6048*y*y - 2016*z*z - 1944;
   values[191] = 4032*y - 16128*x*y - 4032*y*z - 4032*y*y;
   values[192] = 23184*y - 24192*x*y - 24192*y*z - 36288*y*y;
   values[193] = 48384*x*y - 13608*y - 4536*z - 17136*x + 16128*x*z + 12096*y*z + 18144*x*x + 12096*y*y + 2016*z*z + 2520;
   values[194] = 16128*x*y - 4536*y + 4032*y*z + 6048*y*y;
   values[195] = 16632*y - 24192*x*y - 18144*y*z - 18144*y*y;
   values[196] = 24192*x*y - 5040*y - 2520*z - 13608*x + 12096*x*z + 4032*y*z + 18144*x*x + 3024*y*y + 1008*z*z + 1512;
   values[197] = 12096*x*y - 2520*y + 2016*y*z + 2016*y*y;
   values[198] = 6048*x*y - 3024*y + 4032*y*z + 4032*y*y;
   values[199] = 12096*x + 4536*y + 6048*z - 12096*x*y - 16128*x*z - 6048*y*z - 12096*x*x - 2016*y*y - 4032*z*z - 2160;
   values[200] = 4032*x*y - 1512*y + 2016*y*z + 2016*y*y;
   values[201] = 8064*y - 12096*x*y - 8064*y*z - 16128*y*y;
   values[202] = 48384*x*y - 20160*y - 13608*z - 27216*x + 32256*x*z + 24192*y*z + 24192*x*x + 12096*y*y + 8064*z*z + 5616;
   values[203] = 4032*y - 8064*x*y - 4032*y*z - 8064*y*y;
   values[204] = 6048*x*y - 5040*y + 4032*y*z + 12096*y*y;
   values[205] = 15120*x + 16632*y + 7560*z - 36288*x*y - 16128*x*z - 18144*y*z - 12096*x*x - 12096*y*y - 4032*z*z - 3528;
   values[206] = 4032*x*y - 2520*y + 2016*y*z + 6048*y*y;
   values[207] = 10584*y - 24192*x*y - 12096*y*z - 12096*y*y;
   values[208] = 36288*x*y - 10584*y - 14112*z - 42336*x + 48384*x*z + 12096*y*z + 48384*x*x + 4032*y*y + 8064*z*z + 6048;
   values[209] = 3528*y - 12096*x*y - 4032*y*z - 4032*y*y;
   values[210] = 24192*x*y - 13608*y + 12096*y*z + 24192*y*y;
   values[211] = 46872*x + 23184*y + 15624*z - 72576*x*y - 48384*x*z - 24192*y*z - 48384*x*x - 12096*y*y - 8064*z*z - 7560;
   values[212] = 12096*x*y - 4536*y + 4032*y*z + 8064*y*y;
   values[213] = 20160*x*y - 8064*y + 8064*y*z + 8064*y*y;
   values[214] = 32256*x + 6048*y + 8064*z - 24192*x*y - 32256*x*z - 6048*y*z - 40320*x*x - 2016*y*y - 4032*z*z - 4032;
   values[215] = 8064*x*y - 2016*y + 2016*y*z + 2016*y*y;
   values[216] = 16128*x*y - 21168*y + 48384*y*z + 24192*y*y;
   values[217] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[218] = 14112*y - 32256*x*y - 24192*y*z - 16128*y*y;
   values[219] = 14112*y - 24192*x*y - 32256*y*z - 16128*y*y;
   values[220] = 48384*x*y - 21168*y - 56448*z - 56448*x + 129024*x*z + 48384*y*z + 48384*x*x + 8064*y*y + 48384*z*z + 12096;
   values[221] = 14112*y - 32256*x*y - 24192*y*z - 16128*y*y;
   values[222] = 14112*y - 24192*x*y - 32256*y*z - 16128*y*y;
   values[223] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[224] = 48384*x*y - 21168*y + 16128*y*z + 24192*y*y;
   values[225] = 24192*y - 16128*x*y - 72576*y*z - 24192*y*y;
   values[226] = 32256*x*y - 16128*y - 24192*z - 16128*x + 48384*x*z + 48384*y*z + 12096*x*x + 12096*y*y + 24192*z*z + 4032;
   values[227] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[228] = 24192*x*y - 16128*y + 48384*y*z + 16128*y*y;
   values[229] = 64512*x + 24192*y + 96768*z - 48384*x*y - 193536*x*z - 72576*y*z - 48384*x*x - 8064*y*y - 96768*z*z - 16128;
   values[230] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[231] = 24192*x*y - 18144*y + 64512*y*z + 16128*y*y;
   values[232] = 32256*x*y - 18144*y - 34272*z - 18144*x + 64512*x*z + 64512*y*z + 12096*x*x + 12096*y*y + 36288*z*z + 5040;
   values[233] = 46368*y - 96768*x*y - 48384*y*z - 48384*y*y;
   values[234] = 24192*y - 16128*x*y - 48384*y*z - 36288*y*y;
   values[235] = 48384*x*y - 24192*y - 16128*z - 16128*x + 32256*x*z + 48384*y*z + 12096*x*x + 24192*y*y + 12096*z*z + 4032;
   values[236] = 32256*x*y - 16128*y + 24192*y*z + 24192*y*y;
   values[237] = 24192*x*y - 18144*y + 32256*y*z + 32256*y*y;
   values[238] = 62496*x + 46368*y + 62496*z - 96768*x*y - 129024*x*z - 96768*y*z - 48384*x*x - 24192*y*y - 48384*z*z - 15120;
   values[239] = 32256*x*y - 18144*y + 24192*y*z + 32256*y*y;
   values[240] = 24192*x*y - 16128*y + 32256*y*z + 24192*y*y;
   values[241] = 48384*x*y - 24192*y - 16128*z - 16128*x + 32256*x*z + 48384*y*z + 12096*x*x + 24192*y*y + 12096*z*z + 4032;
   values[242] = 24192*y - 48384*x*y - 16128*y*z - 36288*y*y;
   values[243] = 46368*y - 48384*x*y - 96768*y*z - 48384*y*y;
   values[244] = 64512*x*y - 18144*y - 18144*z - 34272*x + 64512*x*z + 32256*y*z + 36288*x*x + 12096*y*y + 12096*z*z + 5040;
   values[245] = 64512*x*y - 18144*y + 24192*y*z + 16128*y*y;
   values[246] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[247] = 96768*x + 24192*y + 64512*z - 72576*x*y - 193536*x*z - 48384*y*z - 96768*x*x - 8064*y*y - 48384*z*z - 16128;
   values[248] = 48384*x*y - 16128*y + 24192*y*z + 16128*y*y;
   values[249] = 48384*x*y - 24192*y + 48384*y*z + 24192*y*y;
   values[250] = 48384*x*y - 16128*y - 16128*z - 24192*x + 48384*x*z + 32256*y*z + 24192*x*x + 12096*y*y + 12096*z*z + 4032;
   values[251] = 24192*y - 72576*x*y - 16128*y*z - 24192*y*y;
}
static void Curl_T_P4_3D_3D_D011(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 4032*x*y - 3360*y - 3360*z - 2520*x + 4032*x*z + 5376*y*z + 1344*x*x + 2688*y*y + 2688*z*z + 960;
   values[1] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[2] = 840*x - 1344*x*y - 1344*x*z - 1344*x*x;
   values[3] = 17136*x + 11592*y + 11592*z - 24192*x*y - 24192*x*z - 16128*y*z - 12096*x*x - 8064*y*y - 8064*z*z - 3960;
   values[4] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[5] = 4032*x*y - 3528*x + 4032*x*z + 8064*x*x;
   values[6] = 36288*x*y - 13104*y - 13104*z - 28728*x + 36288*x*z + 16128*y*z + 24192*x*x + 8064*y*y + 8064*z*z + 5184;
   values[7] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[8] = 4536*x - 4032*x*y - 4032*x*z - 12096*x*x;
   values[9] = 14112*x + 4872*y + 4872*z - 16128*x*y - 16128*x*z - 5376*y*z - 13440*x*x - 2688*y*y - 2688*z*z - 2184;
   values[10] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[11] = 1344*x*y - 1848*x + 1344*x*z + 5376*x*x;
   values[12] = 840*x + 1680*y + 840*z - 2688*x*y - 1344*x*z - 2688*y*z - 672*x*x - 2016*y*y - 672*z*z - 240;
   values[13] = 2688*x*y - 1680*y - 2520*z - 2520*x + 4032*x*z + 2688*y*z + 2016*x*x + 672*y*y + 2016*z*z + 720;
   values[14] = 840*x + 1680*y + 840*z - 2688*x*y - 1344*x*z - 2688*y*z - 672*x*x - 2016*y*y - 672*z*z - 240;
   values[15] = 16128*x*y - 13104*y - 3528*z - 3528*x + 4032*x*z + 16128*y*z + 2016*x*x + 18144*y*y + 2016*z*z + 1440;
   values[16] = 17136*x + 16632*y + 17136*z - 24192*x*y - 24192*x*z - 24192*y*z - 12096*x*x - 8064*y*y - 12096*z*z - 5760;
   values[17] = 16128*x*y - 13104*y - 3528*z - 3528*x + 4032*x*z + 16128*y*z + 2016*x*x + 18144*y*y + 2016*z*z + 1440;
   values[18] = 4536*x + 24192*y + 4536*z - 24192*x*y - 4032*x*z - 24192*y*z - 2016*x*x - 36288*y*y - 2016*z*z - 2376;
   values[19] = 48384*x*y - 36288*y - 28728*z - 28728*x + 36288*x*z + 48384*y*z + 18144*x*x + 20160*y*y + 18144*z*z + 11016;
   values[20] = 4536*x + 24192*y + 4536*z - 24192*x*y - 4032*x*z - 24192*y*z - 2016*x*x - 36288*y*y - 2016*z*z - 2376;
   values[21] = 10752*x*y - 12768*y - 1848*z - 1848*x + 1344*x*z + 10752*y*z + 672*x*x + 20160*y*y + 672*z*z + 1176;
   values[22] = 14112*x + 21840*y + 14112*z - 26880*x*y - 16128*x*z - 26880*y*z - 8064*x*x - 13440*y*y - 8064*z*z - 6048;
   values[23] = 10752*x*y - 12768*y - 1848*z - 1848*x + 1344*x*z + 10752*y*z + 672*x*x + 20160*y*y + 672*z*z + 1176;
   values[24] = 840*x + 840*y + 1680*z - 1344*x*y - 2688*x*z - 2688*y*z - 672*x*x - 672*y*y - 2016*z*z - 240;
   values[25] = 840*x + 840*y + 1680*z - 1344*x*y - 2688*x*z - 2688*y*z - 672*x*x - 672*y*y - 2016*z*z - 240;
   values[26] = 4032*x*y - 2520*y - 1680*z - 2520*x + 2688*x*z + 2688*y*z + 2016*x*x + 2016*y*y + 672*z*z + 720;
   values[27] = 4032*x*y - 3528*y - 13104*z - 3528*x + 16128*x*z + 16128*y*z + 2016*x*x + 2016*y*y + 18144*z*z + 1440;
   values[28] = 4032*x*y - 3528*y - 13104*z - 3528*x + 16128*x*z + 16128*y*z + 2016*x*x + 2016*y*y + 18144*z*z + 1440;
   values[29] = 17136*x + 17136*y + 16632*z - 24192*x*y - 24192*x*z - 24192*y*z - 12096*x*x - 12096*y*y - 8064*z*z - 5760;
   values[30] = 4536*x + 4536*y + 24192*z - 4032*x*y - 24192*x*z - 24192*y*z - 2016*x*x - 2016*y*y - 36288*z*z - 2376;
   values[31] = 4536*x + 4536*y + 24192*z - 4032*x*y - 24192*x*z - 24192*y*z - 2016*x*x - 2016*y*y - 36288*z*z - 2376;
   values[32] = 36288*x*y - 28728*y - 36288*z - 28728*x + 48384*x*z + 48384*y*z + 18144*x*x + 18144*y*y + 20160*z*z + 11016;
   values[33] = 1344*x*y - 1848*y - 12768*z - 1848*x + 10752*x*z + 10752*y*z + 672*x*x + 672*y*y + 20160*z*z + 1176;
   values[34] = 1344*x*y - 1848*y - 12768*z - 1848*x + 10752*x*z + 10752*y*z + 672*x*x + 672*y*y + 20160*z*z + 1176;
   values[35] = 14112*x + 14112*y + 21840*z - 16128*x*y - 26880*x*z - 26880*y*z - 8064*x*x - 8064*y*y - 13440*z*z - 6048;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = 0;
   values[58] = 0;
   values[59] = 0;
   values[60] = 0;
   values[61] = 504*y - 672*y*y - 72;
   values[62] = 0;
   values[63] = 0;
   values[64] = 2016*z - 504*y - 8064*y*z + 2016*y*y - 72;
   values[65] = 6048*y*y - 3024*y + 216;
   values[66] = 0;
   values[67] = 16128*y*z - 1008*z - 504*y - 2016*y*y - 6048*z*z + 144;
   values[68] = 4032*y - 1008*z + 8064*y*z - 12096*y*y - 144;
   values[69] = 0;
   values[70] = 504*y - 1008*z - 8064*y*z + 672*y*y + 6048*z*z;
   values[71] = 504*z - 1008*y - 8064*y*z + 6048*y*y + 672*z*z;
   values[72] = 504*x - 1008*x*x - 36;
   values[73] = 0;
   values[74] = 0;
   values[75] = 504*z - 504*x - 4032*x*z + 2016*x*x;
   values[76] = 0;
   values[77] = 504*x - 2016*x*x;
   values[78] = 4032*x*z - 1008*x*x - 1008*z*z;
   values[79] = 0;
   values[80] = 2016*x*x - 2016*x*z;
   values[81] = 1008*y - 8064*x*y + 2016*x*x - 72;
   values[82] = 6048*x*x - 1512*x;
   values[83] = 0;
   values[84] = 504*z - 504*x + 8064*x*y + 4032*x*z - 8064*y*z - 2016*x*x;
   values[85] = 12096*x*z - 6048*x*x;
   values[86] = 504*x - 8064*x*y + 2016*x*x;
   values[87] = 504*y - 504*x + 8064*x*y - 1008*x*x - 3024*y*y;
   values[88] = 504*x + 4032*x*y - 6048*x*x;
   values[89] = 0;
   values[90] = 504*x - 1008*x*x - 36;
   values[91] = 0;
   values[92] = 0;
   values[93] = 1008*z - 8064*x*z + 2016*x*x - 72;
   values[94] = 0;
   values[95] = 6048*x*x - 1512*x;
   values[96] = 504*z - 504*x + 8064*x*z - 1008*x*x - 3024*z*z;
   values[97] = 0;
   values[98] = 504*x + 4032*x*z - 6048*x*x;
   values[99] = 504*y - 504*x - 4032*x*y + 2016*x*x;
   values[100] = 504*x - 2016*x*x;
   values[101] = 0;
   values[102] = 504*y - 504*x + 4032*x*y + 8064*x*z - 8064*y*z - 2016*x*x;
   values[103] = 504*x - 8064*x*z + 2016*x*x;
   values[104] = 12096*x*y - 6048*x*x;
   values[105] = 4032*x*y - 1008*x*x - 1008*y*y;
   values[106] = 2016*x*x - 2016*x*y;
   values[107] = 0;
   values[108] = 4032*x*y - 3024*y - 3024*z - 1512*x + 4032*x*z + 8064*y*z + 1008*x*x + 3024*y*y + 3024*z*z + 540;
   values[109] = 4536*x + 3024*y + 9072*z - 4032*x*y - 12096*x*z - 8064*y*z - 3024*x*x - 1008*y*y - 9072*z*z - 1620;
   values[110] = 8064*x*y - 6048*y - 4536*z - 3024*x + 6048*x*z + 12096*y*z + 2016*x*x + 6048*y*y + 4032*z*z + 1080;
   values[111] = 3528*x + 7056*y + 10584*z - 8064*x*y - 12096*x*z - 24192*y*z - 2016*x*x - 6048*y*y - 12096*z*z - 1512;
   values[112] = 8064*x*y - 7056*y - 31752*z - 10584*x + 36288*x*z + 24192*y*z + 6048*x*x + 2016*y*y + 36288*z*z + 4536;
   values[113] = 10584*x + 21168*y + 21168*z - 24192*x*y - 24192*x*z - 48384*y*z - 6048*x*x - 18144*y*y - 20160*z*z - 4536;
   values[114] = 4032*x*y - 4032*y - 8064*z - 2016*x + 8064*x*z + 16128*y*z + 1008*x*x + 3024*y*y + 10080*z*z + 1008;
   values[115] = 6048*x + 4032*y + 24192*z - 4032*x*y - 24192*x*z - 16128*y*z - 3024*x*x - 1008*y*y - 30240*z*z - 3024;
   values[116] = 16128*x*y - 16128*y - 20160*z - 8064*x + 20160*x*z + 40320*y*z + 4032*x*x + 12096*y*y + 20160*z*z + 4032;
   values[117] = 4032*x + 15120*y + 8064*z - 16128*x*y - 8064*x*z - 32256*y*z - 2016*x*x - 18144*y*y - 6048*z*z - 1944;
   values[118] = 24192*x*y - 19656*y - 40320*z - 20160*x + 48384*x*z + 48384*y*z + 12096*x*x + 8064*y*y + 36288*z*z + 8208;
   values[119] = 8064*x + 30240*y + 12096*z - 32256*x*y - 12096*x*z - 48384*y*z - 4032*x*x - 36288*y*y - 8064*z*z - 3888;
   values[120] = 16128*x*y - 17136*y - 13608*z - 4536*x + 12096*x*z + 48384*y*z + 2016*x*x + 18144*y*y + 12096*z*z + 2520;
   values[121] = 23184*x + 22680*y + 69552*z - 24192*x*y - 72576*x*z - 72576*y*z - 12096*x*x - 8064*y*y - 72576*z*z - 11088;
   values[122] = 48384*x*y - 51408*y - 27216*z - 13608*x + 24192*x*z + 96768*y*z + 6048*x*x + 54432*y*y + 20160*z*z + 7560;
   values[123] = 12096*x*y - 13608*y - 5040*z - 2520*x + 4032*x*z + 24192*y*z + 1008*x*x + 18144*y*y + 3024*z*z + 1512;
   values[124] = 16632*x + 21168*y + 33264*z - 24192*x*y - 36288*x*z - 48384*y*z - 9072*x*x - 10080*y*y - 27216*z*z - 7560;
   values[125] = 24192*x*y - 27216*y - 7560*z - 5040*x + 6048*x*z + 36288*y*z + 2016*x*x + 36288*y*y + 4032*z*z + 3024;
   values[126] = 4032*x*y - 3024*y - 3024*z - 1512*x + 4032*x*z + 8064*y*z + 1008*x*x + 3024*y*y + 3024*z*z + 540;
   values[127] = 6048*x*y - 4536*y - 6048*z - 3024*x + 8064*x*z + 12096*y*z + 2016*x*x + 4032*y*y + 6048*z*z + 1080;
   values[128] = 4536*x + 9072*y + 3024*z - 12096*x*y - 4032*x*z - 8064*y*z - 3024*x*x - 9072*y*y - 1008*z*z - 1620;
   values[129] = 4032*x + 8064*y + 15120*z - 8064*x*y - 16128*x*z - 32256*y*z - 2016*x*x - 6048*y*y - 18144*z*z - 1944;
   values[130] = 8064*x + 12096*y + 30240*z - 12096*x*y - 32256*x*z - 48384*y*z - 4032*x*x - 8064*y*y - 36288*z*z - 3888;
   values[131] = 48384*x*y - 40320*y - 19656*z - 20160*x + 24192*x*z + 48384*y*z + 12096*x*x + 36288*y*y + 8064*z*z + 8208;
   values[132] = 4032*x*y - 5040*y - 13608*z - 2520*x + 12096*x*z + 24192*y*z + 1008*x*x + 3024*y*y + 18144*z*z + 1512;
   values[133] = 6048*x*y - 7560*y - 27216*z - 5040*x + 24192*x*z + 36288*y*z + 2016*x*x + 4032*y*y + 36288*z*z + 3024;
   values[134] = 16632*x + 33264*y + 21168*z - 36288*x*y - 24192*x*z - 48384*y*z - 9072*x*x - 27216*y*y - 10080*z*z - 7560;
   values[135] = 3528*x + 10584*y + 7056*z - 12096*x*y - 8064*x*z - 24192*y*z - 2016*x*x - 12096*y*y - 6048*z*z - 1512;
   values[136] = 10584*x + 21168*y + 21168*z - 24192*x*y - 24192*x*z - 48384*y*z - 6048*x*x - 20160*y*y - 18144*z*z - 4536;
   values[137] = 36288*x*y - 31752*y - 7056*z - 10584*x + 8064*x*z + 24192*y*z + 6048*x*x + 36288*y*y + 2016*z*z + 4536;
   values[138] = 12096*x*y - 13608*y - 17136*z - 4536*x + 16128*x*z + 48384*y*z + 2016*x*x + 12096*y*y + 18144*z*z + 2520;
   values[139] = 24192*x*y - 27216*y - 51408*z - 13608*x + 48384*x*z + 96768*y*z + 6048*x*x + 20160*y*y + 54432*z*z + 7560;
   values[140] = 23184*x + 69552*y + 22680*z - 72576*x*y - 24192*x*z - 72576*y*z - 12096*x*x - 72576*y*y - 8064*z*z - 11088;
   values[141] = 8064*x*y - 8064*y - 4032*z - 2016*x + 4032*x*z + 16128*y*z + 1008*x*x + 10080*y*y + 3024*z*z + 1008;
   values[142] = 20160*x*y - 20160*y - 16128*z - 8064*x + 16128*x*z + 40320*y*z + 4032*x*x + 20160*y*y + 12096*z*z + 4032;
   values[143] = 6048*x + 24192*y + 4032*z - 24192*x*y - 4032*x*z - 16128*y*z - 3024*x*x - 30240*y*y - 1008*z*z - 3024;
   values[144] = 4536*x + 6048*y + 12096*z - 6048*x*y - 12096*x*z - 16128*y*z - 2016*x*x - 4032*y*y - 12096*z*z - 2160;
   values[145] = 2016*x*y - 1512*x + 4032*x*z + 2016*x*x;
   values[146] = 4032*x*y - 3024*x + 6048*x*z + 4032*x*x;
   values[147] = 12096*x*y - 14112*y - 42336*z - 10584*x + 36288*x*z + 48384*y*z + 4032*x*x + 8064*y*y + 48384*z*z + 6048;
   values[148] = 3528*x - 4032*x*y - 12096*x*z - 4032*x*x;
   values[149] = 10584*x - 12096*x*y - 24192*x*z - 12096*x*x;
   values[150] = 6048*x + 8064*y + 32256*z - 6048*x*y - 24192*x*z - 32256*y*z - 2016*x*x - 4032*y*y - 40320*z*z - 4032;
   values[151] = 2016*x*y - 2016*x + 8064*x*z + 2016*x*x;
   values[152] = 8064*x*y - 8064*x + 20160*x*z + 8064*x*x;
   values[153] = 24192*x*y - 13608*y - 27216*z - 20160*x + 48384*x*z + 32256*y*z + 12096*x*x + 8064*y*y + 24192*z*z + 5616;
   values[154] = 4032*x - 4032*x*y - 8064*x*z - 8064*x*x;
   values[155] = 8064*x - 8064*x*y - 12096*x*z - 16128*x*x;
   values[156] = 23184*x + 15624*y + 46872*z - 24192*x*y - 72576*x*z - 48384*y*z - 12096*x*x - 8064*y*y - 48384*z*z - 7560;
   values[157] = 4032*x*y - 4536*x + 12096*x*z + 8064*x*x;
   values[158] = 12096*x*y - 13608*x + 24192*x*z + 24192*x*x;
   values[159] = 16632*x + 7560*y + 15120*z - 18144*x*y - 36288*x*z - 16128*y*z - 12096*x*x - 4032*y*y - 12096*z*z - 3528;
   values[160] = 2016*x*y - 2520*x + 4032*x*z + 6048*x*x;
   values[161] = 4032*x*y - 5040*x + 6048*x*z + 12096*x*x;
   values[162] = 4032*x*y - 1512*y - 3024*z - 3024*x + 8064*x*z + 4032*y*z + 3024*x*x + 1008*y*y + 3024*z*z + 540;
   values[163] = 2016*x*y - 1512*x + 4032*x*z + 2016*x*x;
   values[164] = 4536*x - 6048*x*y - 4032*x*z - 6048*x*x;
   values[165] = 8064*x + 4032*y + 15120*z - 8064*x*y - 32256*x*z - 16128*y*z - 6048*x*x - 2016*y*y - 18144*z*z - 1944;
   values[166] = 4032*x - 4032*x*y - 16128*x*z - 4032*x*x;
   values[167] = 24192*x*y - 20160*x + 24192*x*z + 24192*x*x;
   values[168] = 4032*x*y - 2520*y - 13608*z - 5040*x + 24192*x*z + 12096*y*z + 3024*x*x + 1008*y*y + 18144*z*z + 1512;
   values[169] = 2016*x*y - 2520*x + 12096*x*z + 2016*x*x;
   values[170] = 16632*x - 18144*x*y - 24192*x*z - 18144*x*x;
   values[171] = 10584*x + 3528*y + 7056*z - 12096*x*y - 24192*x*z - 8064*y*z - 12096*x*x - 2016*y*y - 6048*z*z - 1512;
   values[172] = 3528*x - 4032*x*y - 8064*x*z - 6048*x*x;
   values[173] = 12096*x*y - 10584*x + 8064*x*z + 18144*x*x;
   values[174] = 12096*x*y - 4536*y - 17136*z - 13608*x + 48384*x*z + 16128*y*z + 12096*x*x + 2016*y*y + 18144*z*z + 2520;
   values[175] = 4032*x*y - 4536*x + 16128*x*z + 6048*x*x;
   values[176] = 23184*x - 24192*x*y - 24192*x*z - 36288*x*x;
   values[177] = 8064*x*y - 2016*y - 4032*z - 8064*x + 16128*x*z + 4032*y*z + 10080*x*x + 1008*y*y + 3024*z*z + 1008;
   values[178] = 2016*x*y - 2016*x + 4032*x*z + 4032*x*x;
   values[179] = 6048*x - 6048*x*y - 4032*x*z - 12096*x*x;
   values[180] = 4536*x + 12096*y + 6048*z - 12096*x*y - 6048*x*z - 16128*y*z - 2016*x*x - 12096*y*y - 4032*z*z - 2160;
   values[181] = 6048*x*y - 3024*x + 4032*x*z + 4032*x*x;
   values[182] = 4032*x*y - 1512*x + 2016*x*z + 2016*x*x;
   values[183] = 36288*x*y - 42336*y - 14112*z - 10584*x + 12096*x*z + 48384*y*z + 4032*x*x + 48384*y*y + 8064*z*z + 6048;
   values[184] = 10584*x - 24192*x*y - 12096*x*z - 12096*x*x;
   values[185] = 3528*x - 12096*x*y - 4032*x*z - 4032*x*x;
   values[186] = 6048*x + 32256*y + 8064*z - 24192*x*y - 6048*x*z - 32256*y*z - 2016*x*x - 40320*y*y - 4032*z*z - 4032;
   values[187] = 20160*x*y - 8064*x + 8064*x*z + 8064*x*x;
   values[188] = 8064*x*y - 2016*x + 2016*x*z + 2016*x*x;
   values[189] = 48384*x*y - 27216*y - 13608*z - 20160*x + 24192*x*z + 32256*y*z + 12096*x*x + 24192*y*y + 8064*z*z + 5616;
   values[190] = 8064*x - 12096*x*y - 8064*x*z - 16128*x*x;
   values[191] = 4032*x - 8064*x*y - 4032*x*z - 8064*x*x;
   values[192] = 23184*x + 46872*y + 15624*z - 72576*x*y - 24192*x*z - 48384*y*z - 12096*x*x - 48384*y*y - 8064*z*z - 7560;
   values[193] = 24192*x*y - 13608*x + 12096*x*z + 24192*x*x;
   values[194] = 12096*x*y - 4536*x + 4032*x*z + 8064*x*x;
   values[195] = 16632*x + 15120*y + 7560*z - 36288*x*y - 18144*x*z - 16128*y*z - 12096*x*x - 12096*y*y - 4032*z*z - 3528;
   values[196] = 6048*x*y - 5040*x + 4032*x*z + 12096*x*x;
   values[197] = 4032*x*y - 2520*x + 2016*x*z + 6048*x*x;
   values[198] = 8064*x*y - 3024*y - 1512*z - 3024*x + 4032*x*z + 4032*y*z + 3024*x*x + 3024*y*y + 1008*z*z + 540;
   values[199] = 4536*x - 4032*x*y - 6048*x*z - 6048*x*x;
   values[200] = 4032*x*y - 1512*x + 2016*x*z + 2016*x*x;
   values[201] = 8064*x + 15120*y + 4032*z - 32256*x*y - 8064*x*z - 16128*y*z - 6048*x*x - 18144*y*y - 2016*z*z - 1944;
   values[202] = 24192*x*y - 20160*x + 24192*x*z + 24192*x*x;
   values[203] = 4032*x - 16128*x*y - 4032*x*z - 4032*x*x;
   values[204] = 24192*x*y - 13608*y - 2520*z - 5040*x + 4032*x*z + 12096*y*z + 3024*x*x + 18144*y*y + 1008*z*z + 1512;
   values[205] = 16632*x - 24192*x*y - 18144*x*z - 18144*x*x;
   values[206] = 12096*x*y - 2520*x + 2016*x*z + 2016*x*x;
   values[207] = 10584*x + 7056*y + 3528*z - 24192*x*y - 12096*x*z - 8064*y*z - 12096*x*x - 6048*y*y - 2016*z*z - 1512;
   values[208] = 8064*x*y - 10584*x + 12096*x*z + 18144*x*x;
   values[209] = 3528*x - 8064*x*y - 4032*x*z - 6048*x*x;
   values[210] = 48384*x*y - 17136*y - 4536*z - 13608*x + 12096*x*z + 16128*y*z + 12096*x*x + 18144*y*y + 2016*z*z + 2520;
   values[211] = 23184*x - 24192*x*y - 24192*x*z - 36288*x*x;
   values[212] = 16128*x*y - 4536*x + 4032*x*z + 6048*x*x;
   values[213] = 16128*x*y - 4032*y - 2016*z - 8064*x + 8064*x*z + 4032*y*z + 10080*x*x + 3024*y*y + 1008*z*z + 1008;
   values[214] = 6048*x - 4032*x*y - 6048*x*z - 12096*x*x;
   values[215] = 4032*x*y - 2016*x + 2016*x*z + 4032*x*x;
   values[216] = 48384*x*y - 56448*y - 56448*z - 21168*x + 48384*x*z + 129024*y*z + 8064*x*x + 48384*y*y + 48384*z*z + 12096;
   values[217] = 14112*x - 24192*x*y - 32256*x*z - 16128*x*x;
   values[218] = 14112*x - 32256*x*y - 24192*x*z - 16128*x*x;
   values[219] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[220] = 16128*x*y - 21168*x + 48384*x*z + 24192*x*x;
   values[221] = 14112*x - 32256*x*y - 24192*x*z - 16128*x*x;
   values[222] = 14112*x + 14112*y + 14112*z - 32256*x*y - 32256*x*z - 32256*y*z - 12096*x*x - 12096*y*y - 12096*z*z - 3024;
   values[223] = 14112*x - 24192*x*y - 32256*x*z - 16128*x*x;
   values[224] = 48384*x*y - 21168*x + 16128*x*z + 24192*x*x;
   values[225] = 24192*x + 64512*y + 96768*z - 48384*x*y - 72576*x*z - 193536*y*z - 8064*x*x - 48384*y*y - 96768*z*z - 16128;
   values[226] = 24192*x*y - 16128*x + 48384*x*z + 16128*x*x;
   values[227] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[228] = 32256*x*y - 16128*y - 24192*z - 16128*x + 48384*x*z + 48384*y*z + 12096*x*x + 12096*y*y + 24192*z*z + 4032;
   values[229] = 24192*x - 16128*x*y - 72576*x*z - 24192*x*x;
   values[230] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[231] = 32256*x*y - 18144*y - 34272*z - 18144*x + 64512*x*z + 64512*y*z + 12096*x*x + 12096*y*y + 36288*z*z + 5040;
   values[232] = 24192*x*y - 18144*x + 64512*x*z + 16128*x*x;
   values[233] = 46368*x - 96768*x*y - 48384*x*z - 48384*x*x;
   values[234] = 24192*x + 96768*y + 64512*z - 72576*x*y - 48384*x*z - 193536*y*z - 8064*x*x - 96768*y*y - 48384*z*z - 16128;
   values[235] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[236] = 48384*x*y - 16128*x + 24192*x*z + 16128*x*x;
   values[237] = 64512*x*y - 34272*y - 18144*z - 18144*x + 32256*x*z + 64512*y*z + 12096*x*x + 36288*y*y + 12096*z*z + 5040;
   values[238] = 46368*x - 48384*x*y - 96768*x*z - 48384*x*x;
   values[239] = 64512*x*y - 18144*x + 24192*x*z + 16128*x*x;
   values[240] = 48384*x*y - 24192*y - 16128*z - 16128*x + 32256*x*z + 48384*y*z + 12096*x*x + 24192*y*y + 12096*z*z + 4032;
   values[241] = 48384*x*y - 24192*x + 48384*x*z + 24192*x*x;
   values[242] = 24192*x - 72576*x*y - 16128*x*z - 24192*x*x;
   values[243] = 46368*x + 62496*y + 62496*z - 96768*x*y - 96768*x*z - 129024*y*z - 24192*x*x - 48384*y*y - 48384*z*z - 15120;
   values[244] = 24192*x*y - 18144*x + 32256*x*z + 32256*x*x;
   values[245] = 32256*x*y - 18144*x + 24192*x*z + 32256*x*x;
   values[246] = 48384*x*y - 16128*y - 16128*z - 24192*x + 48384*x*z + 32256*y*z + 24192*x*x + 12096*y*y + 12096*z*z + 4032;
   values[247] = 24192*x - 16128*x*y - 48384*x*z - 36288*x*x;
   values[248] = 32256*x*y - 16128*x + 24192*x*z + 24192*x*x;
   values[249] = 48384*x*y - 16128*y - 16128*z - 24192*x + 48384*x*z + 32256*y*z + 24192*x*x + 12096*y*y + 12096*z*z + 4032;
   values[250] = 24192*x*y - 16128*x + 32256*x*z + 24192*x*x;
   values[251] = 24192*x - 48384*x*y - 16128*x*z - 36288*x*x;
}

static void Curl_T_P4_3D_3D_Nodal(ELEMENT *elem, FUNCTIONVEC *fun, INT dim, DOUBLE *values)
{
   // 直接给出积分格式
   INT NumPoints = 16;
   DOUBLE QuadX116_1D[16] = {0.00529953250417497523, 0.02771248846338347782, 0.06718439880608401138, 0.12229779582249850067, 0.19106187779867800369, 0.27099161117138648169, 0.35919822461037048678, 0.45249374508118128668, 0.54750625491881865781, 0.64080177538962956874, 0.72900838882861351831, 0.80893812220132199631, 0.87770220417750155484, 0.93281560119391593311, 0.97228751153661652218, 0.99470046749582508028};
   DOUBLE QuadW116_1D[16] = {0.01357622970587699963, 0.03112676196932405090, 0.04757925584124569895, 0.06231448562776750050, 0.07479799440828900636, 0.08457825969750100426, 0.09130170752246150045, 0.09472530522753450088, 0.09472530522753450088, 0.09130170752246150045, 0.08457825969750100426, 0.07479799440828900636, 0.06231448562776750050, 0.04757925584124569895, 0.03112676196932405090, 0.01357622970587699963};
   INT vert0[6] = {0, 0, 0, 1, 1, 2};
   INT vert1[6] = {1, 2, 3, 2, 3, 3};
   INT i, j, k;
   INT num_fun = dim / 3; // 输入函数的个数(curl元是3D的)
   DOUBLE coord[3];
   DOUBLE vec_line[3]; // 带方向的线(按照单元内部的方向)
   DOUBLE *values_temp = malloc(dim * sizeof(DOUBLE));
   #if NEDELECSCALE == 0
   for (i = 0; i < 6; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         values[k + 4 * i * num_fun] = 0.0;
         values[k + (4 * i + 1) * num_fun] = 0.0;
         values[k + (4 * i + 2) * num_fun] = 0.0;
         values[k + (4 * i + 3) * num_fun] = 0.0;
      }
      vec_line[0] = elem->Vert_X[vert1[i]] - elem->Vert_X[vert0[i]];
      vec_line[1] = elem->Vert_Y[vert1[i]] - elem->Vert_Y[vert0[i]];
      vec_line[2] = elem->Vert_Z[vert1[i]] - elem->Vert_Z[vert0[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1 - QuadX116_1D[j]) * elem->Vert_X[vert0[i]] + QuadX116_1D[j] * elem->Vert_X[vert1[i]];
         coord[1] = (1 - QuadX116_1D[j]) * elem->Vert_Y[vert0[i]] + QuadX116_1D[j] * elem->Vert_Y[vert1[i]];
         coord[2] = (1 - QuadX116_1D[j]) * elem->Vert_Z[vert0[i]] + QuadX116_1D[j] * elem->Vert_Z[vert1[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 线上有三个自由度
         // \int_{e} \vec{u}\cdot \vec{\tau}de = \sum_j \vec{u}(t_j)cdot \vec{\tau}\omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t^2 de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j \X_j
         for (k = 0; k < num_fun; k++)
         {
            values[k + 4 * i * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j];
            values[k + (4 * i + 1) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j];
            values[k + (4 * i + 2) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j];
            values[k + (4 * i + 3) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j];
         }
      }
   }
   #else
   DOUBLE line_length[6];
   for (i = 0; i < 6; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         values[k + 4 * i * num_fun] = 0.0;
         values[k + (4 * i + 1) * num_fun] = 0.0;
         values[k + (4 * i + 2) * num_fun] = 0.0;
         values[k + (4 * i + 3) * num_fun] = 0.0;
      }
      vec_line[0] = elem->Vert_X[vert1[i]] - elem->Vert_X[vert0[i]];
      vec_line[1] = elem->Vert_Y[vert1[i]] - elem->Vert_Y[vert0[i]];
      vec_line[2] = elem->Vert_Z[vert1[i]] - elem->Vert_Z[vert0[i]];
      line_length[i] = sqrt(vec_line[0]*vec_line[0]+vec_line[1]*vec_line[1]+vec_line[2]*vec_line[2]);
#if length_change == 1
      line_length[i] = sqrt(line_length[i]);
#endif
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1 - QuadX116_1D[j]) * elem->Vert_X[vert0[i]] + QuadX116_1D[j] * elem->Vert_X[vert1[i]];
         coord[1] = (1 - QuadX116_1D[j]) * elem->Vert_Y[vert0[i]] + QuadX116_1D[j] * elem->Vert_Y[vert1[i]];
         coord[2] = (1 - QuadX116_1D[j]) * elem->Vert_Z[vert0[i]] + QuadX116_1D[j] * elem->Vert_Z[vert1[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 线上有三个自由度
         // \int_{e} \vec{u}\cdot \vec{\tau}de = \sum_j \vec{u}(t_j)cdot \vec{\tau}\omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t^2 de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j \X_j
         for (k = 0; k < num_fun; k++)
         {
            values[k + 4 * i * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] / line_length[i];
            values[k + (4 * i + 1) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] / line_length[i];
            values[k + (4 * i + 2) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j] / line_length[i];
            values[k + (4 * i + 3) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j] / line_length[i];
         }
      }
   }
   #endif
   NumPoints = 36;
   DOUBLE QuadX236[36] = {0.0024666971526702431011246,
                          0.0123750604174400428286740,
                          0.0278110821153605899946903,
                          0.0452432465648983234141056,
                          0.0606792682628188653759516,
                          0.0705876315275886651035009,
                          0.0077918747012864289502865,
                          0.0390907007328242447896649,
                          0.0878504549759971942179959,
                          0.1429156829939483008828915,
                          0.1916754372371212433723286,
                          0.2229742632686590730894949,
                          0.0149015633666711548588335,
                          0.0747589734626491059232833,
                          0.1680095191211918581597473,
                          0.2733189621072580344218750,
                          0.3665695077658007727805511,
                          0.4264269178617787203755540,
                          0.0223868729780306273402513,
                          0.1123116817809537010264265,
                          0.2524035680765180367224332,
                          0.4106117416423277211023901,
                          0.5507036279378919596538822,
                          0.6406284367408150437483982,
                          0.0287653330125591175092659,
                          0.1443114869504166508651366,
                          0.3243183045887760296288604,
                          0.5276030957427396694825461,
                          0.7076099133810990204906943,
                          0.8231560673189565191520956,
                          0.0327753666144598859721881,
                          0.1644292415948274965753484,
                          0.3695299243723767501634825,
                          0.6011536484678384750779401,
                          0.8062543312453877009104986,
                          0.9379082062257553253914466};
   DOUBLE QuadY236[36] = {0.0705876315275886651035009,
                          0.0606792682628188653759516,
                          0.0452432465648983164752117,
                          0.0278110821153605830557964,
                          0.0123750604174400410939505,
                          0.0024666971526702413664012,
                          0.2229742632686590730894949,
                          0.1916754372371212433723286,
                          0.1429156829939483008828915,
                          0.0878504549759971942179959,
                          0.0390907007328242517285588,
                          0.0077918747012864220113926,
                          0.4264269178617787203755540,
                          0.3665695077658007727805511,
                          0.2733189621072580344218750,
                          0.1680095191211918304041717,
                          0.0747589734626490920454955,
                          0.0149015633666711444504926,
                          0.6406284367408150437483982,
                          0.5507036279378919596538822,
                          0.4106117416423276655912389,
                          0.2524035680765179812112819,
                          0.1123116817809537426597899,
                          0.0223868729780306585652738,
                          0.8231560673189565191520956,
                          0.7076099133810990204906943,
                          0.5276030957427395584602436,
                          0.3243183045887759741177092,
                          0.1443114869504166231095610,
                          0.0287653330125591244481598,
                          0.9379082062257553253914466,
                          0.8062543312453877009104986,
                          0.6011536484678384750779401,
                          0.3695299243723767501634825,
                          0.1644292415948275243309240,
                          0.0327753666144598998499760};

   DOUBLE QuadW236[36] = {0.0014970851224726396642983,
                          0.0031524435080471811462810,
                          0.0040887731830897217424892,
                          0.0040887731830897217424892,
                          0.0031524435080471811462810,
                          0.0014970851224726396642983,
                          0.0075305964253833514512881,
                          0.0158573346675929711946385,
                          0.0205672344575326543347771,
                          0.0205672344575326543347771,
                          0.0158573346675929711946385,
                          0.0075305964253833514512881,
                          0.0169030715938862825808986,
                          0.0355931519940526144840653,
                          0.0461649273027165268912242,
                          0.0461649273027165268912242,
                          0.0355931519940526144840653,
                          0.0169030715938862825808986,
                          0.0241212128085302492108699,
                          0.0507925431780953395843881,
                          0.0658787978015734215775367,
                          0.0658787978015734215775367,
                          0.0507925431780953395843881,
                          0.0241212128085302492108699,
                          0.0232217495339950702470944,
                          0.0488985245161156767412791,
                          0.0634222231814079995260158,
                          0.0634222231814079995260158,
                          0.0488985245161156767412791,
                          0.0232217495339950702470944,
                          0.0123885307053177209229977,
                          0.0260867886601656891187861,
                          0.0338350113600253410428564,
                          0.0338350113600253410428564,
                          0.0260867886601656891187861,
                          0.0123885307053177209229977};
   INT vert04face[4] = {1, 0, 0, 0};
   INT vert14face[4] = {2, 2, 1, 1};
   INT vert24face[4] = {3, 3, 3, 2};
   DOUBLE tau0[3]; // 面的第一条边的方向
   DOUBLE tau1[3]; // 面的第二条边的方向
   DOUBLE length[3];
   DOUBLE area;
#if NEDELECSCALE == 0
   for (i = 0; i < 4; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         for (j = 0; j < 12; j++)
            values[k + ((12 * i + 24) + j) * num_fun] = 0.0;
      }
      // 计算面的第一条边的方向
      tau0[0] = elem->Vert_X[vert14face[i]] - elem->Vert_X[vert04face[i]];
      tau0[1] = elem->Vert_Y[vert14face[i]] - elem->Vert_Y[vert04face[i]];
      tau0[2] = elem->Vert_Z[vert14face[i]] - elem->Vert_Z[vert04face[i]];
      // 计算面的第二条边的方向
      tau1[0] = elem->Vert_X[vert24face[i]] - elem->Vert_X[vert04face[i]];
      tau1[1] = elem->Vert_Y[vert24face[i]] - elem->Vert_Y[vert04face[i]];
      tau1[2] = elem->Vert_Z[vert24face[i]] - elem->Vert_Z[vert04face[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_X[vert04face[i]] + QuadX236[j] * elem->Vert_X[vert14face[i]] + QuadY236[j] * elem->Vert_X[vert24face[i]];
         coord[1] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Y[vert04face[i]] + QuadX236[j] * elem->Vert_Y[vert14face[i]] + QuadY236[j] * elem->Vert_Y[vert24face[i]];
         coord[2] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Z[vert04face[i]] + QuadX236[j] * elem->Vert_Z[vert14face[i]] + QuadY236[j] * elem->Vert_Z[vert24face[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 面上有六个自由度
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j xi
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j xi
         for (k = 0; k < num_fun; k++)
         {
            values[k + (12 * i + 24) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j];
            values[k + (12 * i + 24 + 1) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 2) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 3) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j];
            values[k + (12 * i + 24 + 4) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 5) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] * QuadX236[j];
            values[k + (12 * i + 24 + 6) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j];
            values[k + (12 * i + 24 + 7) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 8) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 9) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j];
            values[k + (12 * i + 24 + 10) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] * QuadY236[j];
            values[k + (12 * i + 24 + 11) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] * QuadX236[j];
         }
      }
   }
#else
   INT line4face1[4] = {3,1,0,0};
   INT line4face2[4] = {4,2,2,1};
   for (i = 0; i < 4; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         for (j = 0; j < 12; j++)
            values[k + ((12 * i + 24) + j) * num_fun] = 0.0;
      }
      // 计算面的第一条边的方向
      tau0[0] = elem->Vert_X[vert14face[i]] - elem->Vert_X[vert04face[i]];
      tau0[1] = elem->Vert_Y[vert14face[i]] - elem->Vert_Y[vert04face[i]];
      tau0[2] = elem->Vert_Z[vert14face[i]] - elem->Vert_Z[vert04face[i]];
      // 计算面的第二条边的方向
      tau1[0] = elem->Vert_X[vert24face[i]] - elem->Vert_X[vert04face[i]];
      tau1[1] = elem->Vert_Y[vert24face[i]] - elem->Vert_Y[vert04face[i]];
      tau1[2] = elem->Vert_Z[vert24face[i]] - elem->Vert_Z[vert04face[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_X[vert04face[i]] + QuadX236[j] * elem->Vert_X[vert14face[i]] + QuadY236[j] * elem->Vert_X[vert24face[i]];
         coord[1] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Y[vert04face[i]] + QuadX236[j] * elem->Vert_Y[vert14face[i]] + QuadY236[j] * elem->Vert_Y[vert24face[i]];
         coord[2] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Z[vert04face[i]] + QuadX236[j] * elem->Vert_Z[vert14face[i]] + QuadY236[j] * elem->Vert_Z[vert24face[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 面上有六个自由度
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j xi
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j xi
         for (k = 0; k < num_fun; k++)
         {
            values[k + (12 * i + 24) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 1) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 2) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j] * QuadY236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 3) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 4) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] * QuadY236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 5) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] * QuadX236[j] / line_length[line4face1[i]];
            values[k + (12 * i + 24 + 6) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] / line_length[line4face2[i]];
            values[k + (12 * i + 24 + 7) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j] / line_length[line4face2[i]];
            values[k + (12 * i + 24 + 8) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j] * QuadY236[j] / line_length[line4face2[i]];
            values[k + (12 * i + 24 + 9) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] / line_length[line4face2[i]];
            values[k + (12 * i + 24 + 10) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] * QuadY236[j] / line_length[line4face2[i]];
            values[k + (12 * i + 24 + 11) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] * QuadX236[j] / line_length[line4face2[i]];
         }
      }
   }
#endif
   // 体上的自由度
   NumPoints = 304;
   DOUBLE QuadX304[304] = {.32967147384406063619375162258031196,
                           1. - 3. * .32967147384406063619375162258031196,
                           .32967147384406063619375162258031196,
                           .32967147384406063619375162258031196,
                           .11204210441737877557977355269085785,
                           1. - 3. * .11204210441737877557977355269085785,
                           .11204210441737877557977355269085785,
                           .11204210441737877557977355269085785,
                           .28044602591109291917326968759374922,
                           1. - 3. * .28044602591109291917326968759374922,
                           .28044602591109291917326968759374922,
                           .28044602591109291917326968759374922,
                           .03942164444076165530186041315194336,
                           1. - 3. * .03942164444076165530186041315194336,
                           .03942164444076165530186041315194336,
                           .03942164444076165530186041315194336,
                           .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .07491741856476755331936887653337858,
                           .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .33569310295563458148449388353009909,
                           .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           .76468706758018040128352585522097679,
                           .76468706758018040128352585522097679,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           .76468706758018040128352585522097679,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           .23282680458942511289537501209852151,
                           .23282680458942511289537501209852151,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           .23282680458942511289537501209852151,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           .28324176830779468643565585504428986,
                           .28324176830779468643565585504428986,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           .28324176830779468643565585504428986,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           .01283187405611823970074593204830391,
                           .01283187405611823970074593204830391,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           .01283187405611823970074593204830391,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           .38727096031949030862955433360477075,
                           .38727096031949030862955433360477075,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           .38727096031949030862955433360477075,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           .03723805935523541954759090988386045,
                           .03723805935523541954759090988386045,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           .03723805935523541954759090988386045,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           .74829410783088587547640692441989412,
                           .74829410783088587547640692441989412,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           .74829410783088587547640692441989412,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,

                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740};
   DOUBLE QuadY304[304] = {
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       1. - 3. * .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       1. - 3. * .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       1. - 3. * .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       1. - 3. * .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .76468706758018040128352585522097679,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .23282680458942511289537501209852151,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .28324176830779468643565585504428986,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .01283187405611823970074593204830391,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .38727096031949030862955433360477075,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .03723805935523541954759090988386045,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .74829410783088587547640692441989412,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
   };
   DOUBLE QuadZ304[304] = {
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       1. - 3. * .32967147384406063619375162258031196,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       1. - 3. * .11204210441737877557977355269085785,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       1. - 3. * .28044602591109291917326968759374922,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       1. - 3. * .03942164444076165530186041315194336,
       .5 - .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .76468706758018040128352585522097679,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .23282680458942511289537501209852151,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .28324176830779468643565585504428986,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .01283187405611823970074593204830391,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .38727096031949030862955433360477075,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .03723805935523541954759090988386045,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .74829410783088587547640692441989412,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .08011846127872502380489538529429122,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .31025854986272726854690199497600369,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .10852408019289846676496859793631046,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .24838249878149546329329939696635675,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .39600912110670350629865233312005293,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .06367516197137305787610880659547363,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .17576504661391044061489056539266476,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .47799425320067043636083307018835951,
   };
   DOUBLE QuadW304[304] = {.00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535};
   // if (elem->Volumn < 0.0)
   // {
      elem->Jacobian[0][0] = elem->Vert_X[1] - elem->Vert_X[0];
      elem->Jacobian[0][1] = elem->Vert_X[2] - elem->Vert_X[0];
      elem->Jacobian[0][2] = elem->Vert_X[3] - elem->Vert_X[0];
      elem->Jacobian[1][0] = elem->Vert_Y[1] - elem->Vert_Y[0];
      elem->Jacobian[1][1] = elem->Vert_Y[2] - elem->Vert_Y[0];
      elem->Jacobian[1][2] = elem->Vert_Y[3] - elem->Vert_Y[0];
      elem->Jacobian[2][0] = elem->Vert_Z[1] - elem->Vert_Z[0];
      elem->Jacobian[2][1] = elem->Vert_Z[2] - elem->Vert_Z[0];
      elem->Jacobian[2][2] = elem->Vert_Z[3] - elem->Vert_Z[0];
   // }
   for (k = 0; k < num_fun; k++)
   {
      for (j = 0; j < 12; j++)
         values[k + (72 + j) * num_fun] = 0.0;
   }
   for (j = 0; j < NumPoints; j++)
   {
      coord[0] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_X[0]+QuadX304[j]*elem->Vert_X[1]+QuadY304[j]*elem->Vert_X[2]+QuadZ304[j]*elem->Vert_X[3];
      coord[1] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_Y[0]+QuadX304[j]*elem->Vert_Y[1]+QuadY304[j]*elem->Vert_Y[2]+QuadZ304[j]*elem->Vert_Y[3];
      coord[2] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_Z[0]+QuadX304[j]*elem->Vert_Z[1]+QuadY304[j]*elem->Vert_Z[2]+QuadZ304[j]*elem->Vert_Z[3];
      // u(t_j)
      fun(coord, dim, values_temp);
      // 面上有三个自由度
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[1;0;0]) dA = \sum_j 1/det(B_K)(B_K[1;0;0]) \omega_j |A|
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[0;1;0]) dA = \sum_j 1/det(B_K)(B_K[0;1;0]) \omega_j |A|
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[0;0;1]) dA = \sum_j 1/det(B_K)(B_K[0;0;1]) \omega_j |A|
#if NEDELECSCALE == 0
      for (k = 0; k < num_fun; k++)
      {
         values[k + 72 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * 1.0 / 6.0;
         values[k + 73 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * 1.0 / 6.0;
         values[k + 74 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * 1.0 / 6.0;
         values[k + 75 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0;
         values[k + 76 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0;
         values[k + 77 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0;
         values[k + 78 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0;
         values[k + 79 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0;
         values[k + 80 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0;
         values[k + 81 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0;
         values[k + 82 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0;
         values[k + 83 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0;
      }
#else
      for (k = 0; k < num_fun; k++)
      {
         values[k + 72 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * 1.0 / 6.0 / line_length[0];
         values[k + 73 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * 1.0 / 6.0 / line_length[1];
         values[k + 74 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * 1.0 / 6.0 / line_length[2];
         values[k + 75 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0 / line_length[0];
         values[k + 76 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0 / line_length[1];
         values[k + 77 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadZ304[j] * 1.0 / 6.0 / line_length[2];
         values[k + 78 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0 / line_length[0];
         values[k + 79 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0 / line_length[1];
         values[k + 80 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadY304[j] * 1.0 / 6.0 / line_length[2];
         values[k + 81 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0 / line_length[0];
         values[k + 82 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0 / line_length[1];
         values[k + 83 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * QuadX304[j] * 1.0 / 6.0 / line_length[2];
      }
#endif
   }
   free(values_temp);
}
#endif
