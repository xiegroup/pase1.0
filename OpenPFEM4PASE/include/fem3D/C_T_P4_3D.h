#ifndef __CTP43D__
#define __CTP43D__
#include <stdbool.h>
#include "mesh.h"
#include "element.h"
#include "enumerations.h"
#include "constants.h"
// ***********************************************************************
// P4 element, conforming, 3D
// ***********************************************************************
static INT C_T_P4_3D_dof[4] = {1,3,3,1};
static INT C_T_P4_3D_Num_Bas = 35;
static INT C_T_P4_3D_Value_Dim = 1;  //1: 标量的基函数， 3: 向量类型的基函数
static INT C_T_P4_3D_Inter_Dim = 1;
static INT C_T_P4_3D_Polydeg = 4;  
static bool C_T_P4_3D_IsOnlyDependOnRefCoord = 1;
static INT C_T_P4_3D_Accuracy = 4; 
static MAPTYPE C_T_P4_3D_Maptype = Affine;

static void C_T_P4_3D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (200*x*x*y*y)/3 - (25*y)/3 - (25*z)/3 - (25*x)/3 + (200*x*x*z*z)/3 + (200*y*y*z*z)/3 + (148*x*y)/3 + (148*x*z)/3 + (148*y*z)/3 - 84*x*y*y - 84*x*x*y + 44*x*y*y*y + 44*x*x*x*y - 84*x*z*z - 84*x*x*z + 44*x*z*z*z + 44*x*x*x*z - 84*y*z*z - 84*y*y*z + 44*y*z*z*z + 44*y*y*y*z + (70*x*x)/3 - (80*x*x*x)/3 + (32*x*x*x*x)/3 + (70*y*y)/3 - (80*y*y*y)/3 + (32*y*y*y*y)/3 + (70*z*z)/3 - (80*z*z*z)/3 + (32*z*z*z*z)/3 + (464*x*y*z*z)/3 + (464*x*y*y*z)/3 + (464*x*x*y*z)/3 - 192*x*y*z + 1.0;
   values[1] = (4*x*y)/3 - (4*x*x*y*y)/3 - (4*x*x*z*z)/3 - x + (4*x*z)/3 - (4*x*y*y)/3 - (4*x*x*x*y)/3 - (4*x*z*z)/3 - (4*x*x*x*z)/3 + (22*x*x)/3 - 16*x*x*x + (32*x*x*x*x)/3 + 20*x*y*z*z + 20*x*y*y*z + (56*x*x*y*z)/3 - (64*x*y*z)/3;
   values[2] = (4*x*y)/3 - (4*x*x*y*y)/3 - (4*y*y*z*z)/3 - y + (4*y*z)/3 - (4*x*x*y)/3 - (4*x*y*y*y)/3 - (4*y*z*z)/3 - (4*y*y*y*z)/3 + (22*y*y)/3 - 16*y*y*y + (32*y*y*y*y)/3 + 20*x*y*z*z + (56*x*y*y*z)/3 + 20*x*x*y*z - (64*x*y*z)/3;
   values[3] = (4*x*z)/3 - (4*x*x*z*z)/3 - (4*y*y*z*z)/3 - z + (4*y*z)/3 - (4*x*x*z)/3 - (4*x*z*z*z)/3 - (4*y*y*z)/3 - (4*y*z*z*z)/3 + (22*z*z)/3 - 16*z*z*z + (32*z*z*z*z)/3 + (56*x*y*z*z)/3 + 20*x*y*y*z + 20*x*x*y*z - (64*x*y*z)/3;
   values[4] = 16*x - (544*x*x*y*y)/3 - (544*x*x*z*z)/3 - 96*x*y - 96*x*z + (464*x*y*y)/3 + 240*x*x*y - (224*x*y*y*y)/3 - (448*x*x*x*y)/3 + (464*x*z*z)/3 + 240*x*x*z - (224*x*z*z*z)/3 - (448*x*x*x*z)/3 - (208*x*x)/3 + 96*x*x*x - (128*x*x*x*x)/3 - (800*x*y*z*z)/3 - (800*x*y*y*z)/3 - (1216*x*x*y*z)/3 + 352*x*y*z;
   values[5] = 92*x*x*y*y - 12*x + 92*x*x*z*z + 46*x*y + 46*x*z - 62*x*y*y - 162*x*x*y + 28*x*y*y*y + 128*x*x*x*y - 62*x*z*z - 162*x*x*z + 28*x*z*z*z + 128*x*x*x*z + 76*x*x - 128*x*x*x + 64*x*x*x*x + 116*x*y*z*z + 116*x*y*y*z + 216*x*x*y*z - 156*x*y*z;
   values[6] = (16*x)/3 + (32*x*x*y*y)/3 + (32*x*x*z*z)/3 - (32*x*y)/3 - (32*x*z)/3 + 16*x*y*y + 16*x*x*y - (32*x*y*y*y)/3 - (64*x*x*x*y)/3 + 16*x*z*z + 16*x*x*z - (32*x*z*z*z)/3 - (64*x*x*x*z)/3 - (112*x*x)/3 + (224*x*x*x)/3 - (128*x*x*x*x)/3 - (224*x*y*z*z)/3 - (224*x*y*y*z)/3 - (64*x*x*y*z)/3 + (224*x*y*z)/3;
   values[7] = 16*y - (544*x*x*y*y)/3 - (544*y*y*z*z)/3 - 96*x*y - 96*y*z + 240*x*y*y + (464*x*x*y)/3 - (448*x*y*y*y)/3 - (224*x*x*x*y)/3 + (464*y*z*z)/3 + 240*y*y*z - (224*y*z*z*z)/3 - (448*y*y*y*z)/3 - (208*y*y)/3 + 96*y*y*y - (128*y*y*y*y)/3 - (800*x*y*z*z)/3 - (1216*x*y*y*z)/3 - (800*x*x*y*z)/3 + 352*x*y*z;
   values[8] = 92*x*x*y*y - 12*y + 92*y*y*z*z + 46*x*y + 46*y*z - 162*x*y*y - 62*x*x*y + 128*x*y*y*y + 28*x*x*x*y - 62*y*z*z - 162*y*y*z + 28*y*z*z*z + 128*y*y*y*z + 76*y*y - 128*y*y*y + 64*y*y*y*y + 116*x*y*z*z + 216*x*y*y*z + 116*x*x*y*z - 156*x*y*z;
   values[9] = (16*y)/3 + (32*x*x*y*y)/3 + (32*y*y*z*z)/3 - (32*x*y)/3 - (32*y*z)/3 + 16*x*y*y + 16*x*x*y - (64*x*y*y*y)/3 - (32*x*x*x*y)/3 + 16*y*z*z + 16*y*y*z - (32*y*z*z*z)/3 - (64*y*y*y*z)/3 - (112*y*y)/3 + (224*y*y*y)/3 - (128*y*y*y*y)/3 - (224*x*y*z*z)/3 - (64*x*y*y*z)/3 - (224*x*x*y*z)/3 + (224*x*y*z)/3;
   values[10] = 16*z - (544*x*x*z*z)/3 - (544*y*y*z*z)/3 - 96*x*z - 96*y*z + 240*x*z*z + (464*x*x*z)/3 - (448*x*z*z*z)/3 - (224*x*x*x*z)/3 + 240*y*z*z + (464*y*y*z)/3 - (448*y*z*z*z)/3 - (224*y*y*y*z)/3 - (208*z*z)/3 + 96*z*z*z - (128*z*z*z*z)/3 - (1216*x*y*z*z)/3 - (800*x*y*y*z)/3 - (800*x*x*y*z)/3 + 352*x*y*z;
   values[11] = 92*x*x*z*z - 12*z + 92*y*y*z*z + 46*x*z + 46*y*z - 162*x*z*z - 62*x*x*z + 128*x*z*z*z + 28*x*x*x*z - 162*y*z*z - 62*y*y*z + 128*y*z*z*z + 28*y*y*y*z + 76*z*z - 128*z*z*z + 64*z*z*z*z + 216*x*y*z*z + 116*x*y*y*z + 116*x*x*y*z - 156*x*y*z;
   values[12] = (16*z)/3 + (32*x*x*z*z)/3 + (32*y*y*z*z)/3 - (32*x*z)/3 - (32*y*z)/3 + 16*x*z*z + 16*x*x*z - (64*x*z*z*z)/3 - (32*x*x*x*z)/3 + 16*y*z*z + 16*y*y*z - (64*y*z*z*z)/3 - (32*y*y*y*z)/3 - (112*z*z)/3 + (224*z*z*z)/3 - (128*z*z*z*z)/3 - (64*x*y*z*z)/3 - (224*x*y*y*z)/3 - (224*x*x*y*z)/3 + (224*x*y*z)/3;
   values[13] = (128*x*x*y*y)/3 + (32*x*y)/3 - 16*x*y*y - (208*x*x*y)/3 + (32*x*y*y*y)/3 + (224*x*x*x*y)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[14] = 8*x*x*y*y - 6*x*y + 22*x*y*y + 22*x*x*y - 28*x*y*y*y - 28*x*x*x*y + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[15] = (128*x*x*y*y)/3 + (32*x*y)/3 - (208*x*y*y)/3 - 16*x*x*y + (224*x*y*y*y)/3 + (32*x*x*x*y)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[16] = (128*x*x*z*z)/3 + (32*x*z)/3 - 16*x*z*z - (208*x*x*z)/3 + (32*x*z*z*z)/3 + (224*x*x*x*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[17] = 8*x*x*z*z - 6*x*z + 22*x*z*z + 22*x*x*z - 28*x*z*z*z - 28*x*x*x*z + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[18] = (128*x*x*z*z)/3 + (32*x*z)/3 - (208*x*z*z)/3 - 16*x*x*z + (224*x*z*z*z)/3 + (32*x*x*x*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[19] = (128*y*y*z*z)/3 + (32*y*z)/3 - 16*y*z*z - (208*y*y*z)/3 + (32*y*z*z*z)/3 + (224*y*y*y*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[20] = 8*y*y*z*z - 6*y*z + 22*y*z*z + 22*y*y*z - 28*y*z*z*z - 28*y*y*y*z + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[21] = (128*y*y*z*z)/3 + (32*y*z)/3 - (208*y*z*z)/3 - 16*y*y*z + (224*y*z*z*z)/3 + (32*y*y*y*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[22] = 36*x*y*z*z + 36*x*y*y*z + 144*x*x*y*z - 54*x*y*z;
   values[23] = 36*x*y*z*z + 144*x*y*y*z + 36*x*x*y*z - 54*x*y*z;
   values[24] = 144*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 54*x*y*z;
   values[25] = 216*y*y*z*z + 90*y*z - 198*y*z*z - 198*y*y*z + 108*y*z*z*z + 108*y*y*y*z + 252*x*y*z*z + 252*x*y*y*z + 144*x*x*y*z - 234*x*y*z;
   values[26] = 126*y*z*z - 18*y*z - 108*y*y*z*z + 18*y*y*z - 108*y*z*z*z - 72*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[27] = 18*y*z*z - 18*y*z - 108*y*y*z*z + 126*y*y*z - 108*y*y*y*z + 36*x*y*z*z - 72*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[28] = 216*x*x*z*z + 90*x*z - 198*x*z*z - 198*x*x*z + 108*x*z*z*z + 108*x*x*x*z + 252*x*y*z*z + 144*x*y*y*z + 252*x*x*y*z - 234*x*y*z;
   values[29] = 18*x*z*z - 18*x*z - 108*x*x*z*z + 126*x*x*z - 108*x*x*x*z + 36*x*y*z*z + 36*x*y*y*z - 72*x*x*y*z - 18*x*y*z;
   values[30] = 126*x*z*z - 18*x*z - 108*x*x*z*z + 18*x*x*z - 108*x*z*z*z - 72*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[31] = 216*x*x*y*y + 90*x*y - 198*x*y*y - 198*x*x*y + 108*x*y*y*y + 108*x*x*x*y + 144*x*y*z*z + 252*x*y*y*z + 252*x*x*y*z - 234*x*y*z;
   values[32] = 126*x*y*y - 18*x*y - 108*x*x*y*y + 18*x*x*y - 108*x*y*y*y + 36*x*y*z*z - 72*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[33] = 18*x*y*y - 18*x*y - 108*x*x*y*y + 126*x*x*y - 108*x*x*x*y + 36*x*y*z*z + 36*x*y*y*z - 72*x*x*y*z - 18*x*y*z;
   values[34] = 256*x*y*z - 256*x*y*y*z - 256*x*x*y*z - 256*x*y*z*z;
}

static void C_T_P4_3D_D000(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (200*x*x*y*y)/3 - (25*y)/3 - (25*z)/3 - (25*x)/3 + (200*x*x*z*z)/3 + (200*y*y*z*z)/3 + (148*x*y)/3 + (148*x*z)/3 + (148*y*z)/3 - 84*x*y*y - 84*x*x*y + 44*x*y*y*y + 44*x*x*x*y - 84*x*z*z - 84*x*x*z + 44*x*z*z*z + 44*x*x*x*z - 84*y*z*z - 84*y*y*z + 44*y*z*z*z + 44*y*y*y*z + (70*x*x)/3 - (80*x*x*x)/3 + (32*x*x*x*x)/3 + (70*y*y)/3 - (80*y*y*y)/3 + (32*y*y*y*y)/3 + (70*z*z)/3 - (80*z*z*z)/3 + (32*z*z*z*z)/3 + (464*x*y*z*z)/3 + (464*x*y*y*z)/3 + (464*x*x*y*z)/3 - 192*x*y*z + 1.0;
   values[1] = (4*x*y)/3 - (4*x*x*y*y)/3 - (4*x*x*z*z)/3 - x + (4*x*z)/3 - (4*x*y*y)/3 - (4*x*x*x*y)/3 - (4*x*z*z)/3 - (4*x*x*x*z)/3 + (22*x*x)/3 - 16*x*x*x + (32*x*x*x*x)/3 + 20*x*y*z*z + 20*x*y*y*z + (56*x*x*y*z)/3 - (64*x*y*z)/3;
   values[2] = (4*x*y)/3 - (4*x*x*y*y)/3 - (4*y*y*z*z)/3 - y + (4*y*z)/3 - (4*x*x*y)/3 - (4*x*y*y*y)/3 - (4*y*z*z)/3 - (4*y*y*y*z)/3 + (22*y*y)/3 - 16*y*y*y + (32*y*y*y*y)/3 + 20*x*y*z*z + (56*x*y*y*z)/3 + 20*x*x*y*z - (64*x*y*z)/3;
   values[3] = (4*x*z)/3 - (4*x*x*z*z)/3 - (4*y*y*z*z)/3 - z + (4*y*z)/3 - (4*x*x*z)/3 - (4*x*z*z*z)/3 - (4*y*y*z)/3 - (4*y*z*z*z)/3 + (22*z*z)/3 - 16*z*z*z + (32*z*z*z*z)/3 + (56*x*y*z*z)/3 + 20*x*y*y*z + 20*x*x*y*z - (64*x*y*z)/3;
   values[4] = 16*x - (544*x*x*y*y)/3 - (544*x*x*z*z)/3 - 96*x*y - 96*x*z + (464*x*y*y)/3 + 240*x*x*y - (224*x*y*y*y)/3 - (448*x*x*x*y)/3 + (464*x*z*z)/3 + 240*x*x*z - (224*x*z*z*z)/3 - (448*x*x*x*z)/3 - (208*x*x)/3 + 96*x*x*x - (128*x*x*x*x)/3 - (800*x*y*z*z)/3 - (800*x*y*y*z)/3 - (1216*x*x*y*z)/3 + 352*x*y*z;
   values[5] = 92*x*x*y*y - 12*x + 92*x*x*z*z + 46*x*y + 46*x*z - 62*x*y*y - 162*x*x*y + 28*x*y*y*y + 128*x*x*x*y - 62*x*z*z - 162*x*x*z + 28*x*z*z*z + 128*x*x*x*z + 76*x*x - 128*x*x*x + 64*x*x*x*x + 116*x*y*z*z + 116*x*y*y*z + 216*x*x*y*z - 156*x*y*z;
   values[6] = (16*x)/3 + (32*x*x*y*y)/3 + (32*x*x*z*z)/3 - (32*x*y)/3 - (32*x*z)/3 + 16*x*y*y + 16*x*x*y - (32*x*y*y*y)/3 - (64*x*x*x*y)/3 + 16*x*z*z + 16*x*x*z - (32*x*z*z*z)/3 - (64*x*x*x*z)/3 - (112*x*x)/3 + (224*x*x*x)/3 - (128*x*x*x*x)/3 - (224*x*y*z*z)/3 - (224*x*y*y*z)/3 - (64*x*x*y*z)/3 + (224*x*y*z)/3;
   values[7] = 16*y - (544*x*x*y*y)/3 - (544*y*y*z*z)/3 - 96*x*y - 96*y*z + 240*x*y*y + (464*x*x*y)/3 - (448*x*y*y*y)/3 - (224*x*x*x*y)/3 + (464*y*z*z)/3 + 240*y*y*z - (224*y*z*z*z)/3 - (448*y*y*y*z)/3 - (208*y*y)/3 + 96*y*y*y - (128*y*y*y*y)/3 - (800*x*y*z*z)/3 - (1216*x*y*y*z)/3 - (800*x*x*y*z)/3 + 352*x*y*z;
   values[8] = 92*x*x*y*y - 12*y + 92*y*y*z*z + 46*x*y + 46*y*z - 162*x*y*y - 62*x*x*y + 128*x*y*y*y + 28*x*x*x*y - 62*y*z*z - 162*y*y*z + 28*y*z*z*z + 128*y*y*y*z + 76*y*y - 128*y*y*y + 64*y*y*y*y + 116*x*y*z*z + 216*x*y*y*z + 116*x*x*y*z - 156*x*y*z;
   values[9] = (16*y)/3 + (32*x*x*y*y)/3 + (32*y*y*z*z)/3 - (32*x*y)/3 - (32*y*z)/3 + 16*x*y*y + 16*x*x*y - (64*x*y*y*y)/3 - (32*x*x*x*y)/3 + 16*y*z*z + 16*y*y*z - (32*y*z*z*z)/3 - (64*y*y*y*z)/3 - (112*y*y)/3 + (224*y*y*y)/3 - (128*y*y*y*y)/3 - (224*x*y*z*z)/3 - (64*x*y*y*z)/3 - (224*x*x*y*z)/3 + (224*x*y*z)/3;
   values[10] = 16*z - (544*x*x*z*z)/3 - (544*y*y*z*z)/3 - 96*x*z - 96*y*z + 240*x*z*z + (464*x*x*z)/3 - (448*x*z*z*z)/3 - (224*x*x*x*z)/3 + 240*y*z*z + (464*y*y*z)/3 - (448*y*z*z*z)/3 - (224*y*y*y*z)/3 - (208*z*z)/3 + 96*z*z*z - (128*z*z*z*z)/3 - (1216*x*y*z*z)/3 - (800*x*y*y*z)/3 - (800*x*x*y*z)/3 + 352*x*y*z;
   values[11] = 92*x*x*z*z - 12*z + 92*y*y*z*z + 46*x*z + 46*y*z - 162*x*z*z - 62*x*x*z + 128*x*z*z*z + 28*x*x*x*z - 162*y*z*z - 62*y*y*z + 128*y*z*z*z + 28*y*y*y*z + 76*z*z - 128*z*z*z + 64*z*z*z*z + 216*x*y*z*z + 116*x*y*y*z + 116*x*x*y*z - 156*x*y*z;
   values[12] = (16*z)/3 + (32*x*x*z*z)/3 + (32*y*y*z*z)/3 - (32*x*z)/3 - (32*y*z)/3 + 16*x*z*z + 16*x*x*z - (64*x*z*z*z)/3 - (32*x*x*x*z)/3 + 16*y*z*z + 16*y*y*z - (64*y*z*z*z)/3 - (32*y*y*y*z)/3 - (112*z*z)/3 + (224*z*z*z)/3 - (128*z*z*z*z)/3 - (64*x*y*z*z)/3 - (224*x*y*y*z)/3 - (224*x*x*y*z)/3 + (224*x*y*z)/3;
   values[13] = (128*x*x*y*y)/3 + (32*x*y)/3 - 16*x*y*y - (208*x*x*y)/3 + (32*x*y*y*y)/3 + (224*x*x*x*y)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[14] = 8*x*x*y*y - 6*x*y + 22*x*y*y + 22*x*x*y - 28*x*y*y*y - 28*x*x*x*y + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[15] = (128*x*x*y*y)/3 + (32*x*y)/3 - (208*x*y*y)/3 - 16*x*x*y + (224*x*y*y*y)/3 + (32*x*x*x*y)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[16] = (128*x*x*z*z)/3 + (32*x*z)/3 - 16*x*z*z - (208*x*x*z)/3 + (32*x*z*z*z)/3 + (224*x*x*x*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[17] = 8*x*x*z*z - 6*x*z + 22*x*z*z + 22*x*x*z - 28*x*z*z*z - 28*x*x*x*z + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[18] = (128*x*x*z*z)/3 + (32*x*z)/3 - (208*x*z*z)/3 - 16*x*x*z + (224*x*z*z*z)/3 + (32*x*x*x*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[19] = (128*y*y*z*z)/3 + (32*y*z)/3 - 16*y*z*z - (208*y*y*z)/3 + (32*y*z*z*z)/3 + (224*y*y*y*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[20] = 8*y*y*z*z - 6*y*z + 22*y*z*z + 22*y*y*z - 28*y*z*z*z - 28*y*y*y*z + 32*x*y*z*z + 32*x*y*y*z + 32*x*x*y*z - 32*x*y*z;
   values[21] = (128*y*y*z*z)/3 + (32*y*z)/3 - (208*y*z*z)/3 - 16*y*y*z + (224*y*z*z*z)/3 + (32*y*y*y*z)/3 - (128*x*y*z*z)/3 - (128*x*y*y*z)/3 - (128*x*x*y*z)/3 + (128*x*y*z)/3;
   values[22] = 36*x*y*z*z + 36*x*y*y*z + 144*x*x*y*z - 54*x*y*z;
   values[23] = 36*x*y*z*z + 144*x*y*y*z + 36*x*x*y*z - 54*x*y*z;
   values[24] = 144*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 54*x*y*z;
   values[25] = 216*y*y*z*z + 90*y*z - 198*y*z*z - 198*y*y*z + 108*y*z*z*z + 108*y*y*y*z + 252*x*y*z*z + 252*x*y*y*z + 144*x*x*y*z - 234*x*y*z;
   values[26] = 126*y*z*z - 18*y*z - 108*y*y*z*z + 18*y*y*z - 108*y*z*z*z - 72*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[27] = 18*y*z*z - 18*y*z - 108*y*y*z*z + 126*y*y*z - 108*y*y*y*z + 36*x*y*z*z - 72*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[28] = 216*x*x*z*z + 90*x*z - 198*x*z*z - 198*x*x*z + 108*x*z*z*z + 108*x*x*x*z + 252*x*y*z*z + 144*x*y*y*z + 252*x*x*y*z - 234*x*y*z;
   values[29] = 18*x*z*z - 18*x*z - 108*x*x*z*z + 126*x*x*z - 108*x*x*x*z + 36*x*y*z*z + 36*x*y*y*z - 72*x*x*y*z - 18*x*y*z;
   values[30] = 126*x*z*z - 18*x*z - 108*x*x*z*z + 18*x*x*z - 108*x*z*z*z - 72*x*y*z*z + 36*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[31] = 216*x*x*y*y + 90*x*y - 198*x*y*y - 198*x*x*y + 108*x*y*y*y + 108*x*x*x*y + 144*x*y*z*z + 252*x*y*y*z + 252*x*x*y*z - 234*x*y*z;
   values[32] = 126*x*y*y - 18*x*y - 108*x*x*y*y + 18*x*x*y - 108*x*y*y*y + 36*x*y*z*z - 72*x*y*y*z + 36*x*x*y*z - 18*x*y*z;
   values[33] = 18*x*y*y - 18*x*y - 108*x*x*y*y + 126*x*x*y - 108*x*x*x*y + 36*x*y*z*z + 36*x*y*y*z - 72*x*x*y*z - 18*x*y*z;
   values[34] = 256*x*y*z - 256*x*y*y*z - 256*x*x*y*z - 256*x*y*z*z;
}
static void C_T_P4_3D_D100(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (140*x)/3 + (148*y)/3 + (148*z)/3 - 168*x*y - 168*x*z - 192*y*z + (400*x*y*y)/3 + 132*x*x*y + (400*x*z*z)/3 + 132*x*x*z + (464*y*z*z)/3 + (464*y*y*z)/3 - 80*x*x + (128*x*x*x)/3 - 84*y*y + 44*y*y*y - 84*z*z + 44*z*z*z + (928*x*y*z)/3 - 25.0/3.0;
   values[1] = (44*x)/3 + (4*y)/3 + (4*z)/3 - (64*y*z)/3 - (8*x*y*y)/3 - 4*x*x*y - (8*x*z*z)/3 - 4*x*x*z + 20*y*z*z + 20*y*y*z - 48*x*x + (128*x*x*x)/3 - (4*y*y)/3 - (4*z*z)/3 + (112*x*y*z)/3 - 1.0;
   values[2] = (4*y)/3 - (8*x*y)/3 - (64*y*z)/3 - (8*x*y*y)/3 + 20*y*z*z + (56*y*y*z)/3 - (4*y*y*y)/3 + 40*x*y*z;
   values[3] = (4*z)/3 - (8*x*z)/3 - (64*y*z)/3 - (8*x*z*z)/3 + (56*y*z*z)/3 + 20*y*y*z - (4*z*z*z)/3 + 40*x*y*z;
   values[4] = 480*x*y - 96*y - 96*z - (416*x)/3 + 480*x*z + 352*y*z - (1088*x*y*y)/3 - 448*x*x*y - (1088*x*z*z)/3 - 448*x*x*z - (800*y*z*z)/3 - (800*y*y*z)/3 + 288*x*x - (512*x*x*x)/3 + (464*y*y)/3 - (224*y*y*y)/3 + (464*z*z)/3 - (224*z*z*z)/3 - (2432*x*y*z)/3 + 16.0;
   values[5] = 152*x + 46*y + 46*z - 324*x*y - 324*x*z - 156*y*z + 184*x*y*y + 384*x*x*y + 184*x*z*z + 384*x*x*z + 116*y*z*z + 116*y*y*z - 384*x*x + 256*x*x*x - 62*y*y + 28*y*y*y - 62*z*z + 28*z*z*z + 432*x*y*z - 12.0;
   values[6] = 32*x*y - (32*y)/3 - (32*z)/3 - (224*x)/3 + 32*x*z + (224*y*z)/3 + (64*x*y*y)/3 - 64*x*x*y + (64*x*z*z)/3 - 64*x*x*z - (224*y*z*z)/3 - (224*y*y*z)/3 + 224*x*x - (512*x*x*x)/3 + 16*y*y - (32*y*y*y)/3 + 16*z*z - (32*z*z*z)/3 - (128*x*y*z)/3 + 16.0/3.0;
   values[7] = (928*x*y)/3 - 96*y + 352*y*z - (1088*x*y*y)/3 - 224*x*x*y - (800*y*z*z)/3 - (1216*y*y*z)/3 + 240*y*y - (448*y*y*y)/3 - (1600*x*y*z)/3;
   values[8] = 46*y - 124*x*y - 156*y*z + 184*x*y*y + 84*x*x*y + 116*y*z*z + 216*y*y*z - 162*y*y + 128*y*y*y + 232*x*y*z;
   values[9] = 32*x*y - (32*y)/3 + (224*y*z)/3 + (64*x*y*y)/3 - 32*x*x*y - (224*y*z*z)/3 - (64*y*y*z)/3 + 16*y*y - (64*y*y*y)/3 - (448*x*y*z)/3;
   values[10] = (928*x*z)/3 - 96*z + 352*y*z - (1088*x*z*z)/3 - 224*x*x*z - (1216*y*z*z)/3 - (800*y*y*z)/3 + 240*z*z - (448*z*z*z)/3 - (1600*x*y*z)/3;
   values[11] = 46*z - 124*x*z - 156*y*z + 184*x*z*z + 84*x*x*z + 216*y*z*z + 116*y*y*z - 162*z*z + 128*z*z*z + 232*x*y*z;
   values[12] = 32*x*z - (32*z)/3 + (224*y*z)/3 + (64*x*z*z)/3 - 32*x*x*z - (64*y*z*z)/3 - (224*y*y*z)/3 + 16*z*z - (64*z*z*z)/3 - (448*x*y*z)/3;
   values[13] = (32*y)/3 - (416*x*y)/3 + (128*y*z)/3 + (256*x*y*y)/3 + 224*x*x*y - (128*y*z*z)/3 - (128*y*y*z)/3 - 16*y*y + (32*y*y*y)/3 - (256*x*y*z)/3;
   values[14] = 44*x*y - 6*y - 32*y*z + 16*x*y*y - 84*x*x*y + 32*y*z*z + 32*y*y*z + 22*y*y - 28*y*y*y + 64*x*y*z;
   values[15] = (32*y)/3 - 32*x*y + (128*y*z)/3 + (256*x*y*y)/3 + 32*x*x*y - (128*y*z*z)/3 - (128*y*y*z)/3 - (208*y*y)/3 + (224*y*y*y)/3 - (256*x*y*z)/3;
   values[16] = (32*z)/3 - (416*x*z)/3 + (128*y*z)/3 + (256*x*z*z)/3 + 224*x*x*z - (128*y*z*z)/3 - (128*y*y*z)/3 - 16*z*z + (32*z*z*z)/3 - (256*x*y*z)/3;
   values[17] = 44*x*z - 6*z - 32*y*z + 16*x*z*z - 84*x*x*z + 32*y*z*z + 32*y*y*z + 22*z*z - 28*z*z*z + 64*x*y*z;
   values[18] = (32*z)/3 - 32*x*z + (128*y*z)/3 + (256*x*z*z)/3 + 32*x*x*z - (128*y*z*z)/3 - (128*y*y*z)/3 - (208*z*z)/3 + (224*z*z*z)/3 - (256*x*y*z)/3;
   values[19] = (128*y*z)/3 - (128*y*z*z)/3 - (128*y*y*z)/3 - (256*x*y*z)/3;
   values[20] = 32*y*z*z - 32*y*z + 32*y*y*z + 64*x*y*z;
   values[21] = (128*y*z)/3 - (128*y*z*z)/3 - (128*y*y*z)/3 - (256*x*y*z)/3;
   values[22] = 36*y*z*z - 54*y*z + 36*y*y*z + 288*x*y*z;
   values[23] = 36*y*z*z - 54*y*z + 144*y*y*z + 72*x*y*z;
   values[24] = 144*y*z*z - 54*y*z + 36*y*y*z + 72*x*y*z;
   values[25] = 252*y*z*z - 234*y*z + 252*y*y*z + 288*x*y*z;
   values[26] = 36*y*y*z - 72*y*z*z - 18*y*z + 72*x*y*z;
   values[27] = 36*y*z*z - 18*y*z - 72*y*y*z + 72*x*y*z;
   values[28] = 90*z - 396*x*z - 234*y*z + 432*x*z*z + 324*x*x*z + 252*y*z*z + 144*y*y*z - 198*z*z + 108*z*z*z + 504*x*y*z;
   values[29] = 252*x*z - 18*z - 18*y*z - 216*x*z*z - 324*x*x*z + 36*y*z*z + 36*y*y*z + 18*z*z - 144*x*y*z;
   values[30] = 36*x*z - 18*z - 18*y*z - 216*x*z*z - 72*y*z*z + 36*y*y*z + 126*z*z - 108*z*z*z + 72*x*y*z;
   values[31] = 90*y - 396*x*y - 234*y*z + 432*x*y*y + 324*x*x*y + 144*y*z*z + 252*y*y*z - 198*y*y + 108*y*y*y + 504*x*y*z;
   values[32] = 36*x*y - 18*y - 18*y*z - 216*x*y*y + 36*y*z*z - 72*y*y*z + 126*y*y - 108*y*y*y + 72*x*y*z;
   values[33] = 252*x*y - 18*y - 18*y*z - 216*x*y*y - 324*x*x*y + 36*y*z*z + 36*y*y*z + 18*y*y - 144*x*y*z;
   values[34] = 256*y*z - 256*y*z*z - 256*y*y*z - 512*x*y*z;
}
static void C_T_P4_3D_D010(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (148*x)/3 + (140*y)/3 + (148*z)/3 - 168*x*y - 192*x*z - 168*y*z + 132*x*y*y + (400*x*x*y)/3 + (464*x*z*z)/3 + (464*x*x*z)/3 + (400*y*z*z)/3 + 132*y*y*z - 84*x*x + 44*x*x*x - 80*y*y + (128*y*y*y)/3 - 84*z*z + 44*z*z*z + (928*x*y*z)/3 - 25.0/3.0;
   values[1] = (4*x)/3 - (8*x*y)/3 - (64*x*z)/3 - (8*x*x*y)/3 + 20*x*z*z + (56*x*x*z)/3 - (4*x*x*x)/3 + 40*x*y*z;
   values[2] = (4*x)/3 + (44*y)/3 + (4*z)/3 - (64*x*z)/3 - 4*x*y*y - (8*x*x*y)/3 + 20*x*z*z + 20*x*x*z - (8*y*z*z)/3 - 4*y*y*z - (4*x*x)/3 - 48*y*y + (128*y*y*y)/3 - (4*z*z)/3 + (112*x*y*z)/3 - 1.0;
   values[3] = (4*z)/3 - (64*x*z)/3 - (8*y*z)/3 + (56*x*z*z)/3 + 20*x*x*z - (8*y*z*z)/3 - (4*z*z*z)/3 + 40*x*y*z;
   values[4] = (928*x*y)/3 - 96*x + 352*x*z - 224*x*y*y - (1088*x*x*y)/3 - (800*x*z*z)/3 - (1216*x*x*z)/3 + 240*x*x - (448*x*x*x)/3 - (1600*x*y*z)/3;
   values[5] = 46*x - 124*x*y - 156*x*z + 84*x*y*y + 184*x*x*y + 116*x*z*z + 216*x*x*z - 162*x*x + 128*x*x*x + 232*x*y*z;
   values[6] = 32*x*y - (32*x)/3 + (224*x*z)/3 - 32*x*y*y + (64*x*x*y)/3 - (224*x*z*z)/3 - (64*x*x*z)/3 + 16*x*x - (64*x*x*x)/3 - (448*x*y*z)/3;
   values[7] = 480*x*y - (416*y)/3 - 96*z - 96*x + 352*x*z + 480*y*z - 448*x*y*y - (1088*x*x*y)/3 - (800*x*z*z)/3 - (800*x*x*z)/3 - (1088*y*z*z)/3 - 448*y*y*z + (464*x*x)/3 - (224*x*x*x)/3 + 288*y*y - (512*y*y*y)/3 + (464*z*z)/3 - (224*z*z*z)/3 - (2432*x*y*z)/3 + 16.0;
   values[8] = 46*x + 152*y + 46*z - 324*x*y - 156*x*z - 324*y*z + 384*x*y*y + 184*x*x*y + 116*x*z*z + 116*x*x*z + 184*y*z*z + 384*y*y*z - 62*x*x + 28*x*x*x - 384*y*y + 256*y*y*y - 62*z*z + 28*z*z*z + 432*x*y*z - 12.0;
   values[9] = 32*x*y - (224*y)/3 - (32*z)/3 - (32*x)/3 + (224*x*z)/3 + 32*y*z - 64*x*y*y + (64*x*x*y)/3 - (224*x*z*z)/3 - (224*x*x*z)/3 + (64*y*z*z)/3 - 64*y*y*z + 16*x*x - (32*x*x*x)/3 + 224*y*y - (512*y*y*y)/3 + 16*z*z - (32*z*z*z)/3 - (128*x*y*z)/3 + 16.0/3.0;
   values[10] = 352*x*z - 96*z + (928*y*z)/3 - (1216*x*z*z)/3 - (800*x*x*z)/3 - (1088*y*z*z)/3 - 224*y*y*z + 240*z*z - (448*z*z*z)/3 - (1600*x*y*z)/3;
   values[11] = 46*z - 156*x*z - 124*y*z + 216*x*z*z + 116*x*x*z + 184*y*z*z + 84*y*y*z - 162*z*z + 128*z*z*z + 232*x*y*z;
   values[12] = (224*x*z)/3 - (32*z)/3 + 32*y*z - (64*x*z*z)/3 - (224*x*x*z)/3 + (64*y*z*z)/3 - 32*y*y*z + 16*z*z - (64*z*z*z)/3 - (448*x*y*z)/3;
   values[13] = (32*x)/3 - 32*x*y + (128*x*z)/3 + 32*x*y*y + (256*x*x*y)/3 - (128*x*z*z)/3 - (128*x*x*z)/3 - (208*x*x)/3 + (224*x*x*x)/3 - (256*x*y*z)/3;
   values[14] = 44*x*y - 6*x - 32*x*z - 84*x*y*y + 16*x*x*y + 32*x*z*z + 32*x*x*z + 22*x*x - 28*x*x*x + 64*x*y*z;
   values[15] = (32*x)/3 - (416*x*y)/3 + (128*x*z)/3 + 224*x*y*y + (256*x*x*y)/3 - (128*x*z*z)/3 - (128*x*x*z)/3 - 16*x*x + (32*x*x*x)/3 - (256*x*y*z)/3;
   values[16] = (128*x*z)/3 - (128*x*z*z)/3 - (128*x*x*z)/3 - (256*x*y*z)/3;
   values[17] = 32*x*z*z - 32*x*z + 32*x*x*z + 64*x*y*z;
   values[18] = (128*x*z)/3 - (128*x*z*z)/3 - (128*x*x*z)/3 - (256*x*y*z)/3;
   values[19] = (32*z)/3 + (128*x*z)/3 - (416*y*z)/3 - (128*x*z*z)/3 - (128*x*x*z)/3 + (256*y*z*z)/3 + 224*y*y*z - 16*z*z + (32*z*z*z)/3 - (256*x*y*z)/3;
   values[20] = 44*y*z - 32*x*z - 6*z + 32*x*z*z + 32*x*x*z + 16*y*z*z - 84*y*y*z + 22*z*z - 28*z*z*z + 64*x*y*z;
   values[21] = (32*z)/3 + (128*x*z)/3 - 32*y*z - (128*x*z*z)/3 - (128*x*x*z)/3 + (256*y*z*z)/3 + 32*y*y*z - (208*z*z)/3 + (224*z*z*z)/3 - (256*x*y*z)/3;
   values[22] = 36*x*z*z - 54*x*z + 144*x*x*z + 72*x*y*z;
   values[23] = 36*x*z*z - 54*x*z + 36*x*x*z + 288*x*y*z;
   values[24] = 144*x*z*z - 54*x*z + 36*x*x*z + 72*x*y*z;
   values[25] = 90*z - 234*x*z - 396*y*z + 252*x*z*z + 144*x*x*z + 432*y*z*z + 324*y*y*z - 198*z*z + 108*z*z*z + 504*x*y*z;
   values[26] = 36*y*z - 18*x*z - 18*z - 72*x*z*z + 36*x*x*z - 216*y*z*z + 126*z*z - 108*z*z*z + 72*x*y*z;
   values[27] = 252*y*z - 18*x*z - 18*z + 36*x*z*z + 36*x*x*z - 216*y*z*z - 324*y*y*z + 18*z*z - 144*x*y*z;
   values[28] = 252*x*z*z - 234*x*z + 252*x*x*z + 288*x*y*z;
   values[29] = 36*x*z*z - 18*x*z - 72*x*x*z + 72*x*y*z;
   values[30] = 36*x*x*z - 72*x*z*z - 18*x*z + 72*x*y*z;
   values[31] = 90*x - 396*x*y - 234*x*z + 324*x*y*y + 432*x*x*y + 144*x*z*z + 252*x*x*z - 198*x*x + 108*x*x*x + 504*x*y*z;
   values[32] = 252*x*y - 18*x - 18*x*z - 324*x*y*y - 216*x*x*y + 36*x*z*z + 36*x*x*z + 18*x*x - 144*x*y*z;
   values[33] = 36*x*y - 18*x - 18*x*z - 216*x*x*y + 36*x*z*z - 72*x*x*z + 126*x*x - 108*x*x*x + 72*x*y*z;
   values[34] = 256*x*z - 256*x*z*z - 256*x*x*z - 512*x*y*z;
}
static void C_T_P4_3D_D001(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (148*x)/3 + (148*y)/3 + (140*z)/3 - 192*x*y - 168*x*z - 168*y*z + (464*x*y*y)/3 + (464*x*x*y)/3 + 132*x*z*z + (400*x*x*z)/3 + 132*y*z*z + (400*y*y*z)/3 - 84*x*x + 44*x*x*x - 84*y*y + 44*y*y*y - 80*z*z + (128*z*z*z)/3 + (928*x*y*z)/3 - 25.0/3.0;
   values[1] = (4*x)/3 - (64*x*y)/3 - (8*x*z)/3 + 20*x*y*y + (56*x*x*y)/3 - (8*x*x*z)/3 - (4*x*x*x)/3 + 40*x*y*z;
   values[2] = (4*y)/3 - (64*x*y)/3 - (8*y*z)/3 + (56*x*y*y)/3 + 20*x*x*y - (8*y*y*z)/3 - (4*y*y*y)/3 + 40*x*y*z;
   values[3] = (4*x)/3 + (4*y)/3 + (44*z)/3 - (64*x*y)/3 + 20*x*y*y + 20*x*x*y - 4*x*z*z - (8*x*x*z)/3 - 4*y*z*z - (8*y*y*z)/3 - (4*x*x)/3 - (4*y*y)/3 - 48*z*z + (128*z*z*z)/3 + (112*x*y*z)/3 - 1.0;
   values[4] = 352*x*y - 96*x + (928*x*z)/3 - (800*x*y*y)/3 - (1216*x*x*y)/3 - 224*x*z*z - (1088*x*x*z)/3 + 240*x*x - (448*x*x*x)/3 - (1600*x*y*z)/3;
   values[5] = 46*x - 156*x*y - 124*x*z + 116*x*y*y + 216*x*x*y + 84*x*z*z + 184*x*x*z - 162*x*x + 128*x*x*x + 232*x*y*z;
   values[6] = (224*x*y)/3 - (32*x)/3 + 32*x*z - (224*x*y*y)/3 - (64*x*x*y)/3 - 32*x*z*z + (64*x*x*z)/3 + 16*x*x - (64*x*x*x)/3 - (448*x*y*z)/3;
   values[7] = 352*x*y - 96*y + (928*y*z)/3 - (1216*x*y*y)/3 - (800*x*x*y)/3 - 224*y*z*z - (1088*y*y*z)/3 + 240*y*y - (448*y*y*y)/3 - (1600*x*y*z)/3;
   values[8] = 46*y - 156*x*y - 124*y*z + 216*x*y*y + 116*x*x*y + 84*y*z*z + 184*y*y*z - 162*y*y + 128*y*y*y + 232*x*y*z;
   values[9] = (224*x*y)/3 - (32*y)/3 + 32*y*z - (64*x*y*y)/3 - (224*x*x*y)/3 - 32*y*z*z + (64*y*y*z)/3 + 16*y*y - (64*y*y*y)/3 - (448*x*y*z)/3;
   values[10] = 352*x*y - 96*y - (416*z)/3 - 96*x + 480*x*z + 480*y*z - (800*x*y*y)/3 - (800*x*x*y)/3 - 448*x*z*z - (1088*x*x*z)/3 - 448*y*z*z - (1088*y*y*z)/3 + (464*x*x)/3 - (224*x*x*x)/3 + (464*y*y)/3 - (224*y*y*y)/3 + 288*z*z - (512*z*z*z)/3 - (2432*x*y*z)/3 + 16.0;
   values[11] = 46*x + 46*y + 152*z - 156*x*y - 324*x*z - 324*y*z + 116*x*y*y + 116*x*x*y + 384*x*z*z + 184*x*x*z + 384*y*z*z + 184*y*y*z - 62*x*x + 28*x*x*x - 62*y*y + 28*y*y*y - 384*z*z + 256*z*z*z + 432*x*y*z - 12.0;
   values[12] = (224*x*y)/3 - (32*y)/3 - (224*z)/3 - (32*x)/3 + 32*x*z + 32*y*z - (224*x*y*y)/3 - (224*x*x*y)/3 - 64*x*z*z + (64*x*x*z)/3 - 64*y*z*z + (64*y*y*z)/3 + 16*x*x - (32*x*x*x)/3 + 16*y*y - (32*y*y*y)/3 + 224*z*z - (512*z*z*z)/3 - (128*x*y*z)/3 + 16.0/3.0;
   values[13] = (128*x*y)/3 - (128*x*y*y)/3 - (128*x*x*y)/3 - (256*x*y*z)/3;
   values[14] = 32*x*y*y - 32*x*y + 32*x*x*y + 64*x*y*z;
   values[15] = (128*x*y)/3 - (128*x*y*y)/3 - (128*x*x*y)/3 - (256*x*y*z)/3;
   values[16] = (32*x)/3 + (128*x*y)/3 - 32*x*z - (128*x*y*y)/3 - (128*x*x*y)/3 + 32*x*z*z + (256*x*x*z)/3 - (208*x*x)/3 + (224*x*x*x)/3 - (256*x*y*z)/3;
   values[17] = 44*x*z - 32*x*y - 6*x + 32*x*y*y + 32*x*x*y - 84*x*z*z + 16*x*x*z + 22*x*x - 28*x*x*x + 64*x*y*z;
   values[18] = (32*x)/3 + (128*x*y)/3 - (416*x*z)/3 - (128*x*y*y)/3 - (128*x*x*y)/3 + 224*x*z*z + (256*x*x*z)/3 - 16*x*x + (32*x*x*x)/3 - (256*x*y*z)/3;
   values[19] = (32*y)/3 + (128*x*y)/3 - 32*y*z - (128*x*y*y)/3 - (128*x*x*y)/3 + 32*y*z*z + (256*y*y*z)/3 - (208*y*y)/3 + (224*y*y*y)/3 - (256*x*y*z)/3;
   values[20] = 44*y*z - 32*x*y - 6*y + 32*x*y*y + 32*x*x*y - 84*y*z*z + 16*y*y*z + 22*y*y - 28*y*y*y + 64*x*y*z;
   values[21] = (32*y)/3 + (128*x*y)/3 - (416*y*z)/3 - (128*x*y*y)/3 - (128*x*x*y)/3 + 224*y*z*z + (256*y*y*z)/3 - 16*y*y + (32*y*y*y)/3 - (256*x*y*z)/3;
   values[22] = 36*x*y*y - 54*x*y + 144*x*x*y + 72*x*y*z;
   values[23] = 144*x*y*y - 54*x*y + 36*x*x*y + 72*x*y*z;
   values[24] = 36*x*y*y - 54*x*y + 36*x*x*y + 288*x*y*z;
   values[25] = 90*y - 234*x*y - 396*y*z + 252*x*y*y + 144*x*x*y + 324*y*z*z + 432*y*y*z - 198*y*y + 108*y*y*y + 504*x*y*z;
   values[26] = 252*y*z - 18*x*y - 18*y + 36*x*y*y + 36*x*x*y - 324*y*z*z - 216*y*y*z + 18*y*y - 144*x*y*z;
   values[27] = 36*y*z - 18*x*y - 18*y - 72*x*y*y + 36*x*x*y - 216*y*y*z + 126*y*y - 108*y*y*y + 72*x*y*z;
   values[28] = 90*x - 234*x*y - 396*x*z + 144*x*y*y + 252*x*x*y + 324*x*z*z + 432*x*x*z - 198*x*x + 108*x*x*x + 504*x*y*z;
   values[29] = 36*x*z - 18*x*y - 18*x + 36*x*y*y - 72*x*x*y - 216*x*x*z + 126*x*x - 108*x*x*x + 72*x*y*z;
   values[30] = 252*x*z - 18*x*y - 18*x + 36*x*y*y + 36*x*x*y - 324*x*z*z - 216*x*x*z + 18*x*x - 144*x*y*z;
   values[31] = 252*x*y*y - 234*x*y + 252*x*x*y + 288*x*y*z;
   values[32] = 36*x*x*y - 72*x*y*y - 18*x*y + 72*x*y*z;
   values[33] = 36*x*y*y - 18*x*y - 72*x*x*y + 72*x*y*z;
   values[34] = 256*x*y - 256*x*y*y - 256*x*x*y - 512*x*y*z;
}
static void C_T_P4_3D_D200(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 264*x*y - 168*y - 168*z - 160*x + 264*x*z + (928*y*z)/3 + 128*x*x + (400*y*y)/3 + (400*z*z)/3 + 140.0/3.0;
   values[1] = (112*y*z)/3 - 8*x*y - 8*x*z - 96*x + 128*x*x - (8*y*y)/3 - (8*z*z)/3 + 44.0/3.0;
   values[2] = 40*y*z - (8*y)/3 - (8*y*y)/3;
   values[3] = 40*y*z - (8*z)/3 - (8*z*z)/3;
   values[4] = 576*x + 480*y + 480*z - 896*x*y - 896*x*z - (2432*y*z)/3 - 512*x*x - (1088*y*y)/3 - (1088*z*z)/3 - 416.0/3.0;
   values[5] = 768*x*y - 324*y - 324*z - 768*x + 768*x*z + 432*y*z + 768*x*x + 184*y*y + 184*z*z + 152.0;
   values[6] = 448*x + 32*y + 32*z - 128*x*y - 128*x*z - (128*y*z)/3 - 512*x*x + (64*y*y)/3 + (64*z*z)/3 - 224.0/3.0;
   values[7] = (928*y)/3 - 448*x*y - (1600*y*z)/3 - (1088*y*y)/3;
   values[8] = 168*x*y - 124*y + 232*y*z + 184*y*y;
   values[9] = 32*y - 64*x*y - (448*y*z)/3 + (64*y*y)/3;
   values[10] = (928*z)/3 - 448*x*z - (1600*y*z)/3 - (1088*z*z)/3;
   values[11] = 168*x*z - 124*z + 232*y*z + 184*z*z;
   values[12] = 32*z - 64*x*z - (448*y*z)/3 + (64*z*z)/3;
   values[13] = 448*x*y - (416*y)/3 - (256*y*z)/3 + (256*y*y)/3;
   values[14] = 44*y - 168*x*y + 64*y*z + 16*y*y;
   values[15] = 64*x*y - 32*y - (256*y*z)/3 + (256*y*y)/3;
   values[16] = 448*x*z - (416*z)/3 - (256*y*z)/3 + (256*z*z)/3;
   values[17] = 44*z - 168*x*z + 64*y*z + 16*z*z;
   values[18] = 64*x*z - 32*z - (256*y*z)/3 + (256*z*z)/3;
   values[19] = -(256*y*z)/3;
   values[20] = 64*y*z;
   values[21] = -(256*y*z)/3;
   values[22] = 288*y*z;
   values[23] = 72*y*z;
   values[24] = 72*y*z;
   values[25] = 288*y*z;
   values[26] = 72*y*z;
   values[27] = 72*y*z;
   values[28] = 648*x*z - 396*z + 504*y*z + 432*z*z;
   values[29] = 252*z - 648*x*z - 144*y*z - 216*z*z;
   values[30] = 36*z + 72*y*z - 216*z*z;
   values[31] = 648*x*y - 396*y + 504*y*z + 432*y*y;
   values[32] = 36*y + 72*y*z - 216*y*y;
   values[33] = 252*y - 648*x*y - 144*y*z - 216*y*y;
   values[34] = -512*y*z;
}
static void C_T_P4_3D_D020(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = 264*x*y - 160*y - 168*z - 168*x + (928*x*z)/3 + 264*y*z + (400*x*x)/3 + 128*y*y + (400*z*z)/3 + 140.0/3.0;
   values[1] = 40*x*z - (8*x)/3 - (8*x*x)/3;
   values[2] = (112*x*z)/3 - 8*x*y - 96*y - 8*y*z - (8*x*x)/3 + 128*y*y - (8*z*z)/3 + 44.0/3.0;
   values[3] = 40*x*z - (8*z)/3 - (8*z*z)/3;
   values[4] = (928*x)/3 - 448*x*y - (1600*x*z)/3 - (1088*x*x)/3;
   values[5] = 168*x*y - 124*x + 232*x*z + 184*x*x;
   values[6] = 32*x - 64*x*y - (448*x*z)/3 + (64*x*x)/3;
   values[7] = 480*x + 576*y + 480*z - 896*x*y - (2432*x*z)/3 - 896*y*z - (1088*x*x)/3 - 512*y*y - (1088*z*z)/3 - 416.0/3.0;
   values[8] = 768*x*y - 768*y - 324*z - 324*x + 432*x*z + 768*y*z + 184*x*x + 768*y*y + 184*z*z + 152.0;
   values[9] = 32*x + 448*y + 32*z - 128*x*y - (128*x*z)/3 - 128*y*z + (64*x*x)/3 - 512*y*y + (64*z*z)/3 - 224.0/3.0;
   values[10] = (928*z)/3 - (1600*x*z)/3 - 448*y*z - (1088*z*z)/3;
   values[11] = 232*x*z - 124*z + 168*y*z + 184*z*z;
   values[12] = 32*z - (448*x*z)/3 - 64*y*z + (64*z*z)/3;
   values[13] = 64*x*y - 32*x - (256*x*z)/3 + (256*x*x)/3;
   values[14] = 44*x - 168*x*y + 64*x*z + 16*x*x;
   values[15] = 448*x*y - (416*x)/3 - (256*x*z)/3 + (256*x*x)/3;
   values[16] = -(256*x*z)/3;
   values[17] = 64*x*z;
   values[18] = -(256*x*z)/3;
   values[19] = 448*y*z - (256*x*z)/3 - (416*z)/3 + (256*z*z)/3;
   values[20] = 44*z + 64*x*z - 168*y*z + 16*z*z;
   values[21] = 64*y*z - (256*x*z)/3 - 32*z + (256*z*z)/3;
   values[22] = 72*x*z;
   values[23] = 288*x*z;
   values[24] = 72*x*z;
   values[25] = 504*x*z - 396*z + 648*y*z + 432*z*z;
   values[26] = 36*z + 72*x*z - 216*z*z;
   values[27] = 252*z - 144*x*z - 648*y*z - 216*z*z;
   values[28] = 288*x*z;
   values[29] = 72*x*z;
   values[30] = 72*x*z;
   values[31] = 648*x*y - 396*x + 504*x*z + 432*x*x;
   values[32] = 252*x - 648*x*y - 144*x*z - 216*x*x;
   values[33] = 36*x + 72*x*z - 216*x*x;
   values[34] = -512*x*z;
}
static void C_T_P4_3D_D002(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (928*x*y)/3 - 168*y - 160*z - 168*x + 264*x*z + 264*y*z + (400*x*x)/3 + (400*y*y)/3 + 128*z*z + 140.0/3.0;
   values[1] = 40*x*y - (8*x)/3 - (8*x*x)/3;
   values[2] = 40*x*y - (8*y)/3 - (8*y*y)/3;
   values[3] = (112*x*y)/3 - 96*z - 8*x*z - 8*y*z - (8*x*x)/3 - (8*y*y)/3 + 128*z*z + 44.0/3.0;
   values[4] = (928*x)/3 - (1600*x*y)/3 - 448*x*z - (1088*x*x)/3;
   values[5] = 232*x*y - 124*x + 168*x*z + 184*x*x;
   values[6] = 32*x - (448*x*y)/3 - 64*x*z + (64*x*x)/3;
   values[7] = (928*y)/3 - (1600*x*y)/3 - 448*y*z - (1088*y*y)/3;
   values[8] = 232*x*y - 124*y + 168*y*z + 184*y*y;
   values[9] = 32*y - (448*x*y)/3 - 64*y*z + (64*y*y)/3;
   values[10] = 480*x + 480*y + 576*z - (2432*x*y)/3 - 896*x*z - 896*y*z - (1088*x*x)/3 - (1088*y*y)/3 - 512*z*z - 416.0/3.0;
   values[11] = 432*x*y - 324*y - 768*z - 324*x + 768*x*z + 768*y*z + 184*x*x + 184*y*y + 768*z*z + 152.0;
   values[12] = 32*x + 32*y + 448*z - (128*x*y)/3 - 128*x*z - 128*y*z + (64*x*x)/3 + (64*y*y)/3 - 512*z*z - 224.0/3.0;
   values[13] = -(256*x*y)/3;
   values[14] = 64*x*y;
   values[15] = -(256*x*y)/3;
   values[16] = 64*x*z - (256*x*y)/3 - 32*x + (256*x*x)/3;
   values[17] = 44*x + 64*x*y - 168*x*z + 16*x*x;
   values[18] = 448*x*z - (256*x*y)/3 - (416*x)/3 + (256*x*x)/3;
   values[19] = 64*y*z - (256*x*y)/3 - 32*y + (256*y*y)/3;
   values[20] = 44*y + 64*x*y - 168*y*z + 16*y*y;
   values[21] = 448*y*z - (256*x*y)/3 - (416*y)/3 + (256*y*y)/3;
   values[22] = 72*x*y;
   values[23] = 72*x*y;
   values[24] = 288*x*y;
   values[25] = 504*x*y - 396*y + 648*y*z + 432*y*y;
   values[26] = 252*y - 144*x*y - 648*y*z - 216*y*y;
   values[27] = 36*y + 72*x*y - 216*y*y;
   values[28] = 504*x*y - 396*x + 648*x*z + 432*x*x;
   values[29] = 36*x + 72*x*y - 216*x*x;
   values[30] = 252*x - 144*x*y - 648*x*z - 216*x*x;
   values[31] = 288*x*y;
   values[32] = 72*x*y;
   values[33] = 72*x*y;
   values[34] = -512*x*y;
}
static void C_T_P4_3D_D110(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (800*x*y)/3 - 168*y - 192*z - 168*x + (928*x*z)/3 + (928*y*z)/3 + 132*x*x + 132*y*y + (464*z*z)/3 + 148.0/3.0;
   values[1] = (112*x*z)/3 - (64*z)/3 - (16*x*y)/3 - (8*y)/3 + 40*y*z - 4*x*x + 20*z*z + 4.0/3.0;
   values[2] = 40*x*z - (64*z)/3 - (16*x*y)/3 - (8*x)/3 + (112*y*z)/3 - 4*y*y + 20*z*z + 4.0/3.0;
   values[3] = 40*x*z - (64*z)/3 + 40*y*z + (56*z*z)/3;
   values[4] = 480*x + (928*y)/3 + 352*z - (2176*x*y)/3 - (2432*x*z)/3 - (1600*y*z)/3 - 448*x*x - 224*y*y - (800*z*z)/3 - 96.0;
   values[5] = 368*x*y - 124*y - 156*z - 324*x + 432*x*z + 232*y*z + 384*x*x + 84*y*y + 116*z*z + 46.0;
   values[6] = 32*x + 32*y + (224*z)/3 + (128*x*y)/3 - (128*x*z)/3 - (448*y*z)/3 - 64*x*x - 32*y*y - (224*z*z)/3 - 32.0/3.0;
   values[7] = (928*x)/3 + 480*y + 352*z - (2176*x*y)/3 - (1600*x*z)/3 - (2432*y*z)/3 - 224*x*x - 448*y*y - (800*z*z)/3 - 96.0;
   values[8] = 368*x*y - 324*y - 156*z - 124*x + 232*x*z + 432*y*z + 84*x*x + 384*y*y + 116*z*z + 46.0;
   values[9] = 32*x + 32*y + (224*z)/3 + (128*x*y)/3 - (448*x*z)/3 - (128*y*z)/3 - 32*x*x - 64*y*y - (224*z*z)/3 - 32.0/3.0;
   values[10] = 352*z - (1600*x*z)/3 - (1600*y*z)/3 - (1216*z*z)/3;
   values[11] = 232*x*z - 156*z + 232*y*z + 216*z*z;
   values[12] = (224*z)/3 - (448*x*z)/3 - (448*y*z)/3 - (64*z*z)/3;
   values[13] = (128*z)/3 - 32*y - (416*x)/3 + (512*x*y)/3 - (256*x*z)/3 - (256*y*z)/3 + 224*x*x + 32*y*y - (128*z*z)/3 + 32.0/3.0;
   values[14] = 44*x + 44*y - 32*z + 32*x*y + 64*x*z + 64*y*z - 84*x*x - 84*y*y + 32*z*z - 6;
   values[15] = (128*z)/3 - (416*y)/3 - 32*x + (512*x*y)/3 - (256*x*z)/3 - (256*y*z)/3 + 32*x*x + 224*y*y - (128*z*z)/3 + 32.0/3.0;
   values[16] = (128*z)/3 - (256*x*z)/3 - (256*y*z)/3 - (128*z*z)/3;
   values[17] = 64*x*z - 32*z + 64*y*z + 32*z*z;
   values[18] = (128*z)/3 - (256*x*z)/3 - (256*y*z)/3 - (128*z*z)/3;
   values[19] = (128*z)/3 - (256*x*z)/3 - (256*y*z)/3 - (128*z*z)/3;
   values[20] = 64*x*z - 32*z + 64*y*z + 32*z*z;
   values[21] = (128*z)/3 - (256*x*z)/3 - (256*y*z)/3 - (128*z*z)/3;
   values[22] = 288*x*z - 54*z + 72*y*z + 36*z*z;
   values[23] = 72*x*z - 54*z + 288*y*z + 36*z*z;
   values[24] = 72*x*z - 54*z + 72*y*z + 144*z*z;
   values[25] = 288*x*z - 234*z + 504*y*z + 252*z*z;
   values[26] = 72*x*z - 18*z + 72*y*z - 72*z*z;
   values[27] = 72*x*z - 18*z - 144*y*z + 36*z*z;
   values[28] = 504*x*z - 234*z + 288*y*z + 252*z*z;
   values[29] = 72*y*z - 144*x*z - 18*z + 36*z*z;
   values[30] = 72*x*z - 18*z + 72*y*z - 72*z*z;
   values[31] = 864*x*y - 396*y - 234*z - 396*x + 504*x*z + 504*y*z + 324*x*x + 324*y*y + 144*z*z + 90.0;
   values[32] = 36*x + 252*y - 18*z - 432*x*y + 72*x*z - 144*y*z - 324*y*y + 36*z*z - 18.0;
   values[33] = 252*x + 36*y - 18*z - 432*x*y - 144*x*z + 72*y*z - 324*x*x + 36*z*z - 18.0;
   values[34] = 256*z - 512*x*z - 512*y*z - 256*z*z;
}
static void C_T_P4_3D_D101(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (928*x*y)/3 - 192*y - 168*z - 168*x + (800*x*z)/3 + (928*y*z)/3 + 132*x*x + (464*y*y)/3 + 132*z*z + 148.0/3.0;
   values[1] = (112*x*y)/3 - (8*z)/3 - (64*y)/3 - (16*x*z)/3 + 40*y*z - 4*x*x + 20*y*y + 4.0/3.0;
   values[2] = 40*x*y - (64*y)/3 + 40*y*z + (56*y*y)/3;
   values[3] = 40*x*y - (64*y)/3 - (8*x)/3 - (16*x*z)/3 + (112*y*z)/3 + 20*y*y - 4*z*z + 4.0/3.0;
   values[4] = 480*x + 352*y + (928*z)/3 - (2432*x*y)/3 - (2176*x*z)/3 - (1600*y*z)/3 - 448*x*x - (800*y*y)/3 - 224*z*z - 96.0;
   values[5] = 432*x*y - 156*y - 124*z - 324*x + 368*x*z + 232*y*z + 384*x*x + 116*y*y + 84*z*z + 46.0;
   values[6] = 32*x + (224*y)/3 + 32*z - (128*x*y)/3 + (128*x*z)/3 - (448*y*z)/3 - 64*x*x - (224*y*y)/3 - 32*z*z - 32.0/3.0;
   values[7] = 352*y - (1600*x*y)/3 - (1600*y*z)/3 - (1216*y*y)/3;
   values[8] = 232*x*y - 156*y + 232*y*z + 216*y*y;
   values[9] = (224*y)/3 - (448*x*y)/3 - (448*y*z)/3 - (64*y*y)/3;
   values[10] = (928*x)/3 + 352*y + 480*z - (1600*x*y)/3 - (2176*x*z)/3 - (2432*y*z)/3 - 224*x*x - (800*y*y)/3 - 448*z*z - 96.0;
   values[11] = 232*x*y - 156*y - 324*z - 124*x + 368*x*z + 432*y*z + 84*x*x + 116*y*y + 384*z*z + 46.0;
   values[12] = 32*x + (224*y)/3 + 32*z - (448*x*y)/3 + (128*x*z)/3 - (128*y*z)/3 - 32*x*x - (224*y*y)/3 - 64*z*z - 32.0/3.0;
   values[13] = (128*y)/3 - (256*x*y)/3 - (256*y*z)/3 - (128*y*y)/3;
   values[14] = 64*x*y - 32*y + 64*y*z + 32*y*y;
   values[15] = (128*y)/3 - (256*x*y)/3 - (256*y*z)/3 - (128*y*y)/3;
   values[16] = (128*y)/3 - (416*x)/3 - 32*z - (256*x*y)/3 + (512*x*z)/3 - (256*y*z)/3 + 224*x*x - (128*y*y)/3 + 32*z*z + 32.0/3.0;
   values[17] = 44*x - 32*y + 44*z + 64*x*y + 32*x*z + 64*y*z - 84*x*x + 32*y*y - 84*z*z - 6.0;
   values[18] = (128*y)/3 - 32*x - (416*z)/3 - (256*x*y)/3 + (512*x*z)/3 - (256*y*z)/3 + 32*x*x - (128*y*y)/3 + 224*z*z + 32.0/3.0;
   values[19] = (128*y)/3 - (256*x*y)/3 - (256*y*z)/3 - (128*y*y)/3;
   values[20] = 64*x*y - 32*y + 64*y*z + 32*y*y;
   values[21] = (128*y)/3 - (256*x*y)/3 - (256*y*z)/3 - (128*y*y)/3;
   values[22] = 288*x*y - 54*y + 72*y*z + 36*y*y;
   values[23] = 72*x*y - 54*y + 72*y*z + 144*y*y;
   values[24] = 72*x*y - 54*y + 288*y*z + 36*y*y;
   values[25] = 288*x*y - 234*y + 504*y*z + 252*y*y;
   values[26] = 72*x*y - 18*y - 144*y*z + 36*y*y;
   values[27] = 72*x*y - 18*y + 72*y*z - 72*y*y;
   values[28] = 504*x*y - 234*y - 396*z - 396*x + 864*x*z + 504*y*z + 324*x*x + 144*y*y + 324*z*z + 90.0;
   values[29] = 252*x - 18*y + 36*z - 144*x*y - 432*x*z + 72*y*z - 324*x*x + 36*y*y - 18.0;
   values[30] = 36*x - 18*y + 252*z + 72*x*y - 432*x*z - 144*y*z + 36*y*y - 324*z*z - 18.0;
   values[31] = 504*x*y - 234*y + 288*y*z + 252*y*y;
   values[32] = 72*x*y - 18*y + 72*y*z - 72*y*y;
   values[33] = 72*y*z - 144*x*y - 18*y + 36*y*y;
   values[34] = 256*y - 512*x*y - 512*y*z - 256*y*y;
}
static void C_T_P4_3D_D011(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x=RefCoord[0]; y=RefCoord[1]; z=RefCoord[2];
   values[0] = (928*x*y)/3 - 168*y - 168*z - 192*x + (928*x*z)/3 + (800*y*z)/3 + (464*x*x)/3 + 132*y*y + 132*z*z + 148.0/3.0;
   values[1] = 40*x*y - (64*x)/3 + 40*x*z + (56*x*x)/3;
   values[2] = (112*x*y)/3 - (8*z)/3 - (64*x)/3 + 40*x*z - (16*y*z)/3 + 20*x*x - 4*y*y + 4.0/3.0;
   values[3] = 40*x*y - (8*y)/3 - (64*x)/3 + (112*x*z)/3 - (16*y*z)/3 + 20*x*x - 4*z*z + 4.0/3.0;
   values[4] = 352*x - (1600*x*y)/3 - (1600*x*z)/3 - (1216*x*x)/3;
   values[5] = 232*x*y - 156*x + 232*x*z + 216*x*x;
   values[6] = (224*x)/3 - (448*x*y)/3 - (448*x*z)/3 - (64*x*x)/3;
   values[7] = 352*x + 480*y + (928*z)/3 - (2432*x*y)/3 - (1600*x*z)/3 - (2176*y*z)/3 - (800*x*x)/3 - 448*y*y - 224*z*z - 96.0;
   values[8] = 432*x*y - 324*y - 124*z - 156*x + 232*x*z + 368*y*z + 116*x*x + 384*y*y + 84*z*z + 46.0;
   values[9] = (224*x)/3 + 32*y + 32*z - (128*x*y)/3 - (448*x*z)/3 + (128*y*z)/3 - (224*x*x)/3 - 64*y*y - 32*z*z - 32.0/3.0;
   values[10] = 352*x + (928*y)/3 + 480*z - (1600*x*y)/3 - (2432*x*z)/3 - (2176*y*z)/3 - (800*x*x)/3 - 224*y*y - 448*z*z - 96.0;
   values[11] = 232*x*y - 124*y - 324*z - 156*x + 432*x*z + 368*y*z + 116*x*x + 84*y*y + 384*z*z + 46.0;
   values[12] = (224*x)/3 + 32*y + 32*z - (448*x*y)/3 - (128*x*z)/3 + (128*y*z)/3 - (224*x*x)/3 - 32*y*y - 64*z*z - 32.0/3.0;
   values[13] = (128*x)/3 - (256*x*y)/3 - (256*x*z)/3 - (128*x*x)/3;
   values[14] = 64*x*y - 32*x + 64*x*z + 32*x*x;
   values[15] = (128*x)/3 - (256*x*y)/3 - (256*x*z)/3 - (128*x*x)/3;
   values[16] = (128*x)/3 - (256*x*y)/3 - (256*x*z)/3 - (128*x*x)/3;
   values[17] = 64*x*y - 32*x + 64*x*z + 32*x*x;
   values[18] = (128*x)/3 - (256*x*y)/3 - (256*x*z)/3 - (128*x*x)/3;
   values[19] = (128*x)/3 - (416*y)/3 - 32*z - (256*x*y)/3 - (256*x*z)/3 + (512*y*z)/3 - (128*x*x)/3 + 224*y*y + 32*z*z + 32.0/3.0;
   values[20] = 44*y - 32*x + 44*z + 64*x*y + 64*x*z + 32*y*z + 32*x*x - 84*y*y - 84*z*z - 6;
   values[21] = (128*x)/3 - 32*y - (416*z)/3 - (256*x*y)/3 - (256*x*z)/3 + (512*y*z)/3 - (128*x*x)/3 + 32*y*y + 224*z*z + 32.0/3.0;
   values[22] = 72*x*y - 54*x + 72*x*z + 144*x*x;
   values[23] = 288*x*y - 54*x + 72*x*z + 36*x*x;
   values[24] = 72*x*y - 54*x + 288*x*z + 36*x*x;
   values[25] = 504*x*y - 396*y - 396*z - 234*x + 504*x*z + 864*y*z + 144*x*x + 324*y*y + 324*z*z + 90.0;
   values[26] = 36*y - 18*x + 252*z + 72*x*y - 144*x*z - 432*y*z + 36*x*x - 324*z*z - 18.0;
   values[27] = 252*y - 18*x + 36*z - 144*x*y + 72*x*z - 432*y*z + 36*x*x - 324*y*y - 18.0;
   values[28] = 288*x*y - 234*x + 504*x*z + 252*x*x;
   values[29] = 72*x*y - 18*x + 72*x*z - 72*x*x;
   values[30] = 72*x*y - 18*x - 144*x*z + 36*x*x;
   values[31] = 504*x*y - 234*x + 288*x*z + 252*x*x;
   values[32] = 72*x*z - 144*x*y - 18*x + 36*x*x;
   values[33] = 72*x*y - 18*x + 72*x*z - 72*x*x;
   values[34] = 256*x - 512*x*y - 512*x*z - 256*x*x;
}
// values of the derivatives in RefCoord[1]-RefCoord[1] direction
static void C_T_P4_3D_Nodal(ELEMENT * elem, FUNCTIONVEC *fun, INT dim, DOUBLE* values)
{
   DOUBLE Coords[105] = {0.0000000000000000, 0.0000000000000000,  0.0000000000000000,
1.0000000000000000, 0.0000000000000000,  0.0000000000000000,
0.0000000000000000, 1.0000000000000000,  0.0000000000000000,
0.0000000000000000, 0.0000000000000000,  1.0000000000000000,
0.2500000000000000, 0.0000000000000000,  0.0000000000000000,
0.5000000000000000, 0.0000000000000000,  0.0000000000000000,
0.7500000000000000, 0.0000000000000000,  0.0000000000000000,
0.0000000000000000, 0.2500000000000000,  0.0000000000000000,
0.0000000000000000, 0.5000000000000000,  0.0000000000000000,
0.0000000000000000, 0.7500000000000000,  0.0000000000000000,
0.0000000000000000, 0.0000000000000000,  0.2500000000000000,
0.0000000000000000, 0.0000000000000000,  0.5000000000000000,
0.0000000000000000, 0.0000000000000000,  0.7500000000000000,
0.7500000000000000, 0.2500000000000000,  0.0000000000000000,
0.5000000000000000, 0.5000000000000000,  0.0000000000000000,
0.2500000000000000, 0.7500000000000000,  0.0000000000000000,
0.7500000000000000, 0.0000000000000000,  0.2500000000000000,
0.5000000000000000, 0.0000000000000000,  0.5000000000000000,
0.2500000000000000, 0.0000000000000000,  0.7500000000000000,
0.0000000000000000, 0.7500000000000000,  0.2500000000000000,
0.0000000000000000, 0.5000000000000000,  0.5000000000000000,
0.0000000000000000, 0.2500000000000000,  0.7500000000000000,
0.6666666666666666, 0.1666666666666667,  0.1666666666666667,
0.1666666666666667, 0.6666666666666666,  0.1666666666666667,
0.1666666666666667, 0.1666666666666667,  0.6666666666666666,
0.0000000000000000, 0.1666666666666667,  0.1666666666666667,
0.0000000000000000, 0.1666666666666667,  0.6666666666666666,
0.0000000000000000, 0.6666666666666666,  0.1666666666666667,
0.1666666666666667, 0.0000000000000000,  0.1666666666666667,
0.6666666666666666, 0.0000000000000000,  0.1666666666666667,
0.1666666666666667, 0.0000000000000000,  0.6666666666666666,
0.1666666666666667, 0.1666666666666667,  0.0000000000000000,
0.1666666666666667, 0.6666666666666666,  0.0000000000000000,
0.6666666666666666, 0.1666666666666667,  0.0000000000000000,
0.2500000000000000, 0.2500000000000000,  0.2500000000000000};


   //printf("Interpolation for P3 element!\n");
   //INT dim =1;
   DOUBLE Coord[3], RefCoord[3];
   INT i;
   INT j;
   //先计算4个节点上的函数值
   for(i=0;i<35;i++)
   {
      // dof on the verts 
      for(j=0;j<3;j++){
         RefCoord[j] = Coords[i*3+j];
      }
      ElementRefCoord2Coord(elem, RefCoord, Coord);
      fun(Coord, dim, values+i*dim);
   }
        
}
#endif