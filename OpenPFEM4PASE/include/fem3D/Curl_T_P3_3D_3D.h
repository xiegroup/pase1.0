#ifndef __CURLTP33D3D__
#define __CURLTP33D3D__
#include <stdbool.h>
#include "mesh.h"
#include "enumerations.h"
#include "constants.h"
// ***********************************************************************
// Hcurl P3 element, CURL CONFORMING EDGE ELEMENTS OF NÉDÉLEC, 3D
// ***********************************************************************
static INT Curl_T_P3_3D_3D_dof[4] = {0, 3, 6, 3};
static INT Curl_T_P3_3D_3D_Num_Bas = 45;
static INT Curl_T_P3_3D_3D_Value_Dim = 3;
static INT Curl_T_P3_3D_3D_Inter_Dim = 3; // 现在Piola变换的基函数自由度排布时没有用到这个参数
static INT Curl_T_P3_3D_3D_Polydeg = 3;
static bool Curl_T_P3_3D_3D_IsOnlyDependOnRefCoord = 0;
static INT Curl_T_P3_3D_3D_Accuracy = 3;
static MAPTYPE Curl_T_P3_3D_3D_Maptype = Piola;

static void Curl_T_P3_3D_3D_InterFun(DOUBLE RefCoord[], INT dim, DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 * x * y - 54 * y - 54 * z - 36 * x + 120 * x * z + 180 * y * z - 90 * x * y * y - 45 * x * x * y - 90 * x * z * z - 45 * x * x * z - 135 * y * z * z - 135 * y * y * z + 30 * x * x + 90 * y * y - 45 * y * y * y + 90 * z * z - 45 * z * z * z - 180 * x * y * z + 9;
   values[1] = 18 * x - 60 * x * y - 60 * x * z + 45 * x * y * y + 90 * x * x * y + 45 * x * z * z + 90 * x * x * z - 60 * x * x + 45 * x * x * x + 90 * x * y * z;
   values[2] = 18 * x - 60 * x * y - 60 * x * z + 45 * x * y * y + 90 * x * x * y + 45 * x * z * z + 90 * x * x * z - 60 * x * x + 45 * x * x * x + 90 * x * y * z;
   values[3] = 192 * x + 156 * y + 156 * z - 540 * x * y - 540 * x * z - 420 * y * z + 360 * x * y * y + 270 * x * x * y + 360 * x * z * z + 270 * x * x * z + 270 * y * z * z + 270 * y * y * z - 180 * x * x - 210 * y * y + 90 * y * y * y - 210 * z * z + 90 * z * z * z + 720 * x * y * z - 36;
   values[4] = 180 * x * y - 84 * x + 180 * x * z - 90 * x * y * y - 360 * x * x * y - 90 * x * z * z - 360 * x * x * z + 330 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[5] = 180 * x * y - 84 * x + 180 * x * z - 90 * x * y * y - 360 * x * x * y - 90 * x * z * z - 360 * x * x * z + 330 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[6] = 450 * x * y - 105 * y - 105 * z - 180 * x + 450 * x * z + 240 * y * z - 270 * x * y * y - 270 * x * x * y - 270 * x * z * z - 270 * x * x * z - 135 * y * z * z - 135 * y * y * z + 180 * x * x + 120 * y * y - 45 * y * y * y + 120 * z * z - 45 * z * z * z - 540 * x * y * z + 30;
   values[7] = 75 * x - 120 * x * y - 120 * x * z + 45 * x * y * y + 270 * x * x * y + 45 * x * z * z + 270 * x * x * z - 315 * x * x + 270 * x * x * x + 90 * x * y * z;
   values[8] = 75 * x - 120 * x * y - 120 * x * z + 45 * x * y * y + 270 * x * x * y + 45 * x * z * z + 270 * x * x * z - 315 * x * x + 270 * x * x * x + 90 * x * y * z;
   values[9] = 18 * y - 60 * x * y - 60 * y * z + 90 * x * y * y + 45 * x * x * y + 45 * y * z * z + 90 * y * y * z - 60 * y * y + 45 * y * y * y + 90 * x * y * z;
   values[10] = 120 * x * y - 36 * y - 54 * z - 54 * x + 180 * x * z + 120 * y * z - 45 * x * y * y - 90 * x * x * y - 135 * x * z * z - 135 * x * x * z - 90 * y * z * z - 45 * y * y * z + 90 * x * x - 45 * x * x * x + 30 * y * y + 90 * z * z - 45 * z * z * z - 180 * x * y * z + 9;
   values[11] = 18 * y - 60 * x * y - 60 * y * z + 90 * x * y * y + 45 * x * x * y + 45 * y * z * z + 90 * y * y * z - 60 * y * y + 45 * y * y * y + 90 * x * y * z;
   values[12] = 180 * x * y - 84 * y + 180 * y * z - 360 * x * y * y - 90 * x * x * y - 90 * y * z * z - 360 * y * y * z + 330 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[13] = 156 * x + 192 * y + 156 * z - 540 * x * y - 420 * x * z - 540 * y * z + 270 * x * y * y + 360 * x * x * y + 270 * x * z * z + 270 * x * x * z + 360 * y * z * z + 270 * y * y * z - 210 * x * x + 90 * x * x * x - 180 * y * y - 210 * z * z + 90 * z * z * z + 720 * x * y * z - 36;
   values[14] = 180 * x * y - 84 * y + 180 * y * z - 360 * x * y * y - 90 * x * x * y - 90 * y * z * z - 360 * y * y * z + 330 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[15] = 75 * y - 120 * x * y - 120 * y * z + 270 * x * y * y + 45 * x * x * y + 45 * y * z * z + 270 * y * y * z - 315 * y * y + 270 * y * y * y + 90 * x * y * z;
   values[16] = 450 * x * y - 180 * y - 105 * z - 105 * x + 240 * x * z + 450 * y * z - 270 * x * y * y - 270 * x * x * y - 135 * x * z * z - 135 * x * x * z - 270 * y * z * z - 270 * y * y * z + 120 * x * x - 45 * x * x * x + 180 * y * y + 120 * z * z - 45 * z * z * z - 540 * x * y * z + 30;
   values[17] = 75 * y - 120 * x * y - 120 * y * z + 270 * x * y * y + 45 * x * x * y + 45 * y * z * z + 270 * y * y * z - 315 * y * y + 270 * y * y * y + 90 * x * y * z;
   values[18] = 18 * z - 60 * x * z - 60 * y * z + 90 * x * z * z + 45 * x * x * z + 90 * y * z * z + 45 * y * y * z - 60 * z * z + 45 * z * z * z + 90 * x * y * z;
   values[19] = 18 * z - 60 * x * z - 60 * y * z + 90 * x * z * z + 45 * x * x * z + 90 * y * z * z + 45 * y * y * z - 60 * z * z + 45 * z * z * z + 90 * x * y * z;
   values[20] = 180 * x * y - 54 * y - 36 * z - 54 * x + 120 * x * z + 120 * y * z - 135 * x * y * y - 135 * x * x * y - 45 * x * z * z - 90 * x * x * z - 45 * y * z * z - 90 * y * y * z + 90 * x * x - 45 * x * x * x + 90 * y * y - 45 * y * y * y + 30 * z * z - 180 * x * y * z + 9;
   values[21] = 180 * x * z - 84 * z + 180 * y * z - 360 * x * z * z - 90 * x * x * z - 360 * y * z * z - 90 * y * y * z + 330 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[22] = 180 * x * z - 84 * z + 180 * y * z - 360 * x * z * z - 90 * x * x * z - 360 * y * z * z - 90 * y * y * z + 330 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[23] = 156 * x + 156 * y + 192 * z - 420 * x * y - 540 * x * z - 540 * y * z + 270 * x * y * y + 270 * x * x * y + 270 * x * z * z + 360 * x * x * z + 270 * y * z * z + 360 * y * y * z - 210 * x * x + 90 * x * x * x - 210 * y * y + 90 * y * y * y - 180 * z * z + 720 * x * y * z - 36;
   values[24] = 75 * z - 120 * x * z - 120 * y * z + 270 * x * z * z + 45 * x * x * z + 270 * y * z * z + 45 * y * y * z - 315 * z * z + 270 * z * z * z + 90 * x * y * z;
   values[25] = 75 * z - 120 * x * z - 120 * y * z + 270 * x * z * z + 45 * x * x * z + 270 * y * z * z + 45 * y * y * z - 315 * z * z + 270 * z * z * z + 90 * x * y * z;
   values[26] = 240 * x * y - 105 * y - 180 * z - 105 * x + 450 * x * z + 450 * y * z - 135 * x * y * y - 135 * x * x * y - 270 * x * z * z - 270 * x * x * z - 270 * y * z * z - 270 * y * y * z + 120 * x * x - 45 * x * x * x + 120 * y * y - 45 * y * y * y + 180 * z * z - 540 * x * y * z + 30;
   values[27] = 30 * x * y - 3 * y - 45 * x * x * y;
   values[28] = 9 * x - 45 * x * x + 45 * x * x * x;
   values[29] = 0;
   values[30] = 90 * x * x * y - 180 * x * y * y - 6 * y + 30 * y * y;
   values[31] = 180 * x * x * y - 60 * x * y - 6 * x + 60 * x * x - 90 * x * x * x;
   values[32] = 0;
   values[33] = 180 * x * y * y - 30 * x * y - 45 * x * x * y + 15 * y * y - 45 * y * y * y;
   values[34] = 30 * x * y + 45 * x * y * y - 180 * x * x * y - 15 * x * x + 45 * x * x * x;
   values[35] = 0;
   values[36] = 30 * x * z - 3 * z - 45 * x * x * z;
   values[37] = 0;
   values[38] = 9 * x - 45 * x * x + 45 * x * x * x;
   values[39] = 90 * x * x * z - 180 * x * z * z - 6 * z + 30 * z * z;
   values[40] = 0;
   values[41] = 180 * x * x * z - 60 * x * z - 6 * x + 60 * x * x - 90 * x * x * x;
   values[42] = 180 * x * z * z - 30 * x * z - 45 * x * x * z + 15 * z * z - 45 * z * z * z;
   values[43] = 0;
   values[44] = 30 * x * z + 45 * x * z * z - 180 * x * x * z - 15 * x * x + 45 * x * x * x;
   values[45] = 0;
   values[46] = 30 * y * z - 3 * z - 45 * y * y * z;
   values[47] = 9 * y - 45 * y * y + 45 * y * y * y;
   values[48] = 0;
   values[49] = 90 * y * y * z - 180 * y * z * z - 6 * z + 30 * z * z;
   values[50] = 180 * y * y * z - 60 * y * z - 6 * y + 60 * y * y - 90 * y * y * y;
   values[51] = 0;
   values[52] = 180 * y * z * z - 30 * y * z - 45 * y * y * z + 15 * z * z - 45 * z * z * z;
   values[53] = 30 * y * z + 45 * y * z * z - 180 * y * y * z - 15 * y * y + 45 * y * y * y;
   values[54] = 15 * y * z - 90 * x * y * z;
   values[55] = 135 * x * x * z - 45 * x * z;
   values[56] = 15 * x * y - 45 * x * x * y;
   values[57] = 90 * x * y * z - 45 * y * z * z;
   values[58] = 135 * x * z * z - 135 * x * x * z;
   values[59] = 45 * x * x * y - 90 * x * y * z;
   values[60] = 15 * y * z - 90 * y * y * z + 90 * x * y * z;
   values[61] = 15 * x * z - 135 * x * x * z + 180 * x * y * z;
   values[62] = 15 * x * y - 90 * x * y * y + 45 * x * x * y;
   values[63] = 15 * y * z - 90 * x * y * z;
   values[64] = 15 * x * z - 45 * x * x * z;
   values[65] = 135 * x * x * y - 45 * x * y;
   values[66] = 15 * y * z - 90 * y * z * z + 90 * x * y * z;
   values[67] = 15 * x * z - 90 * x * z * z + 45 * x * x * z;
   values[68] = 15 * x * y - 135 * x * x * y + 180 * x * y * z;
   values[69] = 90 * x * y * z - 45 * y * y * z;
   values[70] = 45 * x * x * z - 90 * x * y * z;
   values[71] = 135 * x * y * y - 135 * x * x * y;
   values[72] = 75 * y * z - 90 * y * z * z - 90 * y * y * z - 90 * x * y * z;
   values[73] = 90 * z - 225 * x * z - 150 * y * z + 270 * x * z * z + 135 * x * x * z + 180 * y * z * z + 45 * y * y * z - 225 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[74] = 75 * x * y - 30 * y + 150 * y * z - 90 * x * y * y - 45 * x * x * y - 135 * y * z * z - 180 * y * y * z + 75 * y * y - 45 * y * y * y - 180 * x * y * z;
   values[75] = 135 * y * z * z - 90 * y * z + 90 * y * y * z + 90 * x * y * z;
   values[76] = 270 * x * z - 135 * z + 180 * y * z - 405 * x * z * z - 135 * x * x * z - 270 * y * z * z - 45 * y * y * z + 405 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[77] = 45 * y - 90 * x * y - 270 * y * z + 90 * x * y * y + 45 * x * x * y + 270 * y * z * z + 270 * y * y * z - 90 * y * y + 45 * y * y * y + 270 * x * y * z;
   values[78] = 90 * y * z * z - 105 * y * z + 180 * y * y * z + 90 * x * y * z;
   values[79] = 255 * x * z - 120 * z + 330 * y * z - 270 * x * z * z - 135 * x * x * z - 360 * y * z * z - 135 * y * y * z + 255 * z * z - 135 * z * z * z - 360 * x * y * z;
   values[80] = 60 * y - 105 * x * y - 210 * y * z + 180 * x * y * y + 45 * x * x * y + 135 * y * z * z + 360 * y * y * z - 195 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[81] = 75 * y * z - 90 * y * z * z - 90 * y * y * z - 90 * x * y * z;
   values[82] = 75 * x * z - 30 * z + 150 * y * z - 90 * x * z * z - 45 * x * x * z - 180 * y * z * z - 135 * y * y * z + 75 * z * z - 45 * z * z * z - 180 * x * y * z;
   values[83] = 90 * y - 225 * x * y - 150 * y * z + 270 * x * y * y + 135 * x * x * y + 45 * y * z * z + 180 * y * y * z - 225 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[84] = 180 * y * z * z - 105 * y * z + 90 * y * y * z + 90 * x * y * z;
   values[85] = 60 * z - 105 * x * z - 210 * y * z + 180 * x * z * z + 45 * x * x * z + 360 * y * z * z + 135 * y * y * z - 195 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[86] = 255 * x * y - 120 * y + 330 * y * z - 270 * x * y * y - 135 * x * x * y - 135 * y * z * z - 360 * y * y * z + 255 * y * y - 135 * y * y * y - 360 * x * y * z;
   values[87] = 90 * y * z * z - 90 * y * z + 135 * y * y * z + 90 * x * y * z;
   values[88] = 45 * z - 90 * x * z - 270 * y * z + 90 * x * z * z + 45 * x * x * z + 270 * y * z * z + 270 * y * y * z - 90 * z * z + 45 * z * z * z + 270 * x * y * z;
   values[89] = 270 * x * y - 135 * y + 180 * y * z - 405 * x * y * y - 135 * x * x * y - 45 * y * z * z - 270 * y * y * z + 405 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[90] = 90 * z - 150 * x * z - 225 * y * z + 180 * x * z * z + 45 * x * x * z + 270 * y * z * z + 135 * y * y * z - 225 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[91] = 75 * x * z - 90 * x * z * z - 90 * x * x * z - 90 * x * y * z;
   values[92] = 75 * x * y - 30 * x + 150 * x * z - 45 * x * y * y - 90 * x * x * y - 135 * x * z * z - 180 * x * x * z + 75 * x * x - 45 * x * x * x - 180 * x * y * z;
   values[93] = 180 * x * z - 135 * z + 270 * y * z - 270 * x * z * z - 45 * x * x * z - 405 * y * z * z - 135 * y * y * z + 405 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[94] = 135 * x * z * z - 90 * x * z + 90 * x * x * z + 90 * x * y * z;
   values[95] = 45 * x - 90 * x * y - 270 * x * z + 45 * x * y * y + 90 * x * x * y + 270 * x * z * z + 270 * x * x * z - 90 * x * x + 45 * x * x * x + 270 * x * y * z;
   values[96] = 330 * x * z - 120 * z + 255 * y * z - 360 * x * z * z - 135 * x * x * z - 270 * y * z * z - 135 * y * y * z + 255 * z * z - 135 * z * z * z - 360 * x * y * z;
   values[97] = 90 * x * z * z - 105 * x * z + 180 * x * x * z + 90 * x * y * z;
   values[98] = 60 * x - 105 * x * y - 210 * x * z + 45 * x * y * y + 180 * x * x * y + 135 * x * z * z + 360 * x * x * z - 195 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[99] = 150 * x * z - 30 * z + 75 * y * z - 180 * x * z * z - 135 * x * x * z - 90 * y * z * z - 45 * y * y * z + 75 * z * z - 45 * z * z * z - 180 * x * y * z;
   values[100] = 75 * x * z - 90 * x * z * z - 90 * x * x * z - 90 * x * y * z;
   values[101] = 90 * x - 225 * x * y - 150 * x * z + 135 * x * y * y + 270 * x * x * y + 45 * x * z * z + 180 * x * x * z - 225 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[102] = 60 * z - 210 * x * z - 105 * y * z + 360 * x * z * z + 135 * x * x * z + 180 * y * z * z + 45 * y * y * z - 195 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[103] = 180 * x * z * z - 105 * x * z + 90 * x * x * z + 90 * x * y * z;
   values[104] = 255 * x * y - 120 * x + 330 * x * z - 135 * x * y * y - 270 * x * x * y - 135 * x * z * z - 360 * x * x * z + 255 * x * x - 135 * x * x * x - 360 * x * y * z;
   values[105] = 45 * z - 270 * x * z - 90 * y * z + 270 * x * z * z + 270 * x * x * z + 90 * y * z * z + 45 * y * y * z - 90 * z * z + 45 * z * z * z + 270 * x * y * z;
   values[106] = 90 * x * z * z - 90 * x * z + 135 * x * x * z + 90 * x * y * z;
   values[107] = 270 * x * y - 135 * x + 180 * x * z - 135 * x * y * y - 405 * x * x * y - 45 * x * z * z - 270 * x * x * z + 405 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[108] = 90 * y - 150 * x * y - 225 * y * z + 180 * x * y * y + 45 * x * x * y + 135 * y * z * z + 270 * y * y * z - 225 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[109] = 150 * x * y - 30 * x + 75 * x * z - 135 * x * y * y - 180 * x * x * y - 45 * x * z * z - 90 * x * x * z + 75 * x * x - 45 * x * x * x - 180 * x * y * z;
   values[110] = 75 * x * y - 90 * x * y * y - 90 * x * x * y - 90 * x * y * z;
   values[111] = 180 * x * y - 135 * y + 270 * y * z - 270 * x * y * y - 45 * x * x * y - 135 * y * z * z - 405 * y * y * z + 405 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[112] = 45 * x - 270 * x * y - 90 * x * z + 270 * x * y * y + 270 * x * x * y + 45 * x * z * z + 90 * x * x * z - 90 * x * x + 45 * x * x * x + 270 * x * y * z;
   values[113] = 135 * x * y * y - 90 * x * y + 90 * x * x * y + 90 * x * y * z;
   values[114] = 330 * x * y - 120 * y + 255 * y * z - 360 * x * y * y - 135 * x * x * y - 135 * y * z * z - 270 * y * y * z + 255 * y * y - 135 * y * y * y - 360 * x * y * z;
   values[115] = 60 * x - 210 * x * y - 105 * x * z + 135 * x * y * y + 360 * x * x * y + 45 * x * z * z + 180 * x * x * z - 195 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[116] = 90 * x * y * y - 105 * x * y + 180 * x * x * y + 90 * x * y * z;
   values[117] = 150 * x * y - 30 * y + 75 * y * z - 180 * x * y * y - 135 * x * x * y - 45 * y * z * z - 90 * y * y * z + 75 * y * y - 45 * y * y * y - 180 * x * y * z;
   values[118] = 90 * x - 150 * x * y - 225 * x * z + 45 * x * y * y + 180 * x * x * y + 135 * x * z * z + 270 * x * x * z - 225 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[119] = 75 * x * y - 90 * x * y * y - 90 * x * x * y - 90 * x * y * z;
   values[120] = 60 * y - 210 * x * y - 105 * y * z + 360 * x * y * y + 135 * x * x * y + 45 * y * z * z + 180 * y * y * z - 195 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[121] = 330 * x * y - 120 * x + 255 * x * z - 135 * x * y * y - 360 * x * x * y - 135 * x * z * z - 270 * x * x * z + 255 * x * x - 135 * x * x * x - 360 * x * y * z;
   values[122] = 180 * x * y * y - 105 * x * y + 90 * x * x * y + 90 * x * y * z;
   values[123] = 45 * y - 270 * x * y - 90 * y * z + 270 * x * y * y + 270 * x * x * y + 45 * y * z * z + 90 * y * y * z - 90 * y * y + 45 * y * y * y + 270 * x * y * z;
   values[124] = 180 * x * y - 135 * x + 270 * x * z - 45 * x * y * y - 270 * x * x * y - 135 * x * z * z - 405 * x * x * z + 405 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[125] = 90 * x * y * y - 90 * x * y + 135 * x * x * y + 90 * x * y * z;
   values[126] = 540 * y * z - 540 * y * z * z - 540 * y * y * z - 360 * x * y * z;
   values[127] = 180 * x * z * z - 180 * x * z + 180 * x * x * z + 360 * x * y * z;
   values[128] = 180 * x * y * y - 180 * x * y + 180 * x * x * y + 360 * x * y * z;
   values[129] = 180 * y * z * z - 180 * y * z + 180 * y * y * z + 360 * x * y * z;
   values[130] = 540 * x * z - 540 * x * z * z - 540 * x * x * z - 360 * x * y * z;
   values[131] = 180 * x * y * y - 180 * x * y + 180 * x * x * y + 360 * x * y * z;
   values[132] = 180 * y * z * z - 180 * y * z + 180 * y * y * z + 360 * x * y * z;
   values[133] = 180 * x * z * z - 180 * x * z + 180 * x * x * z + 360 * x * y * z;
   values[134] = 540 * x * y - 540 * x * y * y - 540 * x * x * y - 360 * x * y * z;
}
static void Curl_T_P3_3D_3D_D000(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 * x * y - 54 * y - 54 * z - 36 * x + 120 * x * z + 180 * y * z - 90 * x * y * y - 45 * x * x * y - 90 * x * z * z - 45 * x * x * z - 135 * y * z * z - 135 * y * y * z + 30 * x * x + 90 * y * y - 45 * y * y * y + 90 * z * z - 45 * z * z * z - 180 * x * y * z + 9;
   values[1] = 18 * x - 60 * x * y - 60 * x * z + 45 * x * y * y + 90 * x * x * y + 45 * x * z * z + 90 * x * x * z - 60 * x * x + 45 * x * x * x + 90 * x * y * z;
   values[2] = 18 * x - 60 * x * y - 60 * x * z + 45 * x * y * y + 90 * x * x * y + 45 * x * z * z + 90 * x * x * z - 60 * x * x + 45 * x * x * x + 90 * x * y * z;
   values[3] = 192 * x + 156 * y + 156 * z - 540 * x * y - 540 * x * z - 420 * y * z + 360 * x * y * y + 270 * x * x * y + 360 * x * z * z + 270 * x * x * z + 270 * y * z * z + 270 * y * y * z - 180 * x * x - 210 * y * y + 90 * y * y * y - 210 * z * z + 90 * z * z * z + 720 * x * y * z - 36;
   values[4] = 180 * x * y - 84 * x + 180 * x * z - 90 * x * y * y - 360 * x * x * y - 90 * x * z * z - 360 * x * x * z + 330 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[5] = 180 * x * y - 84 * x + 180 * x * z - 90 * x * y * y - 360 * x * x * y - 90 * x * z * z - 360 * x * x * z + 330 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[6] = 450 * x * y - 105 * y - 105 * z - 180 * x + 450 * x * z + 240 * y * z - 270 * x * y * y - 270 * x * x * y - 270 * x * z * z - 270 * x * x * z - 135 * y * z * z - 135 * y * y * z + 180 * x * x + 120 * y * y - 45 * y * y * y + 120 * z * z - 45 * z * z * z - 540 * x * y * z + 30;
   values[7] = 75 * x - 120 * x * y - 120 * x * z + 45 * x * y * y + 270 * x * x * y + 45 * x * z * z + 270 * x * x * z - 315 * x * x + 270 * x * x * x + 90 * x * y * z;
   values[8] = 75 * x - 120 * x * y - 120 * x * z + 45 * x * y * y + 270 * x * x * y + 45 * x * z * z + 270 * x * x * z - 315 * x * x + 270 * x * x * x + 90 * x * y * z;
   values[9] = 18 * y - 60 * x * y - 60 * y * z + 90 * x * y * y + 45 * x * x * y + 45 * y * z * z + 90 * y * y * z - 60 * y * y + 45 * y * y * y + 90 * x * y * z;
   values[10] = 120 * x * y - 36 * y - 54 * z - 54 * x + 180 * x * z + 120 * y * z - 45 * x * y * y - 90 * x * x * y - 135 * x * z * z - 135 * x * x * z - 90 * y * z * z - 45 * y * y * z + 90 * x * x - 45 * x * x * x + 30 * y * y + 90 * z * z - 45 * z * z * z - 180 * x * y * z + 9;
   values[11] = 18 * y - 60 * x * y - 60 * y * z + 90 * x * y * y + 45 * x * x * y + 45 * y * z * z + 90 * y * y * z - 60 * y * y + 45 * y * y * y + 90 * x * y * z;
   values[12] = 180 * x * y - 84 * y + 180 * y * z - 360 * x * y * y - 90 * x * x * y - 90 * y * z * z - 360 * y * y * z + 330 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[13] = 156 * x + 192 * y + 156 * z - 540 * x * y - 420 * x * z - 540 * y * z + 270 * x * y * y + 360 * x * x * y + 270 * x * z * z + 270 * x * x * z + 360 * y * z * z + 270 * y * y * z - 210 * x * x + 90 * x * x * x - 180 * y * y - 210 * z * z + 90 * z * z * z + 720 * x * y * z - 36;
   values[14] = 180 * x * y - 84 * y + 180 * y * z - 360 * x * y * y - 90 * x * x * y - 90 * y * z * z - 360 * y * y * z + 330 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[15] = 75 * y - 120 * x * y - 120 * y * z + 270 * x * y * y + 45 * x * x * y + 45 * y * z * z + 270 * y * y * z - 315 * y * y + 270 * y * y * y + 90 * x * y * z;
   values[16] = 450 * x * y - 180 * y - 105 * z - 105 * x + 240 * x * z + 450 * y * z - 270 * x * y * y - 270 * x * x * y - 135 * x * z * z - 135 * x * x * z - 270 * y * z * z - 270 * y * y * z + 120 * x * x - 45 * x * x * x + 180 * y * y + 120 * z * z - 45 * z * z * z - 540 * x * y * z + 30;
   values[17] = 75 * y - 120 * x * y - 120 * y * z + 270 * x * y * y + 45 * x * x * y + 45 * y * z * z + 270 * y * y * z - 315 * y * y + 270 * y * y * y + 90 * x * y * z;
   values[18] = 18 * z - 60 * x * z - 60 * y * z + 90 * x * z * z + 45 * x * x * z + 90 * y * z * z + 45 * y * y * z - 60 * z * z + 45 * z * z * z + 90 * x * y * z;
   values[19] = 18 * z - 60 * x * z - 60 * y * z + 90 * x * z * z + 45 * x * x * z + 90 * y * z * z + 45 * y * y * z - 60 * z * z + 45 * z * z * z + 90 * x * y * z;
   values[20] = 180 * x * y - 54 * y - 36 * z - 54 * x + 120 * x * z + 120 * y * z - 135 * x * y * y - 135 * x * x * y - 45 * x * z * z - 90 * x * x * z - 45 * y * z * z - 90 * y * y * z + 90 * x * x - 45 * x * x * x + 90 * y * y - 45 * y * y * y + 30 * z * z - 180 * x * y * z + 9;
   values[21] = 180 * x * z - 84 * z + 180 * y * z - 360 * x * z * z - 90 * x * x * z - 360 * y * z * z - 90 * y * y * z + 330 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[22] = 180 * x * z - 84 * z + 180 * y * z - 360 * x * z * z - 90 * x * x * z - 360 * y * z * z - 90 * y * y * z + 330 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[23] = 156 * x + 156 * y + 192 * z - 420 * x * y - 540 * x * z - 540 * y * z + 270 * x * y * y + 270 * x * x * y + 270 * x * z * z + 360 * x * x * z + 270 * y * z * z + 360 * y * y * z - 210 * x * x + 90 * x * x * x - 210 * y * y + 90 * y * y * y - 180 * z * z + 720 * x * y * z - 36;
   values[24] = 75 * z - 120 * x * z - 120 * y * z + 270 * x * z * z + 45 * x * x * z + 270 * y * z * z + 45 * y * y * z - 315 * z * z + 270 * z * z * z + 90 * x * y * z;
   values[25] = 75 * z - 120 * x * z - 120 * y * z + 270 * x * z * z + 45 * x * x * z + 270 * y * z * z + 45 * y * y * z - 315 * z * z + 270 * z * z * z + 90 * x * y * z;
   values[26] = 240 * x * y - 105 * y - 180 * z - 105 * x + 450 * x * z + 450 * y * z - 135 * x * y * y - 135 * x * x * y - 270 * x * z * z - 270 * x * x * z - 270 * y * z * z - 270 * y * y * z + 120 * x * x - 45 * x * x * x + 120 * y * y - 45 * y * y * y + 180 * z * z - 540 * x * y * z + 30;
   values[27] = 30 * x * y - 3 * y - 45 * x * x * y;
   values[28] = 9 * x - 45 * x * x + 45 * x * x * x;
   values[29] = 0;
   values[30] = 90 * x * x * y - 180 * x * y * y - 6 * y + 30 * y * y;
   values[31] = 180 * x * x * y - 60 * x * y - 6 * x + 60 * x * x - 90 * x * x * x;
   values[32] = 0;
   values[33] = 180 * x * y * y - 30 * x * y - 45 * x * x * y + 15 * y * y - 45 * y * y * y;
   values[34] = 30 * x * y + 45 * x * y * y - 180 * x * x * y - 15 * x * x + 45 * x * x * x;
   values[35] = 0;
   values[36] = 30 * x * z - 3 * z - 45 * x * x * z;
   values[37] = 0;
   values[38] = 9 * x - 45 * x * x + 45 * x * x * x;
   values[39] = 90 * x * x * z - 180 * x * z * z - 6 * z + 30 * z * z;
   values[40] = 0;
   values[41] = 180 * x * x * z - 60 * x * z - 6 * x + 60 * x * x - 90 * x * x * x;
   values[42] = 180 * x * z * z - 30 * x * z - 45 * x * x * z + 15 * z * z - 45 * z * z * z;
   values[43] = 0;
   values[44] = 30 * x * z + 45 * x * z * z - 180 * x * x * z - 15 * x * x + 45 * x * x * x;
   values[45] = 0;
   values[46] = 30 * y * z - 3 * z - 45 * y * y * z;
   values[47] = 9 * y - 45 * y * y + 45 * y * y * y;
   values[48] = 0;
   values[49] = 90 * y * y * z - 180 * y * z * z - 6 * z + 30 * z * z;
   values[50] = 180 * y * y * z - 60 * y * z - 6 * y + 60 * y * y - 90 * y * y * y;
   values[51] = 0;
   values[52] = 180 * y * z * z - 30 * y * z - 45 * y * y * z + 15 * z * z - 45 * z * z * z;
   values[53] = 30 * y * z + 45 * y * z * z - 180 * y * y * z - 15 * y * y + 45 * y * y * y;
   values[54] = 15 * y * z - 90 * x * y * z;
   values[55] = 135 * x * x * z - 45 * x * z;
   values[56] = 15 * x * y - 45 * x * x * y;
   values[57] = 90 * x * y * z - 45 * y * z * z;
   values[58] = 135 * x * z * z - 135 * x * x * z;
   values[59] = 45 * x * x * y - 90 * x * y * z;
   values[60] = 15 * y * z - 90 * y * y * z + 90 * x * y * z;
   values[61] = 15 * x * z - 135 * x * x * z + 180 * x * y * z;
   values[62] = 15 * x * y - 90 * x * y * y + 45 * x * x * y;
   values[63] = 15 * y * z - 90 * x * y * z;
   values[64] = 15 * x * z - 45 * x * x * z;
   values[65] = 135 * x * x * y - 45 * x * y;
   values[66] = 15 * y * z - 90 * y * z * z + 90 * x * y * z;
   values[67] = 15 * x * z - 90 * x * z * z + 45 * x * x * z;
   values[68] = 15 * x * y - 135 * x * x * y + 180 * x * y * z;
   values[69] = 90 * x * y * z - 45 * y * y * z;
   values[70] = 45 * x * x * z - 90 * x * y * z;
   values[71] = 135 * x * y * y - 135 * x * x * y;
   values[72] = 75 * y * z - 90 * y * z * z - 90 * y * y * z - 90 * x * y * z;
   values[73] = 90 * z - 225 * x * z - 150 * y * z + 270 * x * z * z + 135 * x * x * z + 180 * y * z * z + 45 * y * y * z - 225 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[74] = 75 * x * y - 30 * y + 150 * y * z - 90 * x * y * y - 45 * x * x * y - 135 * y * z * z - 180 * y * y * z + 75 * y * y - 45 * y * y * y - 180 * x * y * z;
   values[75] = 135 * y * z * z - 90 * y * z + 90 * y * y * z + 90 * x * y * z;
   values[76] = 270 * x * z - 135 * z + 180 * y * z - 405 * x * z * z - 135 * x * x * z - 270 * y * z * z - 45 * y * y * z + 405 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[77] = 45 * y - 90 * x * y - 270 * y * z + 90 * x * y * y + 45 * x * x * y + 270 * y * z * z + 270 * y * y * z - 90 * y * y + 45 * y * y * y + 270 * x * y * z;
   values[78] = 90 * y * z * z - 105 * y * z + 180 * y * y * z + 90 * x * y * z;
   values[79] = 255 * x * z - 120 * z + 330 * y * z - 270 * x * z * z - 135 * x * x * z - 360 * y * z * z - 135 * y * y * z + 255 * z * z - 135 * z * z * z - 360 * x * y * z;
   values[80] = 60 * y - 105 * x * y - 210 * y * z + 180 * x * y * y + 45 * x * x * y + 135 * y * z * z + 360 * y * y * z - 195 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[81] = 75 * y * z - 90 * y * z * z - 90 * y * y * z - 90 * x * y * z;
   values[82] = 75 * x * z - 30 * z + 150 * y * z - 90 * x * z * z - 45 * x * x * z - 180 * y * z * z - 135 * y * y * z + 75 * z * z - 45 * z * z * z - 180 * x * y * z;
   values[83] = 90 * y - 225 * x * y - 150 * y * z + 270 * x * y * y + 135 * x * x * y + 45 * y * z * z + 180 * y * y * z - 225 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[84] = 180 * y * z * z - 105 * y * z + 90 * y * y * z + 90 * x * y * z;
   values[85] = 60 * z - 105 * x * z - 210 * y * z + 180 * x * z * z + 45 * x * x * z + 360 * y * z * z + 135 * y * y * z - 195 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[86] = 255 * x * y - 120 * y + 330 * y * z - 270 * x * y * y - 135 * x * x * y - 135 * y * z * z - 360 * y * y * z + 255 * y * y - 135 * y * y * y - 360 * x * y * z;
   values[87] = 90 * y * z * z - 90 * y * z + 135 * y * y * z + 90 * x * y * z;
   values[88] = 45 * z - 90 * x * z - 270 * y * z + 90 * x * z * z + 45 * x * x * z + 270 * y * z * z + 270 * y * y * z - 90 * z * z + 45 * z * z * z + 270 * x * y * z;
   values[89] = 270 * x * y - 135 * y + 180 * y * z - 405 * x * y * y - 135 * x * x * y - 45 * y * z * z - 270 * y * y * z + 405 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[90] = 90 * z - 150 * x * z - 225 * y * z + 180 * x * z * z + 45 * x * x * z + 270 * y * z * z + 135 * y * y * z - 225 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[91] = 75 * x * z - 90 * x * z * z - 90 * x * x * z - 90 * x * y * z;
   values[92] = 75 * x * y - 30 * x + 150 * x * z - 45 * x * y * y - 90 * x * x * y - 135 * x * z * z - 180 * x * x * z + 75 * x * x - 45 * x * x * x - 180 * x * y * z;
   values[93] = 180 * x * z - 135 * z + 270 * y * z - 270 * x * z * z - 45 * x * x * z - 405 * y * z * z - 135 * y * y * z + 405 * z * z - 270 * z * z * z - 180 * x * y * z;
   values[94] = 135 * x * z * z - 90 * x * z + 90 * x * x * z + 90 * x * y * z;
   values[95] = 45 * x - 90 * x * y - 270 * x * z + 45 * x * y * y + 90 * x * x * y + 270 * x * z * z + 270 * x * x * z - 90 * x * x + 45 * x * x * x + 270 * x * y * z;
   values[96] = 330 * x * z - 120 * z + 255 * y * z - 360 * x * z * z - 135 * x * x * z - 270 * y * z * z - 135 * y * y * z + 255 * z * z - 135 * z * z * z - 360 * x * y * z;
   values[97] = 90 * x * z * z - 105 * x * z + 180 * x * x * z + 90 * x * y * z;
   values[98] = 60 * x - 105 * x * y - 210 * x * z + 45 * x * y * y + 180 * x * x * y + 135 * x * z * z + 360 * x * x * z - 195 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[99] = 150 * x * z - 30 * z + 75 * y * z - 180 * x * z * z - 135 * x * x * z - 90 * y * z * z - 45 * y * y * z + 75 * z * z - 45 * z * z * z - 180 * x * y * z;
   values[100] = 75 * x * z - 90 * x * z * z - 90 * x * x * z - 90 * x * y * z;
   values[101] = 90 * x - 225 * x * y - 150 * x * z + 135 * x * y * y + 270 * x * x * y + 45 * x * z * z + 180 * x * x * z - 225 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[102] = 60 * z - 210 * x * z - 105 * y * z + 360 * x * z * z + 135 * x * x * z + 180 * y * z * z + 45 * y * y * z - 195 * z * z + 135 * z * z * z + 180 * x * y * z;
   values[103] = 180 * x * z * z - 105 * x * z + 90 * x * x * z + 90 * x * y * z;
   values[104] = 255 * x * y - 120 * x + 330 * x * z - 135 * x * y * y - 270 * x * x * y - 135 * x * z * z - 360 * x * x * z + 255 * x * x - 135 * x * x * x - 360 * x * y * z;
   values[105] = 45 * z - 270 * x * z - 90 * y * z + 270 * x * z * z + 270 * x * x * z + 90 * y * z * z + 45 * y * y * z - 90 * z * z + 45 * z * z * z + 270 * x * y * z;
   values[106] = 90 * x * z * z - 90 * x * z + 135 * x * x * z + 90 * x * y * z;
   values[107] = 270 * x * y - 135 * x + 180 * x * z - 135 * x * y * y - 405 * x * x * y - 45 * x * z * z - 270 * x * x * z + 405 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[108] = 90 * y - 150 * x * y - 225 * y * z + 180 * x * y * y + 45 * x * x * y + 135 * y * z * z + 270 * y * y * z - 225 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[109] = 150 * x * y - 30 * x + 75 * x * z - 135 * x * y * y - 180 * x * x * y - 45 * x * z * z - 90 * x * x * z + 75 * x * x - 45 * x * x * x - 180 * x * y * z;
   values[110] = 75 * x * y - 90 * x * y * y - 90 * x * x * y - 90 * x * y * z;
   values[111] = 180 * x * y - 135 * y + 270 * y * z - 270 * x * y * y - 45 * x * x * y - 135 * y * z * z - 405 * y * y * z + 405 * y * y - 270 * y * y * y - 180 * x * y * z;
   values[112] = 45 * x - 270 * x * y - 90 * x * z + 270 * x * y * y + 270 * x * x * y + 45 * x * z * z + 90 * x * x * z - 90 * x * x + 45 * x * x * x + 270 * x * y * z;
   values[113] = 135 * x * y * y - 90 * x * y + 90 * x * x * y + 90 * x * y * z;
   values[114] = 330 * x * y - 120 * y + 255 * y * z - 360 * x * y * y - 135 * x * x * y - 135 * y * z * z - 270 * y * y * z + 255 * y * y - 135 * y * y * y - 360 * x * y * z;
   values[115] = 60 * x - 210 * x * y - 105 * x * z + 135 * x * y * y + 360 * x * x * y + 45 * x * z * z + 180 * x * x * z - 195 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[116] = 90 * x * y * y - 105 * x * y + 180 * x * x * y + 90 * x * y * z;
   values[117] = 150 * x * y - 30 * y + 75 * y * z - 180 * x * y * y - 135 * x * x * y - 45 * y * z * z - 90 * y * y * z + 75 * y * y - 45 * y * y * y - 180 * x * y * z;
   values[118] = 90 * x - 150 * x * y - 225 * x * z + 45 * x * y * y + 180 * x * x * y + 135 * x * z * z + 270 * x * x * z - 225 * x * x + 135 * x * x * x + 180 * x * y * z;
   values[119] = 75 * x * y - 90 * x * y * y - 90 * x * x * y - 90 * x * y * z;
   values[120] = 60 * y - 210 * x * y - 105 * y * z + 360 * x * y * y + 135 * x * x * y + 45 * y * z * z + 180 * y * y * z - 195 * y * y + 135 * y * y * y + 180 * x * y * z;
   values[121] = 330 * x * y - 120 * x + 255 * x * z - 135 * x * y * y - 360 * x * x * y - 135 * x * z * z - 270 * x * x * z + 255 * x * x - 135 * x * x * x - 360 * x * y * z;
   values[122] = 180 * x * y * y - 105 * x * y + 90 * x * x * y + 90 * x * y * z;
   values[123] = 45 * y - 270 * x * y - 90 * y * z + 270 * x * y * y + 270 * x * x * y + 45 * y * z * z + 90 * y * y * z - 90 * y * y + 45 * y * y * y + 270 * x * y * z;
   values[124] = 180 * x * y - 135 * x + 270 * x * z - 45 * x * y * y - 270 * x * x * y - 135 * x * z * z - 405 * x * x * z + 405 * x * x - 270 * x * x * x - 180 * x * y * z;
   values[125] = 90 * x * y * y - 90 * x * y + 135 * x * x * y + 90 * x * y * z;
   values[126] = 540 * y * z - 540 * y * z * z - 540 * y * y * z - 360 * x * y * z;
   values[127] = 180 * x * z * z - 180 * x * z + 180 * x * x * z + 360 * x * y * z;
   values[128] = 180 * x * y * y - 180 * x * y + 180 * x * x * y + 360 * x * y * z;
   values[129] = 180 * y * z * z - 180 * y * z + 180 * y * y * z + 360 * x * y * z;
   values[130] = 540 * x * z - 540 * x * z * z - 540 * x * x * z - 360 * x * y * z;
   values[131] = 180 * x * y * y - 180 * x * y + 180 * x * x * y + 360 * x * y * z;
   values[132] = 180 * y * z * z - 180 * y * z + 180 * y * y * z + 360 * x * y * z;
   values[133] = 180 * x * z * z - 180 * x * z + 180 * x * x * z + 360 * x * y * z;
   values[134] = 540 * x * y - 540 * x * y * y - 540 * x * x * y - 360 * x * y * z;
}
static void Curl_T_P3_3D_3D_D100(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 60 * x + 120 * y + 120 * z - 90 * x * y - 90 * x * z - 180 * y * z - 90 * y * y - 90 * z * z - 36;
   values[1] = 180 * x * y - 60 * y - 60 * z - 120 * x + 180 * x * z + 90 * y * z + 135 * x * x + 45 * y * y + 45 * z * z + 18;
   values[2] = 180 * x * y - 60 * y - 60 * z - 120 * x + 180 * x * z + 90 * y * z + 135 * x * x + 45 * y * y + 45 * z * z + 18;
   values[3] = 540 * x * y - 540 * y - 540 * z - 360 * x + 540 * x * z + 720 * y * z + 360 * y * y + 360 * z * z + 192;
   values[4] = 660 * x + 180 * y + 180 * z - 720 * x * y - 720 * x * z - 180 * y * z - 810 * x * x - 90 * y * y - 90 * z * z - 84;
   values[5] = 660 * x + 180 * y + 180 * z - 720 * x * y - 720 * x * z - 180 * y * z - 810 * x * x - 90 * y * y - 90 * z * z - 84;
   values[6] = 360 * x + 450 * y + 450 * z - 540 * x * y - 540 * x * z - 540 * y * z - 270 * y * y - 270 * z * z - 180;
   values[7] = 540 * x * y - 120 * y - 120 * z - 630 * x + 540 * x * z + 90 * y * z + 810 * x * x + 45 * y * y + 45 * z * z + 75;
   values[8] = 540 * x * y - 120 * y - 120 * z - 630 * x + 540 * x * z + 90 * y * z + 810 * x * x + 45 * y * y + 45 * z * z + 75;
   values[9] = 90 * x * y - 60 * y + 90 * y * z + 90 * y * y;
   values[10] = 180 * x + 120 * y + 180 * z - 180 * x * y - 270 * x * z - 180 * y * z - 135 * x * x - 45 * y * y - 135 * z * z - 54;
   values[11] = 90 * x * y - 60 * y + 90 * y * z + 90 * y * y;
   values[12] = 180 * y - 180 * x * y - 180 * y * z - 360 * y * y;
   values[13] = 720 * x * y - 540 * y - 420 * z - 420 * x + 540 * x * z + 720 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[14] = 180 * y - 180 * x * y - 180 * y * z - 360 * y * y;
   values[15] = 90 * x * y - 120 * y + 90 * y * z + 270 * y * y;
   values[16] = 240 * x + 450 * y + 240 * z - 540 * x * y - 270 * x * z - 540 * y * z - 135 * x * x - 270 * y * y - 135 * z * z - 105;
   values[17] = 90 * x * y - 120 * y + 90 * y * z + 270 * y * y;
   values[18] = 90 * x * z - 60 * z + 90 * y * z + 90 * z * z;
   values[19] = 90 * x * z - 60 * z + 90 * y * z + 90 * z * z;
   values[20] = 180 * x + 180 * y + 120 * z - 270 * x * y - 180 * x * z - 180 * y * z - 135 * x * x - 135 * y * y - 45 * z * z - 54;
   values[21] = 180 * z - 180 * x * z - 180 * y * z - 360 * z * z;
   values[22] = 180 * z - 180 * x * z - 180 * y * z - 360 * z * z;
   values[23] = 540 * x * y - 420 * y - 540 * z - 420 * x + 720 * x * z + 720 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[24] = 90 * x * z - 120 * z + 90 * y * z + 270 * z * z;
   values[25] = 90 * x * z - 120 * z + 90 * y * z + 270 * z * z;
   values[26] = 240 * x + 240 * y + 450 * z - 270 * x * y - 540 * x * z - 540 * y * z - 135 * x * x - 135 * y * y - 270 * z * z - 105;
   values[27] = 30 * y - 90 * x * y;
   values[28] = 135 * x * x - 90 * x + 9;
   values[29] = 0;
   values[30] = 180 * x * y - 180 * y * y;
   values[31] = 120 * x - 60 * y + 360 * x * y - 270 * x * x - 6;
   values[32] = 0;
   values[33] = 180 * y * y - 90 * x * y - 30 * y;
   values[34] = 30 * y - 30 * x - 360 * x * y + 135 * x * x + 45 * y * y;
   values[35] = 0;
   values[36] = 30 * z - 90 * x * z;
   values[37] = 0;
   values[38] = 135 * x * x - 90 * x + 9;
   values[39] = 180 * x * z - 180 * z * z;
   values[40] = 0;
   values[41] = 120 * x - 60 * z + 360 * x * z - 270 * x * x - 6;
   values[42] = 180 * z * z - 90 * x * z - 30 * z;
   values[43] = 0;
   values[44] = 30 * z - 30 * x - 360 * x * z + 135 * x * x + 45 * z * z;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = -90 * y * z;
   values[55] = 270 * x * z - 45 * z;
   values[56] = 15 * y - 90 * x * y;
   values[57] = 90 * y * z;
   values[58] = 135 * z * z - 270 * x * z;
   values[59] = 90 * x * y - 90 * y * z;
   values[60] = 90 * y * z;
   values[61] = 15 * z - 270 * x * z + 180 * y * z;
   values[62] = 15 * y + 90 * x * y - 90 * y * y;
   values[63] = -90 * y * z;
   values[64] = 15 * z - 90 * x * z;
   values[65] = 270 * x * y - 45 * y;
   values[66] = 90 * y * z;
   values[67] = 15 * z + 90 * x * z - 90 * z * z;
   values[68] = 15 * y - 270 * x * y + 180 * y * z;
   values[69] = 90 * y * z;
   values[70] = 90 * x * z - 90 * y * z;
   values[71] = 135 * y * y - 270 * x * y;
   values[72] = -90 * y * z;
   values[73] = 270 * x * z - 225 * z + 180 * y * z + 270 * z * z;
   values[74] = 75 * y - 90 * x * y - 180 * y * z - 90 * y * y;
   values[75] = 90 * y * z;
   values[76] = 270 * z - 270 * x * z - 180 * y * z - 405 * z * z;
   values[77] = 90 * x * y - 90 * y + 270 * y * z + 90 * y * y;
   values[78] = 90 * y * z;
   values[79] = 255 * z - 270 * x * z - 360 * y * z - 270 * z * z;
   values[80] = 90 * x * y - 105 * y + 180 * y * z + 180 * y * y;
   values[81] = -90 * y * z;
   values[82] = 75 * z - 90 * x * z - 180 * y * z - 90 * z * z;
   values[83] = 270 * x * y - 225 * y + 180 * y * z + 270 * y * y;
   values[84] = 90 * y * z;
   values[85] = 90 * x * z - 105 * z + 180 * y * z + 180 * z * z;
   values[86] = 255 * y - 270 * x * y - 360 * y * z - 270 * y * y;
   values[87] = 90 * y * z;
   values[88] = 90 * x * z - 90 * z + 270 * y * z + 90 * z * z;
   values[89] = 270 * y - 270 * x * y - 180 * y * z - 405 * y * y;
   values[90] = 90 * x * z - 150 * z + 180 * y * z + 180 * z * z;
   values[91] = 75 * z - 180 * x * z - 90 * y * z - 90 * z * z;
   values[92] = 150 * x + 75 * y + 150 * z - 180 * x * y - 360 * x * z - 180 * y * z - 135 * x * x - 45 * y * y - 135 * z * z - 30;
   values[93] = 180 * z - 90 * x * z - 180 * y * z - 270 * z * z;
   values[94] = 180 * x * z - 90 * z + 90 * y * z + 135 * z * z;
   values[95] = 180 * x * y - 90 * y - 270 * z - 180 * x + 540 * x * z + 270 * y * z + 135 * x * x + 45 * y * y + 270 * z * z + 45;
   values[96] = 330 * z - 270 * x * z - 360 * y * z - 360 * z * z;
   values[97] = 360 * x * z - 105 * z + 90 * y * z + 90 * z * z;
   values[98] = 360 * x * y - 105 * y - 210 * z - 390 * x + 720 * x * z + 180 * y * z + 405 * x * x + 45 * y * y + 135 * z * z + 60;
   values[99] = 150 * z - 270 * x * z - 180 * y * z - 180 * z * z;
   values[100] = 75 * z - 180 * x * z - 90 * y * z - 90 * z * z;
   values[101] = 540 * x * y - 225 * y - 150 * z - 450 * x + 360 * x * z + 180 * y * z + 405 * x * x + 135 * y * y + 45 * z * z + 90;
   values[102] = 270 * x * z - 210 * z + 180 * y * z + 360 * z * z;
   values[103] = 180 * x * z - 105 * z + 90 * y * z + 180 * z * z;
   values[104] = 510 * x + 255 * y + 330 * z - 540 * x * y - 720 * x * z - 360 * y * z - 405 * x * x - 135 * y * y - 135 * z * z - 120;
   values[105] = 540 * x * z - 270 * z + 270 * y * z + 270 * z * z;
   values[106] = 270 * x * z - 90 * z + 90 * y * z + 90 * z * z;
   values[107] = 810 * x + 270 * y + 180 * z - 810 * x * y - 540 * x * z - 180 * y * z - 810 * x * x - 135 * y * y - 45 * z * z - 135;
   values[108] = 90 * x * y - 150 * y + 180 * y * z + 180 * y * y;
   values[109] = 150 * x + 150 * y + 75 * z - 360 * x * y - 180 * x * z - 180 * y * z - 135 * x * x - 135 * y * y - 45 * z * z - 30;
   values[110] = 75 * y - 180 * x * y - 90 * y * z - 90 * y * y;
   values[111] = 180 * y - 90 * x * y - 180 * y * z - 270 * y * y;
   values[112] = 540 * x * y - 270 * y - 90 * z - 180 * x + 180 * x * z + 270 * y * z + 135 * x * x + 270 * y * y + 45 * z * z + 45;
   values[113] = 180 * x * y - 90 * y + 90 * y * z + 135 * y * y;
   values[114] = 330 * y - 270 * x * y - 360 * y * z - 360 * y * y;
   values[115] = 720 * x * y - 210 * y - 105 * z - 390 * x + 360 * x * z + 180 * y * z + 405 * x * x + 135 * y * y + 45 * z * z + 60;
   values[116] = 360 * x * y - 105 * y + 90 * y * z + 90 * y * y;
   values[117] = 150 * y - 270 * x * y - 180 * y * z - 180 * y * y;
   values[118] = 360 * x * y - 150 * y - 225 * z - 450 * x + 540 * x * z + 180 * y * z + 405 * x * x + 45 * y * y + 135 * z * z + 90;
   values[119] = 75 * y - 180 * x * y - 90 * y * z - 90 * y * y;
   values[120] = 270 * x * y - 210 * y + 180 * y * z + 360 * y * y;
   values[121] = 510 * x + 330 * y + 255 * z - 720 * x * y - 540 * x * z - 360 * y * z - 405 * x * x - 135 * y * y - 135 * z * z - 120;
   values[122] = 180 * x * y - 105 * y + 90 * y * z + 180 * y * y;
   values[123] = 540 * x * y - 270 * y + 270 * y * z + 270 * y * y;
   values[124] = 810 * x + 180 * y + 270 * z - 540 * x * y - 810 * x * z - 180 * y * z - 810 * x * x - 45 * y * y - 135 * z * z - 135;
   values[125] = 270 * x * y - 90 * y + 90 * y * z + 90 * y * y;
   values[126] = -360 * y * z;
   values[127] = 360 * x * z - 180 * z + 360 * y * z + 180 * z * z;
   values[128] = 360 * x * y - 180 * y + 360 * y * z + 180 * y * y;
   values[129] = 360 * y * z;
   values[130] = 540 * z - 1080 * x * z - 360 * y * z - 540 * z * z;
   values[131] = 360 * x * y - 180 * y + 360 * y * z + 180 * y * y;
   values[132] = 360 * y * z;
   values[133] = 360 * x * z - 180 * z + 360 * y * z + 180 * z * z;
   values[134] = 540 * y - 1080 * x * y - 360 * y * z - 540 * y * y;
}
static void Curl_T_P3_3D_3D_D010(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 * x + 180 * y + 180 * z - 180 * x * y - 180 * x * z - 270 * y * z - 45 * x * x - 135 * y * y - 135 * z * z - 54;
   values[1] = 90 * x * y - 60 * x + 90 * x * z + 90 * x * x;
   values[2] = 90 * x * y - 60 * x + 90 * x * z + 90 * x * x;
   values[3] = 720 * x * y - 420 * y - 420 * z - 540 * x + 720 * x * z + 540 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[4] = 180 * x - 180 * x * y - 180 * x * z - 360 * x * x;
   values[5] = 180 * x - 180 * x * y - 180 * x * z - 360 * x * x;
   values[6] = 450 * x + 240 * y + 240 * z - 540 * x * y - 540 * x * z - 270 * y * z - 270 * x * x - 135 * y * y - 135 * z * z - 105;
   values[7] = 90 * x * y - 120 * x + 90 * x * z + 270 * x * x;
   values[8] = 90 * x * y - 120 * x + 90 * x * z + 270 * x * x;
   values[9] = 180 * x * y - 120 * y - 60 * z - 60 * x + 90 * x * z + 180 * y * z + 45 * x * x + 135 * y * y + 45 * z * z + 18;
   values[10] = 120 * x + 60 * y + 120 * z - 90 * x * y - 180 * x * z - 90 * y * z - 90 * x * x - 90 * z * z - 36;
   values[11] = 180 * x * y - 120 * y - 60 * z - 60 * x + 90 * x * z + 180 * y * z + 45 * x * x + 135 * y * y + 45 * z * z + 18;
   values[12] = 180 * x + 660 * y + 180 * z - 720 * x * y - 180 * x * z - 720 * y * z - 90 * x * x - 810 * y * y - 90 * z * z - 84;
   values[13] = 540 * x * y - 360 * y - 540 * z - 540 * x + 720 * x * z + 540 * y * z + 360 * x * x + 360 * z * z + 192;
   values[14] = 180 * x + 660 * y + 180 * z - 720 * x * y - 180 * x * z - 720 * y * z - 90 * x * x - 810 * y * y - 90 * z * z - 84;
   values[15] = 540 * x * y - 630 * y - 120 * z - 120 * x + 90 * x * z + 540 * y * z + 45 * x * x + 810 * y * y + 45 * z * z + 75;
   values[16] = 450 * x + 360 * y + 450 * z - 540 * x * y - 540 * x * z - 540 * y * z - 270 * x * x - 270 * z * z - 180;
   values[17] = 540 * x * y - 630 * y - 120 * z - 120 * x + 90 * x * z + 540 * y * z + 45 * x * x + 810 * y * y + 45 * z * z + 75;
   values[18] = 90 * x * z - 60 * z + 90 * y * z + 90 * z * z;
   values[19] = 90 * x * z - 60 * z + 90 * y * z + 90 * z * z;
   values[20] = 180 * x + 180 * y + 120 * z - 270 * x * y - 180 * x * z - 180 * y * z - 135 * x * x - 135 * y * y - 45 * z * z - 54;
   values[21] = 180 * z - 180 * x * z - 180 * y * z - 360 * z * z;
   values[22] = 180 * z - 180 * x * z - 180 * y * z - 360 * z * z;
   values[23] = 540 * x * y - 420 * y - 540 * z - 420 * x + 720 * x * z + 720 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[24] = 90 * x * z - 120 * z + 90 * y * z + 270 * z * z;
   values[25] = 90 * x * z - 120 * z + 90 * y * z + 270 * z * z;
   values[26] = 240 * x + 240 * y + 450 * z - 270 * x * y - 540 * x * z - 540 * y * z - 135 * x * x - 135 * y * y - 270 * z * z - 105;
   values[27] = 30 * x - 45 * x * x - 3;
   values[28] = 0;
   values[29] = 0;
   values[30] = 60 * y - 360 * x * y + 90 * x * x - 6;
   values[31] = 180 * x * x - 60 * x;
   values[32] = 0;
   values[33] = 30 * y - 30 * x + 360 * x * y - 45 * x * x - 135 * y * y;
   values[34] = 30 * x + 90 * x * y - 180 * x * x;
   values[35] = 0;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 30 * z - 90 * y * z;
   values[47] = 135 * y * y - 90 * y + 9;
   values[48] = 0;
   values[49] = 180 * y * z - 180 * z * z;
   values[50] = 120 * y - 60 * z + 360 * y * z - 270 * y * y - 6;
   values[51] = 0;
   values[52] = 180 * z * z - 90 * y * z - 30 * z;
   values[53] = 30 * z - 30 * y - 360 * y * z + 135 * y * y + 45 * z * z;
   values[54] = 15 * z - 90 * x * z;
   values[55] = 0;
   values[56] = 15 * x - 45 * x * x;
   values[57] = 90 * x * z - 45 * z * z;
   values[58] = 0;
   values[59] = 45 * x * x - 90 * x * z;
   values[60] = 15 * z + 90 * x * z - 180 * y * z;
   values[61] = 180 * x * z;
   values[62] = 15 * x - 180 * x * y + 45 * x * x;
   values[63] = 15 * z - 90 * x * z;
   values[64] = 0;
   values[65] = 135 * x * x - 45 * x;
   values[66] = 15 * z + 90 * x * z - 90 * z * z;
   values[67] = 0;
   values[68] = 15 * x + 180 * x * z - 135 * x * x;
   values[69] = 90 * x * z - 90 * y * z;
   values[70] = -90 * x * z;
   values[71] = 270 * x * y - 135 * x * x;
   values[72] = 75 * z - 90 * x * z - 180 * y * z - 90 * z * z;
   values[73] = 180 * x * z - 150 * z + 90 * y * z + 180 * z * z;
   values[74] = 75 * x + 150 * y + 150 * z - 180 * x * y - 180 * x * z - 360 * y * z - 45 * x * x - 135 * y * y - 135 * z * z - 30;
   values[75] = 90 * x * z - 90 * z + 180 * y * z + 135 * z * z;
   values[76] = 180 * z - 180 * x * z - 90 * y * z - 270 * z * z;
   values[77] = 180 * x * y - 180 * y - 270 * z - 90 * x + 270 * x * z + 540 * y * z + 45 * x * x + 135 * y * y + 270 * z * z + 45;
   values[78] = 90 * x * z - 105 * z + 360 * y * z + 90 * z * z;
   values[79] = 330 * z - 360 * x * z - 270 * y * z - 360 * z * z;
   values[80] = 360 * x * y - 390 * y - 210 * z - 105 * x + 180 * x * z + 720 * y * z + 45 * x * x + 405 * y * y + 135 * z * z + 60;
   values[81] = 75 * z - 90 * x * z - 180 * y * z - 90 * z * z;
   values[82] = 150 * z - 180 * x * z - 270 * y * z - 180 * z * z;
   values[83] = 540 * x * y - 450 * y - 150 * z - 225 * x + 180 * x * z + 360 * y * z + 135 * x * x + 405 * y * y + 45 * z * z + 90;
   values[84] = 90 * x * z - 105 * z + 180 * y * z + 180 * z * z;
   values[85] = 180 * x * z - 210 * z + 270 * y * z + 360 * z * z;
   values[86] = 255 * x + 510 * y + 330 * z - 540 * x * y - 360 * x * z - 720 * y * z - 135 * x * x - 405 * y * y - 135 * z * z - 120;
   values[87] = 90 * x * z - 90 * z + 270 * y * z + 90 * z * z;
   values[88] = 270 * x * z - 270 * z + 540 * y * z + 270 * z * z;
   values[89] = 270 * x + 810 * y + 180 * z - 810 * x * y - 180 * x * z - 540 * y * z - 135 * x * x - 810 * y * y - 45 * z * z - 135;
   values[90] = 180 * x * z - 225 * z + 270 * y * z + 270 * z * z;
   values[91] = -90 * x * z;
   values[92] = 75 * x - 90 * x * y - 180 * x * z - 90 * x * x;
   values[93] = 270 * z - 180 * x * z - 270 * y * z - 405 * z * z;
   values[94] = 90 * x * z;
   values[95] = 90 * x * y - 90 * x + 270 * x * z + 90 * x * x;
   values[96] = 255 * z - 360 * x * z - 270 * y * z - 270 * z * z;
   values[97] = 90 * x * z;
   values[98] = 90 * x * y - 105 * x + 180 * x * z + 180 * x * x;
   values[99] = 75 * z - 180 * x * z - 90 * y * z - 90 * z * z;
   values[100] = -90 * x * z;
   values[101] = 270 * x * y - 225 * x + 180 * x * z + 270 * x * x;
   values[102] = 180 * x * z - 105 * z + 90 * y * z + 180 * z * z;
   values[103] = 90 * x * z;
   values[104] = 255 * x - 270 * x * y - 360 * x * z - 270 * x * x;
   values[105] = 270 * x * z - 90 * z + 90 * y * z + 90 * z * z;
   values[106] = 90 * x * z;
   values[107] = 270 * x - 270 * x * y - 180 * x * z - 405 * x * x;
   values[108] = 360 * x * y - 450 * y - 225 * z - 150 * x + 180 * x * z + 540 * y * z + 45 * x * x + 405 * y * y + 135 * z * z + 90;
   values[109] = 150 * x - 270 * x * y - 180 * x * z - 180 * x * x;
   values[110] = 75 * x - 180 * x * y - 90 * x * z - 90 * x * x;
   values[111] = 180 * x + 810 * y + 270 * z - 540 * x * y - 180 * x * z - 810 * y * z - 45 * x * x - 810 * y * y - 135 * z * z - 135;
   values[112] = 540 * x * y - 270 * x + 270 * x * z + 270 * x * x;
   values[113] = 270 * x * y - 90 * x + 90 * x * z + 90 * x * x;
   values[114] = 330 * x + 510 * y + 255 * z - 720 * x * y - 360 * x * z - 540 * y * z - 135 * x * x - 405 * y * y - 135 * z * z - 120;
   values[115] = 270 * x * y - 210 * x + 180 * x * z + 360 * x * x;
   values[116] = 180 * x * y - 105 * x + 90 * x * z + 180 * x * x;
   values[117] = 150 * x + 150 * y + 75 * z - 360 * x * y - 180 * x * z - 180 * y * z - 135 * x * x - 135 * y * y - 45 * z * z - 30;
   values[118] = 90 * x * y - 150 * x + 180 * x * z + 180 * x * x;
   values[119] = 75 * x - 180 * x * y - 90 * x * z - 90 * x * x;
   values[120] = 720 * x * y - 390 * y - 105 * z - 210 * x + 180 * x * z + 360 * y * z + 135 * x * x + 405 * y * y + 45 * z * z + 60;
   values[121] = 330 * x - 270 * x * y - 360 * x * z - 360 * x * x;
   values[122] = 360 * x * y - 105 * x + 90 * x * z + 90 * x * x;
   values[123] = 540 * x * y - 180 * y - 90 * z - 270 * x + 270 * x * z + 180 * y * z + 270 * x * x + 135 * y * y + 45 * z * z + 45;
   values[124] = 180 * x - 90 * x * y - 180 * x * z - 270 * x * x;
   values[125] = 180 * x * y - 90 * x + 90 * x * z + 135 * x * x;
   values[126] = 540 * z - 360 * x * z - 1080 * y * z - 540 * z * z;
   values[127] = 360 * x * z;
   values[128] = 360 * x * y - 180 * x + 360 * x * z + 180 * x * x;
   values[129] = 360 * x * z - 180 * z + 360 * y * z + 180 * z * z;
   values[130] = -360 * x * z;
   values[131] = 360 * x * y - 180 * x + 360 * x * z + 180 * x * x;
   values[132] = 360 * x * z - 180 * z + 360 * y * z + 180 * z * z;
   values[133] = 360 * x * z;
   values[134] = 540 * x - 1080 * x * y - 360 * x * z - 540 * x * x;
}
static void Curl_T_P3_3D_3D_D001(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 * x + 180 * y + 180 * z - 180 * x * y - 180 * x * z - 270 * y * z - 45 * x * x - 135 * y * y - 135 * z * z - 54;
   values[1] = 90 * x * y - 60 * x + 90 * x * z + 90 * x * x;
   values[2] = 90 * x * y - 60 * x + 90 * x * z + 90 * x * x;
   values[3] = 720 * x * y - 420 * y - 420 * z - 540 * x + 720 * x * z + 540 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[4] = 180 * x - 180 * x * y - 180 * x * z - 360 * x * x;
   values[5] = 180 * x - 180 * x * y - 180 * x * z - 360 * x * x;
   values[6] = 450 * x + 240 * y + 240 * z - 540 * x * y - 540 * x * z - 270 * y * z - 270 * x * x - 135 * y * y - 135 * z * z - 105;
   values[7] = 90 * x * y - 120 * x + 90 * x * z + 270 * x * x;
   values[8] = 90 * x * y - 120 * x + 90 * x * z + 270 * x * x;
   values[9] = 90 * x * y - 60 * y + 90 * y * z + 90 * y * y;
   values[10] = 180 * x + 120 * y + 180 * z - 180 * x * y - 270 * x * z - 180 * y * z - 135 * x * x - 45 * y * y - 135 * z * z - 54;
   values[11] = 90 * x * y - 60 * y + 90 * y * z + 90 * y * y;
   values[12] = 180 * y - 180 * x * y - 180 * y * z - 360 * y * y;
   values[13] = 720 * x * y - 540 * y - 420 * z - 420 * x + 540 * x * z + 720 * y * z + 270 * x * x + 270 * y * y + 270 * z * z + 156;
   values[14] = 180 * y - 180 * x * y - 180 * y * z - 360 * y * y;
   values[15] = 90 * x * y - 120 * y + 90 * y * z + 270 * y * y;
   values[16] = 240 * x + 450 * y + 240 * z - 540 * x * y - 270 * x * z - 540 * y * z - 135 * x * x - 270 * y * y - 135 * z * z - 105;
   values[17] = 90 * x * y - 120 * y + 90 * y * z + 270 * y * y;
   values[18] = 90 * x * y - 60 * y - 120 * z - 60 * x + 180 * x * z + 180 * y * z + 45 * x * x + 45 * y * y + 135 * z * z + 18;
   values[19] = 90 * x * y - 60 * y - 120 * z - 60 * x + 180 * x * z + 180 * y * z + 45 * x * x + 45 * y * y + 135 * z * z + 18;
   values[20] = 120 * x + 120 * y + 60 * z - 180 * x * y - 90 * x * z - 90 * y * z - 90 * x * x - 90 * y * y - 36;
   values[21] = 180 * x + 180 * y + 660 * z - 180 * x * y - 720 * x * z - 720 * y * z - 90 * x * x - 90 * y * y - 810 * z * z - 84;
   values[22] = 180 * x + 180 * y + 660 * z - 180 * x * y - 720 * x * z - 720 * y * z - 90 * x * x - 90 * y * y - 810 * z * z - 84;
   values[23] = 720 * x * y - 540 * y - 360 * z - 540 * x + 540 * x * z + 540 * y * z + 360 * x * x + 360 * y * y + 192;
   values[24] = 90 * x * y - 120 * y - 630 * z - 120 * x + 540 * x * z + 540 * y * z + 45 * x * x + 45 * y * y + 810 * z * z + 75;
   values[25] = 90 * x * y - 120 * y - 630 * z - 120 * x + 540 * x * z + 540 * y * z + 45 * x * x + 45 * y * y + 810 * z * z + 75;
   values[26] = 450 * x + 450 * y + 360 * z - 540 * x * y - 540 * x * z - 540 * y * z - 270 * x * x - 270 * y * y - 180;
   values[27] = 0;
   values[28] = 0;
   values[29] = 0;
   values[30] = 0;
   values[31] = 0;
   values[32] = 0;
   values[33] = 0;
   values[34] = 0;
   values[35] = 0;
   values[36] = 30 * x - 45 * x * x - 3;
   values[37] = 0;
   values[38] = 0;
   values[39] = 60 * z - 360 * x * z + 90 * x * x - 6;
   values[40] = 0;
   values[41] = 180 * x * x - 60 * x;
   values[42] = 30 * z - 30 * x + 360 * x * z - 45 * x * x - 135 * z * z;
   values[43] = 0;
   values[44] = 30 * x + 90 * x * z - 180 * x * x;
   values[45] = 0;
   values[46] = 30 * y - 45 * y * y - 3;
   values[47] = 0;
   values[48] = 0;
   values[49] = 60 * z - 360 * y * z + 90 * y * y - 6;
   values[50] = 180 * y * y - 60 * y;
   values[51] = 0;
   values[52] = 30 * z - 30 * y + 360 * y * z - 45 * y * y - 135 * z * z;
   values[53] = 30 * y + 90 * y * z - 180 * y * y;
   values[54] = 15 * y - 90 * x * y;
   values[55] = 135 * x * x - 45 * x;
   values[56] = 0;
   values[57] = 90 * x * y - 90 * y * z;
   values[58] = 270 * x * z - 135 * x * x;
   values[59] = -90 * x * y;
   values[60] = 15 * y + 90 * x * y - 90 * y * y;
   values[61] = 15 * x + 180 * x * y - 135 * x * x;
   values[62] = 0;
   values[63] = 15 * y - 90 * x * y;
   values[64] = 15 * x - 45 * x * x;
   values[65] = 0;
   values[66] = 15 * y + 90 * x * y - 180 * y * z;
   values[67] = 15 * x - 180 * x * z + 45 * x * x;
   values[68] = 180 * x * y;
   values[69] = 90 * x * y - 45 * y * y;
   values[70] = 45 * x * x - 90 * x * y;
   values[71] = 0;
   values[72] = 75 * y - 90 * x * y - 180 * y * z - 90 * y * y;
   values[73] = 180 * x * y - 150 * y - 450 * z - 225 * x + 540 * x * z + 360 * y * z + 135 * x * x + 45 * y * y + 405 * z * z + 90;
   values[74] = 150 * y - 180 * x * y - 270 * y * z - 180 * y * y;
   values[75] = 90 * x * y - 90 * y + 270 * y * z + 90 * y * y;
   values[76] = 270 * x + 180 * y + 810 * z - 180 * x * y - 810 * x * z - 540 * y * z - 135 * x * x - 45 * y * y - 810 * z * z - 135;
   values[77] = 270 * x * y - 270 * y + 540 * y * z + 270 * y * y;
   values[78] = 90 * x * y - 105 * y + 180 * y * z + 180 * y * y;
   values[79] = 255 * x + 330 * y + 510 * z - 360 * x * y - 540 * x * z - 720 * y * z - 135 * x * x - 135 * y * y - 405 * z * z - 120;
   values[80] = 180 * x * y - 210 * y + 270 * y * z + 360 * y * y;
   values[81] = 75 * y - 90 * x * y - 180 * y * z - 90 * y * y;
   values[82] = 75 * x + 150 * y + 150 * z - 180 * x * y - 180 * x * z - 360 * y * z - 45 * x * x - 135 * y * y - 135 * z * z - 30;
   values[83] = 180 * x * y - 150 * y + 90 * y * z + 180 * y * y;
   values[84] = 90 * x * y - 105 * y + 360 * y * z + 90 * y * y;
   values[85] = 180 * x * y - 210 * y - 390 * z - 105 * x + 360 * x * z + 720 * y * z + 45 * x * x + 135 * y * y + 405 * z * z + 60;
   values[86] = 330 * y - 360 * x * y - 270 * y * z - 360 * y * y;
   values[87] = 90 * x * y - 90 * y + 180 * y * z + 135 * y * y;
   values[88] = 270 * x * y - 270 * y - 180 * z - 90 * x + 180 * x * z + 540 * y * z + 45 * x * x + 270 * y * y + 135 * z * z + 45;
   values[89] = 180 * y - 180 * x * y - 90 * y * z - 270 * y * y;
   values[90] = 180 * x * y - 225 * y - 450 * z - 150 * x + 360 * x * z + 540 * y * z + 45 * x * x + 135 * y * y + 405 * z * z + 90;
   values[91] = 75 * x - 90 * x * y - 180 * x * z - 90 * x * x;
   values[92] = 150 * x - 180 * x * y - 270 * x * z - 180 * x * x;
   values[93] = 180 * x + 270 * y + 810 * z - 180 * x * y - 540 * x * z - 810 * y * z - 45 * x * x - 135 * y * y - 810 * z * z - 135;
   values[94] = 90 * x * y - 90 * x + 270 * x * z + 90 * x * x;
   values[95] = 270 * x * y - 270 * x + 540 * x * z + 270 * x * x;
   values[96] = 330 * x + 255 * y + 510 * z - 360 * x * y - 720 * x * z - 540 * y * z - 135 * x * x - 135 * y * y - 405 * z * z - 120;
   values[97] = 90 * x * y - 105 * x + 180 * x * z + 180 * x * x;
   values[98] = 180 * x * y - 210 * x + 270 * x * z + 360 * x * x;
   values[99] = 150 * x + 75 * y + 150 * z - 180 * x * y - 360 * x * z - 180 * y * z - 135 * x * x - 45 * y * y - 135 * z * z - 30;
   values[100] = 75 * x - 90 * x * y - 180 * x * z - 90 * x * x;
   values[101] = 180 * x * y - 150 * x + 90 * x * z + 180 * x * x;
   values[102] = 180 * x * y - 105 * y - 390 * z - 210 * x + 720 * x * z + 360 * y * z + 135 * x * x + 45 * y * y + 405 * z * z + 60;
   values[103] = 90 * x * y - 105 * x + 360 * x * z + 90 * x * x;
   values[104] = 330 * x - 360 * x * y - 270 * x * z - 360 * x * x;
   values[105] = 270 * x * y - 90 * y - 180 * z - 270 * x + 540 * x * z + 180 * y * z + 270 * x * x + 45 * y * y + 135 * z * z + 45;
   values[106] = 90 * x * y - 90 * x + 180 * x * z + 135 * x * x;
   values[107] = 180 * x - 180 * x * y - 90 * x * z - 270 * x * x;
   values[108] = 180 * x * y - 225 * y + 270 * y * z + 270 * y * y;
   values[109] = 75 * x - 180 * x * y - 90 * x * z - 90 * x * x;
   values[110] = -90 * x * y;
   values[111] = 270 * y - 180 * x * y - 270 * y * z - 405 * y * y;
   values[112] = 270 * x * y - 90 * x + 90 * x * z + 90 * x * x;
   values[113] = 90 * x * y;
   values[114] = 255 * y - 360 * x * y - 270 * y * z - 270 * y * y;
   values[115] = 180 * x * y - 105 * x + 90 * x * z + 180 * x * x;
   values[116] = 90 * x * y;
   values[117] = 75 * y - 180 * x * y - 90 * y * z - 90 * y * y;
   values[118] = 180 * x * y - 225 * x + 270 * x * z + 270 * x * x;
   values[119] = -90 * x * y;
   values[120] = 180 * x * y - 105 * y + 90 * y * z + 180 * y * y;
   values[121] = 255 * x - 360 * x * y - 270 * x * z - 270 * x * x;
   values[122] = 90 * x * y;
   values[123] = 270 * x * y - 90 * y + 90 * y * z + 90 * y * y;
   values[124] = 270 * x - 180 * x * y - 270 * x * z - 405 * x * x;
   values[125] = 90 * x * y;
   values[126] = 540 * y - 360 * x * y - 1080 * y * z - 540 * y * y;
   values[127] = 360 * x * y - 180 * x + 360 * x * z + 180 * x * x;
   values[128] = 360 * x * y;
   values[129] = 360 * x * y - 180 * y + 360 * y * z + 180 * y * y;
   values[130] = 540 * x - 360 * x * y - 1080 * x * z - 540 * x * x;
   values[131] = 360 * x * y;
   values[132] = 360 * x * y - 180 * y + 360 * y * z + 180 * y * y;
   values[133] = 360 * x * y - 180 * x + 360 * x * z + 180 * x * x;
   values[134] = -360 * x * y;
}
static void Curl_T_P3_3D_3D_D200(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 60 - 90 * z - 90 * y;
   values[1] = 270 * x + 180 * y + 180 * z - 120;
   values[2] = 270 * x + 180 * y + 180 * z - 120;
   values[3] = 540 * y + 540 * z - 360;
   values[4] = 660 - 720 * y - 720 * z - 1620 * x;
   values[5] = 660 - 720 * y - 720 * z - 1620 * x;
   values[6] = 360 - 540 * z - 540 * y;
   values[7] = 1620 * x + 540 * y + 540 * z - 630;
   values[8] = 1620 * x + 540 * y + 540 * z - 630;
   values[9] = 90 * y;
   values[10] = 180 - 180 * y - 270 * z - 270 * x;
   values[11] = 90 * y;
   values[12] = -180 * y;
   values[13] = 540 * x + 720 * y + 540 * z - 420;
   values[14] = -180 * y;
   values[15] = 90 * y;
   values[16] = 240 - 540 * y - 270 * z - 270 * x;
   values[17] = 90 * y;
   values[18] = 90 * z;
   values[19] = 90 * z;
   values[20] = 180 - 270 * y - 180 * z - 270 * x;
   values[21] = -180 * z;
   values[22] = -180 * z;
   values[23] = 540 * x + 540 * y + 720 * z - 420;
   values[24] = 90 * z;
   values[25] = 90 * z;
   values[26] = 240 - 270 * y - 540 * z - 270 * x;
   values[27] = -90 * y;
   values[28] = 270 * x - 90;
   values[29] = 0;
   values[30] = 180 * y;
   values[31] = 360 * y - 540 * x + 120;
   values[32] = 0;
   values[33] = -90 * y;
   values[34] = 270 * x - 360 * y - 30;
   values[35] = 0;
   values[36] = -90 * z;
   values[37] = 0;
   values[38] = 270 * x - 90;
   values[39] = 180 * z;
   values[40] = 0;
   values[41] = 360 * z - 540 * x + 120;
   values[42] = -90 * z;
   values[43] = 0;
   values[44] = 270 * x - 360 * z - 30;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = 0;
   values[55] = 270 * z;
   values[56] = -90 * y;
   values[57] = 0;
   values[58] = -270 * z;
   values[59] = 90 * y;
   values[60] = 0;
   values[61] = -270 * z;
   values[62] = 90 * y;
   values[63] = 0;
   values[64] = -90 * z;
   values[65] = 270 * y;
   values[66] = 0;
   values[67] = 90 * z;
   values[68] = -270 * y;
   values[69] = 0;
   values[70] = 90 * z;
   values[71] = -270 * y;
   values[72] = 0;
   values[73] = 270 * z;
   values[74] = -90 * y;
   values[75] = 0;
   values[76] = -270 * z;
   values[77] = 90 * y;
   values[78] = 0;
   values[79] = -270 * z;
   values[80] = 90 * y;
   values[81] = 0;
   values[82] = -90 * z;
   values[83] = 270 * y;
   values[84] = 0;
   values[85] = 90 * z;
   values[86] = -270 * y;
   values[87] = 0;
   values[88] = 90 * z;
   values[89] = -270 * y;
   values[90] = 90 * z;
   values[91] = -180 * z;
   values[92] = 150 - 180 * y - 360 * z - 270 * x;
   values[93] = -90 * z;
   values[94] = 180 * z;
   values[95] = 270 * x + 180 * y + 540 * z - 180;
   values[96] = -270 * z;
   values[97] = 360 * z;
   values[98] = 810 * x + 360 * y + 720 * z - 390;
   values[99] = -270 * z;
   values[100] = -180 * z;
   values[101] = 810 * x + 540 * y + 360 * z - 450;
   values[102] = 270 * z;
   values[103] = 180 * z;
   values[104] = 510 - 540 * y - 720 * z - 810 * x;
   values[105] = 540 * z;
   values[106] = 270 * z;
   values[107] = 810 - 810 * y - 540 * z - 1620 * x;
   values[108] = 90 * y;
   values[109] = 150 - 360 * y - 180 * z - 270 * x;
   values[110] = -180 * y;
   values[111] = -90 * y;
   values[112] = 270 * x + 540 * y + 180 * z - 180;
   values[113] = 180 * y;
   values[114] = -270 * y;
   values[115] = 810 * x + 720 * y + 360 * z - 390;
   values[116] = 360 * y;
   values[117] = -270 * y;
   values[118] = 810 * x + 360 * y + 540 * z - 450;
   values[119] = -180 * y;
   values[120] = 270 * y;
   values[121] = 510 - 720 * y - 540 * z - 810 * x;
   values[122] = 180 * y;
   values[123] = 540 * y;
   values[124] = 810 - 540 * y - 810 * z - 1620 * x;
   values[125] = 270 * y;
   values[126] = 0;
   values[127] = 360 * z;
   values[128] = 360 * y;
   values[129] = 0;
   values[130] = -1080 * z;
   values[131] = 360 * y;
   values[132] = 0;
   values[133] = 360 * z;
   values[134] = -1080 * y;
}
static void Curl_T_P3_3D_3D_D020(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 180 - 270 * y - 270 * z - 180 * x;
   values[1] = 90 * x;
   values[2] = 90 * x;
   values[3] = 720 * x + 540 * y + 540 * z - 420;
   values[4] = -180 * x;
   values[5] = -180 * x;
   values[6] = 240 - 270 * y - 270 * z - 540 * x;
   values[7] = 90 * x;
   values[8] = 90 * x;
   values[9] = 180 * x + 270 * y + 180 * z - 120;
   values[10] = 60 - 90 * z - 90 * x;
   values[11] = 180 * x + 270 * y + 180 * z - 120;
   values[12] = 660 - 1620 * y - 720 * z - 720 * x;
   values[13] = 540 * x + 540 * z - 360;
   values[14] = 660 - 1620 * y - 720 * z - 720 * x;
   values[15] = 540 * x + 1620 * y + 540 * z - 630;
   values[16] = 360 - 540 * z - 540 * x;
   values[17] = 540 * x + 1620 * y + 540 * z - 630;
   values[18] = 90 * z;
   values[19] = 90 * z;
   values[20] = 180 - 270 * y - 180 * z - 270 * x;
   values[21] = -180 * z;
   values[22] = -180 * z;
   values[23] = 540 * x + 540 * y + 720 * z - 420;
   values[24] = 90 * z;
   values[25] = 90 * z;
   values[26] = 240 - 270 * y - 540 * z - 270 * x;
   values[27] = 0;
   values[28] = 0;
   values[29] = 0;
   values[30] = 60 - 360 * x;
   values[31] = 0;
   values[32] = 0;
   values[33] = 360 * x - 270 * y + 30;
   values[34] = 90 * x;
   values[35] = 0;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = -90 * z;
   values[47] = 270 * y - 90;
   values[48] = 0;
   values[49] = 180 * z;
   values[50] = 360 * z - 540 * y + 120;
   values[51] = 0;
   values[52] = -90 * z;
   values[53] = 270 * y - 360 * z - 30;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = 0;
   values[58] = 0;
   values[59] = 0;
   values[60] = -180 * z;
   values[61] = 0;
   values[62] = -180 * x;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = 0;
   values[67] = 0;
   values[68] = 0;
   values[69] = -90 * z;
   values[70] = 0;
   values[71] = 270 * x;
   values[72] = -180 * z;
   values[73] = 90 * z;
   values[74] = 150 - 270 * y - 360 * z - 180 * x;
   values[75] = 180 * z;
   values[76] = -90 * z;
   values[77] = 180 * x + 270 * y + 540 * z - 180;
   values[78] = 360 * z;
   values[79] = -270 * z;
   values[80] = 360 * x + 810 * y + 720 * z - 390;
   values[81] = -180 * z;
   values[82] = -270 * z;
   values[83] = 540 * x + 810 * y + 360 * z - 450;
   values[84] = 180 * z;
   values[85] = 270 * z;
   values[86] = 510 - 810 * y - 720 * z - 540 * x;
   values[87] = 270 * z;
   values[88] = 540 * z;
   values[89] = 810 - 1620 * y - 540 * z - 810 * x;
   values[90] = 270 * z;
   values[91] = 0;
   values[92] = -90 * x;
   values[93] = -270 * z;
   values[94] = 0;
   values[95] = 90 * x;
   values[96] = -270 * z;
   values[97] = 0;
   values[98] = 90 * x;
   values[99] = -90 * z;
   values[100] = 0;
   values[101] = 270 * x;
   values[102] = 90 * z;
   values[103] = 0;
   values[104] = -270 * x;
   values[105] = 90 * z;
   values[106] = 0;
   values[107] = -270 * x;
   values[108] = 360 * x + 810 * y + 540 * z - 450;
   values[109] = -270 * x;
   values[110] = -180 * x;
   values[111] = 810 - 1620 * y - 810 * z - 540 * x;
   values[112] = 540 * x;
   values[113] = 270 * x;
   values[114] = 510 - 810 * y - 540 * z - 720 * x;
   values[115] = 270 * x;
   values[116] = 180 * x;
   values[117] = 150 - 270 * y - 180 * z - 360 * x;
   values[118] = 90 * x;
   values[119] = -180 * x;
   values[120] = 720 * x + 810 * y + 360 * z - 390;
   values[121] = -270 * x;
   values[122] = 360 * x;
   values[123] = 540 * x + 270 * y + 180 * z - 180;
   values[124] = -90 * x;
   values[125] = 180 * x;
   values[126] = -1080 * z;
   values[127] = 0;
   values[128] = 360 * x;
   values[129] = 360 * z;
   values[130] = 0;
   values[131] = 360 * x;
   values[132] = 360 * z;
   values[133] = 0;
   values[134] = -1080 * x;
}
static void Curl_T_P3_3D_3D_D002(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 180 - 270 * y - 270 * z - 180 * x;
   values[1] = 90 * x;
   values[2] = 90 * x;
   values[3] = 720 * x + 540 * y + 540 * z - 420;
   values[4] = -180 * x;
   values[5] = -180 * x;
   values[6] = 240 - 270 * y - 270 * z - 540 * x;
   values[7] = 90 * x;
   values[8] = 90 * x;
   values[9] = 90 * y;
   values[10] = 180 - 180 * y - 270 * z - 270 * x;
   values[11] = 90 * y;
   values[12] = -180 * y;
   values[13] = 540 * x + 720 * y + 540 * z - 420;
   values[14] = -180 * y;
   values[15] = 90 * y;
   values[16] = 240 - 540 * y - 270 * z - 270 * x;
   values[17] = 90 * y;
   values[18] = 180 * x + 180 * y + 270 * z - 120;
   values[19] = 180 * x + 180 * y + 270 * z - 120;
   values[20] = 60 - 90 * y - 90 * x;
   values[21] = 660 - 720 * y - 1620 * z - 720 * x;
   values[22] = 660 - 720 * y - 1620 * z - 720 * x;
   values[23] = 540 * x + 540 * y - 360;
   values[24] = 540 * x + 540 * y + 1620 * z - 630;
   values[25] = 540 * x + 540 * y + 1620 * z - 630;
   values[26] = 360 - 540 * y - 540 * x;
   values[27] = 0;
   values[28] = 0;
   values[29] = 0;
   values[30] = 0;
   values[31] = 0;
   values[32] = 0;
   values[33] = 0;
   values[34] = 0;
   values[35] = 0;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 60 - 360 * x;
   values[40] = 0;
   values[41] = 0;
   values[42] = 360 * x - 270 * z + 30;
   values[43] = 0;
   values[44] = 90 * x;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 60 - 360 * y;
   values[50] = 0;
   values[51] = 0;
   values[52] = 360 * y - 270 * z + 30;
   values[53] = 90 * y;
   values[54] = 0;
   values[55] = 0;
   values[56] = 0;
   values[57] = -90 * y;
   values[58] = 270 * x;
   values[59] = 0;
   values[60] = 0;
   values[61] = 0;
   values[62] = 0;
   values[63] = 0;
   values[64] = 0;
   values[65] = 0;
   values[66] = -180 * y;
   values[67] = -180 * x;
   values[68] = 0;
   values[69] = 0;
   values[70] = 0;
   values[71] = 0;
   values[72] = -180 * y;
   values[73] = 540 * x + 360 * y + 810 * z - 450;
   values[74] = -270 * y;
   values[75] = 270 * y;
   values[76] = 810 - 540 * y - 1620 * z - 810 * x;
   values[77] = 540 * y;
   values[78] = 180 * y;
   values[79] = 510 - 720 * y - 810 * z - 540 * x;
   values[80] = 270 * y;
   values[81] = -180 * y;
   values[82] = 150 - 360 * y - 270 * z - 180 * x;
   values[83] = 90 * y;
   values[84] = 360 * y;
   values[85] = 360 * x + 720 * y + 810 * z - 390;
   values[86] = -270 * y;
   values[87] = 180 * y;
   values[88] = 180 * x + 540 * y + 270 * z - 180;
   values[89] = -90 * y;
   values[90] = 360 * x + 540 * y + 810 * z - 450;
   values[91] = -180 * x;
   values[92] = -270 * x;
   values[93] = 810 - 810 * y - 1620 * z - 540 * x;
   values[94] = 270 * x;
   values[95] = 540 * x;
   values[96] = 510 - 540 * y - 810 * z - 720 * x;
   values[97] = 180 * x;
   values[98] = 270 * x;
   values[99] = 150 - 180 * y - 270 * z - 360 * x;
   values[100] = -180 * x;
   values[101] = 90 * x;
   values[102] = 720 * x + 360 * y + 810 * z - 390;
   values[103] = 360 * x;
   values[104] = -270 * x;
   values[105] = 540 * x + 180 * y + 270 * z - 180;
   values[106] = 180 * x;
   values[107] = -90 * x;
   values[108] = 270 * y;
   values[109] = -90 * x;
   values[110] = 0;
   values[111] = -270 * y;
   values[112] = 90 * x;
   values[113] = 0;
   values[114] = -270 * y;
   values[115] = 90 * x;
   values[116] = 0;
   values[117] = -90 * y;
   values[118] = 270 * x;
   values[119] = 0;
   values[120] = 90 * y;
   values[121] = -270 * x;
   values[122] = 0;
   values[123] = 90 * y;
   values[124] = -270 * x;
   values[125] = 0;
   values[126] = -1080 * y;
   values[127] = 360 * x;
   values[128] = 0;
   values[129] = 360 * y;
   values[130] = -1080 * x;
   values[131] = 0;
   values[132] = 360 * y;
   values[133] = 360 * x;
   values[134] = 0;
}
static void Curl_T_P3_3D_3D_D110(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 - 180 * y - 180 * z - 90 * x;
   values[1] = 180 * x + 90 * y + 90 * z - 60;
   values[2] = 180 * x + 90 * y + 90 * z - 60;
   values[3] = 540 * x + 720 * y + 720 * z - 540;
   values[4] = 180 - 180 * y - 180 * z - 720 * x;
   values[5] = 180 - 180 * y - 180 * z - 720 * x;
   values[6] = 450 - 540 * y - 540 * z - 540 * x;
   values[7] = 540 * x + 90 * y + 90 * z - 120;
   values[8] = 540 * x + 90 * y + 90 * z - 120;
   values[9] = 90 * x + 180 * y + 90 * z - 60;
   values[10] = 120 - 90 * y - 180 * z - 180 * x;
   values[11] = 90 * x + 180 * y + 90 * z - 60;
   values[12] = 180 - 720 * y - 180 * z - 180 * x;
   values[13] = 720 * x + 540 * y + 720 * z - 540;
   values[14] = 180 - 720 * y - 180 * z - 180 * x;
   values[15] = 90 * x + 540 * y + 90 * z - 120;
   values[16] = 450 - 540 * y - 540 * z - 540 * x;
   values[17] = 90 * x + 540 * y + 90 * z - 120;
   values[18] = 90 * z;
   values[19] = 90 * z;
   values[20] = 180 - 270 * y - 180 * z - 270 * x;
   values[21] = -180 * z;
   values[22] = -180 * z;
   values[23] = 540 * x + 540 * y + 720 * z - 420;
   values[24] = 90 * z;
   values[25] = 90 * z;
   values[26] = 240 - 270 * y - 540 * z - 270 * x;
   values[27] = 30 - 90 * x;
   values[28] = 0;
   values[29] = 0;
   values[30] = 180 * x - 360 * y;
   values[31] = 360 * x - 60;
   values[32] = 0;
   values[33] = 360 * y - 90 * x - 30;
   values[34] = 90 * y - 360 * x + 30;
   values[35] = 0;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = -90 * z;
   values[55] = 0;
   values[56] = 15 - 90 * x;
   values[57] = 90 * z;
   values[58] = 0;
   values[59] = 90 * x - 90 * z;
   values[60] = 90 * z;
   values[61] = 180 * z;
   values[62] = 90 * x - 180 * y + 15;
   values[63] = -90 * z;
   values[64] = 0;
   values[65] = 270 * x - 45;
   values[66] = 90 * z;
   values[67] = 0;
   values[68] = 180 * z - 270 * x + 15;
   values[69] = 90 * z;
   values[70] = -90 * z;
   values[71] = 270 * y - 270 * x;
   values[72] = -90 * z;
   values[73] = 180 * z;
   values[74] = 75 - 180 * y - 180 * z - 90 * x;
   values[75] = 90 * z;
   values[76] = -180 * z;
   values[77] = 90 * x + 180 * y + 270 * z - 90;
   values[78] = 90 * z;
   values[79] = -360 * z;
   values[80] = 90 * x + 360 * y + 180 * z - 105;
   values[81] = -90 * z;
   values[82] = -180 * z;
   values[83] = 270 * x + 540 * y + 180 * z - 225;
   values[84] = 90 * z;
   values[85] = 180 * z;
   values[86] = 255 - 540 * y - 360 * z - 270 * x;
   values[87] = 90 * z;
   values[88] = 270 * z;
   values[89] = 270 - 810 * y - 180 * z - 270 * x;
   values[90] = 180 * z;
   values[91] = -90 * z;
   values[92] = 75 - 90 * y - 180 * z - 180 * x;
   values[93] = -180 * z;
   values[94] = 90 * z;
   values[95] = 180 * x + 90 * y + 270 * z - 90;
   values[96] = -360 * z;
   values[97] = 90 * z;
   values[98] = 360 * x + 90 * y + 180 * z - 105;
   values[99] = -180 * z;
   values[100] = -90 * z;
   values[101] = 540 * x + 270 * y + 180 * z - 225;
   values[102] = 180 * z;
   values[103] = 90 * z;
   values[104] = 255 - 270 * y - 360 * z - 540 * x;
   values[105] = 270 * z;
   values[106] = 90 * z;
   values[107] = 270 - 270 * y - 180 * z - 810 * x;
   values[108] = 90 * x + 360 * y + 180 * z - 150;
   values[109] = 150 - 270 * y - 180 * z - 360 * x;
   values[110] = 75 - 180 * y - 90 * z - 180 * x;
   values[111] = 180 - 540 * y - 180 * z - 90 * x;
   values[112] = 540 * x + 540 * y + 270 * z - 270;
   values[113] = 180 * x + 270 * y + 90 * z - 90;
   values[114] = 330 - 720 * y - 360 * z - 270 * x;
   values[115] = 720 * x + 270 * y + 180 * z - 210;
   values[116] = 360 * x + 180 * y + 90 * z - 105;
   values[117] = 150 - 360 * y - 180 * z - 270 * x;
   values[118] = 360 * x + 90 * y + 180 * z - 150;
   values[119] = 75 - 180 * y - 90 * z - 180 * x;
   values[120] = 270 * x + 720 * y + 180 * z - 210;
   values[121] = 330 - 270 * y - 360 * z - 720 * x;
   values[122] = 180 * x + 360 * y + 90 * z - 105;
   values[123] = 540 * x + 540 * y + 270 * z - 270;
   values[124] = 180 - 90 * y - 180 * z - 540 * x;
   values[125] = 270 * x + 180 * y + 90 * z - 90;
   values[126] = -360 * z;
   values[127] = 360 * z;
   values[128] = 360 * x + 360 * y + 360 * z - 180;
   values[129] = 360 * z;
   values[130] = -360 * z;
   values[131] = 360 * x + 360 * y + 360 * z - 180;
   values[132] = 360 * z;
   values[133] = 360 * z;
   values[134] = 540 - 1080 * y - 360 * z - 1080 * x;
}
static void Curl_T_P3_3D_3D_D101(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 120 - 180 * y - 180 * z - 90 * x;
   values[1] = 180 * x + 90 * y + 90 * z - 60;
   values[2] = 180 * x + 90 * y + 90 * z - 60;
   values[3] = 540 * x + 720 * y + 720 * z - 540;
   values[4] = 180 - 180 * y - 180 * z - 720 * x;
   values[5] = 180 - 180 * y - 180 * z - 720 * x;
   values[6] = 450 - 540 * y - 540 * z - 540 * x;
   values[7] = 540 * x + 90 * y + 90 * z - 120;
   values[8] = 540 * x + 90 * y + 90 * z - 120;
   values[9] = 90 * y;
   values[10] = 180 - 180 * y - 270 * z - 270 * x;
   values[11] = 90 * y;
   values[12] = -180 * y;
   values[13] = 540 * x + 720 * y + 540 * z - 420;
   values[14] = -180 * y;
   values[15] = 90 * y;
   values[16] = 240 - 540 * y - 270 * z - 270 * x;
   values[17] = 90 * y;
   values[18] = 90 * x + 90 * y + 180 * z - 60;
   values[19] = 90 * x + 90 * y + 180 * z - 60;
   values[20] = 120 - 180 * y - 90 * z - 180 * x;
   values[21] = 180 - 180 * y - 720 * z - 180 * x;
   values[22] = 180 - 180 * y - 720 * z - 180 * x;
   values[23] = 720 * x + 720 * y + 540 * z - 540;
   values[24] = 90 * x + 90 * y + 540 * z - 120;
   values[25] = 90 * x + 90 * y + 540 * z - 120;
   values[26] = 450 - 540 * y - 540 * z - 540 * x;
   values[27] = 0;
   values[28] = 0;
   values[29] = 0;
   values[30] = 0;
   values[31] = 0;
   values[32] = 0;
   values[33] = 0;
   values[34] = 0;
   values[35] = 0;
   values[36] = 30 - 90 * x;
   values[37] = 0;
   values[38] = 0;
   values[39] = 180 * x - 360 * z;
   values[40] = 0;
   values[41] = 360 * x - 60;
   values[42] = 360 * z - 90 * x - 30;
   values[43] = 0;
   values[44] = 90 * z - 360 * x + 30;
   values[45] = 0;
   values[46] = 0;
   values[47] = 0;
   values[48] = 0;
   values[49] = 0;
   values[50] = 0;
   values[51] = 0;
   values[52] = 0;
   values[53] = 0;
   values[54] = -90 * y;
   values[55] = 270 * x - 45;
   values[56] = 0;
   values[57] = 90 * y;
   values[58] = 270 * z - 270 * x;
   values[59] = -90 * y;
   values[60] = 90 * y;
   values[61] = 180 * y - 270 * x + 15;
   values[62] = 0;
   values[63] = -90 * y;
   values[64] = 15 - 90 * x;
   values[65] = 0;
   values[66] = 90 * y;
   values[67] = 90 * x - 180 * z + 15;
   values[68] = 180 * y;
   values[69] = 90 * y;
   values[70] = 90 * x - 90 * y;
   values[71] = 0;
   values[72] = -90 * y;
   values[73] = 270 * x + 180 * y + 540 * z - 225;
   values[74] = -180 * y;
   values[75] = 90 * y;
   values[76] = 270 - 180 * y - 810 * z - 270 * x;
   values[77] = 270 * y;
   values[78] = 90 * y;
   values[79] = 255 - 360 * y - 540 * z - 270 * x;
   values[80] = 180 * y;
   values[81] = -90 * y;
   values[82] = 75 - 180 * y - 180 * z - 90 * x;
   values[83] = 180 * y;
   values[84] = 90 * y;
   values[85] = 90 * x + 180 * y + 360 * z - 105;
   values[86] = -360 * y;
   values[87] = 90 * y;
   values[88] = 90 * x + 270 * y + 180 * z - 90;
   values[89] = -180 * y;
   values[90] = 90 * x + 180 * y + 360 * z - 150;
   values[91] = 75 - 90 * y - 180 * z - 180 * x;
   values[92] = 150 - 180 * y - 270 * z - 360 * x;
   values[93] = 180 - 180 * y - 540 * z - 90 * x;
   values[94] = 180 * x + 90 * y + 270 * z - 90;
   values[95] = 540 * x + 270 * y + 540 * z - 270;
   values[96] = 330 - 360 * y - 720 * z - 270 * x;
   values[97] = 360 * x + 90 * y + 180 * z - 105;
   values[98] = 720 * x + 180 * y + 270 * z - 210;
   values[99] = 150 - 180 * y - 360 * z - 270 * x;
   values[100] = 75 - 90 * y - 180 * z - 180 * x;
   values[101] = 360 * x + 180 * y + 90 * z - 150;
   values[102] = 270 * x + 180 * y + 720 * z - 210;
   values[103] = 180 * x + 90 * y + 360 * z - 105;
   values[104] = 330 - 360 * y - 270 * z - 720 * x;
   values[105] = 540 * x + 270 * y + 540 * z - 270;
   values[106] = 270 * x + 90 * y + 180 * z - 90;
   values[107] = 180 - 180 * y - 90 * z - 540 * x;
   values[108] = 180 * y;
   values[109] = 75 - 180 * y - 90 * z - 180 * x;
   values[110] = -90 * y;
   values[111] = -180 * y;
   values[112] = 180 * x + 270 * y + 90 * z - 90;
   values[113] = 90 * y;
   values[114] = -360 * y;
   values[115] = 360 * x + 180 * y + 90 * z - 105;
   values[116] = 90 * y;
   values[117] = -180 * y;
   values[118] = 540 * x + 180 * y + 270 * z - 225;
   values[119] = -90 * y;
   values[120] = 180 * y;
   values[121] = 255 - 360 * y - 270 * z - 540 * x;
   values[122] = 90 * y;
   values[123] = 270 * y;
   values[124] = 270 - 180 * y - 270 * z - 810 * x;
   values[125] = 90 * y;
   values[126] = -360 * y;
   values[127] = 360 * x + 360 * y + 360 * z - 180;
   values[128] = 360 * y;
   values[129] = 360 * y;
   values[130] = 540 - 360 * y - 1080 * z - 1080 * x;
   values[131] = 360 * y;
   values[132] = 360 * y;
   values[133] = 360 * x + 360 * y + 360 * z - 180;
   values[134] = -360 * y;
}
static void Curl_T_P3_3D_3D_D011(ELEMENT *Elem, DOUBLE Coord[], DOUBLE RefCoord[], DOUBLE *values)
{
   DOUBLE x, y, z;
   x = RefCoord[0];
   y = RefCoord[1];
   z = RefCoord[2];
   values[0] = 180 - 270 * y - 270 * z - 180 * x;
   values[1] = 90 * x;
   values[2] = 90 * x;
   values[3] = 720 * x + 540 * y + 540 * z - 420;
   values[4] = -180 * x;
   values[5] = -180 * x;
   values[6] = 240 - 270 * y - 270 * z - 540 * x;
   values[7] = 90 * x;
   values[8] = 90 * x;
   values[9] = 90 * x + 180 * y + 90 * z - 60;
   values[10] = 120 - 90 * y - 180 * z - 180 * x;
   values[11] = 90 * x + 180 * y + 90 * z - 60;
   values[12] = 180 - 720 * y - 180 * z - 180 * x;
   values[13] = 720 * x + 540 * y + 720 * z - 540;
   values[14] = 180 - 720 * y - 180 * z - 180 * x;
   values[15] = 90 * x + 540 * y + 90 * z - 120;
   values[16] = 450 - 540 * y - 540 * z - 540 * x;
   values[17] = 90 * x + 540 * y + 90 * z - 120;
   values[18] = 90 * x + 90 * y + 180 * z - 60;
   values[19] = 90 * x + 90 * y + 180 * z - 60;
   values[20] = 120 - 180 * y - 90 * z - 180 * x;
   values[21] = 180 - 180 * y - 720 * z - 180 * x;
   values[22] = 180 - 180 * y - 720 * z - 180 * x;
   values[23] = 720 * x + 720 * y + 540 * z - 540;
   values[24] = 90 * x + 90 * y + 540 * z - 120;
   values[25] = 90 * x + 90 * y + 540 * z - 120;
   values[26] = 450 - 540 * y - 540 * z - 540 * x;
   values[27] = 0;
   values[28] = 0;
   values[29] = 0;
   values[30] = 0;
   values[31] = 0;
   values[32] = 0;
   values[33] = 0;
   values[34] = 0;
   values[35] = 0;
   values[36] = 0;
   values[37] = 0;
   values[38] = 0;
   values[39] = 0;
   values[40] = 0;
   values[41] = 0;
   values[42] = 0;
   values[43] = 0;
   values[44] = 0;
   values[45] = 0;
   values[46] = 30 - 90 * y;
   values[47] = 0;
   values[48] = 0;
   values[49] = 180 * y - 360 * z;
   values[50] = 360 * y - 60;
   values[51] = 0;
   values[52] = 360 * z - 90 * y - 30;
   values[53] = 90 * z - 360 * y + 30;
   values[54] = 15 - 90 * x;
   values[55] = 0;
   values[56] = 0;
   values[57] = 90 * x - 90 * z;
   values[58] = 0;
   values[59] = -90 * x;
   values[60] = 90 * x - 180 * y + 15;
   values[61] = 180 * x;
   values[62] = 0;
   values[63] = 15 - 90 * x;
   values[64] = 0;
   values[65] = 0;
   values[66] = 90 * x - 180 * z + 15;
   values[67] = 0;
   values[68] = 180 * x;
   values[69] = 90 * x - 90 * y;
   values[70] = -90 * x;
   values[71] = 0;
   values[72] = 75 - 180 * y - 180 * z - 90 * x;
   values[73] = 180 * x + 90 * y + 360 * z - 150;
   values[74] = 150 - 360 * y - 270 * z - 180 * x;
   values[75] = 90 * x + 180 * y + 270 * z - 90;
   values[76] = 180 - 90 * y - 540 * z - 180 * x;
   values[77] = 270 * x + 540 * y + 540 * z - 270;
   values[78] = 90 * x + 360 * y + 180 * z - 105;
   values[79] = 330 - 270 * y - 720 * z - 360 * x;
   values[80] = 180 * x + 720 * y + 270 * z - 210;
   values[81] = 75 - 180 * y - 180 * z - 90 * x;
   values[82] = 150 - 270 * y - 360 * z - 180 * x;
   values[83] = 180 * x + 360 * y + 90 * z - 150;
   values[84] = 90 * x + 180 * y + 360 * z - 105;
   values[85] = 180 * x + 270 * y + 720 * z - 210;
   values[86] = 330 - 720 * y - 270 * z - 360 * x;
   values[87] = 90 * x + 270 * y + 180 * z - 90;
   values[88] = 270 * x + 540 * y + 540 * z - 270;
   values[89] = 180 - 540 * y - 90 * z - 180 * x;
   values[90] = 180 * x + 270 * y + 540 * z - 225;
   values[91] = -90 * x;
   values[92] = -180 * x;
   values[93] = 270 - 270 * y - 810 * z - 180 * x;
   values[94] = 90 * x;
   values[95] = 270 * x;
   values[96] = 255 - 270 * y - 540 * z - 360 * x;
   values[97] = 90 * x;
   values[98] = 180 * x;
   values[99] = 75 - 90 * y - 180 * z - 180 * x;
   values[100] = -90 * x;
   values[101] = 180 * x;
   values[102] = 180 * x + 90 * y + 360 * z - 105;
   values[103] = 90 * x;
   values[104] = -360 * x;
   values[105] = 270 * x + 90 * y + 180 * z - 90;
   values[106] = 90 * x;
   values[107] = -180 * x;
   values[108] = 180 * x + 540 * y + 270 * z - 225;
   values[109] = -180 * x;
   values[110] = -90 * x;
   values[111] = 270 - 810 * y - 270 * z - 180 * x;
   values[112] = 270 * x;
   values[113] = 90 * x;
   values[114] = 255 - 540 * y - 270 * z - 360 * x;
   values[115] = 180 * x;
   values[116] = 90 * x;
   values[117] = 75 - 180 * y - 90 * z - 180 * x;
   values[118] = 180 * x;
   values[119] = -90 * x;
   values[120] = 180 * x + 360 * y + 90 * z - 105;
   values[121] = -360 * x;
   values[122] = 90 * x;
   values[123] = 270 * x + 180 * y + 90 * z - 90;
   values[124] = -180 * x;
   values[125] = 90 * x;
   values[126] = 540 - 1080 * y - 1080 * z - 360 * x;
   values[127] = 360 * x;
   values[128] = 360 * x;
   values[129] = 360 * x + 360 * y + 360 * z - 180;
   values[130] = -360 * x;
   values[131] = 360 * x;
   values[132] = 360 * x + 360 * y + 360 * z - 180;
   values[133] = 360 * x;
   values[134] = -360 * x;
}

static void Curl_T_P3_3D_3D_Nodal(ELEMENT *elem, FUNCTIONVEC *fun, INT dim, DOUBLE *values)
{
   // 直接给出积分格式
   INT NumPoints = 16;
   DOUBLE QuadX116_1D[16] = {0.00529953250417497523, 0.02771248846338347782, 0.06718439880608401138, 0.12229779582249850067, 0.19106187779867800369, 0.27099161117138648169, 0.35919822461037048678, 0.45249374508118128668, 0.54750625491881865781, 0.64080177538962956874, 0.72900838882861351831, 0.80893812220132199631, 0.87770220417750155484, 0.93281560119391593311, 0.97228751153661652218, 0.99470046749582508028};
   DOUBLE QuadW116_1D[16] = {0.01357622970587699963, 0.03112676196932405090, 0.04757925584124569895, 0.06231448562776750050, 0.07479799440828900636, 0.08457825969750100426, 0.09130170752246150045, 0.09472530522753450088, 0.09472530522753450088, 0.09130170752246150045, 0.08457825969750100426, 0.07479799440828900636, 0.06231448562776750050, 0.04757925584124569895, 0.03112676196932405090, 0.01357622970587699963};
   INT vert0[6] = {0, 0, 0, 1, 1, 2};
   INT vert1[6] = {1, 2, 3, 2, 3, 3};
   INT i, j, k;
   INT num_fun = dim / 3; // 输入函数的个数(curl元是3D的)
   DOUBLE coord[3];
   DOUBLE vec_line[3]; // 带方向的线(按照单元内部的方向)
   DOUBLE *values_temp = malloc(dim * sizeof(DOUBLE));
   #if NEDELECSCALE == 0
   for (i = 0; i < 6; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         values[k + 3 * i * num_fun] = 0.0;
         values[k + (3 * i + 1) * num_fun] = 0.0;
         values[k + (3 * i + 2) * num_fun] = 0.0;
      }
      vec_line[0] = elem->Vert_X[vert1[i]] - elem->Vert_X[vert0[i]];
      vec_line[1] = elem->Vert_Y[vert1[i]] - elem->Vert_Y[vert0[i]];
      vec_line[2] = elem->Vert_Z[vert1[i]] - elem->Vert_Z[vert0[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1 - QuadX116_1D[j]) * elem->Vert_X[vert0[i]] + QuadX116_1D[j] * elem->Vert_X[vert1[i]];
         coord[1] = (1 - QuadX116_1D[j]) * elem->Vert_Y[vert0[i]] + QuadX116_1D[j] * elem->Vert_Y[vert1[i]];
         coord[2] = (1 - QuadX116_1D[j]) * elem->Vert_Z[vert0[i]] + QuadX116_1D[j] * elem->Vert_Z[vert1[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 线上有三个自由度
         // \int_{e} \vec{u}\cdot \vec{\tau}de = \sum_j \vec{u}(t_j)cdot \vec{\tau}\omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t^2 de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j \X_j
         for (k = 0; k < num_fun; k++)
         {
            values[k + 3 * i * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j];
            values[k + (3 * i + 1) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j];
            values[k + (3 * i + 2) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j];
         }
      }
   }
   #else
   DOUBLE line_length[6];
   for (i = 0; i < 6; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         values[k + 3 * i * num_fun] = 0.0;
         values[k + (3 * i + 1) * num_fun] = 0.0;
         values[k + (3 * i + 2) * num_fun] = 0.0;
      }
      vec_line[0] = elem->Vert_X[vert1[i]] - elem->Vert_X[vert0[i]];
      vec_line[1] = elem->Vert_Y[vert1[i]] - elem->Vert_Y[vert0[i]];
      vec_line[2] = elem->Vert_Z[vert1[i]] - elem->Vert_Z[vert0[i]];
      line_length[i] = sqrt(vec_line[0]*vec_line[0]+vec_line[1]*vec_line[1]+vec_line[2]*vec_line[2]);
#if length_change == 1
      line_length[i] = sqrt(line_length[i]);
#endif
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1 - QuadX116_1D[j]) * elem->Vert_X[vert0[i]] + QuadX116_1D[j] * elem->Vert_X[vert1[i]];
         coord[1] = (1 - QuadX116_1D[j]) * elem->Vert_Y[vert0[i]] + QuadX116_1D[j] * elem->Vert_Y[vert1[i]];
         coord[2] = (1 - QuadX116_1D[j]) * elem->Vert_Z[vert0[i]] + QuadX116_1D[j] * elem->Vert_Z[vert1[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 线上有三个自由度
         // \int_{e} \vec{u}\cdot \vec{\tau}de = \sum_j \vec{u}(t_j)cdot \vec{\tau}\omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j
         // \int_{e} \vec{u}\cdot \vec{\tau} t^2 de = \sum_j \vec{u}(t_j)cdot \vec{\tau} t \omega_j|e| = \sum_j \vec{u}(t_j)cdot vec_line \omega_j \X_j \X_j
         for (k = 0; k < num_fun; k++)
         {
            values[k + 3 * i * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] / line_length[i];
            values[k + (3 * i + 1) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] / line_length[i];
            values[k + (3 * i + 2) * num_fun] += (values_temp[3 * k] * vec_line[0] + values_temp[3 * k + 1] * vec_line[1] + values_temp[3 * k + 2] * vec_line[2]) * QuadW116_1D[j] * QuadX116_1D[j] * QuadX116_1D[j] / line_length[i];
         }
      }
   }
   #endif
   NumPoints = 36;
   DOUBLE QuadX236[36] = {0.0024666971526702431011246,
                          0.0123750604174400428286740,
                          0.0278110821153605899946903,
                          0.0452432465648983234141056,
                          0.0606792682628188653759516,
                          0.0705876315275886651035009,
                          0.0077918747012864289502865,
                          0.0390907007328242447896649,
                          0.0878504549759971942179959,
                          0.1429156829939483008828915,
                          0.1916754372371212433723286,
                          0.2229742632686590730894949,
                          0.0149015633666711548588335,
                          0.0747589734626491059232833,
                          0.1680095191211918581597473,
                          0.2733189621072580344218750,
                          0.3665695077658007727805511,
                          0.4264269178617787203755540,
                          0.0223868729780306273402513,
                          0.1123116817809537010264265,
                          0.2524035680765180367224332,
                          0.4106117416423277211023901,
                          0.5507036279378919596538822,
                          0.6406284367408150437483982,
                          0.0287653330125591175092659,
                          0.1443114869504166508651366,
                          0.3243183045887760296288604,
                          0.5276030957427396694825461,
                          0.7076099133810990204906943,
                          0.8231560673189565191520956,
                          0.0327753666144598859721881,
                          0.1644292415948274965753484,
                          0.3695299243723767501634825,
                          0.6011536484678384750779401,
                          0.8062543312453877009104986,
                          0.9379082062257553253914466};
   DOUBLE QuadY236[36] = {0.0705876315275886651035009,
                          0.0606792682628188653759516,
                          0.0452432465648983164752117,
                          0.0278110821153605830557964,
                          0.0123750604174400410939505,
                          0.0024666971526702413664012,
                          0.2229742632686590730894949,
                          0.1916754372371212433723286,
                          0.1429156829939483008828915,
                          0.0878504549759971942179959,
                          0.0390907007328242517285588,
                          0.0077918747012864220113926,
                          0.4264269178617787203755540,
                          0.3665695077658007727805511,
                          0.2733189621072580344218750,
                          0.1680095191211918304041717,
                          0.0747589734626490920454955,
                          0.0149015633666711444504926,
                          0.6406284367408150437483982,
                          0.5507036279378919596538822,
                          0.4106117416423276655912389,
                          0.2524035680765179812112819,
                          0.1123116817809537426597899,
                          0.0223868729780306585652738,
                          0.8231560673189565191520956,
                          0.7076099133810990204906943,
                          0.5276030957427395584602436,
                          0.3243183045887759741177092,
                          0.1443114869504166231095610,
                          0.0287653330125591244481598,
                          0.9379082062257553253914466,
                          0.8062543312453877009104986,
                          0.6011536484678384750779401,
                          0.3695299243723767501634825,
                          0.1644292415948275243309240,
                          0.0327753666144598998499760};

   DOUBLE QuadW236[36] = {0.0014970851224726396642983,
                          0.0031524435080471811462810,
                          0.0040887731830897217424892,
                          0.0040887731830897217424892,
                          0.0031524435080471811462810,
                          0.0014970851224726396642983,
                          0.0075305964253833514512881,
                          0.0158573346675929711946385,
                          0.0205672344575326543347771,
                          0.0205672344575326543347771,
                          0.0158573346675929711946385,
                          0.0075305964253833514512881,
                          0.0169030715938862825808986,
                          0.0355931519940526144840653,
                          0.0461649273027165268912242,
                          0.0461649273027165268912242,
                          0.0355931519940526144840653,
                          0.0169030715938862825808986,
                          0.0241212128085302492108699,
                          0.0507925431780953395843881,
                          0.0658787978015734215775367,
                          0.0658787978015734215775367,
                          0.0507925431780953395843881,
                          0.0241212128085302492108699,
                          0.0232217495339950702470944,
                          0.0488985245161156767412791,
                          0.0634222231814079995260158,
                          0.0634222231814079995260158,
                          0.0488985245161156767412791,
                          0.0232217495339950702470944,
                          0.0123885307053177209229977,
                          0.0260867886601656891187861,
                          0.0338350113600253410428564,
                          0.0338350113600253410428564,
                          0.0260867886601656891187861,
                          0.0123885307053177209229977};
   INT vert04face[4] = {1, 0, 0, 0};
   INT vert14face[4] = {2, 2, 1, 1};
   INT vert24face[4] = {3, 3, 3, 2};
   DOUBLE tau0[3]; // 面的第一条边的方向
   DOUBLE tau1[3]; // 面的第二条边的方向
   DOUBLE length[3];
   DOUBLE area;
#if NEDELECSCALE == 0
   for (i = 0; i < 4; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         for (j = 0; j < 6; j++)
            values[k + (6 * i + 18 + j) * num_fun] = 0.0;
      }
      // 计算面的第一条边的方向
      tau0[0] = elem->Vert_X[vert14face[i]] - elem->Vert_X[vert04face[i]];
      tau0[1] = elem->Vert_Y[vert14face[i]] - elem->Vert_Y[vert04face[i]];
      tau0[2] = elem->Vert_Z[vert14face[i]] - elem->Vert_Z[vert04face[i]];
      // 计算面的第二条边的方向
      tau1[0] = elem->Vert_X[vert24face[i]] - elem->Vert_X[vert04face[i]];
      tau1[1] = elem->Vert_Y[vert24face[i]] - elem->Vert_Y[vert04face[i]];
      tau1[2] = elem->Vert_Z[vert24face[i]] - elem->Vert_Z[vert04face[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_X[vert04face[i]] + QuadX236[j] * elem->Vert_X[vert14face[i]] + QuadY236[j] * elem->Vert_X[vert24face[i]];
         coord[1] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Y[vert04face[i]] + QuadX236[j] * elem->Vert_Y[vert14face[i]] + QuadY236[j] * elem->Vert_Y[vert24face[i]];
         coord[2] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Z[vert04face[i]] + QuadX236[j] * elem->Vert_Z[vert14face[i]] + QuadY236[j] * elem->Vert_Z[vert24face[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 面上有六个自由度
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j xi
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j xi
         for (k = 0; k < num_fun; k++)
         {
            values[k + (6 * i + 18) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j];
            values[k + (6 * i + 18 + 1) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j];
            values[k + (6 * i + 18 + 2) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j];
            values[k + (6 * i + 18 + 3) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j];
            values[k + (6 * i + 18 + 4) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j];
            values[k + (6 * i + 18 + 5) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j];
         }
      }
   }
#else
   INT line4face1[4] = {3,1,0,0};
   INT line4face2[4] = {4,2,2,1};
   for (i = 0; i < 4; i++)
   {
      for (k = 0; k < num_fun; k++)
      {
         for (j = 0; j < 6; j++)
            values[k + (6 * i + 18 + j) * num_fun] = 0.0;
      }
      // 计算面的第一条边的方向
      tau0[0] = elem->Vert_X[vert14face[i]] - elem->Vert_X[vert04face[i]];
      tau0[1] = elem->Vert_Y[vert14face[i]] - elem->Vert_Y[vert04face[i]];
      tau0[2] = elem->Vert_Z[vert14face[i]] - elem->Vert_Z[vert04face[i]];
      // 计算面的第二条边的方向
      tau1[0] = elem->Vert_X[vert24face[i]] - elem->Vert_X[vert04face[i]];
      tau1[1] = elem->Vert_Y[vert24face[i]] - elem->Vert_Y[vert04face[i]];
      tau1[2] = elem->Vert_Z[vert24face[i]] - elem->Vert_Z[vert04face[i]];
      for (j = 0; j < NumPoints; j++)
      {
         // coord
         coord[0] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_X[vert04face[i]] + QuadX236[j] * elem->Vert_X[vert14face[i]] + QuadY236[j] * elem->Vert_X[vert24face[i]];
         coord[1] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Y[vert04face[i]] + QuadX236[j] * elem->Vert_Y[vert14face[i]] + QuadY236[j] * elem->Vert_Y[vert24face[i]];
         coord[2] = (1.0 - QuadX236[j] - QuadY236[j]) * elem->Vert_Z[vert04face[i]] + QuadX236[j] * elem->Vert_Z[vert14face[i]] + QuadY236[j] * elem->Vert_Z[vert24face[i]];
         // u(t_j)
         fun(coord, dim, values_temp);
         // 面上有六个自由度
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_0} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_0}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_0} \omega_j xi
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1}df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} eta df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j eta  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j eta
         // 1/|f|*\int_{f} \vec{u}\cdot \vec{\tau_1} xi df = \sum_j \vec{u}(t_j)cdot \vec{\tau_1}\omega_j xi  = \sum_j \vec{u}(t_j)cdot \vec{\tau_1} \omega_j xi
         for (k = 0; k < num_fun; k++)
         {
            values[k + (6 * i + 18) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] / line_length[line4face1[i]];
            values[k + (6 * i + 18 + 1) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadY236[j] / line_length[line4face1[i]];
            values[k + (6 * i + 18 + 2) * num_fun] += (values_temp[3 * k] * tau0[0] + values_temp[3 * k + 1] * tau0[1] + values_temp[3 * k + 2] * tau0[2]) * QuadW236[j] * QuadX236[j] / line_length[line4face1[i]];
            values[k + (6 * i + 18 + 3) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] / line_length[line4face2[i]];
            values[k + (6 * i + 18 + 4) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadY236[j] / line_length[line4face2[i]];
            values[k + (6 * i + 18 + 5) * num_fun] += (values_temp[3 * k] * tau1[0] + values_temp[3 * k + 1] * tau1[1] + values_temp[3 * k + 2] * tau1[2]) * QuadW236[j] * QuadX236[j] / line_length[line4face2[i]];
         }
      }
   }
#endif
   // 体上的自由度
   NumPoints = 304;
   DOUBLE QuadX304[304] = {.32967147384406063619375162258031196,
                           1. - 3. * .32967147384406063619375162258031196,
                           .32967147384406063619375162258031196,
                           .32967147384406063619375162258031196,
                           .11204210441737877557977355269085785,
                           1. - 3. * .11204210441737877557977355269085785,
                           .11204210441737877557977355269085785,
                           .11204210441737877557977355269085785,
                           .28044602591109291917326968759374922,
                           1. - 3. * .28044602591109291917326968759374922,
                           .28044602591109291917326968759374922,
                           .28044602591109291917326968759374922,
                           .03942164444076165530186041315194336,
                           1. - 3. * .03942164444076165530186041315194336,
                           .03942164444076165530186041315194336,
                           .03942164444076165530186041315194336,
                           .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .07491741856476755331936887653337858,
                           .07491741856476755331936887653337858,
                           .5 - .07491741856476755331936887653337858,
                           .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .33569310295563458148449388353009909,
                           .33569310295563458148449388353009909,
                           .5 - .33569310295563458148449388353009909,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           .76468706758018040128352585522097679,
                           .76468706758018040128352585522097679,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
                           .04904898759556674851708836681755199,
                           .04904898759556674851708836681755199,
                           .76468706758018040128352585522097679,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           .23282680458942511289537501209852151,
                           .23282680458942511289537501209852151,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
                           .01412609568309253397023383730394044,
                           .01412609568309253397023383730394044,
                           .23282680458942511289537501209852151,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           .28324176830779468643565585504428986,
                           .28324176830779468643565585504428986,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
                           .06239652058154325208658365747436279,
                           .06239652058154325208658365747436279,
                           .28324176830779468643565585504428986,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           .01283187405611823970074593204830391,
                           .01283187405611823970074593204830391,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
                           .18909592756965597717224790600688877,
                           .18909592756965597717224790600688877,
                           .01283187405611823970074593204830391,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           .38727096031949030862955433360477075,
                           .38727096031949030862955433360477075,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
                           .27501760012954439487094433006628105,
                           .27501760012954439487094433006628105,
                           .38727096031949030862955433360477075,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           .03723805935523541954759090988386045,
                           .03723805935523541954759090988386045,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
                           .00594489825256994551853672034278369,
                           .00594489825256994551853672034278369,
                           .03723805935523541954759090988386045,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           .74829410783088587547640692441989412,
                           .74829410783088587547640692441989412,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
                           .11830580710999444802339227394482751,
                           .11830580710999444802339227394482751,
                           .74829410783088587547640692441989412,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
                           .08011846127872502380489538529429122,
                           .08011846127872502380489538529429122,
                           .51463578878883950389557459439302904,
                           .51463578878883950389557459439302904,
                           .39080211141879205490649181473579692,
                           .39080211141879205490649181473579692,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
                           .31025854986272726854690199497600369,
                           .31025854986272726854690199497600369,
                           .16457394683790985618365497081112626,
                           .16457394683790985618365497081112626,
                           .06995093322963368912711775566495230,
                           .06995093322963368912711775566495230,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
                           .10852408019289846676496859793631046,
                           .10852408019289846676496859793631046,
                           .03435867950145695762069918674064249,
                           .03435867950145695762069918674064249,
                           .85571569922057519215400444464572650,
                           .85571569922057519215400444464572650,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
                           .24838249878149546329329939696635675,
                           .24838249878149546329329939696635675,
                           .66253175448505093003700617661466167,
                           .66253175448505093003700617661466167,
                           .01098323448764900444073955046178437,
                           .01098323448764900444073955046178437,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,

                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
                           .39600912110670350629865233312005293,
                           .39600912110670350629865233312005293,
                           .01226898678006518846085017274454891,
                           .01226898678006518846085017274454891,
                           .01878187449597509734542135006798163,
                           .01878187449597509734542135006798163,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
                           .06367516197137305787610880659547363,
                           .06367516197137305787610880659547363,
                           .20546049913241051283959332798606714,
                           .20546049913241051283959332798606714,
                           .13624495088858952521524101996191479,
                           .13624495088858952521524101996191479,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
                           .17576504661391044061489056539266476,
                           .17576504661391044061489056539266476,
                           .46106788607969942837687869968339046,
                           .46106788607969942837687869968339046,
                           .13875357096122530918456923931101452,
                           .13875357096122530918456923931101452,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
                           .47799425320067043636083307018835951,
                           .47799425320067043636083307018835951,
                           .01344788610299629067374252417192062,
                           .01344788610299629067374252417192062,
                           .32213983065389956084144880408640740,
                           .32213983065389956084144880408640740};
   DOUBLE QuadY304[304] = {
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       1. - 3. * .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       1. - 3. * .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       1. - 3. * .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       1. - 3. * .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .76468706758018040128352585522097679,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .23282680458942511289537501209852151,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .28324176830779468643565585504428986,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .01283187405611823970074593204830391,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .38727096031949030862955433360477075,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .03723805935523541954759090988386045,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .74829410783088587547640692441989412,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
   };
   DOUBLE QuadZ304[304] = {
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       .32967147384406063619375162258031196,
       1. - 3. * .32967147384406063619375162258031196,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       .11204210441737877557977355269085785,
       1. - 3. * .11204210441737877557977355269085785,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       .28044602591109291917326968759374922,
       1. - 3. * .28044602591109291917326968759374922,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       .03942164444076165530186041315194336,
       1. - 3. * .03942164444076165530186041315194336,
       .5 - .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .07491741856476755331936887653337858,
       .07491741856476755331936887653337858,
       .5 - .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       .5 - .33569310295563458148449388353009909,
       .33569310295563458148449388353009909,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .76468706758018040128352585522097679,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       1. - 2. * .04904898759556674851708836681755199 - .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       .76468706758018040128352585522097679,
       .04904898759556674851708836681755199,
       .04904898759556674851708836681755199,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .23282680458942511289537501209852151,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       1. - 2. * .01412609568309253397023383730394044 - .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       .23282680458942511289537501209852151,
       .01412609568309253397023383730394044,
       .01412609568309253397023383730394044,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .28324176830779468643565585504428986,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       1. - 2. * .06239652058154325208658365747436279 - .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       .28324176830779468643565585504428986,
       .06239652058154325208658365747436279,
       .06239652058154325208658365747436279,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .01283187405611823970074593204830391,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       1. - 2. * .18909592756965597717224790600688877 - .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       .01283187405611823970074593204830391,
       .18909592756965597717224790600688877,
       .18909592756965597717224790600688877,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .38727096031949030862955433360477075,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       1. - 2. * .27501760012954439487094433006628105 - .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       .38727096031949030862955433360477075,
       .27501760012954439487094433006628105,
       .27501760012954439487094433006628105,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .03723805935523541954759090988386045,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       1. - 2. * .00594489825256994551853672034278369 - .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       .03723805935523541954759090988386045,
       .00594489825256994551853672034278369,
       .00594489825256994551853672034278369,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .74829410783088587547640692441989412,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       1. - 2. * .11830580710999444802339227394482751 - .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       .74829410783088587547640692441989412,
       .11830580710999444802339227394482751,
       .11830580710999444802339227394482751,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .39080211141879205490649181473579692,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       1. - .08011846127872502380489538529429122 - .51463578878883950389557459439302904 - .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .08011846127872502380489538529429122,
       .39080211141879205490649181473579692,
       .51463578878883950389557459439302904,
       .39080211141879205490649181473579692,
       .08011846127872502380489538529429122,
       .51463578878883950389557459439302904,
       .08011846127872502380489538529429122,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .06995093322963368912711775566495230,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       1. - .31025854986272726854690199497600369 - .16457394683790985618365497081112626 - .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .31025854986272726854690199497600369,
       .06995093322963368912711775566495230,
       .16457394683790985618365497081112626,
       .06995093322963368912711775566495230,
       .31025854986272726854690199497600369,
       .16457394683790985618365497081112626,
       .31025854986272726854690199497600369,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .85571569922057519215400444464572650,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       1. - .10852408019289846676496859793631046 - .03435867950145695762069918674064249 - .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .10852408019289846676496859793631046,
       .85571569922057519215400444464572650,
       .03435867950145695762069918674064249,
       .85571569922057519215400444464572650,
       .10852408019289846676496859793631046,
       .03435867950145695762069918674064249,
       .10852408019289846676496859793631046,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .01098323448764900444073955046178437,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       1. - .24838249878149546329329939696635675 - .66253175448505093003700617661466167 - .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .24838249878149546329329939696635675,
       .01098323448764900444073955046178437,
       .66253175448505093003700617661466167,
       .01098323448764900444073955046178437,
       .24838249878149546329329939696635675,
       .66253175448505093003700617661466167,
       .24838249878149546329329939696635675,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01878187449597509734542135006798163,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       1. - .39600912110670350629865233312005293 - .01226898678006518846085017274454891 - .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .39600912110670350629865233312005293,
       .01878187449597509734542135006798163,
       .01226898678006518846085017274454891,
       .01878187449597509734542135006798163,
       .39600912110670350629865233312005293,
       .01226898678006518846085017274454891,
       .39600912110670350629865233312005293,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .13624495088858952521524101996191479,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       1. - .06367516197137305787610880659547363 - .20546049913241051283959332798606714 - .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .06367516197137305787610880659547363,
       .13624495088858952521524101996191479,
       .20546049913241051283959332798606714,
       .13624495088858952521524101996191479,
       .06367516197137305787610880659547363,
       .20546049913241051283959332798606714,
       .06367516197137305787610880659547363,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .13875357096122530918456923931101452,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       1. - .17576504661391044061489056539266476 - .46106788607969942837687869968339046 - .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .17576504661391044061489056539266476,
       .13875357096122530918456923931101452,
       .46106788607969942837687869968339046,
       .13875357096122530918456923931101452,
       .17576504661391044061489056539266476,
       .46106788607969942837687869968339046,
       .17576504661391044061489056539266476,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .32213983065389956084144880408640740,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       1. - .47799425320067043636083307018835951 - .01344788610299629067374252417192062 - .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .47799425320067043636083307018835951,
       .32213983065389956084144880408640740,
       .01344788610299629067374252417192062,
       .32213983065389956084144880408640740,
       .47799425320067043636083307018835951,
       .01344788610299629067374252417192062,
       .47799425320067043636083307018835951,
   };
   DOUBLE QuadW304[304] = {.00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00403487893723887049265039601220684,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .00526342800804762849619159416605960,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .01078639106857763822747522856875491,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00189234002903099684224605393918402,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00601451100300045870650067420901317,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00869219123287302312727334847940610,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00350762328578499275659307009126951,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00118866227331919807192940951748929,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00434522531446666750864079816261451,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00266068657020316603764648612019192,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00555317430212401341478384714866174,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00015314314053958076996931089638380,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00223120413671567928866289542090939,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00247237715624996985149659692307816,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00659612289181792506876909773112724,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00060043490676974216087269159340661,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00224233099687652742449689510982133,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00082444223624718372748546097521409,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00438801583526791034381358666584691,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00345427842574096232228714634129452,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535,
                           .00392928947333557070846156486164535};
   // if (elem->Volumn < 0.0)
   // {
      elem->Jacobian[0][0] = elem->Vert_X[1] - elem->Vert_X[0];
      elem->Jacobian[0][1] = elem->Vert_X[2] - elem->Vert_X[0];
      elem->Jacobian[0][2] = elem->Vert_X[3] - elem->Vert_X[0];
      elem->Jacobian[1][0] = elem->Vert_Y[1] - elem->Vert_Y[0];
      elem->Jacobian[1][1] = elem->Vert_Y[2] - elem->Vert_Y[0];
      elem->Jacobian[1][2] = elem->Vert_Y[3] - elem->Vert_Y[0];
      elem->Jacobian[2][0] = elem->Vert_Z[1] - elem->Vert_Z[0];
      elem->Jacobian[2][1] = elem->Vert_Z[2] - elem->Vert_Z[0];
      elem->Jacobian[2][2] = elem->Vert_Z[3] - elem->Vert_Z[0];
   // }
   for (k = 0; k < num_fun; k++)
   {
      values[k + 42 * num_fun] = 0.0;
      values[k + 43 * num_fun] = 0.0;
      values[k + 44 * num_fun] = 0.0;
   }
   for (j = 0; j < NumPoints; j++)
   {
      coord[0] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_X[0]+QuadX304[j]*elem->Vert_X[1]+QuadY304[j]*elem->Vert_X[2]+QuadZ304[j]*elem->Vert_X[3];
      coord[1] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_Y[0]+QuadX304[j]*elem->Vert_Y[1]+QuadY304[j]*elem->Vert_Y[2]+QuadZ304[j]*elem->Vert_Y[3];
      coord[2] = (1-QuadX304[j]-QuadY304[j]-QuadZ304[j])*elem->Vert_Z[0]+QuadX304[j]*elem->Vert_Z[1]+QuadY304[j]*elem->Vert_Z[2]+QuadZ304[j]*elem->Vert_Z[3];
      // u(t_j)
      fun(coord, dim, values_temp);
      // 面上有三个自由度
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[1;0;0]) dA = \sum_j 1/det(B_K)(B_K[1;0;0]) \omega_j |A|
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[0;1;0]) dA = \sum_j 1/det(B_K)(B_K[0;1;0]) \omega_j |A|
      // \int_{A} \vec{u}\cdot 1/det(B_K)(B_K[0;0;1]) dA = \sum_j 1/det(B_K)(B_K[0;0;1]) \omega_j |A|
#if NEDELECSCALE == 0
      for (k = 0; k < num_fun; k++)
      {
         values[k + 42 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * 1.0 / 6.0;
         values[k + 43 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * 1.0 / 6.0;
         values[k + 44 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * 1.0 / 6.0;
      }
#else
      for (k = 0; k < num_fun; k++)
      {
         values[k + 42 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][0] + values_temp[3 * k + 1] * elem->Jacobian[1][0] + values_temp[3 * k + 2] * elem->Jacobian[2][0]) * QuadW304[j] * 1.0 / 6.0 / line_length[0];
         values[k + 43 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][1] + values_temp[3 * k + 1] * elem->Jacobian[1][1] + values_temp[3 * k + 2] * elem->Jacobian[2][1]) * QuadW304[j] * 1.0 / 6.0 / line_length[1];
         values[k + 44 * num_fun] += (values_temp[3 * k] * elem->Jacobian[0][2] + values_temp[3 * k + 1] * elem->Jacobian[1][2] + values_temp[3 * k + 2] * elem->Jacobian[2][2]) * QuadW304[j] * 1.0 / 6.0 / line_length[2];
      }
#endif
   }
   free(values_temp);
}
#endif
