#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[1], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu1D(double X[1], int dim, double *values)
{
    values[0] = sin(PI * X[0]);
}
void ExactGrad1D(double X[1], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]);
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (1.0 +  PI * PI) * right[0] * sin(PI * coord[0]);
}
void ErrFun1DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun1DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 1.0 * PI * PI * sin(PI * coord[0]);
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]) *
               (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}

INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, DEFAULT_COMM);
    BuildMesh(mesh, "../data/Line01.txt", LINETYPE, LINEMESH);
    // UniformRefineMesh(mesh, 1);
    // MeshPartition(mesh);
    // UniformRefineMesh(mesh, 2);

    // FEMSPACE *femspace = BuildFEMSpace(mesh, C_L_P1_1D, BoundCond); //目前没有给出边界条件
    // QUADRATURE *Quadrature = BuildQuadrature(QuadLine3);
    // MULTIINDEX stiffLmultiindex[2] = {D0, D1};
    // MULTIINDEX stiffRmultiindex[2] = {D0, D1};
    // DISCRETEFORM *StiffDiscreteForm = BuildDiscreteForm(femspace, 2, &stiffLmultiindex[0], femspace, 2,
    //                                                     &stiffRmultiindex[0], stiffmatrix, rhsvec, BoundFun, Quadrature);
    // MATRIX *StiffMatrix = AssembleMatrix(StiffDiscreteForm);
    // //开始调用解法器进行线性方程的求解
    // LINEARSOLVER *solver = NULL;
    // LinearSolverCreate(&solver);
    // LinearSolverSetType(solver, PETSc);
    // LinearSolve(StiffMatrix, solver, CG);
    // FEMFUNCTION *solution = NULL;
    // LinearSolverGetFEMVECSolution(solver, femspace, &solution);
    // LinearSolverDestory(&solver);
    // MULTIINDEX massmultiindex[1] = {D0};
    // DOUBLE H1Error = 0.0, L2Error = 0.0;
    // L2Error = ErrorEstimate(solution, 1, &massmultiindex[0], ExactSolu1D, ErrFun1DL2, Quadrature);
    // MULTIINDEX H1ErrorIndex[1] = {D1};
    // H1Error = ErrorEstimate(solution, 1, &H1ErrorIndex[0], ExactGrad1D, ErrFun1DH1, Quadrature);
    // OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    OpenPFEM_Finalize();
    return 0;
}