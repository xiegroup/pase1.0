#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE AddBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}
void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0];
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]) + right[0] * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]));
}
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 3.0 * PI * PI * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]) + (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2])) * (sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]));
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]) *
               (rhsvalue[0] + femvalue[3] + femvalue[4] + femvalue[5]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}

//考虑方程$-\Delta u + u^3 = f$进行非线性迭代
//这个例子中 将刚度矩阵的线性部分和非线性部分分开计算
//线性部分只进行一次组装 非线性部分在每次迭代时组装
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    BuildMesh(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    UniformRefineMesh(mesh, 1);
    MeshPartition(mesh);
    UniformRefineMesh(mesh, 1);
    FEMSPACE *femspace = BuildFEMSpace(mesh, C_T_P3_3D, BoundCond);
    FEMSPACE *addfemspace = BuildFEMSpace(mesh, C_T_P3_3D, AddBoundCond);
    QUADRATURE *Quadrature = BuildQuadrature(QuadTetrahedral56);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX auxfemfunmultiIndex[1] = {D000};
    MULTIINDEX massmultiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    INT NAuxFEMFunMultiIndex[1] = {1};
    //给定初始值
    DISCRETEFORM *StiffDiscreteForm = BuildDiscreteForm(femspace, 4, &stiffLmultiindex[0], femspace, 4, &stiffRmultiindex[0],
                                                        stiffmatrixlinear, rhsvec, BoundFun, Quadrature);
    MATRIX *StiffMatrixLinear = AssembleMatrix(StiffDiscreteForm);
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver);
    LinearSolverSetType(solver, PETSc);
    LinearSolve(StiffMatrixLinear, solver, CG);
    FEMFUNCTION *solution = NULL;
    LinearSolverGetFEMVECSolution(solver, femspace, &solution);
    FreeDiscreteForm(StiffDiscreteForm);
    //下面实现自洽场不动点迭代
    INT max_outer = 5;
    INT ind_outer;
    DOUBLE H1Error = 0.0, L2Error = 0.0;
    MATRIX *StiffMatrix;
    //生成非线性部分 这里存储的是solution的地址 所以随着solution存储的改变 非线性项也会跟着改变 不需要再次build非线性部分
    AUXFEMFUNCTION *outerauxfemfunc = BuildAuxFEMFunction(1, &solution, &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
    //生成带有非线性的离散变分形式 这里只需要生成矩阵而不需要生成右端项
    StiffDiscreteForm = NULL;
    StiffDiscreteForm = DiscreteFormAuxFeFunBuild(addfemspace, 4, &stiffLmultiindex[0], addfemspace, 4, &stiffLmultiindex[0],
                                                  outerauxfemfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);
    for (ind_outer = 0; ind_outer < max_outer; ind_outer++)
    {
        OpenPFEM_Print("======================== [ outer iteration ] 第 %d 次 ========================\n", ind_outer);
        //组装有限元矩阵中非线性的部分
        StiffMatrix = NULL;
        StiffMatrix = AssembleMatrix(StiffDiscreteForm);
        //删除solution
        FreeFEMFuntion(solution);
        //求解
        // MATRIXaXpbY(1.0, StiffMatrixLinear, 1.0, StiffMatrix);
        // LinearSolve(StiffMatrix, solver, CG);
        AuxLinearSolve(StiffMatrixLinear, 1, &StiffMatrix, solver, CG);
        solution = NULL;
        LinearSolverGetFEMVECSolution(solver, femspace, &solution);
        MatrixDestory(StiffMatrix);
        L2Error = ErrorEstimate(solution, 1, &massmultiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        H1Error = ErrorEstimate(solution, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);
    }
    LinearSolverDestory(&solver);
    FreeDiscreteForm(StiffDiscreteForm);
    MatrixDestory(StiffMatrixLinear);
    OpenPFEM_Finalize();
    return 0;
}