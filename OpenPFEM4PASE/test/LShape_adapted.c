#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
        return DIRICHLET;
    else
        return INNER;
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = right[0];
}
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}
void rhs(DOUBLE *coord, INT dim, DOUBLE *value)
{
    value[0] = 1.0;
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[1] + femvalue[2] + femvalue[3]) *
               (rhsvalue[0] + femvalue[1] + femvalue[2] + femvalue[3]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}

// 考虑方程$-\Delta u = 1$在LShape区域上进行自适应加密
#if 0
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);

    // 积分微分信息
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral304);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle27);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX ErrorIndex[1] = {D000};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};

    // 自适应加密
    INT ind_refine, max_refine = 10;
    DOUBLE TotalError;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        int totalnum, maxnum, minnum;
        MPI_Allreduce(&(mesh->num_volu), &totalnum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&(mesh->num_volu), &maxnum, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
        MPI_Allreduce(&(mesh->num_volu), &minnum, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
        OpenPFEM_Print("总数 %d 最大 %d 最小 %d\n", totalnum, maxnum, minnum);
        // 针对该层网格生成有限元空间和线性部分的离散变分形式
        FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P1_3D, BoundCond);
        DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 4, &stiffLmultiindex[0], femspace, 4, &stiffRmultiindex[0],
                                                            stiffmatrix, rhsvec, BoundFun, Quadrature);

        MATRIX *StiffMatrix = NULL;
        VECTOR *Rhs = NULL, *Solution = NULL;
        MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
        VectorCreateByMatrix(&Solution, StiffMatrix);
        LINEARSOLVER *solver = NULL;
        LinearSolverCreate(&solver, PETSC_KSPCG);
        double start_time = MPI_Wtime();
        LinearSolve(solver, StiffMatrix, Rhs, Solution);
        double end_time = MPI_Wtime();
        OpenPFEM_Print("求解时间: %g sec\n", end_time - start_time);
        FEMFUNCTION *femsol = FEMFunctionBuild(femspace, 1);
        VectorGetFEMFunction(Solution, femsol, 0);
        LinearSolverDestroy(&solver);
        DiscreteFormDestroy(&StiffDiscreteForm);
        MatrixDestroy(&StiffMatrix);
        VectorDestroy(&Rhs);
        VectorDestroy(&Solution);

        DOUBLE L2Error = ErrorEstimate(femsol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        DOUBLE H1Error = ErrorEstimate(femsol, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        OpenPFEM_Print("L2误差为%2.14f  H1误差为%2.14f\n", L2Error, H1Error);
        // 后验误差估计
        DOUBLE *PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        PosterioriErrorEstimate(femsol, 4, 3, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        FEMFunctionDestroy(&femsol);
        FEMSpaceDestroy(&femspace);

        // 网格加密
        if (ind_refine < max_refine - 1)
        {
            OpenPFEM_Print("Start Adaptive refining the mesh!\n");
            MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
            start_time = MPI_Wtime();
            MeshRepartition(mesh, theta, NULL);
            end_time = MPI_Wtime();
            OpenPFEM_Print("重分布时间: %g sec\n", end_time - start_time);
            OpenPFEM_Print("完成自适应加密\n");
        }
        OpenPFEM_Free(PosteriorError);
    }

    MeshDestroy(&mesh);
    QuadratureDestroy(&Quadrature);
    QuadratureDestroy(&QuadratureEdge);
    OpenPFEM_Finalize();
    return 0;
}
#else

//  先看一下什么时候free粗网格还有有限元空间aaa
int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);

    // 积分微分信息
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral304);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle27);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX ErrorIndex[1] = {D000};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};

    // 自适应加密
    INT ind_refine, max_refine = 6;
    DOUBLE TotalError;
    MULTILEVEL *multilevel = NULL;
    MultiLevelCreate(&multilevel, TYPE_PETSC);
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver, MULTILEVEL_LINEAR);
    LinearSolverMultiLevelSetUp(solver, multilevel); 

    // 第一层
    FEMSPACE *space = FEMSpaceBuild(mesh, C_T_P2_3D, BoundCond);
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(space, 4, &stiffLmultiindex[0], space, 4, &stiffRmultiindex[0],
                                                        stiffmatrix, rhsvec, BoundFun, Quadrature);
    MultiLevelAddLevelSimple(multilevel, space);
    MATRIX *StiffMatrix0 = NULL;
    VECTOR *Rhs0 = NULL, *Solution0 = NULL;
    MatrixAssemble(&StiffMatrix0, &Rhs0, StiffDiscreteForm, TYPE_PETSC);
    VectorCreateByMatrix(&Solution0, StiffMatrix0);
    MultiLevelSetMatrix(multilevel, StiffMatrix0, NULL, 0);
    MultiLevelSetVector(multilevel, Rhs0, Solution0, 0);
    DiscreteFormDestroy(&StiffDiscreteForm);

    MESH *finermesh = NULL;
    FEMSPACE *finerspace = NULL;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        // (1) 求解线性方程组
        LinearSolveMultiLevel(solver, multilevel);
        FEMFUNCTION *femsol = FEMFunctionBuild(space, 1);
        VectorGetFEMFunction(multilevel->solution[ind_refine], femsol, 0);
        DOUBLE L2Error = ErrorEstimate(femsol, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        DOUBLE H1Error = ErrorEstimate(femsol, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        OpenPFEM_Print("L2误差为%2.14f  H1误差为%2.14f\n", L2Error, H1Error);
        // (2) 后验误差估计
        DOUBLE *PosteriorError = NULL;
        if (finermesh == NULL)
            PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        else
            PosteriorError = malloc(finermesh->num_volu * sizeof(DOUBLE));
        PosterioriErrorEstimate(femsol, 4, 3, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        // (3) 加密
        finermesh = MeshDuplicate(mesh);
        OpenPFEM_Print("Start Adaptive refining the mesh!\n");
        MeshAdaptiveRefine(finermesh, PosteriorError, 0.6);
        OpenPFEM_Print("完成自适应加密\n");
        OpenPFEM_Free(PosteriorError);
        // (4) 加一层插值矩阵
        finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
        MultiLevelAddLevelSimple(multilevel, finerspace);
        // MatView((Mat)(multilevel->prolongs[ind_refine]->data_petsc), PETSC_VIEWER_STDOUT_WORLD);
        // (5) 重分布
        BRIDGE *bridge;
        MeshRepartition(finermesh, &bridge);
        // // (6) 对旧的插值矩阵进行调整
        ProlongMatrixReorder(&(multilevel->prolongs[ind_refine]), &finerspace, finermesh, bridge);
        multilevel->femspace = finerspace;
        // (7) 组装新的刚度矩阵放进 multilevel
        DISCRETEFORM *DiscreteForm = DiscreteFormBuild(finerspace, 4, &stiffLmultiindex[0], finerspace, 4, &stiffRmultiindex[0],
                                                       stiffmatrix, rhsvec, BoundFun, Quadrature);
        MATRIX *StiffMatrix = NULL;
        VECTOR *Rhs = NULL, *Solution = NULL;
        MatrixAssemble(&StiffMatrix, &Rhs, DiscreteForm, TYPE_PETSC);
        VectorCreateByMatrix(&Solution, StiffMatrix);
        MultiLevelSetMatrix(multilevel, StiffMatrix, NULL, ind_refine + 1);
        MultiLevelSetVector(multilevel, Rhs, Solution, ind_refine + 1);

        DiscreteFormDestroy(&DiscreteForm);
        MeshDestroy(&mesh);
        FEMSpaceDestroy(&space);
        mesh = finermesh;
        space = finerspace;
    }
    LinearSolveMultiLevel(solver, multilevel);
    FEMFUNCTION *solution = FEMFunctionBuild(space, 1);
    VectorGetFEMFunction(multilevel->solution[max_refine], solution, 0);
    DOUBLE L2Error = ErrorEstimate(solution, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    DOUBLE H1Error = ErrorEstimate(solution, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    OpenPFEM_Print("L2误差为%2.14f  H1误差为%2.14f\n", L2Error, H1Error);

    MeshDestroy(&finermesh);
    FEMSpaceDestroy(&space);
    QuadratureDestroy(&Quadrature);
    QuadratureDestroy(&QuadratureEdge);
    // MultiLevelDestroy(&multilevel);
    LinearSolverDestroy(&solver);
    OpenPFEM_Finalize();
    return 0;
}
#endif