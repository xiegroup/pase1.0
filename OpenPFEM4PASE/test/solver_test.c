#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    DOUBLE starttime = MPI_Wtime();
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 1);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    DISCRETEFORM *StiffDiscreteForm2 = NULL, *MassDiscreteForm2 = NULL;
    INT level_num = 2, i;
    MATRIX **stiffmatrices = (MATRIX **)malloc(level_num * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(level_num * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((level_num - 1) * sizeof(MATRIX *));
    for (i = 0; i < level_num; i++)
    {
        if (i == 0) // 初始层
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P5_3D, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, C_T_P3_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            // MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            // MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else // 每次添加层
        {
            // 加密一次
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, MassBoundCond);
            StiffDiscreteForm2 = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                   stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm2 = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                  massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm2, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm2, TYPE_OPENPFEM);
            // MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            // MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            // ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            // destory
            // MeshDestroy(&coarsemesh);
            // FEMSpaceDestroy(&coarsespace);
            // copy
            // coarsemesh = finermesh;
            // coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    // MatrixDeleteDirichletBoundary(stiffmatrices[0], StiffDiscreteForm);
    // MatrixDeleteDirichletBoundary(stiffmatrices[1], StiffDiscreteForm2);
    // VECTOR *vector1 = NULL;
    // VectorCreateByMatrix(&vector1, massmatrices[0]);
    // DOUBLE *array1 = NULL;
    // VectorGetArray(vector1, &array1);
    // for (i = 0; i < vector1->local_length; i++)
    // {
    //     array1[i] = (rand()) / RAND_MAX + 1.0;
    // }
    // for (i = 0; i < coarsespace->dirichletbdnum; i++)
    // {
    //     array1[coarsespace->dirichletbdindex[i]] = 0.0;
    // }
    // VECTOR *result1 = NULL;
    // VectorCreateByMatrix(&result1, massmatrices[1]);
    // MatrixVectorMult(prolongs[0], vector1, result1);

    // ProlongDeleteDirichletBoundary(prolongs[0], coarsespace, finerspace);
    // VECTOR *vector2 = NULL;
    // VectorCreateByMatrix(&vector2, stiffmatrices[0]);
    // DOUBLE *array2 = NULL;
    // VectorGetArray(vector2, &array2);
    // INT ind = 0;
    // for (i = 0; i < vector1->local_length; i++)
    // {
    //     if (array1[i] != 0.0)
    //     {
    //         array2[ind] = array1[i];
    //         ind++;
    //     }
    // }
    // VECTOR *result2 = NULL;
    // VectorCreateByMatrix(&result2, stiffmatrices[1]);
    // MatrixVectorMult(prolongs[0], vector2, result2);

    // VectorGetArray(result1, &array1);
    // VectorGetArray(result2, &array2);
    // ind = 0;
    // for (i = 0; i < finerspace->dirichletbdnum; i++)
    // {
    //     if (array1[finerspace->dirichletbdindex[i]] != 0.0)
    //         printf("array1[finerspace->dirichletbdindex[i]] = %g\n", array1[finerspace->dirichletbdindex[i]]);
    // }
    // DOUBLE max11 = 0.0;
    // for (i = 0; i < result1->local_length; i++)
    // {
    //     if (array1[i] != 0.0)
    //     {
    //         if (fabs(array1[i] - array2[ind]) > max11)
    //             max11 = fabs(array1[i] - array2[ind]);
    //         ind++;
    //     }
    // }
    // printf("max = %g\n", max11);

    // VecAXPBY((Vec)result2->data_petsc, 1.0, -1.0, (Vec)result1->data_petsc);
    // DOUBLE error;
    // VecNorm((Vec)result2->data_petsc, NORM_INFINITY, &error);
    // OpenPFEM_Print("error : %g\n", error);

    // MatrixPrint(massmatrices[0], 0);

    // MatrixConvert(stiffmatrices[2], TYPE_PETSC, 0);
    // MatrixConvert(massmatrices[2], TYPE_PETSC, 0);
    // MatrixConvert(prolongs[0], TYPE_PETSC, 0);
    // MatView((Mat)massmatrices[0]->data_petsc, PETSC_VIEWER_STDOUT_WORLD);

    // EIGENSOLVER *solver = NULL;
    // EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    // INT nev = 10;
    // VECTORS *evec = NULL;
    // VectorsCreateByMatrix(&evec, stiffmatrices[2], nev);
    // DOUBLE *eval = (DOUBLE *)malloc(nev * sizeof(DOUBLE));
    // EigenSolverSolve(solver, stiffmatrices[2], massmatrices[2], eval, evec, nev);
    // for (i = 0; i < nev; i++)
    // {
    //     OpenPFEM_Print("eval[%d]=%g\n", i, eval[i]);
    // }

    for (i = 0; i < level_num; i++)
    {
        MatrixDestroy(stiffmatrices + i);
        MatrixDestroy(massmatrices + i);
    }
    for (i = 0; i < level_num - 1; i++)
    {
        MatrixDestroy(prolongs + i);
    }

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);
    OpenPFEM_Finalize();
    return 0;
}