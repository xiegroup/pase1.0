// 本程序是为了检测基函数的设置是否正确
#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"
#include "math.h"
// 下面的这个全局参数是为了来确定真解的正则性
#define alpha 0.166
#define tau 1.0e-14

DOUBLE C = 2.0;

// 下面是具体问题的设置
BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
// exact solution : u = (x^2+y^2+z^2)^(\alpha/2)
// u = x^4+y^4+z^4;
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    values[1] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    values[2] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    // values[0] = C;
    // values[1] = C;
    // values[2] = C;
    // values[0] = C;
    // values[1] = C;
    // values[2] = 0;
    // // values[0] = X[1];
    // // values[1] = -X[0];
    // // values[2] = 1;
    values[0] = X[0] + X[1] + X[2];
    values[1] = X[0] + X[1] + X[2];
    values[2] = X[0] + X[1] + X[2];
    // // values[0] = X[0];
    // // values[1] = 0;
    // // values[2] = 0;
}
void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    values[1] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    values[2] = X[0] * X[0] * X[0] * X[0] + X[1] * X[1] * X[1] * X[1] + X[2] * X[2] * X[2] * X[2];
    // values[0] = C;
    // values[1] = C;
    // values[2] = C;
    // values[0] = C;
    // values[1] = C;
    // values[2] = 0;
    // // values[0] = X[1];
    // // values[1] = -X[0];
    // // values[2] = 1;
    values[0] = X[0] + X[1] + X[2];
    values[1] = X[0] + X[1] + X[2];
    values[2] = X[0] + X[1] + X[2];
    // // values[0] = X[0];
    // // values[1] = 0;
    // // values[2] = 0;
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = 4.0 * X[0] * X[0] * X[0];
    values[1] = 4.0 * X[0] * X[0] * X[0];
    values[2] = 4.0 * X[0] * X[0] * X[0];
    values[3] = 4.0 * X[1] * X[1] * X[1];
    values[4] = 4.0 * X[1] * X[1] * X[1];
    values[5] = 4.0 * X[1] * X[1] * X[1];
    values[6] = 4.0 * X[2] * X[2] * X[2];
    values[7] = 4.0 * X[2] * X[2] * X[2];
    values[8] = 4.0 * X[2] * X[2] * X[2];
    INT i;
    for (i = 0; i < 9; i++)
    //     // values[i] = 0;
        values[i] = 1;
    // // values[1] = -1;
    // // values[3] = 1;
    // values[0] = 1;
}

void ExactHess3D(double X[3], int dim, double *values)
{
    values[0] = 12.0 * X[0] * X[0];
    values[1] = 12.0 * X[0] * X[0];
    values[2] = 12.0 * X[0] * X[0];
    values[3] = 12.0 * X[1] * X[1];
    values[4] = 12.0 * X[1] * X[1];
    values[5] = 12.0 * X[1] * X[1];
    values[6] = 12.0 * X[2] * X[2];
    values[7] = 12.0 * X[2] * X[2];
    values[8] = 12.0 * X[2] * X[2];
    values[9] = 0.0;
    values[10] = 0.0;
    values[11] = 0.0;
    values[12] = 0.0;
    values[13] = 0.0;
    values[14] = 0.0;
    values[15] = 0.0;
    values[16] = 0.0;
    values[17] = 0.0;
    INT i;
    for (i = 0; i < 18; i++)
        values[i] = 0.0;
}
// void rhs(DOUBLE *X, INT dim, DOUBLE *value)
// {
//     value[0] = -12.0 * (X[0] * X[0] + X[1] * X[1] + X[2] * X[2]);
// }
//============================================
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 3; i++)
    {
        // value[0] += (femvale[3+i] - Funvalue[i]) * (femvale[3+i] - Funvalue[i]);
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
    }
}
void ErrFun3DHcurl(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    DOUBLE u, v, w;
    u = (femvale[5]-femvale[7]) - (Funvalue[5]-Funvalue[7]);
    v = (femvale[6]-femvale[2]) - (Funvalue[6]-Funvalue[2]);
    w = (femvale[1]-femvale[3]) - (Funvalue[1]-Funvalue[3]);
    value[0] += u*u+v*v+w*w;
}
void ErrFun3DH2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = 0.0;
    for (INT i = 0; i < 18; i++)
        value[0] += (femvale[i] - Funvalue[i]) * (femvale[i] - Funvalue[i]);
}
// 考虑方程$-\Delta u = f$进行非线性迭代
// 这个例子中 将刚度矩阵的线性部分和非线性部分分开计算
// 线性部分只进行一次组装 非线性部分在每次迭代时组装
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube6.txt", SIMPLEX, TETHEDRAL);
    // BuildMesh(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);
    MeshSetAsAncestor(mesh);
    MESH *meshH = MeshDuplicate(mesh);
    // MeshUniformRefine(mesh, 1);
    // 积分微分信息
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral304);
    // MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX L2multiindex[2] = {D000, D000};
    MULTIINDEX HcurlErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX H2ErrorIndex[6] = {D200, D020, D002, D110, D101, D011};
    // 自适应加密
    INT ind_refine, ind, NUMVOLU;
    INT max_refine = 2;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *L2ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *HcurlErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H2ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *L2ErrorOfinter = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *HcurlErrorOfinter = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H2ErrorOfinter = malloc(max_refine * sizeof(DOUBLE));
    FEMSPACE *femspace = NULL;
    FEMSPACE *femspaceH = FEMSpaceBuild(meshH, Curl_T_P2_3D_3D, BoundCond);
    DOUBLE *PosteriorError = NULL, TotalError;
    DOUBLE time1, time2;
    VECTOR *vec_H;
    VECTOR *vec_h;
    FEMFUNCTION *solution;
    FEMFUNCTION *solution_H;
    MATRIX *Prolong = NULL;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        // 计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;
        // 针对该层网格生成有限元空间
        femspace = FEMSpaceBuild(mesh, Curl_T_P2_3D_3D, BoundCond);
        // 进行有限元插值
        solution = FEMFunctionBuild(femspace, 1);
        time1 = GetTime();
        FEMInterpolation(solution, ExactSolu3D, 1); // 有限元插值
        time2 = GetTime();
        OpenPFEM_Print("有限元插值的时间为%2.10f\n", time2 - time1);
        L2ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 2, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        HcurlErrorOfBisection[ind_refine] = ErrorEstimate(solution, 3, &HcurlErrorIndex[0], ExactGrad3D, ErrFun3DHcurl, Quadrature);
        H2ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 6, &H2ErrorIndex[0], ExactHess3D, ErrFun3DH2, Quadrature);
        OpenPFEM_Print("L2误差为 %2.14f  Hcurl误差为 %2.14f  H2误差为 %2.14f\n", L2ErrorOfBisection[ind_refine], HcurlErrorOfBisection[ind_refine],
                       H2ErrorOfBisection[ind_refine]);
        // return 0;
        // 输出每次自适应加密的结果
        OpenPFEM_Print("单元个数 N = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %d ", NumElemOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        OpenPFEM_Print("L2Err = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", L2ErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        OpenPFEM_Print("HcurlErr = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", HcurlErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        OpenPFEM_Print("H2Err = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", H2ErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");
        // 将结果插值到最粗网格上
        ProlongMatrixAssemble(&Prolong, femspaceH, femspace, TYPE_OPENPFEM);
        // MatrixPrint(Prolong, 0);
        // MPI_Barrier(MPI_COMM_WORLD);
        // MatrixPrint(Prolong, 1);
        // MPI_Barrier(MPI_COMM_WORLD);
        // return 0;
        vec_h = NULL;
        VectorCreateByMatrix(&vec_h, Prolong);
        // printf("prolong size local %d*%d global %d*%d\n", Prolong->local_nrows, Prolong->local_ncols, Prolong->global_nrows, Prolong->global_ncols);
        // printf("vec_h size local %d global %d\n", vec_h->local_length, vec_h->global_length);
        // printf("solution size local %d global %d\n", solution->LocalNumDOF, solution->GlobalNumDOF);
        FEMFunctionGetVector(solution, vec_h);
        // VectorPrint(vec_h, 0);
        // printf("end fun!!!!\n");
        vec_H = NULL;
        // VectorPrint(vec_h, 0);
        // MPI_Barrier(MPI_COMM_WORLD);
        // VectorPrint(vec_h, 1);
        // MPI_Barrier(MPI_COMM_WORLD);
        VectorCreateByMatrixTranspose(&vec_H, Prolong);
        MatrixTransposeVectorMult(Prolong, vec_h, vec_H);
        // VectorPrint(vec_H, 0);
        // VectorAxpby(-1.0, vec_H, 1.0, vec_h);
        // VectorPrint(vec_h, 0);
        // MPI_Barrier(MPI_COMM_WORLD);
        // VectorPrint(vec_h, 1);
        // MPI_Barrier(MPI_COMM_WORLD);
        // VectorPrint(vec_H, 0);
        // MPI_Barrier(MPI_COMM_WORLD);
        // VectorPrint(vec_H, 1);
        // MPI_Barrier(MPI_COMM_WORLD);
        // return 0;
        solution_H = FEMFunctionBuild(femspaceH, 1);
        VectorGetFEMFunction(vec_H, solution_H, 0);
        // FEMFunctionPrint(solution_H);
        // L2ErrorOfinter[ind_refine] = ErrorEstimate(solution_H, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        // HcurlErrorOfinter[ind_refine] = ErrorEstimate(solution_H, 3, &HcurlErrorIndex[0], ExactGrad3D, ErrFun3DHcurl, Quadrature);
        // H2ErrorOfinter[ind_refine] = ErrorEstimate(solution_H, 6, &H2ErrorIndex[0], ExactHess3D, ErrFun3DH2, Quadrature);
        // OpenPFEM_Print("将细网格解插值到粗网格之后的误差结果\n");
        // OpenPFEM_Print("L2误差为 %2.14f  Hcurl误差为 %2.14f  H2误差为 %2.14f\n", L2ErrorOfinter[ind_refine], HcurlErrorOfinter[ind_refine],
        //                H2ErrorOfinter[ind_refine]);
        VectorDestroy(&vec_H);
        VectorDestroy(&vec_h);
        FEMFunctionDestroy(&solution);
        FEMFunctionDestroy(&solution_H);
        vec_H = NULL;
        vec_h = NULL;
        solution = NULL;
        solution_H = NULL;
        solution_H = FEMFunctionBuild(femspaceH, 1);
        time1 = GetTime();
        FEMInterpolation(solution_H, ExactSolu3D, 1); // 有限元插值
        time2 = GetTime();
        OpenPFEM_Print("粗网格有限元插值的时间为%2.10f\n", time2 - time1);
        VectorCreateByMatrixTranspose(&vec_H, Prolong);
        FEMFunctionGetVector(solution_H, vec_H);
        VectorCreateByMatrix(&vec_h, Prolong);
        MatrixVectorMult(Prolong, vec_H, vec_h);
        solution = FEMFunctionBuild(femspace, 1);
        VectorGetFEMFunction(vec_h, solution, 0);
        // VectorGetFEMFunction(vec_H, solution);
        L2ErrorOfinter[ind_refine] = ErrorEstimate(solution, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        HcurlErrorOfinter[ind_refine] = ErrorEstimate(solution, 3, &HcurlErrorIndex[0], ExactGrad3D, ErrFun3DHcurl, Quadrature);
        H2ErrorOfinter[ind_refine] = ErrorEstimate(solution, 6, &H2ErrorIndex[0], ExactHess3D, ErrFun3DH2, Quadrature);
        OpenPFEM_Print("将粗网格解插值到细网格之后的误差结果\n");
        OpenPFEM_Print("L2误差为 %2.14f  Hcurl误差为 %2.14f  H2误差为 %2.14f\n", L2ErrorOfinter[ind_refine], HcurlErrorOfinter[ind_refine],
                       H2ErrorOfinter[ind_refine]);
        VectorDestroy(&vec_H);
        VectorDestroy(&vec_h);
        FEMFunctionDestroy(&solution);
        FEMFunctionDestroy(&solution_H);
        MatrixDestroy(&Prolong);

        if (ind_refine < max_refine - 1)
        // if (ind_refine < 1)
        {
            // 一致加密
            MeshUniformRefine(mesh, 1);
        }
    }

    OpenPFEM_Finalize();
    return 0;
}