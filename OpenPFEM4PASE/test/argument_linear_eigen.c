#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"
#include "multilevelmesh.h"
#include "matvec_ops_petsc.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE AddBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}
void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}
void rhs_fine(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * right[0];
}
void PostErrFunElem(DOUBLE lambda, DOUBLE *eigfun, DOUBLE *value)
{
    value[0] = (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3]) *
               (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}

//考虑线性特征值问题$-\Delta u = \lambda u$
//采用扩展子空间的迭代方法求解
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    BuildMesh(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    UniformRefineMesh(mesh, 1);
    MeshPartition(mesh);
    UniformRefineMesh(mesh, 1);
    // 粗网格
    MESH *meshH = MeshDuplicate(mesh);
    // 细网格
    UniformRefineMesh(mesh, 1);

    // 积分微分信息
    QUADRATURE *Quadrature = BuildQuadrature(QuadTetrahedral56);
    QUADRATURE *QuadratureEdge = BuildQuadrature(QuadTriangle13);
    // 刚度矩阵只用到了梯度的信息
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};
    // 质量矩阵只用到了原函数的信息
    MULTIINDEX massLmultiindex[1] = {D000};
    MULTIINDEX massRmultiindex[1] = {D000};
    // 在做校正步时 把解带入到右端时需要的信息
    INT NAuxFEMFunMultiIndex[1] = {1};
    MULTIINDEX auxfemfunmultiIndex[1] = {D000};
    // 计算误差 需要的积分信息
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    // 计算内积 需要的积分信息
    MULTIINDEX ErrorIndex[1] = {D000};

    INT num_eig = 3; //扩展子空间求解时增加的特征对个数 (如果只针对第k个特征对求解 前面的特征对需要添加？)
    INT aim_eig = 0; //自适应加密是针对的特征对
    INT ind_eig;

    // 自适应加密
    INT i;
    // 整体的自适应加密次数
    INT ind_refine;
    INT max_refine = 6;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *L2ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1ErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    // 对非线性问题做SCF的最大次数
    // INT ind_SCF;
    // INT max_SCF = 30;
    // INT conv_ind = 0;
    // DOUBLE TolValue = 1e-12; //判断特征值是否收敛
    // DOUBLE TolVec = 1e-10;   //判断特征向量是否收敛
    // DOUBLE Error1 = 0.0;
    // DOUBLE Error2 = 0.0;

    INT NUMVOLU;
    DOUBLE TotalError;
    DOUBLE H1Error = 0.0, L2Error = 0.0;

    MVOPS *MVOps = NULL;
    MVOPSCreate(&MVOps, PETSc); //支持两个求解器用同一种类型转换

    // 声明需要用到的变量
    FEMSPACE *femspaceH = NULL; // 粗 有限元空间
    FEMSPACE *massfemspaceH = NULL;
    FEMSPACE *femspaceh = NULL; // 细 有限元空间
    FEMSPACE *massfemspaceh = NULL;
    FEMSPACE *femspacecoarse = NULL; // 细有限元空间上一层的有限元空间

    DISCRETEFORM *StiffDiscreteFormH = NULL;
    DISCRETEFORM *MassDiscreteFormH = NULL;
    DISCRETEFORM *StiffDiscreteFormh = NULL;
    DISCRETEFORM *MassDiscreteFormh = NULL;

    MATRIX *StiffMatrixH = NULL; // A_H
    MATRIX *MassMatrixH = NULL;  // B_H
    MATRIX *StiffMatrixh = NULL; // A_h
    MATRIX *MassMatrixh = NULL;  // B_h

    EIGENSOLVER *solver_eigen = NULL;   //特征值求解器
    LINEARSOLVER *solver_linear = NULL; //边值问题求解器

    MATRIX *Prolong = NULL;
    void **Prolongs = malloc(max_refine * sizeof(void *));
    void **Restricts = malloc(max_refine * sizeof(void *));

    DOUBLE *eigval;
    void **eigvec = NULL; //从求解器中得到的一些列特征向量
    void *eig_vec_petsc;           // 临时存储特征向量or解向量(petsc)
    FEMVEC *eig_vec_femvec = NULL; // 临时存储特征向量or解向量(femvec)

    AUXFEMFUNCTION *rhs_upper;     //校正步非线性部分
    DISCRETEFORM *rhsDiscreteForm; //校正步变分形式

    // 存储扩展子空间用到的矩阵
    void *Ahuh = NULL;                                  // A_h * u_h
    void *Bhuh = NULL;                                  // B_h * u_h
    void **b_Hh = malloc(num_eig * sizeof(void *));     // b_Hh
    void **c_Hh = malloc(num_eig * sizeof(void *));     // c_Hh
    DOUBLE **beta = malloc(num_eig * sizeof(DOUBLE *)); // beta
    for (i = 0; i < num_eig; i++)
    {
        beta[i] = malloc(num_eig * sizeof(DOUBLE *));
    }
    DOUBLE **gamma = malloc(num_eig * sizeof(DOUBLE *)); // gamma
    for (i = 0; i < num_eig; i++)
    {
        gamma[i] = malloc(num_eig * sizeof(DOUBLE *));
    }

    void *A_Hh = NULL; // [A_H b_Hh; b_Hh' beta]
    void *B_Hh = NULL; // [B_H c_Hh; c_Hh' gamma]

    DOUBLE *PosteriorError = NULL;

    int m, n, length;

    // 1. 在粗空间上计算特征值问题
    femspaceH = BuildFEMSpace(meshH, C_T_P2_3D, BoundCond);
    massfemspaceH = BuildFEMSpace(meshH, C_T_P2_3D, MassBoundCond);
    StiffDiscreteFormH = BuildDiscreteForm(femspaceH, 4, &stiffLmultiindex[0], femspaceH, 4, &stiffRmultiindex[0],
                                           stiffmatrix, NULL, BoundFun, Quadrature);
    MassDiscreteFormH = BuildDiscreteForm(massfemspaceH, 1, &massLmultiindex[0], massfemspaceH, 1, &massRmultiindex[0],
                                          massmatrix, NULL, BoundFun, Quadrature);
    StiffMatrixH = AssembleMatrix(StiffDiscreteFormH); // A_H
    MassMatrixH = AssembleMatrix(MassDiscreteFormH);   // B_H
    FreeFEMSpace(massfemspaceH);
    // 转换成petsc格式 删掉MATRIX格式
    MatDataConvert(StiffMatrixH, 1, MVOps);
    MatDataConvert(MassMatrixH, 1, MVOps);
    // 求解
    solver_eigen = NULL;
    EigenSolverCreate(&solver_eigen);
    EigenSolverSetType(solver_eigen, SLEPc);
    EigenSolverSetNev(solver_eigen, num_eig);                         // 求解num_eig个特征对
    EigenSolve(StiffMatrixH, MassMatrixH, solver_eigen, KrylovSchur); //这里 A_H B_H 格式转换
    // 取出petsc格式的一系列特征向量
    eigval = NULL;
    eigvec = NULL;
    EigenSolverGetOriginalEigenpairs(solver_eigen, &eigval, &eigvec);
    solver_eigen->Eval = NULL;
    solver_eigen->Evec = NULL;
    EigenSolverDestory(&solver_eigen);

    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        //计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;

        // 将上层的解插值到新有限元空间
        // 1）更新有限元空间
        if (ind_refine == 0)
        {
            femspacecoarse = femspaceH;
        }
        else
        {
            femspacecoarse = femspaceh;
        }
        femspaceh = BuildFEMSpace(mesh, C_T_P2_3D, BoundCond);
        // 2）生成插值矩阵并进行格式转化
        Prolong = BuildProlongMatrix(femspacecoarse, femspaceh);
        FreeFEMSpace(femspacecoarse);
        MATRIX2Mat_PETSc(Prolong, &(Prolongs[ind_refine])); //转换格式并存储
        MatrixDestory(Prolong);
        Prolong = NULL;
        // 对特征对进行循环
        LinearSolverCreate(&solver_linear);
        LinearSolverSetType(solver_linear, PETSc);
        // 3）插值
        for (ind_eig = 0; ind_eig < num_eig; ind_eig++)
        {
            VecCreateByMat_PETSc(&eig_vec_petsc, Prolongs[ind_refine], MVOps);
            MatVecMult_PETSc(Prolongs[ind_refine], eigvec[ind_eig], eig_vec_petsc, MVOps);
            // 将插值后的解储存到原eigvec的位置
            eigvec[ind_eig] = eig_vec_petsc;
        }

        // 2. 在更细层的网格求解边值问题
        for (ind_eig = 0; ind_eig < num_eig; ind_eig++)
        {
            // 将解转化为FEMVEC的格式 并生成非线性的部分
            Vec2FEMVEC_PETSc(femspaceh, eigvec[ind_eig], &eig_vec_femvec);
            if (ind_eig > 0)
            {
                VecDestory_PETSc(&(solver_linear->solution), MVOps); // free上一个特征向量
            }
            // solver_linear->solution = eigvec[ind_eig]; // 用粗空间的特征向量做初值
            eigvec[ind_eig] = NULL;

            // \lambda * u
            for (i = 0; i < eig_vec_femvec->LocalNumDOF; i++)
            {
                eig_vec_femvec->Values[i] = eig_vec_femvec->Values[i] * eigval[ind_eig];
            }
            rhs_upper = BuildAuxFEMFunction(1, &eig_vec_femvec, &NAuxFEMFunMultiIndex[0], &auxfemfunmultiIndex[0]);
            rhsDiscreteForm = DiscreteFormAuxFeFunBuild(femspaceh, 4, &stiffLmultiindex[0], femspaceh, 4, &stiffLmultiindex[0],
                                                        rhs_upper, stiffmatrix, rhs_fine, BoundFun, Quadrature);
            // 这里也可以写成细网格上的向量乘矩阵的形式 Rhs = \lambda C^T I_h^H A_h
            // 求解 校正步线性方程组
            if (ind_eig > 0)
                MatrixDestory(StiffMatrixh);
            StiffMatrixh = AssembleMatrix(rhsDiscreteForm); // A_h MATRIX + 新组装的右端项 多次组装A_h!
            FreeFEMFuntion(eig_vec_femvec);
            eig_vec_femvec = NULL;
            LinearSolve(StiffMatrixh, solver_linear, CG);
            LinearSolverGetOriginalSolution(solver_linear, &(eigvec[ind_eig])); // u_h vec
            solver_linear->solution = NULL;
        }
        LinearSolverDestory(&solver_linear);
        MatDataConvert(StiffMatrixh, 1, MVOps);
        // 3. 在{H,h}空间计算特征值问题
        massfemspaceh = BuildFEMSpace(mesh, C_T_P2_3D, MassBoundCond);
        MassDiscreteFormh = BuildDiscreteForm(massfemspaceh, 1, &massLmultiindex[0], massfemspaceh, 1, &massRmultiindex[0],
                                              massmatrix, NULL, BoundFun, Quadrature);
        MassMatrixh = AssembleMatrix(MassDiscreteFormh); // B_h MATRIX
        MatDataConvert(MassMatrixh, 1, MVOps);

        for (ind_eig = 0; ind_eig < num_eig; ind_eig++)
        {
            // 计算 A_hh * u_h
            VecCreateByMat_PETSc(&(Ahuh), StiffMatrixh->data_solver, MVOps);
            // MatGetSize((Mat)(StiffMatrixh->data_solver), &m, &n);
            // VecGetSize((Vec)(eigvec[ind_eig]), &length);
            // printf("Global  Ah (%d,%d) u_h (%d)\n", m, n, length);
            // MatGetLocalSize((Mat)(StiffMatrixh->data_solver), &m, &n);
            // VecGetLocalSize((Vec)(eigvec[ind_eig]), &length);
            // printf("Local  Ah (%d,%d) u_h (%d)\n", m, n, length);
            MatVecMult_PETSc(StiffMatrixh->data_solver, eigvec[ind_eig], Ahuh, MVOps); //多个这里需要替换成矩阵乘矩阵
            // b_Hh = I_h^H * Ahuh
            VecCreateByMatT_PETSc(&(b_Hh[ind_eig]), Prolongs[ind_refine], MVOps);
            MatTVecMult_PETSc(Prolongs[ind_refine], Ahuh, b_Hh[ind_eig], MVOps);
            // beta = u_h^T * Ahuh
            for (i = ind_eig + 1; i < num_eig; i++)
            {
                VecInner_PETSc(eigvec[i], Ahuh, &(beta[i][ind_eig]), MVOps);
                beta[ind_eig][i] = beta[i][ind_eig];
            }
            VecInner_PETSc(eigvec[ind_eig], Ahuh, &(beta[ind_eig][ind_eig]), MVOps);
            VecDestory_PETSc(&(Ahuh), MVOps);

            // 计算 B_hh * u_h
            VecCreateByMat_PETSc(&(Bhuh), MassMatrixh->data_solver, MVOps);
            MatVecMult_PETSc(MassMatrixh->data_solver, eigvec[ind_eig], Bhuh, MVOps); //多个这里需要替换成矩阵乘矩阵
            // c_Hh = I_h^H * Bhuh
            VecCreateByMatT_PETSc(&(c_Hh[ind_eig]), Prolongs[ind_refine], MVOps);
            MatTVecMult_PETSc(Prolongs[ind_refine], Bhuh, c_Hh[ind_eig], MVOps);
            // gamma = u_h^T * Bhuh
            for (i = ind_eig + 1; i < num_eig; i++)
            {
                VecInner_PETSc(eigvec[i], Bhuh, &(gamma[i][ind_eig]), MVOps);
                gamma[ind_eig][i] = gamma[i][ind_eig];
            }
            VecInner_PETSc(eigvec[ind_eig], Bhuh, &(gamma[ind_eig][ind_eig]), MVOps);
            VecDestory_PETSc(&(Bhuh), MVOps);
            VecDestory_PETSc(&(eigvec[ind_eig]), MVOps);
            eigvec[ind_eig] = NULL;
        }
        // 求解 [A_H b_Hh; b_Hh' beta] u = \lambda [B_H c_Hh; c_Hh' gamma] u
        // 现在矩阵已经改成了对称矩阵 线性问题可以调用有预条件的 PASE
        // 这里需要一个接口来调用带有预条件的PASE 
        // 现有的有：1. 并行的矩阵A_H B_H petsc格式 void*存储 2. 并行的向量组b_Hh c_Hh 以单向量petsc格式 void*存储 整体存储在void**中
        // 3.串行的小矩阵beta gamma DOUBLE**存储 输入这六个东西以及是否有A^{-1} 如果是NULL就计算并保存这个矩阵 如果非NULL就直接使用
        // 返回一个新的向量组 以什么形式 每个向量前半部分是并行存储的 后半部分是串行存储的

        // 这里由于没有 所以直接调用细空间上的eigsolver
        solver_eigen = NULL;
        EigenSolverCreate(&solver_eigen);
        EigenSolverSetType(solver_eigen, SLEPc);
        EigenSolverSetNev(solver_eigen, num_eig);                         // 求解num_eig个特征对
        EigenSolve(StiffMatrixh, MassMatrixh, solver_eigen, KrylovSchur); //这里 A_H B_H 格式转换
        EigenSolverGetOriginalEigenpairs(solver_eigen, &eigval, &eigvec);
        solver_eigen->Eval = NULL;
        solver_eigen->Evec = NULL;
        EigenSolverDestory(&solver_eigen);
        // 得到细空间的解 自适应加密

        PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        Vec2FEMVEC_PETSc(femspaceh, eigvec[aim_eig], &eig_vec_femvec);
        EigenPosterioriErrorEstimate(eig_vec_femvec, &(eigval[aim_eig]), 4, 3, &PosterioriErrorIndex[0],
                                     PostErrFunElem, PostErrFunEdge, Quadrature,
                                     QuadratureEdge, PosteriorError, &TotalError);
        FreeFEMFuntion(eig_vec_femvec);
        eig_vec_femvec = NULL;
        PostErrorOfBisection[ind_refine] = TotalError;
        //网格加密
        if (ind_refine < max_refine - 1)
        {
            OpenPFEM_Print("Start Adaptive refining the mesh!\n");
            MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
            OpenPFEM_Print("完成自适应加密\n");
        }
        OpenPFEM_Free(PosteriorError);
    }

    //输出每次自适应加密的结果

    OpenPFEM_Print("单元个数 N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");

    OpenPFEM_Finalize();
    return 0;
}