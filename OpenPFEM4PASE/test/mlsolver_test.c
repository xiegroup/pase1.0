#include "multilevel.h"
#include "linearsolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
}

void rhsvec(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (1.0 + 3.0 * PI * PI) * right[0] * sin(PI * coord[0]) * sin(PI * coord[1]) * sin(PI * coord[2]);
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void ExactSolu3D(double X[3], int dim, double *values)
{
    values[0] = sin(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
}

void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = PI * cos(PI * X[0]) * sin(PI * X[1]) * sin(PI * X[2]);
    values[1] = PI * sin(PI * X[0]) * cos(PI * X[1]) * sin(PI * X[2]);
    values[2] = PI * sin(PI * X[0]) * sin(PI * X[1]) * cos(PI * X[2]);
}

void ErrFun3DL2(DOUBLE *femvalue, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvalue[0] - Funvalue[0]) * (femvalue[0] - Funvalue[0]);
}

void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}

#if 0
int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);

    // 基础设置
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};

    // 创建multilevel和solver
    MULTILEVEL *multilevel = NULL;
    MultiLevelCreate(&multilevel, TYPE_PETSC);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    INT level_num = 3, i;
    for (i = 0; i < level_num; i++)
    {
        if (i == 0) // 初始层
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, BoundCond);
            DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(coarsespace, 4, &stiffLmultiindex[0], coarsespace, 4,
                                                                &stiffRmultiindex[0], stiffmatrix, rhsvec, BoundFun, Quadrature);
            MATRIX *StiffMatrix = NULL;
            VECTOR *Rhs = NULL;
            MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
            MultiLevelAddLevelSimple(multilevel, coarsespace);
            MultiLevelSetMatrix(multilevel, StiffMatrix, NULL, 0);
            MultiLevelSetVector(multilevel, Rhs, NULL, 0);
        }
        else // 每次添加层
        {
            // 加密一次
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
            DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(finerspace, 4, &stiffLmultiindex[0], finerspace, 4,
                                                                &stiffRmultiindex[0], stiffmatrix, rhsvec, BoundFun, Quadrature);
            MATRIX *StiffMatrix = NULL;
            VECTOR *Rhs = NULL;
            MatrixAssemble(&StiffMatrix, &Rhs, StiffDiscreteForm, TYPE_PETSC);
            MultiLevelAddLevelSimple(multilevel, finerspace);
            MultiLevelSetMatrix(multilevel, StiffMatrix, NULL, i);
            MultiLevelSetVector(multilevel, Rhs, NULL, i);
            // destory
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            // copy
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);

    MultiLevelDestroy(&multilevel);
    QuadratureDestroy(&Quadrature);
    OpenPFEM_Finalize();
    return 0;
}
#else

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 2);
    MeshPartition(mesh);

    // 基础设置
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral127);
    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX stiffRmultiindex[4] = {D000, D100, D010, D001};

    // 创建multilevel和solver
    MULTILEVEL *multilevel = NULL;
    MultiLevelCreate(&multilevel, TYPE_PETSC);
    LINEARSOLVER *solver = NULL;
    LinearSolverCreate(&solver, MULTILEVEL_LINEAR);
    LinearSolverMultiLevelSetUp(solver, multilevel);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    INT level_num = 2, i;
    for (i = 0; i < level_num; i++)
    {
        if (i == 0) // 初始层
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, BoundCond);
            DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(coarsespace, 4, &stiffLmultiindex[0], coarsespace, 4,
                                                                &stiffRmultiindex[0], stiffmatrix, rhsvec, BoundFun, Quadrature);
            MultiLevelAddLevel(multilevel, StiffDiscreteForm, NULL);
            LinearSolveMultiLevel(solver, multilevel);
        }
        else // 每次添加层
        {
            // 加密一次
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
            DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(finerspace, 4, &stiffLmultiindex[0], finerspace, 4,
                                                                &stiffRmultiindex[0], stiffmatrix, rhsvec, BoundFun, Quadrature);
            MultiLevelAddLevel(multilevel, StiffDiscreteForm, NULL);
            LinearSolveMultiLevel(solver, multilevel);
            // destory
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            // copy
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    LinearSolveMultiLevel(solver, multilevel);

    FEMFUNCTION *solution = FEMFunctionBuild(coarsespace, 1);
    VectorGetFEMFunction(multilevel->solution[level_num - 1], solution, 0);

    MULTIINDEX massmultiindex[1] = {D000};
    DOUBLE H1Error = 0.0, L2Error = 0.0;
    L2Error = ErrorEstimate(solution, 1, &massmultiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    H1Error = ErrorEstimate(solution, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
    OpenPFEM_Print("L2Error is %2.10f, H1Error is %2.10f\n", L2Error, H1Error);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    MultiLevelDestroy(&multilevel);
    LinearSolverDestroy(&solver);
    QuadratureDestroy(&Quadrature);
    OpenPFEM_Finalize();
    return 0;
}

#endif