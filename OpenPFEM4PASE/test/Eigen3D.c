#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    // (\nable u, \nable v)
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

int main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56), *QuadratureEdge = QuadratureBuild(QuadTriangle13);
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P1_3D, BoundCond);
    FEMSPACE *massfemspace = FEMSpaceBuild(mesh, C_T_P1_3D, MassBoundCond);
    DISCRETEFORM *StiffDiscreteForm = DiscreteFormBuild(femspace, 3, stiffLmultiindex, femspace, 3, stiffRmultiindex,
                                                        stiffmatrix, NULL, BoundFun, Quadrature);
    DISCRETEFORM *MassDiscreteForm = DiscreteFormBuild(massfemspace, 1, massLmultiindex, massfemspace, 1, massRmultiindex,
                                                       massmatrix, NULL, BoundFun, Quadrature);

    MATRIX *StiffMatrix = NULL, *MassMatrix = NULL;
    MatrixAssemble(&StiffMatrix, NULL, StiffDiscreteForm, TYPE_SLEPC);
    MatrixAssemble(&MassMatrix, NULL, MassDiscreteForm, TYPE_SLEPC);

    INT nev = 10;
    VECTORS *evec = NULL;
    VectorsCreateByMatrix(&evec, StiffMatrix, nev);
    DOUBLE *eval = (DOUBLE *)malloc(nev * sizeof(DOUBLE));

    EIGENSOLVER *solver = NULL;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    int i;
    EigenSolverSolve(solver, StiffMatrix, MassMatrix, eval, evec, nev);
    for (i = 0; i < nev; i++)
    {
        OpenPFEM_Print("eval[%d]=%g\n", i, eval[i]);
    }

    EigenSolverDestroy(&solver);
    OpenPFEM_Free(eval);
    VectorsDestroy(&evec);
    MatrixDestroy(&StiffMatrix);
    MatrixDestroy(&MassMatrix);
    DiscreteFormDestroy(&StiffDiscreteForm);
    DiscreteFormDestroy(&MassDiscreteForm);
    FEMSpaceDestroy(&femspace);
    FEMSpaceDestroy(&massfemspace);
    QuadratureDestroy(&Quadrature);
    QuadratureDestroy(&QuadratureEdge);
    MeshDestroy(&mesh);
    OpenPFEM_Finalize();
    return 0;
}