#include "OpenPFEM.h"
#include "linearsolver.h"
#include "eigensolver.h"
#include "multilevel.h"
#include "errorestimate.h"

void GetAugmentedMatrix(MATRIX *AH, MATRIX *BH, VECTORS *aHh, VECTORS *bHh, DOUBLE *alpha, DOUBLE *beta, MATRIX **AHh, MATRIX **BHh);
void AugmentedMatrix(Mat A, Mat B, Mat *auA, Mat *auB);
void SeparateEvecs(VECTORS *Evecs, VECTORS *output, DOUBLE *scale);
void VectorsAxpby(DOUBLE a, DOUBLE b, VECTORS *x, VECTORS *y);

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}
// BOUNDARYTYPE AddBoundCond(INT bdid)
// {
//     if (bdid > 0)
//     {
//         return MASSDIRICHLET;
//     }
//     else
//     {
//         return INNER;
//     }
// }
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}
// void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
// {
//     value[0] = left[1] * right[1] + left[2] * right[2] + left[3] * right[3];
// }

void stiffmatrixlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0] + left[1] * right[1] + left[2] * right[2];
}

void stiffmatrixnonlinear(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * AuxFEMValues[0] * left[0] * right[0];
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

void rhsnonlinear(DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = AuxFEMValues[0] * right[0];
}

void PostErrFunElem(DOUBLE lambda, DOUBLE *eigfun, DOUBLE *value)
{
    value[0] = (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3] - eigfun[0] * eigfun[0] * eigfun[0]) *
               (lambda * eigfun[0] + eigfun[1] + eigfun[2] + eigfun[3] - eigfun[0] * eigfun[0] * eigfun[0]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}

// 考虑非线性特征值问题$-\Delta u + u^3 = \lambda u$
// 采用扩展子空间的迭代方法求解
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);

    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL);
    MeshUniformRefine(mesh, 1);
    MeshPartition(mesh);
    MeshUniformRefine(mesh, 0);
    // 粗网格
    MeshSetAsAncestor(mesh);
    MESH *mesh_H = MeshDuplicate(mesh);
    // 细网格
    // MeshUniformRefine(mesh, 1);
    DOUBLE starttime, endtime;
    starttime = MPI_Wtime();
    // 积分微分信息
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);
    QUADRATURE *QuadratureEdge = QuadratureBuild(QuadTriangle13);
    // 刚度矩阵只用到了梯度的信息
    MULTIINDEX stifflinearLmi[3] = {D100, D010, D001};
    MULTIINDEX stifflinearRmi[3] = {D100, D010, D001};
    // 刚度矩阵只用到了梯度的信息
    MULTIINDEX stiffnonlinearLmi[1] = {D000};
    MULTIINDEX stiffnonlinearRmi[1] = {D000};
    // 质量矩阵只用到了原函数的信息
    MULTIINDEX massLmultiindex[1] = {D000};
    MULTIINDEX massRmultiindex[1] = {D000};
    // 在做校正步时 把解带入到右端时需要的信息
    INT NAuxFEMFunMultiIndex[1] = {1};
    MULTIINDEX auxfemfunmi[1] = {D000};
    // 计算误差 需要的积分信息
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX PosterioriErrorIndex[7] = {D000, D200, D020, D002, D100, D010, D001};
    // 计算内积 需要的积分信息
    MULTIINDEX ErrorIndex[1] = {D000};

    INT eig_num = 1; // 扩展子空间求解时增加的特征对个数
    INT eig_aim = 0; // 自适应加密是针对的特征对是第几个

    INT SCF_ind, refine_ind, eig_ind, level_ind;
    INT SCF_max = 5, refine_max = 5;
    DOUBLE TolValue = 1e-5;          // 判断特征值是否收敛
    DOUBLE TolVec = 1e-5;            // 判断特征向量是否收敛
    DOUBLE TolPosteriorError = 1e-8; // 判断自适应加密是否终止
    // 1. 在粗网格上利用自洽场迭代求解$-\Delta u + u^3 = \lambda u$ 求得特征对作为初值
    MULTILEVEL *multilevel;
    MultiLevelCreate(&multilevel, TYPE_PETSC);
    MultiLevelSetNev(multilevel, eig_num);
    // 1.1 求解线性特征值问题$-\Delta u = \lambda u$的解作为初值
    FEMSPACE *femspace = FEMSpaceBuild(mesh, C_T_P5_3D, BoundCond);
    FEMSPACE *massfemspace = FEMSpaceBuild(mesh, C_T_P5_3D, MassBoundCond);
    FEMSPACE *femspace_H = FEMSpaceBuild(mesh_H, C_T_P5_3D, BoundCond);
    FEMSPACE *massfemspace_H = FEMSpaceBuild(mesh_H, C_T_P5_3D, MassBoundCond);
    DISCRETEFORM *stiff_linear = DiscreteFormBuild(femspace_H, 3, stifflinearLmi, femspace_H, 3, stifflinearRmi,
                                                   stiffmatrixlinear, NULL, BoundFun, Quadrature);
    DISCRETEFORM *Mass_DF = DiscreteFormBuild(massfemspace_H, 1, massLmultiindex, massfemspace_H, 1, massRmultiindex,
                                              massmatrix, NULL, BoundFun, Quadrature);
    MultiLevelAddLevel(multilevel, stiff_linear, Mass_DF);
    MATRIX *Stiff_L = multilevel->stiff_matrices[0];
    MATRIX *Mass = multilevel->mass_matrices[0];
    DOUBLE *Eval = (double *)multilevel->eigvalues;
    VECTORS *Evec = multilevel->eigvecs[0];

    // printf("finish 0 2 %p %p\n", Evec, multilevel->eigvecs[0]);
    // Evec->type = TYPE_PETSC;
    // printf("finish 0 1\n");
    // multilevel->eigvecs[0]->type = TYPE_PETSC;
    // printf("finish 0 2\n");
    // MPI_Barrier(MPI_COMM_WORLD);
    MATRIX *Prolong;

    EIGENSOLVER *solver;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    EigenSolverSolve(solver, Stiff_L, Mass, Eval, Evec, eig_num);
    printf("finish 1 2 %2.20f %d\n", Eval[0], 0);
    multilevel->eigvecs[0] = Evec;
    // multilevel->eigvalues[0] = Eval;

    printf("finish 1 2 %2.20f %d\n", Eval[0], 0);
    // Evec->type = TYPE_PETSC;
    // printf("finish 1 1\n");
    // multilevel->eigvecs[0]->type = TYPE_PETSC;
    // printf("finish 1 2\n");
    // MPI_Barrier(MPI_COMM_WORLD);

    // printf("第0层evec的地址 %p\n", &Evec);
    FEMFUNCTION *eigfunc = FEMFunctionBuild(femspace);
    VectorsGetFEMFunction(Evec, eigfunc, eig_aim);
    DOUBLE eigval_temp = Eval[eig_aim];
    // printf("multilevel->eigvecs[refine_ind] %d %d\n", multilevel->eigvecs[0]->local_length, 0);
    // printf("multilevel->eigvecs[refine_ind] %d %d\n", 0, multilevel->eigvecs[0]->global_length);

    // 1.2 对非线性问题进行自洽场迭代
    AUXFEMFUNCTION *auxfunc = AuxFEMFunctionBuild(1, &eigfunc, NAuxFEMFunMultiIndex, auxfemfunmi);
    DISCRETEFORM *StiffNonlinear = DiscreteFormAuxFeFunBuild(femspace, 1, stiffnonlinearLmi, massfemspace, 1, stiffnonlinearRmi,
                                                             auxfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);
    MATRIX *Stiff_NL = NULL;
    MatrixBeginAssemble(&Stiff_NL, NULL, StiffNonlinear, TYPE_PETSC);
    MatrixAxpby(1.0, Stiff_L, 1.0, Stiff_NL, NONZERO_DIFF);
    EigenSolverSolve(solver, Stiff_NL, Mass, Eval, Evec, eig_num);
    multilevel->eigvecs[0] = Evec;
    // multilevel->eigvalues[0] = Eval;
    eigval_temp -= Eval[eig_aim];
    eigval_temp = fabs(eigval_temp);
    FEMFUNCTION *eigfunc_tmp = FEMFunctionBuild(femspace);
    VectorsGetFEMFunction(Evec, eigfunc_tmp, eig_aim);
    FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc);
    DOUBLE Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    FEMFunctionAxpby(-2.0, eigfunc_tmp, -1.0, eigfunc);
    DOUBLE Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
    OpenPFEM_Print("[0] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", Eval[0], eigval_temp, min(Error1, Error2));
    FEMFunctionCopy(eigfunc_tmp, eigfunc);
    eigval_temp = Eval[eig_aim];
    // Evec->type = TYPE_PETSC;
    // printf("finish 2 1\n");
    // multilevel->eigvecs[0]->type = TYPE_PETSC;
    // printf("finish 2 2\n");
    // MPI_Barrier(MPI_COMM_WORLD);
    for (SCF_ind = 0; SCF_ind < SCF_max; SCF_ind++)
    {
        MatrixReAssemble(Stiff_NL, NULL);
        MatrixAxpby(1.0, Stiff_L, 1.0, Stiff_NL, NONZERO_DIFF);
        // Evec->type = TYPE_PETSC;
        // printf("finish 3 1\n");
        // multilevel->eigvecs[0]->type = TYPE_PETSC;
        // printf("finish 3 2\n");
        // MPI_Barrier(MPI_COMM_WORLD);
        EigenSolverSolve(solver, Stiff_NL, Mass, Eval, Evec, eig_num);
        multilevel->eigvecs[0] = Evec;
        // Evec->type = TYPE_PETSC;
        // printf("finish 4 1\n");
        // multilevel->eigvecs[0]->type = TYPE_PETSC;
        // printf("finish 4 2\n");
        // MPI_Barrier(MPI_COMM_WORLD);
        // VectorPrint(Evec, 0);
        eigval_temp -= Eval[eig_aim];
        eigval_temp = fabs(eigval_temp);
        VectorsGetFEMFunction(Evec, eigfunc_tmp, eig_aim);
        FEMFunctionAxpby(1.0, eigfunc_tmp, -1.0, eigfunc);
        Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
        FEMFunctionAxpby(-2.0, eigfunc_tmp, -1.0, eigfunc);
        Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
        OpenPFEM_Print("[%d] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", SCF_ind + 1, Eval[0], eigval_temp, min(Error1, Error2));
        if ((eigval_temp < TolValue) && (min(Error1, Error2) < TolVec))
        {
            SCF_ind = SCF_max;
            OpenPFEM_Print("End SCF!\n");
        }
        FEMFunctionCopy(eigfunc_tmp, eigfunc);
        eigval_temp = Eval[eig_aim];
    }
    EigenSolverDestroy(&solver);
    // FEMFunctionCopy(eigfunc_tmp, eigfunc);
    // FEMFuntionDestroy(&eigfunc);
    FEMFuntionDestroy(&eigfunc_tmp);
    eigval_temp = Eval[eig_aim];
    // endtime = MPI_Wtime();
    // OpenPFEM_Print("The time for Ahh matrix: %2.10f\n", endtime - starttime);
    // 2. 自适应加密迭代
    DOUBLE TotalError = 0.0;
    INT *NumElemOfBisection = malloc(refine_max * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(refine_max * sizeof(DOUBLE));
    VECTOR *Rhs_NL;
    multilevel->eigvecs[0]->type = TYPE_PETSC;
    Evec->type = TYPE_PETSC;
    LINEARSOLVER *linear_solver = NULL;
    VECTOR *solu_temp;
    EigenSolverCreate(&solver, SLEPC_KRYLOVSCHUR);
    // return 0;
    FEMFUNCTION *temp_scf = NULL;
    for (refine_ind = 0; refine_ind < refine_max; refine_ind++)
    {
        // 2.1 加密：根据已计算出的特征对计算后验误差 进行自适应加密
        DOUBLE *PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        EigenPosterioriErrorEstimate(eigfunc, &(eigval_temp), 4, 3, &PosterioriErrorIndex[0],
                                     PostErrFunElem, PostErrFunEdge, Quadrature,
                                     QuadratureEdge, PosteriorError, &TotalError);
        OpenPFEM_Print("[%d] Num_elem %d Posterior Error is %2.14f.\n", refine_ind, mesh->num_volu, TotalError);
        if (TotalError < TolPosteriorError)
        {
            refine_ind = refine_max;
        }
        NumElemOfBisection[refine_ind] = mesh->num_volu;
        PostErrorOfBisection[refine_ind] = TotalError;
        OpenPFEM_Print("Start Adaptive refining the mesh!\n");
        if (temp_scf != NULL)
        {
            FEMFunctionAxpby(1.0, temp_scf, -1.0, eigfunc);
            Error1 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
            FEMFunctionAxpby(-2.0, temp_scf, -1.0, eigfunc);
            Error2 = ErrorEstimate(eigfunc, 1, &ErrorIndex[0], NULL, ErrFun, Quadrature);
            OpenPFEM_Print("[%d] EigenValue is %2.14f, Eval Error is %2.14f, Evec Error is %2.14f.\n", SCF_ind + 1, Eval[0], temp_scf, min(Error1, Error2));
            if ((eigval_temp < TolValue) && (min(Error1, Error2) < TolVec))
            {
                MeshUniformRefine(mesh, 1);
                // 2.2 插值：将特征向量插值到细空间
                femspace = FEMSpaceBuild(mesh, C_T_P5_3D, BoundCond);
                massfemspace = FEMSpaceBuild(mesh, C_T_P5_3D, MassBoundCond);
                stiff_linear = DiscreteFormBuild(femspace, 3, stifflinearLmi, femspace, 3, stifflinearRmi,
                                                 stiffmatrixlinear, NULL, BoundFun, Quadrature);
                Mass_DF = DiscreteFormBuild(massfemspace, 1, massLmultiindex, massfemspace, 1, massRmultiindex,
                                            massmatrix, NULL, BoundFun, Quadrature);
                MultiLevelAddLevel(multilevel, stiff_linear, Mass_DF);

                Stiff_L = multilevel->stiff_matrices[refine_ind + 1];
                Mass = multilevel->mass_matrices[refine_ind + 1];
                Eval = (double *)multilevel->eigvalues;
                Evec = multilevel->eigvecs[refine_ind + 1];
                Prolong = multilevel->prolongs[refine_ind];
                VectorsCreateByMatrix(&Evec, Prolong, eig_num);

                MatrixVectorsMult(Prolong, multilevel->eigvecs[refine_ind], Evec);
                eigfunc = FEMFunctionBuild(femspace);
                VectorsGetFEMFunction(Evec, eigfunc, eig_aim);
            }
            else
            {
                refine_ind--;
            }
            FEMFunctionCopy(eigfunc_tmp, eigfunc);
        }
        // MeshAdaptiveRefine(mesh, PosteriorError, 0.6);
        OpenPFEM_Print("完成自适应加密\n");
        free(PosteriorError);
        MATRIX *A_HH_L, *A_HH_NL, *B_HH, *A_hh, *B_hh, *A_Hh, *B_Hh;
        A_HH_L = Stiff_L;
        B_HH = Mass;
        // 2.3 校正：求解细空间上的线性问题(将细空间上特征向量代入 转化为线性问题)
        StiffNonlinear = DiscreteFormAuxFeFunBuild(femspace, 1, stiffnonlinearLmi, massfemspace, 1, stiffnonlinearRmi,
                                                   auxfunc, stiffmatrixnonlinear, rhsnonlinear, BoundFun, Quadrature);
        MatrixAssemble(&Stiff_NL, &Rhs_NL, StiffNonlinear, TYPE_PETSC);
        MatrixAxpby(1.0, Stiff_L, 1.0, Stiff_NL, NONZERO_DIFF);
        solu_temp = NULL;
        VectorCreateByMatrix(&solu_temp, Stiff_NL);
        LinearSolverCreate(&linear_solver, PETSC_KSPCG); // PETSC_SUPERLU); // PETSC_KSPCG);
        LinearSolve(linear_solver, Stiff_NL, Rhs_NL, solu_temp);
        BVInsertVec((BV)(Evec->data_slepc), 0, (Vec)(solu_temp->data_petsc));
        VectorGetFEMFunction(solu_temp, eigfunc);
        LinearSolverDestroy(&linear_solver);
        // Evec->data_slepc = solu_temp->data_petsc;
        // eigfunc_tmp = FEMFunctionBuild(femspace);
        // 2.4 在V_{H,h}上利用自洽场迭代求解非线性特征值问题$-\Delta u + u^3 = \lambda u$
        // 2.4.1 求解线性问题 特征对当作(u^{old})
        // 扩展广义特征值的两个矩阵A_Hh，B_Hh中的元素
        // 线性部分
        // MATRIX *A_HH_L, *A_HH_NL, *B_HH, *A_hh, *B_hh, *A_Hh, *B_Hh;
        VECTORS *a_Hh, *b_Hh, *temp, *vec_argu;
        DOUBLE *alpha, *beta, *eval_argu;
        VECTORS *vector_coarse;
        DOUBLE scale_fine;
        beta = malloc(eig_num * eig_num * sizeof(DOUBLE));
        alpha = malloc(eig_num * eig_num * sizeof(DOUBLE));
        eval_argu = malloc(eig_num * sizeof(DOUBLE));
        // A_HH_L = multilevel->stiff_matrices[0];
        // B_HH = multilevel->mass_matrices[0];
        A_hh = Stiff_NL;
        B_hh = multilevel->mass_matrices[refine_ind + 1];
        // Prolong = multilevel->prolongs[refine_ind]; //插值矩阵
        StiffNonlinear = DiscreteFormAuxFeFunBuild(femspace_H, 1, stiffnonlinearLmi, massfemspace_H, 1, stiffnonlinearRmi,
                                                   auxfunc, stiffmatrixnonlinear, NULL, BoundFun, Quadrature);

        eigfunc = FEMFunctionBuild(femspace);
        // 非线性的部分
        // 2.4.2 求解非线性问题
        for (SCF_ind = 0; SCF_ind < SCF_max; SCF_ind++)
        {
            // A_HH
            MatrixAssemble(&A_HH_NL, NULL, StiffNonlinear, TYPE_PETSC);
            MatrixAxpby(1.0, A_HH_L, 1.0, A_HH_NL, NONZERO_DIFF); // A_HH = A_HH_NL
            // a_Hh
            temp = Evec;
            VectorsCreateByMatrix(&a_Hh, A_hh, eig_num);
            MatrixVectorsMult(A_hh, temp, a_Hh);
            for (level_ind = 0; level_ind <= refine_ind; level_ind++)
            {
                temp = a_Hh;
                VectorsCreateByMatrixTranspose(&a_Hh, multilevel->prolongs[refine_ind - level_ind], eig_num);
                // printf("level %d prolong row%d %d col%d %d\n", level_ind, multilevel->prolongs[refine_ind - level_ind]->local_nrows, multilevel->prolongs[refine_ind - level_ind]->global_nrows, multilevel->prolongs[refine_ind - level_ind]->local_ncols, multilevel->prolongs[refine_ind - level_ind]->global_ncols);
                // printf("level %d temp %d %d\n", level_ind, temp->local_length, temp->global_length);
                MatrixTransposeVectorsMult(multilevel->prolongs[refine_ind - level_ind], temp, a_Hh);
                VectorsDestroy(&temp);
            }
            // printf("dim :: %d %d\n", a_Hh->local_length, a_Hh->global_length);
            // alpha
            BVSetMatrix((BV)(Evec->data_slepc), (Mat)(A_hh->data_petsc), PETSC_TRUE);
            BVNormColumn((BV)(Evec->data_slepc), 0, NORM_2, alpha);
            alpha[0] = alpha[0] * alpha[0];

            // b_Hh
            temp = Evec;
            VectorsCreateByMatrix(&b_Hh, B_hh, eig_num);
            MatrixVectorsMult(B_hh, temp, b_Hh);
            for (level_ind = 0; level_ind <= refine_ind; level_ind++)
            {
                temp = b_Hh;
                VectorsCreateByMatrixTranspose(&b_Hh, multilevel->prolongs[refine_ind - level_ind], eig_num);
                MatrixTransposeVectorsMult(multilevel->prolongs[refine_ind - level_ind], temp, b_Hh);
                VectorsDestroy(&temp);
            }
            // beta
            BVSetMatrix((BV)(Evec->data_slepc), (Mat)(B_hh->data_petsc), PETSC_TRUE);
            BVNormColumn((BV)(Evec->data_slepc), 0, NORM_2, beta);
            beta[0] = beta[0] * beta[0];
            // 拼接成复合矩阵
            GetAugmentedMatrix(A_HH_NL, B_HH, a_Hh, b_Hh, alpha, beta, &A_Hh, &B_Hh);
            VectorsCreateByMatrix(&vec_argu, A_Hh, eig_num);
            EigenSolverSolve(solver, A_Hh, B_Hh, eval_argu, vec_argu, eig_num);
            VectorsCreateByMatrix(&vector_coarse, A_HH_NL, eig_num);
            SeparateEvecs(vec_argu, vector_coarse, &scale_fine);
            for (level_ind = 0; level_ind <= refine_ind; level_ind++)
            {
                temp = vector_coarse;
                VectorsCreateByMatrix(&vector_coarse, multilevel->prolongs[level_ind], eig_num);
                MatrixVectorsMult(multilevel->prolongs[level_ind], temp, vector_coarse);
                VectorsDestroy(&temp);
            }
            VectorsAxpby(scale_fine, 1.0, Evec, vector_coarse);
            VectorsGetFEMFunction(vector_coarse, eigfunc, eig_aim);
            FEMFunctionCopy(eigfunc, temp_scf);
        }
        // return 0;
        if (refine_ind == 0)
        {
            endtime = MPI_Wtime();
            OpenPFEM_Print("The time for Ahh matrix: %2.10f\n", endtime - starttime);
        }
    }
    OpenPFEM_Finalize();
    return 0;
}

void GetAugmentedMatrix(MATRIX *AH, MATRIX *BH, VECTORS *aHh, VECTORS *bHh, DOUBLE *alpha, DOUBLE *beta, MATRIX **AHh, MATRIX **BHh)
{
    INT lnrows = AH->local_nrows, lncols = AH->local_ncols, nev = aHh->nvecs;
    INT gnrows = AH->global_nrows, gncols = AH->global_ncols;
    INT myrank, size, addnrows, addncols;
    addnrows = 0;
    addncols = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (myrank == (size - 1))
    {
        addnrows += nev;
        addncols += nev;
    }
    // 新矩阵
    MATRIX *argumentA = malloc(sizeof(MATRIX));
    argumentA->type = TYPE_PETSC;
    argumentA->comm = AH->comm;
    argumentA->ops = AH->ops;
    argumentA->global_nrows = AH->global_nrows + nev;
    argumentA->global_ncols = AH->global_ncols + nev;
    argumentA->local_nrows = AH->local_nrows + addnrows;
    argumentA->local_ncols = AH->local_ncols + addncols;
    argumentA->symmetric = AH->symmetric;
    argumentA->delete_dirichletbd = AH->delete_dirichletbd;
    argumentA->if_matrix = 0;
    argumentA->if_petsc = 1;
    Mat A_old = (Mat)AH->data_petsc;

    MATRIX *argumentB = malloc(sizeof(MATRIX));
    argumentB->type = TYPE_PETSC;
    argumentB->comm = BH->comm;
    argumentB->ops = BH->ops;
    argumentB->global_nrows = BH->global_nrows + nev;
    argumentB->global_ncols = BH->global_ncols + nev;
    argumentB->local_nrows = BH->local_nrows + addnrows;
    argumentB->local_ncols = BH->local_ncols + addncols;
    argumentB->symmetric = BH->symmetric;
    argumentB->delete_dirichletbd = BH->delete_dirichletbd;
    argumentB->if_matrix = 0;
    argumentB->if_petsc = 1;
    Mat B_old = (Mat)BH->data_petsc;

    // MatType type;
    // MatGetType(A_old, &type);

    // printf("myrank %d lnrows %d lncols %d addnrows %d addncols %d\n", myrank, lnrows, lncols, addnrows, addncols);
    // MatCreate(AH->comm, &AA);
    // MatSetType(AA, type);
    // MatSetSizes(AA, lnrows + addnrows, lncols + addncols, PETSC_DECIDE, PETSC_DECIDE);
    // MatSetUp(AA);

    // // 将子矩阵A的元素拷贝到矩阵AA中
    // MatCopy(A_old, AA, SAME_NONZERO_PATTERN);

    // 创建并设置矩阵AA
    Mat AA, BB;
    AugmentedMatrix((Mat)AH->data_petsc, (Mat)BH->data_petsc, &AA, &BB);

    // int m,n;
    // MatGetSize(AA, &m, &n);
    // printf("global %d %d\n", m, n);
    // MatGetLocalSize(AA, &m, &n);
    // printf("local %d %d\n", m, n);

    // 将向量b的元素拷贝到矩阵AA中
    Vec a_global, b_global;
    BVCreateVec((BV)(aHh->data_slepc), &a_global);
    BVCreateVec((BV)(bHh->data_slepc), &b_global);
    PetscInt start, end;
    PetscScalar value;
    VecGetOwnershipRange(a_global, &start, &end);
    // VecGetSize(a_global, &m);
    // VecGetLocalSize(a_global, &n);
    // printf("rank %d Vec %d Local %d\n", myrank, m, n);
    // printf("rank %d start %d end %d\n", myrank, start, end);
    for (PetscInt i = start; i < end; i++)
    {
        // printf("rank %d insert %d %d\n", myrank, i, gncols+1);
        VecGetValues(a_global, 1, &i, &value);
        MatSetValue(AA, i, gncols, value, INSERT_VALUES);
        // MatSetValue(AA, gnrows, i, value, INSERT_VALUES);
    }
    VecGetOwnershipRange(b_global, &start, &end);
    for (PetscInt i = start; i < end; i++)
    {
        VecGetValues(b_global, 1, &i, &value);
        MatSetValue(BB, i, gncols, value, INSERT_VALUES);
        // MatSetValue(BB, gnrows, i, value, INSERT_VALUES);
    }
    // return;
    VecDestroy(&a_global);
    VecDestroy(&b_global);

    MatSetValue(AA, gnrows, gncols, alpha[0], INSERT_VALUES);
    MatSetValue(BB, gnrows, gncols, beta[0], INSERT_VALUES);

    // 完成矩阵AA的组装
    MatAssemblyBegin(AA, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(AA, MAT_FINAL_ASSEMBLY);
    // 完成矩阵BB的组装
    MatAssemblyBegin(BB, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BB, MAT_FINAL_ASSEMBLY);

    // PetscViewer viewer;
    // MatView(AA, viewer);

    argumentA->data_petsc = (void *)AA;
    *AHh = argumentA;
    argumentB->data_petsc = (void *)BB;
    *BHh = argumentB;

    // MatConvert(AA, MATSBAIJ, MAT_INITIAL_MATRIX, auA);
    // MatConvert(BB, MATSBAIJ, MAT_INITIAL_MATRIX, auB);
}

void VectorsAxpby(DOUBLE a, DOUBLE b, VECTORS *x, VECTORS *y)
{
    Vec xx, yy;
    BVCreateVec((BV)(x->data_slepc), &xx);
    BVCreateVec((BV)(y->data_slepc), &yy);
    VecAXPBY(yy, a, b, xx);
    BVInsertVec((BV)(y->data_slepc), 0, yy);
    VecDestroy(&xx);
    VecDestroy(&yy);
}

void AugmentedMatrix(Mat A, Mat B, Mat *auA, Mat *auB)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    Mat AA, BB;
    MatCreate(PETSC_COMM_WORLD, &AA);
    MatCreate(PETSC_COMM_WORLD, &BB);
    int local_n, global_n;
    MatGetSize(A, &global_n, NULL);
    MatGetLocalSize(A, &local_n, NULL);
    int nev = 1;
    global_n += nev;
    if (rank == size - 1)
        local_n += nev;
    MatSetSizes(AA, local_n, local_n, global_n, global_n);
    MatSetSizes(BB, local_n, local_n, global_n, global_n);
    MatSetFromOptions(AA);
    MatSetFromOptions(BB);

    int *A_dnnz = (int *)calloc(local_n, sizeof(int));
    int *A_onnz = (int *)calloc(local_n, sizeof(int));
    int *B_dnnz = (int *)calloc(local_n, sizeof(int));
    int *B_onnz = (int *)calloc(local_n, sizeof(int));
    int i, j, A_ncols, B_ncols;
    const int *A_cols, *B_cols;
    const double *A_vals, *B_vals;
    int row_s, row_e;
    MatGetOwnershipRange(A, &row_s, &row_e);
    if (rank != size - 1)
        for (i = 0; i < local_n; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            for (j = 0; j < A_ncols; j++)
            {
                if (A_cols[j] >= row_s && A_cols[j] < row_e)
                    A_dnnz[i]++;
                else
                    A_onnz[i]++;
            }
            for (j = 0; j < B_ncols; j++)
            {
                if (B_cols[j] >= row_s && B_cols[j] < row_e)
                    B_dnnz[i]++;
                else
                    B_onnz[i]++;
            }
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            A_onnz[i] += nev;
            B_onnz[i] += nev;
        }
    else
    {
        for (i = 0; i < local_n - nev; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            for (j = 0; j < A_ncols; j++)
            {
                if (A_cols[j] >= row_s && A_cols[j] < row_e)
                    A_dnnz[i]++;
                else
                    A_onnz[i]++;
            }
            for (j = 0; j < B_ncols; j++)
            {
                if (B_cols[j] >= row_s && B_cols[j] < row_e)
                    B_dnnz[i]++;
                else
                    B_onnz[i]++;
            }
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, NULL);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, NULL);
            A_dnnz[i] += nev;
            B_dnnz[i] += nev;
        }
        for (i = local_n - nev; i < local_n; i++)
        {
            A_dnnz[i] += nev;
            B_dnnz[i] += nev;
        }
    }
    MatMPIAIJSetPreallocation(AA, 0, A_dnnz, 0, A_onnz);
    MatSeqAIJSetPreallocation(AA, 0, A_dnnz);
    MatMPIAIJSetPreallocation(BB, 0, B_dnnz, 0, B_onnz);
    MatSeqAIJSetPreallocation(BB, 0, B_dnnz);
    free(A_dnnz);
    free(A_onnz);
    free(B_dnnz);
    free(B_onnz);

    if (rank != size - 1)
        for (i = 0; i < local_n; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < A_ncols; j++)
                MatSetValue(AA, i + row_s, A_cols[j], A_vals[j], INSERT_VALUES);
            for (j = 0; j < B_ncols; j++)
                MatSetValue(BB, i + row_s, B_cols[j], B_vals[j], INSERT_VALUES);
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                a = 0.0, b = 0.0;
                MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
            }
        }
    else
    {
        for (i = 0; i < local_n - nev; i++)
        {
            MatGetRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatGetRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < A_ncols; j++)
                MatSetValue(AA, i + row_s, A_cols[j], A_vals[j], INSERT_VALUES);
            for (j = 0; j < B_ncols; j++)
                MatSetValue(BB, i + row_s, B_cols[j], B_vals[j], INSERT_VALUES);
            MatRestoreRow(A, i + row_s, &A_ncols, &A_cols, &A_vals);
            MatRestoreRow(B, i + row_s, &B_ncols, &B_cols, &B_vals);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                a = 0.0, b = 0.0;
                MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
            }
        }
        for (i = local_n - nev; i < local_n; i++)
        {
            MatSetValue(BB, i + row_s, i + row_s, 1.0, INSERT_VALUES);
            for (j = 0; j < nev; j++)
            {
                double a = (double)(rand() + 1) / RAND_MAX;
                double b = (double)(rand() + 1) / RAND_MAX;
                if (i + row_s <= global_n - nev + j)
                {
                    if (i + row_s == global_n - nev + j)
                        a = 1.0, b = 1.0;
                    else
                        a = 0.0, b = 0.0;
                    MatSetValue(AA, i + row_s, global_n - nev + j, a, INSERT_VALUES);
                    MatSetValue(BB, i + row_s, global_n - nev + j, b, INSERT_VALUES);
                }
            }
        }
    }

    MatAssemblyBegin(AA, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(AA, MAT_FINAL_ASSEMBLY);
    MatSetOption(AA, MAT_SYMMETRIC, PETSC_TRUE);
    MatAssemblyBegin(BB, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BB, MAT_FINAL_ASSEMBLY);
    MatSetOption(BB, MAT_SYMMETRIC, PETSC_TRUE);

    MatConvert(AA, MATSBAIJ, MAT_INITIAL_MATRIX, auA);
    MatConvert(BB, MATSBAIJ, MAT_INITIAL_MATRIX, auB);
    MatDestroy(&AA);
    MatDestroy(&BB);

    // *auA = AA;
    // *auB = BB;
}

void SeparateEvecs(VECTORS *Evecs, VECTORS *output, DOUBLE *scale)
{
    INT myrank, size, remove;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    remove = 0;
    if (myrank == (size - 1))
        remove = 1;

    Vec temp, temp1;
    BVCreateVec((BV)(Evecs->data_slepc), &temp);
    BVCreateVec((BV)(output->data_slepc), &temp1);
    PetscInt start, end;
    VecGetOwnershipRange(temp, &start, &end);
    INT num = end - start - remove;
    INT *ind = malloc(num * sizeof(INT));
    for (int i = start; i < end - remove; i++)
    {
        ind[i - start] = i;
    }
    DOUBLE *values = malloc(num * sizeof(DOUBLE));
    VecGetValues(temp, num, ind, values);
    VecSetValues(temp1, num, ind, values, INSERT_VALUES);
    VecAssemblyBegin(temp1);
    VecAssemblyEnd(temp1);

    BVInsertVec((BV)(output->data_slepc), 0, temp1);
    INT gnrow = Evecs->global_length - 1;
    if (myrank == (size - 1))
        VecGetValues(temp, 1, &gnrow, scale);
    MPI_Bcast(scale, 1, MPI_DOUBLE, size - 1, MPI_COMM_WORLD);
}