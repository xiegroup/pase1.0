//本程序是为了检测基函数的设置是否正确
#include "OpenPFEM.h"
#include "linearsolver.h"
#include "errorestimate.h"
#include "math.h"
//下面的这个全局参数是为了来确定真解的正则性
#define alpha 0.166 
#define tau 1.0e-14
//下面是具体问题的设置
BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}
// exact solution : u = (x^2+y^2+z^2)^(\alpha/2)
// u = x^4+y^4+z^4;
void BoundFun(double X[3], int dim, double *values)
{
    values[0] = X[0]*X[0]*X[0]*X[0] + X[1]*X[1]*X[1]*X[1] + X[2]*X[2]*X[2]*X[2];
}
void ExactSolu3D(double X[3], int dim, double *values)
{ 
    values[0] = X[0]*X[0]*X[0]*X[0] + X[1]*X[1]*X[1]*X[1] + X[2]*X[2]*X[2]*X[2];
}
void ExactGrad3D(double X[3], int dim, double *values)
{
    values[0] = 4.0*X[0]*X[0]*X[0]; 
    values[1] = 4.0*X[1]*X[1]*X[1];
    values[2] = 4.0*X[2]*X[2]*X[2];
}

void ExactHess3D(double X[3], int dim, double *values)
{
    values[0] = 12.0*X[0]*X[0];
    values[1] = 12.0*X[1]*X[1];
    values[2] = 12.0*X[2]*X[2];
    values[3] = 0.0; 
    values[4] = 0.0;
    values[5] = 0.0;
}
void rhs(DOUBLE *X, INT dim, DOUBLE *value)
{
    value[0] = -12.0*(X[0]*X[0]+X[1]*X[1]+X[2]*X[2]);
}
//============================================
void ErrFun3DL2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]);
}
void ErrFun3DH1(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + 
                (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + 
                (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]);
}
void ErrFun3DH2(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] =  (femvale[0] - Funvalue[0]) * (femvale[0] - Funvalue[0]) + 
                (femvale[1] - Funvalue[1]) * (femvale[1] - Funvalue[1]) + 
                (femvale[2] - Funvalue[2]) * (femvale[2] - Funvalue[2]) + 
                (femvale[3] - Funvalue[3]) * (femvale[3] - Funvalue[3]) + 
                (femvale[4] - Funvalue[4]) * (femvale[4] - Funvalue[4]) + 
                (femvale[5] - Funvalue[5]) * (femvale[5] - Funvalue[5]) ;
}
void PostErrFunElem(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *value)
{
    value[0] = (rhsvalue[0] + femvalue[0] + femvalue[1] + femvalue[2]) *
               (rhsvalue[0] + femvalue[0] + femvalue[1] + femvalue[2]);
}
void PostErrFunEdge(DOUBLE *femvalue, DOUBLE *rhsvalue, DOUBLE *nomalvec, DOUBLE *value)
{
    value[0] = nomalvec[0] * femvalue[0] + nomalvec[1] * femvalue[1] + nomalvec[2] * femvalue[2];
}
void ErrFun(DOUBLE *femvale, DOUBLE *Funvalue, DOUBLE *value)
{
    value[0] = femvale[0] * femvale[0];
}
//考虑方程$-\Delta u = f$进行非线性迭代
//这个例子中 将刚度矩阵的线性部分和非线性部分分开计算
//线性部分只进行一次组装 非线性部分在每次迭代时组装
INT main(int argc, char *argv[])
{
    OpenPFEM_Init(&argc, &argv);
    // 生成初始的网格
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, DEFAULT_COMM);
    //BuildMesh(mesh, "../data/dataCube6.txt", SIMPLEX, TETHEDRAL);
    BuildMesh(mesh, "../data/LShapedArea3D4TreePart.txt", SIMPLEX, TETHEDRAL);
    UniformRefineMesh(mesh, 2);
    MeshPartition(mesh);
    UniformRefineMesh(mesh, 0);
    // 积分微分信息
    QUADRATURE *Quadrature = BuildQuadrature(QuadTetrahedral304);
    QUADRATURE *QuadratureEdge = BuildQuadrature(QuadTriangle36);
    MULTIINDEX L2multiindex[1] = {D000};
    MULTIINDEX H1ErrorIndex[3] = {D100, D010, D001};
    MULTIINDEX H2ErrorIndex[6] = {D200, D020, D002, D110, D101, D011};
    MULTIINDEX PosterioriErrorIndex[6] = {D200, D020, D002, D100, D010, D001};
    // 自适应加密
    INT ind_refine, ind, NUMVOLU;
    INT max_refine = 50;
    INT *NumElemOfBisection = malloc(max_refine * sizeof(INT));
    DOUBLE *PostErrorOfBisection = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *L2ErrorOfBisection   = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H1ErrorOfBisection   = malloc(max_refine * sizeof(DOUBLE));
    DOUBLE *H2ErrorOfBisection   = malloc(max_refine * sizeof(DOUBLE));
    FEMSPACE *femspace = NULL;
    FEMFUNCTION *solution = NULL;
    DOUBLE *PosteriorError = NULL, TotalError;
    DOUBLE time1, time2;
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print("======================== [ bisectionind ] 第 %d 次 ========================\n", ind_refine);
        //计算本次循环中的单元总个数
        MPI_Allreduce(&(mesh->num_volu), &NUMVOLU, 1, MPI_INT, MPI_SUM, mesh->comm);
        NumElemOfBisection[ind_refine] = NUMVOLU;
        //针对该层网格生成有限元空间
        femspace = BuildFEMSpace(mesh, C_T_P3_3D, BoundCond);
        //进行有限元插值
        FEMFUNCTION *solution = BuildFEMFunction(femspace);
        time1 = GetTime();
        FEMInterpolation(solution, ExactSolu3D, 1);  //有限元插值
        time2 = GetTime();
        OpenPFEM_Print("有限元插值的时间为%2.10f\n", time2 - time1);
        L2ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 1, &L2multiindex[0], ExactSolu3D, ErrFun3DL2, Quadrature);
        H1ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 3, &H1ErrorIndex[0], ExactGrad3D, ErrFun3DH1, Quadrature);
        H2ErrorOfBisection[ind_refine] = ErrorEstimate(solution, 6, &H2ErrorIndex[0], ExactHess3D, ErrFun3DH2, Quadrature);
        OpenPFEM_Print("L2误差为 %2.14f  H1误差为 %2.14f  H2误差为 %2.14f\n", L2ErrorOfBisection[ind_refine], H1ErrorOfBisection[ind_refine],
                        H2ErrorOfBisection[ind_refine]);
        //输出每次自适应加密的结果
        OpenPFEM_Print("单元个数 N = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %d ", NumElemOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");
        
        OpenPFEM_Print("L2Error = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", L2ErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        OpenPFEM_Print("H1Error = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", H1ErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        
        OpenPFEM_Print("H2Error = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", H2ErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");

        //后验误差估计
        PosteriorError = malloc(mesh->num_volu * sizeof(DOUBLE));
        time1 = GetTime();
        TotalError = 0.0;
        PosterioriErrorEstimate(solution, 3, 3, &PosterioriErrorIndex[0],
                                rhs, PostErrFunElem, PostErrFunEdge, Quadrature,
                                QuadratureEdge, PosteriorError, &TotalError);
        time2 = GetTime();
        OpenPFEM_Print("后验误差计算的时间为%2.10f\n", time2 - time1);
        PostErrorOfBisection[ind_refine] = TotalError;
        //网格加密
        time1 = GetTime();
        if (ind_refine < max_refine - 1)
        {
             OpenPFEM_Print("Start Adaptive refining the mesh!\n");
             MeshAdaptiveRefine(mesh, PosteriorError, 0.75);
             OpenPFEM_Print("完成自适应加密\n");
        }
        time2 = GetTime();
        OpenPFEM_Print("加密的时间为%2.10f\n", time2 - time1);
        // UniformRefineMesh(mesh, 1);
        //释放目前存储后验误差估计的内存
        OpenPFEM_Free(PosteriorError);   
        FreeFEMFuntion(solution);     
        //输出每次自适应加密的结果
        OpenPFEM_Print("单元个数 N = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %d ", NumElemOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");
        OpenPFEM_Print("PostErr = [ ");
        for (ind = 0; ind <= ind_refine; ind++)
        {
            OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind]);
        }
        OpenPFEM_Print("];\n");
    }
    //输出每次自适应加密的结果
    OpenPFEM_Print("单元个数 N = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %d ", NumElemOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    OpenPFEM_Print("PostErr = [ ");
    for (ind_refine = 0; ind_refine < max_refine; ind_refine++)
    {
        OpenPFEM_Print(" %2.14f ", PostErrorOfBisection[ind_refine]);
    }
    OpenPFEM_Print("];\n");
    // char endtitle[20], number[12], name1[64], filename[128];
    // strcpy(filename, "Cube");
    // strcpy(endtitle, ".vtk");
    // sprintf(number, "%d", myid);
    // strcat(filename, number);
    // strcat(filename, endtitle);
    // OpenPFEM_Print("Out put the mesh!\n");
    // WriteMesh3DVTK(mesh, filename);
    OpenPFEM_Finalize();
    return 0;
}