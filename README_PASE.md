# README For PASE1.0


This README file serves for requesting the "SISC Reproducibility Badge: code and data available" of the paper "A Massively Parallel Augmented Subspace Eigensolver and Its Implementation for Large Scale Eigenvalue Problems", authored by Yangfei Liao, Haochen Liu, Zijing Wang, and Hehu Xie.

GitLab repository link is https://gitlab.com/xiegroup/pase1.0.git.

The numerical examples are carried out on LSSC-IV in the State Key Laboratory of Scientific and Engineering Computing, Chinese Academy of Sciences. Each computing node has two 18-core Intel Xeon Gold 6140 processors at 2.3 GHz and 192 GB memory. In the following tests, we utilized the Intel MPI compiler (mpiicc), and both BLAS and LAPACK are versions of Intel MKL.

## Descriptions of Codes

In the PASE 1.0 repository, there are a total of two folders, namely OpenPFEM4PASE and PASE. 
In Section 6 of the paper, a total of five test cases are presented. The corresponding test programs for these cases are as follows:

1. The first test case is associated with the test program located at `./pase/pase/test/test_gmg1.c`.
2. The second test case is associated with the test program located at `./pase/pase/test/test_gmg2.c`.
3. The third test case is associated with the test programs located at `./pase/pase/test/test_gmg3.c` and `./pase/pase/test/test_gmg4.c`.
4. The fourth test case is associated with the test program located at `./OpenPFEM4PASE/test/Eigen3D_adapted_pase.c`.
5. The fifth test case is associated with the test program located at `./OpenPFEM4PASE/test/Eigen3D_adapted_pase2.c`.

## Preparations For PASE

1. Install MPI, BLAS, LAPACK, PETSc and SLEPc. And export the corresponding paths in the `bashrc` file.

   Note that when installing PETSc, it is necessary to simultaneously install packages `METIS` and `ParMETIS`.

2. Installation for PASE

   - The source code path for the PASE software package is `pase1.0/PASE`. 

   - After configuring and installing the external environment, it is necessary to modify the `./CMakeLists.txt`  in project PASE.
     1. Modify  `./config/LSSC4_oneapi2021` according to the MPI environment.
     2. Notice the paths for linking PETSc, SLEPc, and OPENPFEM in the `./CMakeLists` .
     3. Set `OPENPFEM_DIR` path to `pase1.0/OpenPFEM4PASE/shared`(Absolute path) in `bashrc` file.
   
   - Enter the path and then execute the following commands to install PASE
   
     ```bash
     mkdir build
     cd build
     cmake ../
     make
     ```

## Descriptions of Parameters

### PASE

|   Parameters    |                         Description                          |
| :-------------: | :----------------------------------------------------------: |
|       nev       |               number of eigenvalues to compute               |
|   num_levels    |                  number of multigrid levels                  |
|  initial_level  |      the layer at which the initial value is calculated      |
|    aux_rtol     | convergence tolerance of the eigenproblem (3.2) in Algorithm 1 |
|     pc_type     |                    type of preconditioner                    |
|   if_batches    |           whether to implement a batching strategy           |
|   batch_size    |         size of each batch (the $k$ in Algorithm 11)         |
|  more_aux_nev   | number of additional eigenpairs of the eigenvalue problem 5.3 to be solved such that the desired ones are contained (the $k_{\text{of}}$ in Algorithm 10) |
| more_batch_size | number of additional eigenpairs computed during each batch in order to help PASE converges faster (the $k_{\text{ol}}$ in Algorithm 11) |
|   max_refine    |                  number of refinement times                  |
| smoothing_type  |                   type of smoothing method                   |

### GCGE

|  Parameters  |                 Description                 |
| :----------: | :-----------------------------------------: |
|   nevConv    |      number of eigenvalues to compute       |
|    gapMin    |   threshold for detecting repeated roots    |
|   nevGiven   |         number of known eigenpairs          |
|  block_size  |    sizes of $P$ and $W$ in GCG algorithm    |
|    nevMax    | the maximum number of eigenpairs to compute |
|   nevInit    |    initial size of $X$ in GCG algorithm     |
|  tol_gcg[0]  |             absolute tolerance              |
|  tol_gcg[1]  |             relative tolerance              |
| compW_method |        the method for linear solver         |

### SLEPC

| Parameters |                         Description                          |
| :--------: | :----------------------------------------------------------: |
|    nev     |               number of eigenvalues to compute               |
|    ncv     | the maximum dimension of the subspace to be used by the solver |
|    mpd     |   the maximum dimension allowed for the projected problem    |
|   maxit    |                 the maximum iteration count                  |
| blocksize  |                   the block size in LOBPCG                   |
|  restart   | the percentage of the block of vectors to force a restart in LOBPCG |

## The model eigenvalue problem

The corresponding test code is `./pase/test/test_gmg1.c` in project PASE

### PASE

- Algorithm-related parameters 

  1. Set the macro definition of TESTPASE on line 11 of the program to $1$

     ```c
     #define TESTPASE 1 
     ```
  
  1. Set the number of eigenpairs on Line 26 to $200$($800$ in table 2)
  
     ```c
     int nev = 200; //800 for table 2
     ```
     
  2. Set the number of multigrid levels on Line 27 to $3$
  
     ```c
     int num_levels = 3;
     ```
  
  3. Set the numbers of uniform refinements while multigrid generation using FEM on Line 29 to $4$ and $1$
  
     ```c
     MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, MPI_COMM_WORLD);
     ```
  
     The fourth parameter of function `MatrixRead` means the number of refinements before the parallelization of FEM mesh, and the fifth parameter means the number of refinements after the parallelization of FEM mesh. The sum of the two number is the total  number of uniform refinements. In this example,  $5$ times refinements correspond to a total of $1,698,879$ DoFs, $6$ times refinements correspond to a total of  $13,785,215$ DoFs, while $7$ times refinements correspond to a total of $111,063,295$  DoFs.
  
     The fourth parameter of`PASE_PARAMETER_Create` represents the convergence accuracy of PASE, which is set to $10^{-8}$(Line 35 of PASE1.0/pase/test/test1.c) 
  
     ```c
     PASE_PARAMETER_Create(&param, num_levels, nev, 1e-8, PASE_GMG);
     ```
  
  4. Set the layer at which the initial value is calculated is represented by the following on Line 39 to $1$
  
     ```c
     param->initial_level = 1;
     ```
  
  5. Set the tolerance of the eigenproblem (3.2) in Algorithm 1 on Line 40 to $10^{-9}$
  
     ```c
     param->aux_rtol = 1e-9;
     ```
  
  6. Set the precondition type on Line 41 to $\rm{PRECOND\_A}$
  
     ```c
     param->pc_type = PRECOND_A; // PRECOND_NONE, PRECOND_B, PRECOND_B_A
     //PRECOND_NONE: Do not impose any preconditioning on A_{Hh}
     //PRECOND_B   : Only impose preconditioning on B_{Hh}
     // PRECOND_B_A: Impose preconditioning on B_{Hh} first, and then impose preconditionint on \tilde{A_{Hh}} during the solution of the linear system of equations
     ```

In Table 1 and Table 2, only the numbers of uniform refinements have been altered.  Specifically, 

```c
//Line 2 of Table 1,2
MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, MPI_COMM_WORLD);
//Line 3 of Table 1,2
MatrixRead(&A_array, &B_array, &P_array, 5, 1, num_levels, MPI_COMM_WORLD);
//Line 4 of Table 1,2
MatrixRead(&A_array, &B_array, &P_array, 5, 2, num_levels, MPI_COMM_WORLD);
```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
  -mat_superlu_dist_replacetinypivot
  ```

  to get better efficiency when using directsolver in PETSc(KSP).

  - The CPU time of PASE in ==Table 1==

    Run the executable program `./build/test1` ,  set the parameter `nev`  as $200$ and change the numbers of uniform refinements as stated above. 

    The number of runtime processes is set to $72$, $288$, and $576$, respectively.

  - The CPU time of PASE in ==Table 2==

    Run the executable program  `./build/test1` ,  set the parameter `nev`  as $800$ and change  the numbers of uniform refinements as stated above. 

    The number of runtime processes is set to $72$, $288$, and $1440$, respectively.

  - ==Figure 2==

    Run the executable program `./build/test1` with different numbers of processes ,  set the parameter `nev`  as $200$ and the number of uniform refinements as $6$.

  - Run the executable program `./build/test1` ,  change the parameter `nev`  and the parameter `pc_type`. The number of process is set to $288$.

    ```c
    param->pc_type = PRECOND_A;
    //param->pc_type = PRECOND_NONE;
    ```

### GCGE

The corresponding test code is `./pase/test/test_gmg1.c` in project PASE

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE ` on line 11 of the program to $2$

     ```c
     #define TESTPASE 2
     ```

  2. Set the number of eigenpairs on Line 26 to $200$($800$ in table 2)

     ```c
     int nev = 200; //800 for table 2
     ```
     
  2. In the function `PASE_DIRECT_GCGE`, the relevant GCG algorithm parameters have already been set based on the size of `nev`,  and the user only needs to provide the convergence accuracy. Specifically, when `nev` is 200 and 800 respectively, the corresponding parameters are set as follows

     ```c
     nev = 200;
     nevConv = nev;
     atol = 1e-2;
     rtol = 1e-8;
     block_size = nevConv / 5;
     nevMax = 2 * nevConv;
     nevInit = nevConv;
     ```

     ```c
     nev = 800;
     nevConv = nev;
     atol = 1e-2;
     rtol = 1e-8;
     block_size = nevConv / 10;
     nevInit = 3 * block_size;
     nevMax = nevInit + nevConv;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  - The CPU time of GCGE in ==Table 1 and 2==

    Run the executable program `./build/test1` ,  set the parameter `nev`  as $200$ and $800$, and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $72$, $288$, and $576$, respectively in Table 1. The number of runtime processes is set to $72$, $288$, and $1440$, respectively in Table 2.

### SLEPc

The corresponding test code is `./pase/test/test_gmg1.c` in project PASE. 

We conduct tests separately for the Krylov-Schur and LOBPCG methods and obtain optimal settings for `ncv` and `mpd`. Additionally, corresponding parameters were set for the LOBPCG method to achieve maximum efficiency.

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE` on line 11 of the program to $3$

     ```c
     #define TESTPASE 3
     ```

  2. Set the number of eigenpairs on Line 26 to $200$($800$ in table 2)

     ```c
     int nev = 200; //800 for table 2
     ```

  2. Set the parameters for selecting the`EPS` type on line 66 to choose Krylov-Schur and LOBPCG as solvers respectively.

     ```c
     flag = 2; //flag = 2: krylov-Schur, flag = 6: lobpcg
     ```

  3. Set the parameters for Krylov-Schur and LOBPCG. These parameters have already been set in the function `PASE_DIRECT_EPS`.

     ```c
     if (flag == 2) // ks
     {
       ncv = 2 * nev;
       mpd = ncv;
     }
     else if (flag == 6) // lobpcg
     {
       ncv = 2 * nev;
       blocksize = nev / 5;
       restart = 0.1;
       mpd = 3 * blocksize;
     }
     max_it = 2000;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -st_type sinvert -st_shift 0.0 -eps_target 0.0
  ```

  to get better efficiency when using Krylov-Schur.

  The CPU time of Krylov-Schur and LOBPCG in ==Table 1 and 2==

  Run the executable program `./build/test1` ,  set the parameter `nev`  as $200$ and $800$, and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $72$, $288$, and $576$, respectively in Table 1. The number of runtime processes is set to $72$, $288$, and $1440$, respectively in Table 2.

## A more general eigenvalue problem

### PASE

The corresponding test code is `./pase/test/test_gmg2.c` in project PASE.

- Algorithm-related parameters :

  1. Set the macro definition of TESTPASE on line 11 of the program to $1$

     ```c
     #define TESTPASE 1
     ```

  1. Set the number of eigenpairs on Line 26 to $200$($800$ in table 4)

     ```c
     int nev = 200; //800 for table 4
     ```

  2. Set the number of multigrid levels on Line 27 to $3$

     ```c
     int num_levels = 3;
     ```

  3. Set the numbers of uniform refinements while multigrid generation using FEM on Line 29 to $4$ and $1$

     ```c
     MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, MPI_COMM_WORLD);
     ```

     The fourth parameter of function `MatrixRead` means the number of refinements before the parallelization of FEM mesh, and the fifth parameter means the number of refinements after the parallelization of FEM mesh. The sum of the two number is the total  number of uniform refinements. In this example,  $5$ times refinements correspond to a total of $1,698,879$ DoFs, $6$ times refinements correspond to a total of  $13,785,215$ DoFs, while $7$ times refinements correspond to a total of $111,063,295$  DoFs.

     The fourth parameter of`PASE_PARAMETER_Create` represents the convergence accuracy of PASE, which is set to $10^{-8}$(Line 35 of PASE1.0/pase/test/test_gmg2.c) 

     ```c
     PASE_PARAMETER_Create(&param, num_levels, nev, 1e-8, PASE_GMG);
     ```

  4. Set the layer at which the initial value is calculated is represented by the following on Line 39 to $1$

     ```c
     param->initial_level = 1;
     ```

  5. Set the tolerance of the eigenproblem (3.2) in Algorithm 1 on Line 40 to $10^{-9}$

     ```c
     param->aux_rtol = 1e-9;
     ```

  6. Set the precondition type on Line 41 to $\rm{PRECOND\_A}$

     ```c
     param->pc_type = PRECOND_A;
     ```

  In Table 3 and Table 4, only the numbers of uniform refinements have been altered.  Specifically, 

  ```c
  //Line 2 of Table 3,4
  MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, MPI_COMM_WORLD);
  //Line 3 of Table 3,4
  MatrixRead(&A_array, &B_array, &P_array, 5, 1, num_levels, MPI_COMM_WORLD);
  //Line 4 of Table 3,4
  MatrixRead(&A_array, &B_array, &P_array, 5, 2, num_levels, MPI_COMM_WORLD);
  ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
  -mat_superlu_dist_replacetinypivot
  ```

  to get better efficiency when using directsolver in PETSc(KSP).

  - The CPU time of PASE in ==Table 3==

    Run the executable program `./build/test2` ,  set the parameter `nev`  as $200$ and change  the numbers of uniform refinements as stated above. 

    The number of runtime processes is set to $72$, $288$, and $576$, respectively.

  - The CPU time of PASE in ==Table 4==

    Run the executable program  `./build/test2` ,  set the parameter `nev`  as $800$ and change  the numbers of uniform refinements as stated above. 

    The number of runtime processes is set to $72$, $288$, and $1440$, respectively.

  - ==Figure 3==

    Run the executable program `./build/test2` with different numbers of processes ,  set the parameter `nev`  as $200$ and the number of uniform refinements as $6$.

### GCGE

The corresponding test code is `./pase/test/test_gmg2.c` in project PASE.

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE` on line 11 of the program to $2$

     ```c
     #define TESTPASE 2
     ```

  2. Set the number of eigenpairs on Line 26 to $200$($800$ in table 4)

     ```c
     int nev = 200; //800 for table 4
     ```
     
  2. In the function `PASE_DIRECT_GCGE`, the relevant GCG algorithm parameters have already been set based on the size of `nev`,  and the user only needs to provide the convergence accuracy. Specifically, when `nev` is $200$ and $800$ respectively, the corresponding parameters are set as follows

     ```c
     nev = 200;
     atol = 1e-2;
     rtol = 1e-8;
     nevConv = nev;
     block_size = nevConv / 5;
     nevMax = 2 * nevConv;
     nevInit = nevConv;
     ```

     ```c
     nev = 800;
     atol = 1e-2;
     rtol = 1e-8;
     nevConv = nev;
     block_size = nevConv / 10;
     nevInit = 3 * block_size;
     nevMax = nevInit + nevConv;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The CPU time of GCGE in ==Table 3 and 4==

  Run the executable program `./build/test2` ,  set the parameter `nev`  as $200$ and $800$, and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $72$, $288$, and $576$, respectively in Table 3. The number of runtime processes is set to $72$, $288$, and $1440$, respectively in Table 4.

### SLEPc

The corresponding test code is `./pase/test/test_gmg2.c` in project PASE. 

We conduct tests separately for the Krylov-Schur and LOBPCG methods and obtain optimal settings for `ncv` and `mpd`. Additionally, corresponding parameters were set for the LOBPCG method to achieve maximum efficiency.

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE` on line 11 of the program to $3$

     ```c
     #define TESTPASE 3
     ```

  2. Set the number of eigenpairs on Line 26 to $200$($800$ in table 4)

     ```c
     int nev = 200; //800 for table 4
     ```

  2. Set the parameters for selecting the`EPS` type on line 66 to choose Krylov-Schur and LOBPCG as solvers respectively.

     ```c
     flag = 2; //flag = 2: krylov-Schur, flag = 6: lobpcg
     ```

  3. Set the parameters for Krylov-Schur and LOBPCG. These parameters have already been set in the function `PASE_DIRECT_EPS`.

     ```c
     if (flag == 2) // ks
     {
       ncv = 2 * nev;
       mpd = ncv;
     }
     else if (flag == 6) // lobpcg
     {
       ncv = 2 * nev;
       blocksize = nev / 5;
       restart = 0.1;
       mpd = 3 * blocksize;
     }
     max_it = 2000;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -st_type sinvert -st_shift 0.0 -eps_target 0.0
  ```

  to get better efficiency when using Krylov-Schur.

  The CPU time of Krylov-Schur and LOBPCG in ==Table 3 and 4==

  Run the executable program `./build/test2` ,  set the parameter `nev`  as $200$ and $800$, and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $72$, $288$, and $576$, respectively in Table 3. The number of runtime processes is set to $72$, $288$, and $1440$, respectively in Table 4.

## Numerical tests for batch scheme

### PASE

The corresponding test code is `./pase/test/test_gmg3.c` and `./pase/test/test_gmg4.c` in project PASE.

- Algorithm-related parameters

  1. Set the macro definition of `TESTPASE` on line 11 of the program to $1$

     ```c
     #define TESTPASE 1
     ```

  2. Set the number of eigenpairs on Line 26 to $2000$

     ```c
     int nev = 2000;
     ```

  3. Set the number of multigrid levels on Line 27 to $3$

     ```c
     int num_levels = 3;
     ```

  4. Set the numbers of uniform refinements while multigrid generation using FEM on Line 29 to $4$ and $1$

     ```c
     MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, MPI_COMM_WORLD);
     ```

     The fourth parameter of function `MatrixRead` means the number of refinements before the parallelization of FEM mesh, and the fifth parameter means the number of refinements after the parallelization of FEM mesh. The sum of the two number is the total  number of uniform refinements. In this example, $6$ times refinements correspond to a total of  $13,785,215$ DoFs, while $7$ times refinements correspond to a total of $111,063,295$  DoFs.

     The fourth parameter of`PASE_PARAMETER_Create` represents the convergence accuracy of PASE, which is set to $10^{-8}$(Line 35 of PASE1.0/pase/test/test_gmg3.c) 

     ```c
     PASE_PARAMETER_Create(&param, num_levels, nev, 1e-8, PASE_GMG);
     ```

  5. Set the layer at which the initial value is calculated is represented by the following on Line 39 to $1$

     ```c
     param->initial_level = 1;
     ```

  6. Set the tolerance of the eigenproblem (3.2) in Algorithm 1 on Line 40 to $10^{-9}$

     ```c
     param->aux_rtol = 1e-9;
     ```

  7. Set the precondition type on Line 41 to $\rm{PRECOND\_A}$

     ```c
     param->pc_type = PRECOND_A;
     ```

  8. Set the parameter indicating whether to implement a batching strategy (Algorithm 11) on Line 42 to `true`

     ```c
     param->if_batches = true;
     ```

  9. Set the size of each batch (the $k$ in Algorithm 11) on Line 43 to $500$

     ```c
     param->batch_size = 500;
     ```

  10. Set the number of additional eigenpairs of the eigenvalue problem 5.3 to be solved such that the desired ones are contained (the $k_{\text{of}}$ in Algorithm 10) on Line 44 to $60$

      ```c
      param->more_aux_nev = 60;
      ```

  11. Set the number of additional eigenpairs computed during each batch in order to help PASE converges faster (the $k_{\text{ol}}$ in Algorithm 11) on Line 45 to $70$ in `test_gmg3.c` and $80$ in `test_gmg4.c`.

      ```c
      param->more_batch_size = 70;
      //param->more_batch_size = 80;
      ```

  In Table 5 and Table 6, only the numbers of uniform refinements have been altered.  Specifically, 

  ```c
  //Line 2 of Table 5,6
  MatrixRead(&A_array, &B_array, &P_array, 5, 1, num_levels, MPI_COMM_WORLD);
  //Line 3 of Table 5,6
  MatrixRead(&A_array, &B_array, &P_array, 5, 2, num_levels, MPI_COMM_WORLD);
  ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
  -mat_superlu_dist_replacetinypivot
  ```

   to get better efficiency when using directsolver in PETSc(KSP).

  - The CPU time of PASE in ==Table 5==

    Run the executable program `./build/test3` and change the numbers of uniform refinements as stated above. 

    The number of runtime processes is set to $288$ and $1440$, respectively.

  - The CPU time of PASE in ==Table 6==

    Run the executable program `./build/test4` and change the numbers of uniform refinements as stated above. 
    
    The number of runtime processes is set to $288$ and $1440$, respectively.

### GCGE

The corresponding test code is `./pase/test/test_gmg3.c` and `./pase/test/test_gmg4.c` in project PASE.

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE ` on line 11 of the program to $2$

     ```c
     #define TESTPASE 2
     ```

  2. Set the number of eigenpairs on Line 26 to $2000$

     ```c
     int nev = 2000;
     ```
     
  2. In the function `PASE_DIRECT_GCGE`, the relevant GCG algorithm parameters have already been set based on the size of `nev`,  and the user only needs to provide the convergence accuracy. Specifically, when `nev` is $2000$, the corresponding parameters are set as follows

     ```c
     nev = 2000;
     nevConv = nev;
     atol = 1e-2;
     rtol = 1e-8;
     block_size = 200;
     nevInit = 3 * block_size;
     nevMax = nevInit + nevConv;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  - The CPU time of GCGE in ==Table 5 and 6==

    Run the executable program `./build/test3`  and `./build/test4`,  set the parameter `nev`  as $2000$ and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $288$ and $1440$, respectively in Table 5 and Table 6. 

### SLEPc

The corresponding test code is `./pase/test/test_gmg3.c` and `./pase/test/test_gmg4.c` in project PASE.

We conduct tests separately for the Krylov-Schur and LOBPCG methods and obtain optimal settings for `ncv` and `mpd`. Additionally, corresponding parameters were set for the LOBPCG method to achieve maximum efficiency.

- Algorithm-related parameters 

  1. Set the macro definition of `TESTPASE` on line 11 of the program to $3$

     ```c
     #define TESTPASE 3
     ```

  2. Set the number of eigenpairs on Line 26 to $2000$

     ```c
     int nev = 2000;
     ```

  3. Set the parameters for selecting the`EPS` type on Line 70 to choose Krylov-Schur and LOBPCG as solvers respectively.

     ```c
     flag = 2; //flag = 2: krylov-Schur, flag = 6: lobpcg
     ```

  4. Set the parameters for Krylov-Schur and LOBPCG. These parameters have already been set in the function `PASE_DIRECT_EPS`.

     ```c
     if (flag == 2) // ks
     {
         ncv = 2400;
         mpd = 800;
     }
     else if (flag == 6) // lobpcg
     {
       ncv = 2 * nev;
       blocksize = nev / 5;
       restart = 0.1;
       mpd = 3 * blocksize;
     }
     max_it = 2000;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -st_type sinvert -st_shift 0.0 -eps_target 0.0
  ```

  to get better efficiency when using Krylov-Schur.

  The CPU time of Krylov-Schur and LOBPCG in ==Table 5 and 6==

  Run the executable program `./build/test3`  and `./build/test4`,  set the parameter `nev`  as $2000$ and change the numbers of uniform refinements  as stated above.   The number of runtime processes is set to $288$ and $1440$, respectively in Table 5 and Table 6. 

------------

##  Adaptive finite element method

The corresponding test code is `./test/Eigen3D_adapted_pase.c` in project OPENPFEM4PASE.

- Compilation process

  1. Install MPI and PASE. And export the corresponding paths in the `bashrc` file.

  2. Installation for OpenPFEM4PASE

     - The source code path for the OpenPFEM software package is `pase1.0/OpenPFEM4PASE`. 

     - After configuring and installing the external environment, it is necessary to modify the `./CMakeLists.txt`  in project OpenPFEM4PASE.
       1. Modify  `./config/LSSC4_oneapi2021` according to the MPI environment.
       2. Modify the paths for linking PETSc, SLEPc, and PASE in the `./CMakeLists` .
       3. Set `PASE_DIR` path to `pase1.0/PASE/shared`(Absolute path) in `bashrc` file.

  3. Enter the path and then execute the following commands to install OpenPFEM4PASE

     ```bash
     mkdir build
     cd build
     cmake ../
     make
     ```

- Algorithm-related parameters 

  1. Set the number of refinement times on Line 79 to $10$

     ```c
     INT max_refine = 10;
     ```

  2. Set the number of eigenpairs on Line 80 to $200$

     ```c
     INT nev = 200;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
  -mat_superlu_dist_replacetinypivot
  ```

   to get better efficiency when using directsolver in PETSc(KSP).

  - ==Figure 4==

    Run the executable program `./build/test4` , the data used in the Figure 4 will be printed in the output file like

    ```
    element N = [  196608	236828	319778	444937	639576	940419	1403539	2081119	2974727	4336282
     ];
    PostErr = [  1.2211	0.8477	0.5667	0.3736	0.2381	0.1534	0.1023	0.069	0.0466	0.0308
     ];
    ```

  - ==Figure 5==

    Run the executable program `./build/test4` with different numbers of processes, the data used in the Figure 5 will be printed in the output file like
    
    ```
    The total time of PASE solving after 5 times refinements is 619.915023 sec.
    ```

  


---

###  Adaptive finite element method for Hydrogen atom

The corresponding test code is `./test/Eigen3D_adapted_pase2.c` in project OPENPFEM4PASE.

- Compilation process

  1. Install MPI and PASE. And export the corresponding paths in the `bashrc` file.

  2. Installation for OpenPFEM4PASE

     - The source code path for the OpenPFEM software package is `pase1.0/OpenPFEM4PASE`. 

     - After configuring and installing the external environment, it is necessary to modify the `./CMakeLists.txt`  in project OpenPFEM4PASE.
       1. Modify  `./config/LSSC4_oneapi2021` according to the MPI environment.
       2. Modify the paths for linking PETSc, SLEPc, and PASE in the `./CMakeLists` .
       3. Set `PASE_DIR` path to `pase1.0/PASE/shared`(Absolute path) in `bashrc` file.

  3. Enter the path and then execute the following commands to install OpenPFEM4PASE

     ```bash
     mkdir build
     cd build
     cmake ../
     make
     ```

  make sure the executable in `./CMakeLists.txt` is `${CMAKE_CURRENT_SOURCE_DIR}/test/Eigen3D_adapted_pase2.c` 

- Algorithm-related parameters 

  1. Set the number of refinement times on Line 79 to $10$

     ```c
     INT max_refine = 10;
     ```

  2. Set the number of eigenpairs on Line 80 to $200$

     ```c
     INT nev = 200;
     ```

- Modify the parameters as mentioned above and recompile.

- Running process

  The command-line parameters for the executable should be 

  ```bash
  -mat_superlu_dist_rowperm NOROWPERM -mat_superlu_dist_colperm PARMETIS \
  -mat_superlu_dist_replacetinypivot
  ```

   to get better efficiency when using directsolver in PETSc(KSP).

  - ==Figure 6==

  - Run the executable program  `./build/test5` , the data used in the Figure 6 will be printed in the output file like

    ```
    element N = [  196608	236828	319778	444937	639576	940419	1403539	2081119	2974727	4336282
     ];
    PostErr = [  1.2211	0.8477	0.5667	0.3736	0.2381	0.1534	0.1023	0.069	0.0466	0.0308
     ];
    ```

  - ==Figure 7==

  - Run the executable program  `./build/test5` with different numbers of processes, the data used in the Figure 7 will be printed in the output file like

    ```
    The total time of PASE solving after 5 times refinements is 619.915023 sec.
    ```


In the default settings, we print iteration information of PASE. If you do not need to output relevant information, please set the value of `PRINT_INFO` to `0`  in line 25 of `./pase/src/pase_sol.c`.

